---
layout: post
slug: trucos-cambio-armario-ninos
title: 5 Trucos para el cambio de armario de los niños
date: 2020-10-04T18:19:29+02:00
image: /images/posts/cambio-armario/p-armario-azul.jpg
author: maria
tags:
  - 0-a-3
  - 3-a-6
  - 6-a-12
  - trucos
---
No es que vaya tarde con el cambio de armario, es que en mi tierra no nos atrevemos a ejecutarlo hasta casi noviembre porque le tememos al veranillo de San Miguel más que a un nublao... Pero ya hay que ir afrontándolo, chicas porque ya se nos ve ridículas con las camisetas marineras y los pantalones ibicencos... Que va a llegar Halloween y nos va a pillar en shorts! Por eso, para ayudaros a decidiros os voy a dar unos trucos para el cambio de armario de los niños que os van a facilitar mucho el temible pero inevitable trance, sobre todo si sois familia numerosa, como es nuestro caso (os recuerdo estos [7 trucos para ahorrar en la ropa de los niños](https://madrescabreadas.com/2020/10/04/ahorro-ropa-ninos/).

También os dejo mi lista de mis [imprescindibles para llevar a cabo el cambio de armario con éxito](https://www.amazon.es/shop/madrescabreadas?listId=2ODK7M3D7SLRR&ref=cm_sw_em_r_inf_list_own_madrescabreadas_dp_5wOKewiIaYWm7) (enlace afiliado).

## Programa el momento para el cambio de armario

Lo primero que hay que plantearse es el momento más adecuado para hacerlo. Lo mejor es reservar una mañana o una tarde del fin de semana para que quede culminada la tarea en unidad de acto (si no el caos puede reinar en casa durante toda la semana siguiente), y no pretender hacer el de los adultos y el de los niños a la vez. Es preferible dedicar un tiempo exclusivo para la ropa de los peques, y otro día dedicarte al tuyo (o mejor, organiza primero tu ropa, así estarás más tranquila y con la cabeza más centrada en la de tus hijos).

![mujer ordenando ropa](/images/posts/cambio-armario/mujer-doblando-ropa.jpg)

## Material de almacenaje para el cambio de armario

Procura que para la fecha elegida tengas todo el material necesario para no dejarte flecos sueltos, es decir, todas las bolsas o cajas donde vas a almacenar la ropa saliente o donde la vas a meter para transportar la que vayas a donar. También es muy útil (aparte de un vicio) una [etiquetadora](https://amzn.to/2VkWuT6) para clasificar la que almacenes para herencias de hermanos por talla y sexo, así como bolsitas de [lavanda (espliego)](https://amzn.to/3IB9JsN) para poner en los armarios y en las [bolsas o cajas de almacenaje](https://amzn.to/31VE4Ld) para otras temporadas para evitar que las polillas hagan de las suyas; yo la compro a granel y voy rellenando [bolsitas de tela](https://amzn.to/2OrsGmh) (enlace afiliado) para distribuirlas.

![lavanda](/images/posts/cambio-armario/lavanda.jpg)

Hay muchos tipos de [bolsas de almacenaje de ropa, pero yo os recomiendo que sean transparentes y rectangulares](https://amzn.to/31VE4Ld) (enlace afiliado) de un tamaño que no exceda vuestro altillo, canapé o lugar donde las vayáis a guardar.

## Qué hago con los niños

Ha llegado el día del zafarrancho: manos a la obra! Procura tener a los niños entretenidos, pero no muy lejos de ti porque los vas a necesitar para que se prueben la ropa. Puedes plantearlo como un juego tipo desfile de modelos o competición para ver quién se cambia más rápido, porque para ellos suele ser bastante tedioso probarse 10 camisetas, 5 pantalones, 2 abrigos y 4 jerseys... Pero recuerda, esto es absolutamente necesario para dejar en el armario sólo lo que tengamos claro que van a usar.

## Clasifica la ropa saliente

Es importante que antes de sacar lo almacenado de la temporada anterior clasifiquemos lo que tenemos actualmente en el armario de cada uno y hagamos 3 montones:

* un montón de ropa que pudiere servir al mismo individuo para la temporada siguiente porque le queda un poco grande  que previo lavado, embolsado y etiquetado irá al altillo. Conviene guardarla lavada, arreglarla si hiciera falta; por ejemplo poniendo [parches de rodilleras](https://amzn.to/33aB25U) (enlace afiliado) o reponiendo botones para facilitarle el trabajo a nuestro yo del futuro.
* otro montón de ropa heredable por alguno de sus hermanos, embolsada, lavada, arreglada, etiquetada por talla y sexo y con su bolsita de lavanda dentro.
* otro montón de ropa para donar a primos,otros familiares, o alguna institución benéfica como [Cáritas](https://www.caritas.es), [Proyecto Abraham](http://www.proyectoabraham.org/empleo-de-insercion/recogida/)... en una bolsa o caja fácilmente transportable que deberá abandonar el domicilio a la mayor brevedad (o acabará convirtiéndose en un miembro más de la familia).

Una vez sacada toda la ropa del armario, deja algunas prendas por si acaso el calor no termina de irse, pero no demasiada. 
Aprovecha para limpiar el armario a fondo antes de meter de nuevo ropa en él y coloca las bolsitas de lavanda por todas partes.

## Desfile de modelos

Vamos con la segunda parte! El desfile de modelos
Es la hora de sacar todo lo que guardamos la temporada anterior y revisar lo que les viene y lo que está en buen estado.

Ármate de paciencia y que se vayan probando la ropa que intuiste que les podría valer. De nuevo haz varios montones, pero de momento no te lances a lavar y planchar a lo loco (ahora es cuando te alegrarás si guardaste la ropa limpia, arreglada y con bolsita de lavanda):

![perchas](/images/posts/cambio-armario/perchas.jpg)

* un montón de ropa que les viene, que directamente colocarás en el armario en perchas para que se vaya estirando, o doblada si ves que se va a deformar por colgarla. Usa [perchas finas](https://amzn.to/2MjKayy) para que quepan más en la barra, y todas del mismo tamaño. Ropa interior y pijamas mételos en [cajones con clasificadores](https://amzn.to/2ASYAA4) (enlace afiliado) hecha un rulo y de forma vertical. Si ves necesario lavar algo o plancharlo, no lo hagas en este momento; ve haciéndolo poco a poco. Lo importante es dejar terminado el cambio de armario en el tiempo previsto.
* otro montón de ropa heredable por sus hermanos embolsado y etiquetado por talla y sexo.
* otro montón de ropa para donar en bolsas o cajas dispuestas para salir pitando de casa.

## Consejos prácticos

* Una vez que sabemos con lo que contamos para cada niño, llega el momento de hacer una lista con lo que hace falta comprar. Antes no es recomendable comprar al tun tun, porque seguramente compraremos de más. 
* Distribuye cada armario, en función de la estatura de cada niño, con barras, cajones, cajas o baldas a su altura para que alcancen a guardar y buscar cada día su ropa, y pon una etiqueta en cada una para que sepan dónde están sus cosas. Los [armarios tipo Montessori](https://amzn.to/2MjfWvn) (enlace afiliado) pueden ayudar a que sean autónomos.
* El calzado, mejor sácalo de la habitación si tienes posibilidad, sobre todo si tus hijos ya son mayores (por motivos y olores varios). Puedes aprovechar espacios muertos en el baño para poner unos [zapateros encajables](https://amzn.to/2Iwr2fo) por módulos como éstos, o en la galería o terraza en unos [armarios zapateros de exterior](https://amzn.to/31T91zP) (enlace afiliado) por ejemplo. 

Sólo me resta desearos suerte y pediros que compartáis este post si pensáis que puede ser útil para más familias.

Feliz otoño!

* *Foto de portada by jordi pujadas on Unsplash*
* *Foto 1 by Sarah Brown on Unsplash*
* *For 2 by rocknwool on Unsplash*
* *Foto 3 by Caleb Lucas on Unsplash*