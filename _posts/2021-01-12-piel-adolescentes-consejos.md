---
author: maria
date: '2021-01-12T11:10:29+02:00'
image: /images/posts/piel-adolescentes/p-mascarilla.jpg
layout: post
tags:
- adolescencia
- invitado
title: Consejos para el cuidado de la piel de adolescentes
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puedes inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy merendamos con Leticia Gozalo, de [Make Up by Ele](https://makeupbyele.es), una mujer emprendedora que apostó por su sueño de ser maquilladora y ha creado una comunidad de mujeres que se quieren y se cuidan, entre las que me encuentro. Puedes seguirla en su [Instagram @makeupbyele](https://www.instagram.com/makeupbyele_/), donde encontrarás consejos prácticos y honestos para el cuidado de tu piel y para maquillarte correctamente sin gastarte una pasta en productos y brochas.
> 
> El maquillaje siempre ha sido una constante en su vida. De niña maquillaba a sus muñecos y de adolescente estaba esperando la ocasión perfecta para desenfundar los pinceles y poder maquillarme a sí misma… ¡y a quien se pusiera por delante!
> Durante años creyó lo que le decían sobre que el maquillaje no era una profesión, así que se formó en protocolo y organización de eventos. Pero un buen día decidió dejar de escuchar a los demás y empezar a escribir su propio cuento.
> Así nació Make Up by Ele.
> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os presento a Leticia:

![mujer maquillada](/images/posts/piel-adolescentes/barbilla.jpg)

Uno de mis recuerdos más nítidos de mi niñez es ver a mi madre haciendo su ritual de belleza cada noche. No se lo saltaba ningún día. Su tónico, su crema…y sus mascarillas ocasionales. Tal vez por eso siempre me ha llamado la atención el mundo de la cosmética y el maquillaje. 

Pero lo mejor será que me presente. Soy Leticia y soy maquilladora profesional. Por mi trabajo me encuentro con muchísimas personas de diferentes edades y con la piel en diferentes estados, pero si hay una cosa que siempre me ha llamado la atención es que los adolescentes son los grandes olvidados. Es decir, vemos como habitual leer en una revista femenina un artículo que diga “cuida tu piel a los 40”. Pero ¿recuerdas haber visto "cuida tu piel adolescente" en la “Super Pop” que te comprabas en los 90?

## La piel adolescente, la gran olvidada

Cuando la adolescencia empezó a hacer estragos en mi piel, recuerdo que mi madre me dijo algo como “lávate la cara todas las mañanas y todas las noches, échate agua de rosas y si se te pone la cara peor, te llevamos al dermatólogo”. Y me puedo dar con un canto en los dientes, porque por triste que parezca, no es algo habitual. De hecho, recuerdo a gente conocida con la nariz llena de puntos negros porque no tenían ningún tipo de hábito de cuidado facial. 

Y es que a los padres en ocasiones os cuesta entrar en esos temas con vuestros hijos. Y sí, hablo de hijos, porque la piel deben cuidarla todos. Existe todavía la creencia de que un chico o chica de 12-13 años no necesita cosmética porque “es muy joven para esas cosas y su piel no necesita cremas”. Y eso no deja de ser un mito como otros muchos otros que hay. 

Piel solo tenemos una y aunque va cambiando, debemos tener la costumbre de cuidarla. Aunque eso sí, adaptando la rutina a las necesidades de cada momento. 

Y cuanto más chiquititos adquieran ciertos hábitos, más fácil les será mantenerlos de adultos. 

## Rutina del cuidado de la piel adolescente en 3 pasos

Una rutina básica para un adolescente solo necesita 3 pasos: limpieza, hidratación y protección solar. Sí, protección solar. Todos los días del año. No solo en verano cuando va a la piscina y lo embadurnas en Nivea Sun. Porque, analízalo fríamente: ¿cuántos adolescentes conoces que se pongan FPS antes de ir al instituto? Pocos. Muy pocos. 

## Prevenir el acné en los adolescentes

La piel de los adolescentes tiene ciertas particularidades. Y todos los que ya hemos pasado por esa etapa las recordamos. Está más grasa, tenemos mayor tendencia a los granitos y los puntos negros, los poros están más dilatados y hay una mayor transpiración. 

Si a esto le sumamos que la piel durante la vida diaria se ensucia, pues podemos tener la combinación perfecta para que surja el tan temido acné. 

![chica con acné](/images/posts/piel-adolescentes/chica-acne.jpg)

Para evitar que todo esto se acumule en los poros, la limpieza es imprescindible. El ritual de limpieza es sencillo: se debe efectuar dos veces al día, incidiendo sobre todo en la zona T (frente, nariz y mentón). Lo que sí es recomendable es que no sea un producto muy astringente, porque puede crear efecto rebote. Y es importante que sea un producto poco agresivo, que le proteja el manto hidrolipídico. 

También sería recomendable que tuviera caléndula o camomila, que tienen efecto calmante y va fenomenal para la inflamación que provoca el acné. Y puede ser en espuma, gel…el formato da igual. 

## Si tu hija adolescente ha empezado a maquillarse

En el caso de chicas que ya hayan empezado a maquillarse, también conviene incidir en que por muy cansadas que estén, desmaquillarse es básico. Para ello pueden optar por un aceite como el de jojoba. Y no, no estoy loca, los aceites o mantecas son la mejor opción para desmaquillarse, ya que arrastran perfectamente toda la suciedad. Además, como el desmaquillado es el primer paso, cualquier residuo que quede en la piel se eliminará con el limpiador. 

![mujer con paleta de sombras](/images/posts/piel-adolescentes/paleta.jpg)

## La importancia de la hidratación en la piel de los adolescentes

Otro mito que hay que desmontar es que la piel de los adolescentes no necesita hidratación. Y esto surge debido a que hay muchas personas que confunden grasa con hidratación. Una piel grasa necesita hidratarse tanto como una piel seca, o incluso más. Así que lo verdaderamente importante es elegir una crema adecuada. 

## No olvides la protección solar en los adolescentes

Y para terminar una rutina adecuada para tus hijos, es importante que incidas en la importancia de la protección solar. Aunque lo recomendable es que utilicen un protector solar con un factor 30-50, si crees que no va a mantener ese hábito en la rutina, busca una hidratante que lo contenga. Mejor será eso a que salga sin protegerse la piel. 

Aunque a la rutina se le pueden añadir muchos más productos, es importante no saturarlos al principio. Mejor que cumplan rigurosamente los tres pasos que he comentado a que se saturen de productos y los apliquen de forma incorrecta o que directamente, pasen de ello. 

## Adelántate a las necesidades de tu hija adolescente

Una vez establecida la rutina, se pueden ir introduciendo productos como el tónico o un contorno de ojos que sea hidratante, pero sin necesidad de que sea antiedad ni nada parecido. 

Si tu no le facilitas los productos adecuados puede pasar que se compren lo que ellos crean o te cojan los tuyos y pueden dañarse la piel. 

## Consejos para los granos de los adolescentes

También debes recordarles que no deben tocarse los granitos, ya que pueden quedar marcas y en el caso de que quieran secar un grano, se puede recurrir a un bastoncillo impregnado en aceite del árbol del té, que tiene propiedades bactericidas, aunque la mejor opción es utilizar un stick que ya viene preparado para ello. 

Y por último, y no menos importante, haz hincapié en que mantenga limpia la pantalla del móvil. Van pegaditos a él, y se trata de una fuente de bacterias importante.

## Cada piel es distinta. Asesórate con una experta

Seguro que estás pensando que no he recomendado ningún producto. Y es que cada piel tiene sus necesidades, no hay dos pieles iguales y sería un error decirte una lista de productos que pueden o no ir bien para la piel de tu hijo. 

Si hay un problema cutáneo, como un acné severo, lo mejor es acudir a tu dermatólogo. Si solo se trata de establecer una rutina e implantar hábitos de cuidado facial, una asesoría de skincare o un [curso de auto maquillaje](https://makeupbyele.es/aprende-a-maquillarte-en-murcia/) en el que se trate sobre todo el cuidado de la piel son tu solución. 

Si quieres saber más, puedes visitar [mi web](https://makeupbyele.es/despedidas-de-soltera-beauty/) o mi perfil en [RRSS](https://makeupbyele.es/despedidas-de-soltera-beauty/) y así conocerme un poquito mejor y preguntar todas las dudas que te surjan. También  organizo [beauty parties](https://makeupbyele.es/despedidas-de-soltera-beauty/), ideales para cumpleaños.

Si te ha gustado comparte, así nos ayudas a seguir escribiendo este blog.


*Photo de portada by Mutzii on Unsplash*

*Photo by Ayo Ogunseinde on Unsplash chica*