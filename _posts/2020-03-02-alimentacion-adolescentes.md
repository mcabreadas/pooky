---
date: '2020-03-02T18:19:29+02:00'
image: /images/posts/alimentacion-adolescentes/p-perrito.jpg
layout: post
tags:
- nutricion
- adolescencia
- crianza
- salud
title: Alimentacion saludable y adolescentes
---

En casa ya no es tan fácil que mis hijos sigan una dieta equilibrada como cuando eran pequeños y comían, de mejor o peor grado, lo que se ponía en la mesa. Cuando llegan a la edad adolescente es mucho más difícil para los padres controlar la alimentación de nuestros hijos debido a su mayor grado de autonomía, los horarios dispares de comidas y la rebeldía propia de la edad. Pero es nuestra responsabilidad tomar cartas en el asunto porque una buena alimentación es la clave para que se desarrollen saludablemente y prevenir enfermedades en el futuro. Así que o nos ponemos las pilas con la alimentación de los adolescentes o más nos vale tener una [farmacia barata](https://www.farmaciabarata.es/) cerca.

Cierto es que siempre podemos recurrir a vitaminas o suplementos alimenticios en épocas concretas en que necesite cubrir con urgencia ciertas carencias que puedan hacer peligrar su salud, pero una vez superada la crisis, lo ideal es mantener los buenos niveles de nutrientes que su cuerpo necesita con una buena y equilibrada alimentación adaptada a su edad y sus necesidades.

## Por qué es importante comer bien en la adolescencia

Es importante cuidar la alimentación en esta etapa porque las necesidades nutricionales crecen, ya que los chicos y chicas adquieren el 40-50% de su peso definitivo y el 25% de su talla adulta. También aumenta un 50% su masa esquelética y otro tanto la muscular.

Necesitan ingerir al día 2.750 kilocalorías los chicos y 2.200 las chicas. Y aumentan también las necesidades de **hierro**, vitamina A, vitamina D, vitamina C, **ácido fólico** y de **calcio**, principalmente.

En las chicas es muy importante que mantengan sus reservas de hierro bien llenas debido a las pérdidas que pueda ocasionarle la menstruación (ver post "[Cómo afrontar la primera regla](https://madrescabreadas.com/2019/09/21/primera-regla/)"). Yo misma sufrí a los 19 años una anemia ferropénica y lo pasé francamente mal, lo que me obligó a tomar durante meses suplemento de hierro en ampollas que compraba en la farmacia, hasta que logré reponer los niveles a cifras normales. 

## Qué deben comer los adolescentes

En cuanto a las **proteínas**, es mejor potenciarles el consumo de cereales y legumbres frente a la carne. Hay que limitar las grasas que acompaña a la carne y el embutido. En cambio es recomendable aumentar el consumo de pescado, al menos tres o cuatro veces por semana.

![cocido de la abuela](/images/posts/alimentacion-adolescentes/cocido.jpg)

Los **hidratos de carbono** se deben consumir mayoritariamente en forma compleja para aumentar el aporte de **fibra** con pan, pasta o arroz integrales. Deben huir de los zumos, y acostumbrarse a tomar las frutas frescas y enteras, además de aumentar el consumo de verduras, hortalizas, tubérculos y legumbres. 

![chica comiendo sandía](/images/posts/alimentacion-adolescentes/sandia.jpg)

Para aumentar las **vitaminas** se recomiendan las hortalizas y verduras, sobre todo las de hoja verde, los aceites vegetales o el huevo.

## Por qué mi hijo adolescente come mal
De repente algo más escapa a nuestro control, y nos sentimos perdidos como padres y madres al no saber exactamente qué come nuestro hijo, cuándo dónde. Pero por qué pasa esto?

## Cambio de gustos
Lo habréis observado casi seguro; de anoche a la mañana pasan de adorar un plato a odiarlo con la misma pasión, así, sin avisar siquiera, de manera que andamos locos con la lista de la compra sin saber si acertaremos esta vez.
Además, alimentos que les encantaban de niños ya no los soportan.

## Cambio de hábitos
Ahora pasan mucho más tiempo solos en su cuarto estudiando, escuchando música, jugando a videojuegos. Las actividades en solitario o con amigos han pasado a ocupar casi todo su tiempo. Así que no es de extrañar que aprovechen también para tomar alimentos más fáciles de tomar y más atractivos, como snaks, chuches, bebidas azucaradas, refrescos...

## Más autonomía
A la vez, son más autónomos, manejan algún dinero para transporte y almuerzo, se mueven solos por la ciudad para ir al Instituto, lo que les da acceso a una amplia gama de alimentos, que deberán elegir, cosa que hasta la fecha hacíamos nosotros.
 
## El adolescente es impaciente por naturaleza
Esto les  hace ser fácil de la comida rápida cuando tienen hambre y no tienen paciencia para prepararse algo un poco más elaborado pero más saludable.

![chica comiendo hamburguesa](/images/posts/alimentacion-adolescentes/hamburguesa.jpg)


## Disparidad de horarios
La falta conciliación laboral hace que la mayoría de chicos y chicas tengan que comer sin compañía de sus padres cuando llegan del Instituto muertos de hambre, seguramente con algún problemilla rondándoles la cabeza, y con la impaciencia de sentarse un rato a descansar y desconectar de las clases antes de ponerse a estudiar.
En estos casos, lo fácil es comer cualquier cosa y a otra cosa, mariposa.
Os hablaba de la situación de muchos [adolescentes que pasan lastrados solos en casa en este post](https://madrescabreadas.com/2020/01/20/solos-en-casa/)


## Alternativas a la mala alimentación de los adolescentes

De nuevo nos toca  echarle imaginación al asunto y ponernos en lugar de nuestros hijos para ponerles las cosas fáciles para que les apetezca comer bien. No olvidemos de que están en una edad muy buena para fomentarles hábitos de alimentación saludables que se conviertan en duraderos e invertir en futuros adultos sanos. Ya no vale el "son lentejas, las tomas o las dejas" de antes, sino que ahora debemos conseguir

## Recetas fáciles saludables en microondas
Como alternativa a acomida rápida, mostrémosles otro tipo de comida rápida, pero saludable, cocinada en el microondas, que la pueden elaborar ellos mismos de forma independiente porque no entraña riesgos, y con ingredientes que les gusten dentro de los más aconsejables.

![vegetales para kebab](/images/posts/alimentacion-adolescentes/vegetales.jpg)

## Haz la lista de la compra con ellos
Déjales que aporten ideas. Puedes mandarles un borrador por whats app y que la completen a su gusto. Después puedes sugerir algún cambio para que se ajuste más a una dieta equilibrada razonándoles el por qué, y dejarles algún capricho.
Esto hará que poco a poco se vaya reconduciendo la cosa.

## Elabora el menú semanal con ellos
Esto a mime da super buen resultado porque, sitien es verdad que en algo tengo que ceder, luego acogen mucho mejor determinados platos que no sondeo todo de su agrado porque los han aceptado de antemano. 

![ensalada con garbanzos y aguacate](/images/posts/alimentacion-adolescentes/ensalada.jpg)

## Déjalos cocinar con Tik Tok
Sí, ya lo sé... terminamos antes si lo hacemos nosotras, y además, no ensuciamos tanto. La tentación de no dejarles pisar la cocina es grande, pero tenemos que vencerla y dejarles hacer sus pinitos en la cocina. Es cierto que quizá ya no les guste tanto que cocinar con nosotros, sino más bien solos, incluso grabar las recetas para subirlas a las redes sociales. 
Pues aprovecha eso y sugiérele platos saludables , o como ellos preferirán llamarlos: "Healthy".
La App de moda Tik Tok puede ser tu aliada; busca recetas que puedas recomendárselas y compártelas con ellos. Quizá se animen hacerlas!

## Da ejemplo
Ni qué decir tiene que éste ese punto más importante porque si tus hijos os han visto desde que eran pequeños comer correctamente a vosotros siempre tendrán esa referencia que hará que tengan clarísimo lo que es saludable y lo que no lo es. Cierto que lo harán mal muchas veces, pero lo sabrán, y siempre tenderán a volver al redil si la familia ha tenido unos buenos hábitos alimenticios desde la infancia.






- *Photo by Anna Pelzer on Unsplash ensalada*
- *Photo by Dan Gold on Unsplash vegetales*
- *Photo by Angelos Michalopoulos on Unsplash hamburguesa*
- *Photo by Peter Secan on Unsplash perrito*
- *Photo by Caju Gomes on Unsplash sandía*