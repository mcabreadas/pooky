---
date: '2020-08-09T18:19:29+02:00'
image: /images/posts/peligro-ruidos/audifonos.jpg
layout: post
tags:
- salud
- crianza
title: Los riesgos de los audifonos en nuestros hijos
---

Cada vez vemos a niños más pequeños con auriculares o audífonos viendo videos en el móvil o tablet o jugando a videojuegos. Es curioso que a veces somos los mismos padres quienes se los facilitamos para que no nos moleste el sonido mientras trabajamos por ejemplo; yo misma a veces lo hago por necesidad, y es comprensible que en ocasiones no tengamos más remedio que recurrir a estos artilugios, pero quiero llamar la atención sobre los peligros que entraña para la salud auditiva de nuestros hijos abusar de ellos, sobre todo si el volumen es demasiado alto o el tiempo de uso es elevado, de hecho cada vez son más frecuentes los pequeños que sufren algún tipo de hipoacusia a edades más tempranas, incluso los que llevan algún tipo de [implante auditivo](https://escucharahoraysiempre.com/blog2/como-es-la-cirugia-de-un-implante-auditivo-de-conduccion-osea/).


## Riesgos de usar audífonos en niños

La cosa es tan seria que la OMS ha advertido que en menos de tres décadas el 10 por ciento de la población mundial (cerca de 900 millones) tendrán una discapacidad auditiva que hubiera podido prevenirse, y además ha advertido concretamente de los peligros del uso de audífonos ya que son usados de manera permanente y sin ningún tipo de precaución al menos en el 40 por ciento de los jóvenes. 

De hecho escuchar música con audífonos insertados en el canal auditivo más de cuatro minutos seguidos, o sea, durante una canción aproximadamente, a un volumen alto y de manera permanente puede provocar daños severos en las estructuras que transmiten el sonido, así como afectaciones en la estructura del tímpano. También puede dañar las células ciliadas, que son las que transmiten la audición hacia el cerebro y alterar toda la fisiología auditiva, lo que se manifiesta con ruidos tipo zumbidos o sensación de oído tapado.

Nuestra generación también ha sufrido mucho por los altos ruidos, ya que en nuestros tiempos mozos las discotecas y bares no cuidaban los decibelios a los que nos exponían, y más de una vez llegamos a casa con el típico pitido en los oídos, cosa que a más de uno nos ha dejado alguna que otra secuela.

![](/images/posts/peligro-ruidos/nina.jpg)

## Consejos para el uso de auriculares en niños

Como el nivel seguro de intensidad debe ser menor de 80 decibeles y los audífonos amplifican el sonido por encima de ese nivel, el riesgo de daño se incrementa, especialmente, cuando se introducen en el canal. El tiempo máximo de exposición no debería superar las 40 horas por semana a intensidades bajas. Si llegan a los 100 la exposición semanal no debe ser superior a 15 minutos

Como os contaba en este post sobre el [uso de auriculares en adolescentes](https://madrescabreadas.com/2019/12/19/auriculares/), debemos seguir la regla 60/60 para un uso de cualquier audífono o auricular.

Esta regla consiste en que el volumen no debe superar el 60% del volumen máximo, y la exposición no deberá superar 1 hora al día. Siendo necesarios descansos, caso de ser necesario usarlos durante más tiempo, y lo ideal sería no superar los 65 decibelios, aunque esto resulta algo utópico en ciudades como Barcelona, en las que el ruido suele rondar los 70 decibelios.

Un método sencillo y eficaz es preguntar al de al lado si escucha la música, y si es así, tendré que bajar el volumen hasta que se deje de escuchar desde fuera.



## Cómo detectar que mi hijo no oye bien

- En una conversación habla muy fuerte

- No entiende lo que le dicen

- Con frecuencia tenemos que llamarlo varias veces para que responda

- Siempre pone el volumen de la tele más fuerte de lo normal

- Responde “¿qué?” o “¿cómo?” con demasiada frecuencia.

- Si gira la cabeza para escuchar mejor.

- El bajo rendimiento escolar puede ser un indicativo

![](/images/posts/peligro-ruidos/colegio.jpg)

No nos despistemos con la salud auditiva de nuestros niños, y cuidémosla desde bien pequeños para prevenir problemas que en un futuro podrán no tener solución.

Si te ha servido este post, compártelo para que ayude a más familias! Esto me ayudará mucho y podré seguir escribiendo este blog.








*Photo by Behar Zenuni on Unsplash movil*

*Photo by Alireza Attari on Unsplash niño*

*Photo by Bermix Studio on Unsplash niña*

*Photo by Taylor Wilcox on Unsplash colegio*