---
layout: post
slug: solos-en-casa
title: Adolescentes solos en casa
date: 2020-01-20T12:19:29+02:00
image: /images/posts/solos-en-casa/p-adolescente-solo-en-casa.jpg
author: maria
tags:
  - adolescencia
---
La edad en la que se acaba el derecho a la reducción de jornada laboral por guarda legal es actualmente los 12 años de edad. Edad que, por otra parte, es la recomendada por los expertos para empezar a dejar algunas horas solos a los menores en casa sin compañía de ningún adulto responsable. Claro, que esto depende de la madurez del niño o la niña, de la crianza que se le haya dado, de su carácter o personalidad...

## ¿A los 12 años ya no nos necesitan?

Cuando mis hijos eran pequeños yo veía más que suficiente la edad de 12 años para que finalizara el derecho a reducción de jornada por cuidado de los hijos, pero ahora que ya han llegado a esa edad, y veo cómo nos necesitan, me doy cuenta de que no es suficiente.

El otro día me contaba una amiga que había encontrado tirada en la basura la comida que preparó por la noche a su hijo para que la tomara cuando llegara a casa del [Instituto](https://madrescabreadas.com/2018/09/17/consejos-primer-dia-instituto/). Ella trabaja todo el día, su hija pequeña come en el comedor del colegio, su marido tampoco come en casa, pero le preocupa la alimentación de su hijo de 14 años, así que cocina por las noches para él con el propósito de que lleve una dieta equilibrada. Pero el otro día se dio cuenta de que no está siendo así.

![chica comiendo hamburguesa](/images/posts/solos-en-casa/chica-comiendo-hamburguesa.jpg)

A mi amiga también le preocupa que su hijo haya suspendido casi todas las asignaturas cuando antes sacaba todo notables y sobresalientes. Sabe que es señal de que algo no va bien, incluso la han llamado sus profesores para una reunión, pero él no le cuenta nada y sólo vive pendiente del móvil o los [videojuegos](https://madrescabreadas.com/2019/10/22/el-fortnite-de-madre-a-madre/) y poco o nada se comunica con sus padres.

## El sentimiento de culpa de los padres

Ella sabe que si pudiera estar en casa todo iría mejor. Y lo sabe porque hace unos años pasaba las tardes con sus hijos y podía estar más pendiente de ellos. 

Mi amiga se siente mal. Vive angustiada pensando que no lo está haciendo bien, pero también tiene claro que no tiene otra opción. Además, su familia vive fuera, y no tiene ayuda externa para nada.

## Por si quieren hablar

Hablando de este tema con otra amiga me contó que recuerda con angustia el momento en que tuvo una mala experiencia en clase, y cuando llegó a casa deseando contarlo no había nadie. Apenas estuvo un par de horas sola, pero se le hicieron eternas, y hasta que no llegó su madre y pudo desahogarse con ella lo pasó realmente mal.

## Cada vez nos necesitan más

No sé vosotras, pero yo pensaba que cuando mis hijos fuesen mayores cada vez me iban a necesitar menos porque serían más autónomos en las tareas básicas como comer, vestirse, ducharse, lavarse los dientes, recoger sus cosas, hacer los deberes... Y tenía razón en una cosa aunque en la otra me equivocaba; es cierto que en las tareas básicas ya no me necesitan porque gracias a Dios ya las dominan, pero me siguen necesitando para otras cosas que si bien no requieren estar tan encima de ellos, son tan importantes y cruciales para du desarrollo personal y para su bienestar, que a día de hoy me preocupan más que el hecho de que un día no se laven los dientes o dejen los trastos por en medio (quién me lo iba a decir)

## Cerca pero no encima

Quiero decir que ahora, en la pre y adolescencia, viene lo realmente preocupante de la crianza y educación de nuestros hijos. Ahora es cuando nos la jugamos de verdad en una etapa en la que a pesar de que no nos permiten estar pegados a pespunte a ellos como antes, y eso debemos respetarlo, debemos seguirles la pista y desarrollar nuestro sentido arácnido de madres para intuir por dónde van sin hacernos pesadas ni aún cuando nos cierren la puerta de su cuarto en las narices.

![grupo de adolescentes por la calle](/images/posts/solos-en-casa/grupo-adolescentes.jpg)
	
Creo que nuestros adolescentes necesitan saber que estamos ahí y que pueden acudir a nosotras cuando lo necesiten, aunque sientan la necesidad de [tener su propio espacio](https://madrescabreadas.com/2019/04/22/dormitorios-adolescentes-ikea/), y prefieran pasar una tarde con las amigas en vez de haciendo galletas de avena con su madre.

## Tiempo en cantidad

Una vez más este blog defiende que el mejor regalo que podemos hacer nuestros hijos es nuestro tiempo y nuestra atención. Y no me refiero al tiempo de calidad, sino a la cantidad, sí señora, necesitan cantidad, aunque ni ellos mismos se den cuenta, y aunque ni nos saluden cuando entran en casa. 

Somos su faro cuando llegan abrumados del Instituto por todas las cosas nuevas que están experimentando y porque la vida de repente se les ha vuelto un lugar hostil, así, de repente, sin saber por qué. Porque se están convirtiendo en adultos poco a poco, y eso es duro. 
	
¿No te acuerdas?
	
Yo recuerdo con cariño que incluso cuando ya era mayor antes de independizarme y regresaba a casa y veía desde la calle la luz de la salita de mis abuelos encendida, la sensación de que había alguien esperándome y, aunque me metiera en mi habitación era tremendamente reconfortante saber que no estaba sola, que siempre estaban ahí, como un faro.

![faro en la noche](/images/posts/solos-en-casa/faro.jpg)

## ¿Cómo puedes ayudar a tu adolescente para que no se sienta solo?

**Los hijos deben ser cada vez mas autonomos**, ese es un hecho. Así que, aunque nos duela, debemos ir “soltando las riendas”.

Es cierto que al inicio, es probable que cometa imprudencias. Por ejemplo, coma comida basura, juegue videojuegos hasta tarde, etc. Debemos ser razonables. Nuestro trabajo como madres es **conducirlo hacia una vida adulta responsable.**

Claro, el joven puede sentirse solo al llegar a casa y no encontrar a nadie que lo espere, ¿qué puedes hacer? A continuación, te presentaré algunas sugerencias que me han sido muy útiles.

### Sé razonable y empática

Los adolescentes no son niños, pero tampoco son adultos, así que van a tomar decisiones que lamentarán, serán imprudentes, impulsivos, desordenados, etc.

Algunos padres creen que la etapa más difícil es cuando el hijo era un bebé, pero en realidad la adolescencia es el mayor reto para cualquier padre.

**Debemos ser empáticas y recordar lo que sentíamos a su edad.** Es difícil crecer y abandonar la seguridad de la infancia.

Tu hijo puede experimentar sentimientos de soledad, lo que puede llevarlo a estar irritable e incluso grosero contigo.

**Mantén la calma en estas situaciones** y aplica disciplina positiva de forma razonable.

El **libro “Convivir con un adolescente** (Guías para padres y madres)” del autor Elías Argüello Alonso; licenciado en Filosofía y Letras, maestro de adolescentes, me ha sido de gran ayuda.

Este libro te da ánimo. Tiene un lenguaje fácil y ameno de leer que explica los miedos y retos de esta etapa emocionante.

![Convivir con un adolescente ](/images/uploads/convivir-con-un-adolescente-.jpg "Convivir con un adolescente ")

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Convivir-adolescente-Gu%C3%ADas-Padres-Madres/dp/8436831837/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2LSN1UGMAZDN2&keywords=madre+de+adolescentes&qid=1672748411&sprefix=madre+de+adolescente%2Caps%2C397&sr=8-1&madrescabread-21) (enlace afiliado)

### Mantén la comunicación

Los adolescentes se vuelven inseguros, por lo que generalmente no suelen hablar como cuando eran unos peques. Pero, puedes fomentar la comunicación al ser accesible y abierto.

Este [diccionario de palabras que usan los adolescentes en la actualidad](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) te puede ayudar a conectar con tu hijo.

Se que al llegar del trabajo estarás muy cansada, pero si te muestras cariñosa y atenta, es probable que tu hijo se sienta con ánimos de contarte su día. Eso sí, no presiones para que te hable, más bien, procura ser comunicativa y afectiva.

Esta vía de comunicación debes empezar a **fomentarla desde la preadolescencia.**

El libro **“Diario madre - hija”** de Ema de Marco, es un manual que fomenta la comunicación con las niñas a partir de los 9 años. Su objetivo es contribuir a que las adolescentes puedan expresar sus inquietudes, miedos, anhelos y emociones.

![DIARIO MADRE HIJA](/images/uploads/diario-madre-hija.jpg "DIARIO MADRE HIJA")

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/DIARIO-MADRE-COMUNICACION-CONFIANZA-VINCULO/dp/B09WPVXDMV/ref=sr_1_15?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2LSN1UGMAZDN2&keywords=madre+de+adolescentes&qid=1672748411&sprefix=madre+de+adolescente%2Caps%2C397&sr=8-15&madrescabread-21) (enlace afiliado)

### Aprende adaptarte a la era actual

Los adolescentes experimentan los mismos miedos, pero no se enfrentan a los mismos desafíos. Por ejemplo, cuando eras adolescente había otros peligros y distracciones.

**Debes adaptarte a la sociedad moderna a fin de poder orientar a tus hijos.** Por ejemplo, ¿sabías que 3 de cada 10 menores entre 11 y 16 años intercambia contenido erótico por el móvil?

Algo que me ayudó muchísimo a enfrentarme a los desafíos modernos fueron los cursos de la ***[Escuela Lemon](https://escuelalemon.com/)***, allí pude conseguir consejos para una comunicación afectiva con adolescentes, padres en la era digital, educar sin gritos, entre muchos otros cursos.

[![](/images/uploads/escuela-lemon-2.jpg)](https://escuelalemon.com/)

En conclusión, tus hijos necesitan atención a pesar de que ya sean adolescentes. Aunque tengan la capacidad de quedarse solos en casa, necesitan guía para ser adultos responsables. ¡No te desanimes, puedes lograrlo!