---
author: maria
date: '2021-02-26T10:10:29+02:00'
image: /images/posts/enfermedades-raras/p-manos-colores.jpg
layout: post
tags:
- salud
- invitado
- crianza
title: Día de las enfermedades raras
---

El 28 de febrero se celebra el día de las enfermedades raras y he querido indagar un poco más sobre este tipo de patologías por qué se las considera raras y por qué no hay una cura para ellas. De este modo pretendo aportar mi granito de arena para dar visibilidad a este tema, sobre todo a las enfermedades raras en niños, a las familias que lo sufren y apoyar a las asociaciones que las ayudan y entidades que investigan posibles tratamientos, ya que existe una larga lista de enfermedades raras neurológicas, enfermedades raras de la piel... y un largo etcétera.

Para ello he hablado con la Neuropediatra ya jubilada del Hospital Universitario Virgen de la Arrixaca (Murcia), la Dra.  Rosario Domingo, bien conocedora del tema y participante en el [Plan Integral de Enfermedades Raras de la Región de Murcia 2017-2020 (PIER)](http://www.murciasalud.es/pagina.php?id=407519&idsec=1084), que ha tenido la amabilidad de contestar a mis preguntas para poder contaros lo que todos debemos conocer de estas enfermedades.


### Por qué se llaman enfermedades raras?

El término “enfermedades raras” se aplica a las poco frecuentes, también llamadas minoritarias. Las Enfermedades Raras (ER), según la definición de la Unión Europea, son aquellas patologías que tienen una prevalencia menor de cinco casos por 10.000 habitantes (menos de 1 de cada 2.000 personas). Se trata de un conjunto amplio, heterogéneo: pueden afectar a cualquier órgano, a cualquier edad. La mayoría son de origen genético (80%) y muchas se presentan desde la infancia. 

![tortuga metida en caparazón de frente](/images/posts/enfermedades-raras/zapatos.jpg)

### ¿Cuántas personas en el mundo aproximadamente padecen enfermedades raras?

Entre el 3,5 y el 5,9% de la población mundial, más o menos, estaría afectada por estas enfermedades, o sea más de 3 millones de españoles, 30 millones de europeos, aproximadamente.

### ¿Existe tratamiento para las enfermedades raras?

Como son raras son menos conocidas y hay menos investigación que en las enfermedades comunes, eso afecta también a la investigación en tratamiento. Muy pocas tienen tratamiento curativo, en la mayoría lo que se pueden tratan son los síntomas. 

### Investigación de las enfermedades raras

Aunque ha mejorado, la investigación en EERR en los últimos años, sigue siendo insuficiente. La investigación una por una de las EERR tiene como beneficiarios a pocos pacientes y, como resultado, la industria farmacéutica está poco interesada en inversiones en EERR en general. Los avances en conocimientos en el campo de la genética, así como en tecnología, han propiciado el progreso más rápido de los últimos años, pero todavía es francamente insuficiente. 

![tortuga metida en caparazón de frente](/images/posts/enfermedades-raras/laboratorio.jpg)

Es obvio, como hemos visto en la pandemia COVID-19, que cuando se ponen muchos recursos en la investigación y cooperación, se obtienen resultados rápido (vacunas en este caso). Es el caso inverso a las EERR. 



## ¿Qué podemos hacer para apoyar en la investigación de estas enfermedades raras en España y a las personas que las padecen? 

### Cómo podemos hacer una donación
Podemos apoyar a los grupos de investigación. Esto puede hacerse directamente con donaciones a los centros específicos de investigación o a las asociaciones de las distintas enfermedades o a FEDER.

Por ejemplo, el [Instituto Murciano de Investigación Biosanitaria (IMIB)](https://www.imib.es/portal/instituto/colabora.jsf), en su página web tiene enlace para donaciones para la investigación, también [FEDER tiene enlace para donaciones](https://enfermedades-raras.org/index.php/ayudanos) y también las asociaciones de las distintas enfermedades si queremos colaborar con alguna enfermedad en concreto. 

En Murcia tenemos muchas asociaciones que funcionan muy bien como [DGenes](https://www.dgenes.es) (enfermedades raras en general), [AELIP](http://www.aelip.es) (lipodistrofias), [Todo Corazón ](http://www.todocorazondemurcia.com)(cardiopatías congénitas) y muchas otras. 

![tortuga metida en caparazón de frente](/images/posts/enfermedades-raras/corazon.jpg)

A nivel nacional tenemos [Fundame](https://www.fundame.net) (Atrofia Muscular Espinal, por ejemplo), [Mucopolisacaridosis](https://www.mpsesp.org/portal1/a_index.asp), etc.

Si queremos colaborar siempre es preferible hacerlas a entidades, instituciones, que sean oficiales y cuyos fines estén claramente establecidos porque así sabremos a dónde va nuestro dinero y se evitan fraudes (como ha sucedido a veces cuando solicitan dinero sólo para un paciente). 

### Voluntariado

En cuanto a colaboraciones no monetarias, las asociaciones muchas veces tienen voluntarios, se puede participar en sus actividades etc. 

![tortuga metida en caparazón de frente](/images/posts/enfermedades-raras/manos.jpg)

## ¿Cómo se explica a unos padres que su hijo padece una enfermedad rara? 

No hay ninguna pauta general. Informar a los padres de que su hijo tiene cualquier enfermedad ya es difícil, informarles de que la que tiene no tiene curación y a veces tampoco tratamiento, es más difícil todavía. Hay que empatizar, comprender la situación compleja de los padres y ganarse su confianza con información veraz y entrega. Hay que decir siempre la verdad pero a veces hay que hacerlo de forma gradual según se va asimilando la información previa. 

## ¿Las enfermedades raras tienen cura?

Como se deduce de los apartados anteriores, casi nunca tienen tratamiento de curación total pero hay tratamientos. En unos casos hay tratamientos específicos aunque no curen la enfermedad la mejoran ( por ejemplo, en algunas enfermedades por errores congénitos del metabolismo se pueden administrar las enzimas o sustancias en que tienen deficiencia). En todas hay tratamientos sintomáticos que no curan la enfermedad pero mitigan sus síntomas. Los tratamientos sintomáticos son similares a los que se emplean en otras enfermedades, por ejemplo: Atención Temprana, fisioterapia, cirugías cuando son precisas, etc. etc. 

## ¿Cuáles son las enfermedades raras más comunes?

### Síndrome X-frágil. 

Es la causa genética más frecuente de discapacidad intelectual en varones (1/2.500 RN varones), la anomalía genética que lo provoca está localizada en el cromosoma X y por eso los hombres (por tener sólo un cromosoma X) se afectan con mayor frecuencia. No tiene tratamiento específico pero el diagnóstico precoz permite hacer asesoramiento familiar e iniciar terapias para mejorar los síntomas. 

### Atrofia muscular espinal. 

Se trata de una enfermedad que se hereda de forma autosómica recesiva, es decir que para tener un niño afectado se necesita que los dos padres sean portadores de la anomalía genética. La tasa de portadores se calcula en 1/50 personas y la incidencia es 1/10.000 RN. La enfermedad causa degeneración de neuronas motoras del asta anterior de la médula espinal, hay presentaciones de diversa gravedad, desde bebés las más graves hasta presentaciones más leves las que se presentan pasada la primera infancia. Los síntomas son debilidad muscular progresiva, la inteligencia es normal. Es muy importante hacer el diagnóstico de forma precoz porque desde hace unos años existe tratamiento de tipo genético que facilita que el ARN del gen SMN2 produzca mejor proteína y se atenúen los síntomas. 

## Testimonio de una madre de una niña con una enfermedad rara

> Selena, una niña de 7 años, sufre una DELECION EN EL CROMOSOMA 8 ( falta de información genética en un cromosoma) de una gran escala, nada más y nada menos que de 12 megabases, afectando así a más de 80 genes .No supieron que decirnos, era el único caso que tenían.
> Selena fue creciendo y acudiendo a un montón de terapias, logopedas, psicólogos, fisioterapeutas, terapeutas ocupacionales ... empezó a andar a los 18 meses. A día de hoy, no habla, aunque tiene mucha intención comunicativa. Tiene una discapacidad intelectual grave y es muy muy nerviosa, no puede parar quieta y todo lo que ve se lo lleva a la boca.
> Pero lo que os puedo decir, es que es La Niña más feliz del mundo, y lo más importante de todo, es una niña sana!

Tatiana, su madre abrió un [perfil de Instagram llamado "El viaje de Selena"](https://www.instagram.com/elviajedeselena/) para, en sus palabras: 

> Me gustaría normalizar la discapacidad, que la gente no tenga miedo de acercarse, preguntar y conocer, parece que aún , hoy en día las personas tienen miedo de preguntar por la afección de tu hij@, pensando que te vas a molestar, y ... para nada! Si todo se pregunta con educación nosotr@s estamos encantados de dar a conocer , las personas necesitan saber para perder los miedos. 
> También quiero dar a conocer, conocer que hay “ diagnósticos de enfermedades raras” y lo pongo entre comillas , porque mi hija no está enferma, aunque su patología se denomine así. Ella es única, no hay nadie como ella, nadie en la que yo me pueda reflejar y ver un poco en cierto modo, como será el futuro de mi hija, aunque bueno, si no somos tontos, me lo puedo imaginar.
> Pido empatía por parte de la gente, de esto, nadie está exento, esto es una lotería, incluso como digo en muchas ocasiones, es más fácil que te caiga un rayo a que te nazca un niñ@ con estas patologías, aún así, puedo decir que hoy por hoy, nunca me he cruzado con nadie que nos haya hecho algún mal gesto o signo de desprecio y es algo que también hay que dejar claro.
> Cuando tienes un hij@ de estas características lo único que puedes hacer es mirar para el frente y  “ tirar pa lante “.

Hay miles de enfermedades raras, y son muy variadas, incluso dentro de una misma enfermedad tienen formas de presentación variada. Sufren con frecuencia retraso en el diagnóstico, a veces no encuentran profesionales que conozcan la enfermedad, casi nunca tienen tratamiento específico… En fin, sufren los problemas de tener una enfermedad con bastantes más inconvenientes que en una enfermedad común. 


*Photo by Sharon McCutcheon on Unsplash lápices*

*Photo portada by Tim Mossholder on Unsplash manos pintadas*

*Photo by Alysa Bajenaru on Unsplash zapatos*

*Photo by Clay Banks on Unsplash manos*

*Photo by Tim Marshall on Unsplash corazón*

*Photo by Louis Reed on Unsplash laboratorio*