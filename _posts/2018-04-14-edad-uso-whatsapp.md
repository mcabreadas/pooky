---
layout: post
slug: edad-uso-whatsapp
title: "Colaboración con Bebés Más: WhatsApp sube la edad de sus usuarios"
date: 2018-04-14T10:10:29+02:00
image: /images/posts/prensa/p-moviles.jpg
author: .
tags:
  - adolescencia
  - tecnologia
  - derechos
  - revistadeprensa
---
WhatsApp sube la edad mínima de uso de 13 a 16 años, pero la responsabilidad sigue siendo de los padres.

> "Realmente esta medida no aporta nada, salvo un intento de WhatsApp de eludir su responsabilidad sobre lo que pasa en su red cuando los usuarios son menores de 16 años, y ello justo antes de la entrada en vigor del Reglamento General de Protección de Datos a nivel europeo (RGPR), quizá para librarse de alguna multa que pudiera caer".
>
> "Aunque quizá le compense pagarla y seguir recopilando y usando datos de los usuarios de forma dudosa (no olvidemos que comparte dueño con Facebook e Instagram)".
>
> "No nos engañemos, esta medida no pretende proteger a nuestros adolescentes, sino a WhatsApp"
>
> <a href="https://www.bebesymas.com/educacion-infantil/whatsapp-sube-la-edad-minima-de-uso-de-13-a-16-anos-pero-la-responsabilidad-sigue-siendo-de-los-padres-que-dicen-los-expertos" class='c-btn c-btn--active c-btn--small'>Leer artículo completo en Bebés y Más</a>

Te dejo este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que te ayude a conectar menor con tus adolescentes.

Si te ha gustado comparte y suscríbete al blog para no perderte nada.