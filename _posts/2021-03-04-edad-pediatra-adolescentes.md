---
author: maria
date: '2021-03-04T09:10:29+02:00'
image: /images/posts/pediatra-adolescentes/p-pastillas.jpg
layout: post
tags:
- educacion
- adolescencia
- salud
- invitado
- crianza
title: Pediatras también para adolescentes
---

> Cuando nuestros hijos llegan a la adolescencia, una de las cuestiones que nos planteamos es cuándo un niño deja de ir al pediatra, cuál es el médico que atiende a los adolescentes, qué edades atiende un pediatra... porque lo que la lógica dicta es que al igual que hay un médico especializado en niños, debería haber otro especializado en adolescentes; Con toda la complejidad que entraña esa etapa no parece lógico que nuestros hijos a los 14 años de edad cambien de pediatra a médico de familia directamente.
> 
> Hoy merendamos con Mª Angustias Salmerón Ruiz. Pediatra de la Unidad de Adolescencia del Hospital Universitario La Paz y del Hospital Ruber Internacional de Madrid. Asesora de varios grupos de trabajo para el Ministerio de Industria, Sanidad e Interior sobre infancia, seguridad e Internet. Autora de diversos artículos y ponente en múltiples jornadas y congresos sobre las repercusiones de las TIC en la salud de niños y adolescentes.  
> Granadina, madre de dos hijas y autora del libro “Criar sin complejos” (Edaf, 2018). Fundadora de [www.mimamayanoespediatra.es](https://www.mimamayanoespediatra.es) dedicado a la crianza desde el nacimiento al adulto joven, al acompañamiento y la formación para padres y familias. 
    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8441438242&linkId=8f927920d67c6ebbe9781ea7be11d7c2&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os dejo con la Dra. Salmerón:

![foto libro pediatra adolescentes](/images/posts/pediatra-adolescentes/libro.JPG)

## ¿En qué forma cambió tu percepción de la pediatría al ser madre? 

La maternidad revolucionó mi vida. Además, transformó de modo muy significativo la forma de entender mi profesión. Los padres dejan en mis manos lo que más quieren. Es normal que tengan dudas, que se sientan angustiados. Necesitan alguien que les solucione el problema, por supuesto, pero va más allá. Buscan a alguien que esté dispuesto a escuchar, a dar toda la información y aclarar las dudas. Si la asistencia al paciente es importante, lo es igualmente la información, el qué y el cómo decimos las cosas influye mucho en el paciente y en la familia. 

Y si en la enfermedad es crucial, también lo es en la crianza. la lactancia materna, la alimentación complementaria, el contacto, la estimulación neurosensorial en el niño sano, el porteo, las rabietas, la educación en positivo, la sexualidad, o las conductas de riesgo… La OMS afirma que está demostrado que los grupos de crianza guiados por profesionales sanitarios formados mejora la salud tanto física, como mental y social en la infancia, la adolescencia a corto, medio y largo plazo. El acompañamiento a la crianza también mejora en los padres la percepción positiva de la crianza de los hijos Para mí, es poner mi granito de arena para cambiar el mundo. Así que una gran parte de mi vida profesional está destinada a talleres formativos dirigidos a familias.  

## ¿Crees que la pediatría se ocupa lo suficiente de la etapa adolescente? 

En general la adolescencia es la gran olvidada, en el sistema sanitario y en todo lo demás. Un apunte, unidades de medicina de la adolescencia en el Sistema Nacional de Salud español sólo existen dos: la del Hospital La Paz de la que formo parte y en el Hospital Niño Jesús. Luego hay alguna consulta por España que atiende a adolescentes, pero no exclusivamente. 

La OMS insta a los países a fomentar la atención integral a la adolescencia en espacios específicos para ellos. España aún está muy lejos de alcanzar ese objetivo. 

## ¿Hasta qué punto influye la crianza de la primera infancia en la adolescencia? 

La crianza entendida como la forma de cuidar, acompañar y educar a los hijos, tiene que ver con todo. El ser humano es un mamífero social, necesita vivir en manada. Creo que con la distancia física que nos impone la pandemia todos lo entendemos. El ser humano necesita en todas sus etapas sentir que “pertenece” que “importa” que tiene un significado para el entorno. Y no sólo eso, que es valioso en sí mismo y que tiene utilidad para el resto de la tribu. En todo el periodo de crianza, educación y cuidado de los hijos los preparamos para que se conozcan, que tengan un criterio propio, que se quieran, que sean autónomos, que tengan su propia identidad… Fijaros si esto es importante.  

![foto pediatra adolescentes](/images/posts/pediatra-adolescentes/auscultando.JPG)

## ¿Qué consejo darías a las familias que tienen hijos pequeños de cara a una buena relación con ellos en la adolescencia? 

No hay recetas mágicas y no soy quién para dar consejos. Pero si me preguntan qué hago yo lo puedo resumir en tres aspectos: 

1) Cuidarme y cuidarnos (padre y madre) para poder cuidar. Si nosotros no estamos bien, no nos sentimos bien con lo que somos, lo que hacemos, lo que vivimos, será difícil educar. Por eso es tan complicado.  

2) Acepto que cada día me equivoco. Intento no exigirme. 

3) Pensar en los objetivos a largo plazo. Imaginar qué caja de herramientas me gustaría que tuviesen mis hijas en el futuro y qué pueden necesitar para transitar por la adolescencia. Esto lo pienso en cada conflicto. Me ayuda a respirar cuando tengo ganas de gritar o salir corriendo.  

## ¿Qué recomendarías a las familias que tienen hijos en plena adolescencia con los que tienen una relación difícil? 

Pedir ayuda si creen que se les va la situación de las manos. Nunca es pronto o tarde, ese es el mejor momento para buscar apoyo. Si lo piensas, hazlo. Para eso estamos los profesionales. Y algo muy importante, que se pasa, ahora se alejan, pero volverán, que confíen. Que lean, se informen y formen sobre qué es la adolescencia. Las dos tareas fundamentales de la adolescencia es conseguir autonomía para ser un adulto competente y forjar su propia identidad. Para ello nos tienen que cuestionar como padres, tienen qué decidir qué les vale de lo que han aprendido y con qué necesitan romper para ser ellos mismos. Si vivimos la adolescencia de nuestros hijos como una oportunidad de aprendizaje como ser humano, de intentar escuchar más allá de lo que hacen, saldremos reforzados como padres y como personas.  


## ¿Qué podemos aprender en tus talleres?

Lo que a mí me hubiese gustado que me contaran cuando me sentí tan perdida cuando fui madre. Los talleres tienen una particularidad, cuando se hacen en directo, aunque sean on line y se permite a las familias que participen no sólo se da información, se comparten experiencias. Eso ayuda a crear tribu, a hacer grupo, a sentirte comprendido, a pertenecer como madre o padre. Para mí sería mucho más fácil grabarlos, pero no quiero, perdería esa parte que hace que cada taller sea único. Yo, tengo un temario similar, van evolucionado, pero cada taller es único. 

Llevo dando formación a familias más de siete años, ahora el reto fue pasar al formato “on line”, crear una nueva web. La pandemia obligó a acelerarlo todo porque era imposible hacerlo presencial y las familias seguían pidiéndome los talleres. Imparto formación en empresas, en AMPAS, en grupos de crianza y en los talleres de la web a todos aquél que se quiera embarcar en esta gran aventura. En dos meses, pasaron 70 familias, estoy feliz. 

La temática es diversa: primeros auxilios, sueño infantil, alimentación infantil, salud digital, adolescencia, sexualidad… 

![foto pediatra adolescentes](/images/posts/pediatra-adolescentes/angustias.JPG)

## ¿Qué servicios ofreces en tu web para ayudar a las familias? 

El blog para mi sigue siendo algo fundamental. Así empezó “mi mamá ya no es pediatra”. Para contarle al mundo todos mis descubrimientos al ser madre y pediatra. Además, están los talleres presenciales, la forma de pedir consulta presencial, la consulta on line y los talleres on line. En realidad, es mi ventana al mundo y la forma de llegar a las familias durante la pandemia. 

Muchas gracias por darme esta oportunidad. Me encantó esta entrevista y tu rinconcito en Internet, es mágico, enhorabuena!


> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)

Si te ha sido de ayuda, comparte!
Suscríbete al blog para no perderte nada.


*Photo portada by Diana Polekhina on Unsplash*