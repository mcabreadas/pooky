---
layout: post
slug: mantenerse-en-forma-tras-el-parto
title: Cuándo puedo empezar con el ejercicio tras el parto
date: 2022-02-22T12:25:19.561Z
image: /images/uploads/istockphoto-1280193745-612x612.jpg
author: faby
tags:
  - embarazo
  - postparto
  - salud
---
Mantenerse en forma después del parto es beneficioso para la salud. De hecho, hay ciertos **ejercicios fáciles de llevar a cabo**. Sin embargo, hay que ir poco a poco. 

Es cierto que ver a las celebridades con una hermosa figura días después de parir puede generar la falsa idea de que inmediatamente hay que hacer ejercicio. Pero recuerda que acabas de experimentar un cambio radical en tu cuerpo.

En esta entrada hablaremos de cuántos días de reposo debes esperar después del parto y por supuesto **cuándo empezar a ejercitarte para recuperar tu figura.**

## ¿Cuántos días de reposo después del parto?

**Los especialistas en salud recomiendan guardar reposo durante 10 días;** independientemente del tipo de parto.

Ahora bien, después de esos días, la mujer puede empezar a realizar actividades cotidianas en casa. Sin embargo, **aún no es el momento de ejercitarte.**

![Días de reposo después del parto](/images/uploads/istockphoto-517050098-612x612.jpg "Mujer descansando junto a su bebé")

La idea de este tipo de reposo es que permitas la recuperación del útero. Asimismo, contribuyas a que el organismo se recupere del embarazo y se adecúe a la fase materna (lactancia).

## ¿Cuánto tiempo debo esperar después del parto para hacer ejercicio?

El tiempo de espera para hacer ejercicio físico tras el parto dependerá de varios factores.

* **Parto vaginal.** Este tipo de parto generalmente no ocasiona complicaciones y la madre puede regresar a su rutina cotidiana en pocos días. Sin embargo, para empezar a ejercitarse para bajar peso es necesario dejar pasar la cuarentena o puerperio.
* **Parto por cesárea.** Este tipo de cirugía requiere de un tiempo prolongado para la recuperación y cicatrización de la incisión realizada. En este sentido, la mujer podrá realizar sus labores de manera progresiva. En general, se recomienda que espere alrededor de 2 meses.
* **Parto complicado.** Independientemente del tipo de parto si hubo complicaciones o riesgos en la salud de la madre, será necesario consultar con el médico el momento apropiado para iniciar rutinas de ejercicio tras el parto.

## ¿Qué ejercicio puedo hacer después del parto?

![Ejercicios después del parto](/images/uploads/istockphoto-1070026572-612x612.jpg "Mujer hace ejercicio mientras sostiene a su bebé")

Una vez pasado el tiempo de cicatrización y recuperación del útero, puedes empezar a practicar deporte. Pero hay que ir de forma gradual, así lo señalan los expertos.

* **Ejercicios de bajo impacto.** Puedes empezar haciendo algunas caminatas suaves durante al menos tres días a la semana. Luego, puedes aumentar la frecuencia.
* **Ejercicio de Kegel.** Este tipo de rutinas fortalecen el suelo pélvico (útero, la vejiga, intestino delgado y el recto, los cuales se han visto afectados en el embarazo). La *[Asociación Española de Fisioterapeutas](http://www.aefi.net/fisioterapiaysalud/fisioterapiapostparto.aspx)* recomienda empezar a realizar este ejercicio tan solo 48 horas después del parto. Consiste en apretar los músculos vaginales por 5 segundos y luego relajarlos. 
* **Yoga.** La postura de yoga del bebé feliz es ideal para fortalecer los músculos pélvicos. 

## ¿Por qué no se puede hacer ejercicio de gran impacto tras el parto?

Los ejercicios de gran impacto como correr, saltar, jugar baloncesto, etc., son muy buenos para adelgazar rápido, pero no son apropiados después de un parto.

Según *[Medwave Revista Biomédica](https://www.medwave.cl/link.cgi/Medwave/Revisiones/RevisionClinica/5336)* en su artículo “Efectos del embarazo y el parto sobre el piso pélvico” señala que el **suelo pélvico,** así como la musculatura abdominal se ve afectado por el embarazo.

Según ciertos estudios los **órganos internos tardan entre 4 y 6 meses en volver a su lugar**. De modo que, antes de que eso ocurra está contraindicado las prácticas deportivas de gran impacto.

Es importante destacar que, si no prestas atención a estas recomendaciones médicas pueden afectar tu salud, así como disminuir la calidad y cantidad de leche para tu bebé. 

La *[Revista Cubana de Medicina General Integral](http://scielo.sld.cu/scielo.php?script=sci_arttext&pid=S0864-21252017000200002)* indica que los **defectos del suelo pélvico generan impacto en la calidad de vida de la mujer.**

No querrás aumentar el desarrollo de esta anomalía al apresurarte a realizar deporte, ¿verdad?**Podrás realizar ejercicios de gran impacto después de 6 meses aproximadamente.**

## ¿Cómo recuperar la figura tras el parto?

![Recuperar la figura tras el parto](/images/uploads/istockphoto-905274104-612x612.jpg "Mujer se mide la cintura mientras carga a su hijo")

Para bajar esos kilitos extras necesitas ejercitarte, claro, debes seguir las recomendaciones médicas e ir poco a poco. Sin embargo, también puedes bajar el consumo de alimentos poco nutritivos.

* **Dieta.** Evita los alimentos grasos y adiciona a tu dieta comida nutritiva que aporte saciedad.
* **Ejercicio moderado.** El ejercicio en dosis apropiadas te ayudará a bajar de peso. Recuerda que recuperar la figura lleva su tiempo. Además, no tengas expectativas irreales. Recuerda que el cuerpo de la mujer cambia tras el embarazo. En este [libro de Pilar Rubio vienen ejercicios para realizar despues del parto](https://madrescabreadas.com/2016/06/08/libro-pilar-rubio/).
* La **lactancia materna** ayuda a rebajar la grasa acumulada durante el embarazo, ya qu el bebe la tomara con la leche que produces

En resumen, recuperar la figura o **mantenerse en forma tras el parto** **requiere de paciencia,** pero si eres perseverante y sigues las recomendaciones de los expertos en salud podrás ser una mamá atractiva y vanguardista.

*Photo Portada by FilippoBacci on Istockphoto*

*Photo by skynesher on Istockphoto*

*Photo by LSOphoto on Istockphoto*

*Photo by RossHelen on Istockphoto*