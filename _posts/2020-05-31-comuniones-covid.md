---
author: maria
date: 2020-05-31 16:19:29
image: /images/posts/comuniones-covid/p-capa.jpg
layout: post
slug: comuniones-covid
tags:
- familia
- moda
- comunion
title: Comuniones y Coronavirus
---

Muchas familias sois la que habéis tenido que posponer la comunión de vuestros hijos con motivo de la pandemia por el Covid-19 casi a las puertas de la celebración con todo preparado: restaurante reservado, [vestidos de comunión](https://quemono.org/vestidos-comunion/) encargados, decoración, fotógrafo, flores, mesa dulce y toda la parafernalia que cada a gusto de cada uno (en este post os cuento [cómo celebrar la Comunión sin arruinarse](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/)).

A nosotros no nos ha tocado suspender de milagro porque mi pequeño la hace en 2021, así que crucemos los dedos porque el futuro no es del todo cierto en cuanto a celebraciones se refiere.

## Comuniones 2020 privadas o pospuestas

Los arzobispados en general mantuvieron la posibilidad de hacer celebraciones religiosas en privado. En el caso de las comuniones, sólo con el comulgante, los padres y padrinos con la oportuna distancia de seguridad. 

Ya en fase 1 se permiten hacer comuniones de 5 niños con restricciones de aforo (en función del tamaño de la iglesia) para mantener la distancia de seguridad, y en fase 2, con un máximo de 50 personas en espacios cerrados, y según su aforo.

Aun así, lo que las familias queremos es celebrarlo con nuestros seres queridos. Además, imagina a estas alturas tener que "desinvitar" a la mitad de gente que habíamos invitado en un principio... Por lo tanto, esta solución no tuvo muy buena acogida y en general lo que los padres y madres estamos pidiendo a las parroquias es que se pospongan las comuniones para un momento en que se puedan celebrar sin apenas limitaciones, posponiéndose a septiembre u octubre.


## ¿Qué ha pasado con los vestidos de Comunión 2020 encargados?

El confinamiento nos pilló a principios de marzo, época en la que casi todos los vestidos de comunión estaban encargados, aunque muchos todavía no estaban fabricados (la confección de estos vestidos puede tardar hasta 3 semanas y se suelen entregar en marzo/abril). 

![chaqueta de comunión marrón](/images/posts/comuniones-covid/capa-terciopelo.jpg)

Muchas firmas lo que hicieron fue que pararon la producción de los encargos que no estaban fabricados todavía para esperar a fabricarlos en el futuro con las nuevas medidas de los niños, y los que ya se habían fabricado se arreglarán para ajustarlos a la nueva talla de cara a otoño.

![chaqueta de comunión marrón](/images/posts/comuniones-covid/bolero-terciopelo.jpg)

## Trajes de comunión 2020 para otoño

Las casas de vestidos de comunión han tenido que adaptarse alas nuevas circunstancias de esta pandemia, como ha sido el caso de otros sectores, para dar servicio a sus clientes y superar esta crisis lo mejor posible, así que firmas como [Quémono](https://quemono.org) han tenido sacar una serie de prendas de abrigo para adaptar los vestidos de comunión habitualmente de primavera, a épocas del año en las que hace más frío. 

![comunión bolero azul](/images/posts/comuniones-covid/bolero.jpg)

Prendas como boleros, capas, chaquetas, abriguitos van se van a hacer imprescindibles para las celebraciones otoñales. Para los niños la adaptación será más sencilla, porque por lo general llevan traje tradicional o de marinero, que es más abrigado, y se soluciona fácilmente cambiando el pantalón corto por el largo, pero los de las niñas suelen ser tejido más frescos, y de manga corta o sin manga, que necesitarán una prenda de abrigo para poder aprovecharlos.

![chaqueta de comunión marrón](/images/posts/comuniones-covid/chaqueta.jpg)

Caso de que no estén confeccionado aún nuestro vestido, quizá podamos elegir otro tipo de tela. Así pasaremos de tules a brocados, de mangas transparentes a mangas forradas, de trajes de lino a trajes de algodón... y, por supuesto poner medias a las niñas más o menos tupidas según el frío que haga y la zona de la geografía española donde hagan la comunión.

![chaqueta de comunión marrón](/images/posts/comuniones-covid/chaqueta-pelo.jpg)

Así que, visto lo visto, lo mejor para las comuniones 2021 es no hacer demasiados planes e ir viendo cómo se desarrollan los acontecimientos o bien elegir el vestido de una marca que ofrezca facilidades y que se adapte a las circunstancias que tengamos en el futuro  con el Covid 19.

De todo esto y más solemos hablar en el grupo de Facebook "[Organizando la Primera Comunión](https://www.facebook.com/groups/1377576085877092/)", al que estás invitada. Sólo tienes que entrar y solicitarme entrar. Te aceptaremos encantadas.

¡Si te ha gustado comparte para ayudar a más familias!

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).