---
author: maria
date: '2020-05-28T10:10:29+02:00'
image: /images/posts/prensa/p-auriculares.jpg
layout: post
tags:
- tecnologia
- revistadeprensa
title: 'Colaboración con Bebés Más: Puede el profesor grabar a sus alumnos en las
  clases virtuales'
---

La pandemia está cambiando la forma en que los alumnos se relacionan con los profesores debido a la implementación en muchos colegios de las clases on line. 

Muchos profesores piden videos de sus alumnos haciendo ejercicios de educación física. 

En este artículo analizamos la legalidad de esta medida y hasta que punto los alumnos tienen obligación de grabarse.

También, qué requisitos tiene que cumplir el centro para tratar las imágenes de los alumnos.

<a href="https://www.bebesymas.com/infancia/puede-profesor-grabar-a-sus-alumnos-clases-virtuales-examenes" class='c-btn c-btn--active c-btn--small'>Leer artículo completo en Bebés y Más</a>


Si te ha gustado comparte y suscríbete al blog para no perderte nada.