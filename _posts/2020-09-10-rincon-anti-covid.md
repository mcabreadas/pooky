---
date: '2020-09-10T17:19:29+02:00'
image: /images/posts/rincon-anti-covid/p-mascarillas.jpg
layout: post
tags:
- trucos
- crianza
- salud
title: Como montar un corner anti COVID en casa
---

Las familias hemos tenido que adaptarnos rápidamente a la nueva normalidad provocada por la crisis sanitaria del Coronavirus. Ya hablamos la semana pasada de [cómo afrontar  la vuelta al cole](https://madrescabreadas.com/2020/08/28/vuelta-cole-covid/) con todas las medidas de seguridad que ello conlleva en cuanto al uso obligatorio de mascarillas que sin duda va a provocar que en casa a partir de ahora no sólo encontraremos calcetines desparejados sueltos, piezas de construcción o mochilas por todas partes, sino que un nuevo elemento ha pasado a formar parte de nuestro paisaje hogareño: las mascarillas colgantes de los sitios más insospechados como marcos de cuadros, escaleras, espejos, pomos de armarios (incluso de cocina), picos de mesas, hasta en la esquina de la tele! Cualquier sitio parece bueno para soltar la molesta mascarilla en cuanto cruzamos la puerta de casa.

Éstos son los [materiales que he usado yo para montar nuestro córner anti COVID](https://www.amazon.es/shop/madrescabreadas?listId=3I89V1PH47142&ref=idea_share_inf) (enlace afiliado) en casa, por si os sirve de ayuda, aunque seguramente con cosas que tenéis por casa lo podáis poner.


## Por qué un rincón anti COVID en casa

Todas sabemos a estas alturas que el concepto anti COVID o COVID free no existe y que ningún lugar está libre de Coronavirus estrictamente hablando, pero lo que sí se puede conseguir es minimizar el riesgo lo máximo posible en la medida de nuestras posibilidades. Así, si montamos un rincón fijo en casa con todo lo necesario para protegernos del virus, seguramente nos ayuda toda la familia a cumplir las recomendaciones sanitarias mucho mejor.

Yo comprendo que todos llegamos a casa agobiados y deseando respirar una bocanada de aire sin ella, pero con un poquito más de esfuerzo se puede guardar correctamente y así lograríamos:

⁃ No contaminar la superficie que toca

⁃ No confundir las mascarillas

⁃ No tener que lavarlas o tirarlas antes de tiempo por no saber a quién pertenece

Por eso decidí hace tiempo organizar todo el tinglado de las mascarillas, gel hidroalcohólico, zapatos y demás en la entrada de casa, a ver si podía concentrarlo todo en un punto o al menos lograr contener el caos "covidero", ya que somos 5 de familia, y el lío era monumental.

![rincon covid free](/images/posts/rincon-anti-covid/rincon.jpg)

## Qué necesitas para montar un rincón anti COVID

No necesitas un gran espacio ni un mueble especial, de hecho, en mi caso mi entrada no es muy grande, pero tengo un mueble estrecho aunque alargado, en el que he colocado todo lo necesario par salir seguro a la calle y mantener la seguridad del casa cuando llegamos. Nuestros córner anti COVID consta de:

⁃ [Carpetas colgantes de cartón](https://amzn.to/32d0rOS) con [visores para etiquetas](https://amzn.to/2RdDAMQ) (enlace afiliado) para poner el nombre. Es necesario una por persona.

-[Soporte para carpetas colgantes](https://amzn.to/3heZwSo) (enlace afiliado).  

![](/images/posts/rincon-anti-covid/carpetas.jpg)

⁃ [Sobres de papel](https://www.amazon.es/gp/product/B000NM14QE/ref=as_li_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B000NM14QE&linkId=b899ab629745bf6085361c0bc7f641ec) con la inicial de cada uno y la fecha en la que se comienza a usar la mascarilla (por si sólo la vamos a usar un rato cada día). Estos sobres sirven para llevarlos a la calle y guardar la mascarilla si fuera necesario (mientras comemos, por ejemplo) (enlace afiliado) y también para guardarla en su correspondiente carpeta en casa si es que se ha usado poco tiempo y se puede repetir otro día antes de lavarla o tirarla. Estos sobres se tendrán que tirar cuando se cambie la mascarilla. 

-Una opción alternativa a los sobres es usar [bolsas de tela](https://amzn.to/3ic9uVP) (enlace afiliado), y lavarlas tras cada uso: podéis aprovechar si tenéis por casa de algunos pendientes o pulseras.

⁃ Bolsa de papel para depositar las mascarillas sucias higiénicas (de tela lavables) 

⁃ Bolsa con mascarillas quirúrgicas (las que suelen ser azules de usar y tirar) para las visitas que vengan a casa 

⁃ Caja con mascarillas de tela limpias listas para usar

⁃ Bote de gel hidroalcohólico para quien entre a casa 

-Termómetro para tomar la temperatura a los niños antes de ir al cole

![termómetro digital y mascarilla quirúrgica](/images/posts/rincon-anti-covid/termometro.jpg)
  
⁃ Caja grande de cartón para dejar los zapatos que vengan de la calle

⁃ Apartado para dejar llaves y enseres que se hayan manipulado en la calle

Aún así no quedaréis libres de encontraros mascarillas despistadas por casa, pero sin duda el caos se contendrá más en un sólo punto.

También os digo que es cuestión de tiempo que los niños (y adultos) nos acostumbremos a esta nueva dinámica, y la integremos en nuestras vidas, esperemos que no por mucho tiempo.

> Si os ha servido compartid para ayudar a otras familias. Vuestra difusión me ayuda a seguir escribiendo este blog!!




*Photo by Macau Photo Agency on Unsplash mascarillas quirúrgicas*

*Photo by Annie Spratt on Unsplash termómetro*

*Photo by Tai's Captures on Unsplash gel*