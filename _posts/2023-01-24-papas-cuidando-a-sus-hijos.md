---
layout: post
slug: papas-cuidando-a-sus-hijos
title: "Podcast con los ConPadres: Los hombres también crían"
date: 2023-01-24T12:13:48.567Z
image: /images/uploads/photo_2023-01-23-13.17.25.jpeg
author: .
tags:
  - podcast
---
> Ser padre se ha convertido en una identidad que te hace sentir orgulloso.

Hoy nos descabreamos con Sergio, Miguel y Nacho, padres y protagonistas del [programa de podcast ConPadres](https://open.spotify.com/show/5nWiCBmcvtryg7V77f30Qd), y nos descabreamos de verdad porque esta nueva generación de padres nos devuelve la fe en que los hombres pueden ser igual de buenos en la crianza de los hijos e hijas como las madres.

Son padres implicados, conscientes, con sentido del humor, uno de ellos en reducción de jornada, otro disfrutando de su baja de paternidad, de los que las madres también tenemos cosas que aprender.

Hablamos sobre:

## Que es ejercer la paternidad

Conciliación familiar y laboral siendo hombre

Reparto de las tareas del hogar

Embarazos

Partos

"Efecto picha"

¿Que cabrea a los conPadres?...

Diversión asegurada!

Dale a seguir para no perderte ningin episodio y valora el podcast con estrellitas para apoyar este proyecto.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6PI61UEZUu8fUtJwxY0QNs?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>