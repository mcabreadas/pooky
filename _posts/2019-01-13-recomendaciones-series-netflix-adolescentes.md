---
layout: post
slug: recomendaciones-series-netflix-adolescentes
title: Las 6 series de Netflix para preadolescentes que ven mis hijos
date: 2019-01-13 16:19:29
image: /images/posts/recomendaciones-series-netflix-adolescentes/p_recomendaciones-series-netflix-adolescentes.jpg
author: .
tumblr_url: https://madrescabreadas.com/post/181982665069/recomendaciones-series-netflix-adolescentes
tags:
  - adolescencia
  - series
---
No sé vosotras, pero en casa hace tiempo que dejamos de ver la televisión normal, es decir, la programación que emiten las distintas cadenas. Ni siquiera el telediario, que hemos sustituido por twitter, o Pasa palabra, que me encantaba. Antes de la existencia de la Televisión la carta y de seguir las recomendaciones de series de familiares y amigos, poníamos videos con dibujos previamente seleccionados a nuestros hijos, de manera que casi no saben lo que es un anuncio de publicidad, y bailan con la música del telediario si alguna vez la ven en casa de los abuelos.

Si no estás suscrito a alguna  plataforma de series on line y vives con adolescentes, no podrás resistirte por mucho tiempo, así que te recomiendo varias opciones:

\-Si soléis comprar cosas en Amazon os interesa Amazon Prime Video porque compagina ventaja en envíos y compras con series y películas on line.
Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de contenido original y es bastante bueno.
Además, es económico porque por 3,99€ al mes tienes derecho a:

* Envío 1 día GRATIS en dos millones de productos enviados por Amazon.
* Envío gratis con entrega garantizada en el mismo día del lanzamiento para miles de productos en preventa de cine, series TV y videojuegos entre otros.
* Twitch Prime: contenido adicional para videojuegos todos los meses, descuentos exclusivos en la suscripción Prime con la reserva de videojuegos y mucho más.
* Acceso Prioritario a las Ofertas flash de Amazon.es, 30 minutos antes de su inicio.
* Almacenamiento de fotos gratis e ilimitado en la plataforma Amazon Drive con Prime Fotos.
* Descuentos en una selección de pañales con Amazon Familia.

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21)

\-Oferta para universitarios. Si tienes universitarios en casa, te interesa Amazon Prime Student, además, ahora hay una prueba de 90 días! Con esta opción tienes todas las ventajas de Amazon Prime a mitad de precio; solo EUR 18,00/año. Eso, sí, esta oferta es sólo para estudiantes universitarios.

Puedes [darte de alta en Amazon Prime Student aquí](http://www.amazon.es/joinstudent?tag=ID_de_madrescabread-21)

Desde que lo tenemos en casa han ido descubriendo y desechando series y programas con nuestra supervisión, de manera que ellos solos están aprendiendo a ser selectivos con las cosas que ven. Por eso quería compartir con vosotras algunas de las series que más les han enganchado, y que nosotros hemos aprobado, porque he visto recomendaciones de series para pre y adolescentes, pero las hacen adultos, y eso a mí no me vale. Si son un poco más mayores, a partir de 14 años, podéis ver este post de [recomendaciones de series para adolescentes](https://madrescabreadas.com/2020/06/18/recomendaciones-series-adolescentes-dos/) que os engancharán a vosotros también, y si tenéis hijos detonas las edades, también podéis ver series toda la familia sin necesidad de ver siempre los típicos dibujos animados: en este post encontraréis [recomendaciones de series para toda la familia](https://madrescabreadas.com/2019/08/22/series-familias/). 

Ésta es la auténtica lista de series para pre adolescentes que mis hijos recomendarían a sus amigos:

## Merlin

Recomendada para mayores de 13 años, pero perfectamente entendible a partir de 10, esta nueva versión de la leyenda del Rey Arturo adaptada al público familiar, nos presenta a un joven Merlin recién llegado a Camelot y con muchas ganas de aprender y perfeccionar su magia con la ayuda de su tío.

Tiene todos los ingredientes que pueden atraer a nuestros tener: el gran sentido de la justicia del pequeño mago, su inconformismo y rebeldía, el descubrir el límite entre el bien y el mal, dragones, brujas, espadas, lealtad y traiciones y, cómo no, inocentes romances les engancharán desde el primer capítulo.
![merlin](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla7ujeBhl1qfhdaz_540.jpg)

## Pequeñas grandes mentes

Se trata de un programa científico muy bien planteado y enfocado para despertar la curiosidad y captar la atención de los jóvenes más curiosos. Protagonizado por chicos y chicas de esa misma edad, suelen hablar de gérmenes, emociones y redes sociales planteando una hipótesis o pregunta para que los espectadores se cuestionen algo, dejando un tiempo para la reflexión. Después dan el resultado y la explicación científica correspondiente de forma cercana. Otras veces hacen experimentos muy interesantes.

Uno de los capítulos que más me gustaron fue el de las redes sociales, y cómo relativizarlas dando más importancia a la vida real. Muy recomendable!!
![brain child](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla7yeV95T1qfhdaz_540.jpg)

## Teen Titans Go

He de confesar que al principio me tirópara atrás por el humor absurdo, como absurdas me parecieron sus canciones, sus personajes... pero como no paraban de verla escasa, al final le cogí el gusto y me convertí en su mejor fan. Ahora me parto, me  encanta la personalidad de sus protagonistas, así como la relación que hay entre ellos, y me parto con las canciones, que no paro de tararear.

Parece que no, pero fomentan valores importantes padrenuestros hijos como la lealtad, el amor por la lectura, la no discriminación... pero si le pregunta a mi hijo dirá que no fomenta ninguno, que simplemente es de risa.
![teen titans go](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla816zVtR1qfhdaz_540.jpg)

## Sirenas de maco

Tres sirenas cambian su destino para ayudar  y proteger el secreto de un chico que adquiere poderes mágicos y cola de tritón de forma inesperada, para lo que se matriculan en su Instituto y aprenden las costumbres de la tierra.

Estos ingredientes son clave para el éxito entre el público adolescente.
![](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla84rR4Mm1qfhdaz_540.jpg)

## Padres forzosos

Llamadme nostálgica, pero a pesar del salto generacional y de las referencias a música y pelis de nuestra infancia (que no está mal que nuestros hijos conozcan por cultura general, y por aproximarse un poco a cómo vivíamos nosotros de pequeños), padres forzosos enganchó en casa desde el minuto cero. Además, viene genial para verla en familia y comentar los conflictos que se plantean porque muchos son universales y seguro que se os han planteado o se os plantearán. 

Por si no la visteis en vuestra más tierna edad, se trata de una familia inusual a la par que entrañable donde un padre viudo se ve en la tesitura de criar a 3 niñas, paro lo que le ayudan su mejor amigo, que es como un niño más, y el tío guay y buenorro de las criaturas.
![padres forzosos](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla8767rM11qfhdaz_540.jpg)

## Project MC2

Ideal para fomentar el espíritu científico en las niñas.Sus protagonistas son las más listas del Instituto, y además espías. Usan la ciencia la tecnología para cazar a los malos.

Ya os hable sobre las [muñecas MC2](https://madrescabreadas.com/post/152061596027/munecas-project-mc2) que Famosa sacó de esta serie, muy recomendables!!
![project mc2](/images/posts/recomendaciones-series-netflix-adolescentes/tumblr_inline_pla8boBM1T1qfhdaz_540.jpg)

Y ahora, un buen bol de palomitas, y a disfrutar en familia!