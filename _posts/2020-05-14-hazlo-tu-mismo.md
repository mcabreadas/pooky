---
date: '2020-05-14T12:19:29+02:00'
image: /images/posts/hazlo-tu-mismo/p-utensilios.jpg
layout: post
tags:
- familia
title: Mejor hazlo tu mismo
---

Una de las cosas que estamos descubriendo últimamente es
que somos capaces de ser más autosuficientes de lo que pensábamos. El “quédate en casa” nos ha llevado a salir poco, a comprar lo imprescindible y a tomarle el gusto a fabricar nuestro propio pan, cortarnos nosotros mismos el pelo, aplicarnos el tinte y a confeccionar nuestras propias mascarillas infantiles, en este caso debido también a la escasez que hemos sufrido y a que las que venden suelen ser demasiado grandes para los niños y algo antiestéticas, por lo que los más artistas han tirado de retales de casa y han dado rienda suelta a creaciones como ésta de estilo étnico de [@beadelajungla](https://www.instagram.com/bea_de_la_jungla/)

![pintura-barriga-embarazada-jirafas](/images/posts/hazlo-tu-mismo/serigrafia.jpg)

Quizá por ello está aumentado la compra de todo tipo de artilugios DIY porque si un [kit de serigrafía](https://es.cplfabbrika.com/serigrafiaes/kits-talleres.html) para camisetas o de impresión de mascarillas te puede permitir fabricar tú mismo para toda la familia tal vez merezca la pena ponerse manos a la obra y lograr un resultado totalmente personalizado.

Los manitas juegan con ventaja en estos tiempos! Por eso creo que nos estamos volviendo todos un poco “makers” y autosuficientes, como si nos hiciese sentir más
seguros el no depender de nadie para nuestra subsistencia volviendo un poco al ser del hombre primitivo, (cómo sigamos así sólo nos va a faltar hacer fuego y salir a cazar).

![mecedora de bebé de madera hecha a mano](/images/posts/hazlo-tu-mismo/mecedora.jpg)

Mi admiración por [@jmgj79](https://www.instagram.com/jmgj79/), quien en la soledad de su confinamiento no ha perdido el tiempo, sino que ha pensado en su sobrina, a la que echaba tanto de menos, que le ha fabricado este balancín de madera tan especial a juego con el dormitorio que ya le diseñó y montó, que es la envidia de toda mamá reciente.


![dormitorio recién nacido dosel](/images/posts/hazlo-tu-mismo/dormitorio.jpg)

Además, es que eso de construir cosas necesarias con nuestras propias manos, así como dar rienda suelta a nuestra creatividad mediante el dibujo o la pintura, como han hecho en casa de [@lapetitenature](https://www.instagram.com/lapetitenature/) mientras esperan a su pequeño con ilusión, parece ser muy satisfactorio y beneficioso para nuestra salud! 

![pintura-barriga-embarazada-jirafas](/images/posts/hazlo-tu-mismo/jirafas.jpg)

## Beneficios del "Hazlo tú mismo" o DIY
 
- Mejora nuestra autoestima al sentirnos útiles por haber sido capaces de crear algo por nosotros mismos con nuestro esfuerzo, y ver el resultado que, si bien al principio quizá no sea perfecto, nos dará la oportunidad de mejorar y superarnos a nosotros mismos.
	Y si lo ragalamos a alguien que lo necesita o a quien queremos nos hará sentir el doble de bien.

- Disminuye nuestro estrés: Fijaos, además de hacer algo útil o bonito el DIY es un antídoto eficaz contra el estrés ya que nos obliga a concentrarnos en el aquí y el ahora, con paciencia y dedicación casi exclusiva porque requiere de toda nuestra atención ayudándonos a abstraernos de preocupaciones que, no es que vayan a desaparecer, pero sí que cuando volvamos a poner el foco en ellas quizá las veamos con más calma y podamos afrontarlas mejor.

- Potencia nuestra imaginación y nuestra memoria porque necesitamos cierta planificación, un diseño, búsqueda de materiales adecuados

- Impulsa el aprendizaje de los niños porque contribuye en el desarrollo de la psicomotricidad fina, la habilidad manual, su capacidad de organización y planificación y  su paciencia para esperar el resultado a medio o largo plazo.

Seguro que no te habías parado a pensar la cantidad de cosas positivas que tiene hacer nuestras propias cosas. Y seguro que últimamente tú también has fabricado algo con tus manos! 

Cuéntame, qué has hecho?