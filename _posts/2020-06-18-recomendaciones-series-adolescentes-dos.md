---
layout: post
slug: recomendaciones-series-adolescentes-dos
title: Nueve series para adolescentes recomendadas o no
date: 2020-06-18 16:19:29
image: /images/posts/series-adolescentes-dos/p-series.jpg
author: .
tags:
  - adolescencia
---
Tanto tiempo en casa ha dado para que hayamos hecho un máster en recomendaciones de series Netflix, las mejores series HBO y series Amazon Prime de las que más furor están causando entre los adolescentes de 14 años.

Si hace tiempo os hacía [recomendaciones de series para preadolescentes aquí](https://madrescabreadas.com/2019/01/13/recomendaciones-series-netflix-adolescentes/), y os hacía estas [recomendaciones de series para toda la familia](https://madrescabreadas.com/2019/08/22/series-familias/) por si tenéis hijos de todas las edades, ahora ya hemos pasado a la adolescencia de pleno.

Si no estás suscrito a alguna de estas plataformas y vives con adolescentes, no podrás resistirte por mucho tiempo, así que te recomiendo varias opciones:

\-Si soléis comprar cosas en Amazon os interesa Amazon Prime Video, porque **compagina ventaja en envíos y compras con series y películas Online**.

Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de **contenido original** y es bastante bueno. Además, es económico porque por 3,99€ al mes tienes derecho a:

* Envío 1 día GRATIS en dos millones de productos enviados por Amazon.
* Envío gratis con entrega garantizada en el mismo día del lanzamiento para miles de productos en preventa de cine, series TV y videojuegos entre otros.
* **Twitch Prime:** Contenido adicional para videojuegos todos los meses. Descuentos exclusivos en la suscripción Prime con la reserva de videojuegos y mucho más.
* Acceso Prioritario a las Ofertas flash de Amazon.es, 30 minutos antes de su inicio.
* **Almacenamiento de fotos gratis e ilimitado** en la plataforma Amazon Drive con Prime Fotos.
* Descuentos en una selección de pañales con Amazon Familia.

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21)

\-Oferta para universitarios. Si tienes universitarios en casa, te interesa Amazon Prime Student. ¡Ahora hay una prueba de 90 días! Con esta opción tienes **todas las ventajas de Amazon Prime a mitad de precio**; solo EUR 18,00/año. Eso sí, esta oferta es sólo para estudiantes universitarios.

Puedes [darte de alta en Amazon Prime Student aquí](http://www.amazon.es/joinstudent?tag=ID_de_madrescabread-21)

Una vez que tenemos claro que tener acceso a contenido de calidad, variado y bien categorizado puede salvar muchos momentos en casa con nuestros hijos adolescentes, **vamos recomendaros nuestras series favoritas** para esta edad. Y digo vamos porque toda la información y opiniones me las ha dado mi hija mayor, también conocida como "Princesita", quien últimamente se ha convertido en una devoradora de series, pero no le gusta cualquiera, sino que es bastante crítica, y os dirá las que le gustan y las que no.

## 1 Riverdale

En nuestro número uno está nuestra favorita y la de la mayoría de adolescentes. Riverdale está causando furor porque lo que aparentemente es un pueblo tranquilo en realidad es un lugar donde **pasan cosas paranormales** y un grupo de chicos y chicas se van haciendo amigos y se dedican a investigar una serie de conspiraciones que trazan historia. El narrador de la historia es uno de ellos, que es el protagonista.

Ambientada en el presente y basada en los icónicos personajes de Archie Comics, Riverdale ofrece una sorprendente y subversiva mirada de Archie, Betty, Verónica y el resto de los habitantes de esta pequeña y surrealista ciudad.

**Riverdale Reparto**

![reparto Riverdale](/images/posts/series-adolescentes-dos/riverdale.jpg)

**Personaje favorito de Riverdale**

Nuestro **personaje favorito es Betty**, una chica que tenía problemas de pequeña porque su padre la trataba fatal y su madre era muy controladora, pero llega un momento en que se revela. 

Aunque el personaje de Cheryl también es muy interesante porque tiene bastante recorrido a lo largo de las temporadas, vive en una familia adinerada pero muy problemática, por eso en un principio ella se portaba muy mal con sus compañeros, pero el personaje va evolucionando poco a poco a mejor.

**Riverdale Trailer**

<iframe width="560" height="315" src="https://www.youtube.com/embed/HxtLlByaYTc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si los chicos son verdaderos fans de Riverdale, no se pueden perder la **nueva serie animada** con historias inéditas y más atrevidas que no han sido incluidas en la serie. Disfrutarán de todo lo que puede ofrecer tanto el volumen 1, 2 y 3 en su presentación de tapa dura.

![Riverdale Series animadas](/images/uploads/riverdale-comics.jpg "Riverdale")

RIVERDALE. VOLUMEN 1     [![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Riverdale-Volumen-1-Vv-Aa/dp/8467930470/ref=sr_1_22?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Riverdale&qid=1628610009&sr=8-22&madrescabread-21) (enlace afiliado)

\
RIVERDALE. VOLUMEN 2     [![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/RIVERDALE-VOLUMEN-2-VV-AA/dp/846792408X/ref=pd_bxgy_img_1/261-3752076-3735521?pd_rd_w=Gwj2N&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=HFPYVEFAC7SDB6NRJHNY&pd_rd_r=a58e9aaa-3adb-443e-aa48-bd4f3558f48e&pd_rd_wg=ezbcD&pd_rd_i=846792408X&psc=1&madrescabread-21) (enlace afiliado)

\
RIVERDALE. VOLUMEN 3     [![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Riverdale-3-Greg-Murray/dp/8467936479/ref=pd_bxgy_img_2/261-3752076-3735521?pd_rd_w=Gwj2N&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=HFPYVEFAC7SDB6NRJHNY&pd_rd_r=a58e9aaa-3adb-443e-aa48-bd4f3558f48e&pd_rd_wg=ezbcD&pd_rd_i=8467936479&psc=1&madrescabread-21) (enlace afiliado)

## 2 Las escalofriantes aventuras de Sabrina

Al igual que la versión de los años '90, la serie cuenta las aventuras de Sabrina Spellman (Kiernan Shipka), una bruja adolescente que intenta llevar una vida “normal” en el mundo mortal. Bajo la tutela de sus tías, comienza a descubrir todo lo que puede hacer gracias a sus poderes mágicos.

No sabemos si su éxito se debe a que está conectada con Riverdale, porque pasan cosas fantásticas o porque es mitad bruja y mitad humana, pero **esta serie engancha a lo adolescentes** de ahora, igual que nos enganchó en su momento la serie originaria a nosotras las madres.

Nuestro personaje favorito es el de Sabrina, porque es independiente y sabe arreglárselas sola, aunque vive con sus tías, que molan un montón.

**Sabrina Netflix reparto**

![reparto sabrina](/images/posts/series-adolescentes-dos/sabrina.jpg)

**Sabrina Netflix trailer**

<iframe width="560" height="315" src="https://www.youtube.com/embed/51fOujPcgIE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si tu hija se identifica con Sabrina en su mundo mágico y alocado, tienes que comprarle el funko temático de la serie. ¡La sorprenderás!

![Las escalofriantes aventuras de Sabrina](/images/uploads/funko-chilling-adventures-of-sabrina.jpg "Funko Chilling Adventures of Sabrina")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Funko-Pop-Television-Adventures-Superhappy/dp/B08Z23HMRL/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=funko+Sabrina&qi&madrescabread-21d=1628617203&sr=8-1&madrescabread-21) (enlace afiliado)

## 3 Stranger Things

La historia arranca durante la década de los 80, en el pueblo ficticio de Hawkins, Indiana, cuando un niño llamado Will Byers desaparece, hecho que destapa los **extraños sucesos que tienen lugar en la zona**, producto de una serie de experimentos que realiza el gobierno en un laboratorio científico cercano

Un grupo de niños van descubriendo cosas raras que pasan en el pueblo y que no tienen explicación aparente, Se dedican a investigar y conocen a Eleven, una niña recién salida del laboratorio donde experimentaban con ella por tener unos poderes especiales desde pequeña.

Nuestro personaje favorito es Eleven porque tiene poderes y es la más poderosa.

**Stranger things reparto**

![reparto stranger things](/images/posts/series-adolescentes-dos/strangers-things.jpg)

**Stranger things trailer**

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wre1F5YyIlA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

¿Se han metido de lleno en esta espectacular serie en familia? De igual modo, pueden disfrutar juntos armando el puzzle temático Stranger things mientras comparten en familia anécdotas de lo más recordado de la serie.

![Serie Stranger Things](/images/uploads/clementoni-puzzle-1000-piezas-strange-things-.jpg "Clementoni Puzzle 1000 Piezas Strange Things")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Clementoni-Puzzle-Piezas-Strange-39542-2/dp/B084HGHKWR/ref=sr_1_23?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Stranger+things&qid=1628617919&sr=8-23&madrescabread-21) (enlace afiliado)

## 4 Locke and Key

Basada en las famosas novelas gráficas que llevan su nombre, narra la historia de tres hermanos. Tras el asesinato de su padre, los jóvenes se mudan a una vieja mansión familiar con su madre, donde encuentran unas **llaves mágicas que les dan poderes y habilidades sobrenaturales**, a cuál más increíble (una te hace entrar en tu mente, otra te hace cambiar de aspecto...) Estas llaves son muy codiciadas y tendrán que sortear muchos problemas por esta causa.

Nuestro personaje favorito es el hermano pequeño, porque es el primero en escuchar los susurros que provienen de las llaves. Es el más ingenioso, valiente y súper gracioso (me recuerda a mi hijo pequeño 😜)

**Locke and key reparto**

![reparto locke and Key](/images/posts/series-adolescentes-dos/locke.jpg)

**Locke and key trailer**

<iframe width="560" height="315" src="https://www.youtube.com/embed/_EonRi0yQOE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

No se queden con lo contado en la serie. Juntos descubran la inspiración de este fascinante mundo mágico en su primera serie **Locke & Key Omnibus 1**

![Locke and Key](/images/uploads/locke-key-omnibus-1.jpg "Locke & Key Ómnibus 1")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/gp/product/8490942625/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=8490942625&linkId=c9784a1750c9ef131502f93fa2f525ce&madrescabread-21) (enlace afiliado)

## 5 The A list

Romance, rivalidad y misterio se entremezclan cuando un grupo de adolescentes viaja a un campamento de verano en una remota isla donde el drama sobrenatural de suspense está servido.

La protagonista es una chica, Ambert, muy extraña y que da muy mal rollo porque no duerme por las noches y se va por la montaña sola.

"Es una **serie impactante y que ocurren cosas inesperadas**. No es lo que parece y te rayas mucho"

Es una serie de las que podríamos denominar "adictiva" porque no puedes esperar para ver el siguiente capítulo.

**The A List reparto**

![reparto The A List](/images/posts/series-adolescentes-dos/the-a-list.jpg)

**The A List trailer** 

<iframe width="560" height="315" src="https://www.youtube.com/embed/7kIKGd6w6Y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 6 The rain

The Rain narra la situación de Escandinavia seis años después de que una gran catástrofe biológica aniquilara a casi toda la población. Dos hermanos deciden llegar al final de esta cuestión, por lo que se unen a un grupo de jóvenes para descubrir si hay un nuevo mundo.

El gobierno hizo un **experimento con productos químicos y hubo una lluvia letal para los humanos**. Uno de los científicos salva a su familia en un búnquer y vuelve al laboratorio para investigar la cura. Pero el bunquer era muy codiciado por todos y comenzaron los problemas de tal manera que la protagonista se queda a cargo de su hermano durante 6 años tras los cuales las cosas iban a cambiar mucho...

Nuestro personaje favorito es la chica porque cuida siempre a su hermano y es muy protectora.

**The rain reparto**

![Reparto The Rain](/images/posts/series-adolescentes-dos/the-rain.jpg)

**The rain trailer**

<iframe width="560" height="315" src="https://www.youtube.com/embed/5f33veY_O-4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 7 Green House Academy

Es una serie de ciencia ficción inspirada en la original israelí The Greenhouse. La trama gira en torno a Hayley y Alex, **dos hermanos adolescentes que ingresan en un internado para los estudiantes de élite.** La serie se rodó completamente en Israel, con actores extranjeros.

**Reparto Green House Academy**

![Reparto Green House Academy](/images/posts/series-adolescentes-dos/green.jpg)

**Trailer Green House Academy**

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hwa62p5x-pM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Y qué mejor forma de mostrar pasión por esta icónica serie que mostrando un espectacular poster temático. Consiéntelos con este regalo

![Green House Academy](/images/uploads/daaint-baby-greenhouse-academy-2017-tv-shows-art-print-poster.jpg "Daaint baby Greenhouse Academy 2017 TV Shows Art Print Poster")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Daaint-baby-Greenhouse-Academy-Poster/dp/B085MBZNTR/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=greenhouse+academy&qid=1628622176&sr=8-3&madrescabread-21) (enlace afiliado)

## 8 Anne with An E

Todo vuelve, y **la serie que nos conquistó en nuestra infancia** a las madres, Ana de las Tejas Verdes, ahora engancha a nuestras hijas.

Anne cuenta con una privilegiada imaginación, y con su vivacidad, risas y lágrimas logrará conquistar el corazón de los Cuthbert y del resto de los habitantes de Avonlea, a quienes trata de mostrarles un mundo nuevo y en quienes encontrará, por primera vez, la familia con la que siempre soñó

**Reparto de Anne with An E**

![Reparto Anne with An E](/images/posts/series-adolescentes-dos/Anne.jpg)

**Trailer de Anne with An E**

<iframe width="560" height="315" src="https://www.youtube.com/embed/lxrj5--SiyQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ahora bien, si prefieres tener la colección de 27 episodios en todo su esplendor en formato DVD y así poder llevarla a cualquier lugar, puedes adquirirla.

![Anne with An E](/images/uploads/anne-with-an-e-the-complete-collection-series-1-3-dvd-.jpg "Anne With an 'E' - The Complete Collection: Series 1-3 [DVD]")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Anne-Complete-Collection-1-3-DVD/dp/B08L42HMXB/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Anne+with+An+E&qid=1628622953&sr=8-2&madrescabread-21) (enlace afiliado)

## 9 Katy Keene

Katy Keene es una serie de televisión de drama basada en el personaje de los cómics de Archie del mismo nombre. ... La serie cuenta los **orígenes y las luchas de cuatro aspirantes a artistas** que intentan llegar a Broadway, y es una serie derivada de Riverdale.

Esta serie no nos gusta demasiado porque no tiene mucho gancho, en nuestra opinión.

**Reparto de Katy Keene**

![Reparto Katy Keene](/images/posts/series-adolescentes-dos/katy.jpg)

**Trailer de Katy keene**

<iframe width="560" height="315" src="https://www.youtube.com/embed/p9-3U1J6Skc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Puede que la serie no tenga tanto gancho como otras, pero quizás te interese el repertorio musical que ella contiene

![Katy Keene](/images/uploads/katy-keene-season-1.jpg "Katy Keene: Season 1 ")

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Katy-Keene-Original-Television-Soundtrack/dp/B088LWXRD7/ref=sr_1_7?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Katy+Keene&qid=1628623561&sr=8-7&madrescabread-21) (enlace afiliado)

Espero que os hayan servido estas recomendaciones y que me hagáis otras que os hayan gustado.

Si te ha servido esta información, compártela! Eso me ayudaría a poder seguir escribiendo este blog.