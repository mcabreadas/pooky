---
layout: post
slug: jabon-casero-para-lavadora
title: Cómo hacer jabón casero también para la lavadora
date: 2022-01-14T13:21:34.562Z
image: /images/uploads/istockphoto-913770736-612x612.jpg
author: faby
tags:
  - trucos
---
Estamos en una década en donde **muchas personas se inclinan por lo ecológico y lo “hecho en casa”.** El jabón casero para lavadora es una maravilla. Muchos [detergentes contienen ingredientes químicos que son muy contaminantes](https://revista.consumer.es/portada/en-polvo-lavan-mejor-pero-contaminan-mas.html).

En este post te hablamos de los [tipos de jabones artesanales y sus beneficios](https://madrescabreadas.com/2021/12/16/tipos-de-jabones-artesanales-y-sus-beneficios/).

En esta oportunidad te hablaré de cómo puedes hacer jabón casero para lavadora, además compartiré algunas ventajas de inclinarte por la opción ecológica.

## ¿Por qué utilizar detergente líquido hecho en casa?

Hay varias razones para inclinarse por el uso de jabón líquido artesanal para lavadora, sin embargo, puedo resumirlo en “**no contiene productos químicos”.** ¿No estás convencido? Muchos jabones tanto en barra como en líquido tienen compuestos derivados del petróleo.

Por otra parte, algunos detergentes contienen perfumes sintéticos que afectan el sistema hormonal. En cambio, el **uso de sustancias naturales es un método antiguo que no afecta la salud** y al mismo tiempo higieniza de forma eficiente la ropa.

## ¿Cómo hacer jabón líquido para lavadora?

Hay diferentes métodos para hacer jabón líquido para lavar ropa. A continuación, te revelaré las dos recetas más populares.

### Pasos para hacer Jabón para lavadora con bicarbonato

El **bicarbonato contiene propiedades limpiadoras,** de hecho se suele utilizar ampliamente para el cuidado del hogar. Remueve fácilmente la mugre y desprende grasa de la ropa sin alterar el color o textura de las prendas.

1. **Reúne los materiales.** Necesitas un litro y medio de agua. 75 gramos de jabón artesanal o tipo Marsella, una taza de bicarbonato, 25 gotas de aceite esencial del olor que más te guste.
2. **Ralla el jabón en barra**. Lo primero que hay que hacer es rallar el jabón en barra artesanal. Coloca el jabón rallado en un recipiente resistente y añade la tacita de bicarbonato. 
3. **Haz la mezcla.** Calienta el litro y medio de agua. Una vez caliente, échalo poco a poco en el recipiente donde está la ralladura de jabón. Remueve para que se derrita totalmente. 
4. **Añade el aroma.** Una vez que está mezclado y debidamente derretido el jabón, debes agregar el aroma, para ello, vierte unas 25 gotitas de aceite esencial de tu preferencia.
5. **Deja enfriar.** Una vez frío puedes utilizarlo. Recuerda remover antes de su uso, ya que es posible que cambie su aspecto en reposo.

### Pasos para hacer Jabón líquido con jabón de lagarto

El j**abón de lagarto se ha convertido en una marca de excelencia en toda España**. Es ideal para pieles sensibles y sus ingredientes son naturales. Si deseas, puedes hacer una versión de jabón líquido con base de este jabón.

1. **Reúne los materiales.** Necesitas 125 g de jabón de lagarto, dos cucharadas de bicarbonato de sodio, 3 litros de agua.
2. **Ralla el jabón en barra.** En un recipiente ralla muy bien el jabón de lagarto y reserva.
3. **Haz la mezcla.** Coloca a hervir los 3 litros de agua. Cuando esté en ebullición, agrega dos tazas de agua caliente al bicarbonato para disolverlo. Luego, agrega el jabón rallado y el bicarbonato disuelto al agua que está hirviendo. Remueve por un par de minutos para mezclar bien.
4. **Deja que se enfríe.** Una vez frío puedes pasarlo al recipiente de tu preferencia.

Tal como has podido notar, hacer jabón casero es muy simple. Estos métodos no solo son más ecológicos, sino que además, son mucho más **económicos**, pues la barra de jabón suele rendir más cuando se le derrite en agua.

[![](/images/uploads/lagarto-escamas.jpg)](https://www.amazon.es/Lagarto-Escamas-Hipoalergénicas-400-Gr/dp/B08CHXGNJG/ref=pd_sbs_4/261-4500694-6173210?pd_rd_w=jxjmw&pf_rd_p=20680f7e-bc7f-4504-a338-63dbdaa26d7f&pf_rd_r=00455H2M2Z4T7WSD4A36&pd_rd_r=354a74ff-8b5c-436d-aaab-493fa1d9fb4c&pd_rd_wg=chX7o&pd_rd_i=B08CHXGNJG&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Lagarto-Escamas-Hipoalergénicas-400-Gr/dp/B08CHXGNJG/ref=pd_sbs_4/261-4500694-6173210?pd_rd_w=jxjmw&pf_rd_p=20680f7e-bc7f-4504-a338-63dbdaa26d7f&pf_rd_r=00455H2M2Z4T7WSD4A36&pd_rd_r=354a74ff-8b5c-436d-aaab-493fa1d9fb4c&pd_rd_wg=chX7o&pd_rd_i=B08CHXGNJG&psc=1&madrescabread-21) (enlace afiliado)

Si quieres hacer jabon casero en pastilla, te dejo [nuestra receta estrella de jabon casero de la abuela](https://madrescabreadas.com/2013/11/08/la-salita-de-la-abuela-jabón-casero/) de una de nosotras.

*Photo Portada by tdub303 on Istockphoto*