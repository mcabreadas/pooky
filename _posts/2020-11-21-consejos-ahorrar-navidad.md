---
layout: post
slug: consejos-ahorrar-navidad
title: 10 consejos para ahorrar en Navidad
date: 2020-11-21T18:19:29+02:00
image: /images/posts/ahorrar-navidad/cerdo-lazo.jpg
author: belem
tags:
  - trucos
  - regalos
  - navidad
  - destacado
---

Navidad ha llegado. Bueno, no exactamente, pero la fecha ya se encuentra tan cercana que en todos los escaparates de los centros comerciales podemos verla ya exhibida desde hace unas semanas. Para las y los que tenemos hijos, estas fechas siempre vienen con un extra de todo: felicidad, emoción, anticipación y presión entre otras. La anticipación y la presión van de la mano con los gastos propios de esta época. Las reuniones, los regalos, las comidas, las decoraciones, y demás eventos propios de la época no son gratis. Afortunadamente, también hay muchas ofertas y grandes eventos de descuentos que podemos aprovechar para el ahorro en Navidad, y que podrán determinar en cierto modo qué regalar estas Navidades hacer nuestras compras de manera favorable para nuestros bolsillos. En este artículo, exploraremos algunos consejos para ahorrar en Navidad. 

<iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=30&p=42&l=ur1&category=esgifting&banner=1THHZ23SB887MJNRS282&f=ifr&linkID=a4f6e23656b0f5105865732ba76f723f&t=madrescabread-21&tracking_id=madrescabread-21" width="234" height="60" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

## 10 Consejos para ahorrar en Navidad

## Anticipa tus compras para ahorrar en Navidad 

A estas alturas puede ser un poco precipitado, sin embargo, tengámoslo presente para el próximo año. La mejor forma de saber qué regalar estas Navidades (o las próximas) es, conforme los meses vayan avanzando, ir elaborando una lista de compras. Anota aquello que tus hijos, padres, pareja, hermanos, amigos, etc. quieran o necesiten. A veces hacemos pequeños comentarios de algo que nos ha gustado o que quisiéramos tener. La anticipación nos va a ayudar a no comprar regalos de último momento, lo cual suele terminar con compras demasiado caras y que no siempre serán del total agrado de quien recibe. 

## Ve ahorrando para comprar los regalos de Navidad

Este consejo también debería ser aplicado con anticipación. A partir de enero, ve apartando una pequeña cantidad de dinero para las compras navideñas. Te sorprenderá la cantidad que puedes reunir en un año casi sin darte cuenta. Esto amortiguará los gastos navideños y te permitirá quizás tener un presupuesto más grande para dichas compras. 

![hucha de unicornio dorado](/images/posts/ahorrar-navidad/unicornio.jpg)

## Espera los descuentos para ahorrar en Navidad

Me voy a atrever a asegurar que casi todos conocemos de manera general cuándo hay buenos descuentos. Existen las ofertas de final de temporada, las de media temporada, el Black Friday y las de festividades (por el día de la madre, por los Reyes Magos, etc). Es importante que, con base en estas fechas, vayamos ahorrando dinero para poder hacer compras en estos descuentos. El ahorro en Navidad es posible, sólo es cuestión de organización y previsión. 


<a href="https://s.click.aliexpress.com/e/_AUu2hP?bz=725*90" target="_parent"><img width="725" height="90" src="//ae01.alicdn.com/kf/H4bd1f28f0a10436bb59460ec88777c7fX.png"/></a>


## Aprovecha el Black friday para ahorrar en los regalos de Navidad

Puede que éste sea una extensión del consejo anterior, pero recordemos: si ya no tenemos mucho tiempo para hacer las compras navideñas, aprovechar las promociones del Black Friday (que suelen aparecer en los días o semanas finales de noviembre). Entonces, Black Friday puede ser una de nuestras últimas oportunidades para conseguir buenos precios en regalos y otras cosas que necesitaremos en época navideña.

![escaparate black friday](/images/posts/ahorrar-navidad/black-friday.jpg)

## Rastrea los precios para ahorrar en Navidad

Ve familiarizándote con los precios de aquello que planeas comprar en distintos establecimientos. Tener una lista física o digital puede ser de gran ayuda. Esto te va a ayudar a poder reconocer una buena oferta real y aprovecharla. 

## Establece un presupuesto para ahorrar en Navidad

Una vez con una idea ya muy general de lo que vamos a comprar, establezcamos un presupuesto. Toma en cuenta tus otros gastos, tus ingresos normales y los ingresos extra. No hagas planes para gastarlo todo. Deja un margen para gastos inesperados.

## Paga en efectivo o establece un límite real en tus tarjetas

Esto va a ayudarte a apegarte a un presupuesto. Retira solamente la cantidad que planeas gastar y no lleves tus tarjetas contigo. Muchos consejeros financieros recomiendan usar efectivo en lugar de usar tarjetas de crédito o débito como método de control de gastos. Asimismo, la mayoría de las apps bancarias ofrecen la opción de establecer un límite de gasto. Al apegarnos a un presupuesto previamente planeado, nos ayudamos a nosotros mismos a no gastar de más. 

![planta del dinero](/images/posts/ahorrar-navidad/planta.jpg)

## Comparte gastos

Si se planea asistir a una cena navideña familiar, lo más conveniente es distribuir equitativamente los gastos. De esta manera, se evita que una sola familia absorba los gastos de dicha cena. 

## Amigo invisible para ahorrar en Navidad

En caso de acostumbrar a dar regalos a todos los integrantes de una familia grande, puede organizarse un Amigo Invisible o intercambio de regalos para evitar un gasto importante. De esta manera, en lugar de tener que comprar 25 regalos, por ejemplo, se comprará sólo uno por cada integrante de la familia. 

En el amigo invisible suele establecerse un límite de costes, el cual puede adecuarse a la situación económica que se esté viviendo. Asimismo, es buena idea hacer una lista en la que todos los participantes de dicho intercambio compartamos qué es lo que queremos recibir que se encuentre dentro del límite económico establecido. Así evitamos gastar en regalos que podrían no gustar o terminar siendo más baratos o caros de lo esperado. Aunque le quita un poco de emoción a los regalos navideños, un intercambio es la mejor forma de asegurarnos de dar y recibir un buen regalo dentro de nuestras posibilidades.

![regalo](/images/posts/ahorrar-navidad/regalo.jpg)

## Visita bazares o compra de segunda mano para ahorrar en Navidad

Últimamente esta filosofía cobra más y más fuerza. Comprar de segunda mano no sólo es amigable para nuestro bolsillo, sino que también es una buena forma de ayudar al planeta. En estas épocas de conciencia ética y ambiental, las compras de segunda mano son una excelente opción para comprar aquello que necesitamos. 

Espero que te hayan servido estos consejos para ahorrar en Navidad, y que los compartas, de este modo también ayudas a que sigamos escribiendo este blog.



*Photo portada by Michael Longmire on Unsplash*
*Photo by Micheile Henderson on Unsplash planta*
*Photo by Annie Spratt on Unsplash unicornio*
*Photo by freestocks on Unsplash regalo*
*Photo by CardMapr on Unsplash black friday*