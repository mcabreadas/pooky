---
layout: post
slug: kit-para-cuidados-menstruales-adolescentes
title: Cómo hacer un kit para cuidados menstruales para chicas adolescentes
date: 2022-10-17T09:26:38.380Z
image: /images/uploads/cuidados-menstruales.jpg
author: faby
tags:
  - adolescencia
---
El período menstrual nos acompaña durante gran parte de nuestra vida, así pues **es necesario estar preparadas para cada llegada.**

Sin importar la edad; si eres adolescente o adulta, todas necesitamos ciertos productos específicos para **cuidar la higiene menstrual.** En esta entrada, explicaremos cómo hacer un kit de higiene menstrual para que puedas orientar a tu hija.

En este post te explicamos [como afrontar la primera regla](https://madrescabreadas.com/2019/09/21/primera-regla/) desde el acompañamiento y la empatia. Recuerda hablar en su idioma, o al menos conocer el lenguaje de los adolescentes para conectar mas con ella. Este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) te va a ayudar.

## ¿Qué tener en un kit de menstruación?

**Un kit de menstruación es un pack en el que debes incluir lo esencial durante esos días**. Lo ideal es que lleves todo lo que necesitas, pero que quepa en un bolso pequeño, de lo contrario, será incómodo de transportar.

A continuación, mencionaremos los productos más importantes frente a la llegada de la regla.

### Bolso o estuche

Lo primero que debes tener es un **bolso** en el que puedes colocar las toallas y lo que necesites durante el período menstrual.

[![](/images/uploads/portatiles-de-almacenamiento.jpg)](https://www.amazon.es/Port%C3%A1tiles-Almacenamiento-Compresa-Sanitaria-Sanitarias/dp/B07XC1DBJB/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F0JJRYQ204BR&keywords=estuche+menstrual&qid=1663676028&sprefix=estuche+menstrual%2Caps%2C322&sr=8-3&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Port%C3%A1tiles-Almacenamiento-Compresa-Sanitaria-Sanitarias/dp/B07XC1DBJB/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F0JJRYQ204BR&keywords=estuche+menstrual&qid=1663676028&sprefix=estuche+menstrual%2Caps%2C322&sr=8-3&madrescabread-21) (enlace afiliado)

Este bolso **puede ser del mismo tamaño del que usas para los cosmético**s. De hecho, puedes comprar uno de ese estilo y usarlo para guardar tu kit menstrual.

[![](/images/uploads/cremallera-portatil-compresa-sanitaria-bolsa-de-almacenamiento.jpg)](https://www.amazon.es/Monedero-Historieta-Cremallera-Sanitaria-Almacenamiento/dp/B07XNYKQ5Q/ref=sr_1_9?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F0JJRYQ204BR&keywords=estuche%2Bmenstrual&qid=1663676028&sprefix=estuche%2Bmenstrual%2Caps%2C322&sr=8-9&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Monedero-Historieta-Cremallera-Sanitaria-Almacenamiento/dp/B07XNYKQ5Q/ref=sr_1_9?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F0JJRYQ204BR&keywords=estuche%2Bmenstrual&qid=1663676028&sprefix=estuche%2Bmenstrual%2Caps%2C322&sr=8-9&th=1&madrescabread-21) (enlace afiliado)

### Artículos de higiene menstrual

Ahora es el momento de elegir tus productos para cuidado menstrual. Hay varias opciones, las más comunes son las compresas o **toallas sanitarias**. Coloca la cantidad que necesites para un día, de este modo no te quedas corta.

En funcion de la cantidad de flujo, podras optar por varios tamaños y capacidad de absorcion.

[![](/images/uploads/evax-liberty-compresas-con-alas.jpg)](https://www.amazon.es/Liberty-Super-Unidades-Compresa-Notar%C3%A1s/dp/B086HCYHF9/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=toallas+sanitarias&qid=1663676426&sr=8-3&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Liberty-Super-Unidades-Compresa-Notar%C3%A1s/dp/B086HCYHF9/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=toallas+sanitarias&qid=1663676426&sr=8-3&madrescabread-21) (enlace afiliado)

Por otro lado, los **tampones** son una buena opción, ya que tienen un mayor tiempo de duración con relación a las compresas. Es decir, si esta en clase y no puede ir con frecuencia al baño, es una buena opcion, pero antes debes hacer una labor didactica con tu hija para enseñarle a colocarselo correctamente. 

Es mejor empezar con un tamaño mini hasta que tenga practica, y entonces podra usar uno mas grueso si lo necesita.

Mi consejo es que uses tampones con aplicador tipo Tampax Pearl, ya que al tener una superficie no porosa es mucho mas resbaladizo y evita la friccion que puede asustar en proncipio a las niñas.

[![](/images/uploads/tampax-compak-pearl-super-tampones-con-aplicador.jpg)](https://www.amazon.es/Tampax-Combinaci%C3%B3n-Protecci%C3%B3n-Discreci%C3%B3n-72-Unidades/dp/B09Q96G4H1/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3P3JAW7F10J48&keywords=tampones&qid=1663676456&sprefix=tampone%2Caps%2C260&sr=8-4-spons&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Tampax-Combinaci%C3%B3n-Protecci%C3%B3n-Discreci%C3%B3n-72-Unidades/dp/B09Q96G4H1/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3P3JAW7F10J48&keywords=tampones&qid=1663676456&sprefix=tampone%2Caps%2C260&sr=8-4-spons&th=1&madrescabread-21) (enlace afiliado)

Por último, es muy aconsejable el uso de **copa menstrual.** La ventaja de una copa es que es reutilizable y tiene capacidad de almacenar hasta 12 horas de menstruación. Teneis que elegir bien bien cual es suya, ya que hay varios tamaños y diseños.

[![](/images/uploads/enna-cycle-2-copas-menstruales.jpg)](https://www.amazon.es/CYCLE-Menstrual-Talla-Aplicador-Esterilizador/dp/B01N63E1JO/ref=sr_1_3?crid=2XF6BZKCGRHJA&keywords=copa%2Bmenstrual&qid=1663676803&sprefix=copa%2Bme%2Caps%2C259&sr=8-3&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/CYCLE-Menstrual-Talla-Aplicador-Esterilizador/dp/B01N63E1JO/ref=sr_1_3?crid=2XF6BZKCGRHJA&keywords=copa%2Bmenstrual&qid=1663676803&sprefix=copa%2Bme%2Caps%2C259&sr=8-3&th=1&madrescabread-21) (enlace afiliado)

### Toallitas húmedas

Algo que no debe faltar en tu kit menstrual de emergencia son toallitas, para cuando estes fuera de casa y no puedas lavarte con agua y jabon intimo, que seria lo ideal, de este modo puedes **limpiar la zona externa de la vagina.** Las *[toallitas húmedas de bebé](https://www.amazon.es/WaterWipes-Toallitas-toallitas-limpiadoras-compostables/dp/B08MXT5VDK/ref=sr_1_1_sspa?crid=K77M9KPZUT6M&keywords=toallitas%2Bh%C3%BAmedas%2Bbeb%C3%A9&qid=1663677289&rdc=1&sprefix=toallas%2Bh%C3%BAmedas%2Caps%2C254&sr=8-1-spons&th=1&madrescabread-21)* y las *[toallitas para hemorroides](https://www.amazon.es/HEMOSAN-CINFA-HEMORROIDES-60-TOALLITAS/dp/B00TTWSVJW/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1D7MT5Q9H1XXM&keywords=toallas+para+hemorroides&qid=1663676989&sprefix=toallas+para+hemorroide%2Caps%2C265&sr=8-1&madrescabread-21) (enlace afiliado)* pueden ser buenas opciones.

Por cierto, las **mejores toallas para cuidar tu higiene íntima son las que no poseen perfumes**, de este modo no irritan la piel.

### Muda de ropa interior

Si tu periodo empezó de forma sorpresiva y te manchaste, no pasa nada. A todas nos ha ocurrido. Pero no es higiénico seguir con la misma braga. Te recomendamos **colocar una prenda íntima adicional en casos de emergencia.**

### Incluye algunos analgésicos

Los analgésicos no pueden faltar en tu kit. Los cólicos menstruales pueden hacer que el primer día de período sea una pesadilla.

Algunos de los mejores analgésicos son **ibuprofeno y naproxeno.** Por cierto, no abuses de la dosis.

### Golosinas para consentirte

Por último, incluye en tu bolso de emergencia para la menstruación algunas golosinas. Una buena opción es una **pequeña *[barra de chocolate](https://www.amazon.es/Fazer-Barra-chocolate-leche-Geisha/dp/B085WRM1DX/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=barra+de+chocolate&qid=1663678633&sr=8-6&madrescabread-21) (enlace afiliado)***. Esto te permite contar con energía y a la vez consentirte. Verás que mantendrás tu buen humor.

En conclusión, es importante disponer de productos esenciales para cuando llegue el periodo. Recuerda actualizar el kit de cuidados menstruales, a fin de que en cada regla cuentes con lo esencial.