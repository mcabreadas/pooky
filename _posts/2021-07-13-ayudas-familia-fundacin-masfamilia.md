---
layout: post
slug: ayudas-familia-fundacion-masfamilia
title: Fundación Másfamilia por la conciliación familiar
date: 2021-07-20T10:59:31.760Z
image: /images/uploads/fotomasfamilia.jpg
author: maria
tags:
  - invitado
  - conciliacion
---
Nuestros invitados de esta semana son el equipo de la Fundación MásFamilia, las primeras personas a quienes desvirtualicé allá por el año 2011, época en la que Twitter bullía y era la principal red social... donde se cocía el bacalao, vamos...

Ellos me invitarona un desayuno bloguero (sí, era la época del furor de los blogs, cuando la gente solía leer más) para hablar sobre conciliación, el tema del momento entonces... y ahora también, la verdad, porque algo hemos avanzado, pero en la práctica, no se nota mucho...



## 1. ¿Qué es la Fundación Másfamilia?

[Fundación Másfamilia](https://www.masfamilia.org) es una entidad privada, sin ánimo de lucro, aconfesional e independiente que se constituye en España para la protección, defensa y promoción de la familia, en especial de aquellas con dependencias en su seno.

## 2. ¿Cómo y cuándo surge la Fundacion Masfamilia?

Nacemos en el año 2003, aunque no es hasta el año 2005 cuando impulsamos la Iniciativa efr cuyo principal objetivo es la promoción y evaluación de la gestión de conciliación de la vida personal, familiar y laboral en empresas, organizaciones del tercer sector e instituciones educativas.

Actualmente más de 850 entidades están gestionando la conciliación a través del Modelo efr en todo el mundo, dando cobertura a más de medio millón de colaboradores y a sus familias. Además, la Iniciativa efr cuenta con el aval del Gobierno de España para formar parte de un patronato como principal órgano de gobierno y con el de Naciones Unidas quién reconoció a la iniciativa efr como best practice internacional.

![evento fundacion masfamilia](/images/uploads/acto-de-entrega-efr.jpg)

## 3. ¿Cómo ayuda a las familias?

Trabajamos día a día en la gestión de la conciliación, pero lo hacemos acompañando a empresas y organizaciones de diferentes tamaños y sectores. Así desarrollamos acciones que faciliten el equilibrio de la vida personal, familiar y laboral mejorando así la calidad de vida y bienestar de los colaboradores y sus familias.

## 4. ¿Por qué apuesta por la familia?

La familia es un valor fundamental dentro de nuestra cultura efr. La familia, en toda su diversidad, constituye una dimensión imprescindible dentro del proceso de gestión efr que implantan las organizaciones. Por esta razón, trabajamos de la mano de empresas, e instituciones a través del Modelo efr para facilitar la conciliación.

## 5. ¿Creeis que se valora lo suficiente el papel de la familia en la sociedad?

Las familias fueron uno de los principales motores para salir de la crisis de 2008. Asumieron el reparto de ingresos para poder salir adelante para poder hacer frente las dificultades que la recesión económica provocó. Las familias han seguido siendo ese motor también para salir adelante de la crisis provocada por el Covid-19. Son un agente básico en la realidad socioeconómica española para poder afrontar cualquier problema que se ponga por delante. Ambas crisis hubiesen sido diferentes sin el papel desarrollado desde las familias.

![certificado empresa familiarmente responsable](/images/uploads/certificado-efr.png)

## 6. ¿Tras el confinamiento creeis que la familia se ha “revalorizado”?

La situación provocada por el Covid-19 nos hizo darnos cuenta de lo que es verdaderamente importante en nuestras vidas. El consumismo y nuestro ajetreado nivel de vida han quedado relegados a un segundo plano y hemos aprendido que lo que realmente necesitamos es a nuestra familia y a nuestros seres queridos y amigos.

En definitiva, valores universales como la familia han cobrado importancia y nos permiten encontrar esos mínimos para que la persona siga desarrollándose en todas las facetas de su vida.

## 7. ¿Qué aportan las familias a la sociedad?

Las familias constituyen una unidad básica para nuestra sociedad. Valores universales como la familia, en todos sus modelos, han cobrado importancia y nos permiten encontrar esos mínimos para que la persona siga desarrollándose en todas las facetas de su vida.

## 8. ¿Creeis que se formarían más familias si se ofrecieran más facilidades para conciliar la vida laboral y familiar?

Por supuesto. Es evidente que la conciliación es una de las razones que más influye en los hombres y [mujeres a la hora de tener hijos](https://madrescabreadas.com/2017/03/12/diferencias-tres-hijos/), pero también influye la inestabilidad laboral o los permisos de maternidad y paternidad insuficientes. Por ello, es necesario que la empresa se posicione como un agente de cambio y contribuya a la sociedad a través de sus empleados con una gestión de la conciliación profesionalizada.

## 9. ¿Las familias están solas ante el peligro o reciben el suficiente apoyo de la administración?

La conciliación no había formado parte de la agenda pública hasta la llegada de la pandemia. Las familias no lo han tenido nada fácil y nunca lo han tenido. 

Más allá del tejido empresarial, es importante poner el foco en los gobiernos. Es necesario que los gobiernos, las instituciones, las empresas y demás actores sociales trabajen de manera conjunta para potenciar la conciliación. De tal forma que las personas podamos dedicar más tiempo y de mejor calidad a nuestra familia. Porque [gestionar la conciliación](https://madrescabreadas.com/2020/09/27/teletrabajo/) es dar un paso adelante y comprometerse con el bienestar de las familias.

## 10. ¿Qué proyectos de futuro tiene Fundación Másfamilia?

Por un lado, seguir trabajando del lado de las empresas y organizaciones desarrollando acciones que supongan una mejora de la calidad de vida y bienestar de las personas y sus familias mediante la gestión de la conciliación de la vida personal, familiar y laboral.

Por otro lado, ejerciendo un rol de influencia para que la administración pública no se olvide de esta cuestión tal vital. Además de continuar con la labor de sensibilización a la sociedad en todo lo relativo al equilibrio de las diferentes facetas que componen nuestra vida.

Si te ha gustado, comparte, y suscribete para no perderte nada!