---
layout: post
slug: trucos-ahorrar-vuelta-al-cole
title: 5 trucos para ahorrar en la vuelta al cole, o al menos no gastar tanto
  como pensabas
date: 2021-09-23T10:22:07.481Z
image: /images/uploads/istockphoto-1166892747-612x612.jpg
author: .
tags:
  - trucos
---
Aunque aún seguimos en pandemia y algunos centros de estudio no han abierto normalmente, sobre todo [universidades](https://www.elconfidencialdigital.com/articulo/vivir/universidades-apuestan-definitivamente-modelo-presencial-proximo-curso/20210705190124258483.html), lo cierto es que **estudiar desde casa también conlleva gastos.** Por eso, independientemente si tus hijos volverán a la escuela, lo cierto es que el año escolar hay que afrontarlo desde el punto de vista economico tambien.

A continuación, te brindaré los mejores **5 consejos para ahorrar en la vuelta al cole.**

### **Haz un presupuesto**

Esta es la primera regla de administración: siempre se deben **planificar los gastos.**

Haz una lista de los **materiales que sean realmente necesarios** para cada uno de tus hijos y coloca el precio que estimas de cada útil escolar. De este modo, podrás tener un promedio del gasto que estás dispuesto hacer.

Algunos útiles indispensables son: *[cuadernos](https://www.amazon.es/Miquelrius-Cuaderno-Emotions-Verde-Cuadr%C3%ADcula/dp/B07MQLLCYY/ref=pd_sim_5/259-0761627-6283609?pd_rd_w=zjf2u&pf_rd_p=3e429e8e-1ade-491e-9a6e-8dce5b4e2a78&pf_rd_r=W3HX0XY8Z6G15SHY93W7&pd_rd_r=3f015f1c-7e29-485e-98c4-4556be8835e4&pd_rd_wg=WJZuC&pd_rd_i=B07MQLLCYY&psc=1)*, colores, *[lápices](https://www.amazon.es/AmazonBasics-L%C3%A1pices-n-%C2%BA-madera-afilados/dp/B071JM699B/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=l%C3%A1pices&qid=1630422175&sr=8-6)*, pega blanca, *[goma de borrar](https://www.amazon.es/Milan-BMM9215-Pack-gomas-borrar/dp/B006T8O2PG/ref=pd_bxgy_img_1/259-0761627-6283609?pd_rd_w=eJ0dj&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=76AYAXJG34DFJFVQSMV6&pd_rd_r=e0418869-9d6e-4f60-a466-9e8f95b3e8ec&pd_rd_wg=hXpgm&pd_rd_i=B006T8O2PG&psc=1) (enlace afiliado)*. Recuerda que no es necesario comprar todos los útiles al inicio, puedes hacer compras escaladas, por ejemplo, los cuadernos se pueden comprar en cada periodo o nuevo lapso.

### **Reutiliza algunos útiles escolares**

Uno de los valores más importantes que se debe inculcar en los hijos es cuidar sus cosas personales, lo que incluye libros y cuadernos. **Revisa si pueden usar la misma mochila, libros, colores,** etc.

### **Compara precios**

En el mercado puedes conseguir precios con diferencias abismales, por eso, no te apresures a comprar, además, no te confíes en la palabra “oferta”. Verifica los precios y **cerciórate de que realmente estás frente a una oferta**.

Actualmente, estamos luchando contra la crisis sanitaria, por lo tanto, en vez de salir a la calle a recorrer los comercios te aconsejo **visitar tiendas virtuales**. Amazon ofrece buenos precios en *[mochila](https://www.amazon.es/Puma-Phase-Backpack-Unisex-Adulto/dp/B07D5QZFRL/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=mochila%2Bescolar&qid=1630418294&sr=8-5&th=1)* de excelente calidad, *[pega blanca](https://www.amazon.es/APLI-12850-Cola-color-blanco/dp/B07BSMHFVV/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=pega+blanca&qid=1630419441&sr=8-3)*, *[colores para niños](https://www.amazon.es/Bic-Plastidecor-Ceras-colores-pack/dp/B00JMFRIOU/ref=bmx_dp_vd7vanbn_2/259-0761627-6283609?pd_rd_w=8XKe5&pf_rd_p=e8dfd2a1-b6a1-4831-97fd-e355f5e6dab9&pf_rd_r=CM7HCQYK8QQ1J1MRKYJV&pd_rd_r=5af5a640-2036-45c6-89a8-91abbf0a07c8&pd_rd_wg=BspiK&pd_rd_i=B00JMFRIOU&psc=1) (enlace afiliado)*, etc.

Compara tamben dentro de la amplia oferta de [actividades extraescolares, algunas on line](https://madrescabreadas.com/2017/12/20/extraescolar-matematicas-smartick/).

### **Adquiere libros de segunda mano**

Hay tiendas virtuales (anuncios en Facebook o redes sociales) ventas de garajes que colocan **libros usados en excelente estado y buen precio.** Si cuentas con un presupuesto muy ajustado puedes considerar esta opción.

### **Dispositivos electrónicos**

Si la vuelta al cole se hace de forma presencial, tendrás que hacer **gastos de varios uniformes**, en el que te aconsejo comparar precios.

Pero, si las **clases son de forma remota**, entonces debes invertir en un buen dispositivo que permita hacer videollamadas y soporte las aplicaciones del momento: **Zoom, skype,** y otras.

En este sentido, debes adquirir un paquete que ofrezca amplio acceso a Internet.

![Dispositivos electrónicos para el colegio](/images/uploads/huawei-matepad-t-8-tablet-de-8-con-pantalla-hd.jpg "HUAWEI MatePad T 8 - Tablet de 8\\\\" con pantalla HD ")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/HUAWEI-MatePad-procesador-Octa-core-Operativo/dp/B088XP47WW/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&dchild=1&keywords=tablet+escolar&qid=1630420088&sr=8-4&madrescabread-21) (enlace afiliado)

## **Regreso al cole en condiciones especiales**

El regreso al cole es una época de mucha emoción para los niños. Hacer amigos, conocer la escuela, estrenar útiles, etc. Sin embargo, actualmente **el regreso se hace con mucha cautela,** por eso te ofrezco otros consejos para que el regreso sea seguro.

### **Gel Hidroalcoholico**

Es de suma importancia enseñar a los niños a lavarse las manos con regularidad, pero el **uso de alcohol puede ayudar a desinfectar algunos objetos** en el salón de clases o desinfectar sus manos con más regularidad

### **Mascarillas**

Las mascarillas o cubrebocas se han convertido en una herramienta adicional que debes comprar y reponer cada mes. Lo mejor para ahorrar dinero es **comprar paquetes de 50 cubrebocas**, pues el precio es inferior que las compras por unidad.

Antes que nada, debes cerciorarte de que las mascarillas sean de calidad y por supuesto sean cómodas.

![Máscarillas Para Niños para el colegio](/images/uploads/mascarillas-para-ninos-con-certificado-ce.jpg "Máscarillas Para Niños con Certificado CE")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Máscarillas-Certificado-Selladas-Paquetes-Piezas/dp/B08Y8Y6YZH/ref=bmx_dp_vd7vanbn_6/259-0761627-6283609?pd_rd_w=lcYTM&pf_rd_p=e8dfd2a1-b6a1-4831-97fd-e355f5e6dab9&pf_rd_r=C5CTVX7T0J23WS9N74T5&pd_rd_r=5114788d-9ae3-4475-840d-be2188f377af&pd_rd_wg=Cy6Qk&pd_rd_i=B08Y8Y6YZH&psc=1&madrescabread-21) (enlace afiliado)

En conclusión, el regreso al cole conlleva algunos gastos; sea que se hagan las clases a distancia o de forma presencial. Sin embargo, si haces **compras inteligentes**, podrás ahorrar mucho dinero.

*Photo Portada by FatCamera on Istockphoto*