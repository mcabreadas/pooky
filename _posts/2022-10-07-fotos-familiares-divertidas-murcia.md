---
layout: post
slug: fotos-familiares-divertidas-murcia
title: Podcast Asómate a 43 historias de crianza con el libro de la fotógrafa
  Marta Ahijado
date: 2022-10-07T08:49:28.599Z
image: /images/uploads/yellow-black-and-purple-modern-podcast-cover-800-800-px-1200-800-px-1200-800-px-.png
author: .
tags:
  - podcast
---
Hoy te abro un mundo de empatía y consciencia a través de la fotografía familiar que capta, más que momentos, vivencias de crianza, almas, sensaciones más allá de lo evidente.

Te presento a la murciana Marta Ahijado, eligió dedicarse a fotografiar familias al aire libre o en sus casas de forma respetuosa y lo más natural posible.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3IOmpr9qiol88hUH6m5Wpu?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Marta utiliza el juego y en la conexión entre los miembros de las familias para conseguir fotos espontáneas y que reflejen la esencia de cada una. Cree que es posible fotografiar a las familias tal cual son, sin poses, respetando los ritmos y tiempos de cada integrante, incluyendo los recién nacidos. Y, aunque las fotos en las que todas las personas están perfectamente puestas sonriendo a cámara son bonitas, a ella me gustan más aquellas en las que se transmiten emociones y momentos, porque estas son las que nos ayudarán a recordar cómo nos sentíamos una vez pasen los años. 

Actualmente esta llevando a cabo una [campaña crowdfunding para recoger fondos para poder imprimir mi libro Asómate](Asómate), donde hace una recopilación de sus fotografías familiares junto a relatos escritos por ella misma en los que cuenta 43 momentos que pueden suceder en la vida de cualquier familia. 

Quedan pocos días de crowdfunding (termina el 14 de octubre) y vamos por el 70% financiado. 

## Recomendaciones de este espisodio

\-[Instagram de Marta Ahijado](https://www.instagram.com/marta.ahijado/)

\-[Libro Asómate: para sentir y entender la profundidad de 43 momentos familiares](Asómate)