---
layout: post
slug: clases-virtuales-con-menores-de-edad
title: Todo lo que necesitas saber sobre las clases virtuales con menores de edad
date: 2022-08-31T10:24:39.412Z
image: /images/uploads/clases-virtuales-con-menores-de-edad.jpg
author: faby
tags:
  - educacion
  - derecho
---
La contingencia de la Covid -19 cambió muchos aspectos de la vida cotidiana, uno de ellos es la educación. **La nueva modalidad de enseñanza llegó para quedarse.** En la actualidad las aulas virtuales son ocupadas por alumnos de todas las edades, lo que incluye menores de edad.

En esta entrada hablaremos de los beneficios de este tipo de modalidad de enseñanza, así como los **factores legales que debes tener presente en este tipo de educación** con menores de edad.

## Protección de menores en Internet

![Protección de menores en Internet](/images/uploads/peligros-internet-ninos-.jpg "Peligros en Internet para los niños ")

Las clases virtuales iniciaron como un medio de emergencia. A pesar de ello, no hay que olvidar que **el mundo virtual es peligroso.** En tal sentido, conviene conocer los *[aspectos legales de las clases online con menores en Internet](https://www.granviaabogados.com/blog/aspectos-legales-de-las-clases-online-con-menores-en-internet/)*. ¿No los conoces? A continuación, mencionaremos algunos aspectos relevantes:

*  Los padres deben dar consentimiento expreso de las clases virtuales.
* Hay ciertas metodologías de clase que deben consultarse a los representantes, como por ejemplo las grabaciones en los proyectos educativos.
* En las clases no se debe revelar información personal de los alumnos, como su dirección o nombre completo.

## ¿Puede el profesor grabar a los estudiantes en las clases virtuales?

![Grabar a los estudiantes en las clases virtuales](/images/uploads/profesor-en-clases-or-videoconferencia.jpg "Profesor en clases por videoconferencia")

El profesor tiene la autoridad para grabar el desempeño educativo de los alumnos, como por ejemplo un examen. Ahora bien, según la Agencia Española de Protección de Datos, estas imágenes deben ser estrictamente para uso educativo, y nunca deben publicarse en ningún medio de difusión.

**Lo mejor es pedir la autorización de los padres para realizar vídeos de los alumnos.** Al mismo tiempo, se debe explicar a los representantes qué uso se le dará a las imágenes, dónde se guardarán y por supuesto qué uso se le dará luego.

**El derecho a la protección de datos no queda anulado con las clases virtuales,** más bien, se debe ejercer mayor cuidado, ya que los medios digitales tienen mayor margen de difusión. De tal forma, que si un padre decide no dar permiso para grabaciones de proyectos estudiantiles, se le debe respetar y proveer otro medio de evaluación, como por ejemplo un trabajo escrito.

## Ventajas de la educación virtual

![Ventajas de la educación virtual](/images/uploads/ventajas-de-la-educacion-virtual.jpg "Ventajas de la educación virtual")

Los beneficios de la educación virtual son muy amplios:

* **Accesibilidad y comodidad.** Es mucho más cómodo estudiar desde casa. Se puede acceder en Smartphone, Tablet, ordenador, etc.
* **Ahorro de tiempo.** No es necesario desplazarse a algún sitio, se ahorra tiempo de tráfico.
* **Clases dinámicas.** Se usan materiales didácticos, vídeos, simulaciones, lo que permite que las clases sean más entretenidas para los peques.

## Desventajas de las clases virtuales

![Desventajas de las clases virtuales](/images/uploads/desventajas-de-las-clases-virtuales.jpg "Desventajas de las clases virtuales")

Estudiar desde casa también tiene desventajas:

* **Mayor distracción**. Es necesario adecuar un lugar de estudio para que el niño no se distraiga.
* **No socializan tanto.** La verdad es que este medio no permite que los alumnos socialicen entre ellos, lo que puede ocasionar sentimientos negativos, y debemos preocuparnos si un dia observamos cierto aislamiento o que [tu hijo no se relaciona tanto con sus amigos](https://madrescabreadas.com/2021/01/21/hijo-adolescente-no-tiene-amigos/).

En conclusión, aunque las clases virtuales son un excelente medio de aprendizaje, también tiene sus retos. La verdad es que las clases a distancia son excelentes para implementar cursos de idiomas o para reforzar el aprendizaje en alguna asignatura.