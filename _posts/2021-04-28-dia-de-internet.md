---
author: luis
date: 2021-05-17 10:19:16.432000
image: /images/uploads/portadadiainternet.jpg
layout: post
slug: dia-de-internet
tags:
- educacion
- crianza
title: ¿Qué se celebra el Día de Internet?
---

¿Os imagináis como era el mundo antes de Internet? Esa pregunta seguramente se la hagan nuestros hijos o nietos y es que ellos han nacido con esta herramienta a su alcance desde que nacieron. Esto dice mucho de cómo Internet cambió la vida de la mayor parte del mundo. Ha cambiado nuestra manera de buscar información, de trabajar, comprar e incluso de relacionarnos, como ocurre en las redes sociales.

Hace poco hablamos del Dia de Internet Segura y dimos unos [consejos muy interesantes par proteger a nuestros hijos en el uso de Internet](https://madrescabreadas.com/2021/02/07/dia-de-internet-segura-2021/)

Estoy convencido de que de aquí a un tiempo, la invención de Internet será un hito que marque la cronología de nuestra historia porque la era de las comunicaciones ha supuesto una verdadera revolución. 

Es por eso que este invento merecía tener un día especial en nuestro calendario y en este artículo te contaré qué se celebra, cómo, cuándo y qué es el día de Internet.

## ¿Cuándo se celebra el día de Internet?

El día de Internet, o Día Mundial de las Telecomunicaciones y de la Sociedad de la Información, se celebró por primera vez hace ya, casi, 16 años, en octubre de 2005, y fue una iniciativa llevaba a cabo por la Asociación de Usuarios de Internet de España, a la que se unieron otras muchas asociaciones españolas que encontraron interesante celebrar esta fecha. 

En Latinoamérica la idea también fue muy bien acogida, vinculándose diversas asociaciones de usuarios de Internet de distintos países como Colombia, México, Argentina, Chile o Ecuador, entre otras.

![dia de internet](/images/uploads/internet.jpg "dia de internet")

Pero en la actualidad nada queda de aquella fecha, porque la seleccionada es el 17 de mayo. En la Cumbre de la Sociedad de la Información que se celebró en Túnez durante ese mismo año, los participantes decidieron esta fecha, la cual fue propuesta a la ONU quién dio el visto bueno. Desde entonces, cada 17 de mayo, se celebra el día de Internet en diversos países del mundo.

La fecha del 17 de mayo no está escogida al azar sino que fue un guiño por ser la fecha del aniversario de la firma del primer Convenio Telegráfico Internacional y de la creación de la UIT (Unión Internacional de Telecomunicaciones).

## ¿Qué es el día de Internet y por qué se celebra?

La importancia que tiene Internet en nuestro día a día merece un reconocimiento, también mayor visibilidad porque aún hay un sector de la población reacia a las nuevas tecnologías y las comunicaciones. 

Aqui os dejamos una serie de [consejos para usar de forma segura juguetes conectados a Internet](https://madrescabreadas.com/2018/12/13/juguetes-tecnologicos-conectados-internet/).

Sin duda, si este día ha sido fijado es porque tanto la ONU como la Asociación de Usuarios de Internet quieren resaltar el poder de la red para cambiar todo el panorama social de un país. Además, reivindicar cómo a través del uso de las tecnologías de información y comunicación se puede hacer posible este cambio.

![dia internacional internet](/images/uploads/diainternet.jpg "dia internacional internet")

No se trata del único día dedicado a Internet ya que el 6 de febrero también se celebra el Día del Internet Seguro, o el 23 de agosto que es el Día del Internauta.

Pero, ¿Cómo se celebra el Día de Internet? Pues lo cierto es que durante este día, alrededor del mundo hay cientos de actividades relacionadas con la temática como charlas, eventos, concursos, etc.

Con esto acabamos todo lo que teníamos que contaros sobre el Día de Internet y esperamos que os haya gustado toda esta información tan interesante sobre este festejo y por qué se celebra. 

Comparte y suscribete!



Ms informacion sobre [eventos en el dia de Inernet, aqui](https://www.diadeinternet.org/2021/?)

*Foto de Portada by Thomas Jensen on Unsplash*

*Photo by Benjamin Dada on Unsplash*

*Photo by Vlad Rosh on Unsplash*