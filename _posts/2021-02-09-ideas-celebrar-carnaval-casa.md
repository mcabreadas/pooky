---
author: ana
date: '2021-02-09T10:10:29+02:00'
image: /images/posts/ideas-para-celebrar-carnaval-en-casa/p-disfraz-de-reciclaje.png
layout: post
tags:
- trucos
title: Ideas para celebrar carnaval en casa
---

El carnaval es de las fiestas del año la más divertida, colorida y alegre. Pero este **carnaval 2021** tiene una particularidad: será en casa. Tenemos el reto de hacerlo singular como siempre y poner la casa a tono con la celebración, vestirnos de carnaval o mejor, disfrazarnos.

¿Qué sucede en carnaval? Pues que el mundo queda patas arriba, lo que era una cosa, ahora es otra. Los roles se invierten o divierten: Mamá y papá pueden ser ahora los niños de la casa, y los niños, asumir algunas responsabilidades festivas, como por ejemplo: cocinar. Un pastel hecho por los niños con apenas ayuda, puede resultar sencillo y delicioso para acompañar la celebración.

## Actividades de carnaval para niños

### Disfraces caseros para carnaval

Los disfraces ponen sin duda el ingrediente principal. Rebuscar entre la ropa que hemos acumulado a lo largo de los años e inventar arreglos y disposiciones para adaptar papeles o roles. Con toda seguridad algo original saldrá de las combinaciones.
<iframe src="https://assets.pinterest.com/ext/embed.html?id=7318418134711480" height="621" width="345" frameborder="0" scrolling="no" ></iframe>

<iframe src="https://assets.pinterest.com/ext/embed.html?id=140806225642918" height="673" width="345" frameborder="0" scrolling="no" ></iframe>



### Manualidades de marcaras para carnaval

Dediquémonos en familia a la confección de máscaras y a la decoración de los espacios, con telas, serpentinas y globos, banderines y cintas. Color y confeti son las marcas clásicas del carnaval, rebusquemos en casa hasta dar con los ingredientes para vestir los espacios de vitalidad y energía.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=1337074875328244" height="589" width="345" frameborder="0" scrolling="no" ></iframe>



### Decoremos la casa para carnaval

Y si a los disfraces unimos un ambiente temático, de jungla o isla desierta... O bien, un restaurante o café elegante, como de otra época; en fin, soñar que somos personajes de un tiempo o un lugar exótico. El carnaval tiene todo de fantasía, de juego y algo de la comicidad del glamour.

Os dejamos varias ideas de fiestas temáticas que hemos hecho en casa para que os sirvan de inspiración.

[Decoración fiesta juego World of Warcraft (WOW)](https://madrescabreadas.com/2015/09/18/fiesta-warcraft-wow/)

[Decoración fiesta Minecraft](https://madrescabreadas.com/2017/06/11/decoración-fiesta-infantil-minecraft/)

[Decoración fiesta Star Wars](https://madrescabreadas.com/2016/05/02/cumpleanos-star-wars/)

### Pintacaras para carnaval

Los disfraces los podemos complementar con máscaras, maquillaje o pintacaras. Por supuesto, tendríamos que tener a mano las pinturas adecuadas, hipoalergénicas.

Éstas pinturas para la cara de Klutz las podemos recomendar porque las hemos usado, y dan un resultado espectacular:

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=159174430X&linkId=8ad519d7f17db01645eab6d1912f1108&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

## Actividades de carnaval para adolescentes

### Volvamos a los ´90 en carnaval

Ambientar la casa en una década particular, por ejemplo los '90 los '80 si son fans de las series Riverdale o Strangers Things, por ejemplo, puede motivarlos mucho, al mismo tiempo de hacer la delicia de los más grandes. Lo logramos con música de esa época y objetos del museo familiar, adornando un set de la casa con sus recuerdos o reviviéndolos con anécdotas incluso fotografías de las "pintas" que llevábamos entonces . Claro que no pueden faltar atuendos y accesorios, pues aquí la imaginación y la creatividad van de la mano.


### Photocall casero para carnaval

Y para generar imágenes inolvidables, qué tal si construimos un escenario para fotografías, enmarcado en un tema carnavalesco. Armar un photocall y disponer de antifaces, collares, pelucas, sombreros, gafas, en fin, de todo lo que podamos incorporar para lograr unas fotos impactantes y divertidas.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=62909726032378526" height="553" width="345" frameborder="0" scrolling="no" ></iframe>



### Una gymkhana para carnaval

La gymkhana resulta ideal para jugar en el jardín e incluso dentro de casa, incorporando a los retos los distintos espacios. O jugar a esconder algo y premiar al detective más perspicaz, son **ideas divertidas para carnaval** que podemos barajar para hacer vivir una experiencia familiar única.

### Concurso de decoración de puertas de carnaval

Finalmente, una propuesta que podemos poner en práctica es el concurso de la puerta carnestolenda. Hagamos equipos, escojamos la puerta a decorar, y despleguemos todos los recursos y la imaginación para producir la más vistosa de este **carnaval 2021**. Y para que la votación no se quede en casa, pasemos las fotos a la familia conectada y que sea ella la que decida.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=5066618318435367" height="560" width="345" frameborder="0" scrolling="no" ></iframe>



Ganaremos todos con esas puertas hechas a modo de ventanas abiertas al mundo y a la esperanza, en esta **fecha de carnaval 2021** lleno de color, salud y alegría en casa.

Si te ha servido de ayuda puedes ayudarme tú a mí compartiendo en tus redes y suscribiéndote al blog para no perderte las novedades (no mandamos spam)


*Photo de portada by Jessica Rockowitz on Unsplash*