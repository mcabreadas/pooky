---
layout: post
slug: como-evitar-la-violencia-de-genero
title: Las 5 claves para explicar la violencia de género a nuestros hijos e hijas
date: 2021-11-23T17:20:44.218Z
image: /images/uploads/portada-violencia-genero-ninos.jpg
author: luis
tags:
  - educacion
---
Crecer es sinónimo de dejar atrás esa inocencia que caracteriza a los más pequeños y darse cuenta de que la vida ni es tan fácil ni es tan bonita como habíamos imaginado a nuestra corta edad. Uno de los grandes problemas a los que nos enfrentamos como humanidad es el de la Violencia de Género, un problema que no solo ocurre entre la población adulta, sino que cada vez existen más casos de chicas adolescentes que sufren de este tipo de violencia, en muchos casos sin saber que están siendo víctimas de ella. Pero, ¿cómo explicar la violencia de género a los niños?

Desde luego que es un tema muy delicado de tratar, más cuando queremos hacerlo a edades demasiado tempranas. Lo que sí es cierto es que no deberíamos convertirlo en un tema tabú, al igual que el [abuso sexual a menores](https://madrescabreadas.com/2013/11/19/prevención-del-abuso-sexual-nfantil-juego-del/), ya que la educación en valores, la comunicación y ser un claro ejemplo de ello, son las claves para inculcar ese respeto y el rechazo a todo tipo de violencia entre los jóvenes.

![que es violencia de genero](/images/uploads/que-es-violencia-de-genero.jpg "que es violencia de genero")



## Cmo explicar a nuestros hijos la violencia de género

### 1-Educacion en respeto y tolerancia

Generalmente, desde las instituciones públicas se siguen una serie de indicaciones a la hora de que en escuelas e institutos se impartan valores hacia la no violencia contra las personas. No obstante, esto es algo que debería de suceder desde el nacimiento del bebé ya que,

>  un niño educado en el respeto y la tolerancia a sus iguales, rechazará cualquier tipo de violencia, incluida la de género

Esa enseñanza, también **le ayudará a identificar situaciones que implican violenci**a contra su integridad como persona, ya sea por razones de sexo, aspecto, personalidad, etc, y son más propensos a defender a aquellos que sufren este tipo de acosos.

![explicar violencia de genero a niños](/images/uploads/explicar-violencia-genero-a-ninos.jpg "explicar violencia de genero a niños")

### 2-El ejemplo que damos en casa es clave

El ejemplo en el entorno familiar es clave para que un niño, el día de mañana, sea un adulto que no tolera situaciones de violencia. Lo cierto es que en nuestra sociedad hay veces en las que somos tremendamente permisivos con actos de violencia o se justifican con el clásico: era una broma o son cosas de niños. 

### 3-Limites muy claros

Hay que ser muy claros con los límites y esta es una de las pautas preventivas sobre la violencia de género que deben inculcarse desde la niñez.
A los más pequeños hay que enseñarles a elegir bien de quiénes se rodean, eligiendo a aquellos que nos tratan y nos hacen sentir bien. Además, deberíamos inculcarle a nuestros hijos que se expresen libremente y digan qué aprueban y que rechazan. Frases como: 

> No me gusta que me hagas esto 
>
>  Me molesta esto otro que me estás diciendo
>
>

para que así, desde bien pequeños, sepan poner los límites de los comportamientos que se consideran violentos o que no les hacen sentir bien.

### 4-Comunicacion y empatia

Para poder explicar todo esto hay que fomentar la comunicación con los niños, escucharles desde la empatía y el respeto y que siempre tengan presente que estarás ahí para escuchar cualquier inquietud o problema que le puede surgir. Por otra parte, si animamos a los pequeños y jóvenes a rechazar y que reconozcan aquellas conductas violentas, hará que estos se conviertan en personas activas ante estos comportamientos.

### 5-Usar una situacion cotidiana como excusa para explicarles

Si queremos explicar a los niños qué es la violencia de género, podemos usar como excusa cualquier situación que vivimos en el día a día y que nos llegan a través de medios de comunicación, Internet, los grupos de amigos, publicidad, videojuegos, etc. En muchas ocasiones, el problema no está en lo que ven sino en que lo ven solos y sin tener una persona adulta que les ayude a canalizar los mensajes y analizarlos.

El escenario es fácil de plantear, lo único que debemos hacer es estar preparados para mantener esta charla y saber los puntos que hay que tocar para que quede claro cómo explicar la violencia de género a los niños. Con estas pautas puede que sea mucho más sencillo y te recordamos que nunca olvides la premisa de que somos un ejemplo para ellos (de nada sirve una charla si luego ve comportamientos que chocan contra aquello que explicas) y de que es importante aportar al niño valores femeninos y masculinos positivos.



## ¿Cuántas mujeres han muerto por violencia de género?

Cuando un país tiene que comenzar a legislar sobre violencia de género significa que el problema se está tornando en algo serio. Hay quiénes se preguntan aún por qué no se considera violencia de género las agresiones de mujeres a hombres y la respuesta está en los números. En lo que llevamos de año, la cifra de muertes por violencia de género en 2021 en España alcanza 37 víctimas mortales a manos de sus parejas o ex parejas, una cifra que es algo más baja que las de años anteriores en estas mismas fechas. Las estadísticas de violencia de género de 2020 para noviembre eran de 41 víctimas, o las 54 que se contabilizaron en el año 2019.

Otro de los datos sobre violencia de género 2021 que resulta muy esclarecedor es que de esas 37 víctimas que a día de hoy se contabilizan en esas estadísticas, 29 de ellas jamás habían presentado denuncia previa contra su asesino. Esto significa que, casi el 80% de las mujeres asesinadas en este año, llevaban ese calvario en el más absoluto silencio y con la más absoluta vergüenza. 

Estos datos, reflejan aún más la importancia que existe en educar en valores y en definir las pautas que les llevarán a que los jóvenes rechacen no solo la violencia de género, sino todo tipo de violencia en general.

## ¿Cuándo es violencia de género?

Antes de daros algunas pautas para explicar qué es la violencia de género para niños, tiene que quedarnos muy claro a qué se le denomina violencia de género en nuestro país y qué tipo de violencias entran dentro de esta catalogación. 

Como decimos, la violencia de género hace referencia a aquella que es ejercida por parte de los hombres contra las mujeres, ya sean o hayan sido sus cónyuges o de quienes hayan estado o estuvieran unidos a ellas por relaciones de análoga afectividad, aún sin que existiera convivencia entre ellos al momento que se produjeron los hechos.

Además, la ley también deja muy claro que una persona será considerada víctima de violencia de género cuando sea objeto no solo de la violencia física sino también de la psicológica, incluidas las agresiones a la libertad sexual, coacciones, privación de libertad arbitraria o amenazas. La violencia física es aquella que comprende que el hombre cause daños en el cuerpo de la mujer a través de bofetadas, palizas, golpes, heridas, etc. 

En cuanto a la violencia psíquica, comprende tanto actos como conductas que puedan llevar a la desvaloración y el sufrimiento de la mujer, tales como humillaciones, exigir obediencia, insultos, amenazas, etc. Finalmente, la violencia sexual siempre se produce al imponer a una mujer que manteja relaciones sexuales en contra de su voluntad.

La violencia de género está legislada en nuestro país a través del artículo 1º de la [Ley Orgánica 1/2004 de Medidas de Protección Integral contra la Violencia de Género](https://www.boe.es/buscar/act.php?id=BOE-A-2004-21760). En esta ley se establecen las medidas de protección integral cuya finalidad es la de prevenir, sancionar y erradicar la violencia machista y prestar asistencia a quienes la han tenido que sufrir. En esta se fundamenta el resto de normas que están siendo pulidas a lo largo que pasan los años y que luego se ratifican en el pacto de Estado contra la Violencia de Género.

*Portada by Sydney Sims on Unsplash*

*Photo by Mika Baumeister on Unsplash*

*Photo by Sinitta Leunen on Unsplash*