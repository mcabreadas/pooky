---
author: maria
date: '2020-11-17T17:19:29+02:00'
image: /images/posts/flauta-anti-covid/p-flautas.jpg
layout: post
tags:
- educacion
- invitado
- crianza
title: Flauta anti covid
---

> Esta semana tenemos nuestro primer ~~valiente~~ invitado varón en la sección del blog ["Merendando con..."](https://madrescabreadas.com/merendando-con), que ya sabéis que sirve para daros voz, y que tan buena acogida está teniendo. 
> 
> Muchas estábais dando gracias porque ya no sonaban las flautas en vuestras casas mientras los niños practicaban las canciones que aprendían en clase de música... Os jactábais de que si algo bueno tenía el COVID es que los niños ya no tocaban la flauta, y que vuestros oídos bla, bla, bla... Os las pintabais muy felices hasta que apareció Nestor y su invento... La flauta Anti-Covid!
> 
> No quiero ni un lamento por vuestra parte! No os habéis parado a pensar que si pierden un curso de flauta cuando la retomen va a ser peor? Es mejor que sigan practicando! Que no pierdan ritmo!
> 
> Así que, hale! Ponte un chocolate, un té o un café, y únete a nuestra merienda, que te voy a presentar a este inventor y profesor de música murciano que está dispuesto a devolver la flauta a las aulas.
> 
> Su nombre es Néstor de San Lázaro Rubio, es profesor de Música en el IES Ramón Arcas Meca de Lorca y ha patentado un sistema "anti covid" para poder tocar la flauta dulce de forma segura dentro de las aulas de Música en época de pandemia.

![nestor flauta anti covid](/images/posts/flauta-anti-covid/nestor.jpg)

## ¿Cómo surge la idea de la flauta "anti COVID"?

El invento surge debido a la necesidad de solucionar un problema que se atisbaba ya a comienzos del verano de 2020, cuando se recomendaba no utilizar la flauta dulce en los colegios e institutos debido a la propagación de aerosoles provenientes del sistema respiratorio del alumnado y la profesora o el profesor.

## ¿Cómo funciona la flauta "anti Covid"?

Se trata deán adaptador que conecta la flauta a un inflador externo de pie, a modo de “aparato respiratorio” externo y libre de estos aerosoles. Tanto las flautas como este adaptador para el inflador, son de uso individual y el **alumnado puede interpretar las piezas musicales manteniendo la mascarilla** de seguridad correctamente colocada. 

El invento es sencillo, será económico, accesible a todo el mundo, pequeño, ligero y resistente, todo lo que se puede exigir a una solución para todo el alumnado de este país. 

Debido al distanciamiento social y las restricciones que se han de cumplir, actualmente en los centros educativos se escucha ese incómodo silencio que nos ha dejado la flauta dulce, con sus seguidores, detractores y miles de anécdotas que nos han acompañado estos años alrededor de este pequeño instrumento.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yMThPU18hso" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## ¿Crees que la flauta anti COVID debería llegar a todos los colegios?

El viernes 13 de noviembre el IES Ramón Arcas Meca de Lorca se convirtió en un centro pionero a nivel nacional al ser el primer centro de España en restablecer el uso normalizado de la flauta dulce dentro de las aulas, gracias a este sistema "anti covid". El siguiente paso es compartir y hacer posible esta vuelta de la Música a las aulas en todos los colegios e institutos que deseen contar con esta "medalla", símbolo de una pequeña batalla ganada al COVID-19 con una de las armas más nobles, la música.

## ¿Crees que la asignatura de música se queda coja sin la flauta?

La flauta se ha convertido, desde hace varias décadas, en el instrumento estrella dentro de los colegios e institutos de este país: es económica, fácil de tocar, de uso personal, pequeña, duradera, no necesita mantenimiento y acerca la música a todo tipo de alumnado desde su versión más característica y emotiva, que es la melodía. Pocos instrumentos reúnen todas estas características. Sin duda alguna, la Música se queda coja sin este instrumento.

## ¿Crees que es importante que los niños desarrollen las facultades que la asignatura de música les proporciona?

Es una disciplina artística, esta característica ya la hace indispensable dentro de la educación de nuestras hijas e hijos. Además, favorece la concentración, la cooperación, el trabajo en equipo, la coordinación, la estimulación de la motricidad fina, estimulación ante el hecho artístico, el uso equilibrado de los dos hemisferios del cerebro, la precisión, el autocontrol... 

En música todo esto sucede de una manera coordinada y muy precisa sobre el tiempo, numerosos psicólogos la comparan con un deporte de élite.

## ¿Cómo se pueden adquirir la flauta "anti COVID"?

El primer paso es la demanda. Estamos inmersos en el proceso de hacer llegar algunos prototipos gratuitos de muestra a todos aquellos centros que lo soliciten, siempre dentro de nuestras posibilidades. Cuento con muy poco apoyo. 

El verdadero apoyo, el necesario y el indispensable, es la demanda por parte de los centros educativos; ésta es la única llave con la que contamos en la actualidad para encender los procesos de fabricación y distribución.

## ¿Cuánto cuesta un adaptador para flauta "anti COVID"?

Los prototipos los vamos a entregar de forma gratuita. Mi objetivo es que tenga un precio asequible para que sea accesible para todo el alumnado. Al no haber tenido todavía la oportunidad de negociar (aunque sí contactos) con las empresas encargadas de la distribución y fabricación no puedo mencionar precio, sería una osadía por mi parte. 

Trabajamos a diario con alumnado que cuenta con diferentes situaciones sociales económicas y sociales y somos muy conscientes de qué es un precio accesible y qué es un precio diferenciador para este adaptador.

![flauta-anti-covid](/images/posts/flauta-anti-covid/flauta-anti-covid.jpg)

## ¿Hay colegios que ya lo están usando?

Esta semana estamos felices de poder contar que varios centros de la Región han aceptado estas muestras gratuitas para empezar a probar. Increíble, pero cierto: la pandemia ha cambiado muchos hábitos en nuestras vidas, lo que en septiembre era un jarro de agua fría (no poder utilizar la flauta en las aulas) dos meses después se convierte en una barrera que romper (devolver la flauta al lugar que tiene que ocupar).


## Para pedidos de flauta "anti COVID"

nestorsanlazaro@hotmail.com

También podéis encontrar a [Néstor en Twitter](https://twitter.com/nestorsanlazaro), [Facebook](https://www.facebook.com/flauta.anticovid.9) e [Instagram](https://www.instagram.com/nestordesanlazaro/).


Photo portada by Dorothee Kraemer on Unsplash