---
author: ana
date: '2021-02-05T10:10:29+02:00'
image: /images/posts/como-conocer-mas-a-tu-hijo-adolescente/p-hijos-adolescentes.png
layout: post
tags:
- adolescencia
- crianza
title: Cómo conocer más a tu hijo adolescente
---

Muchos cambios biológicos, psíquicos y sociales ocurren en el interior de nuestros **hijos adolescentes** y con frecuencia la comunicación se pierde tras penosos muros o abismos. Es ahora, cuando en ciertos momentos manifiestan con un portazo que nos quieren lejos, cuando más cerca debemos estar.

## Cómo hablar para que los adolescentes te escuchen

Todo nuestro amor se debe manifestar en esta etapa crucial por la que atraviesan. Si estamos acostumbrados a hablar y aleccionar, ha llegado el momento de escuchar más y de interpretar silencios y gestos. En los adolescentes todo es significativo y cualquier cosa, por pequeña que sea, cobra dimensiones inusitadas.  

Nos permitimos recomendaros el libro "Hijos que callan, gestos que hablan: Lo que los adolescentes dicen sin palabras", de Susana Fuster. 
    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8467052929&linkId=d8c02ddb134d89db015bd0d9f3d48f99&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

A pesar de la distancia autoimpuesta, del encierro en la habitación y en las pantallas, o la inmersión en los auriculares, tenemos que estar cerca. Organicemos encuentros con sus amigos en casa, comidas sobre todo, y con jovialidad, entremos en el círculo de sus conversaciones. Ofrezcamos una perspectiva fresca e interesante sobre algunos temas, pero sobre todo, manifestemos curiosidad sincera sobre sus intereses y preocupaciones.

![incertidumbre](/images/posts/como-conocer-mas-a-tu-hijo-adolescente/incertidumbre.png)


## Las 5 claves para conocer más a tu hijo adolescente

El adolescente está jugando en serio a ser adulto y a saber lo que hace. Tenemos que facilitarle espacios para su desarrollo, cuidar sus movimientos y estar ahí para levantarle el ánimo o alzarlo en hombros. Nosotros sabemos que a su edad ningún error es definitivo, por ello, muy responsables seremos de la dimensión que pueda adquirir un traspiés. A la hora de preguntarnos **cómo tratar a un adolescente** no olvidemos que también lo fuimos.

### El círculo de amigos de los adolescentes

En cuanto a la opinión o el consejo sobre temas que les preocupan, es importante saber que no seremos la primera opción. El entorno ejerce una poderosa influencia y lo que recomendamos, aunque coincida con el consejo de algún amigo, será desestimado de antemano por su procedencia.

Os recomendamos este post sobre el [lenguaje de los adolescentes
](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/).

### Reafirmación de los adolescentes

Debemos entender que nuestro hijo adolescente necesita reafirmarse. Está en plena construcción de su personalidad, y necesita sentir que es quien lleva las riendas y que sabe lo que hace. No está en él planificar escenarios a largo plazo, no ha madurado lo suficiente para ello. Por eso nos toca acompañarlo para que en los tropiezos no esté solo y en los éxitos, ser los primeros en abrazarlo.

![amigas](/images/posts/como-conocer-mas-a-tu-hijo-adolescente/amigas.png)

### Riesgos de los adolescentes

La química de los adolescentes los lleva a creerse invulnerables y por ende a asumir riesgos que nos parecen innecesarios. Es importante para su desarrollo que experimenten esta sensación, pero debemos cuidar en lo posible que no se hagan daño. 

### Impulsividad de los adolescentes

Elevemos su autoestima, acompañémoslo en momentos de tristeza, no dejemos que lo venza la soledad y el aislamiento. Con esto evitaremos lo más dañino o peligroso de su natural impulsividad.

### La lentitud del tiempo en la mente de los adolescentes

El tiempo de los adolescentes transcurre más lento que el nuestro. Claro que se trata de una simple percepción, pero mientras a nosotros no nos alcanza el día, para ellos las horas están detenidas. ¿Qué hacer? Entender que eso pasará. Según la neurociencia el cerebro adolescente procesa cantidades enormes de información e imágenes en poco tiempo, lo que hace que las horas transcurran más lentas. 

A la duda de **cómo hablar con un adolescente** para que nos escuche, respondámonos mejor: escuchemos con suma atención todo lo que nos diga. Hagamos a un lado lo que estemos haciendo por importante que sea para no postergar su intención de abrirnos las puertas de su mundo interior. Más que nuestras palabras, nos deben importar mucho las suyas. 

![fuerza-y-estimulo](/images/posts/como-conocer-mas-a-tu-hijo-adolescente/fuerza-y-estimulo.png)

Nuestro hijo adolescente le está dando forma al mundo que lo espera y un sinfín de sensaciones tiene atravesadas en su cuerpo y garganta. Apenas puede hablar y en cambio mucho siente. Sentémonos a su lado a escucharlo aunque musite, y con palabras de apoyo y estímulo, hagamos que su horizonte de desafíos y sueños se expanda. Que crezca a nuestro lado es un regalo y un privilegio. No lo dudéis un instante.

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=tf_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=006084129X&linkId=c19675eb503fb638f4798028f033fa81&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}


_Photo portada by Alexandre Desane on Unsplash_

_Photo by Jesús Rodríguez on Unsplash_

_Photo by Brooke Cagle on Unsplash_

_Photo by adrianna geo on Unsplash_