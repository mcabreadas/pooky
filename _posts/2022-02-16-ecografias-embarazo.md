---
layout: post
slug: ecografías-embarazo
title: Las 3 ecografías obligatorias en el embarazo
date: 2022-02-17T11:12:42.190Z
image: /images/uploads/portada-ecografias-embarazo.jpg
author: luis
tags:
  - embarazo
---
El avance de la ciencia ha ayudado mucho al proceso de gestación y es que gracias a las ecografías durante el embarazo se puede llevar un mejor control del desarrollo del bebé mientras se encuentra en la barriga de la mamá. Para hablar de las ecografías en el embarazo vamos a tener que diferenciar entre las que son fijas, que serían las ecografías de embarazo de la Seguridad social, y las que puedes realizarte de manera privada por tu cuenta, estas suelen ser las ecografías en 3D y 4D.

Pero, ¿sabes cuántas ecografías se hacen durante el embarazo? En este artículo vamos a señalar las ecografías que te deberán realizar durante el periodo de gestación, además de analizar cuáles son las más importantes y qué se ve en cada una de ellas.

## ¿Cuántas ecografías se hacen durante el embarazo?

Las ecografías van a permitir a los profesionales sanitarios poder ver qué ocurre en el interior del útero y esto es gracias a la conversión de las ondas de sonido de alta frecuencia que rebotan en el mismo. 

![cuantas ecografias embarazo](/images/uploads/cuantas-ecografias-hay-en-el-embarazo.jpg "cuantas ecografias embarazo")

Contestando a la pregunta que encabeza esta sección, por norma general se suelen realizar 3 ecografías a lo largo del embarazo, aunque dependiendo del caso pueden ser alguna más. Esto suele ocurrir en embarazos de alto riesgo, cuyo control debe ser más continuado en el tiempo. 

Pero, si este no es tu caso, lo normal será que te citen para tres ecografías que se esparcen por las diferentes etapas del embarazo, teniendo una inicial en el primer periodo, una segunda ya metidos en la segunda etapa y una última en la etapa final. Estas, las veremos mejor en el siguiente apartado.

## ¿Qué ecografías se hacen en el embarazo?

Pues como te íbamos diciendo, aunque a veces se hace tambien una [ecografia a las 4 semanas de embarazo](https://madrescabreadas.com/2021/05/25/ecografia-4-semanas-no-se-ve-nada/), son tres las ecografías que, por norma general, os van a realizar durante el periodo de gestación y cada una de ellas tiene una función lógica. El número también tiene cierto sentido y es que no por hacerte más ecografías un embarazo va a ir mejor, es más, aún hay reticencias a hacer más de estras tres porque el uso de ultrasonidos y monitores fetales de manera continuada tampoco es bueno para el bebé. Por tanto, las ecografías deben realizarse por un motivo médico justificado y no por el placer de ver que el bebé está bien.

Dicho esto, las tres ecografías que se hacen durante el embarazo son:

* **Primera ecografía**: los ginecólogos recomiendan que se realice a partir de la semana 12 a contar desde el último periodo. Por norma general, se realiza entre las semanas 11 y 13 y también es conocida como la ecografía de determinación de fecha ya que en ella se puede calcular la edad gestacional real y se te puede asignar una nueva fecha de parto posible.
* **Segunda ecografía**: tendrá lugar entre las semanas 18 y 20 de embarazo y también se conoce como la ecografía morfológica. Esta es una de las más relevantes y por ello trataremos un solo punto a ella.
* **Tercera ecografía**: la ecografía final tiene lugar entre las semanas 33 y 34 de embarazo y en ella se valorará el crecimiento del feto. Tras esta ecografía, lo siguiente que nos recomendarán será asistir a monitores las semanas antes a la fecha prevista de parto.

## ¿Cuáles son las ecografías más importantes en el embarazo?

![ecografias embarazo](/images/uploads/ecografias-embarazo.jpg "ecografias embarazo")

Aunque todas las ecografías durante el embarazo tienen su importancia, está claro que hay algunas que pueden ponernos más nerviosas porque de su resultado depende, en gran medida, la continuación correcta de la gestación. En primer lugar, como la ecografía más confiable y exacta podríamos decir que es la primera, la realizada durante las semanas 11 y 13, ya que de ella sacaremos la fecha probable de parto y la edad gestacional, pudiéndose medir la longitud céfalo-caudal.

Pero, la ecografía que se realiza a partir de la semana 12 de gestación podría ser considerada la más importante ya que en ella podremos saber el número de fetos o si existe presencia de cromosomopatía, sobre todo del síndrome de down. 

## ¿Qué se ve en la segunda ecografía del embarazo?

La segunda ecografía del embarazo suele ser la más esperada y tanto si te realizarás la ecografía de embarazo por privado como por la seguridad social, esta tendrá lugar entre las semanas 18 y 20 de gestación. Se conoce también por el nombre de [ecografía morfológic](https://www.natalben.com/embarazo-semana-20)a ya que en ella se comienza a apreciar el desarrollo y formación de los órganos, pudiéndose detectar anomalías si las hubiera. 

Ten en cuenta que en estas semanas el desarrollo del feto ya es bastante avanzado y la persona que crece en nuestro interior comienza a desarrollar, prácticamente, todos sus órganos y sistemas. Además, en estas fechas, la cantidad de líquido amniótico es la más óptima para obtener una buena valoración ecográfica.

Aunque en el punto anterior nos hemos centrado en la primera ecografía, puesto que es una de las más preocupantes, lo cierto es que esta segunda también tiene su importancia. En ella se podrá localizar la placenta, ver si la posición fetal es correcta y las medidas del feto. Además, se realiza un minucioso rastreo de la anatomía del feto, desde la cabeza y cara, hasta la columna vertebral, abdomen, sus extremidades o el corazón, en busca de anomalías.

Por otra parte, aunque esto ya es secundario, en esta ecografía es cuando vamos a poder saber con total seguridad el [sexo del bebé](https://madrescabreadas.com/2021/07/21/como-saber-que-voy-a-tener-nino-o-nina/), aunque está claro que lo que más preocupa es que venga sano, este es un aliciente más para esperar con ganas esta segunda ecografía del embarazo.

*Portada by Tai's Captures on Unsplash*