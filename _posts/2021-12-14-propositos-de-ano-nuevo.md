---
layout: post
slug: propositos-de-ano-nuevo-2022
title: Los propósitos de año nuevo que sí cumplirás
date: 2022-01-05T11:56:59.850Z
image: /images/uploads/istockphoto-1194899402-612x612.jpg
author: faby
---
El año 2022 está empezando. ¿Cómo te fue el 2021?, **¿lograste cumplir tus [propósitos para 2021](https://madrescabreadas.com/2021/01/02/propositos-de-ano-nuevo/)?** Pues bien, nuevamente tienes la oportunidad de colocarte metas y cumplirlas, o al menos intentarlo para sentirte motivada a mejorar eso que tienes tantas ganas.

**Cada inicio de año es una nueva oportunidad de comenzar de nuevo.** ¿Qué metas puedes colocarte este año? Y por supuesto, ¿cómo cumplir tus propósitos este año? En esta entrada te daremos algunos consejos para que inicies con buen pie.

## ¿Cuáles son los propósitos de año nuevo?

¡Puedes hacer cambios en tu vida! Demuestra tu entereza al colocarte **metas que hagan del nuevo año un tiempo de renovación,** pero procura ser realista y hacerlo desde el auto conocimiento y tendiendo en cuenta tus circunstancias**.** Aquí te dejo algunas de las mejores ideas de propósitos de año nuevo.

### Beber menos alcohol

A ve, el vermut de los domingos no te lo vayas a quitar, que se trata de mejorar,no de amargarse la vida :)

![vaso de tequila y mano](/images/uploads/istockphoto-657428228-612x612.jpg)

### Pasar más tiempo con la familia

Es necesario **pasar tiempo de calidad con la familia.** Es cierto que el trabajo puede robarnos mucho tiempo, pero hay que esforzarse.

Acercarte tus hijos a veces no es facil, sobretodo si son adolescentes, por eso te dejamos este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que hables su idioma, o al menos lo intentes y le haga gracia.

![familia en manta con paisaje](/images/uploads/istockphoto-1225741727-612x612.jpg)

El [trabajo remoto](https://es.wikipedia.org/wiki/Teletrabajo) puede representar un desafío (lo digo por experiencia propia). A veces pueden extenderse más de lo necesario, por eso, **plantea un horario de trabajo** y desconéctate para que puedas pasar un rato con tus seres queridos.

Te dejamos estos [tips para trabajar en remoto de forma eficiente](https://madrescabreadas.com/2020/09/27/teletrabajo/).

### Perder peso

El tiempo en casa debido a la pandemia ha hecho que muchos de nosotros subamos de peso, pero no te preocupes, si te lo propones **puedes hacer cambios en tu alimentación.**

![cinta metrica y pesas azules](/images/uploads/istockphoto-584491632-612x612.jpg)

Por otra parte, procura ejercitarte. En Internet **hay muchos Gym virtual** en el que puedes hacer incluso cardio sin implementos especiales y dentro de casa.

### Salir de las deudas y ahorrar dinero

![hucha cerdito](/images/uploads/istockphoto-1025332648-612x612.jpg)

Este año ha sido difícil, y ahorrar en enero parace imposible, pero no lo es, mira este post donde te explicamos [como ahorrar en la cuesta de enero](https://madrescabreadas.com/2021/01/16/como-ahorrar-en-la-cuesta-de-enero-post/).

Por eso, propón un **plan de gastos en el que puedas ahorrar dinero.** Habla con tu familia para que todos los miembros estén de acuerdo y se sometan al nuevo plan de vida.

Aqui te explicamoc [trucos para ahorrar en la ropa de los niños](https://madrescabreadas.com/2020/10/04/ahorro-ropa-ninos/).

Un truco puede ser ahorrar cada mes el 10% de lo que ingreses.

### Sentir menos estrés

¿Eres de los que cae en pánico muy rápido? Pues bien, en el nuevo año puedes **proponerte cambiar tus reacciones ante los problemas.**

![](/images/uploads/istockphoto-1196352291-612x612.jpg)

Si tus problemas de estrés son muy fuertes, puedes hablar con un profesional. Una conversacion con psicologo puede quitartemuchas cargas de encima. A veces unpunto de vista diferente es suficientepara sentirnos mejor.

## ¿Cómo cumplir tus propósitos este año?

¿Te sientes frustrado porque nunca cumples tus metas del nuevo año? Aquí te daré algunas recomendaciones para que este 2022 sea diferente.

* **Enfócate en un solo propósito.** Aunque es habitual ponerse varias metas, es mucho mejor concentrarse en una sola, o al menos trabajar en una y luego en otra.
* **Escribe el plan.** Escribe de forma clara tu propósito, pero también coloca algunos pasos que debes hacer hoy mismo. Puedes hacer una planificación todas las semanas.
* **Habla de tu propósito.** Si tus amigos y familiares saben tus metas pueden ayudarte a lograr tu cometido.
* **Sé constante.** No te olvides de tu propósito. Si tienes fallas, no te desanimes, sigue trabajando con perseverancia. Desecha los pensamientos negativos.
* **Se realista**: hazlo desde el autoconocimiento y teniendo en cuenta tus ciscunstancias.

En resumen, para que inicies con buen pie el año nuevo debes plantearte metas claras y realistas. Además, debes ser perseverante. Tienes 12 meses para renovarte. Recuerda que **nunca es tarde para hacer cambios significativos en la vida**.

¡Puedes lograrlo!

Comparte con nosotras tu primer proposito!

Dejanos un comentario, queremos leerte!

*Photo Portada by Marija Jokic on Istockphoto*

*Photo by GeorgeRudy on Istockphoto*

*Photo by skynesher on Istockphoto*

*Photo by MillefloreImages on Istockphoto*

*Photo by marchmeena29 on Istockphoto*

*Photo by FilippoBacci on Istockphoto*