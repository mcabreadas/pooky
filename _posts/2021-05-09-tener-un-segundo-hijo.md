---
author: ana
date: 2021-05-13 07:29:40.716000
image: /images/uploads/photo_2021-05-09_01-08-20.jpg
layout: post
slug: tener-un-segundo-hijo
tags:
- puericultura
- crianza
title: Tener un segundo hijo
---

## ¿Que es mejor tener uno o dos hijos?

Tener un segundo hijo a veces puede caer por sorpresa y a partir de ahí toca replantear todo en casa para hacerle su espacio. Si no es este tu caso, se trata entonces de una decisión que puedes tomar con argumentos si se quiere más claros que con el primero. Ya has pasado por la experiencia de [madre primeriza](https://madrescabreadas.com/2021/04/28/consejos-madre-primeriza/), sabes de qué trata la crianza y cuáles son los cambios que con frecuencia ocurren en el hogar con la llegada del bebé. A veces nos planteamos cual es la mejor edad para tener un segundo hijo porque tener un segundo hijo a los 40 puede llegar a asustarnos, pero recuerda que las ventajas de tener un segundo hijo son que la experiencia es un grado, y que hoy en dia los avences medicos hacen que lo que antes era una excepcion, ahora se ha convertido en algo bastante comun.

Quiza tambien te interese saber las [diferencias entre tener 2 o 3 hijos](https://madrescabreadas.com/2017/03/12/diferencias-tres-hijos/)

## ¿Cuanto esperar para tener un segundo hijo?

Sin embargo, hay algunas recomendaciones que atender en relación al tiempo de [recuperación del sistema reproductor femenino](https://es.wikipedia.org/wiki/Puerperio) para una segunda gestación. Se ha generalizado que en promedio son dos años lo estimado para que cuerpo y mente estén de nuevo aptos para la maternidad. Coincidiendo también con la duración idónea de la lactancia materna y dando un margen para que el primer hijo madure la idea de lo que significará la llegada de un hermano o hermana.

![hermano mayor observando bebe](/images/uploads/photo_2021-05-09_01-08-48.jpg "Ha llegado mi hermano")

## ¿Cuando es mejor tener un segundo hijo?

El segundo hijo responde, casi siempre, al deseo de los padres de ampliar la familia, de ver crecer a los niños acompañados entre hermanos. Ese deseo debería revisarse considerando algunas reflexiones previas que nos permitan evaluar los escenarios atendiendo a ciertas variantes. 

### Tener un segundo hijo y la salud de los padres

A veces nos planteamos si tener un segundo hijo deteriora la salud de los padres. Y es que la salud no debe pasarse por alto, si vas a encargar el segundo hijo, es bueno que ambos se hagan un chequeo corriente. Principalmente indagar sobre la madre, será bueno corroborar con un especialista cómo ha quedado después del primer parto. Así se evitan complicaciones durante el embarazo y se atiende a tiempo lo necesario para estar en las mejores condiciones.

### Situación laboral y económica de la familia al tener un segundo hijo

Evalúa con sinceridad si tienen la capacidad de asumir con responsabilidad los gastos que implican un segundo niño. Si tienen trabajos sólidos con ingresos y ahorros suficientes, pues este no será impedimento. Ya saben, en teoría, a cuánto equivale un bebé en el presupuesto familiar, lo que les permite proyectar el escenario y actuar.

### Tener un segundo hijo y estabilidad sentimental de la pareja

Este punto quizás te parezca muy subjetivo y sí lo es, sólo tú sabes cómo está de fuerte el nexo con tu pareja. Si hay armonía en la relación y ambos quieren emprender el compromiso de un segundo hijo, las condiciones serán seguro las más favorables. 

Si por el contrario la conexión de la pareja pende de un hilo, es importante pensar entonces con cabeza fría para que el segundo hijo no se busque como un pretexto o intento de mejorar o reparar un matrimonio. Sobre todo porque probablemente no será una “solución” y puede convertirse luego en un sentimiento muy penoso e inmerecido. 

![hermanos jugando cama](/images/uploads/photo_2021-05-09_01-08-40.jpg "El segundo hermano, juego y protección")

## ¿Que hacer cuando se tiene un segundo hijo?

Si ya es inminente la decisión, o mejor aún, ya sabeis que vais a tener un segundo hijol o hija, ahora a preparar con ilusión el lugar que dispondrán para su acogida. En esta tarea vale que se involucren con hermanito o hermanita incluido para disfrutar e imaginar cada detalle, esto abonará el camino para que el recibimiento esté lleno de mucho amor.

*Photo de portada by Jonathan Borba on Unsplash*

*Photo by Gabriel Tovar on Unsplash*

*Photo by Nathan Dumlao on Unsplash*