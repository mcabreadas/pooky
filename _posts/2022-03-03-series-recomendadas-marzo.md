---
layout: post
slug: series-recomendadas-marzo
title: Las 5 series que te recomendadas para marzo
date: 2022-03-08T13:39:33.595Z
image: /images/uploads/portada-series-recomendadas-marzo.jpg
author: luis
tags:
  - series
---
El año 2022 viene cargado de novedades en cuanto a series, tanto en nuevas temporadas muy esperadas por el gran público como en estrenos nuevos, que es en lo que más nos vamos a fijar hoy. La proliferación de diferentes plataformas de streaming colaboran en este auge que está viviendo la pequeña pantalla, añadiendo creaciones propias y exclusivas que están dando mucho de qué hablar.

Para este artículo, nos hemos basado en los estrenos que ya están disponibles o que lo van a estar a lo largo del mes de febrero, las cuales podremos disfrutar en marzo con un buen maratón. Toma lápiz y papel porque estamos convencidos que con estas pequeñas reseñas te darán ganas de verlas todas.

## Sospechosos (Apple TV+)

![serie sospechosos](/images/uploads/serie-sospechosos.jpg "serie sospechosos")

Apple TV+ sorprende con un título que, pese a su reciente estreno, ya está dando mucho de qué hablar. La serie de sospechosos ha logrado una gran expectación no solo por su trama, también porque es la primera en la que tenemos a la actriz [Uma Thurman](https://es.wikipedia.org/w/index.php?search=Uma+Thurman&title=Especial:Buscar&profile=advanced&fulltext=1&ns0=1&ns100=1&ns104=1) como protagonista. La serie cuenta con solo 8 capítulos que nos mantendrá enganchado en este thriller producido por la plataforma de la manzana mordida.

En cuanto a la trama, nos narra cómo el hijo de una reputada empresaria norteamericana es secuestrado en un hotel ubicado en New York, recayendo todas las sospechas de manera rápida sobre 5 ciudadanos británicos que se encontraban en el mismo hotel esa noche. En la trama veremos una gran lucha por demostrar su inocencia frente al FBI y la Agencia Británica contra el Crimen. Solo viéndola hasta el final sabrás quién es culpable y quién simplemente estaba en el sitio y en el momento equivocado.

## ¿Quién es Anna? (Netflix)

Uno de los grandes reclamos de Netflix del pasado mes de febrero es ¿Quién es Anna? una serie basada en hechos reales y que llega bajo la dirección de Shonda Rhimes, famosa por ser la creadora de las series Anatomía de Grey y Scandal. A esto se le suma que su protagonista es Julia Gardner, famosa por su papel en la también serie de Netflix Ozark.

La historia gira en torno a una joven periodista que comienza a investigar el caso de Anna Delvey, una supuesta heredera alemana y reina en Instagram que se hizo un hueco en la élite de Nueva York, tanto que algunos acabaron siendo víctimas de sus robos. Pero, ¿quién es realmente Anna? ¿Se trata de lamayor estafadora de todos los tiempos o tan solo una chica que perseguía el gran sueño americano? Todo esto se irá averiguando mientras transcurre la trama, marcada además por la relación de amor y odio entre Anna y la periodista.

## Furia (Filmin)

Filmin es una de las plataformas de streaming que más terreno están ganando en los últimos meses y nos presenta el estreno en exclusiva de este thriller noruego llamado Furia.

Al sumergirnos en su historia conoceremos a Ragna, una agente encubierta que se ha infiltrado en una célula terrorista de extrema derecha que planea una serie de ataques por Europa. No estará sola y es que el exoficial de operaciones Asgeir el cual acaba de adquirir una nueva identidad. Se enfrentarán a una carrera contrarreloj en la que deberán evitar que esta célula ataque durante las elecciones alemanas.

## Vikingos Valhalla (Netflix)

![serie vikingos valhalla](/images/uploads/serier-vikingos-valhalla.jpg "serie vikingos valhalla")

El esperado spin-off de la exitosa serie Vikingos aterriza en Netflix para narrar hechos que transcurren cien años después de lo acaecido en la serie original. No obstante, vamos a encontrar similitudes ya que se va a combinar el drama histórico con la acción propia de un pueblo tan bélico como el de los vikingos.

Esta secuela se ambienta en más de mil años atrás, como a principios del siglo XI y narra las aventuras de algunos vikingos famosos de la historia como pueden ser el explorador Leif Eriksson, su hermana Freydis Eriksdotter o el príncipe nórdico Harald Sigurdsson. Las tensiones entre vikingos y la corona inglesa están a flor de piel y, mientras, los vikingos se enfrentan entre sí por cuestiones religiosas, distinguiéndose un bando cristiano y otro pagano.

## Operación Marea Negra (Amazon Prime Video)

La primera gran ficción española exclusiva de Amazon Prime Video en este nuevo año es Marea Negra, protagonizada por Álex Gonzáles y que también contará con la participación de rostros muy conocidos como Carles Francino, Nerea Barros o Lucía Moniz. 

La serie se sitúa en el tiempo en noviembre de 2019 y es que se basa en la historia real del primer narcosubmarino que fue interceptado en Europa. Se trata de un semisumergible artesanal que se disponía a atravesar el océano atlántico con tres hombres en su interior y 3 toneladas de cocaína. En la trama no faltarán tormentas, averías, peleas, hambre y, como no, un incesante acoso policial para acabar con sus planes.

La edad de la Ira (Atresplayer Premium)

![serie la edad de la ira](/images/uploads/serie-la-edad-de-la-ira.jpg "serie la edad de la ira")

Ambientada en la novela homónima de Nando López, Manu Ríos se pone frente al papel protagonista, acompañado de Amaia Aberasturi, dos de las jóvenes promesas de nuestro país en el mundo de la interpretación. Se trata de una de las llamadas a ser la serie española del año y su estreno será en exclusiva para la plataforma de Antena3, Atresplayer Premium.

En la serie vamos a vivir el brutal asesinato de un hombre cuyo asesino será, presuntamente, su hijo Marcos, un adolescente normal que no parece tener problemas que le llevaran a este hecho. El incidente deja a todos los vecinos conmocionados, sobre todo en el entorno del chico, su instituto, donde la noticia no la pueden entender ni profesores ni alumnos. ¿Cómo un chico tan ejemplar ha acabado cometiendo un crímen tan atroz? 

Si después de estas recomendaciones no te han entrado unas ganas tremendas de hacer unas palomitas y empezar una de estas series, no sé en qué puedo estar fallando. Lo que sí os digo es que muchos de los estrenos que se han presentado en febrero lograrán seguir en boca de todos hasta que terminemos este año 2022.

Si todava no estás suscrito a alguna de las plataformas de series tipo Netflix, HBO, Amazon... , no podrás resistirte por mucho tiempo, así que te recomiendo varias opciones basadas en mi experiencia:

\-Si soléis comprar cosas en Amazon os interesa Amazon Prime Video, porque **compagina ventaja en envíos y compras con series y películas Online**.

Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de **contenido original** y es bastante bueno. 

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21)

\-Oferta para universitarios. Si tienes universitarios en casa, te interesa Amazon Prime Student. ¡Ahora hay una prueba de 90 días! Con esta opción tienes **todas las ventajas de Amazon Prime a mitad de precio**; solo EUR 18,00/año. Eso sí, esta oferta es sólo para estudiantes universitarios.

Puedes [darte de alta en Amazon Prime Student aquí](http://www.amazon.es/joinstudent?tag=ID_de_madrescabread-21)

Comparte para ayudar a mas familias

Portada by Thibault Penin on Unsplash