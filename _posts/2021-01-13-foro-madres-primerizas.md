---
layout: post
slug: foro-madres-primerizas
title: Vlog el foro en el que las madres se ayudan
date: 2021-01-13T10:10:29+02:00
image: /images/posts/vlog/p-vlog-foro.jpg
author: maria
tags:
  - vlog
  - maternidad
  - mujer
  - crianza
---
En el programa de esta semana hablamos sobre la importancia de compartir experiencias entre madres, apoyarnos y aconsejarnos entre nosotras. De sororidad y empoderamiento femenino. Hablamos del nuevo foro de nuestra comunidad de  Madres Cabreadas.

Invitamos a expertos sobre temas que nos interesan par que nos resuelvan nuestras dudas.



No te olvides se suscribirte al canal y compartir!

<iframe width="560" height="315" src="https://www.youtube.com/embed/BiB3J7fkmmQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>