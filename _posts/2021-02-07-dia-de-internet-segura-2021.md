---
author: maria
date: '2021-02-07T10:10:29+02:00'
image: /images/posts/internet-segura/p-portatil.jpg
layout: post
tags:
- tecnologia
- adolescencia
title: ¿Qué es el día de Internet Segura?
---

## Qué es el Internet seguro y cuándo se conmemora

Los días 9 y 10 de febrero de 2021 se celebra el Día de Internet Segura “Safer Internet Day” (SID, por sus siglas en inglés), unas fechas para tomar consciencia de la importancia de hacer un uso seguro y positivo de las tecnologías digitales sobre todo entre niños y jóvenes, y de protegernos y proteger a nuestros hijos en la red, donde surgen cantidad de iniciativas públicas y privadas para concienciar, formar y transmitir la necesidad de usar las herramientas y recursos necesarios para este fin.

## Qué es el Internet seguro para niños

Se trata de una iniciativa de la Comisión Europea para promover el uso seguro y responsable de las nuevas tecnologías, especialmente entre los más jóvenes con objetivo de frenar problemas como el ciberbullying o ciberacoso, el grooming, la pérdida de privacidad, la suplantación de identidad, el uso excesivo de las TIC, el sexting... y otros males que amenazan en Internet.

![incertidumbre](/images/posts/internet-segura/ninos.jpg)

## Cuál es la importancia del Internet seguro

Creo que así como no permitimos salir solos a la calle a nuestros hijos hasta que tienen cierto grado de madurez, igual tenemos que actuar en la red, porque al fin y al cabo, se trata de un entorno externo al hogar, lleno de desconocidos, y en el que existen los mismos peligros que en la calle, aunque de diferente manera. 

Con esto no digo que debamos actuar desde el miedo o prohibir a nuestros hijos el uso de Internet, porque ya forma parte de nuestras vidas, y más desde el confinamiento, donde todos tuvimos que ponernos las pilas y aprender a comunicarnos y relacionarnos a través de la red.

Conocer las posibles amenazas y riesgos, puede ayudarnos a 
prevenir problemas y a solucionarlos caso de que lleguen, pero para eso debemos formarnos y aprender cómo actuar de forma segura para transmitirlo a nuestros hijos.

![incertidumbre](/images/posts/internet-segura/nina.jpg)

## Herramientas para lograr un uso de Internet segura en nuestra familia

Os voy a dejar una serie de artículos que he ido publicando en distintos medios y en este mismo blog para que os ayuden. 

Pero lo que más recomiendo es que uséis los recursos que ofrece la web [IS4Kids "Internet Segura for Kids"](https://www.is4k.es), con la que ya no tendréis excusa para decir que no entendéis del tema porque tiene guías para padres y educadores, Apps de control parental y screen time (limitar el uso de las pantallas), consejos y mil cosas que podréis usar de forma gratuita para orientar a vuestros hijos y estar al día en tecnología y seguridad.

-[Control parental en móviles sí o no](https://madrescabreadas.com/2020/06/24/control-parental-moviles/)

-[Conoce Internet Segura for Kids](https://madrescabreadas.com/2017/04/27/seguridad-internet-menores-incibe/)

-[WhatsApp sube la edad de sus usuarios](https://madrescabreadas.com/2018/04/14/edad-uso-whatsapp/)

-[Intimidad de los menores: Derecho al olvido en Internet](https://madrescabreadas.com/2015/03/21/internet-intimidad-menores/)

-[Fotos de mis hijos en Internet](https://madrescabreadas.com/2017/10/10/fotografias-menores-internet/)

-[Derecho a la Intimidad de los menores en Internet y responsabilidad de los padres](https://madrescabreadas.com/2017/10/10/fotografias-menores-internet/)

-[Peligros de los juguetes conectados a Internet](https://madrescabreadas.com/2017/10/10/fotografias-menores-internet/)

-[Formación en nuevas tecnologías para padres](https://madrescabreadas.com/2020/11/10/charlas-redes-sociales-padres/)


Espero haberte ayudado! Si es así, ayúdame tú a mí: comparte y suscríbete al blog para no perderte nada!

*Photo portada by Kelly Sikkema on Unsplash* 

*Photo by Creative Christians on Unsplash portátil*

*Photo by McKaela Taylor on Unsplash dos niños*