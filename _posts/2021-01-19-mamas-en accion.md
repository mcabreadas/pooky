---
author: maria
date: '2021-01-19T11:10:29+02:00'
image: /images/posts/mamas-en-accion/p-mamas-en-accion.jpg
layout: post
tags:
- invitado
- participoen
title: Ni un niño solo con Mamás en Acción
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy merendamos con Majo Gimeno, ella se define como hija, madre, esposa, amiga, profesional, compañera... mujer en todas sus perspectivas, cree en el desarrollo íntegro de las personas a través de la realización de todas sus facetas. Ve la maternidad como un regalo, pero piensa que vida nos ofrece muchísimo más.
> Siempre al servicio de su familia, de sus amigos, de su equipo, de la gran comunidad de Mamás en Acción y de la sociedad en general para sumarse y contribuir en cualquier foro de apoyo y protección a la infancia, emprendimiento e impacto.
> 

> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. 
> Os presento a Majo Gimeno, fundadora de la ONG [Mamás en Acción](https://mamasenaccion.es):

![Majo Gimeno](/images/posts/mamas-en-accion/majo-gimeno.jpg)

## Qué es Mamás en Acción

[Mamás en acción](https://www.majogimeno.com/sobre-mamas-en-accion/) es una comunidad de personas que dedica tiempo de cariño a los niños que no tienen padres o no pueden vivir con ellos. Niños que se ponen enfermos, como todos los niños, pero que sí han de estar hospitalizados no cuentan con el acompañamiento en el hospital de un adulto y pasan solos sus periodos de recuperación. Mamás en acción hacemos turnos de hospital y nos organizamos para no dejarlos solos nunca, haciendo lo mismo que hacen los papás y mamás de las habitaciones de al lado.


## De una experiencia personal una ONG

Esta iniciativa nació en 2013, cuando unos meses antes yo descubrí a un niño que estaba enfermo y solo en un hospital. Casualmente tenía la edad de mi hija, no tendría ni dos añitos, y me impactó muchísimo verlo solo. Pregunté que quién acompañaba a ese niño y me dijeron que nadie. Que estando allí estaba en un entorno protegido, pero que no había acompañante porque no tenía padres. Enseguida me ofrecí como voluntaria para acompañarlo y me dijeron que sería genial, pero como no pertenecía a ningún colectivo que me amparase jurídicamente, no me dejaron. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/jd8dIIoZqbQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pedí que me informaran de a qué iniciativa podría unirme para poder colaborar y me dijeron que no había ninguna y que por eso el niño estaba solo. El shock para mí fue brutal. Me fui a casa indignada y enfadada con el mundo...
> 
> Pasaban los días y yo no me sacaba al niño de la cabeza. Incluso al coger a mi hija en brazos, era como que lo veía a él y pensaba: “¿Quién estará ahora cuidándolo?” 

Nunca me planteé crear nada, ni muchísimo menos una ONG! Pero fui contando en mi entorno lo que había visto, lo enfadada que estaba por esa situación, y mis amigas me decían: Si puedo hacer algo, cuenta conmigo. Así que pensé que yo sola no podría, pero que si ellas me lo decían de verdad, igual algo sí podíamos hacer.. Y meses más tarde, nació Mamás en Acción.

## Ni un niño solo en Valencia, Madrid, Murcia y sumando...

Desde el día que lo pusimos en marcha, el proyecto nos ha llevado a nosotras y no nosotras al proyecto. Mamás en acción va como un tiro. Empezamos en un hospital y al enterarse las personas iban llamándonos de otros hospitales hasta que logramos cubrir todos los hospitales públicos de Valencia, donde hemos conseguido que ya no haya niños enfermos y solos. 

![voluntarias en hospital la fe](/images/posts/mamas-en-accion/la-fe.jpg)

Después de Valencia llegó Madrid y ya estamos en los 7 hospitales públicos más importantes de la ciudad. Arrancamos en el Hospital Niño Jesús a mediados de 2019 y un año más tarde, en medio de la pandemia, los hospitales nos llamaban para arrancar en otros nuevos. Gobierno de Madrid nos concedió el permiso y durante el confinamiento hemos arrancado en el resto de hospitales. 

Poco antes de acabar el año arrancamos también en Murcia, donde vamos a comenzar en el Hospital La Arrixaca, el hospital con más partos de España donde todas las semanas tutelan a un recién nacido.. Y tenemos 16 ciudades más de España pidiendo llevar nuestra labor allí, así que seguimos trabajando para llegar cuanto antes a todas ellas y conseguir #NiUnNiñoSolo.

## Colabora con Mamás en Acción

### Ser voluntario de Mamás en Acción

Quien quiera unirse puede hacerlo como voluntario. Si no estamos todavía en su ciudad puede registrarse en nuestra web (mamasenaccion.es) para que le avisemos cuando lleguemos y nos ayude a ponerlo en marcha. 

![](/images/posts/mamas-en-accion/bebe.jpg)

[Hazte voluntaria aquí](https://mamasenaccion.es/que-puedes-hacer-tu/unete/)

### Contribuye con Mamás en Acción

También puedes contribuir a sostener económicamente nuestra labor. Nuestros voluntarios no pagan cuotas económicas porque su labor es impagable, pero eso dificulta muchísimo la sostenibilidad y el crecimiento, así que quién quiera unirse colaborando con una donación, puede sentir que suma tanto como los que nos ponemos la camiseta y nos vamos a hacer turnos!

[Dona aquí a Mamás en Acción](https://mamasenaccion.es/product/donativo-solidario/)

[Compra aquí en la tienda solidaria de Mamás en Acción](https://mamasenaccion.es/que-puedes-hacer-tu/tienda-solidaria/)