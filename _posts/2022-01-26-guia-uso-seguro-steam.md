---
layout: post
slug: guia-uso-seguro-steam
title: Descubre cómo usar Steam para jugar a videojuegos de forma segura
date: 2022-02-01T18:52:15.355Z
image: /images/uploads/portada-para-el-uso-de-steam.jpg
author: luis
tags:
  - adolescencia
  - tecnologia
---
Cuando hablamos de videojuegos, la mayoría de nosotros tienden a pensar en las videoconsolas como PlayStation o XBOX, desconociendo que en el mundo gamer hay una opción que cada día es más demandada por los jóvenes, los juegos desde el PC. Para poder jugar online desde un ordenador, en ocasiones se hace uso de plataformas de juegos en línea, como es el caso de Steam, una de las más conocidas a nivel mundial.

Ya os hemos hablado alguna vez del [INCIBE, Instituto Nacional de Cibersegurida](https://www.incibe.es)d, y de su canal [IS4K](https://www.is4k.es), especializado en la seguridad de los menores en la red, y toca volver a hacerlo porque ellos nos hablan de las claves a tener en cuenta a la hora de crear una cuenta en Steam para nuestros hijos. Si aún no sabes de qué te estamos hablando, presta atención a esta guía en la que trataremos de daros las pautas correctas para un uso seguro de esta plataforma de videojuegos.

## Qué es Steam y cuáles son sus características

Steam es una plataforma en línea muy popular en el mundo gamer en la que se pueden comprar juego, descargarlos e instalarlos en el ordenador sin necesidad de un soporte físico. Si hiciéramos un símil, sería como la PlayStation Store o la tienda de XBOX, en la que compras los juegos digitales en lugar de en físico, directamente en la consola. Steam funciona exactamente igual pero para los juegos de ordenador, ofreciendo también la oportunidad de chatear y jugar con otros usuarios, además de ofertar contenido exclusivo para los videojuegos, tarjetas regalo y paquetes de expansión que puedes comprar e intercambiarlos con otros usuarios de la plataforma.

![características de steam](/images/uploads/caracteristicas-de-steam.jpg "características de steam")

En cuanto a las características principales de Steam, vamos a seleccionar las que nos parecen más interesantes e importantes de conocer, dejándonos la siguiente lista.

* **Chat y mensajería con otros usuarios**: Steam permite la comunicación con otros miembros de la comunidad en privado, con lo que podrán compartir sus gustos, preferencias y quedar para jugar.
* **Edad mínima para su uso**: la comunidad de Steam establece restricciones de edad, no pudiendo usarse por menores de 13 años. Los mayores a esta edad, deben de usar la plataforma bajo la supervisión parental, o al menos es lo que nos recomiendan.
* **Audios de voz**: los usuarios van a poder crear grupos y establecer chats de voz, pudiendo jugar con tus amigos asegurándote de una comunicación perfecta en todo momento.
* **Transmisiones en directo**: los usuarios podrán ver a sus amigos, u otros usuarios que lo permitan, mientras juegan. A su vez, podrán permitir que cualquier jugador pueda observar su partida.
* **Suscripciones**: en Steam también hay planes de suscripciones mensuales, los cuales harán falta para ciertos juegos.
* **Moneda virtual de la comunidad**: los usuarios podrán cargar el saldo de su cuenta mediante tarjeta de recarga digital o financiera, para realizar las compras en Steam.
* **Mercado de la comunidad**: en esta plataforma también tendremos la oportunidad de usar el mercado de la comunidad para publicar anuncios en tablones públicos con los que pueden intercambiar artículos o contenido con otros jugadores.
* **Seguridad y privacidad**: los ajustes de seguridad y privacidad son muy importantes en Steam y a la larga lista de ajustes disponibles, hay que mencionar la función de Steam Guard, una verificación en dos pasos que se activará cuando queramos acceder desde un nuevo dispositivo.
* **Multidispositivos**: puedes conectar tu cuenta de Steam en múltiples dispositivos como consolas, Smartphones, tablet, etc.
* **Modo familiar**: es fundamental para controlar la actividad en la cuenta de nuestros hijos menores, además de dejarnos establecer una configuración adaptada a niños, niñas y adolescentes. Steam, además, aporta recomendaciones para los padres, en las que se incluyen el limitar el uso y la conexión o que solo se le permita comprar y descargar juegos que estén acordes a su edad.

## ¿Por qué los jóvenes se ven atraídos por Steam?

Steam es una plataforma que goza de una buena fama entre el público más joven, sobre todo en aquellos que son más aficionados al gaming en PC que en cualquiera de las consolas que hay actualmente en el mercado. Este éxito no es casual y es que es una plataforma que ha sabido nutrirse de una gran variedad de juegos para todos los públicos, abriéndose camino así a un gran nicho de mercado. No faltarán los juegos de aventuras, estrategias, conocimientos y muchos más, entre los que encontraremos los más jugados del momento.

Por otra parte, Steam te ofrece la oportunidad de coleccionar artículos y tokens (escudos, trajes, armas, armaduras, etc.) de tus juegos y personajes favoritos. Por otra parte, no podemos olvidarnos del componente social y es que eso de poder socializar y contactar con jugadores de todo el mundo, con tus mismos gustos y aficiones, es posible gracias a la gran comunidad que se ha formado alrededor de Steam.

## ¿Cuáles son los peligros a la hora de usar Steam?

![guia uso seguro steam](/images/uploads/guia-steam.jpg "guia uso seguro steam")

Sin duda alguna, uno de los mayores peligros que puede haber en esta aplicación está ligado a la posibilidad de interaccionar con otros usuarios. Nuestros hijos menores, van a poder recibir mensajes de otros jugadores, vía chat principal de la plataforma o de forma privada. No es ser neurótico sino precavido y es que en esta plataforma hay mucha gente y no toda es de fiar.

En ocasiones, este tipo de contactos pueden acabar en situaciones de extorsión, ciberacoso o grooming (abuso y acoso sexual en línea). En otros casos, hay personas que se aprovechan de la inocencia y vulnerabilidad de los más jóvenes, ganándose su confianza para luego llevar a cabo el robo de la cuenta. 

Entre los riesgos que pueden encontrarse los menores al acceder a Steam también hay que mencionar el acceso a contenido inadecuado, riesgos para su privacidad o una sobreexposición, ya que dejando que otras personas vean tus partidas, van a poder escucharte tanto a ti como los comentarios que se hagan en tu hogar. Y no podemos olvidarnos de que uno de los grandes peligros es el uso excesivo de la plataforma, lo que puede derivar en una adicción al juego.

## Recomendaciones para un uso seguro de Steam

La mayor parte de estos riesgos pueden evitarse si seguimos unas pautas determinadas a la hora de crear la cuenta de Steam de nuestros hijos. Este es un proceso que no debería realizar en solitario, sino que es un proceso que debemos comenzar juntos y buscando la manera más segura para que pueda disfrutar de su tiempo de ocio. Entre estas recomendaciones, destacamos:

* Antes de crear la cuenta, establece de forma consensuada los tiempos de uso, así evitaremos los problemas más adelante porque será un tema que ya se ha tratado desde el inicio y no habrá discusiones, además de que evitaremos que esto acabe en un uso excesivo de la plataforma.
* Si tu hijo es menor de edad, no olvides establecer el modo familiar y filtrar los juegos inadecuados a los que podría tener acceso.
* Presta especial atención a la configuración de las opciones de seguridad y privacidad. Desde ahí, vamos a poder controlar las solicitudes de compra, el intercambio de contenido con otros usuarios de Steam, restringir la información del perfil para ciertos usuarios, etc. También se puede bloquear la recepción de mensajes que procedan de cuentas que no hemos autorizado previamente o mensajes privados, como también vamos a poder limitar las opciones de retransmisiones.
* Ante cualquier situación desagradable con otro usuario, podemos reportar el problema y bloquear al usuario para que no vuelva a molestar más.
* Hay que hacer hincapié en la importancia de la privacidad, evitando compartir información personal y salvaguardar nuestros datos personales.

Lo que tiene que quedar realmente claro es que el problema no está en el juego o en la plataforma, el problema está en el mal uso que hacen de ella muchos jugadores y que acarrea este tipo de problemas. Por eso es importante ser cuidadosos, compartir tiempo en línea con nuestros hijos y dejar claras las pautas correctas para evitar todo tipo de problemas en la red.

Este [diccionrio de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) te ayudara a conectar con ellos.

*Portada by ELLA DON on Unsplash*