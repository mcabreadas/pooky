---
layout: post
slug: recetas-cocinar-sano
title: 6 recetas de cocina saludable para ahorrar tiempo
date: 2020-09-19T12:19:29+02:00
image: /images/posts/recetas-cocinar-sano/p-recetario.jpg
author: maria
tags:
  - nutricion
  - trucos
  - crianza
  - recetas
---

Hoy os traigo unos trucos para cocinar sano ahorrando tiempo, más unas recetas y una plantilla imprimible para vuestro menú semanal que podéis descargar en el enlace al final del post.

Visto el éxito del post sobre el [método Batch Cooking](https://madrescabreadas.com/2020/09/05/batch-cooking/) para cocinar en unas horas todo el menú semanal, quincenal o incluso mensual, y por petición popular, he recopilado algunas de las ideas que me habéis ido dando estas semanas de platos que soléis hacer y que os dan buen resultado para compartirlos con todas vosotras.

He pedido también colaboración a [Rebeca Pastor](https://mypersonalfood.es) y [Arantza Barcos Calvo](https://www.instagram.com/arantza_barcos_calvo/), mis dos nutricionistas de cabecera para que nos den ideas prácticas y rápidas que nos ayuden a cocinar platos saludables y que resulten atractivos para nuestra familia. 

Además, me he venido arriba fruto del entusiasmo que habéis mostrado con este tema y he diseñado una plantilla de menú semanal para que la rellenéis a vuestro gusto con las ideas de este post u otros platos que soláis hacer. La tenéis al final del post!

Vamos con las recetas!

## Cocina de más y congela: Berenjenas rellenas 

Ya lo hablábamos en el post del Batch Cooking; el congelador es nuestro gran aliado, pero no todos los platos son idóneos para ser congelados, además, al descongelarlos siempre deberemos darles un "toque" final para que resulten más apetecibles (puedes consultar más sobe este método en mi [post sobre el método Batch Cooking](https://madrescabreadas.com/2020/09/05/batch-cooking/))

Por eso os dejo esta video receta de Berenjenas Rellenas de [@mamabuho](https://www.instagram.com/mamabuho/) ideales para cocinar de más y congelar:
 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ltVg2XXBbbc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Ingredientes**

- Berenjenas
- Carne picada
- Cebolla
- Caldo de verduras

**Condimentos**

- Sal/pimienta
- Comino
- Pimentón dulce

**Complementar**

- Salsa de tomate
- Queso rallado

**Consejos de congelación**

Si se van a congelar este último paso no se haría. 
Dejar enfriar y ponerlas en el congelador en una fiambrera 
Sacarlas siempre 24hs antes en la nevera y completar con la salsa de tomate y queso rallado. Y al horno.

Acompañado con unas patatas al horno o fritas es una cena rápida y deliciosa. 



## Aprovecha los restos de forma creativa: cenas "en cadena"

Una de mis nutricionistas favoritas, a la que podéis seguir en Instagram, [@arantza_barcos_calvo](https://www.instagram.com/arantza_barcos_calvo/), nos da la clave para aprovechar las típicas sobras del día anterior de una forma creativa ofreciéndonos estas **7 cenas en cadena** para toda la semana:

**Lunes**

Espinacas, cebolla y ajo salteado con tacos de salmón. 

**Martes**

Con lo que sobró del día anterior hacemos una crema de espinacas (espinacas, cebolla, ajo, agua y quesito bajo en grasa). 

![](/images/posts/recetas-cocinar-sano/espinacas.jpg)

**Miércoles**

Aprovechando las verduras de los días anteriores podemos preparar un delicioso [hummus](https://www.pequerecetas.com/receta/hummus/) cortándola en bastoncitos de verdura cruda: zanahoria, cascos de cebolla, pepino, pimiento, calabacín...

![](/images/posts/recetas-cocinar-sano/hummus.jpg)

**Jueves**

Y ya que tenemos el calabacín a mano, podemos hacerlo tiras bien a mano o bien con un [espiralizador de verduras](https://amzn.to/33K9lCW) (enlace afiliado) y disfrutar de unos tallarines de calabacín con una lata de caballa y salsa de soja 

**Viernes**

Si asamos las cebollas y pimientos y demás verduras, como berenjena... nos saldrá una riquísima escalibada, y si completamos con bacalao a la plancha nos chuparemos los dedos.

**Sábado**

Podemos usar las berenjenas para hacerlas rellenas con cebolla, ajo tomate, champiñones y carne picada 
(ver Receta mamá búho más arriba y añadir champiñones).

![](/images/posts/recetas-cocinar-sano/berenjenas.jpg)

**Domingo**

Y ya que tenemos arreglados los champiñones, podemos usar el resto para elaborar un delicioso carpaccio de champiñones con zumo limón, aceite, pimienta negra y queso rallado.

![](/images/posts/recetas-cocinar-sano/champinones.jpg)


## Reinventa tus platos favoritos en saludables: Lasaña vegetal sin pasta

![](/images/posts/recetas-cocinar-sano/lasana1.jpg)

Hay platos fetiche en cada familia, y en la nuestra sin duda uno de ellos es la lasaña, por eso me encantó la idea de [@miss-sincericidio](https://www.instagram.com/miss_sincericidio/) de hacer una **lasaña vegetal pero sin pasta**!

Dentro receta!

**INGREDIENTES**
- 2 calabacines.
- 1 berenjena hermosa (en Murcia, dícese de berenjena grande).
- 1 Cebolla.
- 1 tomate.
- 1 puñado de champiñones.
- 1 300 gr carne.
- 2 cucharadas de tómate grandes frito casero.
- muuucho orégano y pimienta.
- opcional SAL 

Para de bechamel casera:
-50 gr AOVE.
-50 ml leche almendra.
-50 ml leche de vaca.
-100 gr harina de trigo ( yo hecho mitad y mitad de integral y normal).
-nuez moscada.
-pimienta 
-Queso rayado de calidad

**PASOS**

Lava las verduras y todas esas cositas!

1- corta con un pelador o una [mandolina](https://amzn.to/33K9lCW) (enlace afiliado) a tiras/ láminas las verduras, reserva los centros y picalos.

2- en el horno pon la cebolla, champiñón, tomate a láminas, con aceite, a cocinar. 

![](/images/posts/recetas-cocinar-sano/lasana2.jpg)

3- haz a la plancha las tiras de calabacín y berenjena (con calma).

![](/images/posts/recetas-cocinar-sano/lasana3.jpg)

4- en otra sartén los centros de la berenjena y el calabacín (picados) hazlos junto con la carne unos 15-20 min (yo lo he triturado muy mucho todo con picadora (la carne en la carnicería le digo que la piquen doble) cuando esté hecho échale una cucharada de tomate orégano (sazonarlo al gusto).

5- monta capa a capa:
Cucharada de tomate, tiras de calabacín, berenjena, tomate, cebolla, queso, carne y cubrimos con calabacín y champiñon. 

![](/images/posts/recetas-cocinar-sano/lasana4.jpg)

**Para la cobertura:**

1- sofreímos en el aceite la harina.

2- añadimos la leche, la nuez moscada y la pimienta y cocinamos hasta espesar (7 min aprox).

Cubrimos la lasaña vegetal añadimos queso y al horno 15 min.

**Truco para batch cooking:**
Para guardar, corto porciones individuales o para dos. Y la guardo tal cual. 

Para servir la dejo descongelar en el frigo de un día para otro y la meto al horno o al micro con grill 6-7 min. y esta súper rico. Es muy completa como plato único.


## Guarnición comodín: Pisto de verduras asado

La clave para que no se abuse de patatas fritas u otras guarniciones no saludables es tener siempre a mano una guarnición comodín para completar cualquier cena a la plancha, tortilla, revuelto o lo que se te ocurra.

Os presento la guarnición comodín de la nutricionista, de la que os aconsejo sus espectáculos saludables para niños [@mypersonalfoodrebeca](https://mypersonalfood.es)

**Pisto de verduras asado** 

**Ingredientes** 
- cebolla
- pimiento
- calabacín
- berenjena
- tomate triturado 4-5 cucharadas
- Chorrico de aceite de oliva viregen extra

**ELABORACIÓN** 
- Colocar a dados semejantes en un recipiente de horno, encima tomate triturado y mezclar bien. 

- Meter al horno 180• unos 35 -40min aproximadamente. 

- Remover un par de veces 

Ummm ideal para guardar un par de días en el frigorífico con cierre hermético. 
Para acompañar pescado, carme, huevo o legumbres cocidas (incluidas las de bote).

**TRUCO PARA BATCH COOKING:**

También se puede congelar en raciones, y después Listo para consumir



## Dále la vuelta al pescado: Albóndigas con sepia y calamar

Y para terminar, [@mamistarcook](https://mamistarscook.com/sobre-mi/), una de mis mamás cocineras favoritas, nos ha preparado unas **albóndigas con sepia y calamar** para aquellos no muy amantes del pescado puro y duro 

**INGREDIENTES** 

![](/images/posts/recetas-cocinar-sano/ingredientes.jpg)

1. Pasar las albóndigas por harina, freír y reservar.
2. En una cazuela con un poquito de aceite sofreír la cebolla cortada en pluma y los ajos pelados y chafados.
3. Añadir la guindilla entera para que aporte sabor pero no pique.
4. Añadir un poquito de sal por encima y remover.
5. Añadir los tomates enteros y pelados en conserva y trocear con el cucharón mientras removemos.
6. Una vez esté la cebolla y el tomate bien sofrito añadir la sepia cortada y el calamar en anillas.
7. Remover hasta que esté bien cocinado.
8. Añadir la cucharadita de pasta de ñora, la melsa y las albóndigas que teníamos reservadas.
9. Cubrir con el caldo, en esta ocasión he utilizado 1/2l de pescado y 1/2l verduras, siempre utilizo el que tengo más a mano en la despensa o el congelador.
10. Cocinar durante aproximadamente 45 minutos removiendo de vez en cuando.
11. Añadir los guisantes y apagar el fuego para que se acaben de cocinar con el calor residual.

![guiso de calamares y sepia](/images/posts/recetas-cocinar-sano/plato.jpg)


## Bizcocho de naranja y semillas de amapola

Y como colofón, y si hacéis un [bizcocho de naranja y semillas de amapola como el de Postre Originales](https://postresoriginales.com/bizcocho-de-naranja/), tendréis desayuno y merienda deliciosa para disfrutar!

<iframe width="560" height="315" src="https://www.youtube.com/embed/_IGPushhnsk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Y ahora sí aquí os dejo un regalillo: una plantilla imprimible para que confeccionéis vuestro propio menú semanal con estas ideas y otras vuestras que os funcionen! Ya me contaréis!

Descarga [aquí tu plantilla imprimible de menú semanal](/descargas/recetas-sanas/plantilla-menu-semanal-mc.pdf)

Gracias a todas las que habéis colaborado aportando ideas y recetas para hacernos la vida más fácil!

Si te ha servido, comparte para ayudar a otras familias!

Comparte y me estarás ayudando a seguir escribiendo este blog.



*Foto de portada by Becca Tapert on Unsplash*