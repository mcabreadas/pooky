---
layout: post
slug: hijo-adolescente-no-tiene-amigos
title: Mi hijo adolescente no tiene amigos, ¿Qué puedo hacer yo?
date: 2021-01-21T10:10:29+02:00
image: /images/posts/adolescente-amigos/p-hojas.jpg
author: belem
tags:
  - adolescencia
---
Imagina el siguiente escenario: vas al Instituto de tu hijo adolescente y lo ves sentado solo esperándote en un banco. Lo ves un poco serio. Te pones a recordar y ahora que lo piensas, no recuerdas que te haya contado últimamente nada acerca de sus amigos. ¿Podría ser que… tu hijo adolescente no tenga amigos? 

La mayoría de nosotros sabemos que la adolescencia puede ser una época complicada. Está llena de cambios, de temas incómodos y extrañas interacciones sociales. Navegar por esta etapa de la vida suele ser complicado, incluso en los días buenos. Los problemas de los adolescentes siempre serán distintos a los de los adultos; cuestiones que para nosotros son insignificantes, pueden ser el fin del mundo para ellos. 

Si sé que mi hijo o hija adolescente no tiene amigos, ¿qué puedo hacer yo por él o ella? ¿es nuestra culpa como padres? ¿realmente es mi lugar el interferir? Lo cierto es que nuestras buenas intenciones para con nuestros adolescentes pueden no traducirse como tal y más bien ser recibidas con irritación y ser malinterpretadas.  

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=tf_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=006084129X&linkId=c19675eb503fb638f4798028f033fa81&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Por otro lado, también existe la posibilidad de que nuestro hijo no tenga amigos o tenga sólo uno o un par de ellos, y se encuentre satisfecho con ello. Recordemos que existen muchísimos tipos distintos de personalidades por lo que no todo el mundo tiene que ser la persona con más amigos, la persona más popular o la persona más extrovertida.

![chico solo sentado con capucha](/images/posts/adolescente-amigos/chico-solo.jpg)

## Mi hijo no tiene amigos: posibles causas

La verdad es que, como con casi todos los cuestionamientos existentes, si intentamos adivinar las causas es muy probable que fallemos. La mejor forma de saber algo es preguntando directamente. Puede que en un afortunado momento sea nuestro propio hijo quien se nos acerque a contarnos lo que le pasa. Sin embargo esto es raro y la verdad es que hablar con adolescentes puede ser como caminar por un campo minado algunas veces.

Lo más probable es que no nos cuente nada y que incluso rehúse a hacerlo si le preguntamos directamente. Entonces podría tocarnos hacer un ejercicio de análisis del porqué de la falta de amigos de nuestro adolescente.

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8467052929&linkId=d8c02ddb134d89db015bd0d9f3d48f99&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Puede que la personalidad de nuestro hijo o hija esté generando rechazo entre sus compañeros. Esto es especialmente cierto en adolescentes que muestran personalidades agresivas u hostiles. Mismo caso cuando nuestro hijo es de personalidad introvertida, es inseguro o está siendo [acosado física o emocionalmente en la escuela o instituto](https://madrescabreadas.com/2019/06/14/acoso-escolar-protocolo/). O quizás, solo quizás, sea su elección el no tener amigos y así se encuentre contento y satisfecho. 

Las causas pueden ser tantas, que incluso cabe la posibilidad de que no exista una razón como tal y simplemente sea rechazado solo porque sí. 

## Cómo ayudar a mi hijo a hacer amigos

La mejor forma de comenzar con este proceso es preguntarles directamente si necesitan nuestra ayuda. Quizás lo mejor no sea preguntar directamente ¿Quieres que te enseñe cómo hacer amigos? Esto podría empeorar las cosas. Sin embargo, una vez establecido el problema podemos preguntarle ¿Necesitas mi ayuda en algo? o ¿Qué necesitas de mí? Si hemos preguntado y nos han respondido, escuchemos. No los ignoremos, no lleguemos con frases como aprende a vivir con ello o a mí me fue peor que a ti. Incluso si nos piden un cambio de escuela, considerémoslo, pues puede que nuestro hijo sienta que ya no hay forma de mejorar si situación y lo mejor sería empezar desde cero. 

Otras cosas que podemos hacer nosotros mismos por nuestros hijos es, si es el caso, ayudarle a darse cuenta de que algunas de sus conductas pueden no ser muy bien recibidas por otras personas. Si notamos que nuestro adolescente es de personalidad dominante, agresiva o presenta conductas que podrían estar causando el rechazo de sus compañeros, hacérselos saber. Esto claro, con tacto e intentando siempre en la medida de lo posible, hacerlo sin lastimar sus sentimientos. Recordemos que la retroalimentación constructiva llega mucho más lejos que la destructiva. Entonces, resaltemos aquellos rasgos de su personalidad que pueden ayudarle a hacer nuevos amigos y minimicemos aquellos que no. 

### Actividades nuevas en grupo

Involucrarlos en actividades nuevas también es una buena idea. Muchas veces nuestros hijos presentan problemas sociabilizando debido a que dependen mucho de la tecnología para ello. Entonces resulta que nuestro hijo sí tiene amigos y sí que sabe hacerlos, sólo que no en la vida real, sino en redes sociales o en las plataformas de videojuegos. 

![tres amigas abrazadas](/images/posts/adolescente-amigos/tres-chicas.jpg)

Retomar algún interés olvidado o uno nuevo puede ayudar a nuestros adolescentes a convivir con personas distintas a las que ve diariamente en la escuela. En un ambiente nuevo, nuestros hijos verán la posibilidad de comenzar desde cero y aprenderán a hacer amigos nuevos. 

### Limitar el tiempo de pantallas

En todo caso es buena idea también limitar el tiempo que nuestros hijos pasan en internet o en las videoconsolas y monitorear, con discreción y siempre respetando su privacidad lo más que se pueda, lo que hacen en línea.

Finalmente, como mencionamos antes, un cambio de centro escolar podría ser lo que necesita. Sin embargo, antes de apresurarnos a tomar esa decisión, podríamos explorar otras opciones como un simple cambio de salón o buscar soluciones en conjunto con los profesores o los tutores escolares de nuestros hijos.

### Cómo ayudar a mi hijo con sus habilidades sociales

Existen una serie de [habilidades sociales](https://es.wikipedia.org/wiki/Habilidades_sociales) que son necesarias para el desarrollo de las relaciones psicoafectivas entre personas. Estas incluyen la empatía, la regulación de las emociones, la asertividad, la correcta comunicación, el respeto, la resolución de conflictos y el apego, entre otras. 
Hay, también, algunas maneras de ayudarles a desarrollarlas. Como siempre decimos, el ejemplo es la mejor forma de enseñar. Vivamos de acuerdo con estas habilidades. Demostremos a nuestros adolescentes que hacer amigos nuevos no es complicado y que no lograrlo tampoco es el fin del mundo. 

![4 chicas riendo fondo blanco](/images/posts/adolescente-amigos/cuatro-chicas.jpg)

## Mi hijo adolescente no es feliz

Si creemos o sabemos que nuestro hijo está siendo acosado o es víctima de bullying, la mejor idea es ir directamente a la escuela y tratar el tema con el coordinador de su grado, su tutor o en la dirección. 

Como siempre, lo mejor es no descartar la necesidad de ayuda de alguien externo, como un psicólogo.

Finalmente, lo que nuestros hijos más necesitan de nosotros es nuestra presencia activa. El que nuestro hijo ya sea un adolescente, no significa que ya no nos necesite, aunque así nos lo quieran hacer creer. De hecho, esta es la etapa en la que más necesitan nuestra compañía, consejo y presencia. 
Si nuestros hijos adolescentes no tienen amigos, esto puede deberse a muchísimas razones. Mantengamos los canales de comunicación abiertos (te dejo este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que sea mas facil romper el hielo) y acompañemos con amor y empatía el camino tan complicado que puede llegar a ser la adolescencia. 

Si te ha gustado comparte y suscríbete al blog para no perderte nada!