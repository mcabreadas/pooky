---
date: '2020-05-02T11:19:29+02:00'
image: /images/posts/dia-madre-confinada/p-vida.jpg
layout: post
tags:
- maternidad
- mujer
- crianza
title: Feliz Dia de la Madre confinada
---

Sin duda el confinamiento por causa del COVID-19 va a hacer este día de la madre inolvidable para todas nosotras. 

Esta situación tan surrealista que estamos viviendo nos ha hecho desdoblarnos y ponernos aún más al límite de lo que solíamos estar dejándonos ver todo de lo que somos capaces las madres por nuestros hijos e hijas.

![madres con hijos viendo la tablet](/images/posts/dia-madre-confinada/pantalla.jpg)

Hacemos las tareas del cole, tele trabajamos o trabajamos fuera, hacemos las tareas del hogar, cocinamos más que nunca (como os contaba en este post sobre la [cuarentena alrededor de la mesa](https://madrescabreadas.com/2020/04/12/comida-cuarentena-covid/)), hacemos pan, bizcochos, manualidades, deporte, celebramos cumpleaños para hacerlos especiales aunque los pasen encerrados, ratoncito Pérez, confeccionamos mascarillas, nos convertimos en peluqueras, en técnicas informáticas para las video conferencias, en psicólogas, en expertas en seguridad e higiene ([ver la entrevista a la Dra.Amalia Arce](https://madrescabreadas.com/2020/04/26/seguridad-ninos-calle/)), maestras, incluso en actrices dignas de Óscar cuando tenemos que contener la tristeza la rabia, la indignación o la impotencia cuando vemos el telediario o leemos alguna noticia y hacemos de tripas corazón esbozando una sonrisa, haciendo una caricia y mostrando una falsa calma que bulle por dentro, pero que por nada queremos que rompa el equilibrio que hemos logrado en nuestro mundo aparte que es nuestro hogar.

![mujer confeccionando mascarillas de tela covid-19](/images/posts/dia-madre-confinada/mascarillas.jpg)

En nuestro espacio seguro que hemos creado para ellos a modo de lugar emocionalmente seguro, construido a base de amor donde el miedo y la preocupación no tienen cabida, aunque a veces asomen y los niños se den cuanta porque no son tontos y saben lo que pasa,  pero entre todos hemos construido una suerte de ilusión emocional para nuestra propia supervivencia, una especie de oasis en medio del horror donde se oyen risas cada día, hay besos y juegos, carreras y saltos, también peleas y reconciliaciones, crecimiento personal y como familia.

![mujer en verja todo saldrá bien coronavirus](/images/posts/dia-madre-confinada/verja.jpg)

También hay deberes y extra escolares, tutorías y rapapolvos, limpieza en grupo y reparto de tareas; nuevos aprendizajes para todos, para la vida.

Un paraíso en medio de la desolación y tristeza, en el que el mundo está paralizado, pero los lazos fuertes tiran de él, y siguen llegando bebés a las familias, esos hijos del Covid-19, como les llaman, pero yo les llamo el triunfo de la vida y del amor sobre el desastre.

![madre con bebé con mascarilla](/images/posts/dia-madre-confinada/bebe.jpg)

Cada día me viene a la cabeza la película "[La vida es Bella](https://es.wikipedia.org/wiki/La_vida_es_bella)", y me siento orgullosa de lo que estamos logrando como madres, como familias alegrar un círculo de protección para los más pequeños. Nuestro futuro. Nuestra esperanza.

Sintámonos orgullosas todas y felicitémonos hoy más que nunca en el Día de la Madre que, aunque confinada, lucha por sus crías y las protege venga lo que venga.

Felicidades a todas la madres!

Lo estamos haciendo bien!



- *Photo by David Veksler on Unsplash bebé*
- *Photo by Dan Burton on Unsplash verja*
- *Photo by Kelly Sikkema on Unsplash mascarillas*
- *Photo by Alexander Dummer on Unsplash pantalla*