---
layout: post
slug: black-friday-precauciones
title: Precauciones para comprar en el Black Friday
date: 2019-11-26T12:19:29+02:00
image: /images/posts/black-friday/p-sale-black-friday.jpg
author: maria
tags:
  - derechos
---
Si vas a lanzarte a la aventura de [comprar ropa](https://amzn.to/2Di0CuX), [comprar zapatos](https://amzn.to/33qI6Ln), o [comprar cosmética on line en el Black Friday](https://amzn.to/2QSAfDS) (enlace afiliado) frena un poco y actúa con precaución en Internet. Lee estos consejos para que tu compra acabe de un modo Feliz, y no más cabreada que una mona por no haber acertado o por sentirte engañada.
Te dejo enlace al post sobre [como comprar zapatos por internet](https://madrescabreadas.com/2018/02/28/comprar-zapatos-online/) por si te inclinas por ellos este viernes negro, y éste otro con [consejos para comprar en Amazon](https://madrescabreadas.com/2017/07/10/comprar-prime-day-amazon/).

## Ojo con los falsos descuentos en el Black Friday

Algunas webs hinchan los precios semanas antes del Black Friday para que luego la bajada de precio parezca mayor, por eso no debes fiarte del porcentaje que la propia tienda indica que ha rebajado, sino que puedes usar estas extensiones de tu navegador para comprobar el histórico de precios del producto que te interesa y así poder comprobar realmente porcentaje de descuento real.

[CamelCamelCamel para Chrome y Firefox](https://chrome.google.com/webstore/detail/the-camelizer/ghnomdcacenbmilgjigehppbamfndblo) 

[Keepa](https://chrome.google.com/webstore/detail/keepa-amazon-price-tracke/neebplgakaahbhdphmkckjjcegoiijjo) 

![cartel black friday](/images/posts/black-friday/cartel-black-frify.jpg)

## Consejos para comprar en el Black Friday por Internet

\-Haz una lista previa de cosas que necesitas.

\-Si se trata de ropa o calzado, calcula tu tallaje según la web (a veces no coincide con tu talla real)

\-Lee bien la ficha del producto, no te guíes sólo por la foto. 

\-Apuesta por lo seguro: una web que conozcas y que sea de tu confianza.

\-Lee la política de devolución y asegúrate de que hay facilidad, y de que cubren los gastos.

\-Lee opiniones de otros usuarios de la web

\-Si la web tiene un sistema de interacción inmediato es un plus de confianza a su favor (chat, teléfono, whatsapp…)

\-Asegúrate de que hay disponibilidad del producto y comprueba el tiempo de entrega.

![entrega paquete](/images/posts/black-friday/entrega-paquete.jpg)

\-Ojo con las compras compulsivas. Piensa antes de hacer el último click si realmente lo necesitas y si merece tanto la pena como crees.

## Precauciones en tus compras on line en el Black Friday

\-Comprueba los datos de la empresa, como el nombre fiscal, los datos de contacto y si disponen de tienda física, dirección y teléfono, los apartados de protección de datos y las condiciones de uso y legales.

\-Revisa que los métodos de pago son seguros

\-Pagar con PayPal, ya que ofrece garantía de devolución, o con tarjeta prepago (e-shopping) para limitar la cantidad en caso de robo caso de que alguien rehaga con tus datos bancarios.

![tarjeta visa](/images/posts/black-friday/tarjeta-visa.jpg)

\-Comprobar que en la dirección web que aparece en el navegador aparezca el candadito de navegación segura.

\-Antes de finalizar la compra revisa que todos tus datos personales  y de envío estén correctos.

Mucha gente aprovecha el Black Friday para hacer sus compras navideñas, pero recuerda hacerlo con cabeza y siguiendo estos consejos. 

Te dejo estas ideas de [regalos para adolescentes](https://madrescabreadas.com/2020/11/26/ideas-regalos-jovenes/) que seguro te sacan de apuros.

![estrellas Navidad](/images/posts/black-friday/estrellas-navidad.jpg)

Buena compra!

* *Photo portada by Allie Smith on Unsplash*
* *Photo 1 by Ashkan Forouzani on Unsplash*
* *Photo 2 by RoseBox رز باکس on Unsplash*
* *Photo 3 by Anastasiia Ostapovych on Unsplash*
* *Photo 4 by Kelly Sikkema on Unsplash*