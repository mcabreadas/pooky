---
date: '2020-12-21T09:19:29+02:00'
image: /tumblr_files/tumblr_inline_oj6gmumOA21qfhdaz_540.jpg
layout: post
tags:
- planninos
- navidad
- recetas
title: Actividades y juegos infantiles de Navidad para que los niños no se aburran
  en casa
---

Esta Navidad la vamos a pasar en casa con los niños, pero ellos no entienden de pandemia ni de aburrimiento, así que tendremos que inventarnos todo tipo de actividades y juegos de Navidad para niños infantiles para hacer con ellos y pasar tardes super divertidas en familia, también cocinando o haciendo manualidades.

## Juegos y dinámicas para niños en Navidad 

Una de las cosas con las que más se entretienen los niños en casa es con las manualidades, y si con ellas pueden decorar el árbol, pues verán su obra expuesta durante todas las fiestas y se sentirán orgullosos de ella.

## Manualidades de Navidad para niños

Os dejo este taller de manualidades que impartí en Ikea donde os doy ideas para hacer algunos adornos para el árbol con materiales muy sencillos y muy fáciles de hacer.

[Taller de adornos para el árbol de Navidad](https://madrescabreadas.com/2017/01/03/tallerikeaninos/)

 ![](/tumblr_files/tumblr_inline_oj6gmsrDoQ1qfhdaz_540.jpg)

Otra manualidad súper bonita que les encantará es hacer un arbolito de Navidad con una piña y un bote de cristal relleno de chuches. También muy fácil!

[Manualidades caseras: dos arbolitos de Navidad](https://madrescabreadas.com/2015/12/01/manualidades-ninos-navidad-westwing/)

## Recetas de dulces de Navidad para niños

Si queremos mantener a los niños ocupados e ilusionados con la recompensa final de su trabajo sin duda los pondremos a hacer repostería navideña.

Dejémoslos que se pringuen y amasen y que luego decoren estas riquísimas galletas de mantequilla con fondant.


[Galletas decoradas de Navidad](https://madrescabreadas.com/2015/12/07/decora-galletas-de-navidad-en-familia/)

![](/tumblr_files/tumblr_inline_o39yy5YaSC1qfhdaz_540.jpg)

Y, ya que tenemos el horno encendido, aprovechemos para hacer este pino de Navidad de hojaldre relleno de crema de cacao tan sencillo como resultón, que está llenando las redes sociales de fotos.

[Árbol de hojaldre de Navidad](https://madrescabreadas.com/2016/12/31/arbol-hojaldre-chocolate/)

![](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto6_400.gif)

Pero si lo bueno queremos es encender el horno para que ellos solitos puedan hacerlo, podemos sugerirles que hagan sandwiches de crema de cacao con los moldes de las galletas, de esta forma:

[Tres dulces caseros de Navidad con crema de cacao](https://madrescabreadas.com/2017/12/10/dulces-nutella-navidad/)

 ![sandwiches de nutella con ventana](/tumblr_files/tumblr_inline_p0r8783dZN1qfhdaz_540.png)

Este tiempo en casa en familia es inolvidable para ellos, así que inventemos juegos infantiles de Navidad porque el tiempo que pasamos con ellos es el mejor regalo.