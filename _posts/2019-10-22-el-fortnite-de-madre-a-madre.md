---
date: '2019-10-22T18:00:29+02:00'
image: /images/posts/fortnite/p-nino-jugando-fortnite.jpg
layout: post
tags:
- adolescencia
title: Consejos sobre el Fortnite de madre a madre
---

Si tu hijo se ha transformado de un tiempo a esta parte en un ser que vive contando las horas que faltan para poder conectarse al [Fortnite](https://www.epicgames.com/fortnite/es-ES/download), se enfada cuando se lo apagas y de repente has dejado de entenderlo cuando habla porque usa términos como Skin, lootear, campear, y además te provoca un infarto cada vez que salta de la silla por haberse hecho una victoria magistral, y si te creías una madre moderna y sientes por primera vez el salto generacional y no sabes de qué va esto, ni si es bueno o malo, ni cómo controlarlo o proteger a tu hijo ni a tu cuenta bancaria, tienes que seguir leyendo porque te cuento lo que debes saber del juego del momento, el Fortnite. De madre a madre.

*Además, si quieres otros consejos para gestionar el tema de las nuevas tecnologías en tu familia puedes leer este post sobre [Internet Segura for Kids](https://madrescabreadas.com/2017/04/27/seguridad-internet-menores-incibe/) y éste otro sobre la [intimidad de los menores en Internet*
*](https://madrescabreadas.com/2015/03/21/internet-intimidad-menores/)* 

## ¿Qué es el Fortnite y en qué consiste?

El Fortnite es un juego para jugar en grupo, los niños  suelen jugar con sus amigos, no suele ser con gente aleatoria. Además no es estrictamente violento, aunque es un battle royale, es decir un sistema de juego basado en la película del mismo nombre en el que básicamente sueltan gente en una isla desde un autobús y el que quede vivo es el que gana.

En el Fortnite hay varios modos de juego:

### Fortnite Save the world

Uno consiste en el modo original llamado Save the World, que se juega contra zombies. Proporciona a los jugadores pavos (moneda usada para la compra de objetos en la tienda del battle royale), pero poca gente juega.

### Fortnite Battle Royale

Otro modo de juego  es el Battle Royale, que encierra a su vez diferentes tipos de juego dependiendo de la cantidad de jugadores que haya en cada equipo: se puede jugar en solitario (solo contra otros 99 jugadores) , o en squads (equipos de cuatro compañeros).

También hay otros modos de juego que son, la mayoría de tiempo limitado, pero luego hay otros que son más de 30 vs 30 y se basan en que gana el que consigue llegar a un número determinado de algo, como por ejemplo de bajas que se hace un equipo (contando con que hay vidas ilimitadas).

### Fortnite Modo Creativo

Pero es el modo creativo lo que diferencia  al Fortnite de otros juegos similares, ya que en esta modalidad los jugadores pueden crear su propia isla echarse pvp's(uno contra uno) etc. disponen de munición ilimitada y pueden estar con sus amigos tranquilamente pasando el rato sin presiones, practicando las habilidades del juego sin que te  puedan matar y que no reaparezcas, lo que es parte del secreto del Fortnite, haciéndolo un sitio socialmente interesante para milenials.


![evento fortnite](/images/posts/fortnite/fortnite-decorado.jpg)

## ¿Qué tan malo es jugar al Fortnite?
 
El juego no es ni bueno ni malo en sí, y no tiene un componente especialmente violento, además se ha demostrado que los juegos shooters, incluso los más violentos reducen la violencia real si quien juega tiene edad para diferenciar la realidad de la ficción.

Como valores, yo resaltaría que se juega cooperativamente con los amigos y es entretenido, no mucho más.

Y como contras diría que no me gusta que pongan alicientes para que los chavales sientan la necesidad de entrar cada día bien para aprovechar ofertas limitadas en e tiempo para comprar determinadas skins, seguir consiguiendo experiencia con el pase de batalla, o para avanzar.

Además, la experiencia es algo que no es muy útil en el Fortnite; sólo sirve para emparejarte con jugadores con tu mismo nivel, pero nada más, a diferencia de otros juegos en los que te da acceso a diferentes modalidades o niveles, en el Fortnite parece que sólo sirve para conseguir que se entre todos los días.

## ¿Cuánto cuesta el Fortnite?

No necesariamente hay que pagar para jugar.
El juego como tal se financia a base de cosas cosméticas. Pagar no añade ninguna ventaja competitiva.

Parte de su gran éxito es que la gente considera que es más justo que otros en los que sí se exige pagar para jugar.

## ¿Cuánto tiempo debemos dejar a nuestros hijos jugar al Fortnite?
Hay que intentar mantener un equilibrio si se entiende el juego como una forma más de socializar si nuestro hijo tiene un grupo de amigos que juegan. 

El problema es el posible desequilibrio entre el tiempo de juego de los diferentes integrantes del grupo. Además, para aprender a construir bien hace falta bastante tiempo y práctica(15 y 20 mins). 

Recomiendo tener un tiempo establecido dentro de unos parámetros lógicos en la familia porque es bastante absorbente, ya que la duración de las partida no está muy definida.


![fortnite pantalla](/images/posts/fortnite/fortnite-pantalla.jpg) 

Podemos aprovechar para educar y enseñar a relativizar la importancia de ser bueno en el juego porque para conseguir destacar hay que jugar todos los días ya que está orientado a que por lo menos una vez día se entre, como casi todos los juegos online, lo que pasa es que con el Fortnite se ha generalizado esto, cosa que antes sólo sufrían los grandes jugadores de juegos online como el [WoW](https://worldofwarcraft.com/es-es/).

## ¿Debo prohibir al Fortnite a mi hijo?

No es conveniente eliminar esa parte de la socialización de los niños, además, la prohibición en mi opinión puede fomentar el descontrol y la adicción. Creo que es mejor educar en el control de los videojuegos para prevenir conductas adictivas en el futuro.

## ¿Qué debemos controlar como padres en el Fortnite?

Lo más importante es controlar el e-mail con el que se crea la cuenta; Si sólo nosotros sabemos la contraseña es más difícil llevarnos sorpresas.

Respecto a la interacción con desconocidos, es más difícil que con otros juegos  porque lo que más se suele usar es el chat de voz (aunque también hay chat de texto), por lo que podemos escuchar las voces de los otros jugadores siempre que nuestro hijo no use cascos. También es interesante echar un vistazo en el lanzador del juego a los amigos y revisar los emails para preguntarle si vemos alguien desconocido.

## Comprar pavos del Fortnite

El juego induce continuamente a comprar, y transforma el dinero real en su propia moneda, los "pavos". Esto impide que no se tenga una percepción clara de lo que se está gastando. Además "obliga" a ingresar más dinero del que te quieres gastar porque los pavos sólo se pueden comprar en packs. Es decir, no puedes cambiar la cantidad exacta que te quieres gastar para adquirir algo, sino que tiene que ser la más aproximada que se permite, y una vez que lo metes ya es dinero del Fortnite.

Por eso es aconsejable no dejar guardado el método de pago, para lo que hay que estar atento porque el juego lo guarda por defecto, o bien usar una tarjeta virtual exclusiva para el Fortnite, y sólo recargarla cuando sea necesario.

## ¿Cuántos años tienes que tener para jugar al Fortnite?
Lo recomendaría para niños con la madurez suficiente para diferenciar a realidad la ficción. Quizá a partir de los 9 ó 10 años, pero depende del niño.

Es un juego muy pensado para niños no muy grandes, porque los objetivos dejan de interesar a los 14 o 15 años porque como juego es bastaste limitado. Cuando al grupo deja de interesarle los cosméticos se pierde interés.

## ¿Por qué el Fortnite tiene tanto éxito?

Una de las claves es que no tiene barreras de entrada, es bastante sencillo de jugar, aunque es relativamente difícil de dominar. Lo que lo diferencia de otros es que se pueden hacer construcciones. La habilidad en el dominio de las construcciones es lo que diferencia a un buen jugador de uno malo y lo que hace que gane, no sólo la estrategia de dónde esconderse o qué armas usar.

Ni los creadores esperaban tanto éxito, y es que lo cosmético tiene una función puramente social; da status en el grupo de amigos porque tener una skin guay es lo que marca la diferencia, por eso se ofertan todo el rato cosas nuevas que adquirir, para dar la sensación de que avanza, pero en realidad el juego es plano, no avanza mucho.

## Conclusión: ¿Los videojuegos son buenos o malos?

Los videojuegos no son negativos necesariamente para los niños, aunque siempre es preferible la actividad física, cada vez más forman parte del ecosistema social de los niños y prohibirlos a nuestros hijos creo que sería negativo. Es mejor eliminar otros sistemas de consumo pasivo como la television porque los videojuegos tienen cierto componente activo.

Descartarlos por ignorancia o miedo a que deriven en comportamientos adictivos no esta solución. La prohibición fomenta el descontrol y la adicción siendo mejor educar en el control del tiempo de uso.
Si adquieren el hábito de controlarlo les será más fácil controlar este tipo de comportamientos en el futuro.
Para eso tenemos que estar cerca de nuestros hijos, interesarnos por sus gustos, aprender si cosas nuevas, actualizarnos leyendo posts como éste y aprovechar para educar. 

Los videojuegos han venido para quedarse.

*Photo by Alex Haney on Unsplash*
*Photo by Sean Do on Unsplash*
*Photo by Andrés Felipe Bedoya Interiano on Unsplash*