---
layout: post
slug: maternidad-y-paternidad-responsable-adolescentes
title: Podcast maternidad y paternidad de hoy en día
date: 2023-06-30T12:56:00.173Z
image: /images/uploads/manu-copia.png
author: .
tags:
  - podcast
---
Manu es amigo y oyente del podcast, y nos da un punto de vista masculino de un padre veterano de un chico adolescente sobre cómo afrontamos las mujeres de hoy en día la maternidad.

También hablamos sobre el talento senior y la posibilidad de que se aprovechen sus conocimientos y experiencia para ayudar a emprendedores en las lanzaderas de startups, ya que con pocos recursos y a través de la innovacion pueden construir un negocio rentable.


Ya hay asociaciones que canalizan ese ingente cantidad de saber de los mayores.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/5ZJ9zTPW5j66WAQzemwrAd?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Hoy nos descabreamos con [Manuel Martín La Orden Garcia](https://www.linkedin.com/in/manuel-martín-la-orden-garcía-178bb35a/). CEO de [GLAO Ibd](https://www.glaoibd.com) y Partner de [FAJARDO ENERGÍAS RENOVABLES](https://www.fajardoenergias.com). Experto en Desarrollo e Innovación de Empresas.
