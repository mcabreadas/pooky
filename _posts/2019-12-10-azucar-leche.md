---
layout: post
slug: azucar-leche
title: Exceso de azúcar. No es la leche, son los bollos
date: 2019-12-10T12:19:29+02:00
image: /images/posts/leche-azucar/p-glaseados-originales.jpg
author: .
tags:
  - 3-a-6
  - nutricion
  - salud
  - crianza
  - 6-a-12
---
Las fechas navideñas nos empujan a empeorar nuestros hábitos alimenticios y a relajarnos con los de nuestros hijos, ya que es casi inevitable caer en el consumo excesivo de azúcares añadidos en alimentos como los dulces navideños. Por eso me hace mucha gracia cuando nos empeñamos en buscarle los tres pies al gato y empezamos a mirar minuciosamente las etiquetas en el super intentando descifrarlas, mientras tenemos el carro lleno de turrón y polvorones (contradicciones de la maternidad, y yo la primera).

A ver, que no digo yo que no debamos leer las etiquetas, pero un poco de coherencia, por favor, que lo que hace que los españoles sobrepasemos con creces el consumo de azúcar añadido OMS no es precisamente los alimentos normales de nuestra dieta mediterránea, por favor, que mucha gente se cree que por ejemplo la leche lleva azúcar añadido porque en la etiqueta pone azúcar refiriéndose a la lactosa, que es un azúcar natural que nadie le añade, que además lleva galactosa, que es un nutriente esencial para el desarrollo del cerebro de los niños. Esto ya os lo expliqué en este [post sobre nutrición infantil y de embarazadas](https://madrescabreadas.com/2019/02/03/leches-adaptadas-embarazadas-ninos-puleva/), pero me gustaría aclarar algo sobre el controvertido tema "azúcar").

![botellas de leche](/images/posts/leche-azucar/botellas-leche.jpg)

Que no, que no son los lácteos los culpable de que superemos los niveles de azúcar añadida de los alimentos, que los responsables son la bollería las bebidas azucaradas, los zumos y demás.

A menudo la solución a un problema es la más evidente, y el sentido común se impone en este caso. Pero si todavía os queda alguna duda sobre el arte de azúcar extra de los lácteos a nuestra dieta, y eres de las que ha optado por reducirlos o incluso por eliminarlos, o lo estás pensando, por favor lee estos datos objetivos:

Los productos lácteos aportan azúcar añadido de forma muy limitada en la dieta – poco más del 10% del consumo total- y, al mismo tiempo tienen una riqueza nutricional muy elevada.

## Compromiso del sector lácteo

A pesar de este limitado aporte de azúcar añadido, el sector lácteo ha tomado la delantera en su firme compromiso por reducir los azúcares añadidos, como puso de manifiesto el acuerdo entre [FENIL (Federación Nacional de Industrias lácteas)](http://fenil.org) y [AESAN (Agencia Española de Seguridad Alimentaria y Nutrición)](https://rgsa-web-aesan.mscbs.es/rgsa/formulario_principal_js.jsp). Ambas instituciones [firmaron en 2018 un acuerdo](http://fenil.org/la-industria-lactea-pone-valor-compromiso-mejorar-la-composicion-productos/) por el que los fabricantes de lácteos se comprometían a reducir en 2020 un 10% de la presencia de azúcares añadidos en los productos lácteos de consumo habitual y en un 5% en los de consumo ocasional.
Algunas marcas van más allá, por ejemplo Puleva, superando en más de 3 veces el compromiso adquirido con las autoridades sanitarias.

Reducir el consumo de azúcar añadido se ha convertido en un caballo de batalla de las autoridades sanitarias españolas estos últimos años, como parte de una estrategia global de promoción de hábitos de vida saludables, principalmente dieta y ejercicio físico, destinados a prevenir y reducir los casos de sobrepeso y obesidad.

## Los culpables son los bollos, no la leche

Según el [estudio ANIBES (2015)](http://www.sennutricion.org/es/2015/04/29/estudio-anibes-antropometra-ingesta-y-balance-energtico-en-espaa-diseo-protocolo-y-metodologa), cerca del 90% del aporte de azúcar añadido en la dieta de la población española (9-75 años) procede de grupos de alimentos como los refrescos con azúcar, el azúcar, bollería y pastelería, chocolate, mermeladas, zumos y néctares y cereales de desayuno y barritas de cereales. Poco más del 10% del azúcar añadido en la dieta procede de lácteos y, en el caso de los niños, esta cifra sube ligeramente al 17%. 

![galletas con leche](/images/posts/leche-azucar/leche-galletas.jpg)

## La leche aporta muchos nutrientes esenciales para el desarrollo infantil en un sólo vaso

En ambos casos, como pone de manifiesto el estudio ANIBES, los productos lácteos aportan azúcar añadido de forma muy limitada en la dieta y, al mismo tiempo, tienen una riqueza nutricional muy elevada, gracias a su contenido en nutrientes y micronutrientes esenciales para el correcto funcionamiento del organismo, como calcio, vitamina D, proteínas, fósforo, etc. que deben considerarse siempre al evaluar la calidad nutricional de un producto. De ahí que los lácteos formen parte de un patrón de alimentación saludable y que se recomiende el consumo diario de 3 raciones al día.

## Hay marcas que van más allá

[Puleva](https://www.institutopulevanutricion.es), con más 60 años en nuestro país es una de las abanderadas en este ambicioso plan y va más allá de la reducción de azúcar añadido acordada con las autoridades. Históricamente ha trabajado para la adaptación de sus productos a las necesidades nutricionales de diferentes grupos de población. Fruto de ello fue el lanzamiento de categorías de producto como las leches con Omega 3, con Calcio o leches infantiles con ácidos grasos esenciales, hierro, etc.

La marca, que cuenta con una gama en su mayoría (el 84%) sin azúcar añadido, ha trabajado estos últimos 4 años para reducirlo en el 16% de las referencias que aún lo contenían. El resultado ha sido una reducción del 45% del azúcar añadido. De este modo, los productos de Puleva que aún contienen azúcar añadido aportan por ración entre un escaso 5% -los de consumo frecuente- y un 19% -los de consumo ocasional-, de la cantidad diaria recomendada como máximo de azúcar añadido por la Organización Mundial de la Salud. 

Dentro de este compromiso, referencias de gran aceptación que contenían azúcar añadido, como Puleva Peques, se han reformulado y actualmente ya no contienen azúcar añadido.
Lácteos, señalados de forma injustificada.

## Ojo con las etiquetas: No todo el azúcar es añadido

Los productos lácteos han sido señalados de forma injustificada y excesiva, entre otras razones por su contenido en lactosa. Se trata de un azúcar naturalmente presente, del que hasta ahora no se ha explicado suficientemente que:

![cuchara con azúcar](/images/posts/leche-azucar/cucharada-azucar.jpg)

* Es un azúcar, que está presente de manera natural en la leche, al igual que la fructosa en la fruta.
* Tiene la virtud de aumentar en menor medida los niveles de glucosa en sangre con respecto a la sacarosa (azúcar comúnmente añadido a los productos). Además, gracias a uno de sus componentes –la galactosa-, desempeña un rol relevante en el desarrollo cerebral especialmente en la etapa infantil.
* Está excluido de las limitaciones de consumo por parte de la OMS y otras autoridades sanitarias, al no considerarse un azúcar libre.
* Cuando leemos el etiquetado de los productos, en caso de que el producto lácteo contenga azúcar añadido, la [normativa europea vigente (INCO)](https://www.boe.es/buscar/doc.php?id=DOUE-L-2011-82311) no permite separar cuánto del azúcar declarado es lactosa y cuánto azúcar añadido, lo que contribuye a una mayor confusión.

Así que, ya sabes, no le eches la culpa a la leche, sino a los bollos que mojas en ella.



*Foto portada by Heather Ford on Unsplash dulces azul y rosa*

*Foto botellas de leche by Crissy Jarvis on Unsplash*

*Foto galletas y leche by Brian Suman on Unsplash*

*Photo by Sharon McCutcheon on Unsplash cuchara con azúcar*