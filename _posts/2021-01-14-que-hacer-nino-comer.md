---
author: ana
date: '2021-01-14T10:10:29+02:00'
image: /images/posts/que-hacer-cuando-un-nino-no-quiere-comer/p-llego-la-hora-de-comer.jpg
layout: post
tags:
- nutricion
- crianza
title: Qué hacer cuando un niño no quiere comer
---

Si a temores vamos, el de que **mi niño no quiere comer** es muy común y desata variadas conjeturas. Nos reclama tácticas, talento y paciencia, pero sobre todo, nos invita a prestar atención a las posibles causas. 

No podemos descuidar un aspecto esencial en el desarrollo de nuestro hijo, pero antes de alarmarnos, indaguemos juntos **qué hacer cuando un niño no quiere comer**. Sígueme y descubrámoslo juntas.

## Por qué mi hijo no quiere comer
Si mi niño es de buen comer y de pronto manifiesta inapetencia, tal vez lo que probó no le gustó. No insistamos, no al menos el mismo día ni con mayor insistencia, no vaya a ocurrir que asocie el alimento con un mal rato.

Nunca está de más probar el alimento que le estoy dando, si está sabroso para mi, lo más probable es que lo esté para él. Debo asegurarme de que tiene una temperatura agradable. Descartados estos detalles, no todos los días son iguales y puede ocurrir que hoy no esté de ganas.

Pero si **mi hijo no quiere comer** de manera recurrente, y rechaza todas las comidas, entonces amerita observación detallada y más de ocuparse. 

![prueba-te-gustara](/images/posts/que-hacer-cuando-un-nino-no-quiere-comer/prueba-te-gustara.jpg)

## Mi hijo no quiere comer, ¿qué debo hacer?
Si la falta de apetito es repentina, puede sentirse mal y reportar algún malestar estomacal, la salida de un diente o un proceso viral. En cualquiera de estos casos mi niño no comerá como acostumbra. 

### Comer en horarios habituales
En primer lugar, revisemos el horario de las comidas pues, en las horas acostumbradas, el apetito despierta. Si ha habido un descontrol, volvamos a la rutina y al ritual. Preparar la comida es también preparar la hora y el ambiente para comer. 

### Apartemos las distracciones
Si mi niño no quiere comer puedo caer en la tentación de distraerlo para que no se de cuenta de que está comiendo. Con el juego del avioncito o con alguna pantalla encendida puedo lograr que no preste atención a lo que está comiendo. Pero ojo, que sirva sólo para atraerlo. Una vez que comience hagamos que enfoque toda la atención en el sabor, el color y la textura de los alimentos. 

### No le temamos a la novedad
Hay niños a los que le gusta probar cosas nuevas, otros reacios a las novedades. Si este es vuestro caso, reviste el alimento de color y detalles que lo hagan atractivo y apetecible. Si un plato le gusta mucho más que otros, hagámoslo especial y que sea el propio de fiestas, domingos, o camping. Esto, para que nos quede lugar en la semana para comidas tal vez menos preferidas, y acaso más nutritivas.

![comer-a-su-gusto](/images/posts/que-hacer-cuando-un-nino-no-quiere-comer/comer-a-su-gusto.jpg)

### Ni amenazas ni reproches
Cuando estamos ante un niño sano y que por algún capricho no quiere comer, nunca recurramos a las amenazas o chantajes. Menos a la fuerza. Paciencia: hagamos de la comida un encuentro familiar, un momento para compartir sabores y colores. La alegría es contagiosa.
Ahora bien, si el problema persiste y se evidencia baja de peso y talla, no dudéis en acudir al pediatra. 

Está claro que si no se alimenta bien, mi niño se descompensará. ¿Ha tomado suficiente agua? ¿Ha evacuado bien? ¿Presenta gases o hipo? Estas señales nos advierten de un pequeño problema e influyen a la hora de decidir **qué hacer cuando un niño no quiere comer**.

Pero, si todo en apariencia está bien, mantengamos la calma e invitémosle con entusiasmo a la mesa. Si tiene hambre, se acercará; y si lo que tiene servido es de su gusto, se quedará. Que no nos note aprensivas ni molestas. El niño está creciendo y construyendo su personalidad: quiere y necesita tomar decisiones. Y decir no, es su principal recurso.

_Foto de portada por Jeff Hendricks en Unsplash_

_Foto por Artyom Kabajev en Unsplash_

_Foto por dimitri.photography en Unsplash_