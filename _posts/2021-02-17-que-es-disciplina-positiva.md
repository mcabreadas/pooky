---
author: maria
date: '2021-02-17T09:10:29+02:00'
image: /images/posts/disciplina-positiva/p-disciplina-positiva.jpg
layout: post
tags:
- educacion
- invitado
- crianza
title: Cómo puede ayudarte la disciplina positiva
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy merendamos con Raquel Peinado, asesora de familias guiándolas para que ejerzan una crianza responsable basada en la firmeza y en el amor, donde no hay lugar para el estrés, los gritos, los premios ni los castigos.
 
> Raquel es fundadora del proyecto [www.mamiintensa.com](https://www.mamiintensa.com), que nace para ayudar a las familias a resolver los conflictos del día a día de manera permanente y para establecer una relación de confianza entre todos sus miembros.

> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os dejo con Raquel:

![taller disciplina positiva](/images/posts/disciplina-positiva/raquel.jpg)

## ¿Qué es la disciplina positiva?

Para mí, la disciplina positiva es un modo de ver la vida con sentido común. Es aplicable a la crianza con los peques, a la pareja, a las empresas y, en general, a cualquier relación humana.

Es una mezcla de respeto, amor, comprensión, empatía y honestidad.

Y esto, ¿cómo se hace?

Todos tenemos conflictos en casa con los peques que tenemos que abordar de alguna manera.

Si abordamos el problema luchando, estaremos avivando el fuego y participando en la batalla. Tendremos un ambiente de estrés y nerviosismo.
Si cambiamos nuestra postura y no entramos en la lucha, sino que vemos la necesidad real de la situación y la aportamos, generaremos un ambiente de calma y serenidad.

Con ejemplos, todo se entiende más fácil. A ver si te suena la siguiente escena:
> Lávate los dientes.
> No.
> ¡Ve al cuarto de baño y lávate los dientes!
> ¡No!
> Los dientes, ¡YA!
> ¡QUE NOOOOO!
> 
¿Cuántas veces has entrado en esas luchas de poder con tu hijo? Parece que nos está echando un pulso a ver quién gana y, nosotros, hemos entrado al trapo y queremos ganar.

Y si en lugar de entrar en esa competición por ganar, ¿lo intentamos de otra manera? Si conseguimos ver la base del problema y eliminarla, vamos a poder conseguir un ambiente relajado.

En este caso, debemos eliminar la orden. Las órdenes no gustan a nadie y generan tensión de por sí. Para no ordenarles, podemos crear una tabla de rutinas con el niño o bien darle alternativas como “¿te pones la pasta tú o te la pongo yo?”, “¿nos lavamos los dientes en la bañera o después del baño?”.

## ¿Es fácil aplicar la disciplina positiva?
 Si tenemos claro que no queremos educar a base de gritos, tensiones y castigos… Lo primero que hay que hacer es aprender las alternativas que tenemos para poder actuar diferente.

Una vez que tenemos este aprendizaje, se trata de practicar y practicar. Como todo, al principio cuesta, pero con mucha constancia practicando día a día, se consigue.

Piensa que, si seguimos con el ejemplo de lavarse los dientes, son las 20:00, estamos agotados, hemos tenido un día duro y se nos ha acabado la paciencia… pues no va a ser fácil. Aún así, piensa que te vas a tener que esforzar e invertir más energía, ¿dónde quieres invertirla: en discutir o en buscar soluciones?

## ¿Por dónde empiezo para aprender a educar de esta manera?

Por un taller de varias sesiones. Mis talleres son vivenciales, participativos y muy divertidos. Te aportan herramientas y una visión global de cómo es educar en disciplina positiva.

Además, al ser de varias sesiones, vas a adquirir los conocimientos y herramientas poquito a poco y las podrás ir poniendo en práctica. Te surgirán dudas que podrás ir consultando y contarás con mi acompañamiento durante el proceso.

Una vez que se acaba el taller, tenemos una comunidad para seguir con el enfoque de la disciplina positiva.




Puedes encontrarme en Instagram: [@mamiintensa](https://www.instagram.com/mamiintensa/) o en la página web [www.mamiintensa.com](https://www.mamiintensa.com/taller-de-disciplina-positiva/)


## Talleres de disciplina positiva on-line

![taller disciplina positiva](/images/posts/disciplina-positiva/cartel.png)

### Descuentos para los talleres

-Descuento para las 5 primeras plazas: 89€

-Código descuento del 15% (acumulable al anterior):

> madrescabreadas15


<a href="https://www.mamiintensa.com/registro/" class='c-btn c-btn--active c-btn--small'>Apúntate al taller</a>

## Consulta gratis con nuestra experta en disciplina positiva

Si quieres consultar alguna duda concreta sobre la educación de tus hij@, está en los terribles dos, no te hace ni caso o te cuesta un mundo que llegar al colegio por las mañanas a tiempo, puedes consultar de forma gratuita con nuestra experta en disciplina positiva, Raquel Peinado.

<a href="https://foro.madrescabreadas.com/t/hablemos-de-disciplina-positiva" class='c-btn c-btn--active c-btn--small'>Haz una consulta a la experta</a>
   
*Foto de portada by Derek Thomson on Unsplash*