---
layout: post
slug: puede-vivir-sin-conflictos
title: Podcast ¿Es posible vivir sin conflictos... y sin cabrearse?
date: 2022-09-12T09:04:30.472Z
image: /images/uploads/3.png
author: .
tags:
  - podcast
---
Entrevista a Paloma Salvador, traductora de las enseñanzas de J. Krishnamurti y Presidenta de la Fundación Krishnamurti Latinoamericana del 2015 al 2019, filósofa de corazón y apasionada del conocimiento propio.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/0A8TT9FtJk8sq5LIpcrNd8?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

## Encuentros en Pozo Amargo, Cuenca

Paloma propone una serie de encuentros de investigación y conocimiento propio sobre cuestiones fundamentales que atañen a todos los seres humanos, con el fin de aprender a mirar nuestras vidas desde otro prisma, desde una mirada en sí misma transformadora.

El primer encuentro de este ciclo, que tiene como eje central la necesidad de un cambio en nuestra forma de vivir y relacionarnos, se desarrollará en Pozoamargo, Cuenca, el último fin de semana de Septiembre, del viernes 30 al domingo 2 de octubre con el tema: ¿Es posible vivir sin problemas?

Lo que Paloma quiere transmitir y compartir es el arte de abordar los problemas humanos cotidianos, como el conflicto, el sufrimiento, la relación, el miedo, etc., desde una mirada clara, sin filtros,o sea, sin opinión, sin creencias, sin prejuicios ni preferencias para darnos la oportunidad de ver las cosas como son, lo cual, en sí mismo, es un cambio radical.

En nuestra charla, hablamos de por qué es importante y necesario aprender ese arte de mirar nuestras vidas sin filtros, quizás la única posibilidad de un cambio profundo en la consciencia humana.

“Lo importante es cómo mira todo esto, no lo que quiere hacer al respecto.¨ 
J. Krishnamurti 

## Libros recomendados para vivir sin conflictos:

\-[Nada es un problema (Sabiduria perenne)](https://amzn.to/3QoRkAz) (enlace afiliado)

\-¿Qué estás haciendo con tu vida? Diálogos de J Krishnamurti con jóvenes. 

Este libro recoge una cuidada selección de sus inspiradoras reflexiones para ayudar a adolescentes a explorar las cuestiones esenciales de la vida. En el texto se plantean preguntas y se ofrecen respuestas sobre cuestiones tan vitales como la educación, el trabajo, el dinero, las relaciones con los demás, el sexo, el matrimonio, el amor, Dios, la meditación y muchos otros temas.

Posts que te ayudaran:

Los conflictos estan presentes en casi todas las casas con adolescentes porque muchas veces son tan impulsivos que no piensan lo que dicen, por eso si [tu hijo adolescente te ha dicho que te odia](https://madrescabreadas.com/2022/02/03/mi-hijo-adolescente-me-odia/) alguna vez no debes tenerselo en cuenta. Leer este afrticulo te ayudara.