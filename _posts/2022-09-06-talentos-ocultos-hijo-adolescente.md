---
layout: post
slug: talentos-ocultos-hijo-adolescente
title: Podcast tu adolescente tiene al menos 5 talentos. Ayúdale a descubrirlos
date: 2022-09-07T09:52:52.228Z
image: /images/uploads/873x490-c-77934.jpg
author: .
tags:
  - podcast
  - educacion
  - adolescencia
---
Entrevista a [Elvi Barrios, Coach Emocional especialista en adolescencia](https://www.elvibarrios.com) y educadora de DP para familias. Se formó como coach infanto-juvenil y familiar, se especializó en inteligencia emocional, educadora de disciplina positiva para familias con Marisa Moya y está realizando la carrera de Psicología.

En nuestra charla hablamos de qué es el talento y qué tipos de talentos hay:

* Por qué es importante descubrir nuestros talentos: encontrar tu propósito.
* La relación entre el talento y la vocación: que es la vocación y ver que juntos son muy potentes.
* Cómo acompañar el desarrollo del talento en los adolescentes: fomentar el autoconocimiento, no vincularlo a una profesión en concreto, no juzgar, dar un feedback descriptivo, tener en cuenta que debe desarrollarse, buena gestión emocional, ver el error como algo natural, evitar etiquetas.

  ## Recomendado en este episodio:

  Libros:

  \-[El Elemento, de Ken Robinson](https://amzn.to/3e1wV72) (enlace afiliado)

  \-[El poder del discurso materno, de Laura Gutman](https://amzn.to/3e4kQOx) (enlace afiliado)

  Película:

  Gattaca





## Parte I Como ayudar a desarrollar los talentos de tu adolescente

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/61SxYAAEb0Y0EmTWDLLSHN?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

## Parte II Como ayudar a desarrollar los talentos de tu adolescente mas preguntas y respuestas

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3RYOCxYkFODa9yjfLM20XQ?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Tambien te puede ayudar este post sobre [como ayudar a tu adolescente a hacer amigos](https://madrescabreadas.com/2021/01/21/hijo-adolescente-no-tiene-amigos/).