---
layout: post
slug: educacion-financiera-en-el-hogar
title: ¿Por qué es importante la educación financiera en el hogar?
date: 2023-03-01T09:47:05.397Z
image: /images/uploads/educacion-financiera-en-ninos.jpg
author: faby
tags:
  - adolescencia
  - educacion
---
**El hogar se parece a una empresa en el sentido de que genera gastos e ingresos.** Si los miembros de la familia no tienen educación financiera es posible que se enfrenten a situaciones muy complicadas, que en el peor de los casos pueden llegar a derivar en el desalojo de la vivienda o la **falta de suministros básicos.** De ahí que es vital saber administrar los recursos e ingresos y programar los gastos.

En esta entrada explicaremos qué es la educación financiera y por qué debes enseñarla a tus hijos adolescentes, incluso a los más pequeños.

Usa palabras coloquiales para ellos, y no te metas en lenguaje demasiado tecnico, uncluso puedes lograr que atiendan mejor a la explicacion si usas lenguaje adolescente del que ellos sueles emplear con sus amigos. Para ello aqui tienes este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/).

## ¿Qué es la educación financiera?

La educación financiera consiste en adquirir conocimientos sobre el **manejo correcto del dinero** a nivel social, personal y familiar. Es la capacidad de [ahorrar](https://es.wikipedia.org/wiki/Ahorro), controlar y planificar.

Gracias al aprendizaje de las finanzas se puede administrar los recursos disponibles y realizar **gastos que no afecten la planificación del futuro.**

Todas las sociedades cuentan con un sistema financiero. Ahora bien, en el entorno familiar es necesario disponer de una cultura financiera saludable. **Esto permite que como familia se pueda mejorar la calidad de vida.**

## ¿Es necesario enseñar educación financiera a los hijos?

Enseñar finanzas a niños es esencial, debido a que **en el contexto familiar es que se aprenden los principales valores y hábitos** que forjarán gran parte de la personalidad del niño.

Pues bien, los niños necesitan aplicar sus conocimientos de matemática en la vida cotidiana. Si se enseña desde temprana edad cómo debe ahorrar un niño, es probable que este aprendizaje lo acompañe hasta la vida adulta, lo que **le permitirá tomar mejores decisiones en el área financiera personal y luego familiar.**

## ¿Cómo enseñar educación financiera a los hijos?

Las finanzas se pueden aprender de forma progresiva; según la edad. **Lo más importante es que sepas cómo administrar el dinero en tu hogar,** una vez tienes un plan de ahorro bien enfocado, es el momento de enseñar el buen manejo del dinero a tus peques.

### Fomenta el estudio de las matemáticas

![Calculo financiero en niños](/images/uploads/calcilo-financiero-en-ninos.jpg "Calculo financiero en niños")

**Es necesario que los niños sepan contar, sumar, restar e incluso multiplicar.**

Si aplicas la matemática en las finanzas personales, es muy probable que vean esta rama como algo divertido.

### Explica a tus hijos qué es el dinero

![Explicando el valor del dinero a un niño](/images/uploads/explicando-el-valor-del-dinero-a-un-nino.jpg "Explicando el valor del dinero a un niño")

**Los niños deben saber de *[dónde sale el dinero](https://www.amazon.es/%C2%BFD%C3%B3nde-crece-dinero-gestionar-ilustrados/dp/841767151X/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=YXYJO0RYY8MG&keywords=finanzas+ni%C3%B1os&qid=1663940110&sprefix=finanzas+ni%C3%B1%2Caps%2C257&sr=8-5&madrescabread-2)*.** Debes explicarle que el dinero es esencial para vivir. Así pues, no debe gastarse en cualquier cosa. Puede ser útil algún *[libro de finanzas para niños.](https://www.amazon.es/Padre-libre-hijo-rico-emprendimiento/dp/B08CWG2WQS/ref=sr_1_3_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=YXYJO0RYY8MG&keywords=finanzas+ni%C3%B1os&qid=1663940032&sprefix=finanzas+ni%C3%B1%2Caps%2C257&sr=8-3-spons&psc=1&madrescabread-2) (enlace afiliado)*

Explícale los **4 conceptos básicos del dinero**: Gastos (debe ser menor que los ingresos), ingresos (se obtiene trabajando), ahorro (habilidad para hacer frente a emergencias), inversión (habilidad para hacer crecer el dinero).

### El dinero se obtiene trabajando

![Niña colaborando en aseo del hogar](/images/uploads/nina-colaborando-en-aseo-del-hogar.jpg "Niña colaborando en aseo del hogar")

**Dales una cuota o ingreso de dinero a tus hijos** (puede ser semanal, quincenal o mensual), pero debes explicarles que ellos deben cumplir con sus deberes para recibirlo. Esto les enseñará que para **obtener ingresos hay que esforzarse.**

Por cierto, si gastan todo su dinero antes del siguiente ingreso, debes decirles que el próximo pago debe administrarlo mejor.

### Fomenta el ahorro en tus hijos

![Niño ahorrando](/images/uploads/nino-ahorrando.jpg "Niño ahorrando")

Ayúdales a ver los **beneficios de guardar algo de dinero**, de esta forma pueden comprar por sí mismo algún juguete que les guste.

También puedes compensarlos por generar ahorros, (pagarles intereses). Por ejemplo, propón que si logran ahorrar 20 Euros entonces le darás algo extra por concepto de interés.

### Compras inteligentes

![Niña comprando viveres con su mamá](/images/uploads/nina-comprando-viveres-con-su-mama.jpg "Niña comprando viveres con su mamá")

Enseña a **comparar los precios**. Además, ayúdalos a no caer presa de la publicidad. Por cierto, recalca la importancia de no sucumbir a los deseos, sino más bien, que piensen en lo que realmente necesita.

### Uso de apps de finanzas

![Niña apps finanzas](/images/uploads/nina-apps-finanzas.jpg "Niña apps finanzas")

¿A tu hijo le encanta la tecnología? Hay apps de finanzas para niños, como [la que usamo nosotras de Revolut](https://madrescabreadas.com/2023/01/29/tarjeta-para-adolescentes/), de esta forma lo ayudas a formarse como todo un administrador, mediante **juegos de finanzas para niños.**

**[Revolut <18](https://bit.ly/3WH91hX)**, es una aplicación que contribuye ampliar el segmento de clientes de la Compañía. Al mismo tiempo, permite que los padres puedan impartir **educación financiera a sus hijos a partir de los 6 años.**

Si tus hijos no han cumplido la mayoría de edad, puedes crear y gestionar una cuenta. De este modo, puedes ayudarles a crear conciencia sobre el dinero e incentivar el buen uso del mismo.

Puedes [descargarla gratis](https://bit.ly/3WH91hX) y dar de alta a un menos de forma gratuita.

## Beneficios de la educación financiera en la familia

* La educación financiera familiar contribuye a mejorar la calidad de vida
* Permite planificar un mejor futuro
* Se aprende a ser previsor
* Se realizan compras inteligentes
* Fomenta el esfuerzo y trabajo en equipo
* Ayuda a desarrollar responsabilidad desde temprana edad
* Mejora el ambiente familiar, ya no se generan discusiones por el dinero.

En conclusión, el aprendizaje del manejo de las finanzas es esencial para enfrentarse a las presiones de la vida real. Toda familia debe contar con una buena **planificación financiera para enfrentar las presiones con éxito.**