---
layout: post
slug: separacion-respetuosa-congreso-nuevas-familias
title: Congreso on line Creando Nuevas Familias
date: 2021-06-06T18:38:48.131Z
image: /images/uploads/kin-li-uoykntgpvne-unsplash.jpg
author: maria
tags:
  - invitado
  - conciliacion
  - derechos
---
Hoy tenemos dos invitados de excepcion, Rocio y Miguel Angel, de Creada, que son pioneros en lanzar un congreso on line para acompañar y dar herramientas a las personas que se estan separando o piensan hacerlo, o estan rehaciendo su vida, para que el camino sea menos tortuoso, respetuoso  consciente, sobre todo para sus hijos e hijas, y tambien para ellos y ellas.

Me ha parecido un tema innovador y necesario, sobretodo porque cada vez hay mas familias enlazadas o ensambladas que encontraran en esta formacion muy buenos consejos para lograr nuevas formas de relacionarse y organizarse en las que todos el mundo se sienta comodo y encuentre su nuevo lugar.

![pareja hombro con hombro](/images/uploads/inicio_creada_ok.jpg)

Tengo el honor de presentar una ponencia sobre la comunidad como sosten emocional, y compartir cartel con otros 29 profesionales mas que aportaran contenido de valor en un programa que no tiene desperdicio con temas como reinvencion familiar y conciliacion, redes de familias, la figura de la madreastra y padrastro, separacion y divorcio respetuosos, temas legales de interes...

Ademas, para llegar a todas las personas que lo necesitan y poder ayudarlas, Rocio y Miguel han tenido la generosidad de establecer un acceso gratuito para ver todas las poencias en tiempo real,que ademas lleva unos regalos incluidos.

![ponentes congreso nuevas familias](/images/uploads/feed-completo.jpg)

## Consigue tu pase para el Congreso



<a href="https://go.hotmart.com/S54715745P" class='c-btn c-btn--active c-btn--small'>Apuntate gratis al congreso</a>



Pero para quien no pueda ver las ponencias en directo, puede comprar el pase vip para poder disfrutarlas en el momento que lo desee, y por tiempo ilimitado, ademas de obtener unos bonus aparte de los regalos del pase general.

<a href="https://go.hotmart.com/S54715745P?dp=1" class='c-btn c-btn--active c-btn--small'>Comprar pase vip del Congreso</a>



## Comparte con quien pueda ayudarle este congreso

Si no es tu caso porque no te encuentras en situacion de separacion matrimonial, comparte este post con quien conozcas que pueda ayudarle esta formacion.

Os dejo con Rocio y Miguel Angel:

> La separación no solo no es algo malo, lo malo y dañino son todos los juicios y
> creencias limitantes que tenemos en torno a esta, como:
>
>
> Es un fracaso. Guau, cuánto pesa esta palabra ¿no? Si creemos que es un fracaso,
> evidentemente vamos a hacer TODO lo posible para evitarlo. Pero es que todo en la
> vida es cíclico, la Naturaleza es cíclica y las relaciones humanas también. Cuando en
> una pareja, cada parte crece a un ritmo diferente, en sentidos distintos o ya no se
> suma, no crecen juntas, no se aportan… ¿por qué seguir en la forma de pareja? ¿Por
> qué no permitir que la relación tome otra forma? ¿Por qué no dejar que el vínculo se
> transforme?
>
>
> Por los hijos e hijas. Claro, esto es lo que más nos han contado, que “hay que aguantar
> por las criaturas”. A lo mejor es que no nos hemos parado a pensar que mucho más
> daño que vivir con papá y mamá separados, les hace el hecho de que siga existiendo
> convivencia en una relación de pareja desgastada, que no solo no se suma sino que se
> resta, llena de hastío… Los niños y niñas no aprenden por lo que les decimos que
> hagan, sino por lo que nos ven hacer y somos su modelo, también como pareja. Por lo
> que si ven una relación basada en el conformismo, interpretarán que eso es amor y
> reproducirán relaciones de pareja del estilo.
>
>
> Los niños y niñas no nos necesitan conviviendo por encima de todo, sino amándoles.
> Necesitan sentir que les amamos por encima de todo y que pueden amar tanto a
> mamá como a papá. Pero no tiene por qué ser en el molde estándar de familia
> socialmente reconocida. Lo más importante no es que estemos juntos como madre y
> padre, sino que les amemos y seamos felices, pues nuestra felicidad les da alas a ellos y ellas para ser felices.
>
>
> ¿Cómo va a vivir sin mí? ¿Cómo voy a vivir sin él? Lo que hay detrás de esta pregunta
> no es más que dependencia emocional. Nos creímos el cuento de la media naranja y
> nos parece ahora difícil que nos podamos sentir completas nosotras mismas, sin una
> persona al lado que nos complete. Seguramente no te contaron que TÚ eres la
> persona más importante de tu vida y que con quien vas a pasar todos, todos, todos los
> días de tu vida va a ser contigo. Nos enseñaron, sobre todo a las mujeres, a contentar
> a los demás, a cuidar a los demás, por eso ahora nos cuesta esfuerzo cuidarnos,
> dedicarnos tiempo y espacio a nosotras mismas, así como sentirnos plenas y completas tal y como ya somos.
>
>
> Además de que nos han hecho creer que la vida en pareja es mucho mejor, pero no es
> real. Esta, las demás y otras muchas, son creencias que tenemos tan arraigadas en
> nuestro inconsciente que nos parecen verdad.
>
> Cuidamos muchos los inicios en la vida, especialmente los inicios de relaciones de
> pareja. Los cuidamos como lo más preciado del mundo, sin embargo, el proceso lo
> cuidamos menos y el final como pareja no solo no lo cuidamos, sino que lo
> descuidamos por completo.
>
>
> Tenemos tanto miedo a la separación que estiramos las relaciones de pareja hasta
> desgastarlo y entonces nos separamos cuando la relación ya está en la U.C.I. Eso hace
> que cuando llega el momento de la separación o divorcio sea muy fácil caer en el odio
> y entonces en la guerra.
>
> ## 
> Separacion consciente
>
>
> Consciente, incluso aunque solo una de las partes esté dispuesta a ello.
> Llegado el momento de la separación es fácil que se abran viejas heridas. Heridas que
> nada o poco tienen que ver con la relación de pareja. En ese momento existen dos
> opciones, vivir la separación desde las heridas, donde se termina siendo una niña o
> niño peleando, o desde la conciencia, es decir, desde el yo adulto.
>
>
> Vivir una Separación Consciente supone poner en el centro de cada decisión las
> necesidades de los hijos y las hijas, y para eso es crucial vivir la separación desde tu
> centro, desde tu yo adulto. Pues de lo contrario, si ambos os quedáis en vuestras
> heridas, termináis siendo dos niños peleando. En ese caso las criaturas se quedan
> huérfanas emocionalmente.
>
>
> Como te decía, la separación no solo no es dañina ni perjudicial para los hijos e hijas,
> sino que pueden adaptarse sin problema a la nueva estructura familiar. Ahora bien, los
> pilares y la estructura de la familia tal y como la han conocido hasta el momento es de
> una determinada manera, por ello el cambio de forma de su familia les puede generar
> mucho miedo e inseguridad. Por eso es muy importante cuidar la forma de llevar a
> cabo la separación y tener en cuenta sus necesidades emocionales.
>
>
> No se trata de proteger a los hijos a hijas de la separación, sino de los egos adultos. Lo
> dañino y malo no es la separación en sí, sino una mala gestión de la misma. Y es que
> cuando una relación de pareja se termina, la familia no se rompe, sino que cambia de
> molde.
>
>
> Pero claro, la culpa, la vergüenza y el resto de creencias de las que hablaba al inicio,
> dificultan muchísimos el proceso. Lo importante y lo que va a marcar mucho la
> diferencia en la adaptación a la nueva estructura familiar son dos cuestiones claves:
>
> * Que los hijos e hijas sepan que, aunque ya no seáis pareja, seguís siendo su madre y su padre SIEMPRE.
> * Que sabéis que os quieren muchísimo a ambos. Deben sentir el camino libre para
>   amaros a los dos y no que tienen que elegir.
>
> Una separación no solo no es final del mundo sino que puede ser una oportunidad
> para una vida mejor y un acto de amor, responsabilidad y generosidad.
>
>
> ¿Quieres saber más sobre separaciones conscientes? ¿Quieres conocer a parejas,
> madres y padres que las han llevado a cabo? ¿Quieres saber cómo lleva a cabo una
> Separación Consciente aunque una de las partes no esté dispuesta a ello?
>
>
> Entonces [inscríbete gratis en el Congreso Creando Nuevas Familias](https://go.hotmart.com/S54715745P).
>
> Quiza te interese este post sobre [que debes tener en cuenta para elegir un abogado o abogada para tu divorcio](https://madrescabreadas.com/2021/06/02/mejores-abogados-divorcio-respetuoso/).

*Foto de portada by Photo by Kin Li on Unsplash*