---
layout: post
slug: ideas-montar-belen
title: Ideas para montar el Belén
date: 2021-12-03T18:47:34.198Z
image: /images/uploads/ideas-montar-belen-navidad.jpg
author: luis
tags:
  - navidad
---
Empieza el mes de diciembre y con él todos los preparativos para una de las fiestas que más disfrutamos en familia, la navidad. La ilusión se apodera de los más pequeños, que ya a estas alturas solo son capaces de pensar en la llegada de Papá Noel y Los Reyes Magos. Y en estas fiestas no pueden faltar los elementos decorativos que dan sentido a estas fechas, como el árbol de navidad o el belén. De este último es del que vamos a hablar más en este artículo en el que os vamos a dar las claves e ideas originales sobre cómo montar el belén de navidad.

Os dejamos este [paso a paso para montar el Belen de Navidad en casa](https://madrescabreadas.com/2015/12/13/pasos-hacer-belen/).

La historia del belén de navidad se remonta a 1223 y su “invención” se atribuye a San Francisco de Asís, quién elaboró el primer pesebre que se conoce, para lo que usó arcilla del bosque de Greccio. Su intención fue dar a conocer a los feligreses el significado del nacimiento de Jesús, sin saber que esto se convertiría en una tradición que se extendió por toda Europa a partir del siglo XVIII.

## ¿Dónde poner el Belén en casa?

Montar el belén en familia es una de las actividades que cada año no pueden faltar por estas fechas navideñas. Seguramente tengas ya un sitio asignado pero si no es así, antes de empezar tendremos que encontrar la ubicación ideal para nuestro pesebre. 

![belen en casa](/images/uploads/cuando-montar-belen-navidad.jpg "belen en casa")

Lo ideal es elegir una buena ubicación, sobre todo en aquellas estancias donde más vais a vivir todo el ambiente navideño. Las zonas del salón, el comedor o el recibidor suelen ser las opciones más escogidas, aunque también es algo que va a depender de las dimensiones del belén. Además, lo más recomendable es montarlo en un sitio alto, pero que a su vez no tenga riesgo de que se puedan caer ninguna de las figuritas. Recomendamos hacerlo sobre una superficie plana y que sea lo suficientemente grande para que nos quepan todos los protagonistas del pesebre.

Como te decimos, también va a depender del tamaño de nuestro belén, ya que puede ser de los más sencillos, aquellos que incluyen el pesebre y las piezas más destacadas, o verdaderas obras de artes, con casas, zonas delimitadas, con ríos y cascadas para belenes, etc. En esos casos, cuando la pasión por este elemento decorativo va mucho más allá, quizás colocarlo todo en una habitación puede ser la mejor opción. 

## ¿Cómo se colocan las piezas del belén?

Es cierto que muchas veces surgen dudas a la hora de colocar cada pieza en el belén y puede parecer una tontería pero hay mucha gente que les gusta hacerlo todo a la perfección y siguiendo la tradición. Dentro del pesebre podemos encontrar muchas figuras, pero, sin dudas, la principal es el niño Jesús. Este irá lo más centrado dentro del pesebre y desde esta pieza se irán colocando el resto a su alrededor. 

Por norma general, la Virgen María se coloca a la izquierda del niño y el buey a sus espaldas, mientras que la figura de San José, irá situado a la derecha del niño y con la mula a sus espaldas. Los pastores que fueron los primeros en presentar sus respetos al recién nacido con regalos, se suelen colocar alrededor del mismo. Otro de los protagonistas es el Ángel que anunció el nacimiento de Jesús a los pastores y su sitio en el belén suele ser o dentro del pesebre o encima de él. Finalmente, no podemos olvidarnos de los Tres Reyes Magos que llegaron desde oriente para agasajar al niño con presentes. La ordenación de sus majestades también suele seguir siempre el mismo orden, siendo Melchor el más adelantado, Gaspar en el medio y Baltasar que se coloca a la cola. 

## ¿Cuándo hay que montar el Belén?

La tradición en nuestro país también nos indica cuándo es el momento de montar el belén y para que no te pille el toro, te lo contamos. Esto simplemente es tradición, realmente cada uno elige cuándo empezar la decoración de su hogar en la navidad, pero si seguimos con lo estipulado es en el Puente de la Inmaculada Concepción cuando la mayoría de los hogares españoles se deciden a decorar sus casas, siendo el belén una de las partes más importantes de esta decoración en la tradición cristiana.

![figuras belen](/images/uploads/que-figuras-hay-en-el-belen.jpg "figuras belen")

Eso sí, no todas las figuritas se colocan en dichos días. La figura de el Niños Jesús no se coloca hasta el día 24 de diciembre, coincidiendo con su nacimiento y el 25 es cuando se colocaría el Ángel anunciador. Hay quienes tampoco colocan los Reyes Magos hasta el día previo de su llegada, el 6 de enero.

## ¿Qué figuras lleva un Belén?

El belén es un elemento decorativo tan típico de la Navidad que la mayoría de nosotros sabemos perfectamente cuáles son las figuras que se incluyen en él. Eso sí, el belenismo está tan extendido y hay gente, tanto particular como cofradías, que lo viven tanto que son capaces de recrear toda una ciudad hebrea. Serían innumerables los diferentes tipos de figuras que puede llevar un belén, sin contar con elementos arquitectónicos y naturales que se añaden para dar mayor realismo. Cascadas con agua en el belén, puentes, casas y mucho más se puede añadir a tu belén, siempre y cuando tengas espacio para ello.
Sin embargo, las figuras más típicas de todo belén y que nunca pueden faltar son: el Niños Jesús, la Virgen María y San José, los cuales estarían rodeados de animales de corral como el burro, la mula o el buey. El Ángel anunciador es otro de los imprescindibles, junto con la estrella de belén y los Reyes Magos. Luego ya podrían unirse pastores y otras figuras que ayudan a dar vida al pueblo hebreo, como artesanos y trabajadores.

## Ideas Originales para montar el Belén en Familia

Ahora que ya sabemos un poco más sobre el belén, su historia y las piezas claves que no deben faltar en tu pesebre, es el momento de que te ofrezcamos una serie de ideas originales para salirnos de lo tradicional y dar un toque diferente a nuestro belén, haciendo así esta experiencia más divertida para montar el belén en familia.

* **Belén de Lego, Playmobil, etc.**: desde hace unos años las marcas de juguetes más reconocidas han sacado al mercado sus versiones del belén de navidad. Playmobil o Lego son algunas de estas marcas que nos ofrecen un pesebre diferente con el que dar un toque original y divertido a la navidad de los más pequeños.
* **Belén de manualidades**: otra opción muy interesante y con resultados muy originales es el de crear nuestro propio belén a partir de manualidades. En Pinterest puedes encontrar un montón de ideas como hacer un belén pintado en piedras, crear tu propio belén a partir de corcho u optar por el más sencillo belén de papel que podemos hacer que los más pequeños dibujen.
* **Decorados divertidos**: y como alternativa también podemos centrarnos en los decorados, aunque luego usemos las piezas del belén de siempre, las clásicas. Una idea muy recurrente es la de crear un río o cascada en el belén, para lo que se suele usar espuma de poliuretano, dando un toque muy original y divertido.

Con todo esto, creemos que ya estáis más que preparados para atreveros con esta actividad en familia que seguramente sea muy gratificante para todos. Ahora que ya sabes cómo montar el Belén, solo queda que os reunáis toda la familia, os repartáis las tareas y os pongáis manos a la obra para tener la casa a punto para estas fiestas que están por llegar.

Sitios de interes: [Federacion Española de belenistas](http://anunciata.es).