---
layout: post
slug: control-parental-moviles
title: Control parental en móviles si o no
date: 2020-06-24T18:19:29+02:00
image: /images/posts/control-parental-moviles/p-piscina.jpg
author: maria
tags:
  - adolescencia
---

Ocho de cada diez adolescentes saben perfectamente que usan demasiado el móvil; el 58% de entre 12 y 13 años pierde la noción del tiempo cuando está navegando, un 51% se enfada si le interrumpen y el 67% abandona lo que está haciendo para atender al móvil llegando incluso a bajar su rendimiento escolar. Así lo pone de manifiesto el estudio sobre concienciación y uso del móvil entre adolescentes realizado por Celside Insurance y Pantallas Amigas, con la colaboración de la Universidad de Deusto. En este post analizaremos estos hábitos y qué podemos hacer los padres a nivel de educación digital y haciendo uso de herramientas de control parental que están ofreciendo algunas compañías como [Movistar](https://www.moviles.com/movistar).

## ¿Por qué enganchan las redes sociales a los adolescentes?

Un 33% de los jóvenes encuestados ha pensado algunas veces que la vida sin internet es aburrida y triste, y un 61,3% duerme con su teléfono móvil en la habitación (si no lo habéis hecho, os recomiendo la lectura de este post sobre [dormitorios para mejorar el suelo de los adolescentes](https://madrescabreadas.com/2019/04/22/dormitorios-adolescentes-ikea/)) donde uno de los consejos que doy es sacar el móvil de la habitación para dormir.

Las redes tienen una destacada influencia sobre la imagen que algunos jóvenes tienen de sí mismos y de su entorno: el 20% considera que los likes y el número de seguidores le aporta popularidad, un 16% se compara con su entorno y un 12% necesita subir contenido para que sepan qué hace o dónde está. Además, un 13% acepta a desconocidos para ganar seguidores y likes.

![escaparate pisamonas cartagena](/images/posts/control-parental-moviles/holly.jpg)

Pero a los padres nos preocupa no sólo el tiempo que pasan frente  a la pantalla, sino lo que hacen mientras están conectados, y es que el 69% de los jóvenes emplea el móvil para conectarse a las redes sociales, medio en el que el 35% ha hecho nuevas amistades; al 49% le resulta una manera más fácil de relacionarse que en persona y un 42% lo considera un refugio cuando está triste.

## Habla con tus hijos adolescentes sobre las redes sociales.

El problema es que muchos padres no conocen las aplicaciones que usan sus hijos ni cómo pueden enseñarles a configurar la privacidad de las mismas. Por eso lo primero que recomiendo es que dediquemos un tiempo a ponernos al día con webs que nos ayudarán, como [IS4Kids, como os cuento en este post](https://madrescabreadas.com/2017/04/27/seguridad-internet-menores-incibe/), y que nos interesemos de forma constructiva (no para censurar ni escandalizarnos), por lo que suelen hacer nuestros hijos cuando están con el móvil. Por ejemplo, podemos pedirles que nos muestren las aplicaciones que usan, par qué sirven, en qué consisten y por qué les gusta usarlas. Podemos sugerirles que restrinjan la configuración de privacidad explicándoles los peligros que pueden correr, contándoles incluso casos reales y apelando a su sentido común.

![escaparate pisamonas cartagena](/images/posts/control-parental-moviles/familia.jpg)

## Control parental sí, pero informado

Con el control parental los adultos tienen la posibilidad de restringir o bloquear el dispositivo electrónico que el niño esté utilizando. Además del dispositivo, también podrá bloquear algunas búsquedas, aplicaciones o compras en concreto.

![escaparate pisamonas cartagena](/images/posts/control-parental-moviles/taburete.jpg)

Un 41% de los padres afirma haber cogido el móvil de su hijo sin su permiso, pero es mejor explicar lo que vamos a hacer y por qué. Yo suelo decirles que al igual que no dejamos que salgan solos a la calle hasta cierta edad, y a partir de ella, con unas normas y horarios y advirtiéndoles de los peligros que pueden corre, en el uso de Internet pasa lo mismo, debemos tener un control sobre lo que hacen en la red porque es incluso más peligroso que salir  la calle, ya que no puedes estar seguro de la identidad de tu interlocutor, ano ser que lo conozcas de antes.

## El ejemplo de los padres con el móvil

El 26% de los estudiantes cree que sus progenitores emplean más el móvil que ellos
Dar ejemplo es importante, ya que debemos aprovechar la oportunidad de educar con nuestros propios actos en el uso consciente y responsable del móvil. Somos los mayores influencers de nuestros hijos, pero tendemos a subestimarlo. Si al buen ejemplo le añadimos persuasión, complicidad, acompañamiento y supervisión estaremos formando personas más críticas, responsables y autónomas en el uso del móvil y de Internet en general.

Espero que te haya servido de orientación, y si lo encuentras útil comparte para ayudar a más familias. De ese modo también me ayudarás a que pueda seguir escribiendo este blog.





*Photo by Bruno Gomiero on Unsplash piscina*

*Photo by John Schnobrich on Unsplash familia con Pantalla* 

*Photo by Charles Deluvio on Unsplash niño bajo taburete*

*Photo by Julián Gentilezza on Unsplash selfie en grupo*