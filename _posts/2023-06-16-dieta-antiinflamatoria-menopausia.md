---
layout: post
slug: dieta-antiinflamatoria-menopausia
title: Podcast cómo adelgazar sin cabrearte
date: 2023-06-16T12:14:25.495Z
image: /images/uploads/mariana-egosi-copia.png
author: .
tags:
  - podcast
---
## Intensivo de 6 semanas para perder grasa y aprender a llevar una vida saludable sin cabrearte

Apuntate al Programa de 6 semanas [Fasterway to fat Loss](https://www.fasterwaycoach.com/#madrescabreadas)

<a href="https://www.fasterwaycoach.com/#madrescabreadas" class='c-btn c-btn--active c-btn--small'>Me apunto al intensivo de 6 semanas</a>

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6IiyhTCFPZUa6vlbcscKJ6?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

## ¿Que se puede tomar para bajar de peso en la menopausia?

Mujer de 40 o mas, seguro que te has preguntado:

\-¿Por qué no adelgazo haciendo lo mismo que cuando tenía 30 años?

\-Tengo la menopausia y he subido 3 tallas. El médico me dice que no hay nada que hacer, que me ha tocado, y me ha tocado. Estás de acuerdo?

\-Tengo hijos adolescentes y vivo en una montaña rusa de emociones, además estoy empezando con la menopausia. Ni me planteo empezar a hacer dieta, pero no me reconozco en el espejo, además de no encontrarme bien: no duermo bien, me dan sofocos y tengo ganas de matar a todo el mundo. Me veo incapaz de hacer dieta. No estoy dispuesta a pasar hambre, no puedo permitirme ese lujo en este momento.

[Mariana Egosi](https://www.instagram.com/mariana_egosi/) nos va a yudar a perder peso sin cabrearnos y sin pasar hambre.

[Mariana](https://www.marianaegosi.com) es Health coach de wellness certificada por The Institute for Intergrative Nutrition of New York

Coach y pionera de FASTEr Way to Fatloss en español.

Actualmente esta cursando un diplomado en medicina funcional y anti-aging

## ¿Cómo bajar 10 kilos en menopausia?

Ese es mi proposito, y gracias al programa Fasterway voy consiguiendo poco a poco bajas perdiendo grasa y ganando musculo. Sin prisa , pero sin pausa.

## ¿Cómo eliminar la grasa abdominal de la menopausia?

Yo jamas he tenido barriga, pero desde hace tiempo no hay manera de deshincharme. La clave esta en los alimentos antiinflamatorios y el ejercicio fisico de fuerza.

## ¿Cómo se activa la hormona para bajar de peso?

En el programa Mariana tiene muy en cuenta el funcionamiento de nuestras hormonas, y da una charla sobre como manejarlas muy interesante.

![](/images/uploads/maria-fasterway-.jpeg)

Quiza te interese:

[Como reducir el azucar en la dieta de tus hijos](https://madrescabreadas.com/2021/04/11/cómo-reducir-el-azúcar-de-la-dieta-de-los-niños/)

[No es la leche, son los bollos](https://madrescabreadas.com/2019/12/10/azucar-leche/)