---
layout: post
slug: mascarillas-en-colegios-si-o-no
title: "Vlog unidos por la infancia: fuera mascarillas en los patios de los colegios"
date: 2021-10-20T09:01:51.413Z
image: /images/uploads/unidos-por-la-infancia-mascarillas.png
author: maria
tags:
  - vlog
  - salud
  - mecabrea
---
> Hemos querido unirnos para poner voz a aquellos que no la tienen.
>
> Recordemos que:
>
> • No han elegido ser héroes, ni tienen que dar ejemplo (eso nos corresponde a los adultos) 
>
>
> • Mientras poco a poco volvemos a la normalidad, ellos no se pueden quitar la mascarilla mientras juegan en el patio al aire libre o practican actividad física en exterior 
>
>
> • Mientras les decimos que no se la pueden quitar, ven cómo en otros ámbitos sí está permitido estar sin ella
>
>
> • La salud emocional IMPORTA y esto acabará pasando factura (ya lo está haciendo, de hecho).
>
>
>
> Las ratios en la mayoría de los coles no siguen siendo las que eran bajo las medidas COVID. ¿Aumentamos las ratios pero no permitimos que estén al aire libre sin mascarilla?
>
> Solo pedimos coherencia.
>
> ## ¿Cómo puedes ayudar a #UnidosPorLaInfancia?
>
> Si a ti te resuena este mensaje, si a ti te cala, si tú también crees que merecen tener voz, por favor, comparte este vídeo, utiliza el hatshtag #Unidosporlainfancia, comenta, guárdalo… si no, no servirá de nada.

*Texto de [@unamadremolona](https://unamadremolona.com)*

<iframe width="560" height="315" src="https://www.youtube.com/embed/wUdlhqTjvUI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Compate en tus redes sociales para que se nos oiga!

Te dejo tambien este post sobre [como llevar la mochila para proteger la espalda](https://madrescabreadas.com/2016/09/13/como-llevar-mochila/) de nuestros hijos