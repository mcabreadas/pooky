---
layout: post
slug: recetas-de-navidad-saludables
title: Primer plato, segundo y postre para una Navidad saludable
date: 2021-12-14T15:29:16.601Z
image: /images/uploads/istockphoto-1257064029-612x612.jpg
author: faby
tags:
  - recetas
---
Nuestro paladar ya se está preparando para la época navideña. Y es que no es un secreto para nadie lo difícil que es mantener la figura en estos momentos de gran festividad y variedad de platos suculentos a degustar.

Ya os propuse unos dulces navideños caseros en este post.

Sin embargo, el menú navideño no tiene por qué ser innegablemente una avalancha de calorías. Te propongo **recetas saludables y muy deliciosas** que podrás comer sin remordimientos de conciencia.

A continuación veremos 3 recetas sencillas y muy saludables para navidad

## Soufflé de tomate y queso de cabra

![Soufflé de tomate y queso de cabra](/images/uploads/souffledequesoytomate.jpg "Soufflé de tomate y queso de cabra")

Esta es una receta muy ligera y sencilla de realizar. Es excelente para servir como plato principal o como contorno acompañante. Aunque este plato francés originalmente es preparado con salsa bechamel y queso, **haremos una ligera modificación para hacerlo más ligero de calorías** pero igual de delicioso.

* En un recipiente refractario debe rellenarlos con capas de tomates en rodajas y tiras de queso de cabra. 
* Luego **realizas el soufflé con claras de huevos con la ayuda de un batidor** hasta dejarlo a punto de nieve. 
* Añades a la mezcla sal y pimienta al gusto e igualmente agregas la albahaca y el tomillo y sigues mezclando.

[![](/images/uploads/bosch-styline-colour.jpg)](https://www.amazon.es/Bosch-Styline-Red-Diamond-MFQ40304/dp/B005J49SNW/ref=sr_1_27?__mk_es_ES=ÅMÅŽÕÑ&keywords=batidor&qid=1637097261&sr=8-27&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Bosch-Styline-Red-Diamond-MFQ40304/dp/B005J49SNW/ref=sr_1_27?__mk_es_ES=ÅMÅŽÕÑ&keywords=batidor&qid=1637097261&sr=8-27&th=1&madrescabread-21) (enlace afiliado)

## Lenguado Menier a la mantequilla de estragón

![Lenguado Menier a la mantequilla de estragón](/images/uploads/lenguado-alcaparras.jpg "Lenguado Menier a la mantequilla de estragón")

El [lenguado](https://www.lavanguardia.com/comer/20180816/451331137009/lenguado-valor-nutricional-porpiedades-beneficios.html) es ideal para una receta baja en calorías, **especialmente recomendado para las personas que padecen de colesterol alto**. Es un platillo muy suculento al paladar que no te resistirás a preparar.

* Comienza triturando 400 gr de merluza junto a 400 gr de nata para montar y añade estragón picado hasta formar una crema fina capa. 
* Agregar sal y pimienta al gusto y dejar reposar. 
* Luego **en un recipiente refractario se puede colocar el lenguado** de tal forma que formen un círculo para finalmente rellenar con la crema de merluza.

[![](/images/uploads/juego-de-3-bandejas-de-cocina-de-vidrio.jpg)](https://www.amazon.es/TU-TENDENCIA-ÚNICA-microondas-Congelador/dp/B06XVJR3SY/ref=sr_1_8?__mk_es_ES=ÅMÅŽÕÑ&crid=3AURSEIWVW6Z2&keywords=refractario+para+horno&qid=1637097805&sprefix=refractario+%2Caps%2C471&sr=8-8&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/TU-TENDENCIA-ÚNICA-microondas-Congelador/dp/B06XVJR3SY/ref=sr_1_8?__mk_es_ES=ÅMÅŽÕÑ&crid=3AURSEIWVW6Z2&keywords=refractario+para+horno&qid=1637097805&sprefix=refractario+%2Caps%2C471&sr=8-8&madrescabread-21) (enlace afiliado)

* Hay que meter el recipiente refractario al horno a una temperatura de 200 °c por un espacio de 10 minutos. 
* Mientras se hornea el preparado, puedes sofreír en un sartén con mantequilla un poco de estragón picado sobrante. 
* Apaga la sartén e incluye zumo de limón y alcaparras.
* Baña el lenguado de esta sabrosa salsa y también será un excelente acompañante de unas papas al horno.

## Plátanos flambeados con naranjas al caramelo

![Plátanos flambeados con naranjas al caramelo](/images/uploads/platanosnaranja.jpg "Plátanos flambeados con naranjas al caramelo")

Por su naturaleza dulce el plátano es la fruta **ideal para preparar postres sin necesidad de recurrir al azúcar refinada** y su aporte calórico no es elevado. Además de estos beneficios, el plátano nos entrega un sabor especial al ser flambeado. Pero calma, tampoco hay que ser un chef profesional para realizar este suculento postre. Realmente es muy sencillo y rápido de preparar.

* Para comenzar debes saber que el plátano debe estar lo suficientemente maduro para garantizar que esté lo más dulce posible, pero que no haya perdido su firmeza. 
* Quita la concha y divide el plátano a lo largo en 2.
* Por otro lado, toma unas naranjas, quitales la concha y corta en rodajas. 
* Deja un par de naranjas apartadas para exprimirlas y extraerles todo el zumo.

[![](/images/uploads/bra-efficient-orange.jpg)](https://www.amazon.es/BRA-Efficient-sartenes-Antiadherente-inducción/dp/B01M27LTK2/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&keywords=sartén&qid=1637098047&sr=8-5&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/BRA-Efficient-sartenes-Antiadherente-inducción/dp/B01M27LTK2/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&keywords=sartén&qid=1637098047&sr=8-5&madrescabread-21) (enlace afiliado)

**Calienta en un sartén; aceite y mantequilla**. 

* Una vez caliente añade en la sartén los plátanos procurando que se cocinen por ambos lados. 
* Ahora incluye las rodajas de naranja con dos cucharas pequeñas de azúcar para acaramelar y si lo deseas puedes agregar una copita de ron encima. 
* Realiza el flambeado con la ayuda de un mechero.
* Finalmente vierte el zumo de naranja sobre toda la preparación y ya está listo para ser servido en cada plato.

Te dejo también estas [recetas de helados caseros saludables](https://madrescabreadas.com/2021/07/15/helados-caseros-saludables/).

Si te ha gustado, comparte en tus redes sociales.

*Photo Portada by supersizer on Istockphoto*