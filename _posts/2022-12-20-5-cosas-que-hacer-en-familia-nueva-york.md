---
layout: post
slug: planes-nueva-york-ninos-adolescentes
title: Los 5 planes en Nueva York que tus adolescentes sí querrán hacer contigo
date: 2022-12-21T09:00:40.571Z
image: /images/uploads/depositphotos_127955130_xl.jpg
author: faby
tags:
  - adolescencia
---
Nueva York es conocida como **la ciudad que nunca duerme**. Y es que, su ritmo frenético hace que puedas aprovechar literalmente las 24 horas del día.

Si en otro post te hablabamos de los [mejores hoteles de Disneyland,](https://madrescabreadas.com/2021/08/03/mejor-hotel-disneyland-para-familias-numerosas/) en este nos centramos mas en tus hijos adolescentes, y en las actividades en familia que podran interesarles, ya que a esa edad suelen ser muy criticos con los planes, y tenemos que adaptarnos bastante a ellos. 

Pues bien, **hay muchas cosas que ver o disfrutar en esta ciudad,** ¿tienes hijos adolescentes? ¿Tienes niños? Acá te presento los mejores planes en familia en Nueva York. ¡Empecemos!

## 1. Basket en New York

¿Sabías que en Nueva York está el estadio más famoso del mundo? ¡Sí!, acá puedes encontrar el **Madison Square Garden.**

![Basket madison square garden](/images/uploads/basket-madison-square-garden.jpg "Basket madison square garden")

Si tu hijo es fanático del basket, puedes incluir en el tour dos **partidos de la NBA en vivo.** De hecho, puedes hacer que este plan sea aún más interesante con *[24Trip](https://24trip.es/)*; una plataforma en la que puedes reservar tus entradas.

Por cierto, con esta plataforma digital puedes incluso disfrutar de un día de **esparcimiento por la Gran Manzana, la Quinta Avenida,** zonas turísticas, etc.

## 2. Visita el Central Park

Si tienes niños de seguro desearás ir a un **parque infantil,** pues bien, en el Central Park encuentras decenas de parques infantiles para niños de distintas edades.

![Familia Central Park](/images/uploads/depositphotos_145265457_xl.jpg "Familia Central Park")

Es uno de los **espacios verdes de Nueva York más amplios y hermosos.**

Algunos de sus atractivos son los lagos, sus mágicos senderos, así como el jardín del Invernadero. Está abierto todo el año, y puedes recorrerlo con un guía.

## 3. Teatro New Ámsterdam

Puedes disfrutar de una **producción de Disney** en el teatro New Ámsterdam. Este es un buen plan si tienes niñas.

![Teatro New Ámsterdam](/images/uploads/teatro-new-amsterdam.jpg "Teatro New Ámsterdam")

Allí puedes ver **preciosos musicales de los clásicos cuentos infantiles** como Frozen, Aladdin, el Rey León, etc.

Cabe destacar que, visitar este lugar es una experiencia de satisfacción para toda la familia, pues se trata de un **edificio de más de un siglo de antigüedad.** Su arquitectura rememora eventos antiguos.

## 4. Estatua de la Libertad

¿Quién no ha conoce la [Estatua de la Libertad](https://es.wikipedia.org/wiki/Estatua_de_la_Libertad)? Es un clásico de las películas. Pues bien, puedes visitarla en ferri e incluso **subir a la corona de la estatua y vislumbrar todo Nueva York.**

![Liberty Island con vistas al horizonte de Manhattan](/images/uploads/liberty-island-con-vistas-al-horizonte-de-manhattan.jpg "Liberty Island con vistas al horizonte de Manhattan")

La aventura empieza en el ferry en el que puedes conocer la isla de la Libertad.

Puedes hacer un recorrido dentro de la estatua y conocer su historia. Ir hasta lo alto es una experiencia enriquecedora para toda la familia.

## 5. Museo Americano de Historia Natural

¿A tus hijos les gustan los dinosaurios? El museo americano de historia natural posee **fósiles prehistóricos**, así como exposiciones que dejarán a todos sin aliento. Es un lugar en el que niños y adultos podrán aprender muchísimo.

![Museo Americano de Historia Natura](/images/uploads/museo-americano-de-historia-natura.jpg "Museo Americano de Historia Natura")

Allí podrás ver un **esqueleto del tiranosaurio de 37 metros de largo.** Es un lugar en el que es imposible aburrirse. De hecho, podrán conocer sobre las galaxias en el **planetario Hayden**. Hay divertidos juegos, talleres y mucho más.

En conclusión, en Nueva York hay muchos lugares que se pueden visitar en familia. Puedes disfrutar de la pasión del basket en el estadio más famoso del mundo o visitar el museo con la mayor recaudación de restos fósiles.