---
layout: post
slug: ideas-noche-reyes-magica
title: Ideas para preparar la noche de Reyes de forma mágica
date: 2021-01-04T08:19:00.000Z
image: /images/posts/preparar-noche-de-reyes/p-reyesmagos.png
author: ana
tags:
  - navidad
  - planninos
  - 3-a-6
  - 6-a-12
---
Ha nacido el Niño en Belén y Tres Magos de Oriente vienen a adorarlo. Nuestros niños son ese hijo de Dios, y con estas ideas para preparar la noche de reyes dejémonos llevar por la ilusión y hacer que reine la alegría y la esperanza en los corazones de la casa.

## Ideas para preparar una noche de Reyes mágica

La noche de reyes es sin duda la más esperada. La emoción de los niños en casa pide ser acompañada de magia y sorpresas. Te mostramos algunas alternativas sencillas, frases y estrategias para hacer de esa noche la más especial del año que comienza.

Lo primero es que redacten la carta a sus Majestades de oriente. Si no saben que pedir, puedes orientarle con estas [recomendaciones de juguetes segun la edad](https://madrescabreadas.com/2016/12/08/cómo-preparar-la-carta-a-los-reyes-magos-según-la/).

Podemos hacer la espera más entretenida leyendo todos juntos cuentos clásicos, como os sugería en este post de [12 cuentos clásicos para leer esta Navidad](https://madrescabreadas.com/2020/12/07/cuentos-navidad/), o viendo un película navideña en familia de las que os contaba en este otro post de [las mejores películas navideñas para ver en familia](https://madrescabreadas.com/2020/12/06/peliculas-navidad-ver-familia/).

### Receta de ambientador natural de Navidad

Prepara la noche de Reyes de una forma para que sea más mágica aún.

La magia también se puede lograr a través del olfato, ya que nos evoca sensaciones o despierta emociones como la ilusión.

Logra que tu hogar huela a Navidad en la noche más bonita del año para los niños con esta sencilla receta.



[![naranja manzana y arandanos rojos](/images/uploads/photo_2023-01-03-12.49.54.jpeg)](https://jspc.es/madrescabreadas)


\
**Ingredientes**

\-arándanos rojos 2 puñados

\-1 manzana

\-1 naranja

\-Una cucharda de [mix carrot cake de Just Spices](https://jspc.es/madrescabreadas)

**Elaboracion**

\-Pon los arandanos, la manzana y naranja a rodajas y los polvios de mix para carrot cake de just spices en una olla o cacerola.

\-Cubre con agua en una cacerola

\-Cuece a fuego lento 20’ sin tapar y disfruta del aroma



### El camino encantado

Primero, coloca los regalos de manera disimulada en distintos rincones de la casa. Luego, puedes marcar la ruta para encontrarlos con pegatinas de estrellas. Si lo deseas usa cintas, o caramelos con envolturas vistosas, cajitas o cofres que contengan pistas para avanzar hasta el lugar donde los regalos esperan.

### La carta con pistas

Las cartas no pasan de moda y a los Reyes Magos les va muy bien que dejen en la casa una señal escrita de su paso. Más mágica será si está dirigida a los niños dejando constancia del deseo hecho regalo.

Ponle más emoción. La carta contiene pistas, y traza un recorrido: ¡es un mapa! Síguelo y el sueño se hará realidad.

Incluso, para darle más realismos se puede solicitar en [esta web](https://www.cartadelosreyesmagos.es/index.html) una carta dirigida a tu peque con una respuesta personalizada de los Reyes Magos.

Pero si queremos sorprenderlos más podemos incluso solicitar una [video llamada con sus majestades en esta web](https://www.losreyesmagos.tv/escoge-rey-favorito/).

### Pruebas de la presencia mágica

![noche-de-reyes](/images/posts/preparar-noche-de-reyes/noche-de-reyes.png)

Resulta mágico que las cosas aparezcan o desaparezcan. Pero nada le gana a la prueba de la existencia de una realidad invisible que, de pronto está ahí, frente a nuestros ojos.

Prueba a dejar en el sitio de los regalos la corona de uno de los reyes, la huella de sus pisadas, bien de sus zapatos o la pezuña de un camello. Para lograrlo usa una plantilla y rocíale a través harina o arena blanca.

De pronto, deja como abandonado algún anillo de fantasía con una piedra grande y colorida. También restos de paja esparcida en el lugar, restos de la Cabalgata, o un cofre de fantasía y recamado, que contenga dulces.

La idea es dejar a la vista algo exótico, como venido del lejano oriente, una prueba de la la presencia de tan ilustres Magos.

### El rompecabezas

Toma una hoja de color y escribe en ella el lugar o los lugares donde se encuentran los regalos. Traza figuras y recorta. Cada pieza del rompecabezas contiene frases como estas:

* La luz de la estrella de Belén que nos guía, conduce tus pasos a la felicidad.
* La magia es que seas feliz.
* Ama y los sueños se harán realidad.
* Hemos atravesado el desierto para llegar hasta ti, sé feliz y bueno.
* Recibirás un regalo, pero el mejor lo tienes ya a tu alrededor.

Acompaña algunas de estas frases con la pista para llegar a la siguiente pieza del rompecabezas. La primera pieza y la primera pista, se pueden encontrar al pie del Árbol de Navidad.

## Frases para poner en los regalos de reyes

![regalo-de-reyes](/images/posts/preparar-noche-de-reyes/regalo-de-reyes.png)

Y cuando lleguen a los regalos, aprovecha para que descubran algunas palabras llenas de profundidad y belleza, una oportunidad especial para enseñar lo verdaderamente importante.

* El regalo no es lo que vez, sino la alegría de tus ojos.
* Lo material lo tienes en tus manos, la magia está en tu corazón.
* Tu risa ilumina el nacimiento del Niño Dios, es la estrella de Belén que nos guía.

Prueba con estas ideas para preparar la noche de reyes y recuerda que lo inolvidable es crear la atmósfera de emoción y sorpresa. El regalo inmaterial perdurará en el recuerdo, en la magia de lo increíble.

A veces laemocion de esta noche tenespeciahace dificil que los niños sy niñas se duerman, por eso te dejamos estos [trucos para que los niños se vayan a la cama en la noche de Reyes](https://madrescabreadas.com/2021/01/05/noche-de-reyes-ninos-dormir/).

Si tienes hijos mayores te recomendamos [4 formas de pasar la noche de Reyes con tus adolescentes](https://madrescabreadas.com/2022/01/03/celebrar-reyes-con-adolescentes/).

Feliz Noche de Reyes!

Si te ha gustado, comparte y suscríbete gratis al blog para no perderte nada!

<a href="<https://mailchi.mp/cd4b077ecacf/nete-a-madres-cabreadas>" class='c-btn c-btn--active c-btn--small'>Quiero suscribirme gratis</a>