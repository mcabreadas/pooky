---
layout: post
slug: teletrabajo
title: Como teletrebajar desde casa de forma eficiente
date: 2020-09-27T10:19:29+02:00
image: /images/posts/teletrabajo/p-escritorio.PNG
author: maria
tags:
  - conciliacion
---

El pasado 22 de septiembre se aprobó el [Decreto del Teletrabajo](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2020-11043), una norma que, si bien no lo establece de forma obligatoria, sí responde a una necesidad social de contemplarlo de forma seria y regular el estatus de los trabajadores que se acogen esta modalidad de trabajo no presencial. El un logro y una viejísima reivindicación de este humilde blog y de su comunidad, como osé reflejar en este [post de 2011 sobre mis propuestas para la conciliación laboral y familiar](https://madrescabreadas.com/2011/10/21/mis-propuestas-legislativas-para-la-conciliación/) y así lo estamos acogiendo las madres, pero no deja de sorprender que haya tenido que asolarnos una pandemia mundial para que por fin nos hayamos dado cuenta de la necesidad imperiosa de conciliar para la supervivencia de a especie humana, y de que una de las soluciones más sencillas para ello es algo tan de sentido común como el teletrabajo.
> 
> REGALO: Al final del post os dejo un descargable para que podáis imprimir un cartel de "no molestar" muy especial.

No voy a desmenuzar el decreto porque esto no es un blog jurídico ~~y además perdería a la mitad de mis lectoras~~, pero sí voy a señalar 6 puntos prácticos que me han parecido interesantes a tener en cuenta:

## 6 claves del Decreto de Teletrabajo

- La voluntariedad del teletrabajo por ambas partes (trabajador y empresa). En ningún caso se puede obligar a un trabajador a acogerse a esta modalidad de trabajo, ni a a empresa tampoco.

- A Los trabajadores que teletrabajan no se les puede discriminar por este motivo y tienen los mismos derechos que el resto en cuanto a promoción, formación prevención de riesgos laborales...

- Es la Empresa la que dota de los equipos necesarios y sufraga los gastos derivados del teletrabajo y asistencia técnica si fuera necesaria.

“El desarrollo del trabajo a distancia deberá ser sufragado o compensado por la empresa, y no podrá suponer la asunción por parte de la persona trabajadora de gastos relacionados con los equipos, herramientas y medios vinculados al desarrollo de su actividad laboral.”

- Los tele trabajadores tienen Derecho a horario flexible en los términos que acuerden con la empresa. Es decir, la flexibilidad horaria debería presuponerse porque (para eso existe esta figura, porque si no, no tendría sentido como solución  la conciliación), pero los términos de dicha flexibilidad deben pactarse por ambas partes.

- “De conformidad con los términos establecidos en el acuerdo de trabajo a distancia y la negociación colectiva, respetando los tiempos de disponibilidad obligatoria y la normativa sobre tiempo de trabajo y descanso, la persona que desarrolla trabajo a distancia podrá flexibilizar el horario de prestación de servicios establecido.”

- Derecho a la Desconexión digital.  

![vaso de cafe y libreta](/images/posts/teletrabajo/vaso.PNG)

-No obligatoriedad de usar dispositivos personales para tareas del trabajo.

## Tips para teletrabajar 

Lo primero es que siento deciros que el teletrabajo no es para todo el mundo, ni es la panacea, ya que hay que ser muy disciplinado y tener grandes dotes de comunicación para que la relación laboral con los compañeros no se desgaste, y para no caer en el aislamiento, que puede llegar a minarnos.

Yo he teletrabajado desde el embargo de mi primera hija en 2005, y por eso puedo aventurarme a ofreceros estos consejos que os pueden ayudar a afrontar esta nueva forma de trabajar:

-**Dúchate y arréglate cada día nada más levantarte** (no sólo de cintura para arriba para las videollamadas), te hará sentir mejor y te predispondrá para el trabajo.

![mujer con toalla de ducha](/images/posts/teletrabajo/ducha.PNG)

-**Márcate un horario** (aunque se pueda adaptar a imprevistos). 

-Haz un **organigrama con los horarios de tus hijos y tareas que debas atender**, y encájalas en una franja horaria concreta  para respetar el tiempo de trabajo.

-**No mezcles tareas del hogar con tareas del trabajo**, ni hagas multi tasking (la gran mentira). Ten en cuenta que los tiempos de desconexión de una y conexión a otra restan eficiencia y ralentizan el resultado, aunque no nos demos cuenta y parezca que estamos haciendo un montón de cosas.

-Usa un [**planificador general de sobre mensa**](https://amzn.to/3ibsSBs) (enlace afiliado) como éste que yo uso por ejemplo y luego vuelca tareas concretas a tu calendario del móvil con alarmas.

-**Usa distintos tipos de sonidos para las notificaciones del**
**Móvil** para no tener que estar mirándolo siempre que suena, sino hacer diacriminación auditiva que terminará siendo mecánica para sólo mirar en el momento lo necesariamente inminente y dejar para después lo que pueda esperar.
Lo puedes hacer incluso por personas. 

-**Tu espacio es tu santuario**. Acógete a sagrado. No lo compartas y respétalo para que lo respeten, da igual lo grande o pequeño que sea.

![mano de niño en ratón](/images/posts/teletrabajo/raton.PNG)

-Pon **carteles de no molesta**r cuando estés trabajando puedes descargar este cartel que he diseñado para ti. Hazlo respetar; cuando hayas terminado quítalo para que surta efecto cuando esté puesto.

![cartel no molestar](/images/posts/teletrabajo/no-molestar.PNG)

Descarga [aquí tu cartel imprimible "no molestar"](/descargas/teletrabajo/no-molestar.pdf)

-**Haz que respeten tu horario labora**l y respétalo tú también salvo emergencias.

-**Comunícate fluidamente** con tu oficina, más aún que si estuvieras trabajando de forma presencial tanto en el plano humano (muy importante) como en el laboral, y sobre todo informa minuciosamente de todas las tareas que haces y el tiempo que te ha llevado cada una y justifica el exceso de tiempo en alguna si lo hubiere.

-**Usa un lenguaje correcto** y antes de mandar mensaje reléelo por si pudiera dar lugar a malentendidos.
Si estás enfadada o molesta por algo, no mandes el mensaje todavía, espera a calmarte y revísalo antes.

-Haz **videoconferencia** siempre que puedas para ver a tus compañeros 

-**Cuando te toque ir a la oficina** aprovecha para relacionarte con todos. 

-**Tómate los descansos** correspondientes

-**Termina a tu hora** y haz deporte , sal a la calle, haz actividades sociales

-**Dedícate un tiempo cada día obligatorio** , aunque sea poco, pero haz algo que te apasione o te relaje)

![cartel no molestar](/images/posts/teletrabajo/infografia.JPG)

Espero que con estas consejos puedas organizarte mejor con tu teletrabajo y tu familia. 

Si te han resultado de utilidad, por favor compártelos para que lleguen a más gente. De esta forma también estarás ayudándome a mí para que pueda seguir escribiendo este blog.

Comparte!