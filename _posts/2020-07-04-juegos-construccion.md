---
layout: post
slug: juegos-construccion
title: Mi hijo no quiere jugar solo
date: 2020-07-04T18:19:29+02:00
image: /images/posts/juegos-construccion/p-construccion.jpg
author: maria
tags:
  - 3-a-6
  - juguetes
  - educacion
  - crianza
---

Las vacaciones escolares nos traen muchos momentos en que los niños no saben a qué jugar o cómo entretenerse. El auge del tele trabajo en casa ha hecho que muchas familias hayan tenido que ingeniárselas para convivir con los niños a la vez que realizan su actividad laboral siendo el recurso más fácil acudir a las pantallas y, ojo, que no lo critico porque yo soy la primera que muchas veces me han salvado la vida mientras tenía una reunión por video llamada, pero he querido daros unos tips para fomentar el juego infantil en solitario, como los sets de [construcciones para niños](https://prenatal.es/product-category/juguetes/juguetes-juguetes/construcciones/) (el catálogo de Prénatal tiene muchas para niños pequeños), que tan beneficioso es para ellos, y tanto necesitaremos los adultos estas vacaciones de verano.

## No le pidamos peras al olmo

La edad en la que los pequeños pueden querer empezar a jugar solos son los dos años, pero no olvidemos que cada uno es diferente, y dependerá también de la costumbre que tengan y de su personalidad.

![bebé jugando con cuchara](/images/posts/juegos-construccion/cuchara.jpg)

Si somos padres o madres muy "encimarios" o controladores, y no solemos dejar espacio para que se ensucien o se equivoquen por ellos mismos, y enseguida saltamos para evitarles el "fracaso", lo normal es que les cueste más lanzarse al juego en solitario.

## Beneficios del juego en solitario

Es cierto que jugar con los padres también es beneficioso y normalmente un estímulo para su creatividad, sin embargo el juego en solitario hará que aprendan a explorar el mundo y a relacionarse con otros niños y con su entorno, además de generar un ambiente ideal para que los niños aprenden a tomar decisiones por sí mismos, a crear su propio espacio personal como lugar donde mostrarse como si no los estuviéramos mirando (lo que nos aporta información valiosísima).

También s importante para que aprendan a desenvolverse en el terreno del juego simbólico, que  suele empezar sobre los 2 años, cuando los vemos, por ejemplo, cogiendo un muñeco, dandole de comer o cantándole una nana. Es muy importante que desarrollen el juego simbólico espontáneo.

![niño jugando con un cebra](/images/posts/juegos-construccion/cebra.jpg)


## Cómo lograr que mi hijo juegue solo. El entrenamiento

Roma no se construyó en un día, así que lo más importante es que **empecemos poco a poco**; primero podemos intentar jugar juntos y en algún momento dejarlo jugar solo. Para esto lo ideal son los juegos de construcciones o los puzzles en los que podemos iniciarlos con nuestro peque, y en el momento más interesante ausentarnos unos minutos con una excusa (una llamada de teléfono o un correo electrónico) para provocar quiera seguir haciéndolo solo.

Pero, ojo, no es buena idea es empezar dejándole jugar solo más de cinco minutos ya que puede sentirse agobiado o con sentimiento de abandono, pero si lo vamos haciendo a menudo en diferentes ocasiones, con paciencia y tiempo, seguramente lograremos que poco a poco vayan cogiendo ese hábito.

**No se trata de dejarlos solos** en una habitación, sin ningún tipo de supervisión, ni alejarnos de ellos, se trata de estar juntos, realizando cada uno sus propias tareas y estar disponible cuando necesiten de nuestra ayuda.

![puzzle](/images/posts/juegos-construccion/puzzle.jpg)

Otro truco es que en lugar de participar en el mismo juego, intentar **realizar una acción en paralelo**. Por ejemplo, si está construyendo una casa, construir nosotros la nuestra y sólo intervenir si necesita ayuda.

**Crear juntos una zona de juego** donde el peque se sienta cómodo y seguro es importante. Si es muy pequeño, podemos recurrir al típico corralito, pero si son mayores o vamos a estar supervisando, una alfombra pequeña y un cajón de juguetes con ruedas son lo único que necesitamos, de manera que podamos transportarla a la habitación donde estemos y que ellos jueguen ahí mientras nosotros continuamos con nuestras tareas.

Puede que también le ayude tener **nuevos juegos** en los que implicarse y disfrutar, por ejemplo podemos ofrecerles nuevas formas de jugar con sus antiguos juguetes,


## Los extremos son malos

Como decía Aristóteles, en el punto medio está la virtud. Así que debemos buscar un equilibro entre el juego individual y el que disfrutan con nosotros, con los abuelos, sus hermanos y amigos de su edad.

También podemos [educarlos en el uso de las pantallas](https://madrescabreadas.com/2017/04/27/seguridad-internet-menores-incibe/) ya que nos pueden sacar de más de un apuro como os contaba antes, pero no pueden estar todo el día jugando con dispositivos electrónicos en solitario porque les podría llegar a crear dependencia. Ponerles desde pequeños un horario y no utilizarlos como niñera para que estén entretenidos sería una buena práctica.

Ahora sólo queda ponernos manos a la obra!

Si te ha servido esta información compártela! Eso me ayudará a poder seguir escribiendo este blog.


*Photo by Isabel Lenis on Unsplash cebra*

*Photo by Kelly Sikkema on Unsplash niño construcción* 

*Photo by Shirota Yuri on Unsplash bebé con cuchara*

*Photo by Debby Hudson on Unsplash puzzle*