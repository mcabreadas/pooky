---
layout: post
slug: charlas-redes-sociales-padres
title: Formacion en nuevas tecnologias para padres
date: 2020-11-10T13:19:29+02:00
image: /images/posts/charlas-redes-sociales/p-familia.jpg
author: maria
tags:
  - adolescencia
  - educacion
  - tecnologia
  - invitado
---

> Esta semana merendamos con una mujer excepcional, honesta y sincera. A Maider la conocí en las Redes Sociales, pero he tenido la suerte de estar con ella en persona en dos ocasiones: una en mi tierra tomando unas marineras, y otra en la suya, callejeando por San Sebastián donde disfruté de la mejor guía.
> 
> Maider es una mujer comprometida con la educación de sus hijos que un día se dio cuenta de los peligros de Internet para los más jóvenes y decidió emprender formando a padres e hijos en el buen uso de las redes sociales y ayudando a muchas familias a través de sus charlas amenas y cercanas donde los chicos y chicas se abren y aprenden a protegerse de las amenazas que les acechan en la jungla de la red.
> 
> Así que prepárate un buen chocolate, un té o un café, que hoy toca aprender para educar a nuestros hijos.
> 

![maider](/images/posts/charlas-redes-sociales/maider.jpg)

Soy Maider Pevi ([@maiderPeVi](https://twitter.com/maiderpevi) en Twitter y [@Bidai_On](https://www.instagram.com/bidai_on/) en Instagram) Tengo dos hijos de 12 y 8 años que son adictos a los juegos online. 

Mi primera incursión en la blogosfera fue de contenido maternal, infantil, consejos y vivencias propias que pudieran ayudar a otras mamis y familias. También había algo de comercio local con el que pretendía monetizar el blog y viajes en autocaravana, que es mi otra pasión. 

Pero decidí cambiar la temática cuando trabajaba de Community Manager en un Colegio. Ver los perfiles tan abiertos de los chavales, dando tanta información gratuita y ver que los padres no tenían ni idea de dónde trasteaban sus hijos, me hizo decidir compartir mis conocimientos junto con una abogada experta en redes sociales, y así ofrecer diferentes puntos de vista sobre la tecnología y su uso. 

## El proyecto de formación en redes sociales 

Las charlas van enfocadas a alumnos a partir de 5º de primaria que es la edad en la que la mayoría tienen acceso a una tablet propia, el móvil de sus padres, juegos online... Aunque cada vez vemos más [niños pequeños pegados a un móvil](https://madrescabreadas.com/2020/06/24/control-parental-moviles/), pero considero que aún no están preparados para entender los peligros en la red. 

Quizá sería interesante que fueran los padres y madres de los menores de 10 años los que acudieran a las charlas, pero normalmente los que dan un móvil en edades tempranas a los hijos no son los que acuden a mis charlas. 

## Charlas adaptadas para niños

A todos los niños y niñas que asisten a nuestras charlas, les preguntamos previamente el uso que hacen de la tecnología y en qué dispositivos. No es lo mismo tener un smartphone propio que compartir tablet con tu hermano o jugar a la Play. Por eso, a partir de sus respuestas adaptamos la charla y siempre ponemos muchos ejemplos muy prácticos y fáciles de entender por ellos. 

![charla aula nuevas tecnologias](/images/posts/charlas-redes-sociales/charla-nuevas-tecnologias.jpg)

También les hago hablar mucho porque les gusta mucho levantar la mano y dar su opinión, sobre todo si no tienen a profesores ni padres cerca se sueltan muchísimo y te cuentan cosas muy interesantes y a la vez preocupantes. 

Les hablamos de los riesgos en Internet; además del ya conocido CiberBullying, hablamos de sexting, grooming, suplantación de identidad, huella digital... 

## Charlas para padres

¿Y qué les contamos a los padres? 

Ellos creen que sus hijos no hacen nada malo por tener un perfil en una red social o jugar online con sus amigos, pero es que no es sólo eso lo que pueden hacer. Les hablamos de las diferentes maneras que pueden hacer daño a otros, o que se lo hagan a ellos. Hablamos de contenido inapropiado, de grooming, de la sobreexposición (u oversharing)... Hay tanto contenido que una sola sesión se hace corta. 

## La comunicación de los padres con los chavales es crucial

Me gusta cuando las charlas se dan a los padres y a los hijos, y luego lo comentan e incluso te mandan un mensaje dando las gracias por haber abierto esa puerta. 

Me consta que no es nada fácil hablar con los hijos en edad pre-adolescente, lo sé por experiencia propia. Pero a veces basta una noticia en la tele, o un comentario en un grupo de WhatssApp para empezar una conversación, tantearles, ver si algo les preocupa. 

Pero si no estás preparado te puedo ayudar con una charla en tu escuela, club de fútbol o asociación de tiempo libre. Allá donde hay un niño pegado a un móvil es necesaria una charla sobre tecnología. 

Ellos han nacido con un móvil bajo el brazo, pero como nosotros no hemos aprendido a conducir por haber nacido con los coches en funcionamiento, ellos tampoco han aprendido a utilizarlo como es debido. 

No tenemos que dejar en manos de cualquiera la educación digital o tecnológica de nuestros hijos e hijas. 

## Reflexión final

Cada vez hay más información al respecto, cada vez los padres sabemos más sobre tecnología pero a veces no queremos ver la realidad de los peligros; la frase "mi hijo no hace nada de eso en Internet" es un placebo que a veces nos decimos para no ver lo que está pasando de verdad. 

La realidad es que pasa más de lo que pensamos el acoso, los grupos de WhatssApp, la manipulación de fotos, los bulos, las grabaciones no autorizadas, la divulgación de contenido, la normalización de la violencia... por eso debemos estar preparados, presentes, atentos, informados y no bajar la guardia. 

Es lo que nos ha tocado, vivir con la tecnología para lo bueno y para lo malo. No es cuestión de demonizar, sino de tenerle respeto, de estar educados en tecnología, de saber qué hacer cuando ocurren esas cosas. De tener confianza. 


Espero que os haya servido y si es así, comparte para ayudar a más familias y contribuir a que podamos seguir escribiendo este blog.

Más [información sobre formación en el uso de las nuevas tecnologías aquí.](https://madrescabreadas.com/2017/04/27/seguridad-internet-menores-incibe/)


*Photo portada by John Schnobrich on Unsplash*