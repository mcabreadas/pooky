---
layout: post
slug: calendario-adviento-2021
title: Ideas para meter en tu calendario de Adviento navideño más allá de los
  típicos dulces
date: 2021-11-22T10:12:23.532Z
image: /images/uploads/4503714.jpg
author: .
tags:
  - trucos
  - navidad
---
Uno de los meses más anhelado por toda la familia es diciembre, con sus luces, villancicos, regalos y demás. La navidad se apodera de cada miembro de la familia y los niños no son la excepción. A todo esto se suma **una tradición muy divertida y muy esperada en [Adviento](https://es.wikipedia.org/wiki/Adviento),** sobre todo por los más pequeñitos de la casa, a saber: el calendario de Adviento.

Este calendario de adviento permite unir a toda la familia día a día mientras se preparan para Nochebuena. Cada día traerá una sorpresa nueva y hará que la espera sea dulce.

[![](/images/uploads/calendario-de-adviento-playmobil.jpg)](https://www.amazon.es/Playmobil-Calendario-Adviento-Navidad-Multicolor/dp/B06WVQV8X9/ref=pd_bxgy_img_2/259-8252965-5643646?pd_rd_w=tsIJP&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=1QDJ7RF48BJE5VJJEHXG&pd_rd_r=3a7883b2-beb8-4352-a2c4-4575e0b1c5d4&pd_rd_wg=0yGVc&pd_rd_i=B06WVQV8X9&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Playmobil-Calendario-Adviento-Navidad-Multicolor/dp/B06WVQV8X9/ref=pd_bxgy_img_2/259-8252965-5643646?pd_rd_w=tsIJP&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=1QDJ7RF48BJE5VJJEHXG&pd_rd_r=3a7883b2-beb8-4352-a2c4-4575e0b1c5d4&pd_rd_wg=0yGVc&pd_rd_i=B06WVQV8X9&psc=1&madrescabread-21) (enlace afiliado)

## Origen del calendario de Adviento

En este otro post hablamos de [que es el calendario de adviento, como funciona, y los tipos de calendario de adviento que puedes hacer](https://madrescabreadas.com/2020/11/30/calendario-adviento-tipos/).

Existen constancia de esta práctica **desde el siglo XIX,** donde se encendía una vela cada día hasta llegar al día 24 de diciembre. Más tarde, se llevó el conteo de forma más física; las familias pobres marcaban en la puerta 24 rayas con tiza y los niños las borraban con el pasar de los días, mientras que en las familias adineradas los chicos recibían golosinas.

Con el pasar de los años el calendario de Adviento fue evolucionando y posteriormente **llevado al mercado para su comercialización en el siglo XX**. Sin embargo, para los años 50 's los calendarios de Adviento incluían pegatinas sorpresas, donde en algunos casos escondían en su interior hermosos paisajes decembrinos o de temática cristiana.

[![](/images/uploads/calendario-de-adviento-patrulla-paw-patrol.jpg)](https://www.amazon.es/Adventskalender-Pat-Patrol-Weihnachten-2021/dp/B08SR5HZBG/ref=sr_1_39?dchild=1&keywords=calendario+adviento&qid=1635121745&qsid=259-8252965-5643646&sprefix=calen%2Caps%2C573&sr=8-39&sres=B085FLGLLH%2CB08QSMLT33%2CB08YK8WK98%2CB07Y95HPG2%2CB085FLKYFB%2CB08W9K2CTM%2CB09GCMFPDP%2CB08Q7D131Z%2CB07P57Z363%2CB00O233MR4%2CB08DQVBNNK%2CB09CF14TXY%2CB08X1T5275%2CB08WWKZK88%2CB084R1RPQM%2CB08X1B622Z%2CB084ZXKF7L%2CB08NTRHWZR%2CB07B41JMNF%2CB08V4Q6XL1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Adventskalender-Pat-Patrol-Weihnachten-2021/dp/B08SR5HZBG/ref=sr_1_39?dchild=1&keywords=calendario+adviento&qid=1635121745&qsid=259-8252965-5643646&sprefix=calen%2Caps%2C573&sr=8-39&sres=B085FLGLLH%2CB08QSMLT33%2CB08YK8WK98%2CB07Y95HPG2%2CB085FLKYFB%2CB08W9K2CTM%2CB09GCMFPDP%2CB08Q7D131Z%2CB07P57Z363%2CB00O233MR4%2CB08DQVBNNK%2CB09CF14TXY%2CB08X1T5275%2CB08WWKZK88%2CB084R1RPQM%2CB08X1B622Z%2CB084ZXKF7L%2CB08NTRHWZR%2CB07B41JMNF%2CB08V4Q6XL1&madrescabread-21) (enlace afiliado)

## Calendario de Adviento para niños

El calendario de Adviento está constituido por 24 casillas las cuales **guardarán en su interior una sorpresa diaria para los niños.** Imagina sus caritas de intriga justo antes de descubrir qué regalo le traerá cada día. Definitivamente una forma muy especial y divertida de esperar hasta la añorada Nochebuena.

Hay muchos estilos y formas para un calendario de Adviento, desde los más sencillos y tradicionales hasta los más vistosos y profesionales. Aunque no está mal obtener el que más nos guste, queremos centrarnos en las actividades que unirán a la familia y no caer en el error de que mientras más grande y caro sea el calendario, le dará más valor a la navidad.

Los **regalos sorpresas tendrán los límites que te permita la imaginación.** No solo puedes usar dulces o galletas, también puedes incluir otras cosas, como por ejemplo:

* Practicar o escuchar villancicos.
* Realizar [manualidades navideñas](https://madrescabreadas.com/2020/12/23/vlog-juegos-navidad-ninos-infantiles/).
* Lectura de [cuentos navideños](https://madrescabreadas.com/2020/12/07/cuentos-navidad/).
* Ver una [pelicula navideña toda la familia](https://madrescabreadas.com/2020/12/06/peliculas-navidad-ver-familia/).
* Notitas pintorescas con valores motivacionales.
* Recetas y preparación de [comida navideña](https://madrescabreadas.com/2017/12/10/dulces-nutella-navidad/).
* Hacer un [arbol de navidad de hojaldre](https://madrescabreadas.com/2016/12/31/arbol-hojaldre-chocolate/).
* Dictado y escritura de poemas.
* Planificación de [actividades navideñas recreativas para toda la familia](https://madrescabreadas.com/2020/12/21/actividades-caseras-navidad-ninos/).
* Notas de frases bíblicas y reflexionar en ellas

En fin, son muchas las opciones que puedes elegir para hacer de tu calendario de Adviento una experiencia maravillosa para tus peques.

En este [video te damos algunas ideas para hacer vuestro propio clandario de adviento](https://madrescabreadas.com/2020/12/05/vlog-calendarios-adviento/)

[![](/images/uploads/calendario-de-adviento-barbie-christmas.jpg)](https://www.amazon.es/Barbie-Fashionista-Calendario-Accesorios-Sorpresa/dp/B07NW9MYBM/ref=sr_1_17?crid=FL81MS4JN05S&dchild=1&keywords=calendario+adviento+para+niñas&qid=1635125251&qsid=259-8252965-5643646&sprefix=calendario+adviento+para+%2Caps%2C15760&sr=8-17&sres=B08W2Q284X%2CB085FLGLLH%2CB08QSMLT33%2CB085FLKYFB%2CB08V1JJ4QG%2CB07B41JMNF%2C1704205840%2CB085QZD9SK%2CB085R131XW%2CB07Y95HPG2%2CB085QZKC2R%2CB085QZ2FJT%2CB07NW9MYBM%2CB08Q7D131Z%2CB07P57Z363%2CB07GB2NF2Z%2CB08NWWK9LJ%2CB084ZXKF7L%2CB084R1RPQM%2CB07CN16KSD&srpt=ADVENT_CALENDAR&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Barbie-Fashionista-Calendario-Accesorios-Sorpresa/dp/B07NW9MYBM/ref=sr_1_17?crid=FL81MS4JN05S&dchild=1&keywords=calendario+adviento+para+niñas&qid=1635125251&qsid=259-8252965-5643646&sprefix=calendario+adviento+para+%2Caps%2C15760&sr=8-17&sres=B08W2Q284X%2CB085FLGLLH%2CB08QSMLT33%2CB085FLKYFB%2CB08V1JJ4QG%2CB07B41JMNF%2C1704205840%2CB085QZD9SK%2CB085R131XW%2CB07Y95HPG2%2CB085QZKC2R%2CB085QZ2FJT%2CB07NW9MYBM%2CB08Q7D131Z%2CB07P57Z363%2CB07GB2NF2Z%2CB08NWWK9LJ%2CB084ZXKF7L%2CB084R1RPQM%2CB07CN16KSD&srpt=ADVENT_CALENDAR&madrescabread-21) (enlace afiliado)

La idea es que los críos se entretengan con el juego del conteo diario y del reto que deben superar para recibir el regalo. Por otro lado, **toda la familia podrá disfrutar de este juego** apartando un poco el ajetreo del día a día y saborear cada esencia que trae está época llena de magia y unión.

Recuerda que puedes planificar desde ya tu calendario de Adviento, bien sea que decidas confeccionarlo de forma casera o que lo adquieras en el mercado. Lo importante en sí es poder disfrutar al máximo y en familia estos momentos tan gratos que quedarán grabados permanentemente en la memoria de todos, pero especialmente en la mente de los niños.

*Photo Portada by pikisuperstar on Freepik.es*