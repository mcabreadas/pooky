---
author: maria
date: 2021-04-20 08:54:05.888000
image: /images/uploads/mq2-3.jpg
layout: post
slug: cambio-climatico-para-ninos
tags:
- vlog
title: Vlog os presento Madres por el Clima
---

Esta semana celebramos el Día de la tierra, por eso os quiero presentar una iniciativa de unas madres preocupadas por el futuro de sus hijos y el mundo que les vamos a dejar, que se llama [Madres por el Clima, de la cual tenéis mas informacion aqui.](https://madrescabreadas.com/2021/04/19/madres-por-el-clima-contra-el-cambio-climático/)

Os animo a conocerla, compartirla y uniros!

<iframe width="560" height="315" src="https://www.youtube.com/embed/_F1-2Hn_nhE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha gusta do comparte y suscribete!