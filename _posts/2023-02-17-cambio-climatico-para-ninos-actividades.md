---
layout: post
slug: cambio-climatico-para-ninos-actividades
title: "Podcast Madres por el Clima: 5 formas fáciles de sustituir los plásticos
  en casa"
date: 2023-02-17T10:50:21.792Z
image: /images/uploads/madres_clima.jpg
author: .
tags:
  - podcast
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/xdsIxapGduc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

En este episodio nos descabreamos con Patricia Esteve, representante del movimiento "[Madres por el Clima](https://madresporelclima.org)".

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/2eeq9OKRIMb4Eur2eQsHYj?utm_source=generator&theme=0" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Ya [merendamos con ellas en este blog](https://madrescabreadas.com/2021/04/19/madres-por-el-clima-contra-el-cambio-climático/) hace un tiempo, pero hemos querido traerlas de nuevo por lo interesante de su labor.

Madres por el Clima es un colectivo principalmente formado por madres muy diferentes entre sí, a las que les une un compromiso por actuar frente a los desafíos socioambientales, como la cambio climático. Desde que comenzaron en 2019, han ido avanzando a distinto ritmo, conciliando sus acciones con la familia, el trabajo y otras responsabilidades. Así, hoy día son más de 200 personas, que han caminado de la mano para alcanzar pequeños grandes logros que las inspiran a continuar.

Su atención recae en realizar acciones en su ámbito más cercano, como los colegios, su casa, en los barrios o en el trabajo, a fin de promover un estilo de vida más sostenible en su familia y su entorno. A igual tiempo, organizan y apoyan iniciativas para exigir a los responsables políticos la puesta en marcha de medidas efectivas para afrontar y visibilizar la crisis climática. En estos momentos ya son un colectivo presente en varias mesas de trabajo, formando parte de la toma de decisiones y eso las enorgullece.

Todas estas acciones las realizan desde el compromiso de actuar en favor del futuro de sus hijos e hijas, y por toda una generación que no debería heredar una hipoteca ambiental. También para implicarles y que se reconozcan como agentes de cambio, que vean que sus acciones, por pequeñas que sean, son importantes. 

Nuestro futuro, su futuro depende del buen estado de la naturaleza, de la que formamos parte y que debemos defender. Y en este camino, todas las madres suman y son bienvenidas.

¿Te apuntas?

Puedes seguirlas en [Instagram](https://www.instagram.com/madresclima/), [Twitter](https://twitter.com/MadresXelClima)

En este episodio hablamos de:

a) qué actividades han realizado hasta el momento, dónde podemos atender a las que son a nivel de familia/barrio y a nivel institucional.
b) qué actividades tienen previstas
c) qué estrategias tienen para implicar a nuestras hijas e hijos y por qué es importante.

¿Cómo surge el movimiento Madres por el Clima?
¿Por qué madres? ¿Es que lo tenemos que arreglar todo nosotras o qué?.
¿Cómo se pueden unir otras madres?
¿Qué le dirías a los negacionistas del cambio climático?
Plásticos: en qué punto estamos para acabar con ellos?
Ayudas next generation
¿Tiene alguna solución el Mar Menor?
Cosas que podamos hacer en casa con nuestros niños para mejorar el medio ambiente. Cosas sencillas de todos los días.
Que les cabrea más

![](/images/uploads/pajitas.jpg)