---
layout: post
slug: dia-del-medio-ambiente-2022
title: Día del medio ambiente
date: 2022-05-30T09:46:14.025Z
image: /images/uploads/dia-del-ambiente.jpg
author: faby
tags:
  - educacion
  - planninos
---
Hoy más que nunca se hace indispensable **cuidar el ecosistema y respetar el medio ambiente.**  De acuerdo al *[Programa de las Naciones Unidas para el Medio Ambiente](https://www.unep.org/es/noticias-y-reportajes/reportajes/ahora-es-el-momento-de-tomar-medidas-urgentes-para-lograr-un)* “Somos la generación que puede salvar al planeta”. Es el momento de hacer cambios “ahora”. En este respecto, es necesario enseñar a los niños a valorar la naturaleza y aportar un grano de arena para la recuperación del mismo.

Pues bien, pensando en el bienestar de los más pequeños hay muchas actividades que se pueden hacer para ayudarlos a conectar con la naturaleza. En esta entrada hablaremos del medio ambiente y los niños.

## ¿Por qué se celebra el día del medio ambiente?

El día del medio ambiente se celebra con el objetivo de ayudar a la población a **valorar el entorno natural y cuidarlo.** 

El desarrollo sostenible es un asunto que incluye a todas las personas, sin importar raza, etnia, edad, sexo, etc. De hecho, se invita a todas las personas o entes de autoridad a hacer cambios significativos. Por un lado, la población en general; sean adultos o niños, deben **mejorar los hábitos de consumo.** A las empresas se les invita a desarrollar herramientas de producción o instalaciones más ecológicas. Y a los gobiernos se les exige proteger las áreas naturales y los animales.

Por supuesto, cada persona puede convertirse en un promotor del cuidado del medio ambiente, “**todos podemos alzar la voz en pro del medio ambiente”.**

## ¿Cuándo se celebra el Día del medio ambiente 2022?

**El Día del medio ambiente se celebra el 5 de junio**, así lo proclamó la Asamblea General de las Naciones Unidas en el año 1972.

El tema principal de la campaña global del Día Mundial del Medio Ambiente es “**Una sola Tierra”**. En este sentido, el lema del 2022 es “Invertir en nuestro planeta”, en el que se resalta la importancia de la sostenibilidad.

## Libros para educar en el respeto al medio ambiente

Una de las formas de celebrar el Día del Medio Ambiente con tus hijos es ***[leer cuentos sobre cómo salvar el planeta](https://www.amazon.es/Cuentos-salvar-planeta-Destino-colecci%C3%B3n/dp/8408226827/ref=sr_1_1?crid=385HWQ582JNWR&keywords=cuentos+salvar+planeta&qid=1652734372&sprefix=cuentos+salvar+%2Caps%2C515&sr=8-1)*** o *[**cuidar el agua**. ](https://www.amazon.es/%C2%A1Oh-agua-Jard%C3%ADn-Los-Libros/dp/8467378182/ref=sr_1_8?crid=1273I6KZ5ZRU0&keywords=cuentos+agua&qid=1652734440&sprefix=cuentos+agua%2Caps%2C251&sr=8-8) (enlace afiliado)*

Claro, dependiendo de su edad puedes darle libros para que puedan motivarse a “marcar la diferencia y trabajen en pro de un **mundo más saludable**”.

### Los superpreguntones ¡salvamos el planeta!

**¿Tu hijo es muy curioso y preguntón?** Este libro plantea preguntas que despiertan su interés de inmediato: “¿Los pedos y eructos de las vacas contaminan?, ¿La basura se exporta?”.

Este libro explica en un lenguaje sencillo algunas acciones básicas para **prevenir la contaminación**. Explica conceptos básicos del cambio climático, la importancia del reciclaje, las plantas depuradoras y mucho más.

[![](/images/uploads/los-superpreguntones-salvamos-el-planeta-.jpg)](https://www.amazon.es/Los-superpreguntones-%C2%A1salvamos-el-planeta/dp/8499743471/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=29GOKPWAMPXQ4&keywords=Los+superpreguntones+%C2%A1salvamos+el+planeta%21&qid=1652737632&sprefix=los+superpreguntones+salvamos+el+planeta+%2Caps%2C403&sr=8-1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Los-superpreguntones-%C2%A1salvamos-el-planeta/dp/8499743471/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=29GOKPWAMPXQ4&keywords=Los+superpreguntones+%C2%A1salvamos+el+planeta%21&qid=1652737632&sprefix=los+superpreguntones+salvamos+el+planeta+%2Caps%2C403&sr=8-1&madrescabread-21) (enlace afiliado)

### Las Plantas: 1 (Colección Medio Ambiente para Niños)

Este libro está diseñado para **niños pequeños de unos 3 a 5 años.** Les ayuda a querer las plantas. Tiene un formato de lectura divertido y sus ilustraciones son preciosas.

Esta obra literaria ayuda a los más pequeños a fijarse en las plantas y que desarrollen desde temprana edad una percepción de ellas.

[![](/images/uploads/las-plantas-1-coleccion-medio-ambiente-para-ninos.jpg)](https://www.amazon.es/Plantas-Colecci%C3%B3n-Medio-Ambiente-Ni%C3%B1os/dp/1542729688/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IK5IWXNEX42T&keywords=libros+del+medio+ambiente+ni%C3%B1os&qid=1652730614&sprefix=libros+del+medio+ambiente+ni%C3%B1o%2Caps%2C274&sr=8-4&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Plantas-Colecci%C3%B3n-Medio-Ambiente-Ni%C3%B1os/dp/1542729688/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IK5IWXNEX42T&keywords=libros+del+medio+ambiente+ni%C3%B1os&qid=1652730614&sprefix=libros+del+medio+ambiente+ni%C3%B1o%2Caps%2C274&sr=8-4&madrescabread-21) (enlace afiliado)

### Había una vez una semilla

Este libro es ideal para ser leído de padres a hijos, **fomenta la buena comunicación.** De hecho, dispone de notas y actividades relacionadas que contribuyen a que se refuerce la idea del contenido.

Esta obra literaria cuenta con una narrativa infantil que explica de forma clara y sencilla cómo **crece una planta**, de dónde salen las semillas y mucho más.

[![](/images/uploads/habia-una-vez-una-semilla.jpg)](https://www.amazon.es/Hab%C3%ADa-una-semilla-Ocio-Conocimientos/dp/8466786848/ref=pd_sim_sccl_2_1/259-3610049-6138935?pd_rd_w=6BTCq&pf_rd_p=747ecc58-7051-4a8d-9e6f-76ef8dff3cf9&pf_rd_r=YWQ6WC4M7JQW8DJNFVQA&pd_rd_r=acb7e39b-123a-4607-834d-ca3a7f8e11db&pd_rd_wg=pHM2G&pd_rd_i=8466786848&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Hab%C3%ADa-una-semilla-Ocio-Conocimientos/dp/8466786848/ref=pd_sim_sccl_2_1/259-3610049-6138935?pd_rd_w=6BTCq&pf_rd_p=747ecc58-7051-4a8d-9e6f-76ef8dff3cf9&pf_rd_r=YWQ6WC4M7JQW8DJNFVQA&pd_rd_r=acb7e39b-123a-4607-834d-ca3a7f8e11db&pd_rd_wg=pHM2G&pd_rd_i=8466786848&psc=1&madrescabread-21) (enlace afiliado)

### Tu planeta te necesita

Finalizamos esta lista con la obra literaria: “Tu planeta te necesita. Una guía optimista para reducir, reutilizar, reciclar y renovar”.

**Este libro está pensado para niños de 4 a 8 años.** Es un escrito muy bien redactado y atractivo. Explica de dónde sale la basura y cuánto tiempo tarda en descomponerse. Al mismo tiempo, señala cómo luchar contra los residuos para salvar el planeta.

[![](/images/uploads/tu-planeta-te-necesita.jpg)](https://www.amazon.es/necesita-optimista-reutilizar-reciclar-ENCANTADO/dp/8427299613/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IK5IWXNEX42T&keywords=libros+del+medio+ambiente+ni%C3%B1os&qid=1652733429&sprefix=libros+del+medio+ambiente+ni%C3%B1o%2Caps%2C274&sr=8-6&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/necesita-optimista-reutilizar-reciclar-ENCANTADO/dp/8427299613/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IK5IWXNEX42T&keywords=libros+del+medio+ambiente+ni%C3%B1os&qid=1652733429&sprefix=libros+del+medio+ambiente+ni%C3%B1o%2Caps%2C274&sr=8-6&madrescabread-21) (enlace afiliado)

## Actividades para celebrar el Día Mundial del Medio Ambiente

Hay muchas cosas que puedes hacer en familia para celebrar esta efeméride. Lo señalaré a continuación.

* **Enseña a tu hijo a reciclar.** Puedes tener en casa ***[cubos de basura para reciclaje](https://www.amazon.es/Prosperplast-basura-compartimentos-Pl%C3%A1stico-3x35L/dp/B087RLVPSJ/ref=pd_sbs_sccl_3_4/259-3610049-6138935?pd_rd_w=2Dfex&pf_rd_p=bead054f-de1a-4d92-98b9-04253f60cdcd&pf_rd_r=K454APG562G3KAM6DH4K&pd_rd_r=b77322c4-53db-4e73-b159-af6378211351&pd_rd_wg=yhVZE&pd_rd_i=B087RLVPSJ&th=1) (enlace afiliado)***, en el que se indique el cubo para cada tipo de desperdicios, sea orgánico, plástico, vidrio, etc.
* Haz jabon casero artesanal para reciclar el aceite. Es este post te explicacamos [como hacer jabon casero](https://madrescabreadas.com/2021/12/16/tipos-de-jabones-artesanales-y-sus-beneficios/).
* **Andar en bici.** Elige uno o dos días a la semana para trasladarse en bici en vez de ir en coche.
* **Haz un jardín o huerto.** Esto le ayuda a valorar las plantas.
* **Lee cuentos**. Los cuentos fomentan la imaginación, ayudan a desarrollar la lectura y enseñan lecciones valiosas. Busca *[**cuentos del medio ambiente**.](https://www.amazon.es/Cuentos-Ni%C3%B1os-Quieren-Salvar-Stories/dp/6073186797/ref=sr_1_2?crid=385HWQ582JNWR&keywords=cuentos+salvar+planeta&qid=1652734605&sprefix=cuentos+salvar+%2Caps%2C515&sr=8-2) (enlace afiliado)*

En definitiva, cuidar el medio ambiente es responsabilidad de todos, así que es muy bueno inculcar el amor por la naturaleza a los más pequeños de la casa.