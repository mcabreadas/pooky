---
layout: post
slug: cuidado-suelo-pelvico
title: Suelo pélvico en cada etapa de la mujer
date: 2020-03-26T16:19:29+02:00
image: /images/posts/suelo-pelvico/p-mujer-tumbada.jpg
author: .
tags:
  - embarazo
  - mujer
  - salud
  - postparto
  - crianza
---

Cuando di a luz a mi primera hija hace 14 años noté que la zona del suelo pélvico se me quedó diferente tras el parto. Entonces no había tanta información como ahora sobre estos temas, pero ya se empezaba a hablar de las famosas bolas chinas. Estos productos [se pueden encontrar en una tienda especializada](https://www.diversual.com/es/), incluso on line, y ayudan a fortalecer toda la musculatura de esta zona que queda atrofiada tras el parto, sobre todo tras el primero,  debido a su hiperestiramiento para permitir la salida del bebé. 

Me surgieron mil dudas, así que decidí preguntar a mi ginecólogo, quien evitó responderme escudándose en la ironía y las evasivas. Su falta de diligencia la suplí investigando por mi cuenta, ya que su consejo "relájate y disfruta" no fue suficiente.

Menos mal que actualmente hay mucha más cultura de cuidado de esta delicada zona, sobre todo en la mujer tras el parto (todas deberíamos realizarnos una valoración tras cada parto), pero no debemos descuidarla en ninguna fase de nuestra vida. 

## Qué es el suelo pélvico

El suelo pélvico es el conjunto de músculos y ligamentos que están en la parte inferior de nuestra cavidad abdominal.
Sostiene nuestros órganos pélvicos: vejiga, uretra,  útero, vagina y recto en la posición adecuada facilitando su normal funcionamiento. Además, da estabilidad a la columna y la pelvis.

## Por qué debemos cuidar el suelo pélvico

Porque es el gran aliado de la salud ginecológica, sexual y del aparato urinario de la mujer. De él depende evitar prolapsos del útero, evitar la incontinencia urinaria, tener una vida sexual placentera, incluso evitar dolores en la zona lumbar. 

## El suelo pélvico en el embarazo

El suelo pélvico es el encargado de sostener el peso de nuestro útero, por eso es importante que controlemos el aumento de peso durante el embarazo haciendo ejercicio físico regular y llevando una dieta equilibrada, también para evitar el estreñimiento.

![embarazada haciendo yoga en la playa](/images/posts/suelo-pelvico/yoga.jpg)

No debemos subestimar la importancia de la higiene postura, os hablo por experiencia.No imagináis lo importante que es, y los daños que os puede evitar en el futuro, ya que nos ayudará no sólo a fortalecer los músculos perineales, sino también a evitar prolapsos.

Es interesante realizar una valoración del suelo pélvico con un especialista, ya que lo podemos tener hiper o hipo tónico, y dependiendo del caso serán adecuados unos ejercicios u otros. Por eso no es conveniente lanzarnos a hacer los famosos ejercicios de Kegel por ejemplo sin encomendarnos a nadie porque puede que no los necesitemos.

En este post os explico algunos [ejercicios que van fenomenal para el suelo pélvico](https://madrescabreadas.com/2015/04/08/suelo-pelvico-ejercicios/).

## El suelo pélvico tras el parto

Todas las que hemos dado a luz hemos notado cómo se ha visto afectado nuestro suelo pélvico, ya que los músculos y las fibras deben estirarse tanto para dar salida al bebé que, de no ser por los  mecanismos protectores del embarazo, el daño sería irremediable.

![post parto](/images/posts/suelo-pelvico/flores.jpg)

Por eso, en el postparto notamos una sensación extraña, que no es otra cosa que debilitamiento muscular que en la mayoría de los casos se recupera, pero en otros se producen daños a medio plazo, como la incontinencia o el prolapso uterino.
Por eso debería ser obligatorio que los profesionales de la salud que nos hacen el seguimiento tras el parto, ya sean ginecólogos o matronas, velaran también por nuestro suelo pélvico (lo contrario de lo que hizo mi ginecólogo), y nos recomendaran someternos a una valoración de la zona por un especialista.

Ya hemos comentado que los ejercicios de Kegel suelen ser útiles y eficaces, además de resultarnos muy sencillos de hacer, pero hacerlos incorrectamente puede empeorar la situación de nuestros músculos perineales.
Por eso es importante la valoración del estado del suelo pélvico tras el parto. Siempre.


## El suelo pélvico en la menopausia 

Cuando nos llega la menopausia, nos disminuyen los niveles hormonales, lo que hace que disminuya el tono, la fuerza y la elasticidad muscular en general y en particular la musculatura perineal y el colágeno de los tejidos del suelo pélvico, lo que incrementa el riesgo de que suframos alguna disfunción del suelo pélvico, como la incontinencia urinaria, el descenso de órganos o prolapso, o problemas sexuales como la dispareunia o vaginismo, que consiste en sufrir dolor durante las relaciones sexuales.
En este post os hablo de la [menopausia precoz](https://madrescabreadas.com/2015/05/31/menopausia-consejos/), y cómo sobrellevarla.

![mujer madura](/images/posts/suelo-pelvico/mujer-madura.jpg)

Por eso es muy recomendable seguir haciendo deporte en esta etapa también, pero poner especial atención en una buena práctica, vigilando la postura correcta si haces pilates o yoga, y guiada por un instructor profesional. 

Es fundamental que informes ate monitor si algún ejercicio te resulta incómodo o si notas molestias, quien seguramente lo sustituya por otro equivalente, pero que te resulte más cómodo.
En esta etapa debemos prestar especial atención a determinados hábitos que nos ayudarán a proteger nuestro suelo pélvico y nuestra calidad de vida, como llevar una dieta rica en fibra y evitar los esfuerzos al ir al baño, hidratarnos correctamente, no levantar cargas pesadas, cuidar nuestra higiene íntima con geles específicos y, por supuesto, acudir a revisiones ginecológicas periódicas y comentar con el médico cualquier cambio que sintamos en nuestra zona genital.

Sobre todo quiérete mucho y disfruta de cada etapa por la que pasas como mujer, ya que todas nos aportan y son importantes. 

Somos importantes!

Espero que te haya sido útil esta información y que la compartas!






*Photo by Amelia Bartlett on Unsplash yoga*

*Photo by Ava Sol on Unsplash flores*

*Photo by Marcos Heredia on Unsplash mujer tumbada*

*Photo by Dave Francis on Unsplash mujer madura*