---
date: '2020-04-12T18:19:29+02:00'
image: /images/posts/cuarentena-comida/p-cocinera.jpg
layout: post
tags:
- familia
title: Cuarentena en torno a la mesa
---

La comida ha recobrado un nuevo valor para las familias durante el confinamiento por el Covid-19, que ha propiciado que volvamos a cocinar en casa y comer todos juntos.

Sabías que el término [hogar, según la RAE](https://dle.rae.es/hogar?m=form) tiene como primera acepción "Sitio donde se hace la lumbre en las cocinas"? Quizá estemos volviendo a ese concepto de antaño en el que la familia se reunía en torno a la mesa y la comida, como punto de reencuentro donde se hacía algo más que comer.

¡Mi hijo mediano dice que en la cuarentena la comida sabe mucho mejor! 

No sé si será porque estamos aprendiendo a valorar más lo que tenemos, ya que hacer la compra no es tarea tan fácil como antes, o porque nos esforzamos más por cocinar cosas que nos y les gustan para aliviar lo tedioso de la situación (las penas con pan son menos), o porque tenemos más tiempo para dedicarle a la cocina con calma al pasar más tiempo en casa, pero lo cierto  es que estamos disfrutando mucho de los platos que elaboramos y, sobre todo, de compartirlos todos juntos.

![marinera tapa murciana](/images/posts/cuarentena-comida/marinera.jpg)

No sé si pasa en tu casa, pero en la nuestra cada día es un espectáculo: 
> “mamá, qué comemos hoy?“
> 
> ”Mamá, qué hay de cena?“
> 
> ”Te ayudo?“

Incluso los mayores se atreven a hacer sus primeros pinitos con recetas sacadas de Tik Tok.

Sin duda el momento de alimentarnos está cobrando un nuevo significado, y creo que me gusta porque se está respetando y valorando más la figura de la “ama o amo de casa” como aquella persona sin la cuál el hogar sería un caos infinito, y comeríamos tristemente “cualquier cosa”. Esa persona que, a pesar de que siempre ha estado cuidando de la casa, ahora está logrando un protagonismo inesperado y no buscado, dónde sus pocas veces valoradas y exaltadas virtudes se están ensalzando con un efusivo: 

> “gracias por hacernos la cena” 
> 
> “qué rico te ha salido el guiso, qué lleva?”

Y es que un desmesurado interés repentino en los aspectos culinarios está invadiendo los hogares españoles, que ahora vuelven a hace pan y dulces tradicionales en función de la época. Si has visto últimamente las redes sociales habrás observado que están llenas de torrijas, toñas, monas de Pascua, hornazos con huevo... y todo tipo de panes deliciosos hasta tal punto que la harina escasea en los supers!

![ingredientes receta magdalenas](/images/posts/cuarentena-comida/ingredientes.jpg)

![magdalenas en el horno](/images/posts/cuarentena-comida/magdalenas.jpg)


¿Qué será lo próximo? ¿Fabricar nuestro propio jabón? 

Y hablando de jabón... Cómo echo de menos a mi abuela! Y cuánto me acuerdo de ella cuando decía que con patatas, aceite, harina y azúcar no se pasaba hambre. 

![patata con forma de corazon](/images/posts/cuarentena-comida/patata.jpg)

Ojalá hubiera aprendido más de ella... pero al menos tengo su [receta del jabón casero](https://madrescabreadas.com/2013/11/08/la-salita-de-la-abuela-jabón-casero/) que ahora puedo compartir con vosotras!

Nunca pensé que lo diría, pero hasta yo le estoy tomando el gusto a esto de cocinar, y me estoy lanzando a hacer todo tipo de recetas nuevas que subo a las redes sociales a diario porque me anima a mejorar y me encanta recibir vuestro feed back ... Me hace la vida más bonita. Gracias.

![bizcocho de zanahoria](/images/posts/cuarentena-comida/bizcocho.jpg)

Así que, yo no sé cuánto tiempo va a durar esta situación de aislamiento en los hogares de todo el mundo, pero creo que a partir de ahora se respetará mucho más a la persona que cuida de los niños, de la casa y que cocina para la familia. 

¿Crees que el “hogar” está tomando un nuevo valor en el mundo?