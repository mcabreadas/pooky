---
layout: post
slug: dia-derechos-nino
title: Día de los derechos de la infancia.Como explicarlo a tus hijos
date: 2020-11-14T09:19:29+02:00
image: /images/posts/derechos-nino/p-hamaca.jpg
author: maria
tags:
  - 6-a-12
  - derechos
---

El 20 de noviembre se celebra el día de los derechos del niño y de la niña, y este año cobra un nuevo sentido bajo mi punto de vista porque los pequeños están demostrando un grado de adaptación, madurez y valor ante esta situación de pandemia digna de admiración. Cada día nos dan lecciones de responsabilidad y entereza que, al menos en mi caso, me ayudan a seguir adelante con esperanza y actitud positiva.

Si bien es cierto que los niños no son los grandes afectados por el virus COVID-19 también lo es que son los más vulnerables cuando se dan situaciones críticas o anormales en las familias, colegios y sociedad en general.

Quizá os sirva este post sobre los [derechos del niño hospitalizado](https://madrescabreadas.com/2014/06/23/derechos-ni-o-hospitalizado/).

## Este año más que nunca, Día de los Derechos del Niño

Por eso este año debemos explicarles muy bien cuáles son sus derechos, vigentes en el lugar del mundo en el que estén, y que existe una [Convención sobre los derechos del niño](https://www.un.org/es/events/childrenday/convention.shtml) para la infancia y adolescencia, que cubre hasta los menores de 18 años, y que ha sido ratificada por todos los países del mundo menos por Estados Unidos. Aquí os dejo la [ratificación de España del Convenio Internacional de los Derechos del niño](https://www.boe.es/buscar/doc.php?id=BOE-A-1990-31312).

Esto es tan importante, que todos debemos tener claro cuáles son esos derechos, tanto los niños, como los adultos, que en ocasiones nos tocará alzar la voz por ellos.

Para trabajar este tema con los niños debemos tener en cuenta su edad y la mejor forma de llegar a ellos. 

![tres niñas en un bordillo](/images/posts/derechos-nino/tres-ninas.jpg)

## Cuáles son los derechos de los niños y niñas

En el tratado internacional se detallan más de 40 derechos, que podrían resumirse en estos ámbitos, pero que os animo a echar un vistazo a este [resumen de los derechos de la infancia de UNICEF](https://www.unicef.es/sites/unicef.es/files/comunicacion/unicef-educa-CONVENCION-SOBRE-LOS-DERECHOS-DEL-NINO-version-resumida.pdf) ameno y entendible por todos con imágenes, y a que lo miréis con vuestros hijos.


- Derecho a la vida
- Derecho al juego
- Derecho a ofrecer sus opiniones
- Derecho a tener una familia
- Derecho a la salud
- Derecho a la protección contra el trabajo infantil
- Derecho a un nombre y una nacionalidad
- Derecho a la alimentación y la nutrición
- Derecho a vivir en armonía
- Derecho a la educación

![refugiados tras la alambrada](/images/posts/derechos-nino/alambrada.jpg)

## Recursos para explicar los derechos de los niños a nuestros hijos

Sólo si nuestros hijos tienen claro cuáles son sus derechos podrán exigir que se cumplan o protestar si se les vulneran a ellos o a alguien cercano. 

Sólo si llegan a ser conscientes de la magnitud de la protección que les brinda, incluso a nivel internacional se sentirán seguros para alzar la voz denunciar atropellos, o al menos venir a contárnoslo. Esto también es una buena forma de [prevenir el abuso sexual](https://madrescabreadas.com/2013/11/19/prevención-del-abuso-sexual-nfantil-juego-del/), el maltrato y otras lacras que amenazan constantemente a la infancia.

## Películas sobre los derechos de los niños y niñas

Una forma excelente de abrir debate, sobre todo con los niños más mayores, es ver una peli juntos y aprovechar para ir comentando cosas interesantes que veamos e incidir en lo que nos interesa que aprendan.

Si la vemos con adolescentes seguramente se abrirá un debate que deberemos alimentar para que nos expresen su opinión y aprovechar para darles la nuestra.

Os dejo esta recopilación de [películas que tratan sobre los derechos de los niños](https://www.unicef.es/blog/festival-cine-innocenti-peliculas-derechos-infancia) que podéis ver este fin de semana con unas buenas palomitas.

## Libros sobre los derechos de la infancia

Para los más pequeños lo ideal es tratar este tema, como tantos otros, con un cuento de por medio, ya que así será mucho más entendible por ellos, y para nosotros será mucho más fácil enseñárselo.

![niña leyendo](/images/posts/derechos-nino/leyendo.jpg)

Hay muchos libros que tratan sobre los derechos de la infancia, pero estos son los que más me han gustado:

**Para niños entre 8 y 12 años:**

**Libro los derechos de la infancia**

En este libro, diez grandes autores de literatura infantil y juvenil han unido su talento con el del ilustrador Emilio Urberuaga y nos ofrecen diez cuentos que recrean los principios recogidos en esa declaración, con el objetivo de recordar su importancia y de dar a conocer a los niños y niñas de hoy en día unos derechos fundamentales que desde hace décadas velan por su protección y desarrollo.

    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8467861711&linkId=f4e1bfdf20cf7e6d96677b84d3e63300&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}



**Malala Yousafzai**

Cuando Malala tenía 15 años, un talibán le disparó en la cabeza por ser niña e ir a la escuela. Los talibanes se habían hecho con el poder en el Valle donde ella vivía poniendo bombas y obligando a la gente a cumplir con unas normas totalmente injustas. A pesar de la herida, Malala quiso luchar por sus derechos y por los de todos los niños del mundo, y por ello le otorgaron el Premio Nobel de la paz con tan solo 17 años.

    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8417822666&linkId=9a47616aef2d085b1bf07443cf8e180a&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}
    

**Para niños entre 3 y 6 años** 

"Soy un niño con dos ojos, dos manos, una voz, un corazón y unos derechos. Los derechos de los niños son el presente. Porque es ahora que somos niños." Un libro para conocer, aprender y comprender los derechos de los niños de la voz de niños de todo el mundo y de cualquier condición a través de artísticas ilustraciones y de un texto con un marcado carácter poético. 

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8498014816&linkId=4ef2909d3a24b7923b9a2aeb24d57f34&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}
    

**A partir de 5 años**

Esta colección de la editorial Salvatella de Los derechos del niño recoge los 10 derechos fundamentales: 

<a target="_blank" href="https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_tl?ie=UTF8&tag=madrescabread-21&keywords=los derechos del niño salvatella&index=aps&camp=3638&creative=24630&linkCode=ur2&linkId=0d9388845ff71b1b25aa81cf02e7ea1e">Los derechos del niño. Editorial Salvatella</a> (enlace afiliado)<img src="//ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=ur2&o=30&camp=3638" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />

## Canción de Yo Soy Ratón "Respeta mis derechos"

Yo soy Ratón y la Plataforma de Infancia han concebido una canción para acercar a los más pequeños a sus derechos. 

¡A bailarla toda la familia!

<iframe width="560" height="315" src="https://www.youtube.com/embed/iM6YdAzfNfM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Actividades sobre el Día Mundial de los Derechos del niño

Para pasar a la acción y visibilizar la importancia de los derechos de la infancia y que sean respetados en todas partes del mundo la ONU propone cada año una actividad colectiva que puede ser muy divertida.

![niña oculta tras hoja](/images/posts/derechos-nino/hoja.jpg)

Por ejemplo, en 2019 propuso [teñir el mundo de azul](https://www.un.org/es/observances/world-childrens-day/events#); Los edificios emblemáticos de todo el mundo se iluminaron de azul, también los nos se vistieron de azul y las aulas o las oficinas se decoraron de este color.

Aún estamos a la espera de que salga la acción para este año, pero os animo a que estéis pendientes para participar con vuestros hijos!

Nosotros también lo haremos, así que nos vemos el 20 de noviembre en las redes sociales para celebrar el Día Internacional de los derechos del niño!

Si te ha gustado, comparte, de estemos también nos ayudarás a seguir escribiendo este blog.

*Photo portada by Janko Ferlič on Unsplash*

*Photo by Charlein Gracia on Unsplash 3 niñas*

*Photo by Jonathan Borba on Unsplash leyendo*

*Photo by Gabby Orcutt on Unsplash hoja*

*Photo by Vika Chartier on Unsplash alambrada*