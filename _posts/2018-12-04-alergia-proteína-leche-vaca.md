---
layout: post
slug: alergia-prote%C3%ADna-leche-vaca/
title: Alergia a la proteína de la leche de vaca. Lo que debes saber
date: 2018-12-04T18:19:29+02:00
image: /images/posts/alergia_proteina_leche_vaca/p_alergia_proteina_leche_vaca.jpg
author: .
tumblr_url: https://madrescabreadas.com/post/180799705149/alergia-prote%C3%ADna-leche-vaca
tags:
  - bebe
  - ""
  - nutricion
  - salud
  - infancia
---
Nutribén ha celebrado su I Jornada de Salud y Nutrición Infantil de la cátedra en Salud y Nutrición Infantil Nutrinfant, en la que tuvo lugar un simposio-taller sobre “La alergia a la proteína de leche de vaca desde el punto de vista del especialista en gastroenterología, alergología y nutrición”.  

Ya os he hablado varias veces sobre temas de nutrición infantil en el blog, y más concretamente sobre el tan preocupante problema de la [obesidad infantil](https://madrescabreadas.com/post/162242196699/prevenir-obesidad-infantil), pero esta vez lo que quiero abordar es el tema de las alergias, más concretamente una que está especialmente presente en mi entorno.
![congreso_ponencia](/images/posts/alergia_proteina_leche_vaca/tumblr_inline_pj87dw3Sd11qfhdaz_540.jpg)

## El preocupante aumento de las alergias alimentarias 

Y es que en las últimas décadas, la prevalencia de alergias alimentarias ha aumentado de manera significativa: en España es del 7.4%, siendo la alergia a la proteína de leche de vaca una de las más representada en este grupo encontrándose entre el 2 y el 3%<sup>1</sup>. 

En tel taller de Nutribén participaron la Dra. Josefa Barrio, del Servicio de Pediatría y Gastroenterología Pediátrica del Hospital Universitario de Fuenlabrada; el Dr. Luis Echeverría, de la Unidad de Alergia y Neumología Pediátrica del Hospital Universitario Severo Ochoa de Leganés (Madrid); y el Dr. José Manuel Moreno, codirector del Departamento de Pediatría de la Clínica Universidad de Navarra (Madrid).

## Antes no se solía diagnosticarla alergia a la proteína de la leche de vaca

La Dra. Josefa Barrio explicó que **la enterocolitis inducida por proteínas de los alimentos (Food Protein-Induced Enterocolitis; FPIES) es una alergia alimentaria no mediada por IgE potencialmente grave cuyo síntoma cardinal es el vómito.**

Es un cuadro que hasta épocas recientes no se solía diagnosticar , y por tanto se desconocía el número de casos, ya que no existían unos criterios diagnósticos bien establecidos.

## La importancia de la historia clínica para diagnosticar la alergia a la proteína de la leche de vaca

En 2017 un grupo de expertos de la Academia Americana de Alergia publicó una guía de consenso para el diagnóstico y manejo de esta patología explicando que para el diagnóstico es fundamental realizar una buena historia clínica, que es la herramienta más importante y en la mayoría de los casos permite identificar el alimento responsable de los síntomas. 

Hay que llevar un diario de lo que se ingiere porque la aparición de los síntomas con la ingesta del alimento y su desaparición tras la retirada del mismo son claves para el diagnóstico.

## Los alimentos culpables de la alergia a la proteína de la leche de vaca

Los principales alimentos implicados son las PLV (proteínas de la leche de vaca) y, en segundo lugar, la soja. Dentro de los alimentos sólidos los más frecuentes son los cereales, sobre todo el maíz y la avena. 
![niño_bebiendo_batido](/images/posts/alergia_proteina_leche_vaca/tumblr_inline_paon0huDca1qfhdaz_500.png)

## Alergia a la proteína de la leche de vaca aguda o crónica

1. **El FPIES agudo** se produce por la **exposición intermitente al alimento causal** y se caracteriza por la aparición de vómitos repetidos que comienzan entre 1 y 4 horas después de la ingesta del alimento implicado. A menudo asocian palidez y decaimiento, y pueden acompañarse de diarrea (en ocasiones sanguinolenta).

Hasta un 15% de pacientes pueden tener un cuadro potencialmente grave con hipotensión y shock. Se diferencia de la anafilaxia, donde también puede haber estos síntomas graves, en que en el FPIES las reacciones no son inmediatas a la ingesta y por la ausencia de síntomas respiratorios ni cutáneos. 

1. **EL FPIES crónico** se caracteriza por una clínica más insidiosa que se produce en **relación con la Ingesta diaria del alimento causal**. Los síntomas son vómitos, diarrea crónica, retraso crecimiento.

## Cómo se diagnostica la alergia a la proteína de la leche de vaca actualmente

En ambos casos los síntomas desaparecen tras la retirada del alimento causal: horas después de la retirada en el FPIES agudo o bien entre 3-10 días después de la retirada en el FPIES crónico.

En muchos casos es necesario realizar una provocación oral al alimento sospechoso para confirmar el diagnóstico, que debe realizarse en medio hospitalario, monitorizando al paciente y cateterizando una vía intravenosa ya que puedes ser precisa la administración de sueroterapia casi en el 50% casos.

La adquisición de tolerancia al alimento implicado es más tardía que en otros tipos de alergia no IgE mediada, dependiendo del país de origen y los alimentos implicados. La tolerancia a las PLV se adquiere en torno a los 2-3 años, pero la tolerancia a sólidos no se adquiere hasta los 4-5 años. La provocación oral al alimento excluido se debe realizar al igual que la provocación diagnóstica en medio hospitalario. Es necesario realizar previamente un prick a IGE ya que muchos pacientes desarrollan en la evolución una alergia IgE mediada al mismo alimento, en cuyo caso se retrasará la edad de adquisición de tolerancia. 

## Tratamiento de la alergia a la proteína de la leche de vaca

El Dr. Luis Echeverría manifestó que existe un incremento documentado de la prevalencia de la alergia a la leche de vaca así como un aumento de la gravedad de las reacciones producidas por la alergia alimentaria. A largo plazo, existe un buen pronóstico de la alergia a la leche de vaca, alcanzando el 90% de tolerancia de forma natural. “La inmunoterapia oral con proteínas de leche de vaca es una opción proactiva de tratamiento para niños con alergia mediada por IgE a la leche de vaca, –reveló Echeverría- consiguiendo unas altas tasas de desensibilización, pero con frecuentes efectos adversos durante el procedimiento, siendo la mayoría de ellos de leve gravedad”. Este tratamiento permite a los niños alérgicos llevar una vida prácticamente normal, aunque precisa entrenamiento de la familia y del paciente en el reconocimiento y tratamiento de las reacciones adversas. 

## Qué les damos mientras se curan

El objetivo del tratamiento en la alergia a las proteínas de leche de vaca “seria conseguir la tolerancia” según expuso el DR. José Manuel Moreno, por lo que, mientras se consigue, “el tratamiento pasa por evitar los productos lácteos y garantizar un crecimiento adecuado”. Las [leches de fórmulas](https://madrescabreadas.com/2021/04/13/cuál-es-la-mejor-leche-de-fórmula/) sustitutivas han de ser seguras y eficaces para conseguir una adecuada nutrición y crecimiento. Las opciones terapéuticas son los hidrolizados extensos, las fórmulas de soja (en mayores de 6 meses y sin enteropatía) y las fórmulas elementales. Las más utilizadas son las fórmulas extensamente hidrolizadas, que consisten en “fórmulas infantiles con la proteína digerida en pequeñas porciones (péptidos de pequeño tamaño) que no desencadenan una respuesta inmunológica (alergia)”, como explicó Moreno. Ni las bebidas vegetales ni las leches de otros mamíferos constituyen una alternativa válida.