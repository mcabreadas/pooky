---
layout: post
slug: como-hablar-con-mi-hijo-adolescente
title: "Podcast: Cómo conectar con tu adolescente sin ser encimaria"
date: 2023-05-11T10:03:29.330Z
image: /images/uploads/cristina-mi-adolescente.png
author: .
tags:
  - podcast
---
Hoy nos descabreamos con Cristina Cuadrillero, psicóloga, creadora de la cuenta [@miadolescenteyyo](https://www.instagram.com/miadolescenteyyo/), apasionada de su trabajo en una editorial infantil y juvenil y madre de dos mujercitas adolescentes.\
Como profesional también sufrió el huracán “adolescencia” con su primera hija , pero saber relativizar, el humor y cambios a la hora de relacionarnos para lograr conectar con ellos, son sus ingredientes para sobrellevar esta etapa tan temida.\
Su cuenta de Instagram nació para ayudar a padres y madres que necesitan eliminar el sentimiento de culpa, saber que no son los únicos, que esto se pasa , que pueden contar con un espacio de desahogo y reflexión y que la adolescencia puede llegar a ser una etapa maravillosa.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1VWCFOLprRlN834liOpqMS?utm_source=generator&theme=0" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

En este episodio Cristina nos da trucos para que nuestras palabras lleguen a nuestros hijos y nos dice qué no debemos hacer nunca porque no funciona para comunicarnos con ellos:

¿Cómo hablar con tu hijo adolescente sin discutir?

¿Cómo hablar con tu hijo adolescente para que te escuche?

¿Qué temas debo hablar con mi hijo adolescente?

¿Cómo conseguir que tu hijo adolescente te cuente lo que le pasa?

¿Cómo llegar al corazón de un hijo adolescente?

¿Que decirle a mi hijo adolescente?

¿Que hacer cuando mi hijo adolescente me falta el respeto?

¿Como ayudar a tu hijo adolescente?

\
Os dejo tambien este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que te ayude a romper el hielo.

**Recomendaciones de este episodio:**

[Libro Invisible](https://amzn.to/44Q5HGo) (enlace afiliado)

[Las ventajas de ser un marginado](https://amzn.to/42MhL9P) (enlace afiliado)