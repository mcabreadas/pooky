---
author: maria
date: '2021-01-27T11:13:29+02:00'
image: /images/posts/vlog/p-diccionario.jpg
layout: post
tags:
- vlog
- adolescencia
title: Vlog boomers aprendiendo a hablar como los adolescentes
---

¿Os imagináis a dos boomers intentando hablar como los adolescentes?

Antes utilizábamos expresiones como "efectiviwonder" "guay del paraguay", "me piro, vampiro", o "la cagaste, burlancaster".

Ahora usan "estás mamadísimo", "eres el admin" o "está to gucci".

¿Sabes lo que significan? Os contamos su significado de éstas y muchas más expresiones de la jerga juvenil en el video de hoy.


<iframe width="560" height="315" src="https://www.youtube.com/embed/TTolRNCzYQw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Suscríbete y comparte!

<a href="https://foro.madrescabreadas.com" class='c-btn c-btn--active c-btn--small'>Participa en el foro</a>