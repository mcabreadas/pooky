---
author: maria
date: '2020-11-23T18:19:29+02:00'
image: /images/posts/vlog/p-black-friday.jpg
layout: post
tags:
- vlog
title: Vlog Compra segura en el Black Friday
---

Si vas a lanzarte a la aventura de comprar ropa, comprar zapatos, o comprar cosmética on line en el Black Friday frena un poco y actúa con precaución en Internet. Lee estos consejos para que tu compra acabe de un modo Feliz, y no más cabreada que una mona por no haber acertado o por sentirte engañada. 

En este video os muestro doy unos [consejos para comprar de forma segura en el Black Friday](https://madrescabreadas.com/2019/11/26/black-friday-precauciones/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/IAl8CF7vPkY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha gustado comparte y suscríbete a mi canal! Así me ayudarás a seguir generando este tipo de contenido.