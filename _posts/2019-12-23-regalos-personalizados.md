---
date: '2019-12-23T01:19:29+02:00'
image: /images/posts/regalos-personalizados/p-taza.jpg
layout: post
tags:
- trucos
- navidad
- regalos
- colaboraciones
- ad
title: Seis regalos personalizados para tu familia
---

Me encanta hacer regalos a mi familia con fotos divertidas o de cuando mis hijos eran pequeños. A veces no sabemos qué regalar, y no se nos ocurre que la mejor idea para conseguir un regalo original es hacer sentir especial a la persona a quien va dirigido. Es curioso que al ver nuestra cara en una taza de desayuno parece como si pasara de ser una simple taza a evocar un recuerdo o aflorar un sentimiento.
Os voy a dar unas ideas de regalos personalizados que podéis diseñar vosotras mismas con una calidad excelente y muy buen servicio en la [imprenta on-line Print24](https://print24.com/es/).

## Camisetas con una frase típica familiar para todos

Una vez hicimos una reunión familiar en una casa rural (nos juntamos más de 30 personas) y todos llevábamos la misma camiseta con una frase que venía muy a cuento y de las típicas que se dicen en las familias. Me pareció una gran idea para dar ambiente, y luego siempre nos quedó de recuerdo.

![camiseta personalizada navidad](/images/posts/regalos-personalizados/camiseta-navidad.jpg)

![camiseta personalizada zombie](/images/posts/regalos-personalizados/camiseta-negra.jpg)

## Tazas personalizadas para los cuñados

Una frase o viñeta irónica en una taza de desayuno puede ser una idea muy divertida para regalar a los cuñados que le sacan punta a todo. 

## Delantales personalizados para los cocinillas
En cada familia siempre hay alguien que destaca por hacer la mejor tortilla o el mejor bizcocho. Podrías hacer un diseño con su mejor plato como agradecimiento por deleitaros siempre con él cuando os reunís.

![delantal personalizado](/images/posts/regalos-personalizados/delantal.jpg)

## Material escolar personalizado

Imagina que los chicos lleven sus libretas de clase personalizadas para que no se confundan con las de sus compañeros, o unos post it con su diseño favorito.

![libretas personalizadas](/images/posts/regalos-personalizados/notePads.jpg)

## Tarjetas molonas para los adolescentes

Regálales un crédito para que diseñen sus propias invitaciones totalmente a su gusto para su fiesta de cumpleaños con sus amigos. Seguro que les hace mucha ilusión!

![tarjetas personalizadas](/images/posts/regalos-personalizados/foldedCards.jpg)

## Papel de carta personalizado para los nostálgicos
Os acordáis cuando escribíamos cartas a los amigos del verano? Y poníamos perfume en el sobre y lo decorábamos... Puede ser una bonita idea personalizar el papel de carta y animarnos a volver a usar el correo postal, aunque sea para ocasiones especiales.

![papel de cartas personalizado](/images/posts/regalos-personalizados/papel-cartas.jpg)

## Bolsas de tela para el pan
Para reducir el uso de plásticos una buena idea es diseñar una bolsa de tela chula para las pequeñas compras del super.

![bolsas de tela personalizadas](/images/posts/regalos-personalizados/bolsa-tela.jpg)

## Álbumes de fotos para los abuelos

Aunque hay abuelos digitales, a la mayoría les gusta sentir e tacto al pasar las hojas de papel de un álbum de fotos más que estar siempre mirándolas en el móvil o tablet. Así que una excelente idea es hacer una recopilación de fotos desde que nacieron sus nietos e imprimirla s en un libro para que lo miren cada vez que quieran y lo tengan en la estantería siempre a mano.

![album de fotos imprimir](/images/posts/regalos-personalizados/photoPrints.jpg)

Seguro que alguna de estas ideas te sirve para acertar con el regalo que estás buscando.

Si te ha gustado, comparte!


*Photo portada by Karly Jones on Unsplash*