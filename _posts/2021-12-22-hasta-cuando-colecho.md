---
layout: post
slug: hasta-cuando-colecho
title: ¿Hasta cuándo colecho?
date: 2022-01-10T12:14:31.298Z
image: /images/uploads/portada-hasta-cuando-colecho.jpg
author: luis
tags:
  - crianza
  - bebes
---
Cada día sois más las y los que nos preguntáis por el colecho y si estamos a favor o en contra de esta práctica. No es la primera vez que escribimos sobre este tema y dejamos clara nuestra posición con respecto a este tema, y es que estamos a favor siempre y cuando se trate de un colecho seguro y respetuoso para toda la familia. El tema de la lactancia, las noches y el descanso del bebé es el segundo tema que más preocupa a las mamás y papás primerizos.

Organizaciones como la OMS, hablan de los beneficios que tiene esta práctica en los peques, pero también son estos mismos organismos los que nos dan las pautas a seguir para un colecho seguro y sin riesgos para el bebé. Pero, sin duda alguna, el encontrar el momento de poner fin al colecho es algo que también preocupa. A todo esto le queremos dar respuesta con este artículo en el que queremos dejar claras las pautas para un colecho seguro y cuánto tiempo es recomendable llevar a cabo esta práctica.

## ¿Qué involucra un colecho seguro?

El colecho seguro hace referencia al poder dormir con el bebé recién nacido, o no, al que habilitaremos un espacio propio e independiente para descansar y dormir cerca de los padres. En ese espacio propio va a conseguir un descanso apto y diseñado para él. Tanto el Ministerio de Sanidad como otros organismos recomiendan la práctica del colecho de manera segura, que en muchas ocasiones pasa por utilizar una [cuna de colecho](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/) o cuna sidecar, aunque a falta de ella, deberíamos optar por camas que tengan las características necesarias para realizar esta práctica.

![colecho seguro](/images/uploads/colecho-del-bebe.jpg "colecho seguro")

Con ello conseguimos que el contacto entre el bebé y la madre sea más estrecho en los primeros meses de vida, pero además va implicar una serie de beneficios tanto para padres como para bebés. En primer lugar la tranquilidad y es que cuando el bebé duerme con sus padres y está cerca en todo momento, eso nos aporta una sensación de relajación porque le tenemos cerca y si algo le sucediera, el tiempo de reacción es más rápido.

Igualmente, habrá una respuesta más rápida para la lactancia, ya que el bebé nos quedará más a mano. Ese tiempo que ganamos al amamantar, también es fundamental para que el bebé y la madre consigan dormirse de nuevo más rápidamente. Aquí encontramos otro de los beneficios y es que mejora el sueño, tanto de los padres como del bebé. 

## ¿Cómo hacer colecho seguro en cama?

En español, la palabra colecho en la RAE no hace distinción a si esta práctica se lleva a cabo directamente compartiendo cama o si se hace a través de una cuna de colecho o cuna sidecar, como sí ocurre en inglés. No obstante, lo importante es saber que existen dos modos diferentes de colechar y que para ambos, hay que encontrar la manera más segura de practicarlo. Si vamos a optar por hacer colecho en una cama y prescindir de cuna de colecho, deberás tener en cuenta las siguientes pautas.

* La primera recomendación es que el bebé no sea prematuro y su estado sea saludable.
* Siempre acostar al bebé boca arriba, evitando a toda costa el dormir boca abajo ni de lado.
* No es recomendable abrigar en exceso al bebé y mantener la estancia en unos 20º de máxima.
* No cubrir la cabeza del bebé y evitar que algún complemento de la cama pudiera hacerlo, como por ejemplo almohadas, mantas, edredones, etc.
* Además, esta práctica debe llevarse a cabo en una superficie segura, firme y limpia. Presta atención a los huecos por los que el bebé pudiera caerse o quedarse atrapado. En este punto también cabe mencionar que es necesario evitar utensilios y juguetes en la cama, además de evitar que los animales de compañía duerman con el bebé.
* En la modalidad de colecho en cama habría que tener en cuenta el riesgo de aplastamiento, aunque muchos expertos descartan esta idea porque las mamás desarrollan un sentido por el que esto difícilmente ocurriría.

## ¿Cuándo no se puede hacer colecho?

![cuando no hacer colecho](/images/uploads/como-no-hacer-colecho.jpg "cuando no hacer colecho")

Por otra parte, hay otros factores que tener en cuenta, ya que si se cumplen no deberíamos practicar el colecho, ya que lo estaríamos haciendo de una forma insegura, sin seguir las pautas que los expertos recomiendan. Como os decimos la Asociación Española de Pediatría aconseja esta práctica, pero siempre teniendo en cuenta los factores que aumentan el riesgo de SMSL, [Síndrome de Muerte Súbita del Lactante](https://www.aeped.es/comite-nutricion-y-lactancia-materna/lactancia-materna/documentos/colecho-sindrome-muerte-subita-lactante-y). Estos factores son:

* No es recomendable hacer colecho, sobre todo en cama, cuando el bebé tiene menos de 3 meses.
* No se recomienda el colecho con bebés prematuros o con bajo peso al nacer.
* No es recomendable cuando los papás muestran síntomas de cansancio extremo, por ejemplo durante el postparto.
* Si los padres consumen tabaco, alcohol, algún tipo de estupefaciente o fármacos sedantes, tampoco es aconsejable el colecho. Colecho y alcohol son totalmente incompatibles, al igual que ocurre con el tabaco.
* Si la cama es compartida con otros familiares como hermanos, tampoco se recomienda esta práctica.
* Finalmente, el colecho debe ser realizado en superficies firmes, evitando así las demasiado blandas como sofás, sillones, colchones de agua, etc.

## ¿Cuánto tiempo se usa el colecho?

Pese a que mucha gente se empeñe en encontrar una edad exacta en la que abandonar el colecho, lo cierto es que esto no ocurre en un periodo preestablecido, sino que es algo que se irá dando. Sí, seguro que alguien te ha dicho que cuanto más tiempo, más te costará sacarlo de la cama y recuperar vuestra intimidad, pero ya os adelanto que no es para nada así. 

Estamos ante un proceso más en la evolución del niño y no te preocupes porque llegará el momento en el qué él mismo quiera abandonar el lecho familiar para pasar a hacerlo en solitario. Lo importante es que este proceso acabe como empezó, de la manera más natural y respetuosa posible para todos los miembros de la familia.

Puedes preparar con el niño o niña su habitacion de mayor, invitarle a que elija su edredon y premiara los ratitos que quiera pasarensu nuevacama"de mayor" en las siestas, por ejemplo... Poco a poco sera el peque quienquiera tener su propio espacio, incluso quiza una temorada este alternando ambas habitaciones. En esteblog lo hemos llamado "[colecho interactivo](https://madrescabreadas.com/2014/06/26/sueno-feliz-madrescabreadas/)".

Si estás iniciando en el colecho, no te preocupes en este momento por cuándo debes acabar con esta práctica. En su lugar, asegúrate de hacer las cosas bien, siguiendo las pautas indicadas para un colecho responsable. Y, sobre todo, preocúpate por disfrutar de esta etapa tan bonita de tu vida que está por comenzar.

*Portada by Simon Berger on Unsplash*

*Photo by David Veksler on Unsplash*

*Photo by kevin liang on Unsplash*