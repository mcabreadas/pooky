---
layout: post
slug: adiccion-al-ejercicio-fisico-en-adolescentes
title: 4 señales de que tu adolescente es adicto al ejercicio físico
date: 2023-01-09T12:55:42.286Z
image: /images/uploads/adolescente-haciendo-ejercicio.jpg
author: faby
tags:
  - adolescencia
---
El exceso de ejercicio físico en adolescentes puede desencadenar muchos problemas de salud.

Es evidente que durante la adolescencia se experimentan muchos cambios y **los jóvenes suelen sentir preocupación por su aspecto físico,** pero ¡cuidado! A veces realizan actividad física de forma desmedida.

Hoy hablaremos de los **peligros de desarrollar adicción hacia el deporte.**

## ¿Qué es la adicción al ejercicio físico?

La adicción o ejercicio físico compulsivo es un trastorno en el que el individuo desarrolla un **“fuerte deseo de ejercitarse”** a tal grado de presentar depresión y ansiedad al no poder realizarlo. Así lo describe el profesor de Grado en Ciencias de la Actividad Física y del Deporte de la Universidad Miguel Hernández de Elche; David González-Cutre.

Esta adicción no se reconoce como un trastorno mental, sin embargo, algunos expertos en Psiquiatría han realizado estudios de investigación sobre esta conducta.

## ¿Por qué algunos adolescentes hacen ejercicio físico en exceso?

Los adolescentes suelen ser enérgicos por naturaleza. Sin embargo, algunos jóvenes pueden excederse al ejercitarse debido a **conceptos erróneos de belleza.**

Este deseo de encajar en la sociedad puede conducirlos a fijarse **metas irreales.**

Por un lado, las chicas pueden fijarse un peso corporal muy por debajo de lo que establece la OMS. De hecho, se pueden desarrollar trastornos de la conducta alimentaria (TCA), anorexia o bulimia.

![Adolescente con trastorno de peso](/images/uploads/adolescente-trastorno-peso.jpg "Adolescente con trastorno de peso")

Por otra parte, los varones pueden obsesionarse por desarrollar una musculatura que no está en sintonía con la contextura o genética natural. Las pesas pueden deformar el cuerpo e incluso impedir el alcance de la estatura biológica.

![Adolescente levantando pesas](/images/uploads/adolescente-levantando-pesas.jpg "Adolescente levantando pesas")

## ¿Qué síntomas alertan de una adicción al ejercicio físico?

Aunque aún no se considera un trastorno mental, hay ciertos **parámetros de conducta en los jóvenes que pueden indicar este padecimiento**.

* **Síntomas de Abstinencia.** El joven suele experimentar ansiedad, irritabilidad, desorientación y otros malestares al no poder ejercitarse un día. Es similar a lo que ocurre con un drogadicto.
* **Descontrol.** El adolescente no puede controlar el deseo de ejercitarse, incluso cuando está lesionado.
* **Tiempo.** Dedica largas jornadas a hacer deporte, al grado de descuidar, estudios, relación familiar, amigos y otras facetas de la vida.
* **Insatisfacción.** Es posible que el joven no se sienta satisfecho de su entrenamiento.

## Consecuencias de ejercicio excesivo

![Adolescente se lesiona haciendo ejercicio extremo](/images/uploads/adloescente-se-lesiona-haciendo-ejercicio-extremo.jpg "Adolescente se lesiona haciendo ejercicio extremo")

Ejercitarse demasiado puede desencadenar **consecuencias físicas y mentales.**

* **Dolores y lesiones.** Los esguinces, tendinitis, epicondilitis, son habituales cuando se abusa del deporte. Estas lesiones pueden afectar la calidad de vida de las que lo padecen.
* **Problemas hormonales.** Las chicas pueden experimentar desórdenes hormonales por la rápida pérdida de peso.
* **Cambios de conducta.** La presión por el rendimiento deportivo puede llevar a desarrollar pensamientos negativos, falta de autoestima e incluso conducir al aislamiento social.

## Tratamiento y prevención de la adicción al ejercicio

Es de suma importancia que **los padres se involucren con sus hijos** y desarrollen empatía por sus temores.

Te dejo este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) que te ayudara  conectar .

Pueden ser muy útiles las **intervenciones de tipo cognitivo-conductual psicológico**, para que el adolescente pueda desarrollar un punto de vista equilibrado sobre el ejercicio.

Desde luego, se tiene que prestar ayuda si se presentan otros trastornos como depresión o incluso anorexia.

Para prevenir esta distorsión sobre lo que significa “estar saludable”, es necesario **practicar deporte con la guía de un profesional.**

Los padres también pueden ayudar a prevenir esta obsesión, al no promover aspectos físicos que la sociedad impone.

Mas informacion aqui sobre el [ejercicio fisico compulsivo en adolescentes, aqui](https://kidshealth.org/es/teens/compulsive-exercise.html).