---
layout: post
slug: libros-halloween-ninos
title: Los 8 libros para vencer el miedo, incluso en Halloween
date: 2021-10-27T09:20:30.433Z
image: /images/uploads/depositphotos_35195395_xl.jpg
author: luis
tags:
  - libros
  - planninos
---
Incentivar buenos hábitos de lectura es algo que los niños no solo deben recibir desde las escuelas sino que también en casa hay mucho trabajo por hacer. Aprovechar fechas señaladas como Halloween son la clave para ir adentrando a los más pequeños en el mundo de la lectura, pero en el día de hoy vamos a tener recomendaciones para todos.

Para ello, nos hemos centrado en la mítica colección de El Barco de Vapor que nos ofrece una división por series para identificar los libros que son aptos para cada edad. Con estos libros aprenderán que tener miedo es normal y cómo afrontar ciertas situaciones en las que [el miedo, que es una de las emociones basicas](https://www.mundopsicologos.com/articulos/cuales-son-las-emociones-basicas) del ser humano, les puede invadir. Sin más dilaciones, estas son nuestras recomendaciones de libros infantiles para Halloween 2021.

## Libros de Miedo - Serie Blanca

La serie blanca de El Barco de Vapor está dirigida a niños de una edad comprendida entre los 6-7 años, esos primeros lectores que necesitarán textos de línea cortas, tipografía amplia y un vocabulario adaptado a su edad. El texto, además, irá acompañado de ilustraciones que ayuden a los pequeños a entender lo que están leyendo, que suelen rondar de 32 a 70 páginas.

### Los Monstruos no me dejan dormir

La protagonista de esta historia es la Osita Coco la cual está intentando conciliar el sueño pero los monstruos no permiten que lo consiga. En el cuento vamos a encontrarnos con personajes como un fantasma, un león o una bruja, entre otros muchos. Osita Coco no sabe qué debe hacer para poder dormir y los consejos que va recibiendo no le sirven. ¿Qué puede hacer la pequeña osa para poder dormir sin que los monstruos la atormenten?

![monstruos no me dejan dormir](/images/uploads/monstruos-no-me-dejan-dormir.jpg "monstruos no dejan dormir")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/monstruos-dejan-dormir-Barco-Blanca/dp/8491077707/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3K46YHUZUTIDJ&dchild=1&keywords=los+monstruos+no+me+dejan+dormir&qid=1635245610&qsid=260-2841686-1419548&sprefix=los+monstruos+no+me+dejan+dormir%2Caps%2C100&sr=8-1&sres=8491077707%2C8467530693%2C8418483091%2C8494157825%2C8484645649%2CB08P3QTGJW%2C8491010637%2CB06VYH11DL%2C841830216X%2C8491010386%2C8448857747%2C8491014748%2C8467728582%2C846960564X%2C8484648583%2C8467043709%2C8448856090%2C8467765348%2C8426350100%2C8469847724&srpt=ABIS_BOOK&madrescabread-21) (enlace afiliado)

### ¿Quién dijo miedo?

Lo que parecía una apacible tarde jugando en el desván para los miembros de la pandilla ardilla se convierte en toda una aventura de miedo cuando la luz de la habitación se apaga. Nada es lo que parece en la oscuridad y todos los chicos parecen muy asustados, todos excepto Irene. 

![quien dijo miedo](/images/uploads/quien-dijo-miedo.jpg "quien dijo miedo")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/%C2%BFQui%C3%A9n-miedo-Barco-Vapor-Blanca/dp/8467576960/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3HC9WMCECRGH&dchild=1&keywords=quien+dijo+miedo&qid=1635245683&qsid=260-2841686-1419548&sprefix=quien+dijo+miedo%2Caps%2C121&sr=8-1&sres=8467576960%2C8467578106%2C8467585900%2C8413180171%2C8496192105%2C8413922089%2C8467578130%2C8413180775%2CB08ZF1NRWT%2C8491072837%2C846759585X%2C8413921643%2CB09FGMQSHB%2C9801299673%2C8491825487%2CB094W5Y3XG%2C8426147151%2C8491820566%2C8491825169%2C8467591595&srpt=ABIS_BOOK&madrescabread-21) (enlace afiliado)

## Libros de Miedo - Serie Azul

Un pasito más lo encontramos con la serie azul, dirigida ya a niños que saben leer, a partir de 7 años. Se trata de libros cortos, entre 64 y 90 páginas, con letras todavía de un tamaño considerable y con ilustraciones a color que complementan al texto para su comprensión. De esta serie, los seleccionados son:

### La agradable y misteriosa anciana succionadora de niños

La señora llamada Remedios parece una abuelita apacible, como lo sería cualquier abuela de la ciudad. Tiene un rostro con arrugas y una sonrisa que consigue enternecer a los pequeños. Todo parece normal en ella hasta que un día todos son testigos de cómo se acerca a una niña, le da un beso y la succiona hasta que no queda ni rastro de la joven. Con este plantel, desde luego que hablamos de un libro que les será muy fácil de leer.

![agradable y misteriosa anciana](/images/uploads/la-agradable-y-misteriosa-abuela.jpg "agradable y misteriosa anciana")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/agradable-misteriosa-anciana-succionadora-ni%C3%B1os/dp/849182829X/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1OV54AH0QJ0JH&dchild=1&keywords=la+agradable+y+succionadora+abuela&qid=1635245732&qsid=260-2841686-1419548&sprefix=la+agradable+y+succionadora+abuela%2Caps%2C94&sr=8-1&sres=849182829X%2CB09FJXGQSS%2C8494625152%2CB08BZWZP3W%2CB07ZL9VZGW%2CB08XK4NK3L%2CB07FPGWPJ7%2CB08WLZ1DKR%2CB07VP3WVFV%2CB08929CVBF%2CB08KJ9PJJY%2CB07H286BSG%2CB09HGMRWQW%2CB09HH4M7VN&madrescabread-21) (enlace afiliado)

### El misterio de la Casa del Palomar

No podemos recomendaros libros de Halloween y olvidarnos de los libros de misterio e intriga. En este caso hemos seleccionado El Misterio de la Casa del Palomar porque refleja la importancia de la familia mientras nos narra una historia que atrapa desde las primeras páginas. Los protagonistas serán Rosa y sus nietos gemelos Rita y Ramón, los cuales se mudan a vivir a la Casa del Palomar, una mansión abandonada de la que nadie en el pueblo quiere siquiera mencionar. ¿Qué encontrarán dentro de la Casa del Palomar?

![el misterio de la casa del palomar](/images/uploads/misterio-casa-palomar.jpg "el misterio de la casa del palomar")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/misterio-Casa-Palomar-Barco-Vapor/dp/8491072829/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3TWQO0SA33CE8&dchild=1&keywords=el+misterio+de+la+casa+del+palomar&qid=1635245821&qsid=260-2841686-1419548&sprefix=el+misterio+de+la+casa+del+palomar%2Caps%2C113&sr=8-1&sres=8491072829%2CB07NP6N8Q3%2C8401022878%2CB099MN6J4D%2C8408238787%2C849129354X%2C8467587350%2CB017HX1FO2%2C8417752927%2C8467582677%2CB06WLH1KQW%2C8467589213%2C8498452902%2CB0080TP512%2C1983307688%2C8408128779%2C8467579684%2CB00CXAD2MI%2CB07CBDZ3CK%2C8427222629&srpt=ABIS_BOOK&madrescabread-21) (enlace afiliado)

## Libros de Miedo - Serie Naranja

La serie naranja de El Barco de Vapor está orientada a niños a partir de los 8 años o más que busquen retos nuevos con textos más largos. Aquí la extensión de las novelas puede superar las 200 páginas y las ilustraciones pierden color y son escalonadas en el texto. Las temáticas y el vocabulario también se va adaptando a edades más adultas y los elegidos en la temática de Halloween para esta recomendación son los siguientes.

### Estoy detrás de ti  y otros cuentos de terror

El siguiente título no es solo una historia sino que se trata de una compilación de cuentos de terror que nos presenta historias muy diversas e inquietantes. En este libro habrá de todo un poco: casas abandonadas, desconocidos extremadamente amables, sombras acechantes en la oscuridad, juguetes que cobran vida y mucho más en estas terroríficas historias para no dormir.

![estoy detrás de ti](/images/uploads/estoy-detras-de-ti.jpg "estoy detrás de ti")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Estoy-detr%C3%A1s-Barco-Vapor-Naranja/dp/8467554290/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3GRM4F04GA5YW&dchild=1&keywords=estoy+detras+de+ti&qid=1635245950&qsid=260-2841686-1419548&sprefix=estoy+detras+de+ti+%2Caps%2C138&sr=8-1&sres=8467554290%2CB007VD6K1M%2CB08LKP74GP%2C8408195964%2C841831883X%2CB085MNKMBN%2C8469755463%2C8469747010%2CB088DCZX43%2CB0725P9D3Q%2CB08T77BRWN%2CB07BFSKPDB%2CB07NKNSVQC%2CB0822926HK%2CB08SJ117PV%2CB07B7JP71S%2CB075MH4X7W%2CB092LPG7S7%2CB018XNNSJA%2CB072VMK4PK&madrescabread-21) (enlace afiliado)

### Amanecer Zombi

Para disfrutar de esta historia nos trasladaremos hasta Rumanía donde se encuentra el papá de nuestro protagonista realizando uno de sus trabajos como maquillador de cine. La historia nos la cuenta Fran, acompañado de su perro Watson, que serán testigos de la grabación de la película Amanecer Zombi. Todo parecía normal, pero la aparición de un fantasma comenzó a dar quebraderos de cabeza a nuestro amigo Fran. ¿Quién hay detrás de este fenómeno paranormal?

![amanecer zombi](/images/uploads/amanecer-zombi.jpg "amanecer zombi")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Amanecer-zombi-Barco-Vapor-Naranja/dp/8467590319/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2LR8IPUORLINY&dchild=1&keywords=amanecer+zombi&qid=1635246015&qsid=260-2841686-1419548&sprefix=amanecer+zombi%2Caps%2C130&sr=8-1&sres=8467590319%2C8496710173%2CB09FS58H1T%2CB0856T2W7W%2C8415870345%2CB086WNS4FW%2CB0763TJLNK%2CB07KG265M6%2CB018632PUU%2CB08GWT357N%2C1646946529%2CB08PNZQT3X%2C8494112805%2CB08ZSWH9SB%2CB09BZJF83X%2CB08HMPKWYV%2C8490627606%2CB08R62F9KC%2CB08D6LYKV8%2CB07YR46BZ7&madrescabread-21) (enlace afiliado)

## Libros de Miedo - Serie Roja

Los títulos que encontraremos en la serie roja de El Barco de Vapor son aquellos que van dirigidos a un público de lectores ya experto, de los 10 años en adelante. El número de páginas en estos libros ya puede superar las 250 páginas y se va introduciendo un vocabulario más adulto y acorde con la edad que tienen.

### El Misterio del Ópalo Azul

En El Misterio del Ópalo Azul se puede notar como tanto las historias como el vocabulario dan un salto para contentar a su público objetivo. La historia cuenta la leyenda del ópalo azul del que, desde hace siglos, se dice que todo aquel que posea la joya morirá de manera trágica. En estos tiempos el misterio es más oscuro y los encargados de descubrirlo serán los protagonistas de la novela, Mario y su sobrina Elisa. Ellos serán los encargados de resolver el misterio que esconde el ópalo azul para que todos quieran tenerlo y, a su vez, todos se alejen de él.

![misterio del ópalo azul](/images/uploads/opalo-azul.jpg "misterio del ópalo azul")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/El-misterio-del-%C3%B3palo-azul/dp/8413183855/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F69IA33HTA2C&dchild=1&keywords=el+misterio+del+opalo+azul&qid=1635246116&qsid=260-2841686-1419548&sprefix=el+misterio+del+opalo+azul%2Caps%2C121&sr=8-1&sres=8413183855%2C8477025622%2C8467589213%2C8467577061%2C8412031032%2C8412031075%2C8412031059%2C8467591935%2C8491077758%2C8417416293%2C8491825754%2CB09K6BZGST%2C8467597763%2C8469809067%2C8477024316%2CB088YR3RDJ%2CB097BP4M9W%2CB09DYFJR1J&srpt=ABIS_BOOK&madrescabread-21) (enlace afiliado)

### El coleccionista de relojes extraordinarios

Laura Gallego es la autora de El coleccionista de relojes extraordinarios, la historia que nos cuenta cómo Jonathan y su familia acaban visitando un museo de relojes cuyo propietario es un misterioso Marqués. Los problemas comienzan cuando al entrar en la sala de los relojes extraordinarios, la madrastra de Jonathan desobedece las advertencias y toca uno de los relojes, dejando su alma atrapada en él. Ahora Jonathan debe salvar a su madrastra, para lo que solo tendrá 12 horas y lo hará con la ayuda del Marqués que le dará las claves para conseguirlo.

![el coleccionista de relojes extraordinarios](/images/uploads/coleccionista-de-relojes.jpg "el coleccionista de relojes extraordinarios")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/coleccionista-relojes-extraordinarios-Barco-Vapor/dp/8467589507/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=22HY5IE8UJ066&dchild=1&keywords=el+coleccionista+de+relojes+extraordinarios&qid=1635246187&qsid=260-2841686-1419548&sprefix=el+coleccionista+de+relojes+extraordinarios%2Caps%2C109&sr=8-1&sres=8467589507%2C841570979X%2C8467577908%2C8490439311%2CB07NXWXPSD%2CB081ZH2GB1%2C8467577878%2C1535525312%2C8461700511%2C8490439567%2C8491420339%2CB085KT89HH%2CB08ZFL4Q9W%2C1716331684%2CB086BH8W9C%2CB07MH66X16%2CB09GZPF6TH%2C8490433712%2CB09GZJPWFS%2CB09GZPYMX9&srpt=ABIS_BOOK&madrescabread-21) (enlace afiliado)

Con estos libros de Halloween para niños no habra susto que se nos resista.

Te dejo esta recomendacion del libro [150 actividades infantiles sin pantalla](https://madrescabreadas.com/2016/11/30/planes-caseros-con-niños-150-actividades-sin/)s.

Comparte en tus redes si te ha sido de ayuda.



*Portada by Clint Patterson on Unsplash*