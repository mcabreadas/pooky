---
layout: post
slug: reflujo-bebes-sintomas
title: Las 4 respuestas que estabas buscando sobre el reflujo de tu bebé
date: 2022-01-11T15:53:51.245Z
image: /images/uploads/istockphoto-1311353482-612x612.jpg
author: faby
tags:
  - bebes
  - crianza
---
Ser mamá es maravilloso, pero tiene sus desafíos. En ocasiones el **reflujo puede hacer que tu bebé llore mucho.** Aunque no es una afección grave, puede causar algunas incomodidades en tu peque.

Es de suma importancia que sepas detectar cuándo se trata de reflujo, de este modo puedes saber qué tipos de alimentos se adaptan mejor a tu bebé. En esta entrada te hablaré del **reflujo gastroesofágico**, síntomas y cómo puedes controlar este malestar.

## **¿Qué es el reflujo gástrico infantil?**

Para entender de qué se trata esta molestia debes saber que el esófago es un tubo que lleva el alimento (flujo) de su boca al estómago. Pues bien, cuando tu hijo tiene reflujo, el **alimento que ya estaba en su estómago vuelve al esófago y causa acidez.**

No es una patología grave, de hecho es una molestia muy común en los críos, debido a que aún están muy pequeños y pasan mucho tiempo acostados.

Generalmente **disminuye después de los seis meses** porque es el tiempo en que empieza a sentarse. A los 18 meses este problema es casi inexistente.

Algunas madres solo reconocen este malestar cuando el niño vomita. No obstante, algunos peques sufren de **reflujo silencioso**, el cual puede causar dolor y hacer que el niño llore con frecuencia.

Cuando el reflujo se extiende por muchos meses e interfiere en su alimentación entonces se trata de **reflujo gastroesofágico (ERGE)**. Requiere de atención médica.

## **Causas del reflujo en bebés**

Este malestar se da debido a que el **sistema digestivo aún no ha madurado**. Es decir, los músculos del esfínter esofágico inferior están débiles, por lo tanto cuando tu hijo traga, el esfínter se abre, pero no se cierra como debería, así que la comida se devuelve.

A medida que el niño crece los músculos empiezan a fortalecerse y el problema de reflujo desaparece. Si tu hijo es prematuro, presentará este malestar.

## **¿Cuáles son los síntomas del reflujo infantil?**

El reflujo silencioso es difícil de diagnosticar por cuenta propia. En ese sentido, lo mejor es que vayas al pediatra y le expliques los síntomas que presenta tu bebé.

Ahora bien, **hay varias señales que pueden indicar que se trata de reflujo.** Claro, algunos síntomas pueden ser distintos de un bebé a otro.

* Vómito o regurgitación.
* Llanto excesivo, a veces llora por horas.
* Tiene gases, pero no eructa.
* Náuseas, ahogos.
* Irritabilidad después de comer.
* El bebe tira la cabeza hacia atrás, arqueando la espalda después de comer.
* En casos graves pérdida de peso o poco aumento.
* Vómitos fuertes.

Debes estar atenta a los cambios en tu bebé. Algunos niños no lloran, pero suelen retorcerse, por eso ante cualquier duda acude al médico.

## **¿Cómo aliviar el reflujo en los bebés?**

Aunque no puedes curar este malestar, si puedes hacer mucho para reducir los episodios de reflujo.

Te dejo este post [conconsejos para aliviar el reflujo](https://madrescabreadas.com/2015/11/20/reflujo-bebes/) basado en la experiencia de una de nosotrascon su bebe. 

* Mantén a tu hijo 30 minutos en posición vertical después de comer.
* Usa un portabebes
* Ayúdalo a que eructe.
* Aliméntalo con la dosis adecuada. Algunos niños tienden a comer mucho y eso les causa el reflujo.
* Consulta con el médico antes de cambiar la fórmula de la leche.

Puede ser de mucha utilidad los cojines cuña anti-reflujo. Solo debes colocar el cojín bajo el colchón o como almohada para **darle la inclinación necesaria a tu bebé para reducir los cólicos y el [reflujo](https://enfamilia.aeped.es/temas-salud/reflujo-gastroesofagico-en-bebes).**

[![](/images/uploads/cojin-cuna-anti-reflujo.jpg)](https://www.amazon.es/roba-kids-0446-Cojín-cuña-anti-reflujo/dp/B00TV61QKW/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&crid=31Z7SLS7T7LYV&keywords=colchón%2Bantireflujo%2Bbebe&qid=1636470283&qsid=259-8252965-5643646&sprefix=colchón%2Banti%2Caps%2C438&sr=8-5&sres=B01MCUK1WG%2CB08JVV837W%2CB07RQJZ43G%2CB091GGHHSH%2CB00TV61QKW%2CB07RQK11CR%2CB07ZRQZYTM%2CB0735RYBK2%2CB078KNPMMG%2CB076F97QNX%2CB07YB3CPJR%2CB00VWRMRJ2%2CB0896HVYFC%2CB008Y2DFLM%2CB07W76BTVP%2CB06X43RDG3%2CB004M3PS4O%2CB07D9256ZM%2CB015945WX6%2CB088TSP9SN&srpt=MATTRESS&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/roba-kids-0446-Cojín-cuña-anti-reflujo/dp/B00TV61QKW/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&crid=31Z7SLS7T7LYV&keywords=colchón%2Bantireflujo%2Bbebe&qid=1636470283&qsid=259-8252965-5643646&sprefix=colchón%2Banti%2Caps%2C438&sr=8-5&sres=B01MCUK1WG%2CB08JVV837W%2CB07RQJZ43G%2CB091GGHHSH%2CB00TV61QKW%2CB07RQK11CR%2CB07ZRQZYTM%2CB0735RYBK2%2CB078KNPMMG%2CB076F97QNX%2CB07YB3CPJR%2CB00VWRMRJ2%2CB0896HVYFC%2CB008Y2DFLM%2CB07W76BTVP%2CB06X43RDG3%2CB004M3PS4O%2CB07D9256ZM%2CB015945WX6%2CB088TSP9SN&srpt=MATTRESS&th=1&madrescabread-21) (enlace afiliado)

Si tras estos cambios tu hijo no mejora, entonces **consulta con el médico tratamientos para reducir la acidez del estómago.** Ser mamá es maravilloso, pero hay que estar atentas para mantener la buena salud de los niños.

*Photo Portada by Miljan Živković on Istockphoto*