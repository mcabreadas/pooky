---
date: '2019-12-16T01:00:29+02:00'
image: /images/posts/transformar-dormitorio/p-chica-tumbada.jpg
layout: post
tags:
- adolescencia
- trucos
- decoracion
title: Transforma una habitación infantil en juvenil
---

¿No tenéis a veces la sensación de que habéis cambiado tanto como familia que vuestra casa ya no se adapta a vuestras necesidades? Si tenéis hijos adolescentes me daréis la razón ya que, si la vida nos cambió cuando nacieron, ahora que empiezan a abandonar la etapa infantil, de repente se nos da la vuelta como un calcetín. En esta etapa necesitan más espacio personal y también para sus cosas, sus gustos cambian y se desarrolla su personalidad. A la mayoría les deja de gustar su habitación infantil, y empiezan a pedir una habitación juvenil. En nuestro caso un sólo baño no es suficiente para dos adolescentes de distinto sexo y un niño de 7 años si no queremos que las batallas campales se reproduzcan cada mañana. Pero lo primero que nos planteamos es el tema económico, y para ello lo mejor es tantear un poco los precios de nuestra zona con una sencilla búsqueda en Google tipo [reforma integral barcelona](https://www.grupotecnicobarcelona.com/) o [precios reformas integrales barcelona](https://www.grupotecnicobarcelona.com/reformas-barcelona-presupuesto/). 

Con estos datos ya sabremos si podemos acometer la reforma entera de nuestra vivienda o, caso de que se nos suba mucho de presupuesto, siempre tenemos la opción de limitarnos a las zonas más urgentes, o ingeniárnoslas para lograr una habitación juvenil barata. Con estas ideas podrás dar un nuevo aire a la habitación infantil.

## 1 Pinta las paredes de la habitación juvenil
Esto cambiará el concepto del dormitorio infantil totalmente. Elige tonos suaves tipo chalk paint, y que transmitan calma y buena energía. Es más aconsejable una buena mano de pintura que decorar con papel ya que, además de ser más caro, es menos versátil, y si se cansa será más difícil cambiarlo.
Una buenísima idea es decorar la pared con vinilos de la temática que le guste a tu adolescente.

## 2 Cambia el textil del dormitorio infantil
Te sorprenderá lo que puede llegar a cambiar una estancia simplemente poniendo colchas y cortinas nuevas. Lo mejor es elegir colores neutros, nada demasiado cantoso, para poder darle el toque personal de tu hijo con los cojines o con la alfombra que, por otra parte, siempre serán más fáciles de sustituir sise cansa.

![cojines-ikea](/images/posts/transformar-dormitorio/cojines.jpg)

## 3 Elige complementos para el dormitorio juvenil
Quita los cuadros de dibujos y dalla opción de que elija láminas que le gusten. Enmárcaselas y verás qué cambio! Elegid un tablón de notas de imanes por ejemplo para su horario y sus recordatorios, y ponle un espejo (esto, además es práctico, porque reducirá sustancialmente su tiempo de ocupación del baño).

![dormitorio-juvenil-chica-cojines](/images/posts/transformar-dormitorio/habitacion-juvenil-chica.jpg)

## 4 Cambia la distribución del dormitorio juvenil
Si el espacio es reducido, opta por la distribución en L, de manera que la cama quede pegada a una pared, y el escritorio, en la otra, junto a la ventana, procurando que la luz le entre por la izquierda, siempre que sea posible. Esto dejará más espacio de habitabilidad a la estancia.

## 5 La iluminación es importante
A este aspecto no se le suele prestar demasiada atención, pero es crucial, tanto para la salud visual de nuestros hijos como para lograr un ambiente acogedor y relajado.
Son al menos necesarios tres puntos de iluminación:
- Una lampara para la mesita de noche
- Un flexo de intensidad regulable para el escritorio
- Un punto de luz en el techo para la iluminación general de la habitación.

## 6 Abusa del almacenaje para el dormitorio de adolescentes
Las madres siempre nos quejamos de lo desordenados que se vuelven nuestros hijos cuando llegan a la adolescencia. Y es cierto que se muestran más despistados, pero pongámoselo fácil y démosle espacios adecuados para guardar sus cosas según las actividades que les gusten.
Si se mueven en skate, o si juegan a baloncesto, o si tocan algún instrumento, podemos adecuar el tipo de almacenaje para facilitarles que lo dejen todo en su sitio cada día.
Esto les ayudará a estar más organizado y les dará seguridad.

![habitacion juvenil chico](/images/posts/transformar-dormitorio/habitacion-juvenil-chico.jpg)

## 7 Lugar de estudio para la habitación juvenil
Es muy importante procurarles un lugar de estudio fijo y confortable. Si no disponéis de suficiente espacio en su habitación, podéis plantearos crear una zona común de co-working familiar en el salón de casa, lo que os permitirá seguirle la pista de sus deberes, estar accesible para echarle una mano si lo necesita, y quién sabe, quizá también os ayude a vosotras en vuestro trabajo. 
Este espacio común sin duda propiciará interesantes sinergias entre los distintos miembros de la familia.
 
## 8 Muebles multifuncionales
Opta por muebles a medida para aprovechar el espacio al máximo porque, aunque son más caros, te van permitir optimizar el espacio de la estancia.
Los muebles multifuncionales son ideales para esta etapa del adolescente, ya que le permitirán lograr una pequeña zona para recibir a sus amigos en su cuarto si dispone de un  sofá cama con grandes cojines, o de una litera con espacio abajo par poner una alfombra y unos pufs.

¿A que ya no ves tan difícil transformar la habitación de niño de tu hijo en una habitación juvenil? 

Lánzate al cambio y comparte este post si te ha servido de ayuda!





- *Photo de portada by Tobias Nii Kwatei Quartey on Unsplash*
- *Photo 1 by chuttersnap on Unsplash*
- *Photo 2 by NeONBRAND on Unsplash*
- *Photo 3 by Christa Grover on Unsplash*