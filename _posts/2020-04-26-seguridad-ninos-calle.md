---
date: '2020-04-26T18:19:29+02:00'
image: /images/posts/seguridad-ninos-calle/p-alfonso-x.jpg
layout: post
tags:
- familia
- salud
- crianza
title: Niños en la calle. Medidas de seguridad e higiene
---

Hoy las calles han recobrado su vida gracias a la salida de los niños a jugar y pasear tras haber estado confinados en cuarentena durante más de 40 días.

Son unos campeones y lo han hecho super bien todo este tiempo! 

Hoy los he visto retraídos, a veces sin saber bien qué hacer o cómo saludar a quienes se cruzaban con ellos, pero creo que pronto normalizarán la situación y se adaptarán a esta nueva forma de relacionarnos.

He visto abuelos y abuelas emocionados en los balcones tras ver por primera vez en mucho tiempo a sus nietos y también he visto las ganas que tenían de abrazarlos.

Comienza el alivio de las medidas de confinamiento por el Covid-19 o Coronavirus, y es precisamente ahora cuando tenemos que tomar todas esas precauciones que llevamos aprendiendo desde hace tiempo. 

Ahora que vamos a salir todos poco a poco de casa es cuando no podemos relajarnos! Todavía seguimos en guerra con el bicho y vamos a ganar! Pero no nos despistemos, por favor.

Os dejo el directo de Instagram que hice con la Dra. Amalia Arce, más conocida en las redes sociales como [@lamamapediatra](https://www.dra-amalia-arce.com), que trabaja como pediatra y responsable de comunicación en el [Hospital HM Nens](https://hospitaldenens.com/es/) (antes, hospital de Nens) en Barcelona y además dirige el centro [Mon Pediátric](https://monpediatric.com) en Barcelona. 

## Video del directo de Instagram Mamá pediatra-Madres cabreadas

[Pincha aquí para ver el directo de Amalia Arce](https://youtu.be/bPNykmnXQs0)

En este video la Dra. Arce responde a preguntas como:

-es seguro salir?

-Cómo mentalizamos a los niños para que se sientan preparados y seguros para salir a la calle y para que cumplan todas las recomendaciones?

-Es aconsejable que llevan mascarillas? Cuales son las más recomendables? Qué hacemos si las que hemos conseguido les están grandes?

-Es aconsejable que lleven guantes?

-es conveniente usar pantallas protectoras?

-qué más precauciones debemos tener antes de salir de casa?

-es aconsejable que llevan patinete o bicicleta?

-es aconsejable que jueguen al balón?

-hay coronavirus en el aire o en el suelo?

-si encontramos a un amigo por la calle que debemos hacer?

-es prudente que hagan algún deporte o mejor sólo pasear?

-cómo debemos salir con bebés? Es mejor cochecito o porta bebés? Si llora y lo tenemos qué coger después de haber tocado cosas?

-Podemos protegerlos con algo tipo de mascarilla o no es necesario?

-hora de volver a casa: cuál sería el protocolo correcto desde que estamos por la puerta?

-debemos desinfectar los patinetes, bicis, cochecitos de bebés? Cómo?

-debemos desinfectar los zapatos?

-debemos lavar la ropa al llegar?

-debemos ducharnos todos al llegar?

-pueden sentarse en un banco?

-pueden sentarse en el césped?

-cuándo no deben salir los niños? Tos y mocos?

-Podemos ir a la playa



## Resumen en tweets

Éste es el resumen en tweets que ella misma publicó para que tengamos claro madres y padres cómo salir a a calle, cómo estar en la calle y qué hacer cuando volvamos a casa.

![mama pediatra tuit](/images/posts/seguridad-ninos-calle/tuit-uno.jpg)

![mama pediatra twitter](/images/posts/seguridad-ninos-calle/tuit-dos.jpg)

Espero que os haya aclarado dudas.

Si te ha resultado útil comparte!