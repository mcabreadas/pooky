---
layout: post
slug: tentempies-saludables-para ninos
title: Ideas de tentempiés saludables para que los niños lleven a la piscina
date: 2021-08-06T09:09:01.645Z
image: /images/uploads/kenta-kikuchi-1c55vifjsuq-unsplash-1-.jpg
author: Lucía
tags:
  - recetas
  - crianza
---
¿No se te ocurren tentempiés saludables para niños para bajarlos a la piscina, la playa o el parque sin caer en la rutina? Voy a darte algunas ideas explicándote qué es bueno y, sobre todo, qué no deben comer los niños a deshoras.

## ¿Qué es un snack no saludable?

Seguro que los snacks, almuerzos o meriendas no saludables ya los conoces, pero hay algunos con los que puede que tengas dudas.

Galletas y chocolates industriales, productos con un [alto porcentaje de azúcar](https://madrescabreadas.com/2019/12/10/azucar-leche/), un único alimento (trata siempre de introducir nutrientes diferentes), cremas con alta cantidad de grasas (aunque también sean proteicos, como es el caso del paté), almuerzo hipocalórico (huimos de las grasas malas, pero no de las calorías)...

Todo ello debe ofrecerse de manera excepcional y en pequeñas cantidades. Por supuesto, nadie es de piedra, de manera que te animamos a cocinar recetas alternativas más sanas para quitar el antojo.

## ¿Qué dar a un niño a media mañana?

Con un poco de imaginación, o no tanta, puedes conseguir cantidad de snacks saludables para niños, de manera que no repita almuerzo o merienda en toda la semana si así lo deseas.

### Jam&Cheese rolls

Los enrollados de jamón cocido y queso tienen proteína e hidrato, ideal para que el niño se ejercite jugando en el parque o nadando. Al tener algo de grasa (esto dependerá del queso que escojas) te aconsejamos que sea un snack más de media mañana. Sin embargo, es muy típico para las tardes por su facilidad al prepararlo; si es el caso, asegúrate de optar por quesos menos grasos y de hacerlos los días en los que el niño realice una mayor actividad.

1. Haz un sándwich con dos lonchas de jamón cocido y una de un queso tierno.
2. Utiliza un rodillo amasador para aplastarlo. Así, el bocadillo quedará plano y los ingredientes, pegados.
3. Corta un trozo de plástico transparente y pon el emparedado encima.
4. Enróllalo sobre sí mismo y déjalo de esta forma apretado con el mismo plástico para que se mantenga la forma.
5. Ponlo a enfriar en la nevera durante una hora.
6. Corta en tres o cuatro pedazos y retira el plástico para ofrecérselo a tu peque.

![tentempiés saludables para niños](/images/uploads/pixzolo-photography-biwb1y8wpzk-unsplash.jpg "Pixzolo Photography - Unsplash")

### Sticks de zanahoria

Uno de los aperitivos saludables y fáciles de preparar e ideal para aumentar el consumo de micronutrientes e introducir alimentos, seamos sinceros, de los menos deseados por los peques, las hortalizas y las verduras.

1. Retira la punta y la base de la zanahoria.
2. Pélala.
3. Córtala.
4. Haz dos cortes a lo largo.
5. Ahora corta los tres pedazos verticalmente y las láminas en sticks.
6. Ponlos en agua en un tupper hermético o bote, de este modo, se mantendrá frescos, duros y no se oxidarán.
7. Saca la cantidad deseada y escurre unos minutos antes de tomar para que no tengan tan baja temperatura.

### Tupper completo

Otro de los tentempiés saludables para niños que puedes preparar en un momento y llevar a cualquier parte es el tupper completo. 

Se trata de incluir alimentos que incluyan el máximo de grupos de nutrientes y que al tiempo se mantengan bien fuera de la nevera. Unos ejemplos son:

* Huevo duro+tomates cherry+olivas+bonito en conserva.
* Tortilla francesa+pimiento+pechuga de pollo en hebras.
* Pasta hervida+fiambre de pavo en tacos+cubos de zanahoria.

## ¿Cuál es la mejor fruta para comer a media mañana?

De este grupo de alimentos, te recomendamos optar por frutas energéticas. Con el almuerzo aún quedan muchas horas al día y necesitamos mantener un nivel de vitalidad óptimo. Los niños queman muchas calorías, de manera que es importante darles frutas muy completas, con el máximo de proteína, fibra e hidrato así como de micronutrientes.

Las frutas para hacer golosinas sanas para niños o, simplemente, para ofrecerlas, lavadas, peladas y lavadas, son:

* Higos. Tienen una alta dosis de azúcares naturales así como muchísima fibra.
* Naranja. Esta favorece la formación de aminoácidos, proteínas realmente importantes.
* Plátano. Es ideal para jornadas en las que estén previstas actividades de rendimiento físico ya que reduce la fatiga, conteniendo bastante hidrato de carbono y, atención, un muy buen índice glucémico.
* Aguacate. Es un fruto que tiene de todo. Grasas saludables, cada vez menos presentes en las dietas, carbohidratos y proteínas. Ojo porque es saciante, no lo des si se va a tomar la comida poco después.

Con toda esta información seguro que se te ocurren cantidad de ideas chulas para hacer tentempiés saludables para niños para tomar en el almuerzo o la merienda.

Te dejo tambien:

[4 recetas de helados caseros saludables](https://madrescabreadas.com/2021/07/15/helados-caseros-saludables/) y un metodo que revolucionara la organizacion de las comidas en tu casa: el [metodo batch cooking](https://madrescabreadas.com/2020/09/05/batch-cooking/), para cocinar una vez y comer saludable toda la semana.

Comparte si te ha resultado de utilidad y suscribete para no perderta nada!

*Photo portada by Kenta Kikuchi - Unsplash*

*Photo text: Pixzolo Photography - Unsplash*