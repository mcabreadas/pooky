---
layout: post
slug: sillas-de-coche-estrechas
title: Las sillas de coche más estrechas del mercado
date: 2021-05-11 09:12:00.109000
image: /images/uploads/istockphoto-1004789644-612x612.jpg
author: luis
tags:
  - puericultura
  - crianza
---
En general, elegir la silla de coche perfecta para nuestros hijos es un auténtico dilema. El mercado está copado de grandes marcas que podrían ser muy buenas opciones. La cosa puede llegar a complicarse más cuando hablamos de familias numerosas, que tienen que encontrar la manera de [encajar tres sillas en un coche de tamaño medio](https://madrescabreadas.com/2017/01/25/silla-auto-regulable/). 

Esta debilidad de los modelos convencionales no ha sido ignorada, ya que actualmente podemos encontrar marcas que han apostado por crear sillas de coche estrechas. Hoy vamos a hablar sobre ello y a presentaros algunas de **las sillas de coche más estrechas del mercado** que pueden solucionar este problema. Con relación a ello, también vamos a hablar sobre el nuevo sistema I-size que regula la seguridad en las sillas de coche para bebés y niños.

<iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=30&p=22&l=ur1&category=baby&banner=1ZGQKAFMJX87RESA2202&f=ifr&linkID=de177072ca057ccefdac7904b89953b1&t=madrescabread-21&tracking_id=madrescabread-21" width="250" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0" sandbox="allow-scripts allow-same-origin allow-popups allow-top-navigation-by-user-activation"></iframe>

Quiza te interese tambien conocer los [4 cochecitos para bebe mejor valorados](https://madrescabreadas.com/2021/08/19/mejores-cochecitos-para-bebes/)

y las [cunas de viaje mas ligeras del mercado](recetas-cocinar-sano/).

## ¿Qué es el sistema I-size?

El sistema I-Size es como se conoce a **la normativa de seguridad europea (ECE R129),** por la cual se regula el uso de las sillas de coche para bebés con una edad inferior a los 15 meses. 

La normativa entró en vigor en julio del 2013 y se creó con el objetivo de **mejorar la seguridad de los bebés cuando viajan en coche**, ampliando la edad mínima obligatoria para viajar de espaldas al sentido de la marcha hasta los 15 meses (entre 75-83 cm de altura aproximadamente).

Con esta idea, se simplifica a la hora de elegir una silla de bebé ya que no atiende al tallaje habitual de grupo 0,1, 2 o 3, sino que se centra exclusivamente en el peso y la estatura del bebé o niño.

**Las sillas I-size son mucho más seguras** porque habrán pasado una nueva prueba de choque lateral, más seguras a este tipo de choques que suelen ser más comunes. Además, las pruebas a las que han sido sometidas siguen las técnicas más avanzadas, y también promueven el uso de la posición contraria a la marcha, mucho más seguras al distribuir la fuerza del impacto en caso de accidente.

## ¿Cuál es la silla de coche más estrecha del mercado?

Entre todas las sillas de coche estrechas del mercado, encontramos una que cumple perfectamente con su misión de ayudarnos a ahorrar espacio en los coches de cinco plazas, cuando somos familia numerosa. Hablamos de **la silla Fold & Go I-size de Chicco.** Es la más estrecha del mercado dentro de la norma ECE R129 y de manera general en las sillas de grupo 2/3. 

![Niño en silla de coche](/images/uploads/istockphoto-498555893-612x612.jpg "Niño en silla de coche")

Esta silla cuenta con muchas ventajas, ayudándonos a **ahorrar espacio sin que la confortabilidad del niño se vea perjudicada**. Se puede plegar; algo poco usual, y esto nos proporciona un mayor ahorro de espacio. En cuanto a anchura de hombros, también es de las más estrechas del mercado, sin quitar habitabilidad a los niños y niñas que sean algo más grandes. 

Hablamos de una silla muy segura que solo puede instalarse a través del isofix. En definitiva, Fold & Go I-Size de Chico es la silla más estrecha del mercado, ligera y plegable. Además, es una silla segura, homologada por la normativa ECE R129 y que incluye el impacto lateral y la medición de valores biomecánicos como el criterio de comportamiento de la cabeza. Además, **el precio es bastante competitivo**, pudiendo encontrar una buena oferta.

![Silla de coche más estrecha del mercado](/images/uploads/fold-go-i-size-silla-de-coche-con-conectores-isofix-para-bebe-de-15-36-kg.jpg "Fold & Go I-Size Silla de Coche con Conectores Isofix para Bebé de 15-36 Kg")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/B08M6ZQXSV/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=6684320cf990df01da2d8c8f9c51c287&creativeASIN=B08M6ZQXSV) (enlace afiliado)

## ¿Cuál es la mejor silla de coche del grupo 2 - 3?

Finalmente, os vamos a dejar con una selección de las **mejores sillas de coche del grupo 2 - 3** porque elegir solo una sería muy difícil, ya que el mercado cada vez está más lleno de estas sillas estrechas para bebés.

### Silla de auto estrecha Chicco Oasys 23 FixPlus

Otra de las sillas de coche que se cuela en nuestra lista es el modelo Oasys 23 FixPlus de Chicco. Es una silla de coche reclinable que **se va adaptando al crecimiento de tu hijo**, pudiendo ser usada desde los 3 a los 12 años. También pertenece al grupo 2/3cuyo peso oscila entre los 15 y 36 kg. Tiene refuerzos laterales resistente a los golpes y de muy fácil instalación con los conectores FixPlus.

![](/images/uploads/chicco-oasys-23-fixplus.jpg "Chicco Oasys 23 FixPlus")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/B07576HJ21/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=fffe0fc1cbe40d52da296070e54c5b94&creativeASIN=B07576HJ21) (enlace afiliado)

### Silla estrecha de automovil Safety 1st Road Safe

Otra de las sillas estrechas que podemos encontrar a muy buen precio es la Safety 1st Road Safe, reclinable en dos posiciones y fácil de instalar. Es una silla ajustable en 6 posiciones y que irá adaptándose a las etapas de crecimiento de tu hijo. **Es muy ligera, pesando 2,8 kg**, y es muy fácil de instalar.

![](/images/uploads/safety-1st-road-safe-silla-de-coche-grupo.jpg "Safety 1st Road Safe Silla de Coche Grupo")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://amzn.to/3R4L8ld) (enlace afiliado)

### Silla de coche estrecha Britax Römer Discovery Plus

La última en esta lista, pero no menos importante, es el modelo Discovery SL de la marca Brita Römer. Se trata de otra **gran opción a un precio muy asequible** y que cuenta con esa característica que más buscamos en este artículo, ser una silla de coche estrecha para ahorrar espacio.

![](/images/uploads/britax-romer-silla-coche-discovery-sl.jpg "BRITAX RÖMER Silla Coche DISCOVERY SL")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://amzn.to/3r9azax) (enlace afiliado)

Como veis, os hemos ofrecido opciones para todos los gustos y adaptadas a todos los bolsillos. Si no conocías las sillas estrechas, comparte el artículo para que llegue a más gente y acabar con esa laguna general.

## 2 trucazos para hacer viajes largos con niños sin que se desesperen

\-Si te gusta viajar, y quieres salvar momentos de crisis en el coche cuando ya van desesperados, el truco esta en bajarte videos de sus dibujos favoritos e incluso audiolibros con cuentos para poder ponerselos aun cuando no tengas conexion a Internet. 

Merece la pena el almacenamiento ilimitado sin conexión que te ofrece Amazon Prime, además puedes almacenar una cantidad ilimitada de fotos, con acceso desde cualquier lugar, para que no te pete el movil a mitad de viaje y no tengas que empezar a borrar para podeer hacer mas.

Te dejo esta [prueba gratis de Amazon Prime](https://www.amazon.es/amazonprime?_encoding=UTF8&primeCampaignId=prime_assoc_ft&tag=madrescabread-21) (enlace afiliado) para que veas si te encaja antes de decidirte a suscribirte.

\
-Otro truco muy socorrido y poco conocido por la mayoria de los padres y madres son los audiolibros de cuentos.

Cuando yo erea pequeño mis padres me ponian cintas de cassete con cuentos, y recuerdo que el viaje se nos pasaba volando.

Ahora hay mucha mas variedad, y tienes una prueba gratuita de Amazon Audible con muchos cuentos gratis para disfrutar de viajes mas tranquilos. Te recomiendo ["Cuentos para quererte mejor"](https://amzn.to/3V2AElf) (enlace afiliado).

![aundiolibro cuentos](/images/uploads/61ij0lrbnts.jpg "audiolibro cuentos")

[Suscribete a mi blog aqui](https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c) para no perderte mas novedades.