---
layout: post
slug: celebrar-reyes-con-adolescentes
title: 4 formas diferentes de pasar la Noche de Reyes con tus adolescentes
date: 2022-01-03T11:00:25.017Z
image: /images/uploads/portada-reyes-con-adolescentes.jpg
author: luis
tags:
  - navidad
  - adolescencia
---
La adolescencia es una de las etapas más complicadas, sobre todo al llegar fiestas como la Navidad o la noche de Reyes Magos, más orientadas a los niños de edades más tempranas. Parece que al llegar la adolescencia, la ilusión de estos días no se vive con la misma intensidad, puesto que ya son conscientes de la realidad detrás del día 6 de enero.

No obstante, como madres y padres, nuestro deber es conseguir que la magia de estos días no se pierda, ni siquiera cuando nuestros hijos han llegado a esta etapa de su vida. Por ello, nos parece fundamental que les propongamos planes alternativos en familia que les incentive a continuar con estas fechas tan entrañables como si el tiempo no hubiera pasado para ellos. 

## ¿Qué día es la Noche de Reyes 2022?

En primer lugar, vamos a acabar con la eterna duda cuando se acercan estas fechas y es la de saber en qué día exacto cae la noche de reyes de cada año. Como debes saber ya, la noche de reyes tiene lugar en la madrugada del día 5 al 6 de enero, siendo la mañana del 6 cuando los pequeños se levanten y encuentren su hogar lleno de regalos. 

![noche de reyes magos](/images/uploads/noche-de-reyes-magos.jpg "noche de reyes magos")

Para el año 2022, los Reyes Magos tienen planeada su visita a todos los hogares de nuestro país en la madrugada del miércoles 5 de enero al jueves 6 de enero, siendo el miércoles día 5 cuando tengan lugar las diferentes cabalgatas y festejos que acompañan a esta fecha y que darán lugar a la noche de reyes 2022.

## ¿Cómo se celebra el Día de Reyes Magos?

Desde luego que hay celebraciones muy diversas y para todos los gustos, pero no se puede negar que existe una tradición en torno a la víspera del día de reyes. Sin duda alguna, el sumum de esta celebración es la tradicional cabalgata de reyes, las cuales presentan diferencias según el pueblo o ciudad donde se lleven a cabo. Los tres Reyes Magos de oriente recorren las calles de cada ciudad lanzando caramelos y, en ocasiones, algunos juguetes. Lo más normal es verles montados en carrozas porque atrás quedaron los tiempos en los que se movían a camello. Sin embargo, hay lugares en los que les reciben a su llegada en avión, barcos y todo tipo de medios de transportes.

Este día se acaba temprano, ya que los pequeños estarán nerviosos por ir a la cama y esperar al amanecer para abrir sus regalos. El día de Reyes Magos, es tradicional que se reúna toda la familia para comer después de la apertura de los regalos. Muchos optan por salir fuera y otros por hacerlo en casa, una medida mucho más recomendable en estos tiempos de COVID. Y lo que no puede faltar es la tradicional merienda con el [roscón de reyes](https://www.pequerecetas.com/receta/receta-de-roscon-de-reyes-casero-paso-a-paso/), este dulce típico que se convierte en el gran protagonista de las pastelerías al llegar la fecha.

## Ideas para celebrar la Noche de Reyes con Adolescentes

Pero, ¿qué pasa cuando tenemos niños adolescentes? La apertura de regalos tradicional sigue vigente, aunque suele carecer de esa emoción, puesto que con su edad, muchos ya sabrán qué hay detrás de esos regalos y quiénes son los verdaderos artífices de este día. Además, el plan de salir a coger caramelos con la cabalgata no es el que más apasiona a los jóvenes. Entonces, ¿qué podemos hacer? Te proponemos una serie de ideas chulas para celebrar la noche de reyes con adolescentes.

### Haz que tu hogar huela a Navidad fabricando este ambientador natural

[![naranja manzana y arandanos rojos](/images/uploads/photo_2023-01-03-12.49.54.jpeg)](https://jspc.es/madrescabreadas)

\
**Ingredientes**

\-arándanos rojos 2 puñados

\-1 manzana

\-1 naranja

\-Una cucharda de [mix carrot cake de Just Spices](https://jspc.es/madrescabreadas)

**Elaboracion**

\-Pon los arandanos, la manzana y naranja a rodajas y los polvios de mix para carrot cake de just spices en una olla o cacerola.

\-Cubre con agua en una cacerola

\-Cuece a fuego lento 20’ sin tapar y disfruta del aroma

### Sesión de Películas Temáticas

La primera idea que os ofrecemos es una buena sesión de cine en casa con toda la familia. Podemos aprovechar estas fechas para visionar películas que vayan con la temática, pudiendo encontrar una gran variedad en plataformas como Netflix, HBO, Amazon Prime, Disney+, etc.

![películas noche de reyes](/images/uploads/noche-de-reyes-pelicula.jpg "películas noche de reyes")

Si necesitas alguna recomendación, te invitamos a que veas el catálogo de estas plataformas para ver el contenido que cada año llega por Navidad y entre el que podrás encontrar desde series hasta películas navideñas. 

### Cocina creativa en familia

La cocina creativa es otro de los divertimentos familiares en los que los adolescentes más pueden contribuir. Quizás para esta fecha, lo ideal sería crear nuestro propio roscón casero, siendo una receta fácil y sencilla la que podéis encontrar en el [canal de YouTube Cocina Para Todos](https://www.youtube.com/watch?v=dkN0TEtGYDQ). Es cierto que no es de las recetas más fáciles, así que si buscas algo menos complicado, un tronco de navidad con hojaldre y nutella puede ser una gran opción.

Aqui [3 ideas de dulces de Navidad caseros sencillos y deliciosos](https://madrescabreadas.com/2017/12/10/dulces-nutella-navidad/).

![cocina creativa familia](/images/uploads/cocinar-en-familia.jpg "cocina creativa en familia")

Lo que vayáis a cocinar finalmente será lo de menos, ya que lo importante realmente será el tiempo del cocinado, el trabajo en equipo y las ganas de disfrutar de estas fechas en familia.

### Día de Juegos de Mesa

Entre mis favoritos para los días en familia también se encuentran las tardes de juego de mesa. Con los niños en edades ya avanzadas, podemos optar por grandes clásicos como el [Monopoly](https://amzn.to/3iaRkZM), [Trivial](https://amzn.to/3Qflzvg), [Party](https://amzn.to/3iezZPI) (enlace afiliado) y un sinfín de juegos pensados para disfrutar en familia. También hay juegos especiales de Padres vs Hijos, algo que se podría plantear como un regalo de Reyes para toda la familia y comenzar a disfrutarlo en este día tan especial.

### Planes al aire libre

Si lo prefieres, podéis planear una tarde al aire libre, una visita al monte, un paseo por la playa, una salida en bicicleta o cualquier otra actividad al exterior que se os ocurra. Este tipo de planes suele ser mucho más llamativos para los adolescentes, además de que aportan un toque de novedad en las típicas celebraciones que se suceden durante estos días.

Y tú, ¿cómo tienes pensado organizar la noche de reyes con tus hijos adolescentes? La noche de reyes 2022 se acerca y debemos estar preparados para que este día siga siendo inolvidable para nuestros hijos, aunque ellos se hagan cada año más mayores y piensen que la navidad es cosa de niños pequeños.

En este post te contamos mas ideas sobre [como preparar una noche de Reyes magica](https://madrescabreadas.com/2021/01/04/ideas-noche-reyes-magica/) para toda la familia.

Comparte para que podamos llegar a mas familias!

Feliz noche de Reyes