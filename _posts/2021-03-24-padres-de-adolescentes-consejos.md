---
author: maria
date: '2021-03-24T09:13:29+02:00'
image: /images/posts/lemon/p-adolescentes.jpg
layout: post
tags:
- adolescencia
- invitado
title: Escuela Lemon para madres y padres también de adolescentes
---

En este post podrás encontrar:


1. <a href="#Cómo orientar a padres de adolescentes"> Cómo orientar a padres de adolescentes</a>
2. <a href="#Cómo ayudar a los hijos en la adolescencia"> Cómo ayudar a los hijos en la adolescencia</a>
3. <a href="#Cómo ayudar a madurar a los adolescentes"> Cómo ayudar a madurar a los adolescentes</a>

Uno de los temas en los que más me estoy centrando en esta nueva etapa de mi familia es en aprender sobre la [adolescencia](https://madrescabreadas.com/2017/10/22/adolescencia-positiva/) porque creo que adolescentes y padres andamos bastante perdidos y nos resulta muy difícil hablar el mismo [lenguaje](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) afrontarla con naturalidad y sentido común, ya que todo es muy nuevo para ambas partes. Por un lado la adolescencia trae enfrentamiento con los padres, y muchas veces desconocemos el por qué del comportamiento de nuestro adolescente de 15 años, y nos angustia no saber qué hacer cuando mi hijo adolescente me falta al respeto, por ejemplo.


Hoy merendamos con Laura Monge y Sonia López y , de la [Escuela Lemon](https://escuelalemon.com/?aff=Maria+Madrescabreadas), un espacio donde las familias pueden encontrar orientación y acompañamiento en esta etapa tan convulsa de la vida de nuestros hijos, y que a veces puede llegar a afectar de forma importante a la nuestra también. Gracias a este tipo de escuelas o formaciones para padres no tenemos por qué afrontarlo solos. Tenemos suerte de poder acudir a expertos que nos pueden ayudar.

Puedes consultar sus [cursos y talleres aquí](https://escuelalemon.com/producto/curso-comunicacion-afectiva-en-la-adolescencia/?aff=Maria+Madrescabreadas); A mí me tienen enamorada sus formaciones sobre adolescencia, y creo que son imprescindibles para lo perdidos que nos encontramos los padres muchas veces.

![Laura Escuela Lemon](/images/posts/lemon/laura.jpg)

![Sonia escuela Lemon](/images/posts/lemon/sonia.jpg)

Así que sírvete un té, un chocolate o un café, que empieza nuestra merienda!


### ¿Qué es la escuela Lemon y en qué se diferencia de las además escuelas para padres?

LAURA: La escuela LEMON es un espacio de formación y referencia, en la que profesionales expertos en sus materias, están generando contenido de calidad con el que ayudar a las familias a sacar la mejor versión de ellas mismas. 

En la escuela trabajamos para que las personas con niños y niñas a su cargo puedan encontrar soluciones prácticas de forma sencilla a los retos cotidianos. Y esto es quizás lo que más nos diferencia de otras escuelas, que desde la teoría queremos conectar con las familias, para dar ideas, tips y herramientas prácticas, que puedan ayudar a la resolución real de los conflictos del día a día.

## <a name="Cómo orientar a padres de adolescentes">Cómo orientar a padres de adolescentes</a>

### ¿Por qué os interesa tanto el mundo adolescente?

LAURA: En realidad nos interesan todos los temas que puedan interesar a los padres. Sin embargo, la adolescencia es un tema muy recurrente. En mi caso y en mi casa, estoy rodeada de ellos, así que no me queda más remedio que interesarme por ellos.

Por otro lado, creo que es un periodo que hemos castigado mucho. La adolescencia es una etapa apasionante, llena de vivencias maravillosas de intensidad potente. 

Desde la escuela, queremos dar una visión mucho más positiva de esta etapa, acercarla a los padres, ayudarles a comprenderla, dándonos la oportunidad de disfrutar toda la parte positiva de estos años. 

¡Esto no significa que cerremos los ojos a los peligros! En nuestros cursos, presentamos la realidad en la que conviven nuestros adolescentes. Una realidad muchas veces cruda y escalofriante, pero no para echarnos las manos a la cabeza sino, para recordar la importancia de seguir formando y acompañando desde la calma también durante esta etapa.


##  <a name="Cómo ayudar a los hijos en la adolescencia">Cómo ayudar a los hijos en la adolescencia</a>

SONIA. La adolescencia es una etapa educativa emocionante en la que nuestros hijos necesitan nuestra mejor versión. Un adolescente necesita padres que les acompañen desde la calma, el sentido común y del humor. Desde la escuela intentamos eliminar la etiqueta negativa que la sociedad ha colgado a esta etapa y ayudar a las familias a verla como una oportunidad de seguir creciendo junto a sus hijos. 

## <a name="Cómo ayudar a madurar a los adolescentes">Cómo ayudar a madurar a los adolescentes</a>

SONIA. Formando a sus familias. Padres y madres que tienen el deseo de aprender a acompañarles desde una educación positiva. Adultos que quieren aprender a entender y respetar la metamorfosis de cambios por la que están pasando sus hijos que les hacen actuar de una determinada forma. Que quieren ser  empáticos y mantener los canales de comunicación abiertos.

![frases adolescencia](/images/posts/lemon/culpa.jpeg)

### ¿Cómo ayudáis a los padres y madres de adolescentes que se han quedado sin recursos para educarlos?

LAURA: Cuando nos quedamos sin recursos, sin ideas o herramientas para seguir adelante, creo que es importante parar para reflexionar. Eso es lo realmente difícil. Parar. Después las ideas, llegan siempre. Y si no llegan, ¡¡para eso estamos nosotras!! Para seguir ofreciendo contenido que pueda ayudar a las familias a afrontar de forma positiva y serena los retos del día a día.

SONIA: Animándoles a analizar y a ser conscientes de cómo es la relación con sus hijos y los motivos que les han llevado hasta aquí. Ayudándoles a hacer un “reset” y dándoles estrategias para mejorar esa relación a nivel personal y comunicativa.

![decálogo adolescentes](/images/posts/lemon/frases.png)


### ¿Pensáis en la adolescencia como una carga para las familias como algo positivo?

SONIA: ¿Cómo se puede llegar a pensar que la educación de las personas que más queremos es una carga? La adolescencia es una etapa como todas las anteriores donde nuestros hijos siguen necesitando nuestro apoyo incondicional, nuestro amor, respeto y confianza. Si que es verdad que no es una etapa fácil pero conocer sus características nos va a permitir entenderla y mostrarnos empáticos con nuestros hijos.

### ¿Qué aprenden los adolescentes en vuestras formaciones?

LAURA: Que sus cambios y emociones son absolutamente normales. Que comprendemos esa necesidad que tienen de reafirmarse como personas individuales que son y que estamos aquí para acompañarles y apoyarles siempre que lo necesiten.
SONIA: Que la adolescencia es una etapa emocionante pero a la vez convulsa. Les ayudamos a entender y situar todos esos cambios por los que están transitando. También a tener una actitud positiva ante el futuro. 


### ¿Qué consejo daríais a una madre cabreada con su hij@ adolescente?

SONIA: Quiérele cuando menos lo merece porque es justamente cuando más lo necesita. El amor incondicional, canales de comunicación abiertos y empatía son esenciales para nuestro acompañamiento. Que aprovechen al máximo esta etapa porque pronto volarán del nido.

LAURA: Que disfrute de esta etapa. Nadie nos prepara para esto. Es más, en realidad nos preparan para todo lo contrario, para sufrir y atormentarnos durante los años de adolescencia, pero en realidad es posible disfrutarla. 

![quien más te quiere te hará sufrir](/images/posts/lemon/reir.png)

Y si todavía me siguen escuchando, les diría que hablara de todo y dejara que surjan conversaciones maravillosas. Que se enfadara poco y que le ayude a tener un espíritu crítico. 

Por último le diría que, si lo necesita, seguimos por aquí preparando contenido de calidad que esperamos que le puedan resultar de ayuda, así como nuestros consejos para padres de adolescentes.

Muchas gracias.

Sonia López: Profesora del [curso Comunicación afectiva en la adolescencia](https://escuelalemon.com/producto/curso-comunicacion-afectiva-en-la-adolescencia/?aff=Maria+Madrescabreadas).

Laura Monge: Directora de la Escuela LEMON.


Si te ha gustado comparte para ayudar a más familias y suscríbete par no perderte lo próximo!

*Photo portada by Patrick Buck on Unsplash*