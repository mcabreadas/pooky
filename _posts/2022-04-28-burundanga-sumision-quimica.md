---
layout: post
slug: burundanga-sumision-quimica
title: Precauciones para adolescentes ante la burundanga y la sumisión química
date: 2022-04-29T08:48:37.930Z
image: /images/uploads/1-vaso-noche.jpg
author: maria
tags:
  - adolescencia
---
Hoy merendamos con Maricruz, madre de una chica adolescente, que nos ofrece un motivo menos de preocupación cuando salen con sus amigas.

He querido compartir contigo su propuesta porque esto no es un tema nuevo o una moda pasajera. O si no, dime si tu madre no te advirtió cuando eras jovencita y salías con tus amigas a un bar o discoteca: 

"no sueltes el vaso de la mano" 

"ten siempre tu vaso a la vista"

En mi ciudad hace poco hubo varias denuncias de chicas que creyeron que les habían echado alguna sustancia en su bebida en un [bar de ocio](https://www.laverdad.es/murcia/joven-denuncia-haber-20210927144449-nt.html), y eso me hizo saltar todas las alarmas.

Ahora soy yo la que advierto a mi hija, aunque todavía no sale mucho, pero creo que es un clásico en las advertencias de madre. Así que por eso me parece super buena idea la solución de las Nightcups que nos dan Maricruz y Gema, sobre todo porque podremos afrontar este peligro con nuestras hijas desde la prevención, y no desde el miedo o la indefensión.

![mayjan goma de pelo nightcup](/images/uploads/nosotras-uni.jpg)

Os dejo con Maricruz y Gema y sus Nightcups:

> “Niños pequeños, problemas pequeños; niños grandes, problemas grandes”, lo hemos oído muchas veces, pero cuando realmente lo sufrimos es cuando nuestros hijos llegan a la adolescencia.
>
> Es a partir de esta etapa y después, en su juventud, cuando aumenta su independencia y la necesidad de salir y relacionarse con sus amig@s. También hemos tenido su edad (aunque hace ya algún tiempo), así que les entendemos muy bien, pero ahora nuestro papel es otro y tenemos que desarrollar nuevas funciones para ayudarles en esta etapa:
>
> *Comunicadores*: Tenemos que conseguir (cuando despegan la nariz del móvil), [hablar con ellos](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) sobre lo que les gusta, preocupa, inquieta: amistades, estudios, modas, alcohol, drogas, sexo (esta parte es complicada, la verdad), que nos den su opinión y ofrecerles la nuestra. Sólo conociéndolos bien, podremos ayudarles.
>
> *Conocedores de su medio: E*s decir, tenemos que saber cómo es su mundo, para poder entenderles mejor. Es una realidad que, su época no es la nuestra, la moda es distinta, hay muchas redes sociales, a quién siguen, música, su forma de ocio…
>
> *Controladores desde la distancia* (Todo control les agobia), con quién salen, a qué hora vuelven, dónde navegan con el móvil, el ordenador. Hay que poner límites, y estar atent@s a todos los peligros que les rodea que son muchos, aunque nos puede asustar, es mejor enfrentarlos y darles herramientas para que se protejan ellos mismos.
>
> Es aquí donde me topé con un peligro que preocupó de manera especial: *la indefensión ante la sumisión química,* es decir, que le pudieran echar algo en su bebida sin que se diera cuenta y esto le hiciera perder el control de sus actos y dañarla irreversiblemente.
>
> Lo comenté con mi amiga Gema, y, ¡compartía la misma inquietud! así que nos pusimos manos a la obra para proporcionarles algo que les pudiera ayudar a protegerse y encontramos una ingeniosa idea: una funda que cubre todo el vaso, de nylon (se adapta a muchas medidas ya que es elástica), fácil de llevar porque se guarda en una goma de pelo, y es lavable con lo que se puede reutilizar las veces que quieras. 
>
> Incluso nos pareció buena idea por higiene y contra el COVID. Pero no se podían adquirir en España así que las importamos para nuestras hijas, y pensamos... ¿y si alguien más lo quiere? ¿Por qué no facilitarlo a todas las chicas? 
>
> Así que nos hicimos distribuidoras oficiales en España, creamos una tienda online y blog: 
>
> **[mayjan.es](http://mayjan.es/)**, en Instagram y TikTok **@mjsinmiedo** y para mayor comodidad, facilitamos, de forma fiable y segura, los pedidos, por 
>
> Whatsapp/Bizum: 615105050 Gema y 652495144 Maricruz.
>
> Porque todos los padres queremos un ocio seguro para nuestr@s adolescentes y jóvenes, y cuando salen a divertirse, despedirles sólo con “pásatelo muy bien” y olvidarnos de “ten mucho cuidado”. 
>
> ![goma de pelo tapa vasos nigtcup](/images/uploads/coleteros-1.jpg)
>
> ![tapa para vasos y goma de pelo burundanga nightcups](/images/uploads/jjj-1.jpg)