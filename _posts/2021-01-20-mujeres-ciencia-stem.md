---
author: maria
date: '2021-01-20T11:13:29+02:00'
image: /images/posts/vlog/p-mujeres-ciencia.jpg
layout: post
tags:
- vlog
- conciliacion
- mujer
title: Vlog mujeres en la ciencia, conciliación y pelis familiares
---

Esta semana hablamos con Kika Frutos de las dificultades para conciliar trabajo con niños en tiempos de Coronavirus, sugerimos vías de escape para madres saturadas recomendando series para tomarnos un respiro, libros y películas para ver en familia (hablamos de Soul, ¿la has visto?), y abrimos debate en el foro sobre el papel de la mujer en la ciencia y si la falta de referentes femeninos en ciencia y tecnología puede llegar a influir en la decisión de las chicas a la hora de elegir unos estudios u otros.
¿Qué opinas?

<iframe width="560" height="315" src="https://www.youtube.com/embed/kTMEgH6iiGI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Suscríbete y comparte!

<a href="https://foro.madrescabreadas.com" class='c-btn c-btn--active c-btn--small'>Participa en el foro</a>