---
author: ana
date: 2021-04-09 08:00:00
image: /images/uploads/p-libros-para-leer.jpg
layout: post
slug: lecturas-recomendadas-por-el-d%C3%ADa-del-libro/
tags:
- planninos
- libros
title: Lecturas recomendadas por el día del libro
---

Se acerca el día del libro, un momento propicio para promover los libros y recomendar la lectura en todas sus formas. Y con ella, la creatividad, la diversidad, la igualdad, el acceso al conocimiento y a los recursos educativos, con la participación de autores, editores, docentes, bibliotecarios, instituciones públicas y privadas, ONGs, medios de comunicación y cualquiera de las personas que tienen en el libro, bien cultural y patrimonio de la humanidad, una forma de memoria y belleza que trasciende el tiempo y las culturas.

## Cuándo se celebra el día del libro

El día 23 de abril fue elegido por la UNESCO como Día del Libro y del Derecho de Autor, para hacerlo coincidir con el fallecimiento en el año 1616 de los escritores Miguel de Cervantes, William Shakespeare y el Inca Garcilaso de la Vega.

Pero, como si se tratara de algo muy propio de los libros, hay una errata: Cervantes falleció el 22 y fue enterrado el 23; mientras que Shakespeare murió el 23 de abril, según el calendario juliano que estuvo vigente hasta el año 1582, porque por el calendario gregoriano (que nos rige desde entonces) murió el 3 de mayo.

## Lecturas recomendadas del 2021 para el día del libro

El mundo editorial es profuso y ofrece variedad de libros entre clásicos y novedades que vale la pena conocer y disfrutar para «cultivar los modales del espíritu» como diría el escritor francés Marcel Proust. Así que os regalamos nuestras sugerencias de lecturas recomendadas que abarcan libros que ya se han convertido en clásicos de la literatura y libros en tendencia.

![portada el principito con flores](/images/uploads/el-principito.jpg)

### El Principito y Cumbres Borrascosas están de cumple

La [obra](https://www.amazon.es/Principito-Antoine-Saint-Exup%C3%A9ry-Saint-Exupery/dp/8498381495/ref=mp_s_a_1_1?dchild=1&keywords=el+principito+libro&qid=1616708525&sprefix=el+principi&sr=8-1&tag=madrescabread-21) (enlace afiliado) de Antoine de Saint-Exupery, uno de los libros más leídos y traducidos, fue publicado por primera vez el 6 de abril de 1943. Leerlo es inspirador y aleccionador.

Y 200 años cumple en este 2021 la tormentosa obra de Emily Brontë. [Novela](https://www.amazon.com/-/es/Emily-Bront%C3%AB/dp/8415618891/ref=sr_1_1?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2CGHIV6WZ51LQ&dchild=1&keywords=cumbres+borrascosas+espa%C3%B1ol&qid=1616817538&s=books&sprefix=cumbres+borra%2Cstripbooks-intl-ship%2C212&sr=1-1&tag=madrescabread-21) de amor exaltado en la que todo lo cotidiano aparece como visto a través de una lupa que crispa los nervios.

### Y de Roald Dahl

No te pierdas [Cuentos en verso para niños perversos](https://www.amazon.es/Cuentos-perversos-Colecci%C3%B3n-Alfaguara-Cl%C3%A1sicos/dp/8420482935/ref=mp_s_a_1_2?&tag=madrescabread-21) (1982) (enlace afiliado), en el que los cuentos clásicos te ofrecen otras aventuras, insospechadas y sorprendentes.

### Cuatro de monstruos

![libro frankestein lomo](/images/uploads/frankestein.jpg)

Para los jóvenes, [Frankestein](https://www.amazon.com/-/es/Mary-Shelley/dp/0486282112), de Mary Shelley, una historia sobre los peligrosos límites de la ciencia, y para los más grandes, una compleja y cautivadora novela de Katherine Dunn, [Amor de monstruo](https://www.amazon.com/-/es/Katherine-Dunn/dp/8417552073) (en castellano desde 2019). Para los más pequeños, dos clásicos que se leen de un tirón y no se olvidarán jamás, [Ahora no, Bernardo](https://www.amazon.com/-/es/David-McKee/dp/8466747451) (1980) de David Mckee y [Donde viven los monstruos](https://www.amazon.com/-/es/Maurice-Sendak/dp/0064434222&tag=madrescabread-21) (1963), de Maurice Sendak, breves historias infinitas en su interior.

### Clásicos que no mueren y en cambio vuelven a nacer

Sea por maleficio, como en [El retrato de Dorian Grey](https://www.amazon.com/-/es/Oscar-Wilde/dp/8467033932&tag=madrescabread-21) (1890) de Oscar Wilde, cuyo protagonista envejecía pero no así, su modelo, algo similar ocurre, porque los libros son espejos que se confrontan en el tiempo, con Martine en [La Mujer que no envejecía](https://www.amazon.es/mujer-que-envejec%C3%ADa-sin%C3%B3nimos-literarios/dp/8417708863/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=la+mujer+que+no+envejec%C3%ADa&qid=1616816163&sr=8-1&tag=madrescabread-21) (2021) (enlace afiliado) de Grégoire Delacourt que, tras una sesión fotográfica congeló su belleza de 30 años, mientras moría por dentro. Obsesión por la belleza y la juventud que nos deben llevar a reflexionar sobre la aceptación de la vida y lo que tiene de inevitable.

Y, para cerrar... o abrir:

### Tres libros en tiempo presente

Recomendaciones para interpretar y sentir la contemporaneidad, desde el conflicto íntimo de la migración y los cambios internos y externos del paisaje, con [Los distintos](https://www.amazon.es/Los-distintos-M%C3%B3nica-Monta%C3%B1%C3%A9s/dp/8412163613/ref=mp_s_a_1_1?dchild=1&keywords=los+distintos&qid=1616721810&sr=8-1&tag=madrescabread-21) (2020) de Mónica Montañés, seleccionado por la Biblioteca Pública de Nueva York entre los 10 Libros Notables 2020. La lúcida reflexión que sobrevuela y se detiene en los pliegues e intersticios, con la magna obra de Shoshana Zuboff [La era del capitalismo de la vigilancia](https://www.amazon.com/-/es/Shoshana-Zuboff/dp/8449336937&tag=madrescabread-21) (2020). O bien esta prosa poética urbana, que habla desde dentro de la red, a partir de una fugacidad que busca permanecer: [El tiempo. Todo. Locura](https://www.amazon.com/-/es/M%C3%B3nica-Carrillo/dp/8408165208&tag=madrescabread-21) (2017) (enlace afiliado), de Mónica Carrillo.

*Photo de portada by Eliott Reyna on Unsplash*

*Photo by Casey and Delaney on Unsplash*

*Photo by Laura Chouette on Unsplash*