---
layout: post
slug: talleres-sobre-seguridad-en-internet-para-ninos
title: Talleres sobre seguridad en Internet para niños en Valencia Montessori School
date: 2023-05-31T10:05:48.327Z
image: /images/uploads/depositphotos_156590634_xl.jpg
author: .
tags:
  - 6-a-12
  - educacion
  - invitado
---
Cuando me contactaron del centro Valencia Montessori School para que imartieramos unos talleres para educar a alumnos y padres en nuevas tecnologias y valores digitales supuso un reto para mi, ya que mucho se habla sobre el metodo montessori, pero nunca habia tenido la oportunidad de conocer una escuela Montessori en primera persona.

¿Quieres que organicemos un taller en el cole de tus hijos? Escríbenos un email [info@madrescabreadas.com](mailto:info@madrescabreadas.com) y te mandamos toda la info de mil amores sin compromiso alguno.

Mi compañera [Laura Giménez de Béjar](https://mamaconhijosenlared.com) fue la encargada de impartir estos talleres y a continuacion te cuenta de primera mano su experiencia y por  qué deberian impartirse en todos los colegios.

## Talleres sobre seguridad en Internet para niños

![Laura Gimenes de Bejar coach educacion digital ninos](/images/uploads/photo_2023-05-31-12.27.26.jpeg)

> A principios del mes de mayo tuve la oportunidad de impartir unos talleres para menores y una sesión para padres y madres  en Valencia, en  el centro [Valencia Montessori School](https://valenciamontessori.org).
>
> El tema a tratar, incardinado dentro del proyecto “Educa en Valores Digitales”, no podía ser otro que el camino hacia la identidad digital positiva de nuestros hijos. 
>
> Y digo nuestros hijos porque estas sesiones irremediablemente siempre resultan ser dinámicas, ofrecen puntos de vista diferentes, nuevos, tantos como la propia experiencia personal de quienes asisten a ellas y con ellas terminamos aprendiendo todos. 
>
> Eso es lo enriquecedor. El hecho de ir desgranando el tema de la educación digital de nuestros hijos poco a poco y abriendo la mente a la corresponsabilidad, hace que lleguemos sin prejuicios al punto de que los padres y madres vayamos entendiendo cuál es nuestra función como adultos en la era tecnológica en la que nos encontramos inmersos y en qué punto fallamos o al menos hemos dejado un poco al margen de nuestra rutina diaria ese deber de supervisión del movimiento de nuestros hijos en la Red o en el simple uso de la tecnología.
>
> He de confesar que ha sido la primera vez que he impartido los talleres en un centro Montessori, y la experiencia ha sido enriquecedora. Ha sido curiosa la contraposición entre el método seguido en el colegio y el contenido de las charlas.
>
> Si bien hoy por hoy la tendencia educativa está claramente orientada a la digitalización de las aulas y los métodos de trabajo y estudio de los menores, en este tipo de centros como el Montessori Valencia School la conexión con los demás, el entorno y, en general, la vida es su pieza angular. Siguiendo los métodos y principios propios de la Asociación Montessori Internatioonale.
>
> La edad de los menores que asisten al centro es entre 1 y 12 años. Las aulas están diseñadas de tal manera que permiten que los niños se muevan libremente, potenciando el descubrimiento de habilidades por ellos mismos.
>
> Aunque los dispositivos tecnológicos no están presentes en ellas, la dirección, los educadores y todo el personal del centro son plenamente conscientes de que los niños deben tener una formación en  valores digitales como punto de partida para su incorporación a la sociedad de la información.
>
> Por eso han apostado por mis sesiones y talleres, para trabajar con niños desde los 6 años de edad en adelante y para orientar a sus papás y mamás sobre cómo enfocar la educación digital basada en la parentalidad positiva.
>
> No en vano, hay que decir que no se trata de niños “100% analógicos”. Esta desconexión digital tan solo se da en lo que concierne al aprendizaje en el centro, en términos generales. Como otros niños de su edad, tienen contacto con la tecnología (juegos online, redes sociales…) y es por ello por lo que los adultos consideran necesaria una formación específica.
>
> Personalmente considero adecuado el aprendizaje analógico, con independencia de que el centro siga el método Montessori o no. La digitalización de los medios de estudio no siempre es positiva. Soy de las que piensan que los niños tienen que leer y escribir en papel.
>
>  Entiendo que deben saber utilizar la tecnología, pero no creo que sea necesario usar medios digitales para cualquier aprendizaje. Creo firmemente que el uso de la tecnología y la inmediatez de la obtención de información hacen que nuestros niños y niñas no desarrollen la paciencia y el espíritu de esfuerzo necesario en todo aprendizaje.
>
> En otras palabras: cuanto más usan la tecnología más impacientes se vuelven. 
>
> La educación no es tarea sencilla. 
>
> Creo que es la más complicada de las tareas. 
>
> Y cuando esa tarea se centra en educar a los menores para desenvolverse en un ambiente de adultos, en el que en ocasiones los propios adultos no entienden el funcionamiento de la tecnología, la predeterminación de nuestro yo por los medios digitales, las consecuencias de una identidad digital negativa y un largo etcétera, se hace complicado abordar la tarea.
>
> Como inmigrantes digitales nos hemos visto desbordados y ahora estamos empezando a prestar atención a algo que nunca deberíamos haber pasado por alto: la seguridad de nuestros niños y niñas.
>
> Mi experiencia en Valencia Montessori School me ha demostrado una vez más que el tema de mis jornadas interesa, tanto a menores como a adultos.
>
> De los talleres con los niños y niñas me llevo la sensación gratificante de unos menores que se implican en las sesiones, con sus preguntas y respuestas, con independencia de su edad. Menores que solo necesitan que se les hable con un lenguaje claro y acorde a su madurez pero sin esconder la importancia de su movimiento por la Red. Ello hace que por mucho que planifiquemos las sesiones sea tan dinámico que demos un vuelta de tuerca en el momento según el interés de los niños y niñas.
>
> De la sesión con los padres y madres me llevo la satisfacción de mostrar a otros una forma de educar desde el respeto e intentando dejar a un lado las prohibiciones. Padres y madres que entienden de tecnología y que, sin embargo, lo que necesitan es saber cómo dirigirse a sus hijos y tratar temas tan importantes como el ciberbullying, el grooming, la pertenencia a comunidades peligrosas o incluso el uso abusivo de la tecnología, entre otros problemas, intentando evitar el conflicto y generando confianza en sus hijos.
>
> Padres y menores que sólo necesitan una hoja de ruta como la que el proyecto “Educa en valores digitales” pone en sus manos.
>
> Desde Mamá con hijos en la Red y la comunidad on line Madres Cabreadas seguiremos apostando por una educación digital de calidad como premisa para la protección de nuestro yo digital. 
>
> Laura Gimenez de Bejar

## ¿Cómo podemos garantizar la Seguridad en Internet para niños?

![](/images/uploads/depositphotos_77147091_xl.jpg)

Para garantizar la seguridad en Internet para los niños, es importante tomar medidas proactivas y educarlos sobre los riesgos y las mejores prácticas en línea. Aquí tienes algunas recomendaciones clave:

1. Supervisión y control parental: Utiliza herramientas de control parental para limitar el acceso a contenido inapropiado y establecer límites de tiempo en línea. Supervisa y guía a los niños mientras navegan por Internet.
2. Educación sobre seguridad en línea: Enséñales a los niños sobre los peligros en línea, como el ciberacoso, el contenido inapropiado y el engaño en línea. Fomenta el diálogo abierto para que se sientan cómodos compartiendo cualquier experiencia preocupante contigo.
3. Uso de contraseñas seguras: Enseña a los niños a utilizar contraseñas seguras y a no compartirlas con nadie, excepto con los padres o tutores. Anímalos a utilizar combinaciones de letras, números y símbolos, y a cambiar sus contraseñas regularmente.
4. Cuidado con la información personal: Enseña a los niños a no compartir información personal en línea, como su nombre completo, dirección, número de teléfono o detalles de la escuela. Explícales que no deben confiar en extraños en línea.
5. Privacidad en redes sociales: Enséñales sobre los riesgos de compartir información personal en las redes sociales y ayúdalos a configurar adecuadamente las opciones de privacidad para limitar quién puede ver y acceder a su perfil.
6. Sensibilización sobre el ciberacoso: Educa a los niños sobre el ciberacoso y cómo identificarlo. Anímalos a informar de inmediato cualquier incidente de acoso en línea y a bloquear a los acosadores.
7. Uso responsable de la webcam: Enseña a los niños a utilizar la webcam de manera segura. Explícales que no deben compartir imágenes o videos personales con extraños y que solo deben utilizarla con personas en las que confíen.
8. Uso adecuado de los dispositivos: Establece reglas claras sobre el uso de dispositivos electrónicos y asegúrate de que los niños entiendan la importancia de seguir estas reglas. Esto puede incluir limitar el tiempo de pantalla y establecer períodos de descanso.
9. Conciencia de los peligros del phishing y el malware: Enséñales a reconocer los correos electrónicos sospechosos, los enlaces maliciosos y las descargas no seguras. Anímalos a no abrir mensajes de remitentes desconocidos o hacer clic en enlaces sospechosos.
10. Fomentar la comunicación abierta: Mantén una línea de comunicación abierta con tus hijos, para que se sientan cómodos compartiendo cualquier inquietud o experiencia en línea. Establece un ambiente de confianza en el que sepan que siempre pueden acudir a ti en caso de problemas. Te recoiendo este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para romper el hielo (funciona).

## ¿Quieres que organicemos un taller de seguridad digital en tu cole?

Recuerda que la educación y la supervisión continua son fundamentales para garantizar la seguridad en Internet para los niños.\
¿Quieres que organicemos un taller en el cole de tus hijos? Escríbenos un email [info@madrescabreadas.com](mailto:info@madrescabreadas.com) y te mandamos toda la info de mil amores sin compromiso alguno.

Te dejo el podcast que grabamos Laura y yo con mas consejos que te ayudaran a ser la mejor guia para tus hijos en el mundo de Internet.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6cy4jMY7c1irNG4zFQVeQw?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Comparte para que en todas las familias se trabajen los valores digitales!