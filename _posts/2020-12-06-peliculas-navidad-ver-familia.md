---
author: ana
date: '2020-12-06T08:19:29+02:00'
image: /images/posts/peliculas-navidad/p-television.jpg
layout: post
tags:
- planninos
- navidad
title: Las mejores películas de Navidad para ver en familia
---

Diciembre es esa época del año cuando el deseo de compartir más tiempo con nuestros seres queridos se intensifica y el cine en casa resulta una excelente alternativa. Más aún, considerando las condiciones muy particulares que este año nos trajo en el contexto de la pandemia. Pensando en eso, preparamos una selección de las mejores películas de Navidad para ver en familia que seguro disfrutarán niños y grandes.

Os recuerdo también que ya os recomendamos estas series para ver en familia:

- [10 series para ver en familia](https://madrescabreadas.com/2019/08/22/series-familias/)

- [9 series para preadolescentes](https://madrescabreadas.com/2020/06/18/recomendaciones-series-adolescentes-dos/)

- [6 series para adolescentes](https://madrescabreadas.com/2020/06/18/recomendaciones-series-adolescentes-dos/)

Si no estás suscrito a alguna  plataforma de series on line te recomiendo y soléis comprar cosas en Amazon os interesa Amazon Prime Video porque compagina ventaja en envíos y compras con series y películas on line.

Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de contenido original y es bastante bueno. Además, es económico porque por 3,99€ al mes.

<iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=30&p=20&l=ur1&category=prime_video&banner=0B3W2DNB4B7BVAMWQG82&f=ifr&linkID=80c6e1d7c01f7a2cd3216494dda27e84&t=madrescabread-21&tracking_id=madrescabread-21" width="120" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21). Ahora tienes una prueba gratuita de 30 días.

## Películas de Navidad para ver en familia

El cine navideño para niños está marcado desde hace tres décadas por una película en particular que hemos escogido como primera, porque ha encantado a varias generaciones y su frescura no parece pasar de moda ¿Sabes cuál es?

### Solo en casa (1990)

Trata de las travesuras de un niño de ocho años que en Navidad se queda solo en casa, olvidado por la familia con quienes debió partir para vacacionar en París. Sin embargo, a pesar de la terrible confusión, disfrutará su estancia solo en Chicago, hasta que un par de ladrones intentan robar en su vivienda y a él le tocará inventar las más disparatadas estrategias para vencerlos.

El éxito de la película fue tan grande que en 1992 lanzaron Mi pobre angelito 2: Perdido en Nueva York, ambas protagonizadas por el entonces niño Macaulay Culkin. Al film también se le conoce como Mi pobre angelito.

![cartelera solo en casa](/images/posts/peliculas-navidad/solo-en-casa.jpg)

### Un cuento de navidad o Los fantasmas de Scrooge (2009)

Basada en un cuento del escritor británico Charles Dickens. Narra la vida de un señor mayor que se ha vuelto un hombre avaricioso y con muy mal genio. Incapaz de demostrar afecto ni siquiera a los familiares, la Navidad significa para él una fecha aborrecible e insoportable. Hasta que recibe una intempestiva visita de fantasmas que lo guiarán hacia navidades pasadas y en el tránsito transforman para bien su esencia humana.

![cartelera un cuento de navidad](/images/posts/peliculas-navidad/cuento-navidad.jpg)

### El expreso polar o Polar express (2004)

Esta película recrea el cuento del escritor Chris Van Allsburg que trata sobre la fantasía muy común en la infancia llegada la noche de Navidad: conocer a Santa. El protagonista realizará un viaje en tren hacia el Polo Norte y el recorrido estará cargado de todos los símbolos que alimentan el espíritu navideño. Las aventuras que en él vivirá, le hacen recobrar el entusiasmo de la magia que esta fecha encierra.

![El expreso polar](/images/posts/peliculas-navidad/polar-express.jpg)

### Pesadilla antes de Navidad (1993)

Para quienes buscan películas de navidad menos convencionales, las de Tim Burton pueden ser una buena opción. Pesadilla antes de Navidad fue relanzada en 2006 por Disney Digital 3D. En ella, el Rey de Halloween quiere convertir la Navidad en una época tenebrosa, para lograrlo secuestra a Santa Claus y ocupa su lugar. ¿Quién vencerá?

![pesadillaantes de Navidad](/images/posts/peliculas-navidad/pesadilla.jpg)

### Eduardo Manostijeras (1990)

Otra película de Tim Burton que viene ganando su espacio en las recomendaciones de películas navideñas. Una historia fantástica donde el amor juega un rol protagonista para intervenir en el destino de un niño creado artificialmente cuyas manos son tijeras. Aunque es una película recomendada para niños a partir de los siete años, nosotras la indicaríamos para adolescentes.

![película Eduardo Manostijeras](/images/posts/peliculas-navidad/eduardo-manostijeras.jpg)

Esperamos que el paseo por estas películas de Navidad para ver en familia lo disfruten y sirva para planificar cuándo y con cuáles comenzar la jornada cinéfila decembrina.

Si te ha gustado, comparte! De este modo ayudas a que sigamos escribiendo este blog.


 *Photo portada by joshua herrera on Unsplash*
 
 *Photo Santa by Rune Haugseng on Unsplash* 
 
 *Fotos de carteleras: Filmaffinity.com*