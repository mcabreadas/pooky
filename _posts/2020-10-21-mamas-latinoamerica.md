---
date: '2020-10-21T11:19:29+02:00'
image: /images/posts/mamas-latinoamerica/p-belem.jpg
layout: post
tags:
- comunidad
- maternidad
- crianza
title: Madres Cabreadas en Latinoamerica
---

La [Comunidad de Madres Cabreadas](https://madrescabreadas.com/2020/10/16/comunidad-madres/) nunca ha tenido fronteras espaciales ni temporales. Desde sus comienzos la hemos integrado madres de todas partes del mundo, siendo una parte  muy importante de nuestro grupo las madres de Latinoamérica, quienes me han acompañado y aportado muchísimo a todos los niveles; el culinario, uno de ellos, pero no sólo de comida va la cosa, sino que me han influido en la manera de criar a mis hijos con métodos o corrientes a veces desconocidas en España, y que me han servido para vivir la maternidad de forma más plena, consciente y abierta. Una apertura de miras que no la hubiera construido si no las hubiera conocido gracias a las redes sociales y a este blog. Gracias por formar acompañarme!

A lo largo de estos más de 10 años de andadura bloguera he cultivado grandes amistades allá, pero ahora quiero dar un paso más y expandir Madres Cabreadas dando voz y visibilidad a las mamás latinas, ya que siempre han sido parte imprescindible de esta comunidad. 

Por eso tengo el gusto de presentaros a Belem Martínez, mamá mejicana, que a partir de ahora va a ser colaboradora habitual de este blog, y que va a tratar de acercar un poco más estas dos tierras separadas físicamente por el Atlántico, pero tan cercanas emocionalmente: 



> Hola Madres Cabreadas, permítanme presentarme. Mi nombre es Belem, y tengo el honor de colaborar a partir de hoy en este magnífico blog. Soy mexicana, mamá de un niño de 6 años y esposa desde hace 7.
> 
> Si tuviera que elegir una palabra para describir mi viaje por la maternidad, elegiría “auto descubrimiento”. Mi hijo llegó para enseñarme a no creer todo lo que escucho. Vino duro y directo para destruir todo lo que la sociedad, mi familia y los medios me habían hecho creer de la maternidad. Y por eso le estaré siempre agradecida. 
> 
> He aprendido sobre la marcha a disfrutar y valorar los momentos buenos y a superar, sin trastabillar demasiado, los malos. Amo ser mamá, es el rol que más disfruto. Y la forma en la que prefiero conducirme a través de él, es mediante la honestidad, sin disfrazar nada para nadie. La maternidad no es un camino lineal, y cualquiera que nos quiera hacer creer lo contrario, nos miente. 
> 
> La misión que yo he elegido es la de ayudar a las mamás que lo necesiten con la herramienta más importante que tenemos: la información. Tengan por seguro que en mí encontrarán una guía a través de temas ordinarios y de otros no tan comunes, siempre con solidaridad, honestidad y coraje. 
> 
> Así que ¡adelante!, a criar juntas, que criar es un acto de valentía y el primer paso para cambiar al mundo. 
> 
> Con cariño, Belem.