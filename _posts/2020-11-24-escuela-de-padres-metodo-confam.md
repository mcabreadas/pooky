---
author: maria
date: '2020-11-24T10:19:29+02:00'
image: /images/posts/metodo-confam/p-espaldas.jpg
layout: post
tags:
- educacion
- invitado
- crianza
title: Escuela de madres y padres on line. Método Confam
---

> Hoy, en la sección de este blog "Merendando con" donde os damos voz, y en la que puedes [participar aquí](https://madrescabreadas.com/merendando-con), merendamos con tres chicas que son pura vitamina para las familias que, además, nos traen un **regalo al final del post**. Han venido para darnos ese chute de información y formación que necesitamos las madres y los padres para poder con nuestras fieras, y más en estos tiempos revueltos. Gracias a personas como ellas y proyectos como Confam las familias no estamos solas en la gran aventura de nuestra vida de criar, educar y formar a nuestros hijos. Así que prepárate un chocolate, un té o un café porque te presento a Estefanía, Irene y Kiara y su bonito proyecto.
> 
Muy buenas a todos, me voy a presentar y os voy a contar cómo hemos llegado hasta aquí. Yo me llamo Estefanía, y hablo en nombre de Método Confam… Es leer el nombre y se me ríen los ojos… Bien, os quiero contar que Método Confam está formado por tres maravillosas mujeres, tres mujeres fuertes, terapeutas ocupacionales de profesión, mujeres con ganas de hacer cosas diferentes, emprendedoras, pero sobre todo amigas.

Me encantaría poder hablaros de ellas largo y tendido, pero sabiendo todo lo que sé, nos levaría horas!! Así que voy a tratar de hacer un resumen para que las conozcáis y las améis. 

Os presento a Irene Caselles es espontánea, sensitiva como nadie y dedicada el 110% en todo lo que hace, y por supuesto una de las cosas que mejor sabe hacer es ser una grandísima terapeuta ocupacional, especializa en infancia y siempre en continua formación. 

Kiara Gros es decidida, motivadora y entusiasta, es como un chorro de aire fresco, trae energía y nos motiva a todas con sus formas y su espíritu innovador. Además también es terapeuta ocupacional especializada en infancia, y cómo no, en continua formación. 

Y yo, la que escribe, soy Estefanía Matás, dicen que soy organizada, alegre y resiliente. También soy terapeuta ocupacional especializada en infancia y sólo quiero decir que me siento orgullosa de formar parte de este super equipo que hemos creado y del que estoy segura, tendréis grandes noticias. 

![chicas jóvenes riendose](/images/posts/metodo-confam/risas.jpg)

Ahora que ya tenéis una pequeña visón de cómo somos, os quiero hablar de varias cosas; por un lado de cómo hemos llegado hasta aquí, de cómo decidimos formar Método Confam (que por cierto, Confam viene de Con Familia); por otro lado en qué consiste el proyecto que aquí te presentamos y que espero que os cautive y por supuesto, que comencéis a seguirlo ya mismito; pero primero de todo… explicaros qué es la terapia ocupacional, lo seee, se que al leerlo muchas os habréis quedado con cara de póker, así que lo primero es explicaros qué es. 

## Qué es la terapia ocupacional

La terapia ocupacional es una profesión socio-sanitaria que utiliza como  terapia las actividades de cuidado, trabajo y juego con el fin de incrementar la independencia funcional, aumentar el desarrollo y prevenir la incapacidad; puede incluir la adaptación de tareas o del entorno para alcanzar la máxima independencia y para aumentar la calidad de vida. Es decir, usamos la actividad con propósito, la ocupación, como terapia. 

Así, en nuestro caso que trabajamos con niños y niñas, utilizamos su ocupación principal que es el juego, las actividades de la vida diaria y las escolares para mejorar aquellas aéreas en las que puedan tener dificultades, ya sea físicas, cognitivas, sociales, sensoriales o emocionales.


## ¿Cómo hemos llegado hasta aquí? 

Nos remontamos hasta hace algo más de tres años; yo llegué a trabajar a una clínica infantil de integración sensorial (algo en lo que sólo los Terapeutas Ocupacionales podemos trabajar) y allí me encontré con Irene. Trabajamos juntas durante tres años compartiendo momentos, niños, risas, nervios, cafés… muchos cafés y experiencias profesionales y personales. Conectamos desde el inicio y nos dimos cuenta de que éramos muy parecidas y buscábamos cosas similares. 

Tras un año, llegó a nuestras vidas Kiara, con su aire fresco y su desparpajo, y esto ya sí que fue el mejor de los regalos, nos unimos las tres como si nos conociéramos de toda la vida; no hemos sido amigas de la infancia, pero sí de vocación, y eso, es amor para siempre. 

## Tras el confinamiento damos el salto a Confam

El tiempo nos separó una se fue a Alicante, la otra a Orihuela, yo me quedé en Murcia, y para colmo, llegó una pandemia. Pero a pesar de todo eso seguíamos unidas, y en cuanto se levantó el confinamiento, quedamos! Y madre mía lo que salió de esa quedada… imaginaos, las tres después de muchos meses sin vernos, sin trabajar y ahora estábamos aquí tranquilamente en la piscina en una tarde de verano, bajo el sol… sueños, ideas, planes de futuro y ganas de volver a estar las tres. Y como no podía ser de otra forma, Kiara, dio el salto y nos propuso hacer algo juntas.

Durante nuestros años de experiencia clínica hemos compartido muchos momentos con mamás y papás de los niños a los que tratábamos; veíamos sus anhelos e incertidumbres, sus preocupaciones y deseos, su necesidad de saber más y querer hacer más por sus hijos. Esto unido a las dificultades que trajo consigo la pandemia sobre todo para aquellos niños que recibían terapia y ya no podían, a las madres que de golpe se encontraban 24/7 con sus hijos encerrados en casa, a los conflictos que surgen de una estrecha convivencia y a la incertidumbre que ahora se nos presenta, hizo que diéramos el salto de crear **un proyecto por y para las familias y que fuera prácticamente todo on-line**.

## ¿Qué es el método Confam?

Y ahora viene cuando te explico ya en qué consiste nuestro precioso proyecto. Tras esta primera e informal quedada, hemos estado dedicando gran parte de nuestro tiempo y esfuerzo durante estos últimos 4 meses a crear con mucho cariño este proyecto basado en una escuela de madres y padres on-line. Reuniones cada viernes a las 8 de la mañana (telemáticamente por supuesto), miles de whatsapp, emails, telegram y llamadas, han dado como resultado este mes de noviembre el Método Confam.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5GZh4lQPSBY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## La familia como eje principal de los niños

Siempre hemos pensado que las familias son el eje principal de los niños, que trabajar en entornos naturales es la base de un buen desarrollo, pero es verdad que a veces las madres o padres no saben, ni tienen por qué saber de todo, y para eso estamos nosotras, que nos hemos formado especialmente para poder acompañarte y guiarte en ese proceso de crianza y desarrollo integral de tu hija o hijo. 

## Formación on line para apoyar a las familias

Hemos credo una web ([www.metodoconfam.com](https://metodoconfam.com)) con toda la información, y ¿¿qué puedes encontrar en ella??
1. Cursos de formación sobre diversas temáticas, orientados a padres y madres, desde un lenguaje sencillo y sobre todo práctico. (Rol de la familia, autismo, integración sensorial, alimentación, rutinas….y muchos más).
2. Al acabar tu curso, recibirás un asesoramiento incluido para resolver dudas y asentar los conocimientos aplicándolos a tu realidad familiar. Este asesoramiento lo podrás solicitar con nosotras o con tu terapeuta ocupacional de referencia, si ya lo tienes. Si eliges esta opción, avísanos y así daremos también acceso a tu terapeuta para que realice la formación y pueda luego asesoraros sobre los contenidos.
3. Seguimientos individualizados on-line. Partiendo de que nos basamos en el modelo centrado en la familia (MCF), donde las familias son las protagonistas; y añadiendo la contingencia en la que nos encontramos, apostamos por un seguimiento semanal y online para ir cada semana actualizándonos y cumpliendo objetivos. Una de nosotras llevará vuestro caso y tratará de ayudaros a hacer la convivencia más fácil.
4. Visitas en entorno natural. Seguimos realizando un seguimiento semanal y basado en el MCF, pero en este caso presencialmente desde vuestro domicilio. Realizamos modificaciones de ambiente y recomendaciones sensoriales según proceda.

![chicas jóvenes haciendo un trabajo](/images/posts/metodo-confam/trabajo.jpg)

## Regalo de un curso gratuito

Pues esto es Método Confam, si nos buscas encontrarás un curso gratuito sobre el rol de la familia, para que conozcas cuál es nuestra forma de trabajar, y además en redes sociales vamos publicando todas las novedades. 

> [Curso gratuito El rol de la familia](https://metodoconfam.com/curso/el-rol-de-la-familia/)

y además, para las mamás o familias que hagan ese curso, ofrecemos un 15% de descuento en otro de nuestros cursos: Integración sensorial

> El cupón descuento es: formacion15 

Como ya dije al inicio, no se puede tener un mejor equipo. Trabajamos por vosotras para ayudaros a fortalecer el mejor equipo que tenéis, vuestra familia. 
 
Web: [www.metodoconfam.com](https://metodoconfam.com)

Instagram: [@metodoconfam](https://www.instagram.com/metodoconfam/)

[Facebook: método confam](https://www.facebook.com/metodoconfam)



Si quieres que publiquemos en el blog tu historia o tu proyecto pincha [aquí](https://madrescabreadas.com/merendando-con).


Si te ha gustado comparte! Ayudarás a Estefaía, Irene y Kiara a arrancar su proyecto y también a que podamos seguir escribiendo este blog.