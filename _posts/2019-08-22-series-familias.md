---
layout: post
slug: series-familias
title: Diez series para ver en familia
date: 2019-08-22 16:00:29
image: /images/posts/series-familia/p-netflix.jpg
author: .
tags:
  - 6-a-12
  - series
  - planninos
---
Está siendo duro, lo reconozco. Muchas horas en casa todos juntos este verano me ha hecho replantearme el tema de la televisión y buscar en ella una aliada para la paz familiar con series que podamos ver todos juntos que más o menos sean del gusto de todos para evitar batallas campales por el mando. 

Todo ocurrió cuando encontré una misteriosa nota sobre mi cama que rezaba: "[guía comprar televisor](https://www.panasonic.com/es/consumer/televisores-aprender/televisores/consejos-para-comprar-el-televisor-adecuado.html) para el dormitorio de los chicos" con una serie de sugerencias de teles perfectamente compatibles en tamaño y prestaciones con los deseos de mis hijos para su habitación (qué morro!). 

Después de reírme un rato por la ocurrencia reconozco que me planteé en serio comprarles una tele para ellos, pero luego pensé que sería mejor llegar a un entendimiento familiar, y  compartir la tele del salón para no acabar aislados cada uno en nuestro cuarto. 

Y es que la forma de ver televisión ha cambiado tanto que ahora se puede elegir entre múltiples opciones, cosa que en nuestra época no sucedía, y eso es bueno! Por eso decidí aprovecharlo y hacer, como ya hice en su día con nuestras [series favoritas para pre adolescentes](https://madrescabreadas.com/2019/01/13/recomendaciones-series-netflix-adolescentes/) y [recomendaciones series para adolescentes](https://madrescabreadas.com/2019/01/13/recomendaciones-series-netflix-adolescentes/), una recopilación de series  y programas que nos han enganchado este verano (a toda la familia), y que seguramente también gusten a la tuya. 

Si todava no estás suscrito a alguna de las plataformas de series tipo Netflix, HBO, Amazon... , no podrás resistirte por mucho tiempo, así que te recomiendo varias opciones basadas en mi experiencia:

\-Si soléis comprar cosas en Amazon os interesa Amazon Prime Video, porque **compagina ventaja en envíos y compras con series y películas Online**.

Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de **contenido original** y es bastante bueno. Además, es económico porque por 3,99€ al mes tienes derecho a:

* Envío 1 día GRATIS en dos millones de productos enviados por Amazon.
* Envío gratis con entrega garantizada en el mismo día del lanzamiento para miles de productos en preventa de cine, series TV y videojuegos entre otros.
* **Twitch Prime:** Contenido adicional para videojuegos todos los meses. Descuentos exclusivos en la suscripción Prime con la reserva de videojuegos y mucho más.
* Acceso Prioritario a las Ofertas flash de Amazon.es, 30 minutos antes de su inicio.
* **Almacenamiento de fotos gratis e ilimitado** en la plataforma Amazon Drive con Prime Fotos.
* Descuentos en una selección de pañales con Amazon Familia.

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21)

\-Oferta para universitarios. Si tienes universitarios en casa, te interesa Amazon Prime Student. ¡Ahora hay una prueba de 90 días! Con esta opción tienes **todas las ventajas de Amazon Prime a mitad de precio**; solo EUR 18,00/año. Eso sí, esta oferta es sólo para estudiantes universitarios.

Puedes [darte de alta en Amazon Prime Student aquí](http://www.amazon.es/joinstudent?tag=ID_de_madrescabread-21)

Una vez que tenemos claro que tener acceso a contenido de calidad, variado y bien categorizado puede salvar muchos momentos en casa con nuestros hijos adolescentes, **vamos recomendaros nuestras series favoritas** mas alla de los dibujos, y tambien algun programa molon para que podais disfrutar toda la familia.

Vamos a ello!

## El secreto de Nick

Los Thompson arruinaron la vida de Nick y su padre porque destruyeron su restaurante familiar, y metieron al padre a la cárcel. La chica acabó en una familia de acogida que le enseñó a robar y delinquir, lo que le permitió perpetrar su venganza y colarse en la casa de los Thompson falsificando documentación como si fuera huérfano y ellos fueran los familiares más cercanos con obligación de acogerla. 
Tras hacer muchas fechorías descubre que la madre de los Thompson va a abrir un local donde estaba el que les destruyeron, se enfada y les causa muchos problemas hasta que impide que lo abran.
Pero al mismo tiempo les va cogiendo cariño y se debate todo el rato entre el bien y el mal.
![El secreto de Nick](/images/posts/series-familia/nick.jpg)

## Precaución, piezas sueltas

Todo empieza cuando a Jarvis, un chico de 16 años le regalan un juguete por Navidad que al agitarlo explota y vuela por los aires su casa. Cuando va a reclamar a la empresa juguetera ésta pasa a ser suya , se convierte en director y contrata a sus amigos, cada cual más variopinto, como empleados.
Pero la antigua dueña se disfraza de señora de la limpieza para intentar hacerles la vida imposible y recuperar su empresa mientras los chicos diseñan todo tipo de juguetes inverosímiles para intentar que sea rentable.
Es una serie divertida y entretenida.
![Precaución, piezas sueltas](/images/posts/series-familia/piezas-sueltas.jpg)

## Jessie

Jessie es una actriz de Texas que se fue a New York para triunfar, pero de momento trabaja de niñera en una familia rica con una hija natural, y 3 adoptados de diversas nacionalidades que tienen un lagarto gigante llamado Sr. Kipplin (que luego pasó a llamarse Sra. Kiplin porque descubrieron que era hembra porque se enamoró de un lagarto de juguete del parque).
Es una serie divertida y desenfadada.
![Jessie](/images/posts/series-familia/jessie-buena.jpg)

## Alexa y Katie

Una serie donde aprender que la amistad es lo más importante y descubre muchas formas de demostrarla.
Alexa tiene cáncer y su mejor amiga Katie la apoya en todo incluso se rapa la cabeza cuando empieza a perder el pelo.
![Alexa y Katie](/images/posts/series-familia/alexa-katie.jpg)

## Los vigilantes de Malibú

Un grupo de chicos y chicas se convierten inesperadamente en vigilantes juveniles de la costa de Malibú. Se les asigna la torre 2, pero no caen en gracia a su supervisor directo, el capitán de la torre 1, que se convertirá en su mayor rival asignándoles las peores tareas y tratando de hacerles la vida imposible poniendo continuamente endeuda la valía de la capitana de la torre 2 y cuestionando su equipo.
Los chicos descubrirán que la mejor forma de afrontar esto es trabajando en equipo, fomentando los puntos fuertes de cada uno y confiando en ellos mismos.
![Los vigilantes de Malibú](/images/posts/series-familia/malibu.jpg)

## Pequeñas grandes mentes

Se trata de un programa científico muy bien planteado y enfocado para despertar la curiosidad y captar la atención de los jóvenes más curiosos. Protagonizado por chicos y chicas de esa misma edad, suelen hablar de gérmenes, emociones y redes sociales planteando una hipótesis o pregunta para que los espectadores se cuestionen algo, dejando un tiempo para la reflexión. Después dan el resultado y la explicación científica correspondiente de forma cercana. Otras veces hacen experimentos muy interesantes.

Uno de los capítulos que más me gustaron fue el de las redes sociales, y cómo relativizarlas dando más importancia a la vida real. Muy recomendable!!
![Pequeñas grandes mentes](/images/posts/series-familia/mentes.jpg)

## Carmen San Diego

Aunque es de dibujos es muy interesante para ver en familia por su contenido educativo, sobre todo a nivel de geografía, pero no por eso deja de ser divertida.
Carmen Sandiego de pequeña fue a una escuela de ladrones, pero la llamaban la oveja negra. Acabó escapándose y pasándose al otro bando ayudando a un niño a evitar los robos de otros ladrones y a devolver lo robado.
![Carmen Sandiego](/images/posts/series-familia/sandiego.jpg)

## Malcolm in the middle

Una de las series clásicas con las que más me he reído porque refleja a la perfección el estereotipo de madre de familia numerosa saturada y superviviente, con un marido que es pura pachorra y 5 hijos varones, a cuál más "pieza de museo". 
Desde el prisma del humor, refleja la que podría ser la realidad de cualquier familia, pero llevada muchas veces al extremo.
Advierto de que es antigua, pero ya veréis como vuestros hijos se parten de risa y rápidamente empatizan con los personajes. Y vosotras también!
![Malcolm in the middle](/images/posts/series-familia/malcolm.jpg)

## Zumbo´s just desserts

Este tipo de [programas americanos](https://en.wikipedia.org/wiki/Zumbo%27s_Just_Desserts) suelen entretener bastante y son ideales para cuando os juntáis en casa públicos de diversas edades y gustos. Si os cuesta poneros de acuerdo seguramente os enganche ya que está muy bien pensado para crear suspense y estar deseando conocer el ganador.
Se trata de varios equipos de reposteros, unos con más experiencia y formación que otros: desde amas de casa hasta profesionales, que deben superar varias pruebas, una de ellas imitar un super postre creado por un maestro repostero, a primera vista imposible de reproducir. Algunos postres son tan brutales que nunca pensarías que alguien pudiera hacerlo, y menos aún en un tiempo tasado...pero...sí! Aunque es el jurado quien decidirá al final.
![Zumbo´s just desserts](/images/posts/series-familia/zumbos.jpg)

## Ultimate Beastmaster

Si os gustaba "Humor Amarillo" éste formato os va a encantar pero no porque sea un programa de risa como aquél, sino porque participan verdaderos deportistas de élite que tienen que recorrer unos circuitos imposibles, donde la competencia es feroz por la calidad de los concursantes, y porque en una milésima de segundo pueden caer al agua y quedar descalificados.
![Ultimate Beastmaster](/images/posts/series-familia/beastmaster.jpg)

Como veis, todo es cuestión de buscar las series y programas que más se adapten al gusto de todos, y si alguien protesta, les se le puede decir que proponga algo para el próximo día, y se va variando para que todos queden conformes. Eso sí, hay que ceder cada uno un poquito. 

Qué serie me recomiendas de las que no he nombrado?

*Foto de portada by freestocks.org on Unsplash*