---
author: maria
date: '2021-01-07T11:19:29+02:00'
image: /images/posts/biblia-ecommerce/p-mujer.jpg
layout: post
tags:
- conciliacion
- invitado
title: Madres emprendedoras, es el mejor momento para vender on line
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puedes inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy merendamos con José Luis Torres Revert, autor del recién publicado libro [La Biblia del e-commerce](https://labibliadelecommerce.es), y viene a orientar a todas aquellas que estéis pensando en montar un negocio por vuestra cuento a aquellas que ya lo tengáis y queráis dar el salto a la venta on line.
> Además, al fina del post encontraréis una sorpresa!
> 
> ![José Luis Torres Revert](/images/posts/biblia-ecommerce/jose-luis-torres-revert.jpg)
> 
>  Tras su paso por el Ministerio de Educación en el CNICE (actualmente INTEF) trabajando con nuevas tecnologías aplicadas a la educación, desarrolló su carrera profesional durante más de quince años en el área del análisis, diseño y desarrollo de proyectos en internet, especializándose en e-learning, e-commerce, marketing electrónico, posicionamiento web y redes sociales.
> En  el  ámbito  de  la  formación  y  consultoría,  ha trabajado en numerosos proyectos en internet y metodologías enfocadas a la aplicación estratégica delas TIC en las pymes, además de participar como ponente en cursos y jornadas de temática relacionada.
> 
> Este es el sexto libro que publica relacionado con negocios e internet.
> 
> Lo podéis seguir en Twitter: [@jotaeletorres](https://twitter.com/jotaeletorres) y en su 
> blog [www.labibliadelecommerce.es](https://labibliadelecommerce.es) 
> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotros a charlar. Os presento a José Luis Torres:

## Es el mejor momento para vender online

¿Has pensado en vender en internet? ¿Tienes una idea de negocio, un producto o un servicio que comercializar online? ¿Quieres poner en marcha un canal digital complementario a tu negocio físico?

En todos los casos, necesitas una base de conocimientos para hacerlo en óptimas condiciones, evitando cometer errores de principiante que lastrarán o ralentizarán la obtención de resultados, provocando el final de tu aventura online en unos pocos meses.

Además es necesario tener una visión global de todo lo que implica tener un comercio online o e-commerce. Tener esta perspectiva te ayudará a no perder el foco, detectar oportunidades de mejora en cualquiera de los procesos implicados y tomar las mejores decisiones en cada momento, ahorrando tiempo y dinero, así como aprovechando al máximo los recursos de los que dispongas.

## Una buena idea no basta para vender on line 

Pero todo empieza mucho antes.En tu cabeza. En la idea que tienes de negocio o línea complementaria al principal. Tienes una idea estupenda y crees que eso bastará. Muchas personas tienen que esperar meses para comprobar que no era tan buena idea ya que muy poca gente ha realizado un pedido en esa maravillosa tienda online, o tal vez sí lo era pero no ha sabido utilizar todas las posibilidades que ofrece el medio para dar a conocer un producto o un servicio a los potenciales compradores.

![como vender on line](/images/posts/biblia-ecommerce/libro-teclado.jpg)

## Analiza antes de tirarte a la piscina de vender on line

En el primer caso, podríamos haberlo sabido con antelación al analizar la demanda existente de ese producto, hay herramientas y métodos para valorar razonablemente la viabilidad de una idea de negocio, producto o servicio, así como la competencia existente y qué podemos usar para lograr una diferenciación que no sólo atraiga la codiciada atención de los usuarios sino que seamos además su elección de compra.

## Conoce a tu cliente y empatiza con él

En el segundo caso, donde no pudimos llegar a conectar nuestra oferta con los potenciales clientes -que efectivamente sí existían- necesitamos conocer previamente cómo es ese cliente potencial para hablarle en su lenguaje, de las cosas que sabemos que le gustan, en el momento y lugar adecuados (por ejemplo en su red social más utilizada, sea Twitter o Instagram).

## Ofrece una buena experiencia de compra
 
Suponiendo que tenemos las dos situaciones resueltas, que hay demanda suficiente y hemos atraído a la tienda a un comprador, tenemos que ofrecer un diseño y proporcionar una experiencia de compra que sea satisfactoria, tanto que no sólo repita la experiencia (un cliente fidelizado es más rentable que captar uno nuevo) sino que publique una crítica positiva y nos recomiende a sus contactos (sí, en esas redes sociales en las que participa activamente), pero para este final feliz tenemos que haber ofrecido, entre otras cosas, métodos de pago sencillos, rápidos y seguros; así como haber cumplido los plazos de entrega indicados. Y para eso debemos conocer qué proveedores, plataformas o sistemas existen, sus ventajas e inconvenientes y cuáles se adaptan mejor a las características de nuestra tienda online.

## No hace falta ser Amazon para vender on line

El objetivo final no es convertirse en el próximo Amazon (no hace falta, puedes vender en Amazon usándolo como marketplace, esto es, publicando tus productos allí), ni siquiera tienes que competir con él (no puedes, ni tú ni nadie, busca a  alguien de tu tamaño), se trata de conocer todos los factores que contribuyen a hacer un negocio online rentable y capaz de mantenerse en el tiempo, para lo que es imprescindible tener datos e información que te permitan diseñar tu propuesta de valor y tu rasgo diferenciador, en otras palabras, por qué tienen que comprar en tu tienda online y no en la que tienen en la otra pestaña del navegador, así como ser capaz de comunicarlo a tus potenciales clientes eficazmente, esto implica conseguir hacerse oír o ver entre la competencia. 

## El libro que te lleva de la mano para empezar a vender on line

Todos estos aspectos, junto con algunos más como modelos de negocio, tipos de tienda o legislación que debemos tener en cuenta,  están desarrollados en mi último libro La biblia del e-commerce. En él encontrarás todo lo que necesitas saber para poner en marcha tu proyecto de venta en internet, además de ideas para llevar a la práctica desde el primer momento, técnicas efectivas que usan las mejores tiendas online y enlaces a herramientas y recursos que te ayudarán en la gestión y optimización de tu e-commerce.

Aprovecha la oportunidad, es el mejor momento para vender online.

![guía para vender por internet](/images/posts/biblia-ecommerce/portada-libro.jpg)


José Luis Torres

@jotaeletorres

## Sorteo del libro La biblia del e-commerce

José Luis Torres ha querido ayudar a las madres emprendedoras de esta comunidad sorteando 3 ejemplares del libro La biblia del e-commers para animar a aquellas de vosotras que os interese dar el salto a vender online y necesitéis una guía para dar los primeros pasos firmemente.

### Requisitos para participar en el sorteo:

-Suscríbete al blog [aquí](https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c)

-Deja un comentario en este post diciendo qué te gustaría vender online.

-Puedes participar también a través de mi Instagram para tener más posibilidades de ganar: [@madrescabreadas](https://www.instagram.com/madrescabreadas/)

El ámbito del sorteo es toda España, y el plazo para participar termina el 14 de enero.

El resultado se publicará en este mismos post en los días sucesivos.

Participa!

*Photo portada by Jan Baborák on Unsplash*