---
author: maria
date: '2020-12-02T16:19:29+02:00'
image: /images/posts/ocean-recycled/p-logo.jpg
layout: post
tags:
- invitado
title: Reciclaje, artesanía, amor y océano
---

Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puedes inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.

 [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)

Hoy os traigo para merendar a Marc y Bea, una pareja inspiradora que sueña con un océano limpio, sin plásticos ni residuos de ningún tipo, donde la naturaleza siguiera su curso sin verse mermada por la contaminación. Pero ellos no sólo sueñan, sino que un día decidieron poner su granito de arena para que los océanos vuelvan a ser los lugares limpios que fueron en su día y pusieron en marcha un proyecto que mezcla reciclaje, arte y amor por el mar, pero que necesita de tu ayuda si eres una persona comprometida con el medio ambiente como ellos. 

![pareja junto al mar](/images/posts/ocean-recycled/marc-bea.jpg)

Es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotros a charlar. Os presento a Marc y Bea, y a su proyecto [Ocean Recycled](https://www.facebook.com/oceanrecycled):

## ¿Quiénes somos?

Siempre es difícil definirse a uno mismo y ponerse una etiqueta. Así que nos presentaremos y dejaremos que la etiqueta nos la pongáis vosotros.

Somos Marc y Berta, un catalán y una segoviana unidos por la pasión por el mar. Aunque con hobbies e inquietudes muy distintas, fue el surf lo que nos unió.

Somos de esos hippies que pasan su vida viajando y viviendo en una furgoneta, nuestra "bala gris". Con ella hemos recorrido las costas de España y en ella vivimos de la manera más sencilla y humilde posible allá donde estemos.
Pero por azares de la vida, ahora el destino nos ha colocado en la Isla de Fuerteventura.

![movil de objetos reciclados](/images/posts/ocean-recycled/colgante3.jpg)

## ¿Por qué nace Ocean Recycled?

[Ocean Recycled](https://www.facebook.com/oceanrecycled) es un proyecto que nace en una situación de pandemia en la que no hay apenas salidas. Una situación en la que la imaginación y la creatividad son las mejores y únicas herramientas para buscarse la vida. Y con nuestras manos, lo poco que tenemos y nuestras ideas, hemos creado este proyecto.

A lo largo de nuestros viajes y experiencias por costas de España, surfeando y viajando, si algo hemos podido observar es la masificación de las playas, y toda la degradación medioambiental que esta conlleva.

Pero lo que más nos preocupa como amantes del mar, es la acumulación de residuos que hemos presenciado; plástico y más plástico cubriendo las dunas y los fondos del mar. Y este verano, especialmente, la cantidad brutal de mascarillas que hemos podido ver tiradas.
El mar es nuestro medio y nuestra vida, y no podemos evitar sentir una enorme tristeza cuando vemos el plástico a nuestro pies y la enorme falta de concienciación. Ante la inconformidad y el deseo por mejorar un poquito nuestras playas, nace Ocean Recycled.

## ¿Qué es Ocean Recycled?

Ocean Recycled es amor por el océano, concienciación y reciclaje.

> Ocean Recycled es 100% RECICLAJE, 100% ARTESANÍA, 100% AMOR Y OCÉANO.

![movil de objetos reciclados](/images/posts/ocean-recycled/colgante1.jpg)

Ocean Recycled es nuestro granito de arena dar una segunda vida a los residuos. Es apostar por unas playas limpias y por una concienciación social. Queremos mimar los lugares que más amamos de la manera que podemos y sabemos.
Aunque el proyecto aún se está incubando, queremos crear productos para todos los ámbitos de nuestra vida, hechos con los residuos orgánicos e inorgánicos que encontramos en las playas.

Por ahora, la sencillez y simplicidad es el hilo conductor de nuestros productos, pues están hechos de la manera más humilde posible y con lo poco que tenemos. Aún así, esperamos poder sofisticar y pulir poco a poco nuestro trabajo.

![movil de objetos reciclados](/images/posts/ocean-recycled/colgante2.jpg)

Para este trabajo de reciclaje ya hemos comenzado a realizar recogidas en la isla de Fuerteventura. Esperamos poder contar con la participación de los locales pero también extranjeros que aquí se encuentran. Queremos hacer llegar el mensaje de auxilio que el mar nos pide a voces. Y así, convertirnos juntos en embajadores de las mareas.

## ¿Cómo puedo ayudar a la causa de Ocean Recycled?

A día de hoy, todo se mueve por redes sociales. Es la manera más visual y efectiva de llamar la atención de las personas, especialmente del sector joven de la población.

Por ahora, solo nos gustaría que nuestro proyecto llegase al mayor número de personas para crear una comunidad de cambio (presencial en la isla de Fuerteventura, y virtual para todos aquellos que no están aquí).

Por lo que toda difusión, participación en nuestras recogidas de residuos por las playas o adquisición de nuestro arte, será una manera increíble de ayudarnos.

> Reciclar no es una obligación, pero sí nuestra responsabilidad.

Siempre se ha dicho que la unión hace la fuerza, y nosotros queremos con esto plantar una semilla que haga brotar una idea bonita.

Puedes seguirnos en:

Instagram [@oceanrecycled](https://www.instagram.com/oceanrecycled/)

Facebook [@oceanrecycled](https://www.facebook.com/oceanrecycled)

Si te ha gustado lo que has leído, estaremos felices de acogerte en nuestra pequeña gran familia. Si no quieres unirte, también puedes ayudarnos compartiendo nuestra idea con más gente.

Comparte!

¡Os esperamos!