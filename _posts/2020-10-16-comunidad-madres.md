---
author: maria
date: '2020-10-16T12:19:29+02:00'
image: /images/posts/comunidad-madres/madres-escuchando.jpeg
layout: post
tags:
- comunidad
- maternidad
- mujer
- merendando con
- invitado
- crianza
title: Nueva seccion que da voz a las madres
---

Abrimos **nueva sección en el blog** porque esta comunidad de madres tan bonita que hemos creado entre todas crece y se enriquece cada día con vuestras aportaciones, sobre todo en redes sociales, y siento que sois parte de este blog tanto las que me lleváis acompañando desde sus comienzos, como las que lo habéis descubierto recientemente.

Somos muchas y muy diferentes las madres que compartimos experiencias entorno a "Madres Cabreadas": mujeres embarazadas, madres recientes, madres primerizas, mamás de hijos en la segunda infancia, mamás de adolescentes, madres de familia numerosa, madres de niños con necesidades especiales... Madres emprendedoras, trabajadoras por cuenta ajena, madres a tiempo completo dedicadas al hogar... incluso algunos padres valientes y tíos y tías que adoran a sus sobrinos!

Aquí cabemos todas y todos porque hemos aprendido a centrarnos en lo que nos une, que es mucho más que lo que nos diferencia.

![mujeres en el parque haciendo juegos](/images/posts/comunidad-madres/madres-parque.jpeg)

## Queremos escucharte

Pienso que cada una tenemos algo importante y valioso que aportar a las demás, así que he visto la necesidad de daros voz en este blog para que se os oiga y podáis así ayudar a otras familias, contar vuestra experiencia para servir de inspiración a otras mujeres, vuestro proyecto, vuestra historia de superación personal o contar al mundo vuestra causa.

## Participa y elige el nombre de esta sección

Esta sección todavía no tiene nombre porque se lo vais a poner vosotras!

Así que espero tus comentarios por aquí o por redes sociales con las sugerencias de nombre para esta nueva sección del blog en la que tú vas a ser protagonista, y también tu mensaje proponiéndome el tema con el que quieres participar. 

> Quieres participar en el blog? Escríbeme a <a href="mailto:info@madrescabreadas.com"> **info@madrescabreadas.com**</a>