---
layout: post
slug: proteccion-solar-ninos
title: Protección solar para toda la familia
date: 2021-07-28T11:04:14.147Z
image: /images/uploads/feri-tasos-6cazudsied0-unsplash.jpg
author: .
tags:
  - salud
---
Los potentes rayos de sol y las altas temperaturas, además de indicarnos que el verano ha llegado, también nos lanzan una clara señal de advertencia a la que no siempre prestamos la atención que merece.

Está claro que las ganas de salir a disfrutar al aire libre son muy tentadoras, pero es esencial que lo hagamos con precaución. La mejor arma con la que contamos para ello son los protectores solares, un producto del cuidado de la piel para toda la familia, ya que tanto niños como adultos pueden sufrir las consecuencias de no protegerse adecuadamente.

## Los graves peligros de no usar protección solar

Uno de los resultados más visibles de dejar la piel sin protección frente a los rayos del sol son las quemaduras solares, ese enrojecimiento de la piel que, además de ser molesto, puede acarrear otras consecuencias mucho más graves.

La más temida de ellas sin duda es el cáncer de piel, una enfermedad que, según diversos estudios, estaría provocada en un 99% justamente por la exposición solar sostenida a lo largo de los años.

De hecho los melanomas, uno de los tumores cutáneos malignos más dañinos, son los responsables mayoritarios, entre un 75% y un 95%, de las muertes por cáncer de piel.

Pero, además, exponer la piel al sol continuamente sin utilizar un protector solar, nos lleva irremediablemente al envejecimiento prematuro de las células, haciendo que aparezcan manchas, arrugas e incluso alergias.

Y tampoco podemos olvidarnos de la queratosis actínica, una lesión de la piel que se conoce también como precáncer y que se manifiesta en forma de mancha áspera y con tendencia a la descamación que suele presentarse en la cara, antebrazos, dorso de la mano y otras partes del cuerpo donde ha habido una intensa y prolongada exposición a los rayos UV.

## Especial atención con los niños

Pese a que todos debemos tener cuidado con los rayos UVA y UVB, es imprescindible poner el foco en los niños ya que su piel se diferencia de la de los adultos en distintos puntos:

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Es mucho más fina, teniendo hasta un 40% menos de grosor, lo que lleva directamente a que pueda sufrir mayor deshidratación e irritaciones cutáneas.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->La melanina, el pigmento que genera nuestro cuerpo de forma natural para protegernos del sol, todavía está formándose por lo que se activa de forma mucho más lenta, no pudiendo absorber el sol por completo.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Sus defensas y su sistema inmunitario no terminan de desarrollarse hasta los 2 años de edad, por lo que los más pequeños son muy vulnerables a cualquier factor externo como la exposición al sol.

Teniendo en cuenta estas características resulta evidente que los efectos nocivos del sol son mucho más peligrosos en los niños, a los que también podemos unir otras enfermedades de la piel propias de esta etapa como es la piel sensible o atópica, que provoca irritación y descamación a consecuencia de un cutis seco y con falta de agua y grasa suficientes y que requiere mayor cuidado todavía con la exposición solar.

![](/images/uploads/jelleke-vanooteghem-rpikyfeevkw-unsplash.jpg)

## Consejos imprescindibles para proteger la piel

Con estos *tips* conseguiremos que toda la familia pueda disfrutar de esta época del año con total seguridad:

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Utilizar una crema con protección solar superior a SPF15, aplicándola por todo el cuerpo sin dejar ninguna parte sin cubrir y repetir esta operación cada dos horas o cada vez después de bañarse o sudar.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Beber agua o zumos cuando sea necesario para mantener una correcta hidratación.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Proteger también los ojos con gafas de sol y/o sombreros para no dañar la retina y la córnea.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->Utilizar prendas de vestir tupidas y de colores claros, así como manga larga que actúen de barrera frente al sol.

<!--\\[if !supportLists]-->

•       <!--\\[endif]-->No exponer la piel a los rayos UV en las horas centrales del día ni sobrepasar el tiempo de exposición diario recomendado..

Para más consejos, os dejamos este artículo de nuestro blog con consejos para la [protección solar de niños y bebés](https://madrescabreadas.com/2014/07/11/crema-solar-bebes-madrescabreadas/), y éste otro de la [Asociación Española de Pediatría](https://www.aeped.es/noticias/enfamilia-recuerda-importancia-proteger-ninos-y-adolescentes-sol).



## Los mejores protectores solares del mercado

Proteger la piel correctamente es una prioridad para nuestra salud y la de los más pequeños, quienes necesitan del sol como fuente de vitamina D, pero también son más sensibles a recibir sus efectos perjudiciales.

Por eso debemos utilizar protectores solares de calidad y que nos aporten confianza para cuidar la piel de toda la familia de forma eficaz y cuidadosa, respetando sus características para mantenerla protegida sin alterar su dermis.

Una buena opción son los que encontramos en marcas de farmacia de sobra conocidas como Avene, La Roche Posay, Isdin, Leti At4 o [Mustela](https://www.vistafarma.com/marca/mustela), especialistas en el cuidado de las pieles más delicadas, como las de los bebés y niños.

Un gran abanico de productos que podemos adquirir cómodamente a través de la [Farmacia Online](https://www.vistafarma.com/) , igual que como lo haríamos en la farmacia tradicional, pero consiguiendo los mejores precios, con un solo clic y sin ni siquiera salir de casa, recibiendo nuestro pedido en un plazo de 24-48 horas.

*Photo by Feri & Tasos on Unsplash*

*Photo by Jelleke Vanooteghem on Unsplash*