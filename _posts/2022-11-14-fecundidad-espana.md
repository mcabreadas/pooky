---
layout: post
slug: fecundidad-espana
title: Contrato de fecundidad como condición para quedar embarazada
date: 2022-11-14T11:18:54.560Z
image: /images/uploads/premium_photo-1661771719168-449af6de4e8f.webp
author: maria
tags:
  - mecabrea
  - embarazo
  - conciliacion
---
He oído recientemente a una mujer defender que es necesario firmar un contrato de fecundidad con la contra parte, es decir, el otro progenitor o padre, como condición previa a quedarse embarazada, o si no, no habría embarazo.

Nos anima a las mujeres a llevar a cabo una negociacion agresiva en terminos de [contrato laboral](https://www.sepe.es/HomeSepe/empresas/Contratos-de-trabajo/modelos-contrato.html) con nuestra pareja, y plantearnos no tener hijos si el futuro padre no acata por escrito una serie de funciones y reparto de tareas bien claro y determinado, como por ejemplo quien da su numero de telefono en el cole para que lo llamen si se pone enfermo a media mañana y hay que recogerlo (desconozco las medidas coercitivas para hacerlo cumplir).

## ¿La paternidad cabe en un contrato?

No seré yo quien a estas alturas no defienda a capa y espada la co-responsabilidad porque llevo haciéndolo desde antes de que tú te plantearas ser madre seguramente..., no se trata de eso, pero si pensamos así estamos equivocando las razones que nos deben llevar a querer tener un hijo, y nos vamos a estrellar de bruces con una realidad que va más allá de un contrato que obligue a nuestra pareja a asumir responsabilidades.

Las mujeres estamos confundidas y hemos perdido el norte de lo que realmente significa ser madre porque por muy buen abogado que sea quien redactara un contrato de este tipo, jamás podr recoger todo lo que implica la maternidad, la paternidad o formar una familia.

\
El contrato de fecundidad que nos intentan vender pretende asegurar la excelencia profesional de la futura madre por encima de todo. Pone en primer lugar en la escala de prioridades de la madre a sí misma, cosa que a priori podra resultar plausible, pero nada más lejos de la realidad porque desde el momento en que vemos la [primera ecografia de nuestro o nuestra bebe](https://madrescabreadas.com/2021/05/25/ecografia-4-semanas-no-se-ve-nada/), pasa a ser el centro de nuestro universo, y si no estamos dispuestas a asumir esta premisa, vamos a sufrir mucho por una lucha continua entre nuestros anhelos y la cruda realidad.

![madre teletrabajando con bebe](/images/uploads/premium_photo-1661382501429-d1c450bf7666.webp)

## Cuestion de prioridades

Además, ¿estamos segura de que nuestro primer objetivo vital es lograr el éxito laboral y ganar mucho dinero? Si es asi, quiza un hijo no encaje en nuestro proyecto vital, lo que es perfectamente respetable, licito y consecuente si nos planteamos la veda de este  modo.

Dicho esto, y esta es mi opinion como madre de 3 hijos y trabajadora por cuenta propia que ha adaptado su carrera profesional a su maternidad, poniendo por encima su faceta de madre a la de abogada de exito,  creo que estamos equivocando los motivos por los que somos madres, y las prioridades que nos van a traer la verdadera felicidad en esta vida. 

¿Qué pensamos que nos va a quedar cuando tengamos 80 años? 

¿Qué recuerdos vamos a atesosorar? 

¿El buen sueldo que teníamos, las palmaditas en la espalda de nuestro jefe? ¿Un premio a la trabajadora del año? 

¿O esa primera conexión con nuetsro bebé, esa mirada de arrobo con la que nunca nadie nos habia mirado antes, el abrazo más puro del mundo, ese cumpleaños en el que el corazón nos saltó de gozo por estar rodeada de los nuestros, esa excursión en familia donde comprendimos por fin lo que verdaderamente importa y queda en la vida?

¿Qué nos va a hacer sonreír antes de dormir cada noche?

Si no estás dispuesta a amar sin reservas, a dar sin límites, a cambiar tus prioridades y a tener unos valores claros, plantéate realmente tu camino es el de ser madre, no porque no pudieras ser la mejor madre, sino porque te vas a dar una ostia de la que te va a costar levantarte, por mucho contrato que tengas... 

Tras 16 años seindo madre, te puedo decir que ser madre es desarmarnos y volvernos a armar, vaciarnos y volvernos a llenar de una mujer mucho más fuerte, completa y capaz de lo que jamás habrías imaginado, es el mayor aprendizaje de nuestra vida, ser capaz de amar de una manera que incluso acojona, estar dispuesta a correr el mayor riesgo "ever", pero sabiendo que vamos a ganar más que con cualquier excelencia profesional que logremos, porque estaremos logrando la excelencia como personas, es asomarnos a un abismo de miedos e incertidumbres, y recibir a cambio el mayor amor que vamos a experimentar nunca.

## Elige bien

Por eso, si la mayor garantia de que van a estar a partir un piñon contigo es un contrato de fecundidad, no escojas a esa persona como progenitor, porque si no tiene unos valores como los tuyos que le vienen de serie, no la vas a cambiar porque firme unos compromisos. Es más, como la obligues a cumplir luego el contrato, va a ser mucho peor, porque se trata de cuidar un bebé, una persona, sin límites, porque el amor no los tiene. 

![](/images/uploads/daiga-ellaby-7edwo30e32k-unsplash.jpg "co rresponsabilidad")

Por eso, si te llaman del cole porque esta enfermo, lo mas seguro es que tu estomago de un vuelco y salgas corriendo dejando a quien sea con la palabra en la boca, aunque el contrato diga otra cosa, y quiza os encontreis alli los dos porque hay cosas en las que no manda la razon ni el reparto equitativo milimetrado de tareas,

Si ves que es una persona que no le sale ser generoso y amar así de entrada, y no lo ves muy sacrificado ni comprometido , pues escoge otro progenitor.

\
Entonces, dejate de contratos, y busca el mejor compañero para la mayor aventura de tu vida, uno que no necesite comprometerse por contrato, porque ya lleve de serie la generosidad, la capacidad de  amar, de compromiso y sacrificio, con quien compartas tu forma de ver la vida, tus valores. Alguien que cuando te quedes mirándolo sin que se dé cuenta, de estas veces que parece que estás fuera de la escena y tienes como un momento de lucidez, veas en él al padre de tus hijos sin condiciones, y te imagines tomándolo de la mano y saltando juntos al vacío.

\
Ésa es la mayor garantía.  

¿Que opinas?