---
layout: post
slug: quitar-chupete-dormir
title: Cómo quitar el chupete sin dramas
date: 2020-11-05T09:19:29+02:00
image: /images/posts/quitar-chupete/p-cinturon.jpg
author: belem
tags:
  - bebe
  - trucos
  - crianza
---

Casi todas las mamás hemos pasado por una relación amor-odio con los chupetes o chupones. Aún recuerdo cuando mi hijo tenía entre 3 meses y 1 año: era impensable salir de casa sin uno, pues sin el chupete, él no conciliaba el sueño por demasiado tiempo. Es decir, sí dormía, pero muy poco. En cambio, con el chupete, podía dormir siestas largas que me permitían ponerme al corriente con mis tareas pendientes. Si ustedes también se decidieron por su uso, podrían estar familiarizados con la llegada del momento en que, por una razón u otra, es necesario quitar el chupete. Las razones no suelen competerle a nadie más que a la familia. Sin embargo, vamos a revisar algunos casos en los que podría ser mejor quitar el chupete y algunos trucos para lograrlo.

Puede que les resulte de utilidad también este post sobre  [consejos para el destete](https://madrescabreadas.com/2014/01/09/destete-una-experiencia-y-una-visión-profesional/), y éste otro sobre [consejos para quitar el pañal](https://madrescabreadas.com/2016/06/19/cuandopanalbebe/).

## Usar o no chupete

Empecemos por el principio. El controversial uso del chupete. Muchas mamás y papás creemos, más por costumbre que por otra cosa, que el chupete es uno de los básicos para recibir a un recién nacido en casa. La controversia comienza cuando el número de pediatras que recomiendan su uso es igual al número de pediatras que no lo recomiendan en absoluto. 

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="https://rcm-eu.amazon-adsystem.com/e/cm?ref=tf_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=B00744TMNG&linkId=794cca9f0b49d801bb929324782371cd&bc1=ffffff&lt1=_blank&fc1=333333&lc1=303300&bg1=ffffff&f=ifr">
    </iframe>

Los que indican su uso, se basan principalmente en una serie de estudios estadounidenses y australianos que aseguran, el uso de chupete ayuda a prevenir la muerte de cuna o muerte súbita del lactante. Esto, de acuerdo con los estudios, se debe a que, al succionar el chupete, el ritmo cardiaco del bebé se estabiliza al mismo tiempo que ayuda a asegurar su libre respiración, al mantener libre de obstrucciones la nariz del infante.

Cabe mencionar, por supuesto, que la muerte súbita del lactante es un tema extenso en el cual destaca lo poco que se sabe de éste y de sus causas.

Por otro lado, están los detractores del uso del chupete. Principalmente, no recomiendan el uso durante las primeras cuatro a seis semanas de vida del lactante, para evitar una posible confusión entre el chupete y el pezón y acortar o truncar el éxito de la lactancia materna. Otros factores en contra del chupete y su uso prolongado son los problemas en el habla del niño, movimientos y deformaciones en piezas dentales y paladar difíciles de corregir o tratar, además del aparecimiento prematuro de caries y enfermedades recurrentes como la otitis aguda e infecciones fúngicas en la boca (candidiasis).

Ahora no vamos a juzgar a nadie por sus decisiones. Pero, si en tu familia eligieron utilizarlo, puede que después de un tiempo comiences a preguntarte cómo retirarlo sin dramas. Existen una serie de factores y medidas que conviene tomar en cuenta antes de comenzar con este proceso. Vamos a revisarlos.

![bebé tirando el chupete](/images/posts/quitar-chupete/chupete.jpg)

## ¿Cuándo debemos quitar el chupete a nuestros hijos?

Los pediatras y odontopediatras recomiendan retirarlo antes de los 2 años. Muchos incluso recomiendan hacerlo alrededor del primer cumpleaños del infante. No obstante, esto no siempre será posible, pues la mayoría de los pequeños de alrededor de 1 año siguen siendo lactantes. 

![bebe con chupete sentado en un banco](/images/posts/quitar-chupete/banco.jpg)

Conforme nuestros hijos crezcan, la necesidad de succionar irá disminuyendo y veremos que el uso del chupete queda relegado a la hora de la siesta, como un auxiliar para conciliar el sueño. Más adelante, deberían estarlo usando solo por la noche y solo para ayudarse a dormir, no durante toda la noche. 

Muchos infantes lo dejan por sí mismos. Así, sin más, un buen día lo rechazan o no lo piden y no vuelven a pensar en él. En mi caso, recuerdo claro el día en que, a sus 10 meses, mi hijo hizo lo impensable: a punto de quedarse dormido con su chupete, comenzó a expresar asco y lo “escupió”. Jamás lo volvió a pedir. Tuvimos suerte, creo. Entonces, esperen. La mayoría de los pequeños lo dejan, o gradualmente, o de un día para otro. 

## Trucos para quitar el chupete.

Si después de los dos años nuestro hijo se resiste a dejar el chupete, quizás sea momento de tomar acción. Lo importante es saber que este proceso será gradual. Como tal, debemos proceder con calma y paciencia, sin decisiones bruscas o drásticas. De la misma manera, debemos evitar los castigos, las amenazas y los regaños. 

## No negar, no ofrecer el chupete

Es primordial actuar paulatinamente. Puede ser buena idea usar la famosa filosofía de “no negar, no ofrecer”. Esto significa que nosotros no se lo vamos a ofrecer, pero si nos lo pide se lo daremos. Conforme avance el tiempo, podemos comenzar a distraer a nuestro hijo o hija con otra cosa cuando nos lo pida. 

Por ejemplo, si solo lo quiere por aburrimiento, podemos ofrecer jugar juntos, hacer alguna tarea en casa o salir a caminar. Lo que sea que le distraiga y quite, aunque sea por un momento, el chupete de su mente. 

## Cambiar de tercio para olvidar el chupete
De esta medida, se desprende otra estrategia. Hay que aprovechar las fracturas en la rutina diaria para ir acoplando esta nueva medida. 
> Si, amor, sé que quieres el chupete. Pero ahora estamos en el parque. Cuando regresemos a casa podrás usarlo. Ahora mismo no lo tengo conmigo. 

Un cambio tan drástico puede ayudarse de otros pequeños.

![bebé en carrito con manta verde](/images/posts/quitar-chupete/carrito.jpg)

## Llegar a acuerdos sobre el uso del chupete
Si nuestro niño es un poco mayor, debemos hablarlo con él o ella. Podemos llegar a acuerdos. Muchas veces subestimamos a nuestros niños, pero la verdad es que puede que hablando se logre más de lo que nos imaginamos. 

## El hada del chupete
Muchos padres puedes no estar de acuerdo con la siguiente sugerencia, pero podemos intercambiar el chupete por algún juguete, libro o artículo que les haga mucha ilusión. He visto que hay países en los que, de manera similar al ratón o hada de los dientes, los niños dejan su chupón bajo la almohada y en su lugar amanece un gran regalo. Es una linda idea en mi opinión. 

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=0993420389&linkId=6810b5438c925472d9b72484021fc885&bc1=ffffff&lt1=_blank&fc1=333333&lc1=303300&bg1=ffffff&f=ifr">
    </iframe>

## Sin insistencia, pero con firmeza 
Por último, no hay que insistir demasiado. Muchas veces entre más insistencia de nuestra parte noten los niños, más resistencia opondrán. Actuemos sin urgencia, sin ansiedad y con mucho amor, apoyo y empatía. 

Determinar si el momento de quitar el chupete ha llegado dependerá sólo de los padres y el médico pediatra u odontólogo. No hay una regla en cuanto al momento exacto en que el chupete debe quitarse. Recordemos que cada niño es un mundo y lo que le ha funcionado a uno puede no funcionarle a otro. Como todo con los niños pequeños, ser consistente suele ser la clave. 

Si notamos alguna conducta irregular o que realmente está resultando imposible quitar el chupete, podría ser una buena idea consultar con el pediatra. Este profesional podría indicarnos si vamos bien o si quizás es momento de prestar atención a la forma en la que estamos criando o si es momento de pedir ayuda de alguien más.  

Belem Martínez

*Photo portada by Sharon McCutcheon on Unsplash*

*Photo by Zeesy Grossbaum on Unsplash* 

*Photo by Janko Ferlič on Unsplash niño en el banco*