---
layout: post
slug: libro-los-escribidores-de-cartas
title: Libro infantil Los Escribidores de Cartas
date: 2019-06-07T08:00:00+02:00
image: /images/posts/libro-los-escribidores-de-cartas/p-portada-escribidores-de-cartas.jpg
author: .
tags:
  - 6-a-12
  - planninos
  - libros
---

La [relación nietos-abuelos](https://madrescabreadas.com/2013/11/15/sueno-infantil/) es una de las más especiales que existen. En nuestra familia así lo vivimos cada día porque el vínculo que tienen mis hijos con sus abuelos es muy fuerte y les aporta mucho a ambas partes. Por eso el libro [Los Escribidores de Cartas](https://es.literaturasm.com/libro/escribidores-de-cartas-cartone), de la colección Barco de Vapor, tan querida por todas las mamás de mi generación por haber formado parte de nuestra infancia, Editorial Literatura SM, nos ha conquistado en casa.

Es un libro que yo recomiendo para lectores de 7 a 9 años, pero si tenéis niños más pequeños, podéis leérselo o incluso leerlo a medias con ellos para que no se cansen, pero la historia les va a enganchar desde el minuto cero, además de darles unas ganas locas de escribir cartas.

## Sinopsis 
Esta historia va de amor de una nieta por su abuelo y viceversa, de amistad, de integración, de solidaridad y de trabajo en equipo. También tiene un componente de misterio y alguna que otra sorpresa.

Federico es el cartero de un pequeño pueblo en el que apenas ya se escriben cartas, por lo que su puesto de trabajo peligra.Y no sólo por eso, sino porque el Alcalde parece tener un extraño motivo oculto en contra de él por algo que ocurrió en el río hace mucho tiempo...

Iria, su nieta, y sus amigos, uno de ellos con Trastorno obsesivo compulsivo (TOC), quien lo lleva con total naturalidad y así lo hace ver a sus amigos, lo que visualiza este tipo de trastornos y los normaliza, no permitirán que echen a Federico del trabajo, y se embarcarán en una emocionante aventura con youtuber incluido.

Una curiosa mezcla de la comunicación digital y la tradicional llevará a los pequeños lectores a valorar cada una de ellas.
¿Crees que sería bueno recuperar la costumbre de escribir cartas?