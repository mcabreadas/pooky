---
layout: post
slug: calendario-adviento-tipos
title: Qué es un calendario de Adviento. Tipos e ideas
date: 2020-11-30T17:19:29+02:00
image: /images/posts/calendario-adviento/p-calendario.jpg
author: belem
tags:
  - 6-a-12
  - trucos
  - regalos
  - navidad
---
Dependiendo del país en el que nos encontremos, podemos estar más o menos familiarizados con el calendario de adviento. Vamos por partes entonces para conocer más acerca de esta entretenida tradición. 

## ¿Qué es un calendario de Adviento?

Un calendario de Adviento es prácticamente una cuenta regresiva, un conteo del tiempo que falta para navidad o Nochebuena en forma de calendario. Muchos lo interpretan como un periodo de preparación para la llegada de Jesucristo. 

## ¿Cuál es la historia del calendario de Adviento?

La palabra adviento tiene sus orígenes en el latín adventus que significa advenimiento, venida o llegada. El comienzo del calendario de Adviento coincide con el comienzo del encendido de las velas de la corona de Adviento. Ambas tradiciones son de origen alemán, sólo que mientras que la corona es de origen cristiano, el calendario es de origen protestante. De hecho, se sabe que su creador fue el pastor protestante alemán Johann Hinrich Wichern, quien lo creó como una forma de ayudar a los insistentes niños a saber cuánto tiempo quedaba para Nochebuena. 

Sin embargo, este calendario se parecía más bien a la famosa corona de adviento, pues era una corona fabricada con una estructura de madera con silueta circular y 23 velas (19 pequeñas y 4 grandes; las pequeñas se encendían diario, las grandes se encendían los domingos). 

En 1902 se imprimió el primer calendario de adviento, en Alemania también. Los primeros calendarios tenían imágenes recortables que se iban pegando conforme los días iban avanzando hasta llegar a Nochebuena. Más tarde se fue modificando y pequeñas ventanillas fueron añadidas. Había una ventanita por cada día, dentro de las cuales se encontraban pequeños regalos.  

## ¿Cómo funciona el calendario de adviento?

El concepto es muy sencillo. Es un calendario conformado de diversas casillas (casi siempre entre 23-30), una por cada día desde el comienzo de diciembre o del adviento hasta llegar a Nochebuena. Cada casilla va marcada con un número que indica el orden en que las casillas deben ser abiertas. 

Dentro de cada ventanilla se puede encontrar un pequeño caramelo o regalo. Las casillas deben ser abiertas una por día, de tal forma que se crea anticipación y se lleva de manera ordenada un conteo regresivo hasta la Navidad.  

## Calendario de adviento, ¿Cuándo empieza?

El Adviento litúrgico a comienza el domingo que caiga entre el 27 de noviembre y el 3 de diciembre, es decir, entre los últimos días de noviembre y los primeros de diciembre. 

Sin embargo los calendarios de Adviento que se comercializan dependen del fabricante, pues mientras muchos cuentan con las 24-23 ventanitas exactas por cada día desde que comienza diciembre hasta Nochebuena, otros comienzan al mismo tiempo que el Adviento litúrgico, que es variable según el año.

En cuanto a la fecha de término, esa nunca cambia, pues un calendario de Adviento siempre culmina el 24 de diciembre con la Nochebuena. 

## Tipos de calendarios de Adviento

Existen muchos tipos de calendarios de Adviento. En un comienzo estos estaban dirigidos casi exclusivamente a los niños, no obstante, hoy en día los hay de todos los tipos y para toda clase de personas. Los más clásicos, son el típico calendario con pequeñas ventanas dentro de las cuales se pueden encontrar pequeños dulces o caramelos. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=863002347325508137" height="534" width="345" frameborder="0" scrolling="no" ></iframe>

Las variantes al clásico modelo pueden incluir un pensamiento o reflexión por cada día, o incluso pasajes bíblicos.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=281543707178900" height="344" width="345" frameborder="0" scrolling="no" ></iframe>

Otros incluyen actividades específicas para realizar ese día, por ejemplo, hornear galletas, salir a caminar y juntar piñas de pino, otro día la actividad puede ser pintar las piñas, etc. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=309059593185460907" height="588" width="345" frameborder="0" scrolling="no" ></iframe>

Algunas marcas, por supuesto, no se han quedado atrás y han puesto en el mercado calendarios de Adviento. Por ejemplo, Lego, [Playmobil](https://amzn.to/3qhuzCz) y [Funko](https://amzn.to/2HW4CHy) (enlace afiliado) tienen disponibles sets que cambian cada año, en los que al abrir cada ventanita en lugar de encontrar bombones, caramelos o chocolates, encontramos pequeños juguetes. 

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=B082WDM878&linkId=e70a1ae6ff275b021143360656e00c27&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Y porque los calendarios de adviento no son exclusivos para los niños y resultan ser un excelente regalo, actualmente podemos encontrar calendarios “para adultos” con recompensas como maquillaje, [cerveza](https://amzn.to/3mpve2H), [vino](https://amzn.to/3fPjC6u), [cápsulas de café](https://amzn.to/3ls5OzT) (enlace afiliado)...

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=B08L4NN7LP&linkId=8bb90bc88f9c90e548ba41ca2f2210dc&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Finalmente, están los DIY. Estos son para aquellos que buscan economizar un poco sin dejar de lado la tradición o hacer algo completamente personalizado. Aquí la creatividad no tiene límites. Los regalos dependerán del gusto de cada quién. Podría ser que no hayan encontrado, por ejemplo, un calendario que satisficiera sus deseos y gustos específico, entonces decidieron hacerlo con los dulces favoritos de distintas marcas, botellas de vino, pulseras, accesorios para el cabello, litografías, dulces típicos de una región, libros, etc. 

Aquí os dejo esta manualidad de [Calendario de Adviento de Mamá Búho](https://mamabuhomallorca.wordpress.com/2020/11/28/calendario-de-adviento-de-papel/) para hacer con los peques.

Ahora que ya sabemos qué es un calendario de adviento, comencemos. La idea es divertirse, reflexionar y esperar con ganas la Nochebuena. 

¡No hay que gastar mucho para esto y hay ideas para todos, chicos y grandes!

Si te ha servido compártelo para que ayude a otras familias. además estarás contribuyendo a que sigamos escribiendo este blog.

*Foto de portada by Markus Spiske on Unsplash*