---
layout: post
slug: palabras-jerga-adolescentes
title: Diccionario adolescente para entender a tu hijo
date: 2021-01-24 08:10:29
image: /images/posts/jerga-adolescente/p-banera.jpg
author: maria
tags:
  - adolescencia
---
La pandemia nos hizo a las familias pasar mucho tiempo en casa y esto tambien trajo cosas buenas. Entre ellas, al estar "obligados" a convivir más tiempo tuvimos la oportunidad de **conocer mejor a nuestros hijos adolescentes**. Y está ocurriendo algo maravilloso; ellos nos han mostrado cosas que antes sólo pertenecían al ámbito privado de su círculo de amigos.

Por ejemplo, su forma tan peculiar de hablar, su jerga juvenil, su especial idioma adolescente, su especial vocabulario, esas palabras modernas que en nuestros tiempos eran "dabuten", "tronco", "carroza", "guay"... **palabras que se han quedado con nosotros para siempre**, y que incluso algunas han sido aceptadas por la RAE e incorporadas al vocabulario normal de todos.

![libro padres de adolescentes](/images/uploads/51gynp3-q5s.jpg "Quiéreme cuando menos lo merezca...")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://amzn.to/3EDi2Bk) (enlace afiliado)

Es increíble escucharlos en videollamada o jugando al Fortnite  (si no sabes de qué va este videojuego [aquí te lo explico de madre a madre](https://madrescabreadas.com/2019/10/22/el-fortnite-de-madre-a-madre/)), cuando están en su salsa siendo ellos mismos, ver la cantidad de palabras que usan y que la mayoría desconocemos totalmente lo que significan.

Esto ha llegado esta semana a la comunidad de Madres Cabreadas, y **hemos querido montar entre todas una especie de diccionario adolescente**. Entonces, ha ocurrido algo inesperado: al lanzar la pregunta en Instagram han contestado varios chicos y chicas explicando, armados de paciencia, cada palabra que no sabíamos o teníamos dudas.

En nuestras casas ha habido una conexión especial con ellos porque se han sentido importantes al verse tomados en consideración y ver que tenían algo que aportar constructivo.

La etapa adolescente de nuestros hijos nos hace sentirnos perdidos, muchas veces porque no sabemos cómo acercarnos a ellos. Pero no estamos solos, a todos los padres y madres nos pasa lo mismo. Por eso, os recomiendo que os forméis con **cursos, [talleres](https://madrescabreadas.com/2023/05/31/talleres-sobre-seguridad-en-internet-para-ninos/), libros especializados en la adolescencia,** porque de verdad que os van a ayudar mucho.

![Cómo Hablar para que los Adolescentes escuchen y Cómo escuchar para que los Adolocentes hablen](/images/uploads/3.jpg "Cómo Hablar para que los Adolescentes escuchen y Cómo escuchar para que los Adolocentes hablen")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/C%C3%9Bmo-Hablar-Adolescentes-Escuchen-Escuchar/dp/006084129X/ref=pd_sim_5/261-3752076-3735521?pd_rd_w=jl8mU&pf_rd_p=d7b37f94-52ed-403e-84d3-cba43a6e7975&pf_rd_r=5QCBDQS1G8CV2GYW31V4&pd_rd_r=121d46e3-ce71-4c09-9708-3977d1a53c2d&pd_rd_wg=mWRBY&pd_rd_i=006084129X&psc=1&madrescabread-21) (enlace afiliado)

## Cuáles son algunas palabras que usan los estudiantes en la actualidad

### El diccionario para Boomers de Cristian Olivé

También hemos descubierto la maravillosa actividad que un profesor de Barcelona está llevado a acabo con sus alumnos. Este **consiste en hacer propiamente un diccionario**, con todo el rigor que ello conlleva, con los términos que forman el lenguaje adolescente. Así, este diccionario de Cristian Olivé, en Twitter [@xtianolive](https://twitter.com/xtianolive), ha ayudado a completar el nuestro con su [Diccionario para Boomers](https://twitter.com/hashtag/DiccionarioParaBoomers?src=hashtag_click).

Te recomiendo su libro Una educación Rebelde. Tienes este libro en e-book, y muchos mas en [Kindle Unlimited con una prueba gratis de 30 dias](<https://www.amazon.es/kindle-dbs/hz/signup?tag=madrescabread-21. >), tiempo suficiente para leertelo ;) 

Y si eres de las que te gusta leer te va a salir super rentable porque tienen un catalogo de mas de un millon de libros, entre los cuales estan [mis libros favoritos](https://www.amazon.es/shop/madrescabreadas/list/2Y9ZGCQHAZACW?ref_=aip_sf_list_spv_ons_mixed_d) (enlace afiliado) que me han acompañado a lo largo de mi carrera como madre de 3.

![Una educación rebelde](/images/uploads/2.jpg "Una educación rebelde")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/B08VLR2KP6/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=7da4aa6cf6fd7032c6b25875424e4489&creativeASIN=B08VLR2KP6) (enlace afiliado)

A pesar de que nos llamen "boomers" (es decir, carrozas: la expresión viene de la generación del baby boom), las madres nos interesamos por entenderlos y os puedo contar las palabras más comunes que nos han enseñado hasta ahora:

## Qué palabras usan más los adolescentes

\-Mamadísimo: fuerte, musculoso

![mamadisimo](/images/posts/jerga-adolescente/mamadisimo.JPG "mamadisimo")

\-Dar **cringe**: dar grima o vergüenza ajena 

\-Estar mamadísimo: estar musculoso, fuerte

\-Ser el **“admin”**: ser el jefe

![admin](/images/posts/jerga-adolescente/admin.JPG "admin")

\-**Panas**: colegas, amigos. También hay panas que son animales y que se ven muy "freskos". Cada vez está de moda un pana: empezaron con el pana Miguel, luego el pana Óscar (que es una oveja con una bufanda),y la lamentadísima muerte del pana Ricardo, el canguro mamadísimo que salvó millones de vidas.

\-“**Se ve fresco**”: alguien que se ve bien, agusto...

\-Estar de **chill**: estar de relax 

\-Esta to Gucci: está chulo, guay...

\-**M.D.L.R.**: ser “de calle”, como de barrio, duros...
![mdlr](/images/posts/jerga-adolescente/mdlr.jpg)

\-**Crush**: amor platónico

![crush](/images/posts/jerga-adolescente/crush.JPG)

**Sudadera para Hombre**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Cloud-City-Normal-Hooded-Sweatshirt/dp/B084HP89YK/ref=sr_1_31?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=sudadera%2Bcrush&qid=1628887372&sr=8-31&th=1&madrescabread-21) (enlace afiliado)

\-**Scrunchie**: coletero de tela con el elástico por dentro 

\-Cayetanas: pijas

\-Estár **chetado**: en un videojuego tener demasiado poder

![chetar](/images/posts/jerga-adolescente/chetado.JPG)

\-Se ha **bugueado**: se ha atascado, refiriéndose a un juego o programa

\-**Otaku**: Quien juega demasiado a los videojuegos, ve anime, y no tiene tiempo de duchar

\-**Niño rata**: quien juega demasiado a los videojuegos

\-**Trolear** a alguien: hacerle una broma

\-**Stalkear** a alguien: cotillearle su cuenta de twitter o instagram...

\-Me **putoencanta** está bien dicho, la RAE aceptó el prefijo "puto" hará como un par de años

\-**LOL**: reírse mucho, y lo dicen de palabra, igual que "XD"

\-**Wtf** son las siglas de “What The Fuck”, y se utiliza como “qué coñ***" o "qué cojo***

\-**Random**: algo aleatorio

\-Algo que está **piola**: algo que mola, que está bien

\-**Bro**: amigo, hermano

\-**Dar lache**: dar vergüenza

\-Dar **to el palo**: dar pereza

\-**shippear**: emparejar

![shippear](/images/posts/jerga-adolescente/shippear.JPG)

\-**nerd**: empollón

\-**Spoiler**: desvelar el final o alguna parte de una serie o película 

\-Streamer: retransmitir en directo un evento

\-**Banear** a alguien: echarlo

\-Algo está **fachero** cuando está chulo

\-**F en el chat**: cuando algo no sale bien o no como era de esperar

![f en el chat](/images/posts/jerga-adolescente/f.JPG)

\-**Bufear**: mejorar el atractivo, la potencia o las posibilidades de un objeto, personaje... de un videojuego

\-**Nerfear**: lo contrario de bufear

\-**petado**: fuerte, cachas

\-**OP**: se lee por letras y significa que algo está "roto" (chetado), mejorado a tope

\-**OG**: se lee en inglés y significa "old gang", en un videojuego significa que eres de los que empezaron en el principio de un juego cuando nadie le daba una oportunidad (o sí) y que por ello tienes más experiencia a la hora de jugar.

Aquí tenéis otro [diccionario para boomers que han hecho los alumnos de 4ESO del IES La Morería de Mislata](https://drive.google.com/file/d/1PmCUYMLZ3NtU3gJFtM5_lxEjZEeFwRD8/view) compartido por [@luisaalcacer](https://twitter.com/luisaalcacer).

Otro recurso para conectar con tu adolescente sin llegar a ser pesada por acribillarlo a preguntas es este episodio del podcast de las Madres Cabreadas. Guardalo para escucharlo en un rato!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1VWCFOLprRlN834liOpqMS?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

## Como puede ayudarme el lenguaje a conectar con mi adolescente

Hay veces que los permanentes conflictos con nuestros hijos e hijas adolescentes ensombrecen nuestra relacion por no saber manejarlos correctamente No olvidemos que los adultos somos nosotros, y que ellos tienen su cerebro en vias de desarrollarse todavia.

Usar terminos familiares para lograr un acercamiento o incluso unas risas (dejemos que nos llamen boomers y riamonos con ellos) pueden desbloquear una situacion tensa y evitar consecuencias peores.

No dejes que te afecte cuando [tu adolescente te dice que te odia](https://madrescabreadas.com/2022/02/03/mi-hijo-adolescente-me-odia/), porque lo hacen todos! Y no es verdad.

Y hasta aquí nuestro aprendizaje de idioma juvenil por ahora, podéis dejarme en los comentarios más palabras!!

Si te ha gustado comparte y suscríbete gratis al blog para no perderte nada.

[Quiero suscribirme gratis](https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c)