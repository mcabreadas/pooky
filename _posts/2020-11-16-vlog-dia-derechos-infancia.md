---
author: maria
date: '2020-11-16T18:19:29+02:00'
image: /images/posts/vlog/p-derechos-nino.jpg
layout: post
tags:
- vlog
title: Vlog Día de los Derechos de la Infancia
---

El 20 de noviembre se celebra el [día de los derechos del niño y de la niña](https://madrescabreadas.com/2020/11/14/dia-derechos-nino/), y este año cobra un nuevo sentido bajo mi punto de vista porque los pequeños están demostrando un grado de adaptación, madurez y valor ante esta situación de pandemia digna de admiración. Cada día nos dan lecciones de responsabilidad y entereza que, al menos en mi caso, me ayudan a seguir adelante con esperanza y actitud positiva.

En este video os muestro recursos para enseñar a los niños cuáles son sus derechos y la importancia que tiene que sean respetados.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wRZ5N9jzCC4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha gustado comparte y suscríbete a mi canal! Así me ayudarás a seguir generando este tipo de contenido.