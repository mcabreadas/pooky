---
layout: post
slug: dia-mundial-contra-el-trabajo-infantil
title: ¿Por qué todavía es necesario conmemorar el día mundial contra el trabajo
  infantil?
date: 2022-06-12T15:00:23.492Z
image: /images/uploads/trabajo-infantil.jpg
author: faby
tags:
  - educacion
---
La era de la esclavitud fue abolida hace más de un siglo, a pesar de ello, en la actualidad se sigue percibiendo muchos trabajos de “esclavitud”. Pero este problema empeora cuando los “trabajadores son niños”. Según un informe de la *[Organización Internacional del Trabajo (OIT)](https://www.unaids.org/es/aboutunaids/unaidscosponsors/ilo#:~:text=Organizaci%C3%B3n%20Internacional%20del%20Trabajo%20(OIT)%20%7C%20ONUSIDA&text=La%20Organizaci%C3%B3n%20Internacional%20del%20Trabajo,el%20trabajo%20en%20el%20mundo.)* “**1 de cada 6 niños entre 5 y 17 años, están implicados en el trabajo infantil”**. Esto se traduce en 246 millones de niños alrededor del mundo.

Tal como destacan las estadísticas, este es un problema serio y global. El *[trabajo infantil viola los derechos humanos](https://www.unicef.es/causas/derechos-infancia)* de los niños. 

## ¿Por qué se celebra el día mundial contra el trabajo infantil?

**Millones de niños son obligados a llevar a cabo trabajos de explotación** que ponen en riesgo su bienestar físico, emocional y mental. Muchos de estos trabajos son peligrosos y conllevan más de 12 horas de esfuerzo físico.

Con la finalidad de que la **población en general logre sensibilizarse** en esta problemática mundial, la Organización de las Naciones Unidas (ONU) ha proclamado el “día contra el trabajo infantil”.

### ¿Cuándo se celebra el día internacional contra el trabajo infantil?

El día mundial contra **el trabajo infantil se celebra el 12 de junio.** Este año 2022, se está haciendo un esfuerzo más concienzudo para que se tomen decisiones políticas definidas que conduzcan a la erradicación de la explotación infantil.

## ¿Qué esfuerzos se están realizando para combatir la explotación infantil?

En los últimos años se ha realizado una gran variedad de **esfuerzos políticos y organizativos para mejorar la calidad de vida de las personas.** De hecho, se insta a los países miembros de la OIT a no cesar en la creación de programas para la lucha contra el trabajo en niños.

En muchos lugares del mundo, los niños trabajan para “sostener económicamente a su familia”, así que dejan la escuela en búsqueda de “formas de ganar dinero rápido”. 

Esto evidencia que, la raíz del problema son las malas condiciones de millones de familias y el poco acceso a la formación académica. 

Hay que **abrir mejores oportunidades de empleo para personas de escasos recursos,** y al mismo tiempo ofrecer mejores oportunidades de estudio a las familias más vulnerables.

## Campaña "#TheUnescapeRoom"

![nina mostrando las manos sucias](/images/uploads/no-al-trabajo-en-ninos.jpg)

UNICEF España ha lanzado una campaña por distintos medios digitales llamada **"#TheUnescapeRoom".** Muchos niños piensan que al empezar a trabajar pueden salir cuando quieran, pero la realidad es que muchos de ellos no tienen un escape.

En tal sentido, el video presentado por la UNICEF, pretende que la población empatice y vea de frente esta realidad. De hecho, encierra un mensaje claro y directo “**tú puedes ser el botón de salida de estos niños explotados”.**

<iframe width="560" height="315" src="https://www.youtube.com/embed/n2p8G5mUTRg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Los padres y el trabajo en casa

Los niños tienen derecho a vivir en un entorno familiar sano, además, tienen derecho a una buena educación, comida, salud y una casa digna.

Los padres de familia pueden ayudar a sus peques a valorar el entorno en el que están creciendo. Además, **pueden fomentar; según la edad, algunos trabajos en casa**, como ayudar en el aseo doméstico, el cuidado de mascotas, etc.

Este tipo de trabajo es sano y ayuda a los niños a desarrollar habilidades y cualidades necesarias para su vida adulta.

Otros dias para conmemorar en familia son el [dia de los abuelos](https://madrescabreadas.com/2021/07/25/dia-de-los-abuelos-espana/).