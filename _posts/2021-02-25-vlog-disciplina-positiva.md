---
author: maria
date: 2021-02-25 08:13:29
image: /images/posts/vlog/p-disciplina-positiva.png
layout: post
slug: vlog-disciplina-positiva
tags:
- vlog
- educacion
- puericultura
- crianza
title: Vlog disciplina positiva
---

La disciplina positiva es un modo de ver la vida con sentido común. Es aplicable a la crianza con los peques, a la pareja, a las empresas y, en general, a cualquier relación humana.

Es una mezcla de respeto, amor, comprensión, empatía y honestidad.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xLDD5XQkj-Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## ¿Por dónde empiezo para aprender a educar en disciplina positiva?

Por un taller de varias sesiones. Mis talleres son vivenciales, participativos y muy divertidos. Te aportan herramientas y una visión global de cómo es educar en disciplina positiva.

Además, al ser de varias sesiones, vas a adquirir los conocimientos y herramientas poquito a poco y las podrás ir poniendo en práctica. Te surgirán dudas que podrás ir consultando y contarás con mi acompañamiento durante el proceso.

Una vez que se acaba el taller, tenemos una comunidad para seguir con el enfoque de la disciplina positiva.

![taller disciplina positiva](/images/posts/disciplina-positiva/cartel.png)

<a href="https://www.mamiintensa.com/registro/" class='c-btn c-btn--active c-btn--small'>Código descuento: madrescabreadas15</a>


## Consulta gratis con nuestra experta en disciplina positiva

Si quieres consultar alguna duda concreta sobre la educación de tus hij@, está en los terribles dos, no te hace ni caso o te cuesta un mundo que llegar al colegio por las mañanas a tiempo, puedes consultar de forma gratuita con nuestra experta en disciplina positiva, Raquel Peinado.

<a href="https://foro.madrescabreadas.com/t/hablemos-de-disciplina-positiva" class='c-btn c-btn--active c-btn--small'>Haz una consulta a la experta</a>


Te he ayudado? Ahora ayúdame tú a mí si quieres: suscríbete y comparte!


<div class="" style="background-color: rgba(249, 182, 164, 0.32);padding: 20px;border-radius: 20px;">
    <p>
        <img src="/images/posts/vlog/banner-momo.png" alt="banner momo" data-action="zoom">
    </p>

    <h3 id="la-propuesta" style="text-align: center;">
        La propuesta
    </h3>

    <p>
        Momo Clothes es un comercio local en pleno centro de Murcia de ropa para mujer, chicas adolescentes y Comunión. Es un espacio vivo donde podrás encontrar esas piezas exclusivas que te definen.
    </p>
    <p>
        Esta firma busca y selecciona a través del mundo prendas diferentes que te permitan expresar tu esencia y respeten tu confort; que te hagan sentir especial y perduren en el tiempo.
    </p>
    <p>
        Sus colecciones están basadas en su interpretación de la moda y de la vida, porque la vida es simplemente extraordinaria, y Momo Clothes te la quiere hacer extraordinariamente simple.
    </p>

    <div style="text-align: center;margin-top: 2em;">
        <a href="https://momo.com.es">
            <button class="c-btn c-btn--small">Visita la web de Momo Clothes</button>
        </a>
    </div>
</div>