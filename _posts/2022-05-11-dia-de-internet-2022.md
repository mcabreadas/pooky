---
layout: post
slug: dia-de-internet-2022
title: Día mundial del Internet 2022
date: 2022-05-22T18:34:43.626Z
image: /images/uploads/dia-de-internet.jpg
author: faby
tags:
  - adolescencia
  - tecnologia
---
Desde el 2005 se celebra todos los **17 de mayo el día mundial del Internet.** Y es que Internet es un invento que sencillamente cambió el mundo. Marcó una nueva era, cambió la forma de comunicación e incluso abrió las puertas a nuevas formas de trabajo.

Ahora bien, las ventajas del Internet son conocidas a nivel mundial. Pero, es de suma importancia darle un uso responsable. Aunque nuestros niños tienen derecho a acceder a la red, es vital que lo hagan según su grado de madurez y de forma segura. En este artículo, explicaremos **cómo puedes ayudar a tus hijos a que naveguen de forma segura en Internet.**

## ¿Por qué se celebra el Día del Internet?

El Internet ha sido el resultado de arduas investigaciones de científicos, programadores e ingenieros, indudablemente es una excelente herramienta. De hecho, esta efemérides recuerda el **derecho al acceso a las nuevas tecnologías.** En algunos países se conoce como el Día Mundial de las Telecomunicaciones y la Sociedad de la Información.

Esta fecha de celebración ha sido promovida por la Asociación de Usuarios de Internet e Internet Society. El objetivo de la misma es **promover la accesibilidad.** 

## Peligros del Internet para los niños

![](/images/uploads/internet-ninos.jpg)

Los niños necesitan conocer el entorno digital, pues los medios electrónicos influyen en casi todos los aspectos de la vida. Además, no hay que olvidar que mediante la red, los peques tienen acceso a aprendizaje, se comunican e incluso juegan.

Ahora bien, tal como explica el *[Comité de los Derechos del Niño (CDN)](https://www.ohchr.org/es/treaty-bodies/crc)*, el entorno digital puede exponer a los niños a peligros reales. A continuación, destaco algunos de ellos.

* **Contenido inapropiado o no apto para su edad.** Se puede conseguir información sesgada o parcial, contenido explícito de violencia, drogas, etc.
* **Pornografía.** Dentro de esta clasificación hay muchos peligros como el Sexting (envío de contenido de carácter sexual mediante fotografías, audios o vídeos). De igual forma, se pueden topar con el Grooming (pornografía infantil y la pederastia en Internet).
* **Acceso a foros.** Pueden acceder fácilmente a foros que imparten discursos de odio, discriminación
* **Ciberacoso.** El bullying está extendido en todo el mundo, y el acceso al Internet (redes sociales) ha facilitado las agresiones debido a que se pueden hacer en anonimato y sin acceso a denuncias.

El *[Fondo de las Naciones Unidas para la Infancia](https://www.unicef.es/educa/dias-mundiales/dia-internet-segura)* (UNICEF; por sus siglas en inglés), explica que los peligros en la red no son una situación excepcional, más bien es un problema a nivel mundial que afecta a millones de niños y adolescentes.

## ¿Cómo crear una Internet segura para los niños?

![](/images/uploads/internet-segura-para-los-ninos.jpg)

Tal como un cuchillo es muy útil en la cocina, un mal uso del mismo puede acarrear serios peligros, ¿debíamos dejar de usar un cuchillo para cortar las verduras por los peligros que envuelve su uso? Pues no. Lo más sabio es cerciorarse de usar esta herramienta con precaución. Pues bien, lo mismo podemos decir del Internet. **Es necesario enseñar a los niños la forma correcta de usarlo.**

* **Los padres deben enseñarles a navegar en Internet**. Para que los hijos naveguen seguros, los padres deben tener al menos un conocimiento básico del Internet. En este sentido, deben conocer las redes sociales y sus peligros. Nunca asumas que en la escuela le enseñaran a navegar seguro. Tampoco pienses que ya eres muy mayor para entender “este mundo tecnológico”. Te dejo un [glosario de terminos relacionados con Internet](https://www.granviaabogados.com/blog/glosario-de-terminos-relacionados-con-internet/) que te ayudara a comprender mejor este nuevo mundo.
* **Control parental.** Hay programas en el que puedes restringir el acceso a ciertos contenidos. Esto es importante mientras los niños aprenden a usar de forma responsable las redes. Por cierto, enseña a tus hijos a que nunca den datos personales.
* **Fija reglas de uso.** Es necesario que se establezcan normas para el tiempo de conexión y los tipos de páginas que se visitan.
* **Vigila lo que hacen tus hijos.** Tal como en una empresa se hace cada cierto tiempo inspección, de igual modo, los padres deben inspeccionar las actividades que realizan los niños en Internet. Esto no es una invasión a la privacidad, pues el Internet es una red pública, y hasta el FBI accede a este registro cuando lo necesita.
* **Comunicación**. Este es el consejo más importante. Lejos de colocar interminables reglas, debes inculcar buenos valores. De este modo, no se sentirán tentados a hablar con desconocidos o a mirar contenido no apropiado. Te dejo un [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) que te ayudaran a comunicarte con tu hijo mejor.

En conclusión, Internet encierra muchos peligros, sin embargo, también es muy útil, por eso involucrate en el aprendizaje de los medios electrónicos, de ese modo, tus hijos podrán superar cualquier peligro en la red.