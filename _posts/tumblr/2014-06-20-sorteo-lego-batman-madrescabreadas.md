---
date: '2014-06-20T08:00:31+02:00'
image: /tumblr_files/tumblr_inline_n76hc8hMrx1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- premios
- recomendaciones
- familia
- juguetes
title: Sorteo de vacaciones Lego Batman
tumblr_url: https://madrescabreadas.com/post/89336988977/sorteo-lego-batman-madrescabreadas
---

Por fin han llegado las vacaciones de los peques y, aunque a mí no me gusta condicionar los buenos resultados en el cole con regalos, sino que prefiero que el premio sea la satisfacción del esfuerzo y de haber aprendido cosas nuevas, sí que solemos regalar a las fieras algún juguete divertido y, a poder ser, educativo, para que disfruten los largos días de verano, sobre todo en las horas de más sol, en las que preferimos que estén dentro de casa.

Este año Lego se nos ha adelantado, y les ha regalado este set de Batman que, sobre todo a Osito, le ha encantado, ya que le encanta jugar a que es este superhéroe, y disfrazarse con máscara y capa incluídas.

 ![image](/tumblr_files/tumblr_inline_n76hc8hMrx1qfhdaz.jpg)

Pero También queremos que vosotros lo disfrutéis, por eso LEGO me cede 2 juegos para sorteo.

Qué tenéis que hacer para participar?

Dejar un comentario en este post diciendo que queréis el kit de batmen de Lego, y vuestro nick de Facebook o Twitter para localizaros.

También os invito a que sigáis mi blog en [Facebook](https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063?ref=hl) y [Twitter](https://twitter.com/madrescabreadas). No es obligatorio, pero sí será divertido.

El plazo para participar termina el 30 de junio, y es válido para el territorio español.

¿Lo quieres?

NOTA: Este post no es patrocinado