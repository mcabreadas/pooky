---
date: '2014-05-18T19:05:00+02:00'
image: /tumblr_files/tumblr_inline_pb455bICwg1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- familia
- recomendaciones
- educacion
- testing
- juguetes
title: no title
tumblr_url: https://madrescabreadas.com/post/86122001339/nuestra-experiencia-con-los-juguetes-educativos
---

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/0JsVcw3PwGc?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

**NUESTRA EXPERIENCIA CON LOS JUGUETES EDUCATIVOS MINILAND**

En casa nos gusta trabajar mucho con los niños la electrónica y la robótica, la mecánica, la construcción de bloques, y todo tipo de juegos que les permitan a los niños desarrollar habilidades que les serán útiles para el futuro, independientemente de la profesión que elijan.

Por eso vi una oportunidad muy buena para adquirir estos dos sets de imanes y electricidad que me ofreció Miniland para probar en casa y contaros mi opinión.

Ya habéis visto en el video lo que dan de sí ambos. 

Dejamos un mes de diferencia aproximadamente entre un juguete y otro para que exprimieran al máximo sus posibilidades y los apreciaran cada uno en lo que vale.

El[**MAGNETIC SET**](http://www.minilandeducational.com/magnetic-set/)

Es muy asequible para los niños más pequeños, ya que los imanes son grandes, no hay piezas pequeñas, y el colorido lo hace muy atractivo para los más chicos, incluso Leopardito, de 21 meses, disfrutó haciendo girar uno de los imanes redondos, y moviendo el coche sin tocarlo con simplemente las fuerzas magnéticas.

Pero los mayores también disfrutaron fabricando un muelle de imanes metidos por un eje, de manera que los polos opuestos estuvieran juntos para que se repelieran consiguiendo el efecto muelle.

Este Kit viene perfectamente documetnado con material incluso para profesores , que permite realizar experimentos con los niños para que aprendan las propiedades del magnetismo, “La fuerza invisible”.

En mi opinión, en cada aula debería haber un Kit de este tipo. Os aseguro que los niños son esponjas, y lo que ven y experimentan por ellos mismos se afianza en sus cabezas y lo aprenden para siempre.

 ![](/tumblr_files/tumblr_inline_pb455bICwg1qfhdaz_540.jpg)

El **[ELECTROKIT](http://www.minilandeducational.com/electrokit-88/) **

[**![](/tumblr_files/tumblr_inline_pb455cyESm1qfhdaz_540.jpg)**](http://www.minilandeducational.com/electrokit-88/)

Es una pasada para los niños, y para los mayores también, ya que mi marido disfrutó más que ellos explicándoles la polaridad de las pilas, lo que es un interruptor, un circuito, un motor…

 ![](/tumblr_files/tumblr_inline_pb455dWYss1qfhdaz_540.jpg)

Para este kit sí que es necesario la ayuda y supervisión de un adulto, no porque sea inseguro, ya que en absoluto lo es porque todas la piezas están cuidadosamente pensadas para no suponer ningún peligro, sino porque algunas piezas cuesta encajarlas en la base, (quizá porque el juego todavía está muy nuevo, y con el uso llegue a ser más fácil), y necesidad de explicar y guíar a los niños.

 ![](/tumblr_files/tumblr_inline_pb455eKS8c1qfhdaz_540.jpg)

Por eso el kit viene con unas completas guías en todos los idiomas, con multitud de circuitos y experimentos diferentes para que los papás o profesores no tengan ningún problema a la hora de organizar un buen taller de electricidad.

Nosotros nos estamos planteando ofrecerlo al colegio de mis hijos para organizar algún tipo de actividad por grupos, sobre todo en la clase de Princesita, que ya cursa 2º de Primaria, y no veáis cómo le interesa aprender y montar su propio circuito para hacer funcionar el ventilador, o lucir la bombilla o el LED.

 ![](/tumblr_files/tumblr_inline_pb455e3nCb1qfhdaz_540.jpg)

Ojalá a mí en el colegio me hubieran explicado así los temas de magnetismo o electricidad. Os recomiendo que experimentéis con vuestros hijos.

_NOTA: Este post NO es patrocinad_o. Miniland me ha regalado estos dos juegos para probarlos en casa y contar nuestra experiencia dando mi opinión sincera.