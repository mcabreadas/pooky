---
date: '2018-02-15T12:26:25+01:00'
image: /tumblr_files/tumblr_inline_o7f8gbcY5z1qfhdaz_500.jpg
layout: post
tags:
- derechos
- crianza
- educacion
title: Novedades sobre la admisión de alumnos en Murcia
tumblr_url: https://madrescabreadas.com/post/170902694809/admision-alumnos-murcia
---

Hoy nos levantamos con la sorprendente noticia de que se ha adelantado el proceso de admisión de alumnos 2018/2019 para la Región de Murcia, que se extiende del 5 al 16 de marzo. Además, se acorta bastante la duración del mismo. Así que, si tenemos hijos que vayan a empezar el cole o pasar al Instituto el curso que viene,  debemos estar muy atentas para que no se nos pase el plazo, y ponernos al día cuanto antes de las novedades con respecto al año pasado, que no son pocas.

Ya sabes que este proceso es sólo para los alumnos que vayan a iniciar su vida escolar, aquellos que deseen cambiar de centro. Los que continúan en el mismo, no necesitan hacer este proceso de admisión.

 ![](/tumblr_files/tumblr_inline_o7f8gbcY5z1qfhdaz_500.jpg)
## Necesidad de la firma de ambos progenitores

Cada vez son más frecuentes los casos de niños con padres separados o divorciados e los que se generan discrepancias a la hora de elección de centro. por ello, la Consejería de Educación ha optado por exigir la firma de ambos progenitores en las solicitudes.

Sólo se admitirá una única solicitud por cada alumno, en la que ha de constar las firmas del padre y de la madre, estén o no separados.Cuando se solicite la admisión en un centro educativo y exista discrepancia en relación con la localidad o municipio donde este se ubica, se dará prioridad a la escolarización en la localidad donde reside el progenitor que convive habitualmente con el menor. Si la custodia es compartida o convive por igual con ambos progenitores o tutores legales, se dará prioridad, en primer lugar, al centro en el que el menor tenga hermanos y, en su defecto, al centro más próximo al domicilio de cualquiera de ellos. 

Una vez matriculado un alumno, para proceder a un cambio de centro se requerirá como regla general el consentimiento expreso de ambos progenitores.

## Nuevos canales de comunicación con los padres

Podremos optar por cuatro vías de comunicación para recibir a modo informativo los resultados de los diferentes procesos de baremo y adjudicación: vía web, por mensaje al teléfono móvil, por correo electrónico o a través de la aplicación de mensajería móvil Telegram. 

De esta manera, si lo seleccionamos en la solicitud, al mismo tiempo que los centros publiquen los listados correspondientes, tendremos la información en un click.

## Adelanto del sorteo del apellido de desempate

Cuando varios alumnos tienen la misma cantidad de puntos, y no hay suficientes plazas para todos, se hace un sorteo de entre todas las letras, y los apellidos de la letra ganadora tienen prioridad sobre el resto siguiendo el orden del alfabeto.Para agilizar el proceso se ha adelantado al 1 de marzo el sorteo de letras de apellido de desempate, que las familias conocerán antes de que se produzca la solicitud. Lo cual nos puede ayudar a tomar la decisión del centro a elegir.

## Zona única municipal

Con este cambio se pretende lograr una mayor conciliación de la vida laboral y familiar, ya que se puede optar por un centro que esté en un municipio diferente del de la vivienda familiar, siempre y cuando alguno de los padres trabaje en él.

## Las embarazadas en el cómputo de familia numerosa

Ya desde el pasado curso, Murcia es una de las comunidades, junto a la Comunidad Valenciana, en las que las madres puedan alegar su embarazo para el cómputo de familia numerosa.

## Se tiene en cuenta la madurez de los prematuros

En Murcia junto a con Aragón, Ceuta y Melilla, los progenitores de niños prematuros que vayan a solicitar su escolarización en Infantil, podrán elegir entre la edad legal entre la fecha de nacimiento y la edad corregida según el grado de madurez que presente el niño, es decir pueden matricular a su hijo en el curso que le corresponde por edad o en el que curso que le hubiese correspondido si el embarazo hubiera llegado a término. Con ello se pretende escolarizar a los niños en el nivel que mejor se adapte a sus necesidades, que en ocasiones no coincide con el vinculado a su edad.

Os dejo unos [consejos para que el comienzo del cole](/2015/09/06/adaptacion-colegio.html) de tu peque sea lo más agradable posible y respetuosa. Es mejor que lo vayas preparando poco a poco desde ya!

Para más información sobre el baremo de puntuación y plazos, puedes consultar la web de la [Consejería de Educación](http://www.educarm.es/home).

Suerte!