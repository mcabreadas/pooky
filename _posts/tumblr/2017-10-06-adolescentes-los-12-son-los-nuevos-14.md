---
date: '2017-10-06T07:39:40+02:00'
image: /tumblr_files/tumblr_inline_oxdzq2s68T1qfhdaz_500.jpg
layout: post
tags:
- adolescencia
- familia
title: Adolescentes. Los 12 son los nuevos 14
tumblr_url: https://madrescabreadas.com/post/166100673119/adolescentes-los-12-son-los-nuevos-14
---

Gran parte de la culpa de que la adolescencia se haya adelantado la tenemos nosotros porque la LOGSE adelantó la edad de entrada al Instituto a los 12 años, cuando todavía son muy pequeños.  
Es cierto que en estos tiempos que corren todo va más deprisa, que el acceso a la información es mucho más fácil para los niños gracias a [Internet](/2015/03/21/internet-intimidad-menores.html), que la vida ha cambiado respecto a cuando nosotras jugábamos en la calle al escondite, bebíamos agua de las fuentes y veíamos la Aldea del Arce en la tele mientras merendábamos, pero los adultos hemos validado de algún modo que ellos se sientan mayores antes de tiempo mandándoles el mensaje de que ya son lo suficientemente maduros como para dejar el colegio (que ellos relacionan directamente con la etapa infantil) y pasar al Instituto (que ellos ven como la puerta al un mundo de chicos y chicas jóvenes, independientes, con libertad para hacer lo que quieran…).

 ![niña en columpio](/tumblr_files/tumblr_inline_oxdzq2s68T1qfhdaz_500.jpg)

Ellos tienen prisa, mucha [prisa por crecer](/2013/07/05/preadolescentes.html), por ir adquiriendo cotas de autonomía, parece como si les fuera a faltar tiempo… A menudo intento recordar qué sentía yo a esa edad, cómo era… y al releer [mi diario](https://madrescabreadas.com/2011/07/06/diario-de-una-adolescente-pensamientos-que-se/) he comprendido algunas cosas sobre mis hijos, sólo que yo las viví a una edad más tardía, cuando ya tenía un poquito más de madurez.  
El otro día escuchaba a [Rosa Jové](http://amzn.to/2yav3Cc) decir que los adolescencia que vivan nuestros hijos será fruto de la relación que hayamos tenido con ellos desde la niñez. Nos mandó un mensaje de “que no cunda el pánico”, y la verdad es que me animó a seguir leyendo y formándome para esta nueva aventura que prefiero afrontar con optimismo antes que con miedo, y confiar en mí, en nosotros como familia, y en mis hijos como depositarios de los valores y el amor que les hemos ido dando desde que existen.

 ![rosa jove](/tumblr_files/tumblr_inline_oxdzq28x0o1qfhdaz_500.jpg)

Espero hacerlo lo mejor posible, y que me acompañéis en el camino hacia el maravilloso, inquietante y temido mundo de la adolescencia.  
¿Te vienes?