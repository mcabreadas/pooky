---
date: '2016-05-29T19:16:23+02:00'
image: /tumblr_files/tumblr_inline_o7zas4VwU11qfhdaz_540.png
layout: post
tags: []
title: La rabieta de Disneyland París
tumblr_url: https://madrescabreadas.com/post/145109168829/la-rabieta-de-disneyland-parís
---

![](/tumblr_files/tumblr_inline_o7zas4VwU11qfhdaz_540.png)

Ofú, qué ratito pasamos en la cola de acceso a Disneyland París hace un par de veranos, cuando hicimos ese maravilloso [viaje por Francia que os contaba aquí](/post/96438410679/viajar-con-ni%C3%B1os-sue%C3%B1o-cumplido-llegamos-a). 

Leopardito tenía entonces 2 años y, de verdad, que para mí no fueron los típicos “terribles dos” que cuenta la gente, ya que siempre ha sido un niño “dócil”, cariñoso, “razonable”, y tranquilo en ese sentido.

Pero se ve que pensó: “como nunca he hecho una rabieta, pues voy a montar una que haga historia”.

 ![](/tumblr_files/tumblr_inline_o7zas50uJP1qfhdaz_540.jpg)

Fíjate cómo fue, que han pasado casi dos años, y todavía me acuerdo para contártela.

## La situación fue escalofriante

Puedo decir que todos los temores que pudieran imaginar  unos padres a dar el espectáculo en público se concentraron en una sola escena. (Aviso de que pediré derechos si aparece en alguna película algo similar a lo que voy a contar).

Ni en mis más oscuras pesadillas había soñado nunca con algo así. Menos mal que al ser el tercer hijo, una se curte, sobre todo en capacidad para dar el espectáculo/hacer él ridículo/ser el centro de las miradas/ser señalada con el dedo… porque creo que si hubiera pasado con la primera, todavía me duraría el trauma.

## La escena es como sigue… 

- Mi marido y yo con los tres niños en la cola para pasar por las taquillas de Disneyland París.  
- Mes de julio.  
- Vacaciones escolares en Francia, España, Andorra…y en el resto de Europa. Abarrotamiento extremo.  
- Niño con fiebre, virus del estómago, hambre y sueño.  
- Sillita de paseo diferente a la que solía usar por aquello de ser más ligera (maldita decisión).

## Se masca la tragedia…

Nos acercamos a la taquilla, y justo cuando nos quedan tres personas delante Leopardito dice que tiene sueño. Le invitamos a subir a la sillita ligera y se le cae el mundo encima porque está cansado, enfermo, tiene hambre, y no es su silla de siempre.

Avanzamos un puesto más en la cola…

## La explosión

De repente se hace cargo de la situación, comprende que no va a dormir en su cama ni en su silla favorita , ni en la silla del coche, que le encanta y, justo cuando llega nuestro turno explota en llanto.

Mientras mi marido gestiona las entradas, yo trato de calmarlo sin éxito. Entonces la rabia se apodera de él y se tira al suelo bajo la atenta mirada de la multitud.

## A ver quién lo levanta del suelo

Terminamos con las entradas e intentamos salir de allí para dejar paso a la siguiente familia de la cola, pero no había manera de moverlo. Cuanto más lo intentaba más se enconaba.

Empezamos a hacer tapón. La situación empieza a sobrepasarme (a mi marido ya le había sobrepasado hace rato).

Yo no pierdo la calma y le hablo tranquila, pero no atiende a razones.

## Las miradas de la gente

La gente empieza a murmurar. Nos lo llevamos como podemos de la taquilla para dejar libre el paso, y nos retiramos a un “discreto” segundo plano.

Sigue sin ser capaz de calmarse.

Entonces, más tranquila, me agacho para ponerme a su altura, le cojo las manos, sigo hablándole pausadamente y con tono suave, pero no logro nada.

Intento abrazarle, pero no me deja. 

## Una llamada a la calma familiar

Hago una llamada a la calma familiar porque a sus hermanos mayores les cuesta comprender. Todos estamos nerviosos y sobrepasados, pero no debe cundir el pánico. En algún momento se calmará.

## Con paciencia y amor

Y así, comprendimos y esperamos con paciencia a que se fuera calmando hasta que, después de un buen rato, logró serenarse un poco, entonces aproveché para abrazarlo y tomarlo en brazos hasta que se terminó de serenar y se quedó dormido.

Lo puse en la sillita y durmió varias horas, y todavía se le escuchaban suspiros de sollozo de vez en cuando entre sueños.

 ![](/tumblr_files/tumblr_inline_o7zas5aFGF1qfhdaz_540.jpg)

La verdad es que ha sido la única vez que ha cogido una rabieta importante, pero ésta vale por mil. 

Cuando despertó pasamos un día precioso y mágico en familia en Disneyland París… dicen que todo lo bueno cuesta.

 ![](/tumblr_files/tumblr_inline_o7zas66EzY1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_o7zas6bzmU1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7zas7Yzlm1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7zas7Gfs61qfhdaz_540.png)

Seguro que tú también tienes una rabieta antológica que contar y que no olvidarás nunca.