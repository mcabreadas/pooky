---
date: '2015-01-20T12:59:00+01:00'
image: /tumblr_files/tumblr_inline_ni8etvPMOw1qfhdaz.jpg
layout: post
tags:
- cine
- premios
- planninos
- colaboraciones
- ad
- eventos
- familia
title: Asistimos al preestreno de la película Annie. Y sorteo de pack 4 entradas
tumblr_url: https://madrescabreadas.com/post/108637105989/preestreno-pelicula-annie-sorteo-entradas
---

![image](/tumblr_files/tumblr_inline_ni8etvPMOw1qfhdaz.jpg)

ACTUALIZACIÓN 1-2-15: RESULTADO SORTEO

Escuchando a mi Princesita cantar mañana, mañana… con su preciosa voz acabo de realizar el sorteo, y tengo el gusto de anunciar que la ganadora es…

 ![](/tumblr_files/tumblr_inline_nj3x5d3x0I1qfhdaz.jpg)

Irene Cid!!!! Enhorabuena, guapísima! Me alegro mucho, y espero que lo paséis pipa en el cine viendo la peli, igual que lo pasamos mi familia y yo, incluído el pequeñajo, que no paraba de decir: 

“la nena no está triste…está cantando”.

Espero tu email por privado para enviarte los códigos para las entradas, Irene.

A todos los demás participantes, os doy las gracias por vuestros comentarios. Me han encantado porque salían del corazón.

Hasta…. mañana, mañana…

_Post original:_

¿Quién no tarareó de niña “mañana, mañana, te quiero mañana…”?

Nuestra generación creció con la película Annie, que celebró su 30 aniversario en 2012, y que ahora [Sony Pictures](http://www.sonypictures.com/movies/annie/?hs317=home_BB_annie) ha vuelto a traer al cine en versión moderna, la que tuvimos la suerte de ver en su pre estreno de la mano de Patricia, del blog [Dando Color a los Días](http://www.dandocoloralosdias.com/) ( blog que os recomiendo cien por cien por la sensibilidad con la que su autora lo escribe y el corazón que pone en cada entrada).

![image](/tumblr_files/tumblr_inline_nif9uy3AaH1qfhdaz.jpg)

Ahora, la vivaracha Annie ha cambiado sus rizos pelirrojos por otros negros, sigue bailando y cantando conquistando a quienes la escuchan, doblada en castellano por la cantante [María Parrado](/2014/11/20/sorteo-entradas-maria-parrado-lavoz.html), de la que ya hablamos en el blog no hace mucho.

En este video vemos la Annie antigua, de 1982 (yo tenía 5 añitos):

<iframe src="//www.youtube.com/embed/-0bOH8ABpco" width="100%" height="500" frameborder="0"></iframe>

Y en este otro os presento el trailer de la nueva película, que se estrenará el 30 de enero en cines:

<iframe src="//www.youtube.com/embed/SYdOU-ykzYE" width="100%" height="315" frameborder="0"></iframe>

Patricia asistió al pase privado al que Sony Pictures tuvo la gentileza de invitarnos, y así es cómo nos lo cuenta:

_“El domingo amaneció un día gélido aquí en la capital, y aunque eso de levantarnos a golpe de despertador un domingo en mi casa lo llevamos muy mal, como era por una buena causa, las caras eran de ilusión durante el desayuno, ¡ y es que ir a un pre-estreno en esta casa tan cinéfila siempre es motivo de alegría! ¡Y además a un musical! ¡ Las peques mayores estaban emocionadas!_  
  
_Poco antes de las once menos diez llegábamos al edificio de Sony Pictures para un pase privado como colaboradora del blog [Madres Cabreadas](https://www.tumblr.com)._

## La primera impresión

_Nada más llegar al edificio y traspasar la puerta de entrada, nos recibieron un agradable olor a palomitas, junto con Juan y la simpática Silvia, con el photocall listo y unos refrescos mientras nos observaban desde las alturas el gran Tintín y la imagen de Spiderman._

_![image](/tumblr_files/tumblr_inline_nifaaqPb1c1qfhdaz.jpg)_

![image](/tumblr_files/tumblr_inline_nifab8C1G61qfhdaz.jpg)

_Fuimos las primeras en llegar, y eso nos permitió aclimatarnos, hacernos las correspondientes fotos e ir viendo llegar al resto de compañeros de mañana. Los siguientes en llegar fueron la gran Eva Quevedo de[Blog de Madre](http://blog-demadre.com/), con sus peques, y luego ya fue un pequeño goteo de gente mientras mis hijas se preguntaban nerviosas cómo sería la sala y dónde nos tendríamos que sentar._

## La sala

_Cuando unos minutos después nos dijo Silvia que podíamos ir pasando y acomodarnos, cogimos unas palomitas y unos refrescos y entramos en una sala preciosa, de esas que a cualquier amante de cine le gustaría poder tener en su casa… Las butacas  eran cómodas no, comodísimas, y casi para dos personas, para que os hagáis  una idea de lo “cuqui” del lugar… Ya estábamos espectantes deseando ver la película._

![image](/tumblr_files/tumblr_inline_nih08tATqN1qfhdaz.jpg)

_Foto gracias a http://blog-demadre.com/_

## La película

_¿ Qué os puedo contar de ella sin que me acuséis de hacer spoiler?_

## La trama

_Pese a ser dura ( hay temas que siendo madre son difíciles de ver y vivir para mí) y ya conocerla , está muy bien llevada, (así es que si sois de lágrima sensible no olvidéis ni los tissues ni os pongáis rímel si no es waterproof, advertidos quedáis)._

## La música

_Te llega al corazón en la mayoría de las canciones, y aunque son versiones nuevas, te hacen meterte en ellas sin mayor problema ni añoranza, y la voz de María Parrado está muy bien, a mis peques les ha encantado._

## La ambientación

_Nueva York siempre es una delicia._

## La mala

_Ver a Cameron Díaz de mala-malísima merece la pena._

_En conclusión, creo que es un fantástico film para ver en familia y disfrutar, aunque cuidado que luego las canciones os acompañarán durante mucho rato._  
_Si queréis ver una película con final feliz, ésta es la vuestra, ya que, además recoge distintos tipos de valores y apuesta por los que al final merece la pena vivir._

_¿Que cuáles son?_

_Ya dije que no iba a hacer spoiler, así es que acércate a verla._  
  
_Hasta aquí mi pequeña colaboración con el blog Madres cabreadas, y agradecerle tanto a su autora como al personal de Sony Pictures España la invitación a este fantástico pre-estreno de Annie._  
  
_¡Besotes de DandoColorALosDías!”_

Agradezco enormemente el esfuerzo que hicieron Patricia y su familia por asistir, ya que la menor de sus hijas estaba enferma, y por supuesto la invitación de Sony Pictures, y la oportunidad que nos ha dado de contaros en esta casa el pre estreno de una peli que llega al corazón a grandes y pequeños.

## Cómo participar en el sorteo

Queremos compartir con vosotros la experiencia Annie, por eso Sony Pictures nos ha cedido para sorteo un pack familiar de 4 entradas para asistir a ver la película en circuitos de Cinesa y Ocine a partir del día 30 de enero.

Para conseguirlo sólo tienes que:

1-Dejar un comentario en este post explicando por qué quieres ver la nueva película de Annie.

2-Compartir el sorteo en Twitter o Facebook (acordaros de seleccionar la opción de público para compartir), e indicarnos dónde lo has compartido, y tu nick.

También puedes, aunque no es obligatorio para participar, suscribirte al blog para estar al día de las novedades. Tienes esta opción en el margen derecho.

El sorteo se hará por medio de Sortea2, y el plazo termina el 30 de enero de 2015.

El ganador se publicará en este blog en los días sucesivos.

El ámbito del sorteo es el territorio español.

¿Qué me dices? ¿Te apetece ver Annie?

![image](/tumblr_files/tumblr_inline_nih55i1y1E1qfhdaz.jpg)

_NOTA: Post no patrocinado_