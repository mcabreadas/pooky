---
date: '2013-06-21T13:37:00+02:00'
image: /tumblr_files/tumblr_inline_moqomgi3eJ1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- postparto
- recomendaciones
- familia
- mujer
- bebes
title: Cuidado del cuerpo en el post parto. Prueba de reafirmante e hidratante Skin
  Method
tumblr_url: https://madrescabreadas.com/post/53510550075/cremas-postparto
---

Por fin me llegó el lote de productos de Skin Method para probar. Me hacia especial ilusión participar en el test, primero porque es la primera vez que me meto en un lío de estos, y segundo porque me encantan los potingues (en palabras de mi esposo), es decir, que me chiflan las cremas, sobre todo si son de calidad y me dan resultado.  
 Se trata de la Hidratante Corporal 24 horas, y la Reafirmante Corporal, y me piden que las valore según los siguientes parámetros:

La Hidratante Corporal 24 horas:

 ![image](/tumblr_files/tumblr_inline_moqomgi3eJ1qfhdaz.jpg)

-Resultados (firmeza y elasticidad): La piel se queda hidratada, suave y elástica. Yo, que la tengo seca, y la he usado tras la piscina, he comprobado que dura bastante su efecto.   
 Tendría que probarla más a largo plazo para formarme una opinión más sólida en cuanto a la firmeza.  
 Mi puntuación el 4/5

-Formato adecuado para su uso: Me encanta el dosificador porque es muy cómodo y suave (no se atasca, en principio), también tiene una posición de seguridad para que no salga el producto por accidente.  
 El color negro del envase me parece elegante y da la impresión de un producto de gama alta.  
 Le doy un 5/5

-Aroma: El aroma no me convence. Prefiero olores suaves o, mejor, hidratantes inoloras, ya que me molesta un olor muy concreto. Aunque he de decir que se va enseguida.  
 Mi puntuación es de un 3/5

-Textura: Es ideal porque es ligera y nada untuosa, vamos, que no pringa, se absorbe rápidamente y no mancha la ropa que te pongas encima, aunque te vistas enseguida.  
 Le doy un 5/5

-Relación calidad/precio: El precio es de 14,90 €, y lo veo adecuado para lo que se ofrece, quizá un poco por encima de la media de productos similares. Tendría que probarla más a largo plazo para formarme una opinión más sólida.  
 Mi puntuación es 3/5.

Crema Reafirmante Corporal:

 ![image](/tumblr_files/tumblr_inline_moqpakkkQP1qfhdaz.jpg)

-Resultados (firmeza y elasticidad): La piel se queda hidratada, suave y elástica.   
 Tendría que probarla más a largo plazo para formarme una opinión más sólida en cuanto a la firmeza, ya que después de tres embarazos, y el último hace menos de un año, hay mucho trabajo por hacer para recuperar la firmeza, pero promete.  
 Mi puntuación es 3/5

-Formato adecuado para su uso: Me encanta el dosificador porque es muy cómodo y suave (no se atasca en principio), también tiene una posición de seguridad para que no salga el producto por accidente.  
 El color negro del envase me parece elegante y da la impresión de un producto de gama alta.  
 Le doy un 5/5

-Aroma: El aroma no me gusta porque uno de los sitios donde se aplica es en los pechos, y si estás con la lactancia materna, al bebé puede molestarle el olor (aunque no se aplica en el pezón ni areola, pero se nota bastante). Prefiero cremas inoloras para el pecho. Aunque he de decir que se va enseguida.  
 Mi puntuación es de un 2/5

-Textura: Es ideal porque es ligera y nada untuosa, vamos, que no pringa, se absorbe rápidamente y no mancha la ropa que te pongas encima, aunque te vistas enseguida.  
 Me gusta el color rosa para no confundirse con la hidratante en un momento de despiste.  
 Le doy un 5/5

-Relación calidad/precio: El precio es de 24,80 €, y lo veo adecuado para lo,que se ofrece, quizá un poco por encima de la media de productos similares. Tendría que probarla más a largo plazo para formarme una opinión más sólida.  
 Mi puntuación es 3/5.