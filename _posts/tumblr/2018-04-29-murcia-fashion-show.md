---
date: '2018-04-29T11:01:07+02:00'
image: /tumblr_files/tumblr_n3ohtsJXRU1sr3hrno1_500.gif
layout: post
tags:
- colaboraciones
- local
- ad
- eventos
- moda
title: Talento en el Murcia Fashion Show
tumblr_url: https://madrescabreadas.com/post/173410717849/murcia-fashion-show
---

Que en Murcia hay mucho talento ya te lo había dicho muchas veces, pero es que no me deja de admirar todo lo que aún tengo por conocer y mostrarte.

Si algo nos han traído las redes sociales es que afloren todas las creaciones de diseñadores, escritores, artesanos y artistas murcianos, y una verdadera eclosión de cultural en la Región.

 ![](/tumblr_files/tumblr_n3ohtsJXRU1sr3hrno1_500.gif)

Eventos como al que asistí ayer en el Museo de Bellas Artes de Murcia organizado por [Murcia Fashion Show](http://murciafashionshow.com/) permiten que esa creatividad de nuestros diseñadores se muestre al resto del mundo quedando al nivel de otros ya consolidados.

 ![](/tumblr_files/tumblr_inline_p7xr8ieGJe1qfhdaz_540.png)

Vimos originalidad y frescura en [Peyres](http://peyres.bigcartel.com/), 

![peyres_disenadora_vestido_fluor](/tumblr_files/tumblr_inline_p7xvqc2oAX1qfhdaz_540.gif)

puro romanticismo y delicadeza en [Cayetana Ferrer](https://www.cayetanaferrer.com/), 

![cayetana_ferrer_desfile_novias](/tumblr_files/tumblr_inline_p7xvqcimL41qfhdaz_540.gif)

atrevimiento en colecciones arriesgadas como [Aliaga](http://www.aliagabyaliaga.com/) (que ya os la mostré en su [evento de presentación de su nueva colección Ari Aliaga](/2018/03/18/desfile-aliaga-murcia.html) hace unas semanas), incluso osadía en [Abel Esga](https://www.instagram.com/abelesga/), que nos presentó a una mujer fuerte, guerrera y con personalidad. 

![abel_sga_estola](/tumblr_files/tumblr_inline_p7xvqdcOIs1qfhdaz_540.gif)

Además de la siempre chispeante (pero esta vez aún más) [Agatha Ruiz de la Prada](https://www.agatharuizdelaprada.com/store/es/), que transmite un buen rollo increíble con sus diseños, y a la que tuve el placer de conocer en una [edición de FIMI (Feria Internacional de Moda Infantil) de Madrid](/2016/01/27/fimi-moda-infantil.html), pero nunca la había visto desfilar con moda de mujer.

 ![agatha_ruiz_fimi](/tumblr_files/tumblr_inline_o39ypePqxS1qfhdaz_540.jpg)

![agatha_ruiz_prada_vestido_mangas](/tumblr_files/tumblr_inline_p7xvqeTLWU1qfhdaz_540.gif)

Te aseguro que ningún pase dejó indiferente al públido, y también tuvieron cabida los jóvenes diseñadores murcianos que a modo de maniquí humano tuvieron la oportunidad de exhibir sus creaciones durante el kissing bar para nuestro deleite (creo que fueron los más fotografiados de la jornada).

 ![](/tumblr_files/tumblr_inline_p7xut7uFix1qfhdaz_540.png)

El entorno no me pudo gustar más. El patio del museo de bellas artes me pareció un acierto por las dimensiones que tiene en cuanto a longitud y sobre todo anchura para permitir disfrutar de una pasarela cómoda para el espectador y lucida para los creadores.

 ![](/tumblr_files/tumblr_inline_p7xutxlGR41qfhdaz_540.png)

La organización me consta que fue bastante precipitada, y aún así no tuvo nada que envidiar a los [desfiles de moda infantil de Madrid](/2017/04/17/desfile-moda-infantil-primavera.html) a los que he asistido, que os he contado y que ya conocéis por el Blog. Lo único que mejoraría es la logística entre pase y pase y, ya que para quien va a verlos todos es un poco incómodo tener que salir del recinto y volver a entrar cada vez. Pero por lo demás, se nota el esfuerzo por el trabajo bien hecho y el cariño que se ha puesto en cada detalle. Mi enhorabuena!

![entrevista_television_mujer](/tumblr_files/tumblr_inline_p7xvqglZ651qfhdaz_540.gif)

Creo que en la próxima edición durará 4 días y se impartirán también talleres y conferencias, y me desde aquí me permito la osadía de sugerir que se dedique una jornada a la moda infantil y juvenil.

Brillante iniciativa y que sean muchas más!