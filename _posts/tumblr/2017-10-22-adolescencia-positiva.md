---
date: '2017-10-22T12:25:30+02:00'
image: /tumblr_files/tumblr_inline_oxnomtIdBa1qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- familia
title: Mejor, una adolescencia positiva
tumblr_url: https://madrescabreadas.com/post/166668581259/adolescencia-positiva
---

Prácticamente desde que nacen nuestros hijos, sino antes, empezamos a temer la adolescencia de un modo visceral. No sé si porque recordamos cómo se lo hicimos pasar a nuestros padres en esta etapa de nuestra propia vida, o porque todas las frases que escuchamos sobre la misma contienen una palabra peyorativa relativa al adolescente (la propia palabra “adolescente” es fea).

Yo, sinceramente, prefiero no demonizar la adolescencia porque creo mucho en eso que ocurre a veces de la “profecía auto confirmada”, y no vaya a ser que por tanto decir luego se vaya a cumplir todo lo malo y nuestra casa se convierta en un infierno.

En vez de eso, prefiero [afrontar la adolescencia de forma positiva](/2017/10/06/adolescentes-los-12-son-los-nuevos-14.html) y tomar medidas preventivas, antes de que sea demasiado tarde. Medidas que autores como [Rosa Jové](http://amzn.to/2yav3Cc) ya explican cuando hablan de la crianza desde los primeros meses de vida. Y es que no podemos pretender tener una adolescencia apacible si no los hemos educado para ello desde el principio.

Por ejemplo, creo que si siempre hemos estado apegados a nuestros hijos, hemos sido cariñosos con ellos, les hemos protegido y amparado siempre que nos han necesitado, y escuchado cuando han querido contarnos lo que han hecho en el cole, o lo que les ha molestado de su amiguito, esto no tiene por qué cambiar radicalmente a partir de los 12 años. 

 ![](/tumblr_files/tumblr_inline_oxnomtIdBa1qfhdaz_540.jpg)

Que no digo yo que vaya a ser igual, pero el vínculo que hayamos establecido con ellos es el que permanecerá siempre y sabrán que pueden contar con nosotros (aunque haya momentos en los que sientan que nos odian por haberles confiscado el móvil).

Por contra, si siempre hemos estado pendientes de mil cosas y no les hemos escuchado cuando han venido a enseñarnos el último dibujo que han hecho porque nos interrumpían una conversación de adultos, o porque estábamos mirando el móvil, luego, cuando crezcan, los que no nos van a hacer ni caso van a ser ellos, no crees?

Así que te animo, si estás como yo, afrontando esta nueva etapa en casa, a que pienses en positivo y en lo bueno que tiene tu hijo, lo acompañes y te cargues de toneladas de paciencia. 

¿Te apuntas a la adolescencia positiva?

 ![](/tumblr_files/tumblr_inline_oxnogm0vev1qfhdaz_540.jpg)