---
date: '2016-05-26T11:21:14+02:00'
image: /tumblr_files/tumblr_o7op5eWq7t1qgfaqto1_400.gif
layout: post
tags:
- trucos
- colaboraciones
- ad
- eventos
- recomendaciones
- familia
- gif
- moda
title: verano fresquito con zippy uno de los mejores días
tumblr_url: https://madrescabreadas.com/post/144949339369/verano-fresquito-con-zippy-uno-de-los-mejores-días
---

![](/tumblr_files/tumblr_o7op5eWq7t1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_o7op5eWq7t1qgfaqto2_400.gif)  
 ![](/tumblr_files/tumblr_o7op5eWq7t1qgfaqto3_400.gif)  
 ![](/tumblr_files/tumblr_o7op5eWq7t1qgfaqto4_400.gif)  
  

## Verano fresquito con Zippy

Uno de los mejores días que he pasado junto a mis compañeras bloggers, sin duda, fue el día que [Zippy nos presentó su colección de ropa infantil para verano 2016](https://es.zippykidstore.com) en un evento cargado de frescura y buen gusto, donde el cariño se desprendía de cada detalle. 

 ![](/tumblr_files/tumblr_inline_o7sff5NN5j1qfhdaz_540.jpg)

Helados y fruta fueron los protagonistas en un entorno precioso, como es el jardín de El Ángel, en pleno corazón madrileño.

Las chicas de Zippy nos recibieron con los brazos abiertos y la amabilidad a la que nos tienen acostumbradas para enseñarnos con detalle las prendas de la nueva colección donde priman la calidad de los tejidos, elegidos especialmente para la comodidad y la frescura de los niños, y el cuidado de los detalles y terminaciones. 

## Los prints

Los diseños, especialmente bonitos, algunos clásicos, con un toque moderno muy de Zippy, y los prints originales de animales, topos, o estrellas, me conquistaron.

 ![](/tumblr_files/tumblr_inline_o7sff66Fb61qfhdaz_540.jpg)
## Línea brothers and sisters

La línea Brothers and Sisters para que los hermanos vayan coordinados a la playa me encantó porque abarca todas las tallas, y no necesariamente tienen que ir iguales, sino a conjunto (el sueño de toda madre de familia numerosa).

 ![](/tumblr_files/tumblr_inline_o7sff6Fodl1qfhdaz_540.jpg)
## Los zapatos

Los zapatos me parecieron cómodos y originales. Tienen una gran variedad, a cual más bonito. La piel y la loneta son los protagonistas. 

**Para niño** (aunque hay algunos unisex, como puedes comprobar):

 ![](/tumblr_files/tumblr_inline_o7sff7rZm91qfhdaz_540.jpg)

**Para niña** :

 ![](/tumblr_files/tumblr_inline_o7sff7mjVC1qfhdaz_540.jpg)
## Los bañadores

Los bañadores estampados con helados, frutas, o con colores pastel o los clásicos de rayas son ideales. 

 ![](/tumblr_files/tumblr_inline_o7sff7RO6B1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sff8pLxl1qfhdaz_540.jpg)
## Las camisetas

 Las camisetas, algunas con frases inspiradoras, se pueden combinar con cantidad de shorts o Bermudas. 

 ![](/tumblr_files/tumblr_inline_o7sff8tkv41qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sff980nT1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sff9lYjJ1qfhdaz_540.jpg)
## Pantalones y Bermudas

También encontré tirantes esta vez, (ya os conté [aquí](/2016/03/20/chalecos-y-tirantes-con-zippy-nos-vamos-de-boda.html)que nos encantan) Pero esta vez en su versión más veraniega, y con pantalón corto, para combinar con camisa blanca con botones en madera de coco (los detalles son la clave de esta colección). 

 ![](/tumblr_files/tumblr_inline_o7sff9pxoE1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sffaPMBo1qfhdaz_540.jpg)

El clásico polo azul marino, pero con un toque diferente, que se lo da el cuello mao, combinado con bermudas de  rayas azul clarito, queda ideal.

 ![](/tumblr_files/tumblr_inline_o7sffaZByX1qfhdaz_540.jpg)

Este año Zippy vuelve a apostar por el rosa, pero con un tono más suave, el llamado rosa empolvado, en unas bermudas que se presentan más cortas que otras temporadas.

 ![](/tumblr_files/tumblr_inline_o7sffbiCo61qfhdaz_540.jpg)
## Para teens

Me quedaría con estos conjuntos sin dudarlo.

 ![](/tumblr_files/tumblr_inline_o7sffbVUgZ1qfhdaz_540.jpg)
## Para bebés

Y para bebé, las ranitas y cubre pañales que os muestro me encantaron: 

 ![](/tumblr_files/tumblr_inline_o7sffbIgWp1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sffcLyd11qfhdaz_540.jpg)
## La decoración

La decoración era muy divertida, al final terminamos quitando los polos gigantes y haciéndonos fotos con ellos, y los helados de cartulina, tengo que aprender a hacerlos porque me pirraron. 

 ![](/tumblr_files/tumblr_inline_o7sffcv0AS1qfhdaz_540.jpg)

Y el catering y el puesto de frutas, si digo que fue delicioso, me quedo corta. Felicidades al cocinero! 

 ![](/tumblr_files/tumblr_inline_o7sffdowtd1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7sffdbXU51qfhdaz_540.jpg)

Bueno, felicidades a todos, la verdad, también a la decoradora y a la masajista de [Tao Quiromasaje](https://www.facebook.com/TaoQuiromasajeTorrelodones/info/?tab=overview), porque junto con las chicas de [Zippy](https://es.zippykidstore.com) me hicieron pasar una de las mañanas más divertida con mis compañeras 

¡Gracias!

 ![](/tumblr_files/tumblr_inline_o7sffd6DbB1qfhdaz_540.jpg)