---
layout: post
title: El ańo pasado por estas fechas
date: 2011-06-30T19:12:00+02:00
image: /images/uploads/depositphotos_82056330_xl.jpg
author: maria
tags:
  - mecabrea
  - cosasmias
  - embarazo
tumblr_url: https://madrescabreadas.com/post/7086144974/el-ańo-pasado-por-estas-fechas
---
El ańo pasado por estas fechas no os conocía, ni tenía a nadie con quien hablar de conciliación, maternidad… como lo hago con vosotras.

El ańo pasado por estas fechas yo estaba embarazada. Mi tercer embarazo. Estaba ilusionada y feliz… todo según mis cálculos, como los dos anteriores, en los que conseguí que, incluso, nacieran casi el mismo día (con dos ańos de diferencia).

Entonces aprendí una lección. Creo que la más importante de mi vida. La vida no la controlo yo. Aunque a veces pueda llegar a tener esa percepción, es falsa, se trata de una ilusión que nos hacemos, y que no tarda en darnos de bruces con la realidad.

Es ahora, con vosotras cuando estoy empezando a hablar de ello. Antes no podía ni mencionarlo sin que se me hiciera un nudo en la garganta. Poco a poco me salen cosas en algún tuit o comentario en facebook, y me alegra, porque lo hago con naturalidad, y eso me ha ayudado a mencionarlo en mi TL de la vida real con la misma espontaneidad, y me alegro. Me alegro porque no lo había sacado aún, era algo que estaba ahí dentro, sin hacer mucho ruido…

Quiero daros las gracias por vuestra amistad virtual, que está llegando a ser más importante que muchas amistades reales. Hoy me siento tranquila, un poco más liberada y esperanzada porque, tarde o temprano, [la vida me dará otra oportunidad](https://madrescabreadas.com/2013/05/20/las-horas-contigo-contigo-las-horas-se-llenan-de/)… 

Y si no, de todos modos debo estar muy agradecida por las dos joyas que tengo y por mi experiencia de la maternidad en todo su esplendor.

\
 Hoy no estoy cabreada…estoy un poco más liberada.

*Foto gracias a [sp.depositphotos](https://sp.depositphotos.com)*