---
date: '2016-04-17T18:14:45+02:00'
image: /tumblr_files/tumblr_inline_o3vfq1nbaJ1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- maternidad
- regalos
- colaboraciones
- bebe
- ad
- joyas
- eventos
- recomendaciones
- mujer
title: Joyas para mamá y bebé en el día de la madre
tumblr_url: https://madrescabreadas.com/post/142956119649/joya-madre-bebe
---

Cuando nace un bebé todos los regalos suelen ser para él. Y las madres nos alegramos, no te vayas a creer, pero no estaría de más echar la vista a un lado y ver a la mujer que lo ha traído al mundo, y que entra en una de las etapas más importantes y difíciles de su vida, como es la maternidad.

## Orgullosa de ser madre

Yo me siento muy #orgullosadesermadre, por eso me ha encantado el lema de [Le bebé](http://www.lebebegioielli.com/), una firma de joyas especializada en mamás y bebés, que entiende la maternidad como valor, como logro, porque cuando nace un bebé, nace una madre.

Recientemente asistimos a la presentación de su nueva colección para contarte las novedades, y para que puedas hacer tu wish list para el día de la madre.

Al evento asistió Blanca Crespo, del blog [Ohlala Popelinas](http://ohlalapopelinas.blogspot.com.es/), quien estuvo charlando con la embajadora de la marca, Astrid Klisans, que espera un bebé junto a su marido Carlos Baute.

 ![](/tumblr_files/tumblr_inline_o3vfq1nbaJ1qfhdaz_540.jpg)

Blanca nos cuenta que presentaron dos colecciones, la de madres y la de niños, y que todo es alta joyería, ya que trabajan con materiales de primera calidad como son el oro y los brillantes:

 ![](/tumblr_files/tumblr_inline_o3vfedaG0u1qfhdaz_540.jpg)

> De la colección de niños me lo habría quedado absolutamente todo por su colorido y por mi deformación profesional (soy profesora de infantil y me lanzo a todo aquello que tiene color).

 ![](/tumblr_files/tumblr_inline_o3ve4e8dZb1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3ve58SbRF1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3ve5ljnbh1qfhdaz_540.jpg)

> Pero es que de la colección de madres también lo habría cogido todo. Había figuritas de niños y niñas, cascabeles con forma de chupete, pulseras con charms y todo ello customizable a tu gusto.

 ![](/tumblr_files/tumblr_inline_o3ve43YCbg1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3ve5qqKMS1qfhdaz_540.jpg)

Todas las joyas son personalizables con el nombre o con la frase que quieras.

¿No me digas que no es un bonito detalle, incluso para que mamá y bebé vayan a juego?

Me parece un regalo ideal para una futura mamá en el día de la madre.

 

¿A ti no?