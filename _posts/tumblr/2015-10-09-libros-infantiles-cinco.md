---
date: '2015-10-09T13:00:44+02:00'
image: /tumblr_files/tumblr_inline_o39zavjwBf1qfhdaz_540.jpg
layout: post
tags:
- trucos
- adolescencia
- libros
- planninos
- recomendaciones
- familia
title: 'Libros infantiles: vuelven Los Cinco'
tumblr_url: https://madrescabreadas.com/post/130806900979/libros-infantiles-cinco
---

Quién no se ha leído en su niñez un libro de Los Cinco donde se disfrutaba de la intriga saboreando cada capítulo, y deduciendo poco a poco quién podía ser el “malo”, o cuál podría ser la explicación del misterio.

Quién no se ha imaginado que era uno de los personajes, Jorge, Ana, Julián Dick, o incluso Tim, metiéndose en cada aventura.

Y a quién no se le hacía la boca agua cuando sacaban la cesta con la merendola, y la escritora Enid Blyton daba todo lujo de detalles de cada manjar que contenía, incluídos unos postres que te obligaban a ir a pedir a mamá una merienda para ti también.

 ![](/tumblr_files/tumblr_inline_o39zavjwBf1qfhdaz_540.jpg)

Bueno, pues ahora Los Cinco han vuelto más modernos que nunca, con ilustraciones adaptadas a los tiempos que corren, más llamativas, dinámicas y expresivas. Y el texto combina varios tipos de letras para captar más la atención de unos lectores que ahora son más difíciles de seducir por la cantidad de estímulos a los que están sometidos a diario, y a quienes les gustan las respuestas rápidas y la acción.

En casa hemos leído [Los Cinco y el Gran Enigma](http://www.boolino.es/es/libros-cuentos/los-cinco-y-el-gran-enigma/), una historia que mantiene el misterio hasta el final, y que lleva de nuevo a los protagonistas a la Isla de Kirrin, donde una luz en la noche alerta a Jorge de que algún intruso se encuentra en ella.

 ![](/tumblr_files/tumblr_inline_o39zaww5uH1qfhdaz_540.jpg)

Pero no sólo está lo de la luz, otras cosas extrañas comienzan a suceder.

De la despensa desaparecen las galletas de Tim, naranjas, uvas…, y cuando acuden a la isla ocurre un extraño incidente con la chancla de Ana.

El misterio está servido, y los Cinco no pararán hasta desvelar el gran enigma.

A mis hijos les ha encantado y, aunque la edición es muy diferente (he rebuscado en casa de mis padres, y he encontrado mi colección), creo que es muy acertada porque logra captar la atención de los niños de hoy, que son muy diferentes a los que fuimos nosotros en su día.

 ![](/tumblr_files/tumblr_inline_o39zaxCSo51qfhdaz_540.jpg)

¡Me encanta que lean lo mismo que leía yo!

¿Y a ti?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.