---
date: '2016-09-18T18:00:16+02:00'
image: /tumblr_files/tumblr_inline_odbvohzhv61qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- bebe
- puericultura
- ad
- recomendaciones
- testing
title: 'Prueba extrema Bébécar III: cuestas empinadas'
tumblr_url: https://madrescabreadas.com/post/150589220694/carrito-bebe-resistente-bebecar
---

Lo sé, lo sé, estábais ansiosas por ver cuál iba a ser la tercera de las pruebas extremas a las que estamos sometiendo a nuestro cochecito de bebé Ip Op Bébécar, con capazo XXL.

Después de haber superado con éxito la [Prueba extrema I: “Aglomeraciones”](Coches%20para%20beb%C3%A9s%20B%C3%A9b%C3%A9car.%20Prueba%20extrema%20I:%20Aglomeraciones), y la [Prueba extrema II: “Calor y arena”](/2016/08/26/cochecito-bebecar-playa.html), no podíamos dejar de someter al carrito de mi sobrina Chiquita a las cuestas empinadas de [Moratalla](https://es.wikipedia.org/wiki/Moratalla), pueblo de Murcia precioso, pero con un casco antiguo cuya visita te pone los gemelos como las maromas de los barcos de duros.

 ![](/tumblr_files/tumblr_inline_odbvohzhv61qfhdaz_540.jpg)

Lo mejor es que veas el video porque es de vértigo:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/jptlOHTITU0" frameborder="0" allowfullscreen></iframe>

## Cuesta arriba

Pero no contentos con eso, pusimos al más pequeños de los primos de Chiquita, mi hijo Leopardito, a empujar el cochecito en una de las cuestas más pronunciadas que vimos,y el resultado fue distinto del que esperábamos: no se le echó el carro encima debido a la pendiente! Cierto es que mi niño está fuerte, pero no olvidemos que tiene sólo 4 años, y aún así logró llevarlo hasta arriba del todo.

 ![](/tumblr_files/tumblr_inline_odp6aw93t61qfhdaz_540.jpg)
## Cuesta abajo

Pero… 

¿y cuesta abajo? 

¿Perdimos el control en algún momento?

¿Se nos escapó el carrito? 

De ninguna manera! 

El agarre es perfecto, y el control sobre el mismo no se pierde en ningún momento, aunque en esta cuesta preferimos llevarlo así porque, más que una cuesta parecía un tobogán.

 ¡Qué vértigo!

 ![](/tumblr_files/tumblr_inline_odbvoqwASG1qfhdaz_540.jpg)
## En el Castillo de Moratalla

Entonces fue cuando nos vinimos arriba y alguien dijo: 

> “No hay narices a meter el carro en el Castillo Medieval”

¿Qué no?

¡Pues ala! Allá que nos fuimos… y logramos meterlo, y así demostrar que resistente es un rato, que no atranca en ningún escenario, y que todo depende de tus límites mentales que, desde luego el carro responde a la perfección allá donde lo llevemos.

 ![](/tumblr_files/tumblr_inline_odbvowjQD61qfhdaz_540.jpg)

Así que Chiquita va a salir trotamundos y aventurera, porque con la caña que le estamos metiendo no podría ser de otra manera.

 ![](/tumblr_files/tumblr_inline_odp6acsdKA1qfhdaz_540.jpg)

Ya estamos planeando nuestra siguiente excursión, y maquinando la IV Prueba Extrema Bébécar.

 ![](/tumblr_files/tumblr_inline_odbvpcAcTw1qfhdaz_540.jpg)

Si se te ocurre alguna idea, o quieres darnos alguna sugerencia y no es muy loca, o mejor, que sí lo sea, que estamos a tope , por favor, déjamela en los comentarios y lo estudiaremos.

¡Quién sabe, a lo mejor tu sugerencia se convierte en la siguiente Prueba Extrema Bébécar!