---
date: '2015-12-27T15:47:16+01:00'
image: /tumblr_files/tumblr_inline_o39ytaXbhT1qfhdaz_540.jpg
layout: post
tags:
- trucos
- regalos
- planninos
- recomendaciones
- familia
title: Trucos para hacer la maleta de los niños
tumblr_url: https://madrescabreadas.com/post/136042178574/maletas-ninos-trunki
---

¿No te pasa que no haces más escapadas de fin de semana con los niños por la pereza de preparar equipaje para todos?

Sobre todo si sois una familia numerosa, como nosotros, y con niños muy pequeños suele echarnos para atrás el lío que hay que organizar para salir de casa, aunque sea sólo para un par de días.

<iframe src="//giphy.com/embed/Svqy0xIPSPXd6" frameborder="0" style="max-width: 100%" class="giphy-embed"></iframe>

Te voy a dar unos trucos para hacer la maleta de los niños de manera que con poca cosa, y con un solo bulto los arregles. 

Además, si te haces con una maletita pequeña con ruedas infantil, se pueden encargar de trasladarla ellos mismos, y de paso les fomentas la responsabilidad de ir encargándose poco a poco de su equipaje.

Nosotros usamos la [Trunki](http://trunki.tienda/trunki-suitcase-maletas-infantiles/)para los peques. Y nos cabe el equipaje de dos de ellos para un fin de semana. 

 ![](/tumblr_files/tumblr_inline_o39ytaXbhT1qfhdaz_540.jpg)

Las tienes en distintos colores y diseños ideales, que puedes ver [aquí](http://trunki.tienda/trunki-suitcase-maletas-infantiles/). 

Además, pueden subirse encima porque es muy resistente, incluso jugar a las carreras por el pasillo mientras esperan a que los mayores ultimemos los preparativos para el viaje.

 ![](/tumblr_files/tumblr_inline_o39ytbAOrK1qfhdaz_540.jpg)

¿Que cómo lo hacemos para meter el equipaje de dos niños para un fin de semana en una maleta Trunki? 

## Trucos para hacer la maleta de los niños

- Haz una lista con solamente lo necesario para tus hijos para dos días. En realidad no son tantas cosas si lo piensas porque la ropa que lleven en el viaje de ida puede servir también para el de vuelta también, y sólo son un par de noches como mucho. Piensa, escribe, y luego repasa y tacha alguna cosa.
- Enrolla la ropa en lugar de doblarla. Realmente ahorra mucho espacio y ayuda a evitar arrugas. Mira este video, te enseño cómo hacerlo:<iframe width="100%" height="315" src="https://www.youtube.com/embed/_GBd1-cwNl4?rel=0" frameborder="0" allowfullscreen></iframe>  

- Si aún así se arruga, cuando lleguéis a vuestro destino, puedes aprovechar para dar un baño caliente a los niños mientras dejas colgada la ropa dentro del baño para que el vapor le quite las arrugas.  
- Escoge ropa combinable entre sí, de manera que con el mínimo número de prendas consigas el mayor número de looks.  
- La ropa que abulte o pese más, es aconsejable que la lleven puesta.  
- Los zapatos es lo que más abulta. Si pueden pasar con el par que lleven puestos, mucho mejor. Total, son sólo un par de días.   
- Y para las zapatillas de casa usa calcetines de esos que llevan en la suela puntitos de goma para que no se resbalen, ya que ocupan muy poco.  
- No olvides bolsas de tela para guardar la ropa sucia, que en el viaje de vuelta podrás sacar de la maleta, y ocupar su espacio con algunos recuerdos que compréis.  
- A no ser que tus peques tengan la piel atópica o muy delicada, usa los geles y champús que haya donde vayáis porque ahorraréis bastante peso y espacio. Si prefieres llevarlos, pide en la farmacia muestras pequeñas  para viaje, o pasa parte del contenido de tus botes grandes a pequeñitos que previamente hayas guardado de los hoteles de viajes anteriores.  
- Para guardar cosas pequeñas, como horquillas o coleteros y cepillos de dientes, usa guantes de plástico desechables: se mantiene todo separado, organizado y limpio, y caben en los huecos que quedan entre la ropa.  

Espero que después de todo lo que te he contado no se te ocurra quedarte en casa por pereza.

¿Sabes algún truco más? ¡Cuéntamelo, no te cortes!

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.