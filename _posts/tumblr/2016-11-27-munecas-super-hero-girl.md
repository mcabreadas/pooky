---
date: '2016-11-27T18:09:28+01:00'
image: /tumblr_files/tumblr_inline_ohauurkus11qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- regalos
- familia
- juguetes
title: 'Las primeras muñecas súper heroínas: DC Súper Hero Girls'
tumblr_url: https://madrescabreadas.com/post/153735165034/munecas-super-hero-girl
---

Ya te presenté hace unos meses la nueva [serie on line de DC comics Super Hero Girls](/2016/01/15/serie-superhero-girls.html).

<iframe src="//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Ft6gPIGnOMZo%3Ffeature%3Doembed&amp;url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dt6gPIGnOMZo&amp;image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Ft6gPIGnOMZo%2Fhqdefault.jpg&amp;args=showinfo%3D0&amp;key=b1268f588afd4698bd3e71a47efd182a&amp;type=text%2Fhtml&amp;schema=youtube" allowfullscreen="" frameborder="0"></iframe>

Además, te anuncié que en breve sacarían las [muñecas Súper Hero Girls](https://www.amazon.es/gp/product/B01ARGBMUS/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01ARGBMUS&linkCode=as2&tag=madrescabread-21). Pues acaban de llegar, y están listas para convertirse en un estupendo regalo  de Navidad para niñas intrépidas y valientes, a las que no se les pone nada por delante porque, no sólo los chicos pueden jugar a super héroes, las chicas venimos pegando fuerte.

¡Ojo con nosotras!

Al evento de presentación asistió en nombre de este blog, nuestra querida colaboradora Mónica, de [Paraíso Kids](http://www.paraisokids.es), que salió encantada, y nos lo cuenta todo aquí:

Detrás de la marca [DC Súper Hero Girls](http://play.dcsuperherogirls.com/es-es?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=DCG_Home_blog3) se encuentra [Warner Bros](http://www.warnerbros.es/) apoyando esta gran iniciativa junto a [Dc Entertainment](http://www.dcentertainment.com/) y [Mattel](http://play.mattel.com/). Una iniciativa diferente, muy muy chula, que trasmite unos valores distintos a las de otras patentes de Warner Bros; unos valores de optimismo, amistad, fuerza, valentía, humor, autenticidad, compañerismo y trabajo en equipo, y que a su vez, demuestran que las niñas también poseen unos superpoderes fantásticos, capaces de hacerlas brillar con luz propia.

 ![](/tumblr_files/tumblr_inline_ohauurkus11qfhdaz_540.png)
## ¿Qué hay detrás de una niña DC Súper Hero Girls?

Detrás hay una chiquilla que no tiene nada que ver con lo que pensamos que son las niñas de 6 a 12 años de hoy en día, ni lo que éramos las niñas de antes…

Es una niña luchadora, fuerte, valiente, atrevida, que le gustan los retos, y además, es femenina y divertida.

¿Crees que a las chicas de hoy en día les gusta ser héroes? O por el contrario ¿crees que siguen queriendo ser sólo princesas?

## Cómo surgieron las Super Hero Girls

El poder de la mujer ha llegado a la Tierra y ya es palpable la cantidad de féminas que muestran lealtad a la súper heroína que admiran. El 21 de octubre la ONU anunció que [Wonder Woman](https://www.amazon.es/gp/product/B01AWH01J0/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01AWH01J0&linkCode=as2&tag=madrescabread-21) será la encargada de luchar por la igualdad y dar poder a las mujeres durante este año. (Son muchísimos los rostros conocidos que hacen declaraciones continuamente y apoyan a diversas causas para defender a la mujer y no sólo eso, además, cada vez nos gusta más llevar símbolos, imagines etc.. de nuestras heroínas para sentirnos protagonistas).

De aquí surgió la idea; Y junto a Mattel (marca experta en juguetes femeninos) se pusieron manos a la obra para lanzar una gama de mini-muñecas del mundo de DC Comics con un montón de personajes diferentes para que las niñas puedan elegir con cuál se sienten identificadas 

¡Pero no penséis que los niños quedan excluídos del juego! ya que niños y niñas pueden jugar juntos con super héróes como Batman, Superman, y con super heroínas como Batwoman, Super Girl, Wonder Woman…

## Contenido digital

Esta iniciativa sin precedentes incluirá contenido digital (en el [canal de youtube DC Super Hero Girl](https://www.youtube.com/channel/UCRdVJ9XqHCb6BqApCIO_TYg?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=YouTube_blogpost3), la web de [DCSúperHeroGirls.com](http://play.dcsuperherogirls.com/es-es?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=DCG_Home_blog3), y en Apps para móvil), contenido de TV (en boing), películas Made-for-Video, publicaciones,

## Complementos Super Hero Girls

Además, hay toda una línea de Juegos, textil (Alcampo, Carrefour, ZIPPI, Zara Kids, Lefties, El Corte Inglés, Ditexmed),complementos y accesorios, como las colecciones de Karactermania, y otros productos (artículos para el cuidado personal, mochilas, bolsas de deportes, artículos para el hogar, libros, juegos de construcción de LEGO, la colección de disfraces de Rubie´s…).

 ![](/tumblr_files/tumblr_inline_ohauwnKBJ31qfhdaz_540.png)
## Wonder Woman, la líder  

‘Wondy’, como la llaman sus amigas, creció en la paradisíaca isla de Themyscira, un lugar poblado por valientes mujeres guerreras. Es una auténtica líder, valiente, competitiva, resuelta y decidida, cuya misión es hacer del mundo un lugar mejor y más seguro. Es súper atlética, vuela y posee una formidable fuerza para afrontar cualquier aventura. Siempre va acompañada por su inseparable lazo.

 ![](/tumblr_files/tumblr_inline_ohauy7PR7r1qfhdaz_540.png)
## Supergirl la amable

[Supergirl](http://amzn.to/2hhW6CW) es amable. Es dulce y completamente adorable, aunque a veces está en las nubes y es un poco pardilla. Sin embargo, es la adolescente más poderosa de la Tierra. Solo le falta dejar de tropezar a cada paso. Es optimista, súper fuerte y ¡torpe! Cuando vuela es invencible, pero cuando pone los pies en la tierra, ¡todo cambia! Su visión de rayos X y su oído están super desarrollados.

 ![](/tumblr_files/tumblr_inline_ohav03tKgP1qfhdaz_540.png)
## Batgirl la tecnológica

Es súper lista, de hecho la admitieron en el instituto Super Hero High gracias a su increíble inteligencia, y no por sus superpoderes, porque no nació con ellos. Cuando no está enredada en sus labores detectivescas y perfeccionando sus habilidades, se dedica a buscar nuevas formas de usar sus increíbles artilugios. Es genial, inteligente, experta en artes marciales, muy hábil con la tecnología y una excepcional detective, gracias a su memoria fotográfica.

 ![](/tumblr_files/tumblr_inline_ohav0lkYFd1qfhdaz_540.png)
## BumbleBee, la sociable

Su zumbido la caracteriza y su capacidad para reducir de tamaño la convierte en la superheroína perfecta para averiguar los planes de los súper villanos sin que se den cuenta. Es enérgica, extrovertida y un genio de la tecnología. Posee fuerza y una destreza espectacular en el vuelo.

 ![](/tumblr_files/tumblr_inline_ohav2eCSTt1qfhdaz_540.png)
## Harley Quinn, la divertida

[Harley Quinn](https://www.amazon.es/gp/product/B01AWGZRIQ/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01AWGZRIQ&linkCode=as2&tag=madrescabread-21) es bromista y desorganizada, pero realmente divertida. Es la payasa de la clase. Le encanta hacer reír y llevar sus bromas al límite. No hay que perderla de vista porque es muy escurridiza. Maestra de las acrobacias, muy ágil, rápida y con mucho ingenio.

 ![](/tumblr_files/tumblr_inline_ohavi1HaxS1qfhdaz_540.png)

## Poison Ivy, la Idealista

Un experimento de laboratorio fallido le proporcionó el poder de controlar las plantas y su vida cambió para siempre. Ahora, en su nueva vida, florece e intenta adaptarse a sus nuevos e increíble poderes. Es tímida e idealista, tiene un toque tóxico y puede invocar a las plantas.

 ![](/tumblr_files/tumblr_inline_ohav1khcuc1qfhdaz_540.png)
## Katana, la Intrépida

Es una experta empuñando su espada, que maneja con arte y gracia. Siempre está en el filo de la navaja del arte y la moda. Es súper intrépida y divertida. Cuando no está luchando contra el crimen, está de cháchara con su espada, a la que trata como a una mascota. Es muy creativa, audaz y valiente… y una gran experta en artes marciales.

## Test elige a tu Super Hero

La firma juguetera lanzó su colección de muñecas articuladas en el mes de septiembre. La línea DCSuper Hero Girls de Mattel se completa con figuras de acción de varios tamaños (aproximadamente 33 cm) así como una gama de accesorios característicos de cada personaje.

Si quieres hacer un test súper divertido para ver qué muñeca va más con tu hija, adelante!:

<script src="//dcc4iyjchzom0.cloudfront.net/widget/loader.js" async></script>

¿Cuál te ha salido? Cuéntame!