---
date: '2018-06-21T19:08:36+02:00'
image: /tumblr_files/tumblr_inline_paon0huDca1qfhdaz_540.png
layout: post
tags:
- salud
- crianza
- familia
title: A qué edad pueden usar lentillas los niños
tumblr_url: https://madrescabreadas.com/post/175112371144/edad-uso-lentillas-ninos
---

Cuando diagnosticaron a mi hijo pequeño hipermetropía y astigmatismo a los 4 años, y el oftalmólogo le prescribió el uso de gafas, una de las primeras cosas que tuve claras desde el principio es que haría lo posible para que no le limitara su vida normal. Y con vida normal me refiero a jugar como un niño de 4 años, hacer deporte, el ganso, saltar, correr…

 ![](/tumblr_files/tumblr_inline_paon0huDca1qfhdaz_540.png)
## Por qué me planteo ponerle lentillas a mi hijo

Yo uso gafas desde los 14 años, y sé lo que pueden llegar a limitar, sobre todo a la hora de hacer deporte, por eso decidí enseguida usar lentillas. Las primeras  fueron unas [lentillas baratas](https://www.lentillasadomicilio.com/), de usar y tirar, y recuerdo que las compré con mis ahorros. Después decidí operarme con la cirugía refractaria porque me encanta practicar natación, y me limitaban bastante también. 

Tras mi cirugía he estado casi 15 años sin necesitar corrección en la vista, pero ahora vuelvo a necesitar gafas para para ver la tele, e intuyo que pronto necesitaré unas [lentillas multifocales](https://www.lentillasadomicilio.com/lentillas/lentillas-progresivas/).

Para mí es perfecta la combinación de gafas y lentillas, por eso me planteo cuando será la edad adecuada para adaptarle unas lentillas a mi hijo, aunque todavía es pronto, ya que actualmente sólo tiene 5 años.

Actualmente existen viguerías de gafas para bebés y niños de primera infancia; tan resistentes que parece un milagro. Pero cuando quiere hacer determinado tipo de actividades, como saltar en las colchonetas hinchables, o hacer una guerra de agua con sus amigos, lo primero que hace es quitarse las gafas.

![](/tumblr_files/tumblr_inline_p7n3egE0bq1qfhdaz_540.gif)

De momento él lo lleva genial, y las lleva con mucha naturalidad, pero puede que lleguen a limitarle en el futuro.

## Cómo sabré que mi hijo está preparado para usar lentillas

La comunidad científica no se pone de acuerdo en la edad mínima recomendada para el uso de lentes de contacto. Incluso se prescriben a veces para bebés con determinados problemas de visión mejorando su calidad de vida considerablemente.

Todo dependerá de la cantidad de graduación, de las características del ojo, del tipo de vida y actividades que desarrolle el niño. Quizá lo mejor sea el uso de lentes de contacto desechables, ya que cada día usarían unas nuevas, no tendrían que preocuparse de limpiarlas ni guardarlas, y si en algún momento las pierden o las rompen, el disgusto sería mínimo, y como lo normal es que las usen en momentos puntuales, para hacer deporte generalmente, es la lentilla que mejor se adapta al ojo para usos esporádicos.

 ![](/tumblr_files/tumblr_inline_paon30wACe1qfhdaz_540.png)

Una buena destreza manual del niño es importante. Probablemente, cuando sean capaces de atarse los cordones, tendrán la habilidad manual suficiente para colocarle unas lentillas.

El grado de madurez que tengan es otro indicativo. Y en esto somos los padres quienes tenemos la última palabra porque somos quienes mejor conocemos a nuestros hijos.

La responsabilidad es otro indicativo. Ya que deben tener cuidado de no perderlas, y de procurarles los cuidados necesarios.

La disponibilidad de los padres para estar los dos primeros meses acompañándolos en el proceso de aprendizaje es también un factor importante a la hora de decidir que usen lentillas.

En cualquier caso, creo que si a los niños les damos la oportunidad de demostrar que están preparados, seguramente nos sorprendan y acaben usándolas mejor que nosotros, ya que son más hábiles, tienen más tiempo y seguramente una mayor motivación que nosotros.

¿Tus hijos usan lentillas?