---
date: '2016-09-27T16:52:57+02:00'
image: /tumblr_files/tumblr_inline_oe63bnY1gJ1qfhdaz_540.png
layout: post
tags:
- cine
- planninos
- familia
title: Preestreno de Cigüeñas y nueva web de Sapos y Princesas
tumblr_url: https://madrescabreadas.com/post/151010309599/pelicula-cigue-as-sapos-princesas
---

![](/tumblr_files/tumblr_inline_oe63bnY1gJ1qfhdaz_540.png)

Hoy os contamos todo sobre el preestreno de la [película de la Warner Bros Cigüeñas](http://www.warnerbros.es/cine/cigue-as) y a la presentación de la [imagen renovada de la web Sapos y Princesas](http://www.saposyprincesas.com/), evento al que asistió en nombre de este blog nuestra colaboradora y amiga, [Dando Color a los Días](http://www.dandocoloralosdias.com/), a quien agradezco que haya sacado un ratito para compartirlo con nosotras:

El pasado domingo 25 tuvimos la suerte de poder asistir al preestreno de la película cigüeñas gracias a mi querida Madrescabreadas y a la generosidad del equipo de Sapos y Princesas, que querían hacer la presentación de su nueva web en un ambiente distendido y con parte de su público objetivo: las familias.

## Las familias somos las protagonistas 

Debo decir que la llegada a Kinépolis y encontrarnos a Junior y a Tulip para hacernos una foto nos encantó y sobre todo a mi pequeña. “ La Cigüeña me ha tocado mamá" 

 ![](/tumblr_files/tumblr_inline_oe63acsIk81qfhdaz_540.png)

> Tras un ratito de espera pudimos entrar y ocupar nuestros cómodos asientos. Era una gozada ver tantas familias juntas una mañana de domingo, dispuestos a pasar un rato entretenido , y sobre todo ver las cortas edades de los peques: había muchos que apenas rondarían los 24 meses ,por no hablar de los portados en fulares y demás.

## La nueva web de Sapos y Princesas

> Al ratito nos presentaron desde el equipo de Sapos y Princesas la nueva web que busca facilitar el encuentro de planes de todo tipo para los distintos tipos de familia que somos y existimos, pero eso sí, potenciando actividades que nos hagan relacionarnos y abandonar el aislamiento familiar que en ocasiones nos causa el ocio-tecnológico del mundo 2.0 cuando no es correctamente gestionado. 

 ![](/tumblr_files/tumblr_inline_oe62nmUzlU1qfhdaz_540.jpg)

SaposyPrincesas.com ofrece una web totalmente renovada, de cuidado diseño, perfectamente adaptada a cualquier dispositivo y con importantes funcionalidades como el geoposicionamiento en mapa de una selección de más de 3.500 actividades sólo en España. 

Además, facilita la navegabilidad con información ajustada a todos los gustos: rutas, parques y jardines, deporte y aventura, espectáculos, cine, festivales, talleres y cursos… e implementa un potente buscador para encontrar planes y muchas ideas y recomendaciones de expertos en tecnología, educación, salud, psicología, maternidad y otros temas de gran interés.

## La revista

Para lograr equilibrar las horas de exposición a pantallas con experiencias reales compartidas en familia, Sapos y Princesas ha creado nuevas herramientas.

 ![](/tumblr_files/tumblr_inline_oe62kg4wnL1qfhdaz_540.jpg)

Una tarea que viene realizando desde hace más de 10 años y que en los últimos tiempos se ha convertido en una necesidad, por eso este mes de septiembre presenta su revista con un diseño más actual que responde al estilo de vida de toda una nueva generación de padres.

## La película Cigüeñas

> Y ahora vamos a la película! Tranquilos que no habrá spoilers, o sí? Bueno, lo releeré antes de dar al enter por si acaso. 

## La trama

> Como casi todo el mundo sabe, comienza con el deseo de un niño de tener un hermanito ( bueno, no un hermano cualquiera, uno con poderes Ninja, casi nada!) y las pocas ganas de los padres de "complicarse” la vida con otro hijo. La postura de los padres está llevada a un nivel extremo ( o quizás no, desde cuando no te has puesto a jugar a meriendas , a rematar de cabeza un balón hecho con un trozo de papel o a leer un cuento a media tarde sin que “tocase” con tu/s hijo/s), bueno, pues eso, unos padres con un alto grado de dedicación al trabajo que hace pensar a los adultos que acuden a la sala de cine.
> 
> Por otro lado tenemos a las Cigüeñas y una chica huerfanita porque… ( esto no lo cuento). 
> 
> Las Cigüeñas, ya no se dedican a crear niños y hacerlos llegar a sus familias (esta parte es muy buena porque los niños se generan así, sin más, a partir de un deseo escrito, esto daría para otro post, pero bueno, no es el tema de hoy), las cigüeñas ahora reparten paquetes siendo muy buenas en ello.

## Ahora las cigüeñas trabajan en Amazon
 ![](/tumblr_files/tumblr_inline_oe5r861ygA1qfhdaz_540.jpg)

> Conocéis los servicios de entrega de San Amazon? Pues una cosa así son las cigüeñitas de esta peli. Pero resulta , que haciendo un análisis de rentabilidades del nuevo negocio cigüeñil, se dan cuenta de que la huerfanita baja las estadísticas y deciden que hay que deshacerse de ella, y esa será la prueba para poder llegar a ser jefe a la que tendrá que enfrentarse una de las cigüeñas mejor valoradas de la compañía, y de mejor corazón, nuestro querido Junior.
> 
> A partir de ahí, encontraremos un montón de situaciones cotidianas llevadas al límite en una convivencia de una pareja un poco inusual como es la formada por Junior y Tulip que tratarán de cuidar del mejor modo posible a Bebé en el transcurso de su entrega, qué no será nada sencilla y en la que tendrán que lidiar con unos lobos que son lo más de lo más (un puntazo esos ojitos y esas voces hormigosas de los jefes de la manada) y a un jefe cigüeña con unos valores poco éticos.

 ![](/tumblr_files/tumblr_inline_oe62jkzwCs1qfhdaz_540.jpg)
## Es una película entrañable 

> Muy divertida pero también con momentos que apelan a muchas decisiones que tomamos o dejamos de tomar en nuestro día a día, en nuestra vida con nuestros pequeños… En esta vida que vuela pero no por ello deja de ser estupenda y debemos de vivir al máximo con y para ellos.

## Para todo tipo de familias

> Haré una reseña especial a un momento súper emotivo de la película en el que se presentan un montón de tipos distintos de familias, desde la hetero, la homo, la monoparental… Me encantó este guiño por parte de Warner, y como ya he dicho me emocionó y se me escapó la lágrima pero no cuento más. 

## Conclusión

> Es una buena opción para pasar un rato de cine en familia con peques desde los tiernos dos años hasta los abuelos si queréis. Y esto es todo, esto es todo amigos…