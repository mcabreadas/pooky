---
date: '2014-07-13T20:00:00+02:00'
image: /tumblr_files/tumblr_inline_pbyhkqQ64O1qfhdaz_540.jpg
layout: post
tags:
- crianza
- vacaciones
- planninos
- educacion
- familia
title: Visita al Houston Museum of Natural Science con niños
tumblr_url: https://madrescabreadas.com/post/91660058628/houston-museum-natural-science-ni-os
---

Nuestro viaje a Houston fue una experiencia inolvidable para mis fieras, por muchos motivos. Para empezar, nunca habían subido en un avión, y la incertidumbre y miedo a lo desconocido fueron los protagonistas en las semanas previas al viaje. En segundo lugar, era la primera vez que hacían un viaje tan largo y a un sitio tan lejos. Nunca habían salido de España, y menos aún, cambiado de continente. Todo les resultaba fascinante y novedoso.

Creo que lo que más les entusiasmó y enriqueció a la par fue el [Museo de Ciencia Natural de Houston](http://www.hmns.org/index.php?option=com_content&view=article&id=2&Itemid=2).

 ![image](/tumblr_files/tumblr_inline_pbyhkqQ64O1qfhdaz_540.jpg)

**Aprender experimentando**

Bueno, a ellos y a mí porque en ese momento me di cuenta de que **esa visita podría sustituír a horas y horas de explicaciones docentes** en el colegio. Me refiero a que de un solo vistazo pudimos comprender la teoría de la evolución humana con reproducción de los diferentes aspectos que debía de haber tenido el hombre a lo largo de los siglos (por fin he comprendido del todo aquello que nos explicaban de la capacidad craneal), la magnitud de un dinosaurio o el lugar que ocupa el león en la cadena alimenticia, por poner sólo unos ejemplos.

 ![image](/tumblr_files/tumblr_inline_pbyhkqhwJT1qfhdaz_540.jpg)

Es genial que **los niños puedan tocar literalmente el conocimiento** , puedan aprender con sus sentidos sin necesidad de largas explicaciones verbales, incluso realizar experimentos donde ellos mismos generan energía o, como éste de la foto, donde podían apreciar las distintas densidades de los diferentes tipos de crudo. 

 ![image](/tumblr_files/tumblr_inline_pbyhkratom1qfhdaz_540.jpg)

**Funcionamiento de una depuradora de residuos**

En este video de ve cómo ponemos en marcha una **depuradora de agua** y cómo es el mecanismo para separar los residuos

<iframe width="560" height="315" src="https://www.youtube.com/embed/A6D23pwHz20" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Encontré **gran diferencia con los museos españoles**. Los americanos se curran el ambiente y la decoración muchísimo, de manera que te metes tanto en el entorno que es fácil transportarse a épocas pasadas o a cualquier tipo de ambiente.

Otra gran diferencia es que permiten al visitante tocar prácticamente todo y hacer fotos sin problemas, de manera que no ponen restricciones al disfrute.

**Simulador pozo de petróleo: viaje al centro de la tierra**

Fue increíble cuando nos metimos en un **simulador** como si bajásemos por un **pozo de petróleo** a través de las diferentes capas de la tierra. Pudimos ver por las ventanas de alrededor y por la que teníamos encima nuestro cómo íbamos bajando y dejábamos arriba la superficie de la tierra para adentrarnos hacia el centro, con movimientos incluídos.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uW6gXO0FI3s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Los esperados dinosaurios**

Pero sin duda lo que más gustó a mis niños fueron los **dinosaurios** , cuyo esqueleto está  reconstruído a tamaño real con parte de sus huesos originales completado con otro material de distinto color para distinguir la parte real de la restaurada. La iluminación y ambientación hace el resto. Impresionante sala en dos alturas donde se puede ver un mamut lanzando a un hombre por los aires en lo que, no debía de ser una escena inusual para la época, o un pterodáctilo sobrevolando nuestras cabezas.

 ![image](/tumblr_files/tumblr_inline_pbyhkrQfca1qfhdaz_540.jpg)

**Mariposario**

Digno de mención es también el **mariposario** , donde se recrea el hábitat de las mariposas, que revolotean entre los visitantes, incluso algunas se llegan a posar en ellos. Dato curioso es que a la salida hay un espejo donde es obligatorio mirarse por delante y por detrás para comprobar que no se va contigo ninguna a casa.

 ![image](/tumblr_files/tumblr_inline_pbyhksaVbf1qfhdaz_540.jpg)

**Zona para comer y descansar**

El museo está muy bien pensado para los niños, tiene un Mc Donalds dentro para tomar un lunch rápido y seguir viendo cosas, y también una **ludoteca** muy bonita decorada como un bosque donde las mesas son hojas, hay flores gigantes en las paredes, y los niños se meten en un gigantesco tronco de árbol hueco donde hay una colmena de abejas a jugar. También hay unos cómodos bancos acolchados para los padres que necesitan un paréntesis después de comer. 

 ![image](/tumblr_files/tumblr_inline_pbyhksHNH11qfhdaz_540.jpg)

**Precios**

Como dato práctico os diré que, si pensáis pasar unos días en Houston no dejéis de sacar el [City Pass](http://es.citypass.com/houston), con el que podéis visitar el [Houston Museum of Natural Science](http://www.hmns.org/) el [Downtown Aquarium](http://www.aquariumrestaurants.com/downtownaquariumhouston/default.asp), el [Museo de la Nasa](http://spacecenter.com/index.html), el [zoo](http://www.houstonzoo.org/), el [Museo de los Niños](http://www.cmhouston.org/) y una [feria chulísima](http://www.kemahboardwalk.com/) como la de las películas con atracciones. Merece la pena porque ahorras bastante, ya que el pase de adulto cuesta 90$ y el de niños, de 3 a 12 años, 70$, y a veces hay descuentos de hasta el 50% en el precio del pase.

 ![image](/tumblr_files/tumblr_inline_pbyhksj9961qfhdaz_540.jpg)

Desde luego, si tenéis ocasión, no dejéis de visitar el Museo de Ciencia Natural con los peques de todas la edades, pero sobre todo a partir de 6 años lo disfrutan a tope porque ya empiezan sus cabecitas a hacerse preguntas y **la curiosidad natural de los niños es la mejor aliada del aprendizaje.**