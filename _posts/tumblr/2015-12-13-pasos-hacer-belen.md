---
layout: post
title: Cómo hacer el belén paso a paso
date: 2015-12-13T22:56:39+01:00
image: /images/posts/antiguos/p-belen.jpeg
author: maria
tags:
  - navidad
  - trucos
tumblr_url: https://madrescabreadas.com/post/135140680004/pasos-hacer-belen
---
<!-- BEGIN PROPELBON -->

<a href="https://juguettos.boost.propelbon.com/ts/94359/tsc?typ=r&amc=con.propelbon.499930.510438.CRTz1GCzu0f" target="_blank" rel="sponsored">
<img src="https://juguettos.boost.propelbon.com/ts/94359/tsv?amc=con.propelbon.499930.510438.CRTz1GCzu0f" border=0 width="468" height="60" alt="" />
</a>

<!-- END PROPELBON -->

Una de las cosas que más gusta a los niños de la Navidad es poner el Belén en casa. Por eso os aconsejo montar el portal de Belén de Navidad todoterreno en vez de poner el Nacimiento a modo de exposición tan delicado que estéis siempre sufriendo por si lo tocan, o se cae alguna figurita del belén… En este post os voy a contar cómo poner el Belén paso a paso, montar un pueblo para el belén y todo lo que se necesita para montar un pesebre.

Uno de los recuerdos que me vienen a la mente en estas fechas es cuando era pequeña, y me pasaba las horas muertas con mi hermana jugando con las figuras del belén. Nos tirábamos en la alfombra y nos montábamos nuestra propia historia con los personajes.

Nosotras teniamos un Belen parecido al actual de Playmobil, y guardo recuerdos entrañables de tardes enteras jugando con las figuritas.

[Belen de Navidad de Playmobil](https://juguettos.boost.propelbon.com/ts/94359/tsc?amc=con.propelbon.499930.510438.16420823&rmd=3&trg=https%3A%2F%2Fjuguettos.com%2Ffiguras-accion%2F32400-A0319391-4008789094940.html)

![](/images/uploads/a0319391-2.webp)

Os voy a explicar en seis sencillos pasos cómo montar un belén todoterreno, pero bastante resultón.

## Qué significado tiene el belén de Navidad

El belén es una tradición católica que representa el misterio del nacimiento de Cristo, y es uno de los símbolos de la Navidad en los hogares.

## Cuando se pone el belén de Navidad

La tradición marca que se pone en el puente de diciembre, es decir, el puente de la Inmaculada Concepción, que viene a ser el 6 de diciembre, que es cuando los niños no tienen colegio por esta festividad, suele hacer frío en la calle, y es un momento ideal para quedarse en casa y hacer un plan familiar entrañable y divertido como poner el árbol de Navidad o el belén.

Pero hace unos años cada vez se adelanta más la fechadas decoraciones navideñas, así que desde noviembre ya se ven casa con adornos puestos.

Así que la verdad es que no hay una fecha fija, pero los ue sí está claro es que no puede pasar de la semana de Nochebuena.

## Qué se necesita para montar el portal de belén

### El suelo y el cielo del belén

El [suelo](http://amzn.to/2hoNzvZ) y el cielo son muy importantes porque nos meterá en el [paisaje del Belén](http://amzn.to/2hoRnxr) y dará realismo al escenario.

[![](/images/uploads/paisaje-belen-con-heuraufe.jpg)](https://www.amazon.es/dp/B01CTC2FYU/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=96424e729c03b1641e6a7346479d4b8e&creativeASIN=B01CTC2FYU)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B01CTC2FYU/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=96424e729c03b1641e6a7346479d4b8e&creativeASIN=B01CTC2FYU)

Puedes dibujarlo en cartulina o comprarlo hecho.

Que haya un río es fundamental porque da mucho juego.

![río belén de navidad](/images/uploads/photo_2023-10-25-13.21.04.jpeg)

El cielo u horizonte puedes ponerlo con estrellitas y montañas.

## Cómo poner el portal de belén o pesebre

Es una pieza importante porque en él colocaremos a los protagonistas del belén. 

Hay quien no lo pone, y pone el nacimiento directamente, pero creo que merece la pena hacerlo porque queda mucho más chulo. 

Si tiene una luz quedará súper entrañable por la noche cuando se encienda.

![pesebre nacimiento belén de navidad](/images/uploads/photo_2023-10-25-13.20.54.jpeg)

### Cómo se colocan las figuritas del belén

Escoge unas que no sean muy delicadas para que den juego a tus pequeños.

En esta época hay cantidad de mercadillos artesanales donde podrás encontrar [Nacimientos, Belenes completos](https://amzn.to/33Jfk9g) y figuras sueltas para completar el tuyo.

[![](/images/uploads/arte-pesebre.jpg)](https://www.amazon.es/Arte-Pesebre-Nacimiento-lienzado-Figuras/dp/B07N5D8VFL/ref=psdc_2844354031_t3_B07N5JJ67H&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Arte-Pesebre-Nacimiento-lienzado-Figuras/dp/B07N5D8VFL/ref=psdc_2844354031_t3_B07N5JJ67H&madrescabread-21)

## Cómo decorar el belén

Me encanta lo natural como los [troncos de árbol](https://amzn.to/2Jkq8pO), que quedan genial. También puedes poner piedras o piñas.

[![](/images/uploads/corteza-de-pino-.jpg)](https://www.amazon.es/gp/product/B01M7PS9TI/ref=as_li_tl?ie=UTF8&camp=3638&creative=24630&creativeASIN=B01M7PS9TI&linkCode=as2&tag=madrescabread-21&linkId=361d8967140174bec0199dbd2816f645)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B01M7PS9TI/ref=as_li_tl?ie=UTF8&camp=3638&creative=24630&creativeASIN=B01M7PS9TI&linkCode=as2&tag=madrescabread-21&linkId=361d8967140174bec0199dbd2816f645)

La paja para el portal es fundamental.

El [musgo](https://amzn.to/2HJ6Yt7) da colorido, y la nieve da ambiente navideño.

[![](/images/uploads/rossi-rosa.jpg)](https://www.amazon.es/gp/product/B01M7PS9TI/ref=as_li_tl?ie=UTF8&camp=3638&creative=24630&creativeASIN=B01M7PS9TI&linkCode=as2&tag=madrescabread-21&linkId=361d8967140174bec0199dbd2816f645)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B01M7PS9TI/ref=as_li_tl?ie=UTF8&camp=3638&creative=24630&creativeASIN=B01M7PS9TI&linkCode=as2&tag=madrescabread-21&linkId=361d8967140174bec0199dbd2816f645)

## Cómo hacer un pueblo para el belén

Necesitas puentes, posadas, casitas, molinos, pozos, corralitos con animales… Esto volverá locos a los niños, ya verás cómo inventan mil historias, y quedará genial.

[![](/images/uploads/ferrari-arrighetti-casitas.jpg)](https://www.amazon.es/dp/B01MXT399W/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=55771434d5cbbe063c5d984bc381f466&creativeASIN=B01MXT399W)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B01MXT399W/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=55771434d5cbbe063c5d984bc381f466&creativeASIN=B01MXT399W)

Poner un mercadillo dentro del belén es una idea genial porque hay un montón de puestos para amenizar la decoración: elpanadero, charcutería, pescatera...

[![](/images/uploads/oliver-oficios-8.jpg)](https://www.amazon.es/dp/B077S8CT9C/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=b9b1b470266e93a918785535146107d4&creativeASIN=B077S8CT9C)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B077S8CT9C/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=b9b1b470266e93a918785535146107d4&creativeASIN=B077S8CT9C)

Y ya, si pones cestas de fruta o huevos, o barrilitos es para volverse loca.

## Cómo montar el belén de Navidad en 6 pasos

1- Ponemos el **suelo** y lo fijamos con cinta de doble cara  en la superficie que escojamos. Nosotros lo hemos puesto en la estantería del salón.

2- Ponemos el **horizonte** o cielo al fondo fijado con cinta de doble cara. El nuestro es un cielo de noche con estrellitas.

3- Ponemos el **portal** en el lugar más visible o principal del sitio que hayamos elegido.

4- Colocamos el **nacimiento** con la Virgen, San José, El Niño Jesús, la Mula, el Buey y el ángel. 

Ah! Y la estrella en el tejado.

5- Colocamos el **pastores** y **resto de figuritas y elementos del belén** como puentes, molinos, corralitos…

Si tienes **Reyes Magos**, debes colocarlos de momento lejos del portal, y cada día acercarlos un poco más hasta que la noche de Reyes lleguen por fin.

6- **Decoramos** con la paja, la nieve, el musgo y los troncos toda la escena para darle gracia.

Este video te dará algunas ideas para colocar los elementos decorativos de los que te hablo en este post.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jI9xP8ye0sE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

¡Y ya tenemos nuestro belén para disfrutarlo en familia!

¿Y tú, cómo montas el belén en casa?

Más información sobre el Belén, su origen y más [ideas originales para montar el Belén en casa aquí](https://madrescabreadas.com/2021/12/03/ideas-montar-belen/).

Si te ha gustado compártelo, de este modo ayudas a que sigamos escribiendo este blog.