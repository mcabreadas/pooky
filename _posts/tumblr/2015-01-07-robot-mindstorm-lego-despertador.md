---
date: '2015-01-07T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_nhb3a0Ztv41qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- colaboraciones
- gadgets
- ad
- tecnologia
- testing
- educacion
- familia
- juguetes
title: 'Robótica para niños (II): Despertador musical andante'
tumblr_url: https://madrescabreadas.com/post/107391034531/robot-mindstorm-lego-despertador
---

Son muchas las utilidades que se le podrían dar al Robot de Lego Mindstorms, y de hecho, desde que [llegó a casa](/2014/12/29/robot-mindstorms-lego-ni-os.html) no hemos parado, pero como acabamos de reanudar las clases, y los primeros días cuesta levantarse de la cama bastante después de las fiestas navideñas (a lo bueno se acostumbra uno pronto), hemos pensado que ayudaría a los niños un poco de aliciente mañanero con un robot andando por casa a grito de “Chicos, a levantar” con la voz del bebé, para terminar con la canción “Happy” para animarlos a ir al cole con alegría.

Lo hemos programado para que, partiendo de un punto fijo, avance y entre en cada dormitorio para despertar a cada uno.

¿No es una pasada? Y digno de una familia de frikis!

Y lo mejor es que han sido los peques quienes han ido aportando ideas (bueno, lo de la canción se me ocurrió a mí), y colaboraron activamente en la programación mediante un interfaz sencillo, intuitivo y basado en iconos que van indicando las diferentes acciones que queremos que haga el robot: cambiamos el color del la luz, el texto que aparece en la pantalla, grabamos el mensaje de buenos días con la voz del bebé, le indicamos cómo y hacia donde se debe mover…

Os muestro en este video cuál fue el resultado:

<iframe src="//www.youtube.com/embed/v9nFuPVcuzM?rel=0" width="500" height="315" frameborder="0"></iframe>

El entorno de programación lo bajamos de la [web de de Mindstorms](http://www.lego.com/es-es/mindstorms/downloads/download-software).

El software lleva unos tutoriales, lo único malo es que están en inglés. Lo ideal es que quien dirija a los niños tenga nociones de programación.

El interés que demostraron, sobre todo la mayor, fue enorme quien, bajo la supervisión de su padre hizo sus primeros pinitos en programación, para orgullo del mismo.

 ![image](/tumblr_files/tumblr_inline_nhb3a0Ztv41qfhdaz.jpg)

 ![image](/tumblr_files/tumblr_inline_nhb3akbwh91qfhdaz.jpg)

 ![image](/tumblr_files/tumblr_inline_nhb3azB9kP1qfhdaz.jpg)

**Una curiosidad**

¿Sabías que este robot fue fabricado por LEGO en el marco de su colaboración con el [MIT](http://web.mit.edu/), donde obtuvo nuevas ideas para sus juguetes? 

**¿Qué será lo próximo?**

Pero aún nos quedan más proyectos en el tintero en los que usaremos alguno de los sensores que incorpora la caja: de color, táctil e infrarrojos, que hacen al robot responder a estímulos externos. La cosa promete.

¿Qué locura se les ocurrirá ahora?

Ya os contaré!

¿Alguna sugerencia?