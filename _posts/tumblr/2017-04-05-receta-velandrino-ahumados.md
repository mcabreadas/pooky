---
date: '2017-04-05T12:33:57+02:00'
image: /tumblr_files/tumblr_inline_ontv6h6DCR1qfhdaz_540.png
layout: post
tags:
- recetas
- familia
title: La ensalada de ahumados Ikea de Julio Velandrino
tumblr_url: https://madrescabreadas.com/post/159223190454/receta-velandrino-ahumados
---

Este fin de semana ha tenido lugar un macro evento culinario en las tiendas Ikea de toda España dentro de la campaña [“Días de InspirACCIÓN: Alrededor de la mesa”](http://www.ikea.com/ms/es_ES/campaigns/inspiraccion/?cid=a1:sm%7Ctt:p%7Ca2:es%7Ca3:Megaevento%7Ca5:PAGE%20POST%20VIDEO%7Ca6:FACEBOOK%7Ca7:wink%7Ca9:4%7Ca11:nac%7Crr:no%7Cc2:multi%7Ccc:915) para fomentar las cenas en familia, y pasar tiempo junto a nuestros hijos cocinando juntos.

Bajo el lema, “Nada como cenar juntos para amueblarnos la cabeza”, IKEA promueve las cenas en familia como el momento idóneo para aprender juntos. Y es que, según se desprende del estudio de **IKEA**  **_“Salvemos las cenas_** _”,_ las **cenas son un momento clave de disfrute familiar** ya que para un 31,8% de los padres “son el momento de hablar y saber cómo nos ha ido el día”. 

En ese sentido, un 70% de las familias declara cenar juntas, y en un 18,4% de los casos cenan primero los niños y luego los padres. Por otra parte un 47% de las familias cenan alrededor de la mesa, y un 45,4% de las mismas afirma cenar mientras ve la televisión.

Y allá que nos fuimos las fieras y yo, al showcooking de [Julio Velandrino](https://www.facebook.com/juliovelandrino) en Ikea Murcia, a ver si cogíamos alguna idea con alguna receta fácil para hacerla en familia y saludable.

 ![](/tumblr_files/tumblr_inline_ontv6h6DCR1qfhdaz_540.png)

Julio Velendrino, que además de trabajar en restaurantes de Londres, Marruecos y Francia, y abrir el suyo propio “Taúlla” en Murcia, ha participado en el programa [Top Chef](http://www.antena3.com/programas/top-chef/), nos impactó con un showcooking de lo más divertido en que fusionó sabores vikingos y de la huerta murciana creando un plato sorprendente y sencillo de elaborar, ideal para hacerlo con los niños:

##  “Ensalada de ahumados, vinagreta agridulce de perlas, anisados, cítricos y liliaceas”

## Ingredientes

- Un poquito de [salsa de tomate de Ikea](http://www.ikea.com/es/es/catalog/products/00271058/?query=S%C3%85S+TOMAT+%26+%C3%96RT+Salsatomat%2Bhierb) o casera con las especias que te gusten para el fondo.  
- [Salmón ahumado](http://www.ikea.com/es/es/catalog/products/70360334/?query=Salm%C3%B3n+ahumado+curado+en+fr%C3%ADo)  
- Salazones al gusto. El chef utilizó éstos de arenques de Ikea:  
- [http://www.ikea.com/es/es/catalog/products/30101031/](http://www.ikea.com/es/es/catalog/products/30101031/)  
- [http://www.ikea.com/es/es/catalog/products/10101032/](http://www.ikea.com/es/es/catalog/products/10101032/)  
- [http://www.ikea.com/es/es/catalog/products/30201648/](http://www.ikea.com/es/es/catalog/products/30201648/)  
- [http://www.ikea.com/es/es/catalog/products/90101033/](http://www.ikea.com/es/es/catalog/products/90101033/)  
- Una pizca de ralladura de pomelo (sólo la corteza, no lo blanco)  
- [Flores comestibles](http://amzn.to/2nNOZ6F)  
- Espárragos de sombra (crecen en el campo)  
- Pamplinas (hierba silvestre)  

**Para la vinagreta:**  

- [Mostaza](http://www.ikea.com/es/es/catalog/products/80150627/)  
- Jugo de escurrir los ahumados y salazones  
- [Perlas de alga negras](http://www.ikea.com/es/es/catalog/products/60361517/) y [rojas](http://www.ikea.com/es/es/catalog/products/20361519/)  
- Aceite virgen extra  
- Un poco de [mermelada de naranja](http://www.ikea.com/es/es/catalog/products/30295920/)
- Pizca de sal

## Modo de hacerlo
 ![](/tumblr_files/tumblr_inline_ontv2wjDJj1qfhdaz_540.png)

Se pone un fondo de salsa de tomate con hierbas al gusto en el plato.

Encima se colocan los ahumados y salazones que hayáis elegido a trozos.

Bañamos con la vinagreta, para la que mezclaremos en un bol la mostaza con un poco de jugo de los ahumados y salazones que hemos usado, aceite de oliva virgen extra, un toque de mermelada de naranja, y las perlas de de alga negras y rojas y sazonaremos al gusto.

Por encima pondremos la ralladura de pomelo, los espárragos de sombra, las pamplinas y las flores comestibles.

Pues ya tenemos un plato sencillo de elaborar y digno de los mejores chefs.

 ![](/tumblr_files/tumblr_inline_ontv7fSSzS1qfhdaz_540.png)

A disfrutar!

Muchas gracias, Julio por el ratico que pasamos, lo divertido que lo hiciste, y lo amable que has sido al explicarme la receta para que la pudiera compartir con mis lectoras.

Espero volver pronto a disfrutar de uno de tus platos! 

 ![](/tumblr_files/tumblr_inline_ontv3xyM4O1qfhdaz_540.png)