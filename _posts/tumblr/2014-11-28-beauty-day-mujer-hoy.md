---
date: '2014-11-28T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_pdpcls4QpE1qfhdaz_540.jpg
layout: post
tags:
- crianza
- maternidad
- eventos
- familia
- mujer
title: ¿Maternidad versus belleza?
tumblr_url: https://madrescabreadas.com/post/103793919433/beauty-day-mujer-hoy
---

El pasado 19 de noviembre tuvo lugar la tercera edición de el Beauty day de la revista Mujer Hoy, a la que tuve la suerte de ser invitada, lo que me hizo especial ilusión porque a veces se relaciona la maternidad con el descuido del aspecto físico, y sobre todo si tienes que domar tres fieras cada día, y que pensaran en una mamá cabreada como yo para este evento me encantó porque ponerse guapa no está reñido con los pañales.Como no pude asistir personalmente envié a mi amiga y colaboradora Elena, que escribe el blog La Guinda de Limón (que recomiendo por su frescura y humor), quien no se resistió ni por un momento a dejarse mimar y darse un baño de glamour. Desde aquí agradezco su colaboración y su buen trabajo. Ésta es la crónica de La Guinda de Limón: “Es unas de las cuestiones que siempre me rondan la cabeza porque desde que nació mi hija no tengo tanto tiempo para dedicarme a mí, así que cuando mi amiga María me ofreció la invitación al Beauty Day de Mujer Hoy, no me lo pensé, le dije "Sí sí siiiiii” , los planetas se alineaban, mis padres estaban en Madrid y tenía canguro, oooooh un ratito para mí, fantástico! El evento se celebró en el Círculo de Bellas Artes en Madrid, un auténtico lujazo de edificio, si no juzguen ustedes mismos.

 ![image](/tumblr_files/tumblr_inline_pdpcls4QpE1qfhdaz_540.jpg)

Una alfombra roja nos invitaba a subir por una escalera señorial que me recordaba al mismísimo Titanic. Empieza la fiesta!

 ![image](/tumblr_files/tumblr_inline_pdpclsZwrL1qfhdaz_540.jpg)

Al llegar arriba me encontré con un salón repleto de stands de marcas de belleza dispuestas a mostrarnos sus novedades y mujeres ávidas de esa información, muestras y de pasar un buen rato como yo.

 ![image](/tumblr_files/tumblr_inline_pdpcltx24K1qfhdaz_540.jpg)

En el Beauty Day podías encontrar marcas para el cuidado de pelo, como Moroccanoil o L'oréal, para el cuidad de la piel, como Shiseido, Clarins, Astor, Boots, Galénic o firmas punteras en perfumería como Elizabeth Arden, Chloé o Giorgio Armani y muchas muchas firmas más relacionadas con la belleza.   Empecé por la marca Chloé. Me ofrecieron oler su nueva fragancia Love Story, me fascinó porque llevaba esencias de Jazmín, una de mis flores y aromas favoritos. 

 ![image](/tumblr_files/tumblr_inline_pdpcltgrlD1qfhdaz_540.jpg)

Como de una historia de amor se tratara, cogí mi candado, escribí, lo cerré y tiré la llave, allí se quedó mi candado junto con los demás candados con sus deseos. ¿Y qué escribí?

 ![image](/tumblr_files/tumblr_inline_pdpclt1zL11qfhdaz_540.jpg)

Siguiendo con el itinerario de marcas tengo que confesaros que lo que mas me gustó fue el análisis de mi piel ofrecido por 

[Laboratorios Boots](http://www.boots-laboratories.es/)

, me encantó su línea porque además de luchar contra el envejecimiento de la piel combatiendo contra las arrugas, minimiza la aparición de las manchas desde el interior. El resultado del análisis de mi piel fue piel deshidratada por zonas, sobre todo la parte del mentón (siempre olvido aplicarme por ahí) y con manchas que me saldrán en un futuro si no me lo cuido.

 ![image](/tumblr_files/tumblr_inline_pdpcluJa6e1qfhdaz_540.jpg)

Además me puse en manos de un maquillador de Astor para que me dejara divina…    

 ![image](/tumblr_files/tumblr_inline_pdpclu4lRI1qfhdaz_540.jpg)

  Y éste fue el resultado!

 ![image](/tumblr_files/tumblr_inline_pdpclufcTc1qfhdaz_540.jpg)

    Me encantan las gafas, será porque no necesito llevarlas, y en cuanto puedo probarme, no puedo resistirme ha hacerlo, así que en el stand de Opticalia, me volví loca con unas y otras, cuál me sientan mejor?

 ![image](/tumblr_files/tumblr_inline_pdpclvpPOo1qfhdaz_540.jpg)

  En este stand me regalaron unas gafas de sol muy chulas tanto para mí como para Madres Cabreadas, así que en cuanto tenga la oportunidad de dártelas, cuenta con ellas! A que son chulas!   En el stand de Marqués de Cáceres hice una parada para brindar con un rosado que estaba espectacular. Chin chin, por nosotras! Por más momentos como éste!

 ![image](/tumblr_files/tumblr_inline_pdpclvTE2h1qfhdaz_540.jpg)

    Aquí os dejo más fotos de stands…

 ![image](/tumblr_files/tumblr_inline_pdpclwe1PK1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pdpclwDHqC1qfhdaz_540.jpg)

 ![image](/tumblr_files/tumblr_inline_pdpclw4s8y1qfhdaz_540.jpg)

 ![image](/tumblr_files/tumblr_inline_pdpclxKDBT1qfhdaz_540.jpg)

 ![image](/tumblr_files/tumblr_inline_pdpclxQtUD1qfhdaz_540.jpg)

  Al salir del evento llenita de muestras de los stands a los que pude acceder, caí en la cuenta de que no podía haber faltado, poder pisar una alfombra roja es genial y si es con lunares…ES TOTAL!!!

 ![image](/tumblr_files/tumblr_inline_pdpcly8O8J1qfhdaz_540.jpg)

    Esta fue la “bolsita” con productos que me llevé…  

 ![image](/tumblr_files/tumblr_inline_pdpclycbDg1qfhdaz_540.jpg)

  Lo que más me gustó de asistir al evento fue desde luego tener un ratito para mí descubriendo novedades de belleza y cuidado de una misma y lo que menos me gustó fue la gran cantidad de personas que se agolpaban en los stands, que a veces me fue imposible fotografiar.  

[Aquí](http://www.mujerhoy.com/belleza/lecciones-belleza/edicion-beauty-mujerhoy-841146112014.html)

 podéis encontrar una crónica de lo que fue el Beauty Day vía Mujer Hoy, famosas, premios, bloggeras, asistieron al evento, lo que me hace pensar que el sarao estuvo mucho mejor al principio y no por la tarde que es cuando yo fui, para el próximo me cojo el día de vacaciones! Lo que aprendí de asistir al evento, que ser madre puede estar reñida con la belleza, has de tener tu tiempo para ti, dedicarle tus minutos a tu ritual diario de belleza, tanto en el pelo, y uñas como en la piel con los tratamientos (cremas, serums y demás) y en el momento maquillaje (con la aplicación de innumerables productos, prebases, bases, maquillajes, lápiz, máscara, etc etc.) y puede estar reñida por la inversión que has de hacer para todos los productos. Las mamás tenemos que encontrar ese equilibrio para encontrar el tiempo, dedicárnoslo y ponernos bellas. Ha de ser una de nuestras obligaciones diarias, NUESTRO TIEMPO PARA NOSOTRAS.   También aprendí que a este tipo de eventos no puedes ir sola, has de ir con amigas! Así que… María, te espero para el próximo Beauty Day! ;-) Una vez más gracias por hacer posible esta tarde tan genial!     Y para todas las mamás que estáis leyendo este post, si tenéis la posibilidad de venir a este evento en Madrid, lo aconsejo al 100% por conocer las novedades y por pasar un buen rato y las que no, por lo menos, buscad y encontrar VUESTRO MOMENTO DE SEGUIR SIENDO MUJER.   Gracias!“

 ![image](/tumblr_files/tumblr_inline_pdpclyTZTR1qfhdaz_540.jpg)

  Gracias a ti, Elena. Te dejaron bellísima!