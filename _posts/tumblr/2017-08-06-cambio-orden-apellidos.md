---
date: '2017-08-06T19:01:37+02:00'
layout: post
tags:
- mecabrea
- derechos
title: Apellidos y orden. ¿Lo echamos a suertes?
tumblr_url: https://madrescabreadas.com/post/163871814259/cambio-orden-apellidos
---

Tras demasiado tiempo de espera, [como os contaba aquí](/2014/07/21/apellidos-hijos-nacimiento-madrescabreadas.html), por fin el pasado 30 de junio entró en vigor la [reforma de la Ley del Registro Civil](https://www.boe.es/buscar/act.php?id=BOE-A-2011-12628)que otorgaba supremacía al apellido paterno en cuanto al orden en las inscripciones de nacimiento de los hijos.

Si bien es cierto que desde febrero del 2000 dicho orden se podía cambiar con una solicitud dirigida  al Registro Civil firmada por ambos progenitores, lo que suponía un trámite aparte de la inscripción de nacimiento.

## Cuál es la novedad

La novedad a partir de ahora es que en cada inscripción hay que pronunciarse sobre el orden deseado, es decir, no se da por supuesto que se pone el apellido del padre en primer lugar si no decimos nada.

Desde el momento de la inscripción, los progenitores dispondrán de 3 días para decidir si antepondrán el apellido de la madre o del padre en el orden de los de su hijo, lo que exige acuerdo de ambos.

## Si no hay acuerdo: a pito pito…

Pero lo realmente pintoresco curioso es que si los padres no logran dicho acuerdo (lo cual no será tan infrecuente), y como dice la[canción de Ella baila sola: “lo echamos a suertes”](https://www.youtube.com/watch?v=stYs4VQBbrE).<iframe width="100%" height="315" src="https://www.youtube.com/embed/stYs4VQBbrE" frameborder="0" allowfullscreen></iframe>

 Bueno, en realidad quien lo echa a suertes es el funcionario del Registro Civil, a no ser que desee emplear otro método de elección atendiendo al superior interés del menor.

¿Que cómo se decide esto? Ni idea…

Hombre, si uno de los apellidos es indecoroso o pudiera resultar insultante para el niño, la cosa estaría clara, pero en realidad no habría un criterio claro para que el funcionario de turno pudiera decidir con algo de objetividad salvo eso… echarlo a suertes… triste, pero cierto…

## Consejo de amiga

O sea, que os aconsejo que, por muy mal que os llevéis con el otro progenitor de vuestro retoño, os pongáis de acuerdo entre vosotros, y si es del todo imposible porque ya os ha costado un mundo decidir entre ambos el nombre, la marca del cochecito, la cuna o la bañera, y el apellido es la gota que colma el vaso: 

> ”por ahí sí que no paso; el mío, el primero”

mejor lo echéis a suertes vosotros, que así por lo menos no os quedará el resquemor de que alguien ajeno, que ni le va ni le viene, decidió por vosotros algo que acompañará a vuestro hijo de por vida… a no ser que decida cambiarlo al cumplir la mayoría de edad… pero eso lo contaré en otro post.