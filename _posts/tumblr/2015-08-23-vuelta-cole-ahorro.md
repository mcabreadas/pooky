---
date: '2015-08-23T18:44:54+02:00'
image: /tumblr_files/tumblr_inline_o39zhfHE781qfhdaz_540.png
layout: post
tags:
- trucos
- recomendaciones
- familia
title: Seis sitios donde ahorrarás en la vuelta al cole y sorteo
tumblr_url: https://madrescabreadas.com/post/127402958679/vuelta-cole-ahorro
---

Llega uno de los momentos del año más temido económicamente hablando para la mayoría de las familias españolas. Y no digamos para las familias numerosas. Esas sí que tienen que hacer encaje de bolillos para que les salgan las cuentas con la vuelta al cole.

 ![image](/tumblr_files/tumblr_inline_o39zhfHE781qfhdaz_540.png)

La mejor arma en estos casos es la previsión porque, ya lo decían [los pájaros dodos en la película Ice Age:](https://www.youtube.com/v/dFt-agY1GMU?start=64.582415&end=72.603623&autoplay=1)

“Si no has sido previsor… 

espera lo peor, 

espera lo peor, 

espera lo peor…”

El caso es que los gastos escolares se multiplican en las familias numerosas, por eso, además de las consabidas herencias de ropa entre hermanos (porque lo de heredar libros es cosa del pasado, ya que ahora se escriben los ejercicios sobre ellos, y además los cambian cada año para mayor frotamiento de manos de las editoriales…), no nos queda otra que buscar ofertas para comprar a granel uniformes, zapatos, cuadernos… si no queremos vernos obligados a re hipotecar la casa.

Os cuento lo que hemos encontrado nosotros cuya relación calidad-precio nos ha parecido aceptable pero, por favor, “busque, compare y, si encuentra algo mejor, cómprelo”, y de paso me lo contáis para la próxima.

Toda sugerencia será bienvenida.

## Zapatos

Este año repetimos con los [zapatos de Carrefour](http://www.carrefour.es/uniformes-babis-calzado/cat9290005/c#). A 17 € el par. Son de piel y aguantan los partidos de fútbol en el recreo de mi hijo mediano. 

Hemos comprado dos pares por niño, uno de la talla actual, y otro de una talla más para evitar el descabale de cuentas cuando a mitad de curso cambian de número de calzado.

Tienen la típica mercedita para niña en negro, y para niño unos tipo “castellanos”, que también quedan bien para las chicas, si lo prefieren, y otros abotinados con velcro que, aunque la piel de estos últimos es más dura, todo hay que decirlo, la verdad es que los niños no se han quejado.

![zapatos carrefour colegio](/tumblr_files/tumblr_inline_o39zhfh15q1qfhdaz_540.jpg)

Eso sí, las tallas vuelan. Hay que ir a comprarlos en cuanto los sacan.

## Básicos de uniforme

Las prendas del uniforme que no llevan el escudo del colegio y que, por tanto, no necesariamente hay que comprarlas en la tienda de referencia, las hemos comprado de [Carrefour](http://www.carrefour.es/uniformes-babis-calzado/cat9290005/c#) también.

Los **calcetines cortos, calcetas largas y leotardos** (estos últimos, de canalé y lisos) los tienen en granate, azul marino y verde. Los tenéis en oferta de 3 por 2.

No sé si son de los que salen bolas o no porque es la primera vez que los compro.

![image](/tumblr_files/tumblr_inline_o39zhgCvMj1qfhdaz_540.jpg)

Tienen pantalones gris oscuro y azul marino, tanto cortos como largos. Y son adaptables en la cintura con goma con ojales. Cuestan 12 €.

El género ha mejorado respecto de otros años. Hace un par de cursos compramos unos para el niño, pero le “picaban”, y no los pudo usar. Ya os contaré cómo le van.

Las **faldas de tablas** también las tienen en azul marino y gris oscuro, y son adaptables a la cintura. Valen 12 €.

Los **jerseys de pico** los tienen en azul marino, granate, verde y rojo. Pero vuelan, así que no tardes si te interesan. Valen 12 €. 

Éstos me dan un poco de miedo porque no sé el resultado que dan. Ya os contaré.

Los **polos blancos de algodón** van en packs de 2 unidades, por 10 € los de manga larga, y por 6,99 los de manga corta.

Éstos sí los he probado, y dan bastante buen resultado, usando lejía y todo.

Si queréis calidad y un precio razonable, y vivís en Murcia, os recomiendo también [Bigger](https://www.facebook.com/UniformesEscolaresBigger), donde tienen descuento para familias numerosas.

## Deporte

Para ropa de deporte y zapatillas, nosotros solemos acudir a [Decathlon](denied:deporte%20http://www.decathlon.es/catalogo-vuelta-al-cole-1.html#mrasn=68622.97150), porque los precios son muy competitivos, y la ropa es de calidad y aguanta batalla.

El chándal del colegio es acrílico, así que respetamos la sudadera porque lleva el escudo del colegio, pero los pantalones los sustituímos por unos del mismo color, pero de algodón.

Las zapatillas deportivas de los tres las compramos aquí porque hay mucha variedad, ya a cada uno le va bien un tipo según su pie.

Los precios están muy bien, las tienen incluso a partir de 9 €.

## Libros de texto

Nosotros este año repetimos con [La Casa del Libro](http://www.casadellibro.com/librosdetexto?utm_source=madrescabreadas.com&utm_medium=AfiliadosDirectos&utm_campaign=23045). Nos los han servido en dos días, tienen precio mínimo garantizado (si los encuentras más baratos te abonan la diferencia). ![image](/tumblr_files/tumblr_inline_o39zhgRk101qfhdaz_540.jpg)

## Material escolar (sorteo)

Llegados a este punto, y para que no os agobiéis demasiado, voy a contribuír junto con la tienda on line [materialescolar.com](http://www.materialescolar.es/) sorteando un vale de 20 € más IVA para que compréis el material escolar que necesitéis.

Tienen unos precios muy buenos, descuentos para familias numerosas, y lo tiene todo perfectamente clasificado, de manera que es fácil encontrar lo que necesitas.

El sorteo estará abierto hasta el 1 de septiembre.

[Tenéis las bases del sorteo de material escolar aquí.](/2015/08/23/sorteo-material-escolar.html)

## Promoción familias numerosas

He encontrado [esta web, todoparaelcole.com, que ofrece todo lo necesario para la vuelta al cole](http://www.todoparaelcole.com/familias-numerosas), y que tiene un descuento espacial para familias numerosas. Es sólo un 3%, pero supongo que algo el algo.

No la he usado nunca, pero os la dejo por si os sirve.

Ya sabéis, se acepta cualquier sugerencia que alivie la vuelta al cole.

Como dirían en Bob Esponja:

¿Estáis listos chicos?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.