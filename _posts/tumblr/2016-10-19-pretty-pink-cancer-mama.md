---
date: '2016-10-19T10:00:18+02:00'
image: /tumblr_files/tumblr_inline_of2rn6bc7w1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- regalos
- participoen
- bebe
- colaboraciones
- ad
- familia
- juguetes
title: 'Pretty in Pink contra el cáncer de mama: #sumatealrosa'
tumblr_url: https://madrescabreadas.com/post/152016454804/pretty-pink-cancer-mama
---

El 19 de octubre se celebre el día mundial contra el cáncer de mama, que representa el tipo de cáncer más frecuente en la mujer, con una incidencia anual de más de 22.000 casos en España, el 28,5% de todos los tumores femeninos. Además, supone la primera causa de mortalidad por cáncer en mujeres.

Por eso es muy importante la prevención, no sólo basta sólo con un chequeo, también hay que hacerse una mamografía anual después de los 40 años, si no tiene ningún síntoma o antecedente familiar y, en caso contrario debe ser antes. Además, debe auto explorarse cada mes. 

Esto es algo sencillo y que no lleva más de unos minutos, sin embargo puede salvar vidas.

La prevención es clave para curar este mal.

## Cómo te ayuda la AECC

La [Asociación Española de Lucha contra el Cáncer](https://www.aecc.es/TeAyudamos/informaryconcienciar/Campa%C3%B1as/Paginas/Diacontraelcancerdemama2011.aspx) pone a tu disposición multitud de recursos tanto para prevenir, como para ayudarte si te han diagnosticado un cáncer de mama, hasta hay una [red social](https://www.aecc.es/RedSocial/Paginas/Redsocial.aspx) para acompañarte en el duro proceso, informarte, compartir…, y han creado un [libro de testimonios](https://www.aecc.es/msites/Nosotras/Paginas/Nosotras.aspx) gracias a la conocida firma de joyería [Pandora](http://www.pandora.net/es-es), donde grandes mujeres cuentan su experiencia con la enfermedad.

Además, puedes unirte a la campaña de concienciación [súmate al rosa comparando unas gafas de sol como las mías](http://sumatealrosa.com/inicio/8-gafas-aecc.html), cuyos beneficios van para la investigación.

 ![](/tumblr_files/tumblr_inline_of2rn6bc7w1qfhdaz_540.jpg)
## Juguetes #PrettyInPink contra el cáncer de mama

Millenium Baby se suma al rosa distribuyendo toda una [línea de juguetes rosa: Pretty in Pink](https://www.google.com/url?hl=es&q=http://bit.ly/2eaPrJA&source=gmail&ust=1476958701340000&usg=AFQjCNFZJXwzwd5frXUal2Md5gX6elFJlg), de la marca americana Bright Starts, que lleva tres años impulsando esta campaña de concienciación y lucha. La finalidad de la misma es la prevención. Informar a todas las mujeres que cuiden de sí mismas, que no olviden los controles anuales, las mamografías, por su futuro y el de sus hijos. 

Un porcentaje de los beneficios de la venta de los productos de puericultura Pretty in Pink en España se destinan directamente a la investigación contra esta enfermedad.

 ![](/tumblr_files/tumblr_inline_of31k1vaIc1qfhdaz_540.jpg)
## La experiencia de Rocío

Yo conocí esta enfermedad desde bien pequeña porque Rocío, alguien cercano a mi familia, la tenía. Ella ere muy joven cuando se la diagnosticaron, y todavía no era madre. 

Recuerdo cómo me lo explicó mi madre, y cómo me puse en la piel de Rocío imaginando su sufrimiento  y angustia en cada paso que iba dando: la quimio, la mastectomía, la reconstrucción… 

Recuerdo la impresión que me causaba todo aquello, y cómo admiraba a Rocío que seguía sonriendo a pesar de todo, no dejó nunca de decirme una palabra amable.

Recuerdo la alegría cuando se curó y pudo quedarse embarazada. Creo que lo más terrible para ella hubiera sido no haber podido ser madre por culpa de la enfermedad.

También recuerdo las recaídas, y cómo volvió a levantarse una y otra vez… cómo volvió a crecerle el pelo una y otra vez cada vez que terminaba la quimio.

Rocío ahora está bien tras muchos años de lucha… demasiados para cualquiera, aunque siempre sigue con el alma en vilo hasta superar la siguiente revisión.

Por eso os pido que os suméis al rosa y difundáis esta y otras historias porque si el cáncer de mama se detecta a tiempo, puede curarse.

Está en tu mano.

¡Ayúdanos a difundir! 

¡Comparte y #sumatealrosa!

 ![](/tumblr_files/tumblr_inline_of1av8c0gj1qfhdaz_540.jpg)