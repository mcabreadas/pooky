---
date: '2012-02-22T12:49:00+01:00'
image: /tumblr_files/tumblr_inline_pgaux83qud1qfhdaz_540.jpg
layout: post
tags:
- conciliacion
- conciliacionrealya
- participoen
title: La gran inmersión
tumblr_url: https://madrescabreadas.com/post/18066253673/la-gran-inmersión
---

![image](/tumblr_files/tumblr_inline_pgaux83qud1qfhdaz_540.jpg)

Abandonados es poco, si os explico cómo os tengo. He de confesar que lo mío con Conciliación Real Ya (CRYA) no ha sido una relación, sino una auténtica inmersión. Es lo que pasa cuando crees en lo que haces y luchas por ello como si no hubiera una mañana, que sí lo hay, pero no me gusta cómo pinta, y no soy persona que se conforme o que se quede esperando a ver si alguien cambia algo para que mejoren las cosas.

Mi inmersión en CRYA es la culpable de que haya abandonado este espacio, y le echo la culpa así, sin rodeos, porque es verdad, y además, lo digo orgullosa. Quizá haya decidido dejar de cabrearme tanto y ponerme manos a la obra para intentar solucionar algo, o por lo menos que mis cabreos lleguen a oídos de quién tiene la llave para hacerlo.

Sé que muchos de vosotros me habéis estado siguiendo en esta locura, y que otros muchos os acabáis de unir. Quiero daros las gracias por vuestro apoyo y vuestra fuerza. A veces una se siente sola ante el peligro, y tiene la sensación de que ha perdido un poco la cabeza. Pero sé que vosotros estáis ahí, veo que cada vez somos más, y que cada vez se nos escucha más, y pienso que no estaré tan loca… o tal vez todos lo estemos. Pero en cualquier caso, no estoy sola, y os doy las gracias.

Como también doy las gracias al equipo de CRYA por aguantarme, y por apoyarme, sobre todo en el plano personal. He pasado una temporada muy difícil en la que, sin su amistad y cercanía me hubiera sido muy difícil sobrevivir a la vida.

Os dejo con un grafitti que compartió [@clausegovia](https://twitter.com/#!/clausegovia), y que me encantó:

![image](/tumblr_files/tumblr_inline_pgaux8ukMx1qfhdaz_540.jpg)