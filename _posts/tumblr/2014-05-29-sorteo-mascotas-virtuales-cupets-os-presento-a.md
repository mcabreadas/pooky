---
date: '2014-05-29T13:40:00+02:00'
image: /tumblr_files/tumblr_inline_pb455boOnM1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- premios
- recomendaciones
- familia
- juguetes
title: Mascotas virtuales Cupets
tumblr_url: https://madrescabreadas.com/post/87193377494/sorteo-mascotas-virtuales-cupets-os-presento-a
---

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/eGKvWUaIiwc?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

**SORTEO MASCOTAS VIRTUALES “CUPETS”**

Os presento a Gum, el cerdito, y Alpha, el lobo, las mascotas virtuales de Princesita y su hermano, aunque su papá también ha elegido la suya, el murciélago Radar.

Como veis tienen forma de cubo y son fácilmente transportables y manejables, incluso se pueden llevar colgadas en la cartera del cole o en el llavero.

 ![](/tumblr_files/tumblr_inline_pb455boOnM1qfhdaz_540.jpg)

Tienen un código QR para poder “meterlas” en la tablet o móvil mediante un lector, y jugar con ellas en un aplicación gratuita para Android o Iphone que ha creado Gochi Preziosi, en realidad aumentada, aunque también se puede jugar de forma independiente.

Cada Cupet viene con un código para ser desbloqueado en la aplicación. Se pueden meter tantos códigos como mascotas tengamos, pero no interactúan entre ellas en el juego.

Hay doce animalitos para coleccionar. Además de los elegidos por mi familia, como habéis visto en el video, están el ratón, la serpiente, la rana, el jabalí, la tortuga, el perro, el pez y el pollito.

 ![](/tumblr_files/tumblr_inline_pb455bHw1F1qfhdaz_540.jpg)

La verdad es que cuando llegó el paquete a casa me los quitaron de las manos literalmente, pero no sólo los niños, sino que a los chicos mayores parece entusiasmarles también los Cupets, incluso a los adultos les molan. Les dejé llevárselos, pero a cambio me han dado su opinión. Esto es lo que me han comentado, y lo que nosotros hemos apreciado en casa:

**QUÉ NOS HA PARECIDO**

- **Facilidad se uso** : No tiene complicación. Quizá los niños pequeños necesiten ayuda para bajar la aplicación e introducir el código de desbloqueo, lógicamente, pero a partir de ahí ya no tienes que decirles nada. Es muy intuitivo. Mi hijo mediano, con 6 años lo domina a la perfección.

Lo único es que la realidad aumentada parece no ir muy bien, o al menos con los dispositivos con los que los hemos probado.

Algo importante a tener en cuenta es la facilidad que existe en Android para desacargar opciones de pago. Si bien en Apple te pide siempre la contraseña, no es la primera vez que un niño juega con un teléfono Android y compra cosas sin el conocimiento de sus padres. En este sentido, hay que tener cuidado.

- **Opciones de juego** : Existen muchas opciones para intercactuar con la mascota, se le puede dar de comer, lavar, hacer cosquillas, hacerte fotos con ella, curarla si está malita, vestirla…

Pero lo que más ha divertido a mis hijos son los minijuegos, que resultan bastante entretenidos y son variados, además de permitirles conseguir monedas para comprar comida y otras cosas que va necesitando la mascota.

Lo que me ha encantado, y a vosotros como padres, creo que también os gustará, es que cuando el  juego lleva cierto tiempo activo, la mascota pide irse a dormir, obligando al niño a apagar la luz del juego y dejarla descansar, lo que impedirá que se pasen las horas muertas enganchados a la tablet, favoreciendo que cambien de actividad, por ejemplo, a una física.

-En cuanto al **material** con que está fabricado el juguete, es de una goma semi dura, que a mi bebé le ha encantado. Puede morderla, manipularla, lanzarla… sin peligro ninguno porque no tiene aristas, ni bordes cortantes, ni se desprende la pintura…

 ![](/tumblr_files/tumblr_inline_pb455bjo1V1qfhdaz_540.jpg)

Lo único con lo que hay que llevar cuidado es la anillita que sirve para engancharla, porque viene un poco abierta en algunos muñecos y se puede llegar a salir provocando la pérdida del mismo y el consiguiente disgusto del niño. Pero eso se soluciona apretándola un poquito para asegurarnos de que quede cerrada del todo.

¿Os apetece llevaros un par a casa? Pues **ELEGID DOS** , que **SORTEO CUATRO****!**

Para participar sólo tenéis que dejarme un comentario en este post diciendo qué DOS Cupets elegiríais de resultar ganadores de entre estos cuatro modelos:

El tigre,

 ![](/tumblr_files/tumblr_inline_pb455c7x0c1qfhdaz_540.jpg)

la serpiente,

 ![](/tumblr_files/tumblr_inline_pb455cc0g71qfhdaz_540.jpg)

el pez

 ![](/tumblr_files/tumblr_inline_pb455cr0Eo1qfhdaz_540.jpg)

o el pollito.

 ![](/tumblr_files/tumblr_inline_pb455dJ1U41qfhdaz_540.jpg)

También dejadme un email de contacto o un nick de Facebook o Twitter para poder ponerme en contacto con vosotros.

El sorteo es para el territorio español, y el **plazo para participar acaba el 12 de junio, incluído.**

Participa! ¿Con cuáles te quedas?

NOTA: Este post no es patrocinado. Giochi Preziosi me ha mandado una muestra del producto para probarlo y compartir mi opinión.