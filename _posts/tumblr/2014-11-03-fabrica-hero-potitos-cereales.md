---
date: '2014-11-03T08:01:05+01:00'
image: /tumblr_files/tumblr_inline_petodaD6uo1qfhdaz_540.jpg
layout: post
tags:
- crianza
- colaboraciones
- ad
- eventos
- familia
- bebes
title: Hoy, fabricamos un potito con Hero
tumblr_url: https://madrescabreadas.com/post/101658533919/fabrica-hero-potitos-cereales
---

La alimentación que damos a nuestros hijos es una de las cosas que más nos preocupan a las madres y padres desde incluso antes de que nazcan. Por eso acepté encantada la invitación de [Hero](http://www.herobaby.com/) a una charla sobre nutrición infantil impartida por María Dolores Iniesta, y a visitar de la mano de José Manuel Ruipérez a pie de máquinas la fábrica donde se hacen los tarritos y las papillas de cereales Hero Baby que todos conocéis.

 ![](/tumblr_files/tumblr_inline_petodaD6uo1qfhdaz_540.jpg)

Ya sabéis que a mí me gusta darles comida casera a mis hijos, y que practiqué el Baby Lead-Weaning no de un modo estricto, sino a mi manera, con mi tercer hijo (lo llamo baby lead weaning todo terreno, [aquí](/post/68157636006/alimentacion-complementaria-sin-papillas-para) tenéis la explicación), pero también soy una madre de tres fieras, trabajadora dentro y fuera de casa y, por ende, práctica. Así que he tirado de potitos cuando lo he necesitado y, la verdad, después de ver de primera mano cómo los hacen, me quedo mucho más tranquila porque no les meten conservantes ni colorantes ni nada que no sea natural, y el proceso es como lo haríamos en nuestra cocina pero a lo bestia y con unas medidas exactas de cada ingrediente, estudiadas por expertos nutricionistas y con unos controles a lo largo de todo el proceso que ni la CIA.

**La materia prima**

Para empezar, la materia prima la seleccionan minuciosamente catalogando a sus proveedores en tres categorías, A, B o C, dependiendo de la calidad del producto que les suelan mandar, de manera que comprarán más a los de la categoría A, y menos a los demás. Hero les exige unos requisitos tanto en los cultivos como en la alimentación de los animales de los que obtienen la carne. 

 ![image](/tumblr_files/tumblr_inline_petodawntZ1qfhdaz_540.jpg)

Me llamó la atención que la carne les llega congelada y la pican directamente en una megapicadora que no atranca, oiga!. Directamente sin descongelar la echan al guiso y de este modo impiden la formación de microorganismos que pudieran estropearla.

Pero lo mismo sucede con las verduras, aunque la huerta la tengan al lado, les llegan congeladas y troceadas en idénticos trocitos adecuados al tiempo de cocción a que se van a ser sometidos para que no quede ninguno ni muy crudo ni pasado. Es muy curioso.

Tienen las recetas de los distintos potitos pegadas en una máquina que les va “chivando” la cantidad de ingredientes que poner para cada uno, igual que las tendríamos nosotros en la puerta del frigo con los imanes. Y esa misma máquina tiene un grifo del que sale aceite de oliva en la cantidad ideal para cada una.

Yo vi cómo hacían Verduritas de la huerta con ternera y pollo. Os lo explico:

 ![image](/tumblr_files/tumblr_inline_petoda8BY41qfhdaz_540.jpg)

**Una olla gigante**

Una vez que cuecen las verduras y la carne en una olla gigante (no tiene forma de olla pero funciona como tal), trituran el guiso y lo mezclan, le añaden fécula de patata para espesar y pasa por unas tuberías para ir llenando los tarritos.

**Los tarritos de cristal**

Los tarritos sufren un control de calidad tremendo, ya que van pasando por una cinta y les hacen 4 fotos a cada uno. La súper máquina es capaz de detectar cualquier defecto que tenga el vidrio, por ejemplo, una burbuja de aire que le haría ser más frágil, un astilla de cristal pegada o una rotura en la boquilla. Los tarros defectuosos los retira y son destruídos.

**A la hora de tapar: el vacío**

Las tapas van por otra cinta distinta, y el momento en que se encuentran con los tarritos llenos de potito es descrito por … con una intensidad que transmite su pasión por su trabajo; Justo en el instante anterior al que se cierra el tarrito, se echa un chorro de vapor de agua sobre la superficie del mismo de manera que el aire que tuviera arriba es desplazado, ocupando su lugar el vapor de agua. Justo en ese momento se cierra el tarro, de manera que cuando se enfría el vapor de agua se condensa y se transforma en una gotita minúscula de agua logrando el vacío dentro del tarro y la tapa se “chupa” hacia adentro. 

Al eliminar el aire del interior del tarro se impide que se oxide el alimento y que aguante mucho más tiempo en perfecto estado.

**La esterilización.**

Pero aquí no acaba la cosa, porque para que dure 24 meses, se esteriliza en una especie de lavadora gigante que lo calienta a 121ºC y luego los enfría con agua.

**El etiquetado**

El etiquetado es muy divertido de ver, y nos explicaron su importancia y los controles exhaustivos por los que pasa porque de él depende que obtengamos la información correcta sobre el alimento que vamos a dar a nuestros hijos, sobre todo en caso de intolerancias y alergias.

**Papillas de cereales. Dos curiosidades:**

La primera es que por fin descubrí qué significa “cereales hidrolizados”, y es que después de tostarlos para quitarles el amargor y obtener un sabor más caramelizado y, los tratan para que al aparato digestivo aún inmaduro de los bebés le sea mucho más fácil ,es decir, hacen como una “pre-digestión”.

La segunda curiosidad es que antes de fabricar la papilla de cereales sin gluten paran la producción durante casi una semana para limpiar toda la maquinaria, tornillo por tornillo un equipo de 50 personas en tres turnos para no dejar ni rastro de gluten. Luego fabrican papilla sin gluten exclusivamente durante varios meses antes de empezar otra vez con los cereales con gluten.

En otro post os contaré todo lo que aprendí en la charla que nos ofrecieron sobre nutrición infantil y algunos consejos prácticos sobre manipulación de los alimentos que vamos a dar a nuestros hijos.

 ![image](/tumblr_files/tumblr_inline_petodailVA1qfhdaz_540.jpg)

Quiero agradecer al personal de Hero que nos atendió la gran amabilidad y el trato recibido, y su paciencia con las mamis preguntonas. En especial a María Benedicto y José Manuel Ruipérez por el entusiasmo con el que realizan su trabajo.

 ![image](/tumblr_files/tumblr_inline_petodbTb4m1qfhdaz_540.jpg)