---
layout: post
title: "Desdramatizando las funciones de Navidad: los disfraces"
date: 2015-12-21T15:44:20+01:00
image: /tumblr_files/tumblr_inline_o39yv7yHs11qfhdaz_540.png
author: maria
tags:
  - disfraces
  - trucos
  - 3-a-6
tumblr_url: https://madrescabreadas.com/post/135641571969/disfraz-campana-navidad
---

El otro día me dio un ataque de pánico porque quedaban dos días para la función de Navidad de mi hijo pequeño y todavía no tenía el disfraz hecho.

Lo sé. Tenía que haberme puesto antes manos a la obra, y no dejármelo para última hora pero, lo reconozco, no quería afrontarlo.

El disfraz en sí, tú lo ves y no parece tan complicado, pero es que había que coser ciertas partes, y para alguien que sólo ha cogido una aguja para coser botones, y muy de vez en cuando, esto no es tarea fácil porque los puntos nunca salen en hilera, ni juntitos, ni regulares ni nada de eso que me decía mi abuela.

Por otra parte la paciencia no es mi mejor virtud y, sinceramente, la situación me superaba bastante porque, además de la campana, también había que hacer unas botitas a juego.

## Tensión en el grupo de Whatsapp del cole

Tendríais que ver el grupo de whatsapp de las mamás del cole saturado de fotos explicando el paso a paso, y de comentarios desesperados de las que, como yo, son menos diestras con la aguja. 

Frases del tipo:

> **mis botitas son un despropósito**  (con emoticonos llorando),

> **llevo dos noches cosiendo y estoy desesperada**

o

> **el año que viene los disfrazamos de castores** ,

 adjuntando el famoso video del anuncio, llenaban la pantalla de mi móvil.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8Ru6AhfXvUo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Y es que el subconsciente de toda madre quiere que su hijo vaya perfecto, pero no nos damos cuenta de que **el número de puntadas que demos con la aguja NO es proporcional al amor que le tenemos**.

Qué narices. ¡En realidad que el disfraz esté perfecto no importa!, con que lo parezca es suficiente porque van a estar en el escenario 5 minutos, como mucho, y luego, probablemente, no se lo van a poner nunca más en la vida.

Madres del mundo, no sufráis más por los disfraces de las funciones de vuestro hijos, y haceros un un buen “kit anti costura”, que os salvará la vida y preservará vuestra sanidad mental y la de vuestra familia; a saber: 

## Kit anticostura

- grapadora  
- grapas buenas  
- pegamento de tela  
- velcro  
- precinto  
- tijeras  

Fue mi marido el que me abrió los ojos cuando se ofreció a hacer él la campana y sacó los materiales que iba a utilizar:

 ![](/tumblr_files/tumblr_inline_o39yv7yHs11qfhdaz_540.png)

Entonces me entró la risa, y lo mostré en el grupo de whatsapp del cole para relajación de sus miembros, que habían estado los días anteriores en una vorágine de “paso a paso” de cada elemento campanil.

 ![](/tumblr_files/tumblr_inline_o39yv7Hqxj1qfhdaz_540.jpg)

Unas risas tras comprobar que alguien se lo tomaba con humor y desdramatizaba el momento “confección del disfraz” para convertirlo en algo divertido en vez de angustioso, sirvió para que nos relajáramos y nos saliera así de bonita nuestra campana que, no digo yo que esté perfecta, pero lo parece.

¿A que sí?

 ![](/tumblr_files/tumblr_inline_o39yv8h4uW1qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.