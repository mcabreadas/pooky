---
date: '2015-08-09T17:55:58+02:00'
image: /tumblr_files/tumblr_inline_nsi8s1hwzk1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- crianza
- trucos
- recomendaciones
- familia
title: Las albóndigas de Ikea se vuelven vegetarianas
tumblr_url: https://madrescabreadas.com/post/126260400054/ikea-alrededrodelamesa
---

Aunque no desaparecerán las tradicionales, [Ikea](http://www.ikea.com/es/es/) presenta sus nuevas albóndigas vegetarianas como primer paso hacia una alimentación más sana y sostenible en la que cuidará más su proceso de producción, elección de los ingredientes y bienestar animal.

 ![albondigas ikea vegetales](/tumblr_files/tumblr_inline_nsi8s1hwzk1qfhdaz_540.jpg)

## #AlrededorDeLaMesa

Esto es sólo una novedad de las que nos presentarán en su nueva campaña #AlrededorDeLaMesa, entendido como todo lo que sucede alrededor de la comida en el hogar: cultivo de alimentos, almacenaje inteligente y reciclaje, planificación y preparación de la comida, y la forma de servir y comer en la mesa.

 ![image](/tumblr_files/tumblr_inline_nsi99bKsjk1qfhdaz_540.jpg)

También pretenden facilitarnos la vida ayudándonos a cocinar con ingredientes saludables, que se ofrecerán en las tiendas Ikea en un formato que permita una cocina rápida y sencilla, sobre todo enfocado a las cenas. 

Las mesas de Ikea quieren estar presentes en nuestras vidas, ya sea en el comedor o en la cocina, porque son conscientes del lugar privilegiado que ocupan en nuestras vidas, ya que los acontecimientos más importantes suelen suceder alrededor de ellas. Nunca lo habíes pensado? Una conversación en una mesa puede cambiar el mundo. Me ha encantado este video donde precisamente es una mesa de Ikea la que nos habla:

<iframe width="500" height="281" src="https://www.youtube.com/embed/_6z10Tep52A?feature=oembed" frameborder="0" allowfullscreen></iframe>

## Nueva imagen de las tiendas Ikea

Las tiendas en España van a experimentar una importante transformación física a partir de septiembre para reflejar esta temática. Uno de los cambios más llamativos tendrá lugar en la entrada de la tienda, en el inicio del recorrido de los clientes, en la que habrá varios espacios pensados para sorprendernos a través de una experiencia emocional.

## Estudio “La vida en el hogar”

Ikea nos presenta las conclusiones de su estudio “La vida en el hogar” con datos bastante curiosos que me han sorprendido. Si bien se ha realizado en un ámbito geográfico concreto sólo relativo a Madrid, podría ser extrapolable a cualquier gran ciudad, creo.

Lo que más me ha llamado la atención es que  **el 94% de los encuestados involucra a los niños en la cocina** , lo que me parece una estupenda manera de enseñarles a comer de forma saludable.

 ![image](/tumblr_files/tumblr_inline_nsi98mf1yA1qfhdaz_540.jpg)

Que colaboren en las tareas relacionadas con la comida también me parece importante para fomentarles la responsabilidad, la autonomía y la seguridad en sí mismos. 

En [este post os contaba cómo logré que mi hijo pequeño colaborara en casa](/2015/04/26/tareas-domesticas-toddler.html) dejándole hacer pequeñas cosas, y lo bien que reaccionó.

De este estudio se desprende que los niños colaboran principalmente en:

-el lavado de platos (58%), 

-la puesta a punto de la mesa (56%) y 

-la separación de residuos y reciclaje (42%).

Os dejo este video con las principales conclusiones del estudio “La vida en el hogar”

<iframe width="500" height="281" src="https://www.youtube.com/embed/LGxTpx7x21Q?feature=oembed" frameborder="0" allowfullscreen></iframe>

Vuestros hijos ayudan en casa?