---
date: '2018-06-26T12:20:07+02:00'
image: /tumblr_files/tumblr_inline_paxcl4IbHR1qfhdaz_540.png
layout: post
tags:
- ad
- colaboraciones
- moda
title: 'Amaya: tu vestido perfecto de graduación'
tumblr_url: https://madrescabreadas.com/post/175268321224/vestidos-graduacion
---

La graduación de Primaria es uno de los momentos más emotivos de la vida escolar para las familias porque nuestros hijos terminan una etapa donde los hemos tenido bajo nuestra ala como niños que son, para empezar, de repente, otra en la que querrán echar a volar queramos o no. Y si, además, conlleva un cambio de centro escolar, el salto nos dará más vértigo aún.

Ya os conté que [los 12 son los nuevos 14](/2017/10/06/adolescentes-los-12-son-los-nuevos-14.html), ya me entendéis; nosotros salíamos de las faldas del colegio para enfrentarnos al Instituto a los 14 años, como pronto, pero ahora les enviamos el mensaje a nuestros hijos de que ya son mayores, dos años antes, cuando todavía no tienen la suficiente madurez para afrontar ese cambio tan importante.

Por eso es tan complicada esta edad en la que algunos niños y niñas ya tendrán aspecto e inquietudes de adolescentes, y en cambio otros, todavía parecerán, se comportarán y vestirán como niños.

## Se nos gradúa Princesita

Nosotros en casa hemos vivido este curso dos graduaciones. 

La del pequeño de 5 años, más bien ha sido un acto simbólico porque sólo supone un cambio de etapa (de infantil a primaria) dentro del mismo centro, y a la que los alumnos asistieron con un pantalón o falda vaqueros y unas camisetas con dibujos impresos hechos por ellos mismos.

La graduación de Princesita, que deja el colegio que la ha visto crecer durante su educación Primaria para enfrentarse a un nuevo mundo en el Instituto, ha sido un acto académico más formal, con una cena con toda la comunidad educativa, y en la que nos hemos puesto nuestras mejores galas.

 ![](/tumblr_files/tumblr_inline_paxcl4IbHR1qfhdaz_540.png)

Para ella el vestido era importante, y no ha sido fácil encontrar uno que no fuera demasiado infantil ni de muy mayor hasta que confié en [Amaya](http://es.artesania-amaya.com/coleccion/amaya-for-teen-spring-summer-2018-2), que como os he contado muchas veces, siempre logra encontrar ese término medio para las adolescentes.

Ya sabéis que Princesita es muy delgadita y alta, así que no es fácil encontrar algo que le quede perfecto, porque si de ancho le está bien, le está demasiado corto, y viceversa (cada cuerpo tiene su intríngulis), así que nos decidimos por este vestido Navy en blanco y rojo con falda de vuelo y espalda semi descubierta anudada con este divertido cordón marinero de la colección Tres Chic. Sólo tuvimos que meterle un poquito de los costados del cuerpo para entallárselo más, y quedó así de bonito.

 ![](/tumblr_files/tumblr_inline_paxco4gJRy1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_paxcqs1Xec1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_paxcq1DtQs1qfhdaz_540.png)<h>Colección Teen 2018 de Amaya</h>

Su colección TEEN 2018 es perfecta para cualquier evento que tengáis. y lo mejor es que os va a gustar a vosotras y a ellas. Os muestro un poco para que os hagáis una idea:

Predominan los tonos lisos en colores vivos, aunque el de las flores fue uno de nuestros candidatos!

 ![](/tumblr_files/tumblr_inline_papyi6zfrS1qfhdaz_540.png)

Si os gustan más los géneros con caída, tenéis opción en corto y en largo. Los cinturones completan el look haciéndolo mucho más especial.

 ![](/tumblr_files/tumblr_inline_papyiyyj071qfhdaz_540.png)

No podía faltar el color de moda en esta colección con un diseño tan original como llamativo con la espalda descubierta.

 ![](/tumblr_files/tumblr_inline_papyj9vN7o1qfhdaz_540.png)

El mono fucsia con el hombro descubierto es una maravilla

 ![](/tumblr_files/tumblr_inline_papyjqhlRv1qfhdaz_540.png)

Espero que os haya gustado y que hayáis cogido ideas para los eventos que tengáis próximamente.

Nosotros, por nuestra parte, estamos muy orgullosos de nuestra niña, que deja atrás una etapa y camina segura hacia su futuro.

 ![](/tumblr_files/tumblr_inline_paxe1k4HvZ1qfhdaz_540.gif)