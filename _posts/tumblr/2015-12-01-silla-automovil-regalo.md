---
date: '2015-12-01T19:03:23+01:00'
image: /tumblr_files/tumblr_inline_nymzxjb9di1qfhdaz_540.png
layout: post
tags:
- navidad
- crianza
- trucos
- reyesmagos
- regalos
- premios
- colaboraciones
- puericultura
- ad
- seguridad
title: 'Concurso “Tu Carta a los Reyes Magos” 2016: Silla auto Exo de Jané'
tumblr_url: https://madrescabreadas.com/post/134342029149/silla-automovil-regalo
---

![](/tumblr_files/tumblr_inline_nymzxjb9di1qfhdaz_540.png)

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

[Aquí tenéis todos los regalos entre los que podéis elegir](/2015/12/01/juguetes-navidad-gratis.html).

Uno de los regalos que podéis incluír en vuestra carta a los Reyes Magos para participar en el concurso es esta [silla de automóvil Exo de Jané Grupo I](http://www.jane.es/es/catalogo-jane/para-coche/grupo-1/exo) que ya testeamos en este blog, y cuyas características os expliqué [aquí](/2015/04/14/seguridad-infantil-en-autom%C3%B3vil-testing-silla-exo.html). 

Para niños de 9 a 18 Kg, Grupo I, con sistema Isofix y un exclusivo sistema neumático de absorción de impactos

Además de otras tres nuevas patentes mundiales en seguridad: 

- sistema electrónico de detección de Isofix,   

- tensión del arnés con reducción de desplazamiento y   

- regulación sincronizada del arnés y la cinta central entre piernas.   

Cada año las asociaciones del Consorcio Euro- Test examinan diferentes Sistemas de Retención Infantil que hay en el mercado. En este estudio se valora la seguridad de la silla y otros aspectos como pueden ser la facilidad de uso, confort y facilidad de instalación. 

Las pruebas de seguridad son mucho más exigentes que lo que marca la normativa europea, a mayor velocidad y en diferentes configuraciones, como puede ser impacto lateral.

Exo de Jané obtuvo 4 Estrellas que se corresponden con la máxima puntuación, dentro de la categoría de sillas de Grupo 1 con Isofix y que se instalan en sentido de la marcha. El sistema patentado de absorción de impacto ha influido en la brillante nota en seguridad. El dispositivo electrónico para asegurar la correcta instalación también ha contribuido a la consecución de las 4 Estrellas.

 ![](/tumblr_files/tumblr_inline_nykrgkwG321qfhdaz_540.jpg)
## Para conseguir la silla de auto Exo de Jané Grupo I

-Éste es un regalo muy especial, por ello, queremos que llegue a tiempo para su ganador, así que para optar a él es necesario que os deis de **alta en mi nuevo canal de Telegram “Hacking Familiar”, a través de él gestionaremos la entrega.**

Para lo que te doy estas sencillas instrucciones:

Debes instalar Telegram en tu móvil:

Si tienes Android:

 

[https://play.google.com/store/apps/details?id=org.telegram.messenger](https://play.google.com/store/apps/details?id=org.telegram.messenger)

Si tienes Iphone:

 

[https://itunes.apple.com/app/telegram-messenger/id686449807](https://itunes.apple.com/app/telegram-messenger/id686449807)

Una vez te hayas instalado Telegram, entra en este enlace, que es el de mi canal, y apúntate: [https://telegram.me/hackingfamiliar](https://telegram.me/hackingfamiliar)

Ya está!

No sé si conoces Telegram, es un servicio de mensajes nuevo y gratuíto que nos permitirá avisarte del premio y recopilar tus datos de forma rápida.

Tu número de teléfono no se expone en ningún momento, no te preocupes, nadie lo sabrá, sólo tienes que ponerte un alias y seguir mi canal de trucos, consejos y cosas útiles para familias, “Hacking familiar”, ¡un proyecto con el que estoy muy ilusionada, y del que me encantará que formes parte!

- **Deja un comentario en este post con tu carta a los Reyes Magos**. Se admiten fotos.  Sé original porque ganará la carta más original y auténtica, a elegir por la marca. Déjame el alias de Telegram para que me pueda comunicar contigo, caso de que resultes ganador/a.

-El plazo para subir tu carta finaliza el 15 de diciembre, y el resultado se publicará en el [canal de Telegram Hacking familiar](https://telegram.me/hackingfamiliar).

-El ámbito del concurso es la península.

¡Tienes hasta el 15 de diciembre!

¿La quieres para tu peque?