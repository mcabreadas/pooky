---
date: '2018-08-26T18:51:59+02:00'
image: /tumblr_files/tumblr_inline_ocmo3kwdzm1qfhdaz_500.jpg
layout: post
tags:
- mecabrea
- cosasmias
- familia
title: El cuarto hijo
tumblr_url: https://madrescabreadas.com/post/177415559739/familia-cuatro-hijos
---
A veces fantaseo con otro bebé, (sería mi cuarto hijo y [quinto embarazo](https://madrescabreadas.com/2011/06/30/el-ańo-pasado-por-estas-fechas/) porque las madres somos complejas y estamos llenas de contradicciones, y porque cerrar la [etapa de crianza](https://madrescabreadas.com/2013/06/22/la-soledad-de-la-crianza/) en nuestra vida es duro si lo piensas, aunque por otro lado veamos que ha llegado el momento, que es lo mejor, que no damos para más o por las razones que tenga cada una. ¿Os pasa? 


 ![](/tumblr_files/tumblr_inline_pap7g9rvnI1qfhdaz_500.jpg)

Por un lado estamos exhaustas y por momentos saturadas, pero por otro, vemos un bebé y y nuestro instinto más primario nos borra el cerebro de golpe, entonces cierro los ojos y me imagino cómo sería nuestra vida, la de cada uno de los miembros de mi familia, y pienso que sería muy distinto a cómo fue la llegada mis anteriores hijos porque ellos ya son mayores y mucho menos dependientes de mí, o al menos dependen de otra manera (creo que siempre me necesitarán).

Imagino a Princesita como una segunda madre, ella que es tan responsable y resuelta… al mediano tan cariñoso y protector siempre, y al pequeño con esa empatía y generosidad que le caracteriza.

 ![](/tumblr_files/tumblr_inline_ocmo3kwDzM1qfhdaz_500.jpg)

Sería complicado, no me cabe duda, y supondría una gran prueba como familia, sobre todo porque a mí me costaría todo más que con los otros, ya que el desgaste físico y emocional de la crianza de 3 no se puede ignorar, y necesitaría la ayuda de todos.

Por otra parte ellos tendrían que ser más independientes de repente, lo cual no creo que fuese malo.

¿Pero sabéis lo que peor llevaría? La crítica social, el sentirme cuestionada, incluso abiertamente, el no encajar en los esquemas de la mayoría porque una familia grande distorsiona su realidad (con tres medio te perdonan, pero con cuatro, directamente, juicio, crítica y etiqueta forever).

Si tienes familia numerosa sabrás de qué hablo porque seguro que has tenido que soportar comentarios irrespetuosos y hasta de mal gusto a menudo, ¿me equivoco?