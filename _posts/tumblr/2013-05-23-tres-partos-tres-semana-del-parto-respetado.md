---
date: '2013-05-23T10:38:00+02:00'
image: /tumblr_files/tumblr_inline_mn8swzQ56W1qz4rgp.jpg
layout: post
tags:
- crianza
- maternidad
- participoen
- colaboraciones
- ad
- familia
- parto
- bebes
title: Tres partos tres. Semana del parto respetado.
tumblr_url: https://madrescabreadas.com/post/51137297591/tres-partos-tres-semana-del-parto-respetado
---

Pues falto yo, entonces, por contar mis partos en la semana del parto respetado. He tenido 3, y uno de cada modalidad: uno con todos los items de violencia obstétrica, el segundo, parto natural, y el tercero cesárea de urgencia. Todos ellos en un Hospital privado.

## Primer parto

Aunque el primero no debería contarlo en este contexto, sino más bien en la semana de vamos a ganar dinero con los partos.

¿Que por qué?

Eran las 11 de la noche y llegué al hospital con contracciones. Esa misma mañana mi ginecólogo me había dicho que estaba muy verde, y que tenía que dejar que el cocido se hiciera a fuego lento haciendo chup, chup. (Porque es de los que nos trata como tontas y nos pone ejemplos absurdos para que entendamos las cosas: “Oiga, que soy una mujer leída”!)

Hoy sé que no estaba de parto, pero en ese momento, y siendo primeriza, obviamente, no. Dio la casualidad de que mi ginecólogo estaba de guardia hasta las 9 a.m, así que se hizo su composición de lugar y se organizó con la matrona para que todo acabara a las 9 en punto. Como así fue, aunque para ello me llevara el lote completo. A saber: rasurado, enema, vía y monitor de cables que me inmovilizaba, por descontado. Pero además, oxitocina, epidural, episiotomia brutal, VENTOSA y el premio gordo, fisura de coxis (estuve sin poderme sentar 15 días).

Para no dejaros con mal sabor de boca, el bebé y yo hicimos piel con piel unos segundos. Eso sí, qué guardia más bien aprovechada (y cobrada) tuvo.

 ![image](/tumblr_files/tumblr_inline_mn8swzQ56W1qz4rgp.jpg)

## Segundo parto

Con el segundo tuve 2 falsas alarmas 2 madrugadas diferentes, pero gracias a Dios, me mandaron a casa en vez de provocarme el parto como con el primero.

A la tercera va la vencida, esa noche llegué a las 2 a.m al hospital, y a las 5 a.m. ya había parido. Tuve un matrón genial, Miguel, que me puso a empujar tranquilamente en la cama, en compañía de mi marido y mi madre. De manera que en quirófano lo único que hizo el ginecólogo fue “recoger” al bebe, en palabras del mismo, a quien, por cierto, tuve que esperar sin empujar a que se pusiera la bata y casi se me sale el cachorro.

Me había puesto epidural, pero no dio tiempo a que actuara y di a luz al natural sintiendo la expulsión y viviendo la experiencia más salvaje de mi vida. En este parto, gracias a Dios, no me asistió mi ginecólogo habitual (estaba durmiendo), pero sí quiso cobrarlo, por lo que se me asignó para “seguirme” en la recuperación. A saber, dejar firmada el alta para dos días después del parto sin venir a explorarme.

Cual fue mi sorpresa cuando me dijeron que ya me iba a casa: “Pero si no me ha visto el médico”, dije confusa. Y me planté hasta que vino a verme sin abandonar la habitación. Y ahí fue donde cavé mi tumba, porque aprovechó esa visita para humillarme y vejarme. Me da vergüenza transcribir las cosas que me dijo, así que las obviaré, sólo diré las que al dinero, y no a mi persona, se refieren: dejó claro,que por 60€ que le paga mi compañía de seguros por parto, él no se levantaba de la cama, y que sí le hubiera pagado 1.500 € sí me hubiera asistido (lo que no sabía es que yo los hubiera pagado encantada , pero porque no lo hiciera).

Le puse una queja, pero ahí sigue y, además, creo que está metido en un escándalo de las células madre (menos mal que cuando me ofreció a mí lo del cordón no aceptamos).

Como más vale prevenir que curar, quien quiera saber el nombre del artista que me deje en un comentario un email de contacto, y se lo daré en privado.

## Tercer parto

El tercer parto se complicó y me tuvieron que hacer cesárea de urgencia, no respetada. Por mucho que insistí en que me permitieran hacer piel con piel con el bebé, no me dejaron. Sólo pude apenas verlo y darle un besito, y se lo llevaron dejándome más de 1 hora en reanimación sola soñando con mi retoño enganchado a mi teta ( pero luego recuperó el tiempo perdido… y aún sigue).

Para colmo mi nuevo gine me dijo:

“¿Ves? si te hubieras dejado provocar el parto 15 días antes de cumplir, tal y como te insistí varias veces (casi me hace una Hamilton sin avisar a las 38 semanas) no te hubiera pasado" (es que le venía mal mi fecha probable de parto porque cayó en agosto y se iba a las Bahamas).

En fin, que he tenido de todo, como en botica. Aunque, gracias a Dios, tengo a mis tres fieras conmigo vivitas y coleando.

Espero y sé que esto va a cambiar porque hay mucha gente joven, savia nueva, que está formada en el respeto a la mujer y al bebé, y con el suficiente empuje para cambiar las cosas. Además, cada vez somos más las madres que no nos callamos y nos partimos la cara con quien sea en uno de los momentos más delicados de nuestra existencia como es el parto y el puerperio, para exigir respeto.

 ![image](/tumblr_files/tumblr_inline_mn8sxrHaMm1qz4rgp.jpg)

Foto gracias a [http://www.mamirami.es/](http://www.mamirami.es/)

Os dejo un [post](/2012/12/06/lactando-murcia-por-un-parto-normal.html) donde cuento una reunión que mantuvimos en un grupo de apoyo a la lactancia de mi ciudad con la encargada de realizar un estudio para la Consejería de Sanidad sobre estrategias para la normalización del parto en la Región de Murcia, para recoger testimonios reales de madres contando sus partos. 

Una a una fuimos contando lo que más nos gustó de nuestros partos y lo que menos. El respeto y la atención con la que nos escuchó fueron exquisitos. Tomó buena nota de todo, incluso al final tuvimos la oportunidad de charlar en privado con ella quienes quisimos.

Tuve la oportunidad de hablar de mis tres partos delante de un grupo de personas a las que veía por primera vez, y me sentí cómoda, como en casa. Nunca pensé que me podría abrir así de repente, sin previo aviso a contar algo tan íntimo, y de lo que no se suele hablar en la vida cotidiana, al menos no de aquella manera como la que se habló en ese momento.

Todos nos escuchábamos con atención, empatizamos y nos comprendimos. Hubo risas y hubo lágrimas, pero sobre todo la certeza de que las cosas van a cambiar. El cambio será lento, es cierto, pero estamos en el camino.

 ![image](/tumblr_files/tumblr_inline_mn8t8qqles1qz4rgp.png)