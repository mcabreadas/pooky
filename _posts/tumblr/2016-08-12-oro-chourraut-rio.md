---
date: '2016-08-12T11:06:38+02:00'
image: /tumblr_files/tumblr_obsg0zMDqx1qgfaqto1_1280.jpg
layout: post
tags:
- mecabrea
- crianza
- maternidad
- conciliacion
- mujer
title: oro chourraut rio
tumblr_url: https://madrescabreadas.com/post/148829651354/oro-chourraut-rio
---

![](/tumblr_files/tumblr_obsg0zMDqx1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_obsg0zMDqx1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_obsg0zMDqx1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_obsg0zMDqx1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_obsg0zMDqx1qgfaqto5_1280.png)  
  

## Chourraut apuesta por la maternidad y gana el oro

Aún no ha actualizado su bio de Twitter en la que se presenta como yo la conocí. Como madre de Ane, en primer lugar, con la foto de una manita de bebé en blanco y negro, y como medalla de bronce el Londres.

Lo que yo no sabía era de la tenacidad de esta mujer, que apostó por la maternidad y por seguir en la élite deportiva al mismo tiempo. Si no, mirad en la cuarta foto cómo llegó al aeropuerto de Río.

## Una apuesta por la maternidad

Junto con su marido y entrenador, Xabi Etxaniz, acaban de ganar la apuesta al colgarse la medalla de oro de Piragüismo en las olimpiadas de Río 2016.

Siempre acompañada de su hija, su marido y su cuidadora Raquel mientras ella entrena, han viajado y vivido juntos en familia cada momento, cada sinsabor y cada triunfo de su carrera deportiva.

Me emociona el triunfo de esta madre porque no sólo se lleva el oro olímpico, sino que nos manda un mensaje al resto:

<quote>“tú puedes lograr lo que te propongas porque la maternidad suma, no resta”</quote>

A pesar de su parto por cesárea, lo que hizo más lenta su recuperación, no cejó en su empeño de lograr el oro que tuvo tan cerca en Londres, y que por fin tocó y besó en Río junto con su otro premio, su hija Ane, a quien [Eduardo J. Castelao](https://twitter.com/EJCASTELAO) en el [diario El Mundo](http://www.elmundo.es/deportes/2016/08/11/57acc3e1e5fdea97648b45ab.html) describe así:

<quote>“Llevo unas zapatillitas minúsculas, grises y rosas, como los calcetines, a juego, soy una monada, y los vaqueros remangados, que para eso está la moda, para seguirla. Llevo una camiseta de Río de Janeiro, azul, con Vinicius, la mascota, en el pecho, que me encanta. Ane, así me llamo. Me acurruco en los brazos de Raquel, la chica que cuida de mí cuando mamá (ama para mí, Maialen Chourraut para la historia) está en el agua. Luego, me subo al cuello de aita (papá para vosotros), que responde a los periodistas, que me acarician el pelo y me dicen cosas. Yo me río y me escondo en el sobaco de aita, que no deja de decir cosas que no entiendo. Levanto la vista y pregunto sin hablar, porque todavía no sé hacerlo muy bien: ¿Pero dónde diablos está ama?”</quote>

Lo reconozco, me han conquistado estas palabras del periodista quien ha mostrado una ternura y sensibilidad poco común con estos temas hoy en día.

## Gana la batalla de la conciliación

Maialen ha ganado mucho más que el oro. Ha ganado la batalla de la conciliación.

Sí, sí, ésa que libramos cada día las madres que nos empeñamos en criar a nuestros hijos y desarrollar nuestra carrera profesional al mismo tiempo. Ésa que muchas dan por perdida de antemano porque lo tienen todo en contra y no ven salida. Ésa de la que os he hablado muchas veces [aquí](/2015/03/08/dia-mujer-conciliacion.html), y [aquí](/2013/11/13/diamujerpopulartv.html), y que yo misma juego cada día. Ésa que muchas lucháis solas, y que ganáis día a día, poco a poco, cada vez que vuestros hijos os ven cansadas, os dan un beso y os dicen que os quieren. 

Ése es nuestro oro

Gracias Maialen por hacérnoslo ver.

Fotos y fuentes:

[El Mundo](http://www.elmundo.es/deportes/2016/08/11/57acc3e1e5fdea97648b45ab.html)

[20 Minutos](http://m.20minutos.es/deportes/noticia/chourraut-final-rio-2016-2816021/0/)