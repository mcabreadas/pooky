---
date: '2014-12-03T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petod9gQyk1qfhdaz_540.gif
layout: post
tags:
- trucos
- recomendaciones
- regalos
title: ¿Nos preparamos para la Navidad?
tumblr_url: https://madrescabreadas.com/post/104233201915/regalos-navidad
---

Ha comenzado el mes de diciembre, y parece que es el pistoletazo de salida para prepararnos para la Navidad. Ya se ven las luces adornando calles y escaparates de comercios, y los niños empiezan a hacer la lista de regalos para los Reyes Magos o Papá Noel.

Queramos o no, no podemos escapar. La Navidad llega!

Por eso he recopilado algunos tips que os serán de utilidad para que estas Fiestas no os pillen de sopetón y vayamos entrando en harina poco a poco:

**Calendario de Adviento**

No os preocupéis, que yo no soy nada habilidosa y no os voy a poner a hacer uno, lo que sí os recomiendo es este[calendario de Adviento virtual de Boolino](http://us8.campaign-archive1.com/?u=c6b01544d6db7073f151a04f5&id=235dc8905a&e=c471991799), que cada día os regala una sorpresa para que hagáis una manualidad. El único requisito es suscribiros a su web. 

 ![image](/tumblr_files/tumblr_inline_petod9gQyk1qfhdaz_540.gif)

**Escribir la carta a los Reyes Magos**

Me encanta esta plantilla del Blog de Sarai Llamas que mis hijos usaron la Navidad pasada. Quedó preciosa.

 ![image](/tumblr_files/tumblr_inline_petodaA2hO1qfhdaz_540.jpg)

Cuanto antes sentéis a los niños a escribir, más tiempo tendréis para organizaros y comparar precios.

**Ir ojeando regalos y comparar precios**

Os voy a facilitar la vida con este sitio que recopila todos los catálogos de las grandes superficies más famosas. Así podréis comparar precios a golpe de click. Además, podréis escuchar una parodia sobre los regalos navideños en tiempos de crisis que es tronchante.

<iframe src="//www.youtube.com/embed/1TLSUnaBGQk" frameborder="0"></iframe>

**Seamos solidarios**

Además de estos sitios donde poder comprar los regalos que anuncian en la tele, os sugiero que seamos solidarios y aprovechemos el gasto que prevemos hacer para ayudar a los demás con esta **subasta solidaria** a favor de las familias en riesgo de exclusión que organiza Madresfera. Podéis pujar hasta el 5 de diciembre.

O también podemos ayudar a mamás emprendedoras regalándoles trabajo. **#YoRegaloTrabajo: el mejor regalo de Navidad ** es la iniciativa de mi amiga y bloguera Orquidea Dichosa, que propone regalar productos artesanos, que dan un toque personal muy cool a cualquier regalo que tengáis que hacer, y al mismo tiempo  estaremos dando un empujoncito a estas mamis artesanas.

 ![image](/tumblr_files/tumblr_inline_petoda50S71qfhdaz_540.jpg)

**Aprovechemos para educar**

Nosotros siempre ponemos algún juguete educativo debajo del árbol según lo que estén estudiando en el cole. Por ejemplo, este año seguramente pongamos un set de planetas para Princesita porque le encantó esa lección, y sacó muy buena nota en Sociales en este tema. Y un kit para examinar bichos para Osito,  para que en vez de gastar bromas con ellos se dedique a estudiarlos y haga algo útil con su curiosidad.

 ![image](/tumblr_files/tumblr_inline_petodbNn5o1qfhdaz_540.jpg)

Este tipo de juguetes educativos los encontraréis en la web de[Miniland Educacional](http://www.minilandeducational.com/).

**Si han crecido y necesitan ropa**

Es un buen momento para comprarles algo de ropa que les permita terminar la temporada de invierno si la que tienen se les empieza a quedar pequeña, como es el caso de mi Leopardito, que no le cierra ningún pantalón de la barrigta que está echando.

En la tienda on-line de Les petit Cheris encontraréis conjuntos ideales.

 ![image](/tumblr_files/tumblr_inline_petodbL6a71qfhdaz_540.jpg)

**Si hay un bebé en la familia**

Un detalle muy bonito es equiparlo con uno de estos chupetes de temática navideña de Suavinex. A que molan?

 ![image](/tumblr_files/tumblr_inline_petodcle1G1qfhdaz_540.jpg)

Que me dices, ¿nos vamos preparando para la Navidad?