---
date: '2015-11-15T18:51:53+01:00'
image: /tumblr_files/tumblr_inline_o39z48qdJv1qfhdaz_540.png
layout: post
tags:
- ad
- colaboraciones
- participoen
title: Atentados de París. Mamá, ¿por qué?
tumblr_url: https://madrescabreadas.com/post/133277264414/explicar-ninos-atentado-paris
---

El viernes me acostaba con el alma encogida tras seguir en tiempo real por Twitter los atentados de París.

Miraba a mis tres hijos, dormidos plácidamente en sus camas y la seguridad que aparentemente les proporcionamos, y sentí realmente miedo por ellos, por no ser capaz de protegerlos de algo así.

¿Qué mundo les estamos dejando?

Al día siguiente, lo primero que hice fue volver a mirar Twitter en el móvil, mientras escuchaba cómo mi hija mayor le contaba a su hermano pequeño lo bien que lo pasamos hace dos veranos en Disneyland París porque él era demasiado pequeño para recordarlo.

 ![mama hija torre eifel](/tumblr_files/tumblr_inline_o39z48qdJv1qfhdaz_540.png)

Entonces me planteé seriamente cómo explicarles la barbarie que acababa de suceder cuando ni yo misma podía comprenderlo.

Me agobié durante un rato intentando buscar respuestas a sus posibles preguntas cuando de repente me di cuenta de que quizá la respuesta es que no hay respuesta.

Abordé el tema con mis dos hijos mayores y dejé que fueran ellos quienes hablasen primero y expresasen sus reacciones y sentimientos, que fueron desde el miedo inicial y la rabia a la frustración por no entender por qué. Fue la expresión que más repitieron:

## “Mamá, ¿por qué?, Pero ¿por qué?”

Entonces les dije que yo tampoco entendía por qué un ser humano es capaz de cometer semejantes atrocidades.

Contesté sus preguntas, siempre con la verdad, pero no más allá de lo que querían saber, y sin más detalles de los necesarios, y les dejé la puerta abierta para hablar del tema cada vez que quisieran.

Creo que no es bueno intentar protegerlos demasiado, y mucho menos ocultarles la verdad. Los niños tienen al alcance muchos medios en los que escuchar y ver lo ocurrido, incluso para ver demasiado, por lo que debemos mostrarles la realidad y ayudarles a que expresen lo que les hace sentir mostrando comprensión y dándoles la seguridad que necesitan tras verse violada su inocencia de repente con semejante atrocidad.

Desde aquí mi solidaridad con las víctimas y mi apoyo a tantas y tantas familias destrozadas la noche del viernes 13 en París.

 ![](/tumblr_files/tumblr_inline_o39z4905z41qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.