---
date: '2014-06-16T22:00:56+02:00'
image: /tumblr_files/tumblr_n73rg673191qgfaqto6_250.jpg
layout: post
tags:
- planninos
- diy
title: 'DIY: Ideas originales para decorar tu fiesta. Estilo "picnic" para una boda
  diferente.'
tumblr_url: https://madrescabreadas.com/post/88984454512/diy-ideas-originales-para-decorar-tu-fiesta
---

Una de las cosas increíbles que quería compartir con vosotros de mi viaje familiar a Houston, Texas, es la decoración low cost de la boda a la que asistimos.

Entre todos, chicos y grandes, decoramos el salón de celebraciones bajo las órdenes de una novia segura de sí misma, y de la decoración que había elegido para el evento.

He de reconocer que, aunque hubo momentos de agobio porque pensaba que no lo conseguiríamos, ella estuvo todo el rato tranquila y el resultado fue inmejorable. Un salón, que en principio se me antojó frío,  maravillosamente convertido en un picnic gigante, lleno de vida, calidez y chispa en rojo y blanco y con un exquisito toque vintage.

De verdad, os digo, que no tenía nada que envidiar a ninguna otra boda en la que haya estado. 

Os cuento paso a paso:

## Las sillas

![image](/tumblr_files/tumblr_n73rg673191qgfaqto6_250.jpg)

Vestidas de blanco con gran lazo a cuadros grandes rojos y blancos en el respaldo dieron el toque de color al salón.

**![image](/tumblr_files/tumblr_n73rg673191qgfaqto9_500.jpg)**

## Las mesas de los invitados

Manteles blancos y servilletas a cuadros rojos y blancos para las mesas de los invitados, con una maceta roja o blanca con margaritas blancas y amarillas y el número de la mesa pinchado en la tierra.

![image](/tumblr_files/tumblr_n73rg673191qgfaqto1_250.jpg)

Jarras en lugar de vasos, para que cada uno se sirva sangría o cerveza al gusto, con una original pajita blanca y roja.

Atención al detalle de la etiqueta colgando del asa de la jarra para que cada invitado ponga su nombre con unos rotuladores, que estaban situados en la mesa de las bebidas.

![image](/tumblr_files/tumblr_n73rg673191qgfaqto7_250.jpg) ![image](/tumblr_files/tumblr_n73rg673191qgfaqto2_250.jpg)

Los cubiertos van envueltos en la servilleta estilo picnic, con una cuerdecita que da un toque de lo más rústico.

## Las mesas de los niños

Como os podréis imaginar me enamoró la idea para la mesa de los niños con mantel rojo y con globos.

Al fondo se ve la bombona de helio con la que los hinchamos y los atamos a la maceta de flores. Los vasos y las servilletas con cuadritos rojos, y los mantelitos individuales para dibujar sobre ellos con ceras de colores me conquistaron porque, sin duda, hicieron la espera de la comida más divertida para los peques.

![image](/tumblr_files/tumblr_n73rg673191qgfaqto3_500.jpg)

## La mesa de los novios

Centrada en el salón, con mantel completo de cuadros grandes rojos y blancos, centro de margaritas a los pies y encima de la mesa y los carteles de Mr. y Mrs. la distinguieron del resto.

![image](/tumblr_files/tumblr_n73rg673191qgfaqto5_250.jpg)

## La mesa dulce

Mantel a cuadros al centro sobre otro blanco y tarta de nata decorada con rosas combinaban a la perfección con los cup cakes en los mismos tonos dando como resultado una mesa de dulce.

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto5_250.jpg)

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto4_250.jpg)

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto7_250.jpg)

## La mesa de los regalos

Los invitados iban metiendo en la caja los sobres con dinero, y dejaban sobre la mesa con mantel a cuadros cruzado sobre el blanco, los paquetes.

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto10_500.jpg)

## Regalos para los invitados

Creo que es la boda en la que más se ha pensado en los invitados de las que he asistido.

Dentro de la Iglesia los pajes, entre los que se encontraban mis hijos, repartieron una **campañillas** con un lacito “picnic”, cuando la ceremonia estaba terminando, de modo que su tintineo acompañaba muy bien la alegría de los novios recién casados.

![image](/tumblr_files/tumblr_n73s5ilj7f1qgfaqto1_500.jpg)

A la salida de la Iglesia, como en Texas parece que lo del arroz no se estila; muy bien visto, nos repartieron unos pequeños **pomperos** con su lacito picnic también para que envolviéramos a los novios en pompas de jabón.

![image](/tumblr_files/tumblr_n73s5ilj7f1qgfaqto2_r1_500.jpg)

**Piruletas**  de fresa y nata para los niños

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto3_500.jpg)

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto9_500.jpg)

**Abanicos** para las señoras, cómo no, rojos y blancos, que fueron repartidos por las damas de honor, vestidas de rojo en unas cestitas de picnic a conjunto.

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto2_250.jpg)

![image](/tumblr_files/tumblr_n73s5ilj7f1qgfaqto3_r1_500.jpg)

Botes llenos de **bombones** decorados estilo picnic también.

![image](/tumblr_files/tumblr_n73rbwPrTc1qgfaqto1_250.jpg)

Ya os figuraréis dónde acabaron los bombones, que estaban deliciosos porque eran uno de mis favoritos de chocolate suizo con leche, mmm…

Espero que os haya gustado tanto como a mí, y que os sirva para coger ideas para una boda o para otra celebración que tengáis que organizar, ya que es barato, sencillo y elegante, y da juego ya que, al ser temática picnic, los invitados pueden servirse ellos mismos la comida y la bebida, con el consiguiente ahorro en camareros. Eso sí, lleva trabajo, pero lo más difícil, que son las ideas os las ha dado la artista [Carmen Sánchez Romero](http://carmensr.com/) a través de este humilde blog.