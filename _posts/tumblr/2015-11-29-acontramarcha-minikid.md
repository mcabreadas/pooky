---
date: '2015-11-29T18:06:37+01:00'
image: /tumblr_files/tumblr_inline_o39z08QuWS1qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- seguridad
- bebes
title: Probamos la silla de auto Minikid. Más seguro a contramarcha
tumblr_url: https://madrescabreadas.com/post/134203408824/acontramarcha-minikid
---

Desde que me convencí de que la contramarcha es la forma más segura de viajar en un automóvil, sobre todo para los niños, y más aún para los bebés, [he escrito varias veces sobre el tema en el blog](/2014/11/12/sillas-automovil-seguridad-ninos.html) para convencerte porque tengo claro que las sillas de coche a contramarcha salvan vidas. Así de rotundo.

Por eso, y aunque mis hijos son mayores, quise probar la silla [Axkid de Minikid](https://www.amazon.es/gp/product/B073X1C2GY/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B073X1C2GY&linkId=76eae12b0a178f4cc946d8228c6dcb82) en mi peque de 3 años, con la dificultad añadida de que está acostumbrado a viajar en el sentido de la marcha.

<imgalt contramarcha axkid data-orig-height="540" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nylg99Vx3n1qfhdaz_540.jpg"></imgalt>

Por eso, lo ideal, y desde aquí lo aconsejo fervientemente, es que desde el nacimiento, el bebé viaje siempre a contra marcha. Lo tienes fácil, porque las sillas del grupo 0 van todas en sentido contrario de la marcha. Lo que hay que hacer es que, cuando pase al siguiente grupo, siga viajando a contra marcha, y comprar una silla de este tipo, de este modo, aumentaremos sus seguridad en carretera, y no perderá la costumbre de viajar al revés, por así decirlo.

No tengas miedo de invertir dinero en estas sillas, además, hay de varios precios. Ten claro que, incluso la peor silla a contramarcha es mejor que la mejor en sentido de la marcha. Pero no porque lo diga yo, sino por pura física. Está demostrado.

## Por qué mejor a contramarcha

La cabeza de un bebé es proporcionalmente muy grande y pesada respecto al resto de su cuerpo. Hasta el 25% de su peso.

En un accidente en un asiento en el sentido de la marcha se producen tremendas fuerzas sobre el cuello, hombros y cabeza del pequeño.

El cuello es sometido a una fuerza equivalente a 300 - 320 kg. Si la médula espinal se estira tan solo algo más de 6 mm puede ocasionar lesiones muy graves.

 ![dummie accidente nino](/tumblr_files/tumblr_inline_o39z08QuWS1qfhdaz_540.png)

En un asiento contrario a la marcha la fuerza es distribuida sobre más zonas del cuerpo. La fuerza sobre el cuello es equivalente a 50 kg, que es igual a 6 veces menos.

## Características de la silla Minikid

La silla Minikid que hemos testeado es **de las más económicas** del mercado, pero igualmente segura.

 ![silla contramarcha nino](/tumblr_files/tumblr_inline_o39z09r6yK1qfhdaz_540.jpg)

**Homologada de 9 – 25 kilos** (como pasa de los 18 kilos de uso, no lleva sistema isofix ) se instala en el coche con el cinturón del propio automóvil.

Este modelo es uno de las más vendidas, tanto por precio como por seguridad.

Tiene **5 posiciones de reclinado** , aunque una vez puesta en el coche, hay que soltar la silla para variar el reclinado de la misma.

Incorpora un sistema de **ajuste automático de altura del reposacabezas** y longitud del arnés de la silla simultáneo.

**Es una de las sillas que menos espacio ocupa** una vez instalada en el coche, y esto es importante, porque nosotros tenemos un coche familiar normal y, es cierto que en el asiento de alante queda menos espacio para las piernas, pero a mí me merece la pena porque sé que mi hijo va más seguro.

Es **lavable a máquina a 30º**. Tiene una limpieza buenísima, y se la funda se extrae con relativa facilidad.

## Certificado Plus = más seguridad

Tiene el **certificado Plus.**

 ![certificado plus](/tumblr_files/tumblr_inline_o39z09zkgA1qfhdaz_540.jpg)

La certificación Plus es un test complementario que se efectúa en sillas de seguridad destinadas al mercado sueco. Todas las sillas certificadas Plus cumplen la normativa europea ECE R44 y están aprobadas para montarse en todos los asientos del coche.

Garantiza que el niño no es sometido/a fuerzas que le puedan ocasionar lesiones graves sobre el cuello en una colisión frontal.

## La instalación

Igual de importante que tomar una buena decisión al comprar la silla de auto es instalarla correctamente, si no, no hará su función correctamente en caso de accidente.

Por ello es recomendable asesorarse en la tienda, o leer detenidamente el manual de instrucciones y, ante cualquier duda, siempre preguntar.

A nosotros nos la instalaron en la tienda [La Cigüeña](http://lorcabebe.com/es/) de Lorca, único sitio en la Región de Murcia donde encontramos este tipo de sillas, y auténticos especialistas. Da gusto encontrar gente tan profesional y que se toma en serio algo tan importante como la seguridad de nuestros hijos.

Nos comentaban en la tienda que hay padres que son capaces de gastarse un dineral en un móvil, pero a la hora de comprar la silla de auto para su bebé, escogen la más barata o “quitamultas” para salir del paso, sin pararse a pensar que de su decisión depende la seguridad, incluso a veces, la vida de su pequeño.

Aquí tenéis un video con la instalación de la silla Minikid.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Fw-CK4uYbOk" frameborder="0" allowfullscreen></iframe>

## Nos vamos de viaje

Ya estamos listos para irnos de viaje. Y el peque, encantado porque va mirando a sus hermanos, y contando coches por la ventanilla. Si te das cuenta, tiene más visibilidad que cuando iba en sentido de la marcha, que sólo veía mi nuca.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/FbKp9WY_jjU?rel=0" frameborder="0" allowfullscreen></iframe>

No le importa llevar las piernas encogidas, se siente cómodo porque en realidad, si lo piensas, los niños rara vez se sientan con las piernas estiradas. Incluso si van en sentido de la marcha, no les gusta llevar las piernas colgando, y muchas veces las encogen, o apoyan en el asiento de alante. Observadlo, ya veréis. Y en casa se pasan el día en cuclillas jugando, o sentados a lo indio.

Os puedo decir que mi hijo se siente cómodo en esa posición.

Si os preocupa no verle la cara mientras conducís, tened claro que es más seguro instalar un [par de espejos para vigilarlo](https://www.amazon.es/gp/product/B01BUVH2DA/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B01BUVH2DA&linkId=010f2aac3fc1e08da3f1679dcbb5b80b), que estar girando la cabeza mientras conducimos (y nuestras cervicales nos lo agradecerán).

![](/tumblr_files/tumblr_inline_o2qs323Skv1qfhdaz_540.jpg)

En mi caso, como va con sus dos hermanos, ellos lo vigilan perfectamente.

¡Ya hemos llegado a la playa!

¡Muchas gracias, Axkid, por darme la oportunidad de viajar más seguro!

 ![ninos playa](/tumblr_files/tumblr_inline_o39z09a9Lz1qfhdaz_540.jpg)

¿Te he hecho replantearte la opción de la contramarcha?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.