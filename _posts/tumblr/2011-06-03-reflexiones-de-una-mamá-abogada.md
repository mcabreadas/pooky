---
layout: post
title: Reflexiones de una mamá abogada
date: 2011-06-03T11:21:00+02:00
image: /tumblr_files/tumblr_inline_pe4ml4qVMC1qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/6137543065/reflexiones-de-una-mamá-abogada
---
Me siento en mi mesa de trabajo, en casa, entre zapatillas de ballet, castañuelas, bolsitas de merienda, coches de juguete… como cada día.

Antes de empezar a trabajar, limpieza de mesa de todo lo que mis hijos, o yo misma voy dejando encima… Después intento centrarme en mis casos… 

Mil cosas en la cabeza. Cómo organizarlas? 

Los plazos judiciales, que corren sin piedad, los clientes, que exigen lo suyo, como es lógico, los atuendos para las actuaciones de fin de curso de los niños, el nuevo horario de verano del cole y guardería (a ver cómo compagino las dos cosas), los cumpleaños de los amiguitos, con su correspondiente regalo, el papeleo de la matrícula del pequeño, que entra al cole de su hermana el curso que viene, con su correspondiente reclamación (por un error administrativo sí, pero si no me quejo en plazo se queda fuera), la lista de la compra, mi madre, que necesita mucha atención ahora porque perdió a la suya hace un mes, mi hermana, que me necesita siempre, aunque no dice nada, mi marido.. qué os voy a contar…A veces pienso que hago de todo, pero nada bien. 

 ![](/tumblr_files/tumblr_inline_pe4ml4qVMC1qfhdaz_540.jpg)

Es esa sensación de no llegar. Y me da miedo cometer un error, pero creo que el verdadero error sería perderme la vida de mis hijos y, no sólo verlos crecer, sino participar activamente en su crecimiento, tanto físico como personal. 

¿Cómo podría no estar cuando mi niña pregunta por primera vez acerca de la muerte, o cuando resulta ganadora del concurso de “relevés” en su academia de baile? 

¿Y cómo podría haberme perdido la primera vez que subieron en tranvía, o sus caras de ilusión cuando los recojo del cole y de repente te encuentran entre la gente? 

Observarlos jugar sin que se percaten de que miras, y ver cómo evolucionan, cómo crecen, cómo empiezan a desenvolverse solos poco a poco, en un aprendizaje que no termina nunca.

Despertarlos de la siesta, sin prisas, entre mimos y risas, acostarlos por las noches, cada uno con su ritual de canciones y caricias, cuando ya estás cansada, y casi no puedes ni con tu alma, pero ves que te necesitan y no puedes consentir que se duerman disgustados. 

Y pienso… cuánto tiempo va a durar esto?

 Quizá mañana ya crezcan y no lo necesiten. Voy a disfrutar cada momento, porque los momentos precluyen, como los plazos de los que os hablaba antes, es decir, o los aprovechas o los pierdes para siempre, la vida no tiene vuelta atrás.