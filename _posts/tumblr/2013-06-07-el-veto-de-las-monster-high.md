---
date: '2013-06-07T11:53:00+02:00'
image: /tumblr_files/tumblr_inline_mo0op5qCyQ1qz4rgp.jpg
layout: post
tags:
- mecabrea
- crianza
- adolescencia
- educacion
- familia
title: 'El veto de las Monster High '
tumblr_url: https://madrescabreadas.com/post/52370323129/el-veto-de-las-monster-high
---

No me gustan nada las Monster High, y me cabrea toda su estética y abalorios (hasta creo que una duerme en un ataúd). Pero están tan de moda y a mi Princesita le hacen tanta ilusión que, tras pedírmelo por activa y por pasiva, ya me daba pena y, además, veía que lo prohibido le atraía más aún, y estaba logrando el efecto contrario al deseado.

Por eso decidí reconducir en vez de prohibir, y se me ocurrió que ella misma creara un álbum para pegar las pegatinas coleccionables, así probablemente evitaríamos la estética tétrica y poco recomendable para niñas de 7 años. Y funciono! Eligió cartulinas de colores para las páginas del álbum y las encuadernamos con un gusanillo. Y para la portada le imprimí unos dibujos para colorear, y los pintó de colorines alegres (menos mal).

![image](/tumblr_files/tumblr_inline_mo0op5qCyQ1qz4rgp.jpg)

Luego se me ocurrió fabricarle con un cartón de un paquete de sobre de pegatinas  que le compré, una bolsita interior para guardar los cromos repes para que los cambie con sus amigas. Hicimos cuadrados numerados para pegarlos , y así de paso repasamos los números.

![image](/tumblr_files/tumblr_inline_mo0ohoifX21qz4rgp.jpg)

El resultado salta a la vista. Un álbum exclusivo, y sobre todo la ilusión de mi niña de haberlo hecho entre las dos y de que por fin le “levantara” el veto de las Monster High.

Tus hijas tienen pasión por la Monster también?