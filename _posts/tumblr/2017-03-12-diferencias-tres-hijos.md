---
date: '2017-03-12T20:02:32+01:00'
image: /tumblr_files/tumblr_nlltphCxm71t5aiezo1_500.gif
layout: post
tags:
- familia
title: Diferencias de dos a tres hijos
tumblr_url: https://madrescabreadas.com/post/158319088699/diferencias-tres-hijos
---

Los padres y madres de familia numerosa a menudo nos vemos comprometidos con preguntas que no siempre son fáciles de contestar, como la que me hizo una mamá del colegio, preocupada por el canbio que supondría en su maternidad la llegada de un tercer hijo. Siempre digo que a veces se nos confunde con un manual de parenting, y que no hay formas de educar mágicas, que yo solamente puedo hablar desde mi experiencia como madre de 3 niños, uno pequeño, otro en la pubertad, y otra, entrando en la adolescencia. Espero que os sirva mi respuesta!

> ¿Hay mucha diferencia de dos a tres hijos?

 ![](/tumblr_files/tumblr_nlltphCxm71t5aiezo1_500.gif)

Esta es la pregunta del millón. Si sois familia numerosa seguro que os la han hecho más de una vez. Por eso se me ha ocurrido hacer una lista con las principales diferencias, pero seguro que a ti se te ocurren otras tantas:

-Antes tenías una mano para cada hijo.

> Ahora tienes las mismas manos, pero te tienen que servir para tres.

Para ir andando por la calle con todos te falta una, y si empujas el carrito te faltan otras dos.

 ![](/tumblr_files/tumblr_o68zozqIVm1qigfjto1_500.gif)

Pero hay soluciones: Si uno es bebé, el portero te hace recuperar tus dos extremidades superiores, y si vas con carrito, puedes usar un patín para uno, y al otro tendrás que convencerlo para que camine sujeto al carro.

-Sigues teniendo dos ojos, pero tres puntos de atención, lo que hace bastante estresante vigilarlos en el parque o en la playa, sobre todo cuando cada uno sale corriendo en una dirección. 

 ![ninos-jugando-castillos-arena](/tumblr_files/tumblr_inline_ompt0syPqi1qfhdaz_540.jpg)

 -Tu único cerebro tiene ahora tres focos de preocupación (más los que tuvieras antes de ser madre, claro), y muchas veces no nos cabe todo en la cabeza.

> Si es cierto que sólo utilizamos un tercio de nuestro cerebro, quizá deberíamos buscar la forma de desbloquear los otros dos que nos faltan, 

como si se tratara de un juego de esos del móvil.

 ![](/tumblr_files/tumblr_nx7qfaiLYV1ukxz06o1_400.gif)

-Tienes un sólo corazón, pero con esto no tendrás problema, no te equivoques, cuanto más amor da más tienes. 

-Tienes solamente unos labios para dar besos, y dos manos para acariciar, pero los totum revolutum en el sofá cual manada de leones son geniales para repartir cariño y recibir alguna que otra patada fortuita.

 -Sois, como mucho, dos adultos, 

> ellos os superarán en número

En momentos de caos tendréis las de perder.

-Tres no es divisible entre dos, lo que dificulta la logística caso de que cada uno tenga una actividad en u sitio diferente.

- Los posibles canguros se espantan al verse en clara minoría ante más de dos niños. 

> Llegarás a ver el miedo en sus ojos,

 y los abuelos ya no están para tantos trotes como antes…

- Ya no podréis llevar a la abuela en el coche. Incluso se os quedará justo para vosotros cinco, y si os descuidáis no os cabrán las sillitas, y alguno acabará sentado medio de lado.

 ![](/tumblr_files/tumblr_nhvk79t7261r8dy8go1_400.gif)

- Las mayoría de las habitaciones de los hoteles no aceptan más de cuatro personas, por lo que toca meter a uno en la maleta y colarlo pagar dos habitaciones.

> Adiós hoteles, hola [Airbnb](http://www.airbnb.es/c/msanchez1919)

- Hay restaurantes en los que no te atienden. Esto no me ha pasado a mí, pero a mi amiga [Martina](http://www.nosinmishijos.com/), sí.

- Los premios, ofertas, promociones para familias suelen ser para 4. Ohhh…!! 

> Dejamos de ser una familia stándar… 

pero nosotros: “O vamos todos o ninguno!”

-Pero por fin te encajarán las ofertas tres por dos y no te sobrará ningún zumo de los packs Bifrutas

- Nos salimos de la media y empiezan las miradas interrogantes y las caras de sorpresa:

 ![](/tumblr_files/tumblr_m3buzarCOX1r1z733o1_500.gif)

>  "son todos tuyos?“

 "No, mire, uno lo he pedido prestado para hacer bulto”

- Prepárate para dar explicaciones, o mejor, cortes… porque la gente empieza a preguntarse (y los más osados en voz alta) el por qué de un tercer hijo.

> Acaso nos hemos vuelto locos? 

> Ha sido sin querer? 

- Y ya, cuando comprueban que no ha sido por buscar “El niño” o “la niña” que faltaba (única razón por la que les encajaría “buscar” un tercer descendiente) porque los dos mayores ya cubren todos los géneros posibles, entonces piensan pasa sus adentros (aunque otros maleducados también lo dicen), que habrá sido un accidente.

-Surgen preguntas incómodas que no te hacían cuando tenías dos, como

> Ya os plantais no?

>  Pero vais a tener más?

>  Tú no te aburres, no?

>  Qué entretenida estás!

 -Cuando hace tiempo que un conocido no te ve, en vez de preguntarte por la salud te soltará:

> ¿Cuántos hijos tienes ya?

Oye, qué preocupación tiene la gente, ni que los tuvieran que criar ellos. Deberían estar contentos porque lo mismo les da para cobrar la pensión gracias a ellos, jolines!

-Se sorprenderán de que no vayas en chandal, con el pelo revuelto, ojeras hasta el suelo y los niños subiéndote por la espalda todo el día.

-Les admirará verte llegar al colegio por las mañanas con los tres y “siempre sonriendo”, como me dijo una mamá en el patio una vez, y la verdad que me emocionó mucho.

- Cuando se junten con más niños tus hijos marcarán tendencia en los juegos y será frecuente que los demás niños acaben uniéndose a ellos.

 ![paisaje-vinedos-ninos-jugando](/tumblr_files/tumblr_inline_ompt5pv9121qfhdaz_540.jpg)

- En algunos campamentos o actividades los adorarán porque, una vez que se apuntan ellos, hacen masa crítica y animarán a otros a apuntarse.

- Cuando se hagan mayores y 

> se pongan de acuerdo para reivindicar algo, llegarás a sudar.

 ![](/tumblr_files/tumblr_inline_ompt6rRWS91qfhdaz_540.jpg)

Se me ocurren muchas más diferencias, porque dar el salto a ser familia numerosa es dar el gran salto de tu vida, y apostar por algo grande, que requerirá lo mejor de ti, pero te aportará mucho más de lo que imaginas.

Dime, 

## ¿qué otras diferencias has notado al dar el salto de dos a tres hijos?