---
date: '2014-11-25T13:33:00+01:00'
layout: post
tags:
- crianza
- trucos
- adolescencia
- libros
- planninos
- recomendaciones
- educacion
- familia
title: 'Libros juveniles interactivos: Crea tu propio comic con Coolman y gana un
  libro'
tumblr_url: https://madrescabreadas.com/post/103547378899/libros-juveniles-comic
---

**Coolman y yo**

 Es un libro infantil y juvenil recomendado a partir de los 12 años de edad, con los ingrediente necesarios para divertir y enganchar a todo tipo de lectores, pues la combinación de narración y cómic hacen que el libro resulte muy atractivo.Kai es un joven de once años aparentemente normal si no fuera por el hecho de que desde que tiene uso de razón, desde los 4 años mas o menos, tiene que convivir, no sólo con su hermana y sus padres, sino también con Coolman, un héroe a quien sólo Kai puede ver y que siempre le enreda en situaciones comprometidas que le conducen al desastre. ¡Pero nunca es culpa de Coolman!

**¿Cómo puedes crear tu propio cómic?**

La actividad 

**Coolman y tú **

consiste en un

juego on-line gratuito

, diferente y divertido que permite a los niños crear diferentes historias en formato cómic sobre los personajes de la colección Coolman y yo: Coolman y Kai. Este juego permite que, partiendo de un mismo inicio, podáis crear vuestro propio cómic pasando por 4 pantallas diferentes. Una vez finalizada la historia, podréis ver el conjunto de tiras cómicas que habéis elegido de manera interactiva, imprimirla y compartirla. Además, tenéis la opción de volver a empezar y realizar un nuevo y diferente cómic, pudiendo llegar a crear hasta 16 historias diferentes!