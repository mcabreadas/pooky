---
date: '2018-08-19T17:45:50+02:00'
image: /tumblr_files/tumblr_inline_pdpssehFGG1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- Maternidad
- cosasmias
title: Soy una madre histérica
tumblr_url: https://madrescabreadas.com/post/177167209814/madre-histerica
---

No soy una madre histérica… o sí… no lo sé, pero lo que sí sé es que la [selección natural](https://es.wikipedia.org/wiki/Selecci%C3%B3n_natural) funciona, y que si la mayoría de madres que conozco son así o más exageradas con sus hijos es porque es necesario para la supervivencia de la especie.

Os pongo varios ejemplos recientes para que lo entendáis:

## Ejemplo1

Gracias a mi “histerismo” detectaron la pérdida de audición de mi hijo pequeño y evitamos que el problema fuera a más. El caso es que le hicieron una audiometría “para que la madre se quede tranquila”, y en cuanto el otorrino vio el resultado, inmediatamente decidió operarlo.

**Madre histérica 1- criticones 0**

## Ejemplo 2

Si me hubieran dejado ser una madre histérica hubiera evitado que mi hijo tuviera una accidente en la [bicicleta](https://madrescabreadas.com/2018/07/15/ensenar-montar-bici.html) este verano (no os preocupéis, no fue nada para lo que podría haber sido):

Se me tachó de exagerada porque no quería que fuera de paquete con su hermano porque por experiencia (propia y ajena) sé que la probabilidad de piñazo se triplica. Pues, efectivamente, metió el pie en los radios, y los dos niños al suelo.

De repente oí gritos y salí de casa para encontrarme un camión parado taponado la calle y, supuestamente, mis niños delante de él o debajo, en ese momento no lo sabía.

 ![](/tumblr_files/tumblr_inline_pdpssehFGG1qfhdaz_540.jpg)

Salí corriendo cual madre histérica descompuesta, levanté a mi hijo del suelo, un chico me ayudó a sacarle la pierna de los radios, lo cogí en brazos, y me lo llevé inmediatamente a casa a ponerle hielo y darle ibuprofeno.

Ni siquiera se le hinchó porque mi histerismo me hizo actuar con rapidez y determinación: primero me hizo detectar el accidente casi por intuición porque a penas sería nada dentro de la casa, después mi histerismo me hizo llevarlo a casa rápidamente, mandar a su hermano a la farmacia a por ibuprofeno y ponerle el hielo durante un cuarto de hora. 

Es cierto que podrían haberme puesto tan nerviosa porque realmente no pasó nada grave, pero yo me pregunto:

¿Sin ese nervio habría actuado de forma tan eficaz?

**Madre histérica 1- criticones 0**

## Ejemplo 3

Pero diréis que no siempre tenemos razón las madres histéricas, y es cierto, no os equivocáis. Y para ser justos, también os voy a contar un caso en el que mi histerismo falló:

Era de noche y estábamos tomando algo con unos amigos en una terraza, se hizo tarde y el pequeño empezó a decir agobiado que le dolía al respirar. Insistió y se puso a llorar. Entonces, pensando que repasaba algo grave, rompí rápidamente la reunión y me lo llevé a casa. Después resulta que resultaron ser gases. Esta vez mi instinto falló.

**Madre histérica 0- criticones 1**

Bromas aparte, las madres tenemos un sexto sentido, y cada vez confío más en él porque suele acertar. Cuando nos da un pálpito debemos tenerlo muy en cuenta y hacerlo valer ante quien sea necesario porque estadísticamente solemos tener razón, y criticones nos vamos a encontrar por todas partes.

Ahora decidme si os ha pasado algo parecido o es que estoy equivocada…