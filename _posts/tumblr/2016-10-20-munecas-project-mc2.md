---
date: '2016-10-20T10:00:11+02:00'
image: /tumblr_files/tumblr_inline_of3hseK4Ac1qfhdaz_540.jpg
layout: post
tags:
- crianza
- adolescencia
- planninos
- colaboraciones
- ad
- eventos
- familia
- juguetes
title: Muñecas científicas Project MC2
tumblr_url: https://madrescabreadas.com/post/152061596027/munecas-project-mc2
---

La semana pasada **[FAMOSA](http://www.famosa.es/es/marcas-juguetes/project-mc2/)** presentó en Madrid, concretamente en el Museo de Ciencias Naturales, las muñecas que ha lanzado al mercado con la intención de incluir en el juego la ciencia.

Mónica, de [Paraíso Kids](http://www.paraisokids.es/) fue en nombre de este blog a conocerlas con sus dos hijas para contárnolso todo:

> En colaboración con el blog Madres Cabreadas me dirigí a la dirección, entregada y entusiasmada con mis dos hijas (no sé quién iba más emocionada) me presenté en el Museo de Ciencias. Hacía años y años que no iba al museo y me emocionó reencontrarme con él ☺
> 
> Allí nos esperaban un montón de cositas chulas.
> 
> Las niñas iban a realizar un taller científico mientras a la prensa nos presentaban las muñecas.

 ![](/tumblr_files/tumblr_inline_of3hseK4Ac1qfhdaz_540.jpg)

> Entretanto la peque alucinaba con ellas, a la mayor le llamó la atención una pequeña charca que habían preparado encima de una mesa en la cual, a través de una pipeta cogías un poquito de agua y lo ponías en el microscopio para poder ver la cantidad de insectos que había en ella y que a simple vista no veíamos.
> 
> El acto fue presentado por la periodista científica América Valenzuela.

## Las protagonistas de la serie hechas muñecas

> Tras la [serie Project <sup>MC2 </sup>que se emite en **Netflix**](https://www.netflix.com/es/title/80058852) con un exitazo asombroso entre las adolescentes de nuestro país, se encuentran cuatro chicas bonitas reclutadas para unirse a una organización de espionaje llamada NOV8.
> 
> **FAMOSA** , compañía juguetera internacional, ha querido llevar al mundo infantil a estas inteligentísimas chicas y transformarlas en muñecas con las que las peques de entre 6 y 10 años podrán jugar a ser científicas o a ser agentes secretos.
> 
> Bajo el lema SITNC ¿Cómo te has quedado? ¿Qué no sabes qué significa estas siglas? Jajajaja. Significa Smart is the New Cool ¡Toma! Cada una de las muñecas viste un original look y se presentan acompañadas de un experimento relacionado con la tendencia S.T.E.A.M (Science, Technology, Engineering, art and maths)

## Despertando el interés científico

> Estas muñecas tienen algo especial. Cada una tiene un estilo específico: **McKeyla McAlister** se define como una escritora amante del mundo de la magia.

 ![](/tumblr_files/tumblr_inline_of3hb1qyUA1qfhdaz_540.jpg)

> **Bryden Bandweth** se define como bloguera y le encantan las RRSS.

 ![](/tumblr_files/tumblr_inline_of3hcroljV1qfhdaz_540.jpg)

> **Adrenne Attoms** se define como una cocinera admiradora de recetas químicas ¡claro!

 ![](/tumblr_files/tumblr_inline_of3hfjDJCZ1qfhdaz_540.jpg)

> Y a **Camryn Coyle** le apasiona la construcción creativa.

 ![](/tumblr_files/tumblr_inline_of3hgtBi571qfhdaz_540.jpg)

> Las familias podrán encontrar en las jugueterías estas muñecas en un pack que incluye muñeca + experimento ¡Tranquilas! Estos experimentos son sencillos y se pueden hacer con ingredientes caseros:

- [McKeyla McAlister incluye una lámpara de lava](https://www.amazon.es/gp/product/B00X9MJF7K/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00X9MJF7K&linkCode=as2&tag=madrescabread-21)  
- [Bryden Bandweth incluye un collar fluorescente](https://www.amazon.es/gp/product/B00X9MJW8C/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00X9MJW8C&linkCode=as2&tag=madrescabread-21)  
- [Adrienne Attoms incluye un volcán en erupción](https://www.amazon.es/gp/product/B00X9MJDV8/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00X9MJDV8&linkCode=as2&tag=madrescabread-21)  
- [Camryn Coyle incluye un monopatín motorizado](https://www.amazon.es/gp/product/B00X9MJVRY/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00X9MJVRY&linkCode=as2&tag=madrescabread-21)  
 ![](/tumblr_files/tumblr_inline_of3hl9WSod1qfhdaz_540.jpg)

> La gama de productos sobre esta exitosa serie se amplía con un **Diario Interactivo** El [diario interactivo](https://www.amazon.es/gp/product/B00UMSU7OY/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00UMSU7OY&linkCode=as2&tag=madrescabread-21) con tecnología digital avanzada y proceso de desbloqueo a través de una pulsera NErDy, que incluye bolígrafo con tinta invisible que permite escribir mensajes ocultos que sólo pueden ser leídos con la linterna de luz UV; y un **kit de laboratorio** compuesto por un [maletín rojo de experimentos](https://www.amazon.es/gp/product/B01GE1ZLFC/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01GE1ZLFC&linkCode=as2&tag=madrescabread-21)con más de 30 piezas adicionales que permitirá a los niños realizar más de 15 experimentos extras.

 ![](/tumblr_files/tumblr_inline_of3hjrn1tb1qfhdaz_540.jpg)
## La serie hecha película en Disney Channel

> ¡Notición!
> 
> Queridas adolescentes, nuestra querida The Walt Disney Company, a través de su canal Disney Channel emite la película Project Mc<sup>2</sup> para dar a conocer la vida secreta de las protagonistas hasta el mes de diciembre. Aquí os dejo los pases para que no os lo perdáis
> 
> 23 de octubre a las 17 horas
> 
> 27 de noviembre a las 17 horas
> 
> 4 de diciembre a las 17 horas

## Apúntate a los talleres Project MC<sup>2</sup> en el MNCN de Madrid

> Además, si os emociona el mundo de la ciencia y la investigación, durante el mes de octubre y noviembre, el [Museo de Ciencias Naturales de Madrid va a desarrollar cuatro talleres](http://www.mncn.csic.es/Menu/Actividades/Pblicoindividual/Actividadesfindesemana/seccion=1251&idioma=es_ES.do) (dirigidos a niños de 6 a 12 años) para que puedan conocer de primera mano los experimentos que caracterizan a las cuatro nuevas muñecas.

Muchas gracias, Mónica por acercarnos a la ciencia y a estas muñecas tan interesantes, que sin duda despertarán la curiosidad científica de nuestras hijas.

Una pregunta, ¿con cuál de las cuatro te identificas?