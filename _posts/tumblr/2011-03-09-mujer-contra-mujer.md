---
layout: post
title: Mujer contra mujer
date: 2011-03-09T19:42:00+01:00
image: /images/posts/mujer-contra-mujer/p-atletismo.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/3746195747/mujer-contra-mujer
---

Sorprende cómo las propias mujeres son las que ponen más trabas, a veces, para una posible conciliación de la vida laboral y familiar de otras mujeres (de los hombres ni se lo plantean, claro). Lo digo por conversaciones en el patio del colegio o en cumpleaños de los niños. Os pongo un ejemplo real.

Eva es madre de 2 niños y ocupa un alto cargo en la Administracion Autonómica. Ella vive entra dos aguas debatiéndose entre sus sentimientos de madre y su meteórica carrera profesional. Tiene reuniones a altas horas de la tarde casi a diario, y viaja continuamente.

Un día se le ocurrió plantear dejar dos tardes libres de reuniones a la semana para estar con sus hijos y su superior la llamó al orden y la dejó en ridículo ante sus compañeros por utilizar ese criterio para elegir un día u otro.

Eva se queja de esto al mismo tiempo que lo asume y lo acepta para no perder su cargo ya que, según cuenta, su compañera Rocío era “muy buena antes de tener hijos”, luego la rebajaron de puesto y la dejaron de administrativa porque no podía echar tantas horas ni viajar.

Lo sorprendente es que la propia Eva sea la que se refiera a la calidad del trabajo de Rocío en pasado: “era muy buena”, como si no fuera la misma persona antes y después de ser madre… Después de esta conversación Eva se marcha del cumpleaños con lágrimas en los ojos porque tenía una reunión y nos deja a cargo de su hijo, que se encontraba enfermo. Pero no tenía más remedio.

Al cabo de dos horas la llamamos porque el niño empeora y llegó con el corazón que se le salía del pecho. Esta mujer estaba sufriendo. Se ha visto obligada a elegir entre su vida familiar y su carrera profesional, y ha tomado una decisión que la lleva cada día a plantearse cambiarla, estoy segura. Creo que nadie merece pasar por esto.

No hay derecho, y me cabrea.

Te ha pasado algo parecido?