---
date: '2015-12-01T19:14:26+01:00'
image: /tumblr_files/tumblr_inline_nyoaj5u5ky1qfhdaz_540.jpg
layout: post
tags:
- navidad
- crianza
- trucos
- reyesmagos
- regalos
- premios
- colaboraciones
- puericultura
- ad
- familia
- juguetes
- seguridad
title: Concurso Tu Carta a los Reyes Magos 2016
tumblr_url: https://madrescabreadas.com/post/134342569309/juguetes-navidad-gratis
---

![](/tumblr_files/tumblr_inline_nyoaj5u5ky1qfhdaz_540.jpg)

## Ganadores

Suena tópico, pero os prometo que es rigurosamente cierto. Nos ha costado elegir los ganadores Dios y ayuda porque os lo habéis currado este año de lo lindo. Vuestras cartas nos han emocionado, nos han hecho reír… He disfrutado muchísimo con el concurso porque creo que sacamos lo mejor de nosotros mismos cuando llega la Navidad, y vuestras cartas así lo demuestran.

Ojalá tuviera tantos regalos como participantes, porque ninguna carta tiene desperdicio, y se nota que están escritas desde el corazón, y para alguien que os importa mucho.

G R A C I A S

Siento haberme enrollado tanto. Ya os digo los ganadores de todos los regalos menos de la silla Exo de Jané, que se comunicará a través de Telegram tal y como os indiqué. Dejad de morderos las uñas:

## Juego de mochila y estuche marca Lois
Mamá en Bulgaria
## 3 Mega Espadas de los Power Rangers Super Megaforce ¡3 ganadores!.
Álvaro González Jurado, Ana Pardo y Fátima.
## “kit confort” de Babymoov formado por una lamparita “quitamiedos” Tweesty + un cojín reductor Cosymorpho
Cuentos de Amatxu
## Cenicienta bailarina Chicco
Milagri Sevilla Carrasco
## Alfombra de actividades Beach Bebé de Saro Baby
Palo.falsaria
## Organizador de juguetes Play & Go de Tutete.com 
Elena
## Set Lego Ultra Agents

Susana

Enhorabuena a todos, y por favor pasadme por email nombre, apellidos, dirección y teléfono cuanto antes para que les de tiempo a los Reyes Magos a llevároslos a casa.

info@madrescabreadas.com

Post originario:

Este año celebramos la Navidad en el blog de un modo muy especial, ya que los Reyes Magos de oriente me han nombrado su paje virtual porque se han enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me han pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

Para ello te voy a pedir que  **escribas tu propia carta como padre o madre a los Reyes Magos** , en la que pondrás el regalo que le pides para tus peques de entre los que a continuación enumeraré.

Sé creativo, porque la carta me la tienes que poner en un comentario en el post del regalo que elijas (se admiten fotos), y ganará la más original!

## Reglas del juego:

1-Elige el regalo que más te guste y pincha sobre la imagen para que te lleve al post de ese regalo.

2-En el post del regalo elegido deja en un comentario tu carta a los Reyes Magos.

Sé original porque ganará la carta más original y auténtica, a elegir por la marca.

3-Cumple con los requisitos que se piden en el post de cada regalo.

4-El plazo para subir tu carta finaliza el 15 de diciembre, y el resultado se publicará en este blog en los días sucesivos.

5-El ámbito del concurso depende de cada regalo. Consulta en el post de ada uno.

## Pincha en el regalo que quiereas para escribir tu carta:

## 

## Juego de mochila y estuche marca Lois
[

[![](/tumblr_files/tumblr_inline_nylku0M88q1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_nylku3aLKd1qfhdaz_540.jpg)](/2015/12/01/mochila-navidad-lois.html)

## 3 Mega Espadas de los Power Rangers Super Megaforce ¡3 ganadores!.

[![](/tumblr_files/tumblr_inline_nylke97UVT1qfhdaz_540.jpg)](/2015/12/01/espada-power-rangers.html)

## “kit confort” de Babymoov formado por una lamparita “quitamiedos” Tweesty + un cojín reductor Cosymorpho

[![](/tumblr_files/tumblr_inline_nymiu3RBYX1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_nyq3a18A771qfhdaz_540.jpg)](/2015/12/01/regalo-bebe-navidad.html)

## Cenicienta bailarina Chicco

[![](/tumblr_files/tumblr_inline_nykrsrbu0B1qfhdaz_540.jpg)](/2015/12/01/bailarina-chicco-regalo.html)

## Silla de automóvil Exo de Jané Grupo I

[![](/tumblr_files/tumblr_inline_nykrgkwG321qfhdaz_540.jpg)](/2015/12/01/silla-automovil-regalo.html)

## Alfombra de actividades Beach Bebé de Saro Baby

[![](/tumblr_files/tumblr_inline_nykr6p9vpg1qfhdaz_540.jpg)](/2015/12/01/alfombre-bebe-regalo.html)

## Organizador de juguetes Play & Go de Tutete.com 

[![](/tumblr_files/tumblr_inline_nykqukCQ411qfhdaz_540.jpg)](/2015/12/01/organizador-juguetes-regalo.html)

 ](http://www.loisjeans.com/es/ES/home)
## Set Lego Ultra Agents

[![](/tumblr_files/tumblr_inline_nyovi0K8W71qfhdaz_540.jpg)](/2015/12/01/juguete-lego-navidad.html)