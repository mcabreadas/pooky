---
date: '2015-10-29T08:54:12+01:00'
image: /tumblr_files/tumblr_inline_o39z7cgnpi1qfhdaz_540.jpg
layout: post
tags:
- planninos
- colaboraciones
- ad
- eventos
- familia
- moda
title: El primer desfile de moda de mis niños
tumblr_url: https://madrescabreadas.com/post/132136687499/moda-infantil-pasarela
---

Este fin de semana llevé a las fieras a la pasarela de moda infantil Valencia Petit Walking que organiza la [revista Petit Style](http://www.petitstyle.es/Petit_Style/Bienvenida.html) y [Miramami](http://www.miramami.com/#) en el Mercado Colón.

 ![](/tumblr_files/tumblr_inline_o39z7cgnpi1qfhdaz_540.jpg)

Aunque yo ya había asistido a la del año pasado, y sabía que seguramente les encantaría, como era su primera experiencia en un desfile de moda, y los niños son impredecibles, no sabía cómo iban a reaccionar. 

¡Pero se lo pasaron pipa!

## Recibimiento

Primero participaron en un taller de música súper novedoso (al menos yo no lo había visto antes) a cargo de [Utem Escola de música](http://www.utem.es), que me pareció extraordinario, y sirvió para que los niños, tanto los que desfilaban como los que iban de público, rompieran el hielo y se integraran totalmente en el ambiente.

 ![](/tumblr_files/tumblr_inline_o39z7cANHV1qfhdaz_540.jpg)
## Ultimando detalles

Mientras, se preparaban los últimos detalles para la pasarela.

 ![](/tumblr_files/tumblr_inline_o39z7eCeUs1qfhdaz_540.jpg)

Y las cámaras se preparaban.

 ![](/tumblr_files/tumblr_inline_o39z7f4Oep1qfhdaz_540.jpg)

Nervios en el Backstage y carreras para colocarse en riguroso orden de aparición.

 ![](/tumblr_files/tumblr_inline_o39z7fLfOd1qfhdaz_540.jpg)
## Presentación

Por fin suena la música y comienza el desfile, que abrió Albert Jiménez en nombre de [Padres 2.0](http://padres20.org/), la única ONG dedicada a las nuevas tecnologías y a la que se puede acudirsi necesitamosayuda en caso de que nuestros hijos sufran ciberacoso, por ejemplo. Tomad nota, porque me parece una labor muy importante en los tiempos que corren, y muchos padres no sabríamos cómo actuar caso de que sucediera algo así.

 ![](/tumblr_files/tumblr_inline_o39z7g7FYC1qfhdaz_540.jpg)
## Hortensia Maeso

La primera firma que nos dejó boquiabiertos es arte puro. [Hortensia Maeso](http://byhortensiamaeso.com/) nos presenta auténticas princesas en su colección de trajes de Primera Comunión. 

Vestidos de cuento, con detalles que los hacen exclusivos, y grandes, pero elegantes tocados entusiasmaron al público.

Los trajes de Comunión de niño dan una vuelta de tuerca al clásico marinero, para modernizarlo con un estilo súper original y actualizado, pero sin perder la elegancia que requiere un día tan especial.

 ![](/tumblr_files/tumblr_inline_o39z7gBbDN1qfhdaz_540.jpg)<map name="Map" id="Map"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwufkzYn3L1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,3,144,267">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuha7FqLj1qfhdaz_540.jpg" target="_blank" shape="rect" coords="145,2,238,129">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhacWFf91qfhdaz_540.jpg" target="_blank" shape="rect" coords="248,8,340,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhaixSwz1qfhdaz_540.jpg" target="_blank" shape="rect" coords="345,3,439,133">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhan6AGX1qfhdaz_540.jpg" target="_blank" shape="rect" coords="442,5,535,135">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhatSdF81qfhdaz_540.jpg" target="_blank" shape="rect" coords="149,137,241,267">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhaz7lmi1qfhdaz_540.jpg" target="_blank" shape="rect" coords="246,134,341,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwuhb6zIjl1qfhdaz_540.jpg" target="_blank" shape="rect" coords="345,139,536,266">
<p>Pincha sobre cualquier imagen para ampliarla.</p>
<h4>Lourdes Moda Infantil</h4>
<p><a href="http://www.lourdesmodainfantil.com/" target="_blank">Lourdes Moda Infantil</a> llenó la pasarela de Búhos en una colección donde este animalito que está tan de moda es el protagonista. El poncho me parece una maravilla.</p>
<p>Pero si buscamos algo más formal, pero no queremos caer en lo clásico, tenemos estos conjuntos en los que destaca el gris, y que combinan genial con estas botas negras acolchadas de charol </p>
<img src="/tumblr_files/tumblr_inline_o39z7gze5l1qfhdaz_540.jpg" usemap="#Map2" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwvguyzyop1qfhdaz_540.jpg"><map name="Map2" id="Map2"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgochlKo1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,5,134,267">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgokIVXw1qfhdaz_540.jpg" target="_blank" shape="rect" coords="140,5,237,134">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgoqeoRX1qfhdaz_540.jpg" target="_blank" shape="rect" coords="241,6,336,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgowxUyK1qfhdaz_540.jpg" target="_blank" shape="rect" coords="342,5,538,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgp2sTsC1qfhdaz_540.jpg" target="_blank" shape="rect" coords="141,137,235,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgp8Ciif1qfhdaz_540.jpg" target="_blank" shape="rect" coords="239,139,336,264">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgpgtTD41qfhdaz_540.jpg" target="_blank" shape="rect" coords="339,139,438,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwvgplvAKm1qfhdaz_540.jpg" target="_blank" shape="rect" coords="440,137,537,265">
<p>Pincha sobre cualquier imagen para ampliarla.</p>

<a href="http://shop.condor.es/" target="_blank"><h4>Cóndor</h4></a><p>Cóndor, la marca de los leotardos que usábamos cuando éramos pequeñas, tiene también una línea de ropa infantil muy interesante. Me quedo con sus espectaculares abrigos en varios estilos, de capa, o corte tres cuartos más clásico. Y los colores vivos que presenta me encanta, sobre todo el mostaza, que parece que va a ser un must este invierno.</p>

<img src="/tumblr_files/tumblr_inline_o39z7hpAiJ1qfhdaz_540.jpg" usemap="#Map3" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtpfe3qL51qfhdaz_540.jpg"><map name="Map3" id="Map3"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwxf8uvHH51qfhdaz_540.jpg" target="_blank" shape="rect" coords="7,6,114,264">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxf95oWll1qfhdaz_540.jpg" target="_blank" shape="rect" coords="121,2,217,133">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxf9fQLHS1qfhdaz_540.jpg" target="_blank" shape="rect" coords="221,3,317,134">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxf9k3pgJ1qfhdaz_540.jpg" target="_blank" shape="rect" coords="324,3,420,133">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxfaxWXcb1qfhdaz_540.jpg" target="_blank" shape="rect" coords="424,1,536,131">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxf9voetA1qfhdaz_540.jpg" target="_blank" shape="rect" coords="121,138,421,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxfbjlnM01qfhdaz_540.jpg" target="_blank" shape="rect" coords="425,135,537,265"></map><p>Pincha sobre cualquier imagen para ampliarla.</p>
<p><a href="http://www.bebecar.com/" target="_blank"><h4>Bébécar</h4></a></p>
<p>Nos presentó su colección exclusiva privé 2015 con tejidos dignos de la alta costura tipo chanel, estampados que recuerdan a las holografías, charoles, bordado inglés, acolchados y el color marsala, tan de moda esta temporada.</p>
<p>Como detalle práctico, la <a href="/2015/09/20/test-spot-bebecar.html" target="_blank">silla Spot cuyo test compartí aquí</a>, incorpora ahora manillar corrido, lo que para mí la convierte en la silla para segunda edad perfecta.</p>
<img src="/tumblr_files/tumblr_inline_o39z7hE6oT1qfhdaz_540.jpg" alt="" usemap="#Map4" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtpfwvbRz1qfhdaz_540.jpg"><map name="Map4" id="Map4"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi0yiqiG1qfhdaz_540.jpg" target="_blank" shape="rect" coords="7,3,97,247">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi1lClkz1qfhdaz_540.jpg" target="_blank" shape="rect" coords="103,3,191,124">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi1vntKk1qfhdaz_540.jpg" target="_blank" shape="rect" coords="193,5,280,122">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi23BUot1qfhdaz_540.jpg" target="_blank" shape="rect" coords="285,4,374,122">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi28nZqm1qfhdaz_540.jpg" target="_blank" shape="rect" coords="378,2,497,123">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi28nZqm1qfhdaz_540.jpg" target="_blank" shape="rect" coords="103,128,203,247">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi2cHvkT1qfhdaz_540.jpg" target="_blank" shape="rect" coords="206,126,373,246">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxi2h2lns1qfhdaz_540.jpg" target="_blank" shape="rect" coords="377,125,497,246"></map><p>Pincha sobre cualquier imagen para ampliarla.</p>
<h4>Zippy</h4>
<p><iframe width="100%" height="315" src="https://www.youtube.com/embed/snPNpbrs-uw?rel=0" frameborder="0" allowfullscreen></iframe></p>
<p>Confieso que he redescubierto esta firma portuguesa, que ha logrado adaptar su estilo al gusto español con ropa moderna, pero con un punto divertido e infantil, con colores como el mostaza, que personalmente me encanta.</p>
<img src="/tumblr_files/tumblr_inline_o39z7iUPpk1qfhdaz_540.jpg" alt="" usemap="#Map5" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtpg5S7Y21qfhdaz_540.jpg"><map name="Map5" id="Map5"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwxj37EXPd1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,6,145,267">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxj3bMJWU1qfhdaz_540.jpg" target="_blank" shape="rect" coords="151,4,343,128">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxj3iy9of1qfhdaz_540.jpg" target="_blank" shape="rect" coords="352,5,538,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxj3mVUge1qfhdaz_540.jpg" target="_blank" shape="rect" coords="153,136,345,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwxj3skurw1qfhdaz_540.jpg" target="_blank" shape="rect" coords="350,135,537,266"></map><p>Pincha sobre cualquier imagen para ampliarla.</p>

<p><a href="http://www.elisamenuts.com/" target="_blank"><h4>Elisa Menut</h4></a></p>
<p>Elisa Menut me encantó para vestir a los niños en una ceremonia. Me parece que es ropa cómoda y elegante a la vez, y muy ponible. Los conjuntos tienen un toque especial ideal para acertar en una Comunión, por ejemplo.</p>
<img src="/tumblr_files/tumblr_inline_o39z7iNLoH1qfhdaz_540.jpg" usemap="#Map6" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtphmK0ub1qfhdaz_540.jpg"><map name="Map6" id="Map6"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaq0orwB1qfhdaz_540.jpg" target="_blank" shape="rect" coords="5,8,106,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaq5oDkR1qfhdaz_540.jpg" target="_blank" shape="rect" coords="114,5,201,130">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqa8v601qfhdaz_540.jpg" target="_blank" shape="rect" coords="208,6,297,133">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqe6ukn1qfhdaz_540.jpg" target="_blank" shape="rect" coords="308,6,394,130">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqj2ZjV1qfhdaz_540.jpg" target="_blank" shape="rect" coords="401,4,535,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqonQSO1qfhdaz_540.jpg" target="_blank" shape="rect" coords="111,137,189,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqsWwy81qfhdaz_540.jpg" target="_blank" shape="rect" coords="192,137,288,264">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyaqxQ65W1qfhdaz_540.jpg" target="_blank" shape="rect" coords="292,138,394,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyar1F7qS1qfhdaz_540.jpg" target="_blank" shape="rect" coords="402,137,474,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwyar55U8s1qfhdaz_540.jpg" target="_blank" shape="rect" coords="477,137,536,265"></map><p>Pincha sobre cualquier imagen para ampliarla.</p>
&gt;<p><a href="http://www.rubiokids.com/" target="_blank"><h4>Rubio Kids</h4></a></p>Rubio Kids también me conquista por sus abrigos tipo poncho en tonos caldera o blanco y negro.<img src="/tumblr_files/tumblr_inline_o39z7itVQx1qfhdaz_540.jpg" usemap="#Map7" data-orig-height="270" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtpi489Wz1qfhdaz_540.jpg"><map name="Map7" id="Map7"><area alt="" title="" href="/tumblr_files/tumblr_inline_nwy9ttcmXz1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,6,96,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwy9u74Mp71qfhdaz_540.jpg" target="_blank" shape="rect" coords="101,6,230,262">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwy9ufa7D01qfhdaz_540.jpg" target="_blank" shape="rect" coords="234,3,386,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nwy9uldqtJ1qfhdaz_540.jpg" target="_blank" shape="rect" coords="389,3,536,267"></map><p>Pincha sobre cualquier imagen para ampliarla.</p>
<h4>Carrusel final</h4>
<iframe width="100%" height="315" src="https://www.youtube.com/embed/_fCcTdbUHaM?rel=0" frameborder="0" allowfullscreen></iframe><p>Mis hijos lo pasaron en grande con la lluvia de mariposas,y acabaron subidos a la pasarela jugando a que desfilaban como auténticos modelos.</p>
<img src="/tumblr_files/tumblr_inline_o39z7jWthG1qfhdaz_540.jpg" data-orig-height="538" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtpwhCS2t1qfhdaz_540.jpg"><h4>Networking</h4>
<p>En el momento networking tuvimos la ocasión de hablar con la responsable de marketing de Bébécar, Natalia Salen, y con el presidente de la compañía, que nos puso al día de las novedades en materia de seguridad infanti.</p>
<p>También tuve el placer de conocer e intercambiar impresiones con Carmen Calderón, Brand Manager de Zippy Spain, y de reencontrarme con mis compañeras bloggers, Clara, de <a href="http://www.saquitodecanela.com/Saquito" target="_blank"> Saquito de Canela</a>, y María José Sarrión, de <a href="http://www.alcobadeblanca.com/" target="_blank">La Alcoba de Blanca</a>, con quienes siempre es un gusto volver a hablar.</p>
<img src="/tumblr_files/tumblr_inline_o39z7jV8641qfhdaz_540.jpg" data-orig-height="405" data-orig-width="540" data-orig-src="/tumblr_files/tumblr_inline_nwtr1vVZZ81qfhdaz_540.jpg"><p>Mientras, mis hijos disfrutaron de una rica merienda gentileza de la organización.</p>
<img src="/tumblr_files/tumblr_inline_o39z7lWnDh1qfhdaz_540.jpg" data-orig-width="540" data-orig-height="405" data-orig-src="/tumblr_files/tumblr_inline_nwtpsycLuj1qfhdaz_540.jpg"><p>Muchas gracias a <a href="http://www.petitstyle.es/Petit_Style/Bienvenida.html" target="_blank">revista Petit Style</a> y a <a href="http://www.miramami.com/#" target="_blank">Miramami</a> por invitarme a pasar un rato tan agradable, y en un entorno tan bonito, como es el Mercado de Colón de Valencia. ¡Nos vemos pronto!</p>
<p>También colaboraron: <a href="http://es.calzedonia.com/" target="_blank">Calzedonia</a>, <a href="http://www.garvalin.com/es/" target="_blank">Garvalín</a></p> </map></map>

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).