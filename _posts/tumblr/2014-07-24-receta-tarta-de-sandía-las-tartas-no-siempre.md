---
date: '2014-07-24T19:02:00+02:00'
image: /tumblr_files/tumblr_n903geqHaR1qgfaqto1_1280.jpg
layout: post
tags:
- recetas
title: receta tarta de sandía las tartas no siempre
tumblr_url: https://madrescabreadas.com/post/92742254803/receta-tarta-de-sandía-las-tartas-no-siempre
---

![](/tumblr_files/tumblr_n903geqHaR1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_n903geqHaR1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_n903geqHaR1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_n903geqHaR1qgfaqto4_r1_500.jpg)  
Creación de Noe del Barrio  

## Receta tarta de sandía 

Las tartas no siempre tienen que estar plagadas de azúcar y calorías. Os traigo una manera atractiva de presentar la fruta a los niños que aprendí en casa de unos buenos amigos. No me negaréis que tiene una pinta estupenda.

## Ingredientes de la tarta de sandía
 -1 sandia gigante sin semillas o una grande y otra pequeña.  
 -250 g de moras negras y rojas  
 -2 plátanos  
 -1 piña (opcional)  
 -palillos mondadientes
## Modo de hacer la tarta de sandía
 Se corta la sandia en forma hexagonal reservando un trozo para colocar en la parte de arriba con la misma forma, pero más pequeña.

Se decora alrededor y por arriba al gusto combinando los colores de las frutas pinchando los trocitos en la sandia con los palillos con cuidado de que no se queden a la vista.  
 Nosotros no pusimos piña, pero da un toque colorido precioso a la tarta.

Puedes servirlo en un plato grande con colores vistosos. A nosotros nos lo pusieron en uno giratorio decorado precisamente como si fuera una rodaja sandía.

O puedes dar rienda suelta a tu arte y hacer como mi amiga [Noe del Barrio](https://www.facebook.com/afriquezzz?hc_location=timeline), que dejó a todo el mundo con la boca abierta con su creación (última foto).

Consejo:  
 Si lo metes en el congelador tres minutos antes de servirlo quedará como una tarta helada y la textura será deliciosa.

¿Te atreves?