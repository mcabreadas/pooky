---
date: '2013-05-17T12:32:00+02:00'
image: /tumblr_files/tumblr_inline_mn0b5o2qZp1qz4rgp.jpg
layout: post
tags:
- mecabrea
- crianza
- juguetes
- familia
title: De Monsunos, Basurillas y otros especímenes coleccionables.
tumblr_url: https://madrescabreadas.com/post/50644363233/de-monsunos-basurillas-y-otros-especímenes
---

Vaya tela con las modas de muñecajos y cartas de los peques.

Entre los Monsuno, los Invizimals y los Basurillas me llevan loca mis hijos…

Ayer me pidió mi mediano, también llamado Osito (por mí), un sobre de Monsuno. En realidad ya me lo había pedido el otro día, pero cuando me dijo el chino que valian 3 € exclamé, ¡Que cromos más caros! - ” que no son cromos, mamá, que son cartas”, me respondió el pequeño (uy, que anticuada estoy, pensé yo).

 ![](/tumblr_files/tumblr_inline_mn0b5o2qZp1qz4rgp.jpg)

Pero resulta que no son cartas sólo, sino que el sobre también contiene un juguete que consiste una especie de coraza donde se guarda el Monsuno en cuestión y de la que sale disparado si se presiona un botón. Todo un invento, vamos. Pero como me lo pidió con esa gracia que lo caracteriza no pude resistirme a sus ojillos acuosos de emoción “monsunesca”, y se lo compré.

Pero, claro, Princesita quería contraprestación, y como no había “sobres de niñas”, pues allá que se apuntó a lo mismo que su hermano. Resultado: dos corazas con sendos Monsuno diferentes, con la mala fortuna de que a la niña le tocó el preferido de su hermano, con la consiguiente decepción del mismo, que se paso toooooda la tarde y toooooda la noche, incluso a la mañana siguiente (porque se lo quería llevar al cole), intentando conseguir el que le había tocado a Princesita. Primero negociando, después intentando dar pena, y finalmente a las bravas, sin conseguir su objetivo.

Para colmo, el suyo saltó por los aires impulsado desde su coraza y se le perdió. Imaginaos el llanto y la rabieta (si lo sé me quedo quietecita y no les compro nada).

Menos mal que mi niño es una persona de recursos, y se fue al cole con la coraza sólo, consiguiendo despertar aún más la expectación “monsunesca” entre sus compañeros que si ésta hubiera contenido el muñeco en su interior. Al explicarles cómo era tal especimen, yo no sé lo que se debieron de imaginar, pero a juzgar por sus caras, seguramente superaba con creces la realidad. 

En fin, que las mamás y papás tenemos que estar preparados para bichos raros de todas clases. Pero los que se llevan la palma son los “Basurillas”, una especie de muñecajos de goma que van metidos en un cubo de basura que hace honor a su nombre y que, con motivo de ello, van sucios y tienen nombres que da arcadas el simple hecho de pronunciarlos, como “granizado de meado”, “chipirón pedorrón”, “chinche churretosa”, “cacamelos”…

 ![image](/tumblr_files/tumblr_inline_mmxr0bw06o1qz4rgp.jpg)

Se supone que luchan entre ellos y miden sus fuerzas con un “roñómetro”. Sí, sí… yo me quedé igual que vosotros cuando lo vi… Muy educativo, vamos! Pues por aquí están de última moda!

 ![image](/tumblr_files/tumblr_inline_mmxr8o9RtS1qz4rgp.jpg)

 ![image](/tumblr_files/tumblr_inline_mmxs1sd60h1qz4rgp.jpg) ![image](/tumblr_files/tumblr_inline_mmxs9pdMGy1qz4rgp.jpg)

Así que, si no lo habéis hecho ya, poneos al día de toda esta jungla, que los tiempos han cambiado y lo que se colecciona hoy no son precisamente cromos de Willy Fog, ahora la cosa es mucho más “heavy”.

Que no os pille de sorpresa como a mí y se os quede cara de pánfilos.

O ya os ha pillado?