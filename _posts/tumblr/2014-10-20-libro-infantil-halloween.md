---
date: '2014-10-20T12:02:00+02:00'
image: https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=6073101554
layout: post
tags:
- crianza
- trucos
- premios
- libros
- planninos
- colaboraciones
- ad
- familia
- recomendaciones
- educacion
- diy
title: Concurso de Halloween
tumblr_url: https://madrescabreadas.com/post/100490673684/libro-infantil-halloween
---

¿Quieres ganar este terrorífico y divertido libro infantil [Bat Pat Tesoro del Cementerio](http://www.amazon.es/gp/product/6073101554/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=6073101554&linkCode=as2&tag=madrescabread-21)

 ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=6073101554)?

¿Conoces a Bat Pat? Es un simpático murciélago al que le gusta escribir historias escalofriantes que traten sobre brujas, cementerios, fantasmas…Pero además de escribir, ahora se ha metido a detective y junto a sus amigos Rebecca, Martin y Leo viven muchísimas aventuras, la última en un cementerio… ¿Te atreverás a leerlo en Halloween?

 ![image](/tumblr_files/tumblr_inline_pdpcmy1FsK1qfhdaz_540.jpg)

**Para participar en el concurso de Halloween** que organizamos con [Boolino](http://www.boolino.com) tendrás que demostrar tus habilidades y las de tus hijos **construyendo un vampiro** a partir del descargable que os podéis descargar [aquí](http://www.boolino.com/static/descargas/plantilla-bat-pat-tridimensional.pdf).

Después, sólo tenéis que **subir una foto** de vuestra creación a la web que contiene el código QR del propio descargable. Os la dejo [aquí](http://www.boolino.es/es/libros-cuentos/bat-pat-21-el-abrazo-del-tentaculo/) también.

Para saber que queréis participar es indispensable también **dejar un comentario** en este post indicándo vuestro nick de Facebook o Twitter o vuestro e-mail.

El plazo acaba la noche de Halloween, es decir, el 31 de octubre a las 00:00 h, JAJAJAJAJA… (léase con risa terrorífica…)

No os confundáis! El comentario con vuestro nick o email lo tenéis que dejar en este post, pero la foto la tenéis que subir en [este enlace](http://www.boolino.es/es/libros-cuentos/bat-pat-21-el-abrazo-del-tentaculo/).

¿Te atreves a participar?

 ![image](/tumblr_files/tumblr_inline_pdpcmzTa911qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pdpcn07PWr1qfhdaz_540.jpg)