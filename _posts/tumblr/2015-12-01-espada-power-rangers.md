---
date: '2015-12-01T19:03:43+01:00'
image: /tumblr_files/tumblr_inline_nyml4mEd491qfhdaz_540.png
layout: post
tags:
- navidad
- crianza
- trucos
- reyesmagos
- regalos
- premios
- familia
- juguetes
title: Concurso “Tu Carta a los Reyes Magos” 2016: 3 Espadas Power Ranger Super Mega
  Force
tumblr_url: https://madrescabreadas.com/post/134342043984/espada-power-rangers
---

![](/tumblr_files/tumblr_inline_nyml4mEd491qfhdaz_540.png)

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

[Aquí tenéis todos los regalos entre los que podéis elegir](/2015/12/01/juguetes-navidad-gratis.html).

Uno de los regalos que podéis incluír en vuestra carta a los Reyes Magos para participar en el concurso es una de las 3 [Mega Espada de los Power Rangers Super Megaforce](http://www.bandai.es/files/catalogo-juguetes.pdf) de Bandai.

 ![](/tumblr_files/tumblr_inline_nylkfrH3wO1qfhdaz_540.jpg)

Se trata del arma principal de los Rangers. Con luz y sonidos. Si activas la llave hace efectos exclusivos. Incluye una llave para disfrutar de increíble contenido en el App de Power Rangers.

 ![](/tumblr_files/tumblr_inline_nylke97UVT1qfhdaz_540.jpg)
## Para conseguir una de las 3 Mega Espadas de los Powers Rangers Super Megaforce:

-Deja un comentario en este post con tu carta a los Reyes Magos. Se admiten fotos.  Sé original porque ganará la carta más original y auténtica, a elegir por la marca. Pon tu alias e indica la red social donde sigas a Bandai.  
-Da “me gusta” a la [página de Facebook de Bandai](https://www.facebook.com/BandaiJuguetes/?fref=ts) o sigue a [Bandai en Twitter](https://twitter.com/Bandai_es)

-El plazo para subir tu carta finaliza el 15 de diciembre, y el resultado se publicará en este blog en los días sucesivos.

-El ámbito del concurso es la península.

¡Tienes hasta el 15 de diciembre!   
¿Qué le pides a los Reyes para tus peques?