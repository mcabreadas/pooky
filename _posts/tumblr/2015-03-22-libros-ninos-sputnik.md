---
date: '2015-03-22T09:24:33+01:00'
image: /tumblr_files/tumblr_inline_nlltlk3VpO1qfhdaz_500.jpg
layout: post
tags:
- trucos
- adolescencia
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: 'Libros infantiles: Sputnik'
tumblr_url: https://madrescabreadas.com/post/114294708669/libros-ninos-sputnik
---

La recomendación de hoy sobre lectura infantil es para niños mayorcitos, de unos 10 u 11 años, y os aseguro que les va a enganchar.

Se trata de [Sputnik](http://www.boolino.es/es/libros-cuentos/sputnik/), de la editorial Takatuka. Mi niña lo está leyendo y parece gustarle mucho.

 ![image](/tumblr_files/tumblr_inline_nlltlk3VpO1qfhdaz_500.jpg)

**Si le preguntáis a mi hija** te dirá que va sobre una niña que se llama Ana que tiene que cuidar siempre de su hermanito pequeño, Adrián, (creo que se siente identificada porque es la mayor de dos hermanos), y que tienen un amigo, Karim, con el que juegan en las antiguas vías del tren. Un día aparece un niño extraño que dice venir de las estrellas y que, para colmo, se llama Sputnik, como el primer satélite que el hombre lanzó al espacio.

Sputnik no habla nuestro idioma, y Ana se comunica con él en inglés, lo que también ha gustado a mi hija, porque al ser términos básicos, los comprende muy bien.

La curiosidad por saber si realmente el niño extraño es un extraterrestre o sólo una invención, les durará a los pequeños lectores hasta el final, y les mantendrá la atención.

**Si me preguntáis a mí** os diré que se trata de una historia de amistad, de fraternidad, por el cariño que se tienen los hermanos, de familia, por cómo se cuidan unos a otros a pesar de la ausencia de la madre, y cómo Ana asume su responsabilidad de hermana mayor con Adrián, y de integración y diversidad por cómo tratan a Sputnik, ya que veremos las distintas reacciones que tienen los niños ante alguien diferente, perfectamente trasladables a los adultos. Aunque tendríamos mucho que aprender de ellos.

En conclusión, tanto mi Princesita como yo os lo recomendamos. Os va a gustar.