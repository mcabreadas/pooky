---
date: '2014-12-31T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_p9ttekP3VI1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- mujer
- cosasmias
title: Así veo yo el año nuevo. No me atrevo ni a hacerme propósitos
tumblr_url: https://madrescabreadas.com/post/106690349314/propositos-ano-nuevo-2015
---

![image](/tumblr_files/tumblr_inline_p9ttekP3VI1qfhdaz_540.jpg)

## _Foto gracias a http://www.tectonicmg.com_

¿No os pasa a veces que os sentís en un precipicio a punto de caer?

¿Que la vida os pone en situaciones que no habías previsto, a pesar de haber tomado siempre las decisiones de forma meditada?

¿No habéis tenido la sensación de que la vida se os escapa de las manos de repente y os lleva a cosas que no habéis elegido, y que ni siquiera habíais previsto que pudieran pasar?

Entonces te sientes atrapada, y ya no puedes elegir… ya elegiste, y concienzudamente además,…o eso creías,, pero la situación da un giro de repente y las consecuencias son diferentes, y la vida se torna distinta a la que tú habías imaginado.

No te arrepientes de la opción vital que tomaste. No es eso. Simplemente te has visto avocada a algo que tú nunca quisiste, y que nunca buscaste.

Entonces te preguntas quién eres realmente, y qué queda de la mujer que fuiste.

Te tambaleas, y sigues adelante por ellos.