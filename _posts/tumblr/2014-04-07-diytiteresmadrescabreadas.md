---
layout: post
title: "Manualidad para niños: Títere de pájaro"
date: 2014-04-07T12:57:00+02:00
image: /tumblr_files/tumblr_inline_n33uc8w0f31qfhdaz.jpg
author: .
tags:
  - planninos
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/81981513625/diytiteresmadrescabreadas
---
La semana pasada estuve en la clase de Princesita colaborando con una mamá que ha impartido un taller de títeres muy interesante.

 Yo no tengo mucho tiempo por las mañanas, ya que exprimo las horas de trabajo a tope para tener libre las tardes para mis fieras, pero no desaprovecho ni una oportunidad que brinda el cole para participar en alguna actividad con mis hijos y sus compañeros por varias razones muy importantes:

\-La tremenda ilusión que hace a mis hijos tenerme con ellos en su ambiente del cole con sus compañeros, en su clase…

\-La oportunidad de oro de ver cómo se desenvuelven entre sus compañeros y en un lugar donde los padres no los vemos a diario. el rol que desempeñan en clase, el comportamiento del resto de niños hacia ellos, la actitud del profesor o profesora con mis hijos. Toda esa información vale millones y me da datos reales para poder ayudar a mi hija cuando viene contando algo que le ha sucedido en la escuela y que le preocupa.

\-Conocer a otros padres y madres, aunque en este caso sólo fueron madres, como casi siempre.

\-Tener un trato más cercano con los profesores en una situación distendida y natural, no tan tensa como pudiera ser una tutoría, previa cita.

\-Conocer de primera mano el ambiente del colegio en una jornada ordinaria.

Y si a todo esto le sumas que aprendes a hacer una marioneta súper chula, pues ya me diréis si merece dejar una mañana el trabajo para pasarla rodeada de escolares.

**Os explico lo que hemos hecho** :

<iframe width="560" height="315" src="https://www.youtube.com/embed/aUAWM4UA99E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Material para hacer un títere de pájaro**

\-3 tapones de botella de agua

\-1 m de hilo negro de bordar grueso

\-1 m de cordón del color que queráis hacer la marioneta. en nuestro caso fue rosa.

\-pegamento de tela

\-2 ojos de plástico

\-un globo pequeño para la nariz

\-1 boa de plumas del color elegido para hacer la marioneta, que desplumaremos

\-un trozo de porexpan redondo, del tamaño de una naranja gorda más o menos

\-1 aguja larga y gorda como la de la foto.

 

\-2 palillos chinos para formar el aspa del títere (los podéis pedir cuando vayáis a un restaurante chino a comer.

\-Rotuladores para decorar los palillos

**Cómo se hace un títere de pájaro**



\-Se hace una cruz con los palillos y se atan por el centro con hilo negro de manera que queden más o menos fijos.

\-En un extremo de un palillo se ata un hilo negro más corto que el resto que enganchará el cuerpo de la marioneta.

\-Los decoramos con rotuladores al gusto.

 ![image](/tumblr_files/tumblr_inline_n33u1akdiy1qfhdaz.jpg)

\-En los otros dos palitos que quedan junto a ambos lados del que hemos atado el hilo negro se atan un hilo negro a cada uno, más largos que el primero. El último extremo que queda es para la cabeza, que queda en frente del primero que hemos puesto, y ésta se pone la última.

\-Forramos el polexpam de plumas pegándolas con el pegamento para hacer el cuerpo.

\-Para las patas, forramos dos de los tapones de agua con plumas por su contorno y por encima dejando libre el hueco de la rosca, que quedará hacia abajo.

\-Para la cabeza, forramos también un tapón de agua igual que para las patas.

\-Pasamos un hilo negro (que luego ataremos al palito que nos queda libre del aspa) por el contorno del tapón que va a hacer de cabeza con la aguja y atamos el extremo para que no se salga. En el extremo opuesto pasamos un cordón de color, que a su vez pasaremos por el cuerpo y sacaremos por el culete de la marioneta haciendo un nudo.

 ![image](/tumblr_files/tumblr_inline_n355em5lAR1qfhdaz.jpg)

\-Y pegamos una cara en el hueco de ese tapón en la que pegaremos los ojos y haremos una nariz llenando con un poco de agua un globo pequeño, lo atamos y cortamos el resto y pegamos.

 ![image](/tumblr_files/tumblr_inline_n355ffGHnc1qfhdaz.jpg)

\-Para unir los pies al cuerpo pasamos un cordón de color con la aguja de debajo arriba de un tapón, después por el cuerpo y después por el otro tapón terminando con un nudo.

\-Para terminar, atamos el hilo negro que hemos pasado por la cabeza al extremo del aspa que queda libre, los hilos negros más largos que hemos colocado anteriormente en el aspa, los pasamos con la aguja por las patas y anudamos el extremos para que no se salga, y el hilo más corto que queda, que está frente a la cabeza, lo pasamos con la aguja por el cuerpo.

 ![image](/tumblr_files/tumblr_inline_n33u4ps0YH1qfhdaz.jpg)

Y ya tenemos nuestro títere, además de acabar todos llenos de plumas como pollos.

Si queréis verlo en movimiento podéis ver aquí el [video en el canal Youtube de Madres Cabreadas](https://www.youtube.com/watch?v=aUAWM4UA99E) donde Princesita os presenta a Pelusilla.

No olvidéis suscribiros para ver más sorpresas!

Espero que os animéis a hacerlo y me pongáis fotitos.