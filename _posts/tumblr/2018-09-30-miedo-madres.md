---
layout: post
title: El miedo en las madres. Historia de un brazo roto
date: 2018-09-30T19:44:09+02:00
image: /tumblr_files/tumblr_pftfyfqjlx1qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/178608014829/miedo-madres
---
Creo que el miedo es, junto a la culpa, el sentimiento negativo que más nos invade a las madres. Si no, preguntaos qué fue lo primero que sentísteis al enteraros de que estabais embarazadas… Casi simultáneamente a la alegría que normalmente genera esta buena nueva, nos invade un acojonamiento importante que, si bien se va mitigando conforme vamos calmándonos, asimilándolo o buscando información y consejos (de los buenos)… resurge a la mínima que pasa algo que se sale de nuestros esquemas.

Esto, lamento decirlo, lejos de ir a menos cuando van creciendo las criaturas, va a más porque no tenemos ni idea de ser padres y aprendemos con el método científico ensayo- error qué desastre, y claro tememos que elerror sea fatal. 

 ![](/tumblr_files/tumblr_inline_pfthtqlp9k1qfhdaz_400.png)

Así que estamos siempre en un sin vivir… al menos yo… Qué ahora me toca [enfrentarme a la adolescencia](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) y no tengo ni puta idea (perdonad mi lenguaje hoy, pero es que estoy alterada).

## Un descuido y un brazo roto

Pero hoy no os voy a hablar como últimamente de esta nueva etapa en nuestras vidas, no. Os cuento esto del miedo porque cuando por fin creía que la época de estar vigilando constantemente a los niños había terminado, va y mi pequeño se rompe un brazo por una caída tonta.

Yo pensaba que al ir con sus hermanos mayores y estar en una plaza supuestamente sin peligros aparentes no podría pasarle nada, pero, ¡Oh mala fortuna!, en un descuido intentó bajar de una repisa de una ventana, no muy alta, e inexplicablemente se rompió cúbito y radio del brazo izquierdo (no había tanta altura como para eso, pero debió de caer mal, si no, no me lo explico).

 ![](/tumblr_files/tumblr_pftfyfCp951qfhdaz_400.jpg)

## La culpa y el miedo

Así que ya tengo recién reactivado el pack completo que viene de serie con la maternidad: culpa y miedo.

Culpa por no haber estado en ese momento mirando, y miedo porque estas cosas nos recuerdan lo frágil que es la vida humana, que cada día que pasamos bien y todos juntos es un regalo, y que hagamos lo que hagamos hay cosas que no dependen de nosotros.

## De tripas corazón

Todas estaréis de acuerdo conmigo en que ver sufrir a un hijo te provoca algo tan visceral que querrías cambiarte por él. En esos momentos de incertidumbre hasta que supimos que tenía una buena solución su rotura y la angustia de oírlo gritar de dolor hasta que le pusieron la medicación te hacen tambalearte y si no fuera porque sabes que eres su referencia y punto de apoyo te desmayarías ahí mismo. Pero en cambio le sonríes y le tratas de hablar con voz baja y suave para transmitirle calma y seguridad, y ante su pregunta:

¿Mamá, me voy a morir?

Le dejas claro con el corazón encogido que de eso no se muere nadie y que se va a poner bien.

## Todo salió bien

Gracia a Dios y a los médicos de la [Arrixaca](https://www.murciasalud.es/seccion.php?idsec=347) parece que el brazo va a quedar bastante bien porque le colocaron los huesos y quedaron perfectamente alineados antes de ponerle la escayola. Esto lo hicieron bajo sedación porque si no no hubiera soportado el dolor.

 ![](/tumblr_files/tumblr_pftfyexwfj1qfhdaz_540.jpg)

Una vez en casa, las primeras noches las pasó junto a mí con mucho dolor y agobiado por no poder dormir, pero ya está mucho mejor, sin dolor, y casi acostumbrado a dormir con la escayola, que pesa más que él.

## Ya está mucho mejor

Enseguida volverá al cole donde lo esperan sus compañeros para firmársela, y donde sin duda tendrá su minuto de gloria como protagonista al contar su hazaña.

 ![](/tumblr_files/tumblr_pftfyfQJlX1qfhdaz_540.jpg)

Todo ha salido bien, pero cuando se trata de nuestros hijos las madres sentimos el peor de los miedos que se pueden vivir. Auténtico terror.

¿Lo has sentido?