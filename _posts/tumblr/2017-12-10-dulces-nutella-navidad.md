---
date: '2017-12-10T17:52:28+01:00'
image: /tumblr_files/tumblr_inline_p0r878gEgW1qfhdaz_540.png
layout: post
tags:
- recetas
- navidad
title: 3 dulces caseros de Navidad con Nutella
tumblr_url: https://madrescabreadas.com/post/168396238064/dulces-nutella-navidad
---

Dicen que la inspiración tiene que sorprenderte trabajando. Pues a mí me ha pillado con el ánimo goloso y la he liado este fin de semana en casa. Un tarro de Nutella tiene la culpa… yo que llevaba meses sin comprar y me estaba quitando, voy y tiro la casa por la ventana haciendo no una, ni dos, sino tres recetas navideñas facilísimas con este ingrediente como protagonista que se pueden hacer con los niños.

 ![](/tumblr_files/tumblr_inline_p0r872TKNZ1qfhdaz_540.png)

La verdad que es en estas fechas donde se come tanto dulce, mejor que sean caseros… (es un mal menor, así sabemos lo que llevan… léanse mil excusas para acallar la conciencia).

Pero además de hacer dulces, también hemos aprovechado para poner los adornos de navidad en casa (el subir y bajar del trastero también cuenta como hábito saludable, no?)

 ![](/tumblr_files/tumblr_inline_p0r873JcPA1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p0r876xg6R1qfhdaz_540.png)

Pero voy a dejarme de tonterías y me centro ya en las tres recetas, de menos a mayor dificultad:

## Sandwiches de Nutella con ventana

Su creación, como todas las genialidades de la historia, surgió fruto de la casualidad, es decir, por no tener huevos en casa, ser festivo y encontrarse la tienda cerrada, y tener ya a los niños embarcados en la cocina para hacer unas supuestas [galletas de mantequilla](/2015/12/07/decora-galletas-de-navidad-en-familia.html) con la receta que os di el año pasado.

 ![sandwiches de nutella con ventana](/tumblr_files/tumblr_inline_p0r8783dZN1qfhdaz_540.png)

Así, tuve que inventarme algo para merendar y no frustrar su entusiasmo.

**Ingredientes**

- Pan de molde sin corteza (o con corteza y se la quitas)  
- Cortadores de galletas de formas  
- Nutella  
- Banderitas  

**Cómo se hace**

- Quita los bordes al pan de molde, si los tiene, con un cuchillo de sierra ancha de los de cortar el pan.  
- Unta una rebanada con Nutella  
- Corta otra rebanada con el cortador de galletas en el centro y extrae la figura dejando una ventana en ella  
- Tapa la rebana untada con Nutella de manera que se vea a través de la ventana, la rebanada de abajo.  
- Coloca una banderita para decorar
 ![](/tumblr_files/tumblr_inline_p0r878gEgW1qfhdaz_540.png)
## Tortitas de Nutella con formas de Navidad

**Ingredientes**

- 150 g de harina  
- Aceite de girasol  
- 1 cucharadita de harina de repostería  
- 40 g de azúcar  
- 200 g de leche entera  
- Una pizca de sal  
- 4 huevos  
- Nutella  
- Nata en spray  
- Cortadores de galletas
 ![](/tumblr_files/tumblr_inline_p0r879iTtu1qfhdaz_540.png)

**Cómo se hace**

- Tamizar la harina junto la levadura y la sal.  
- Batir los huevos, la leche, el azúcar  
- Mezclar todo lo anterior con una batidora hasta que la masa esté homogénea  
- engrasar los corta pastas con un poco de aceite o mantequilla  
- Calentar en una sartén el aceite de girasol, pero ojo, que se quema antes que la mantequilla  
- Verter un poco de masa en una sartén con el aceite caliente dentro de los corta pastas.  
- Cuando esté dorada, sacar del molde y dar la vuelta para que se haga por el otro lado.  
- Cubrir de Nutella y decorar con nata al gusto.  
 ![](/tumblr_files/tumblr_inline_p0r87aGegH1qfhdaz_540.png)
 
## Lazos de Hojaldre con Nutella y azúcar moreno

Esta [receta de lazos de Nutella la saqué del blog Cocinillas](http://cocinillas.elespanol.com/2016/02/lazos-de-nutella-o-nocilla/), que me recomendaron las chicas de [Eventos Coconut](https://eventoscoconut4.wixsite.com/eventoscoconut):

**Ingredientes**

- Hojaldre, 2 planchas rectangulares
- Nutella  4 o 5 cucharadas dependiendo del tamaño de la plancha de hojaldre
- Huevo batido para pintar
- Azúcar moreno 1 cucharada para espolvorear

**Cómo se hace**

![](/tumblr_files/tumblr_inline_p0r87czHDI1qfhdaz_540.gif)

![](/tumblr_files/tumblr_inline_p0r87dFh2G1qfhdaz_540.gif)

![](/tumblr_files/tumblr_inline_p0r87eieo31qfhdaz_540.gif)

- Sobre una de las placas untamos la Nutella hasta cubrirla completamente.
- Tapamos con la otra placa, y con un cuchillo bien afilado cortamos en rectángulos de unos 10 cm de largo por 2 cm de ancho.
- Refrigeramos durante, como mínimo, 30 minutos.
- Precalentamos el horno a 200ºC con calor arriba y abajo sin ventilador.
- Sacamos de la nevera y retorcemos nuestros rectángulos dándoles un par de vueltas..
- Pintamos con huevo batido, espolvoreamos con el azúcar moreno y horneamos durante unos 20 minutos a 200ºC o hasta que estén dorados.
- Dejamos enfriar sobre una rejilla.

 ![](/tumblr_files/tumblr_inline_p0r87fUvNr1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p0r87f0fs81qfhdaz_540.png)

Así que ya sabéis, si queréis triunfar estas Fiestas y sorprender con dulces caseros y muy sencillos de hacer, ya no tenéis excusa.

Christmas is comming!