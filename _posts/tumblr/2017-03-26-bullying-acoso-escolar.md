---
date: '2017-03-26T17:25:21+02:00'
image: /tumblr_files/tumblr_inline_n1e6bfE5Js1qfhdaz.jpg
layout: post
tags:
- mecabrea
- crianza
- adolescencia
- educacion
- familia
title: Carta inacabada de un niño excluído
tumblr_url: https://madrescabreadas.com/post/158852921704/bullying-acoso-escolar
---

Esta carta no la ha escrito ningún niño concreto, pero creo que podrían haberla escrito muchos. La he escrito yo basándome en experiencias que de un modo u otro me han ido llegando para haceros ver a los padres y madres que hay muchas formas de [bullying](https://es.wikipedia.org/wiki/Acoso_escolar), no sólo el físico, el papel tan vital que tenemos en el tema del acoso escolar, y lo mucho que podemos hacer por evitarlo dando el ejemplo adecuado a nuestros hijos y corrigiéndoles desde pequeños cualquier comportamiento hacia otro compañero que se acerque a esto.

 ![](/tumblr_files/tumblr_inline_n1e6bfE5Js1qfhdaz.jpg)

Aunque parezca mentira, los padres y madres podemos propiciar actitudes en nuestros hijos que dañen a sus compañeros.

Es importante abstraernos a nuestro papel de adultos en cualquier conflicto en el que nuestros hijos se vean implicados, jamás ponernos a la altura de los niños, insultando o menospreciando a ninguno de ellos, no tomar decisiones en caliente, y acudir a los profesores y profesionales si no sabemos cómo actuar en determinados casos.

Recordemos que si nosotros hablamos con falta de respeto de algún compañero de nuestros hijos, incluso de los profesores, el mensaje que les estamos enviando es que es correcto faltar al respeto a los demás, ya que estamos validando este comportamiento con nuestro ejemplo.

Queridos compañeros y padres de mis compañeros:

Es difícil para mí expresar cómo me siento en estos momentos. 

Acabáis de organizar otro plan todos juntos y, como siempre, yo no estoy incluído. Por más que lo intento no logro acostumbrarme, pero esta vez he roto a llorar cuando lo he contado en casa, y he visto la mirada de dolor de mi madre. Me he sentido peor todavía. 

Sé que no soy perfecto, que he cometido errores, y que a veces nos hemos enfadado, pero os prometo que cada día intento mejorar para tener una buena relación con vosotros. 

Siempre me ha costado hacer amigos, pero he aprendido mucho desde que empezamos el colegio y nos conocimos. Soy sólo un niño, y me gustaría tener la oportunidad de volver a tener amigos, cosa que creo que me estáis negando porque algunos de vosotros guardáis un rencor antiguo de cuando era más pequeño y me equivocaba.

Qué suerte los que tenéis facilidad natural para relacionados con los demás, porque os sale solo, sin esfuerzo, y siempre estáis rodeados de niños, y jugáis, y os reís… 

No es mi caso, y por eso sufro muchísimo, además de por llevar colgado ya para siempre el cartel de “el raro”. No sabéis el dolor que me causa cuando me quedo solo en el recreo, cuando organizáis comidas de clase y todos habláis delante de mí de lo bien que lo vais a pasar, cuando todos os vais de cumpleaños menos yo, cuando me quedo fuera de todos los grupos para hacer los trabajos, o me elegís el último cuando hacemos equipos en clase de educación física… el alma se me parte en dos y me siento la persona más desgraciada del planeta. 

Soy sólo un niño que está creciendo y aprendiendo, no comprendo cómo alguna de las madres me pueden hablar con desprecio, mirarme mal cada vez que me ven y hablar mal de mí a otras madres, quitándome la oportunidad de hacer otros amigos nuevos.

No sé muy bien cómo acabar esta carta, me gustaría acabarla bien, pero de momento no puedo. Ojalá pueda algún día contando que todo ha cambiado, y por fin puedo sonreír en el cole.

 ![](/tumblr_files/tumblr_inline_ondu5s8xly1qfhdaz_540.jpg)