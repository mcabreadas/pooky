---
layout: post
title: Chocolate sin remordimientos. Hazlo tu misma.
date: 2016-02-28T19:00:38+01:00
image: /tumblr_files/tumblr_inline_o3axbqaRoe1qfhdaz_540.jpg
author: .
tags:
  - trucos
  - recetas
  - nutricion
tumblr_url: https://madrescabreadas.com/post/140159398561/hacer-chocolate-cacao
---
Te voy a contar una cosa que te va a encantar, sobre todo si eres amante del chocolate tanto como yo.

Últimamente se habla mucho de los beneficios del cacao y de la cantidad de propiedades que tiene para nuestra salud física y psíquica.

Y una, que le encantaría creerlo a pies juntillas y atiborrarse sin remordimientos de manjar tan exquisito, ha decidido investigar un poco y comprobar qué hay de cierto en todo esto.

## La buena noticia: el cacao es bueno

La buena noticia es que es cierto. El cacao contiene una enorme concentración de minerales (**magnesio, hierro y cromo)**, **vitaminas** , y encabeza la lista de alimentos **antioxidantes por encima del té verde y del vino tinto** (a partir de ahora, en vez de té, para la sobremesa, ya sabes, un tazón de cacao). 

Además, posee **[andamida](https://es.wikipedia.org/wiki/Anandamida)** , que funciona como regulador natural del humor, y tiene efectos favorables sobre la concentración, el amor y el placer.

 ![](/tumblr_files/tumblr_inline_o3axbqaRoe1qfhdaz_540.jpg)

Una de las cosas que más me ha llamado la atención leyendo sobre este tema, es que desde las civilizaciones maya y azteca tradicionalmente se ha utilizado siempre la **planta del cacao con fines curativos**. Desde entonces se han registrado más de 100 usos medicinales del cacao y del chocolate.

Esto puede ser debido a que contiene **flavonoides** , presentes en grandes cantidades en los granos de cacao. 

Se trata de unos compuestos naturales que se encuentran en abundancia en las plantas y en los alimentos y bebidas de origen vegetal (legumbres, manzana, uva, cacao…). Aparentemente, tienen un papel funcional, ya que ayudan a la planta a reparar daños y la protegen de plagas y enfermedades. Recientemente, los científicos han comprobado que el consumo regular de frutas y verduras ricas en flavonoides reduce el riesgo de padecer muchas enfermedades crónicas. Además, tienen propiedades antioxidantes, y  son cardioprotectores.
No sé a ti, pero a mí me han terminado de convencer.

## La mala noticia: tiene demasiado azúcar

Y ahora viene la mala noticia. Y es que normalmente el acceso que los consumidores tenemos al cacao no es en estado puro, y por tanto, con todas sus propiedades intactas, sino que por lo general cuando llega a nuestras manos está alterado y edulcorado en exceso porque, como ya sabéis, el cacao puro no es dulce. Y es la gran cantidad de azúcar que se le añade la que hace que se convierta en un alimento no tan beneficioso como nos cuentan los científicos.

## ¡Hagamos nuestro propio chocolate!

Pero, como a mí no me gusta conformarme, en casa hemos decidido hacernos nuestro propio cacao para la leche, los postres, incluso lo probaremos a hacer en crema para las tostadas (ya te contaré).

Si miráis un bote de caco soluble para la leche, cualquiera que encontréis en el supermercado, os sorprenderá comprobar que está compuesto alrededor de un 70% por azúcar. Por eso, nosotros hemos hecho nuestro propio cacao soluble con cacao puro.

 ![](/tumblr_files/tumblr_inline_o3axbqxGM41qfhdaz_540.jpg)

Preparamos unos tazones de leche para el desayuno que hacen las delicias de mis fieras, al tiempo que se van al cole llenos de energía, y con todos los beneficios de cacao.

 ![](/tumblr_files/tumblr_inline_o3axbqnfK11qfhdaz_540.jpg)

Nosotros buscamos un [cacao elaborado de forma respetuosa con sus propiedades](https://amzn.to/3BXk3HC), porque no todos los cacaos puros se han tratado de igual forma, ya que los que algunos han sido deshidratados de más, y el producto no tiene toda la calidad que debería. 



 ![](/tumblr_files/tumblr_inline_o3axbrYVkz1qfhdaz_540.jpg)

Mi sorpresa fue cuando comprobé que se disuelve fenomenal en la leche. 

 ![](/tumblr_files/tumblr_inline_o3axbsAhHO1qfhdaz_540.jpg)

Sólo hay que echar una cucharadita en un poquito de leche caliente, añadir la miel al gusto, y batir con un tenedor en vez de de con cucharilla. Cuando esté bien mezclado, y sin grumos, añadimos el resto de leche. 

¡Y os aseguro que el sabor es delicioso!

Todavía no hemos probado a hacer crema de cacao con avellanas, pero será el siguiente paso, y lo compartiré en el blog.

Yo, sinceramente, y como adicta declarada al chocolate, me siento entusiasmada porque, si alguna propiedad tiene el cacao, yo, desde luego, las he adquirido todas a lo largo mi mi vida chocolatera, y con creces (qué te voy a contar). 

Así que si ahora lo tomo de forma más saludable sin exceso de azúcares, y encima sin remordimientos, no es que vaya a alcanzar la felicidad plena, pero ahí andaré rondándola cuando me tome un buen tazón de cacao segura de que además de darme una alegría para el cuerpo, estoy tomando uno de los alimentos más saludables que hay.

¿A que te he alegrado el día?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.