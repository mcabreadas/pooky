---
date: '2016-05-25T10:01:54+02:00'
image: /tumblr_files/tumblr_inline_o7n8e9kU321qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- moda
title: Oh, verano! Oh Soleil! Os echábamos de menos
tumblr_url: https://madrescabreadas.com/post/144898451133/ropa-infantil-verano-ohsoleil
---

No te imaginas las ganas que tenía Princesita de estrenar su [ropa nueva de primavera verano de OhSoleil](http://ohsoleil.com/coleccion-pv-2016/).

No te exagero si te digo que cada fin de semana preguntaba si se la podía poner, aunque estuviera lloviendo o hiciera frío y, como el tiempo ha estado revuelto hasta ahora, ha tenido que hacer un ejercicio importante de paciencia para esperar el momento adecuado.

Y por fin llegó el día en que salió el sol, e hizo calor, y pudo lucir sus nuevos modelos de la colección de primavera verano de esta marca especializada en _teens_ que tanto nos gusta, y de la que os he hablado en varias ocasiones [aquí](/2016/01/09/ropa-adolescentes-ohsoleil.html).

## Nueva tienda física en Madrid
 ![](/tumblr_files/tumblr_inline_o7n8e9kU321qfhdaz_540.jpg)

Hasta hace poco, la ropa de OhSoleil sólo se podía comprar on line, y en espacios multi marca, pero hace poco acaban de abrir su primera tienda en el insigne barrio de Salamanca de Madrid. La tienda está ubicada en el número 52 de la calle Hermosilla, y cuenta tanto con la colección infantil, desde los 2 hasta los 12 años, como con su línea juvenil, con tallas de los 12 a los 16 años, aunque en la próxima temporada llegará hasta los 18 años.

 ![](/tumblr_files/tumblr_inline_o7n8eaKgnt1qfhdaz_540.jpg)
## Abarcas de ensueño

Actualmente está a la venta la colección de baño y streetwear P/V 2016 Hip Summer, y en exclusiva una selección de las abarcas de Boho Kids que me vuelven totalmente loca (¡si tuvieran mi número, me las calzaba!)

 ![](/tumblr_files/tumblr_inline_o7n8eaqSOu1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7n8ebkzqT1qfhdaz_540.jpg)

En esta boutique, OhSoleil ha condensado toda la filosofía boho de la marca con un interiorismo en el que los tonos naturales de la madera se mezclan con colgadores metalizados de inspiración industrial, cestas de flores, marcos de fotos y mensajes inspiradores, sin restar visibilidad a las prendas, que son las absolutas protagonistas. 

## Bañadores

Con origen en Tenerife, OhSoleil nació en 2001 como firma infantil de trajes de baño. En 2012 lanzó su primera colección de streetwear y en 2014 fue pionera en lanzar una colección para adolescentes hasta los 16 años, cuyo éxito ha ido en aumento desde entonces al saber entender los gustos de un público en transformación que ya no se identifica con los códigos infantiles pero tampoco con los adultos.

Princesita ha elegido este bikini con el que va a estar preciosa cuando esté morenita:

 ![](/tumblr_files/tumblr_inline_o7n8ebFUjX1qfhdaz_540.jpg)

También tienes bañadores completos como éstos, que me parecen muy originales:

 ![](/tumblr_files/tumblr_inline_o7n8ecvtHh1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7n8eccmex1qfhdaz_540.jpg)
## La ropa para jóvenes _teens_

La ropa para _teens_, esa edad difícil entre los 12 y los 16718 años, es ideal, ya lo sabéis.

Su estilo viene marcado por el auténtico espíritu boho chic. Comunión con la naturaleza y espíritu libre presentando una nueva colección que reinterpreta los códigos setenteros y la estética hippie

Princesita eligió este conjunto de shorts y blusa blanca con detalle en las mangas anudadas con un lazo, y un bolsillo, que es pura delicadeza (como ella).

La caída de las prendas es espectacular, y la calidad y acabado excelentes.

 ![](/tumblr_files/tumblr_inline_o7n8ed2xPq1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7n8eeuNM81qfhdaz_540.jpg)
## Ropa de ceremonia

Ohsoleil también tiene una línea de ceremonia que encantará a las jovencitas que ya van queriendo vestirse de mayores, pero con un toque fresco genial.

 ![](/tumblr_files/tumblr_inline_o7n8eeOHQE1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o7n8efpvrQ1qfhdaz_540.jpg)

Princesita eligió este vestido tipo ibicenco con un toque sutil de brillo, en un tono rosa suave súper dulce.

 ![](/tumblr_files/tumblr_inline_o7n8efmIQw1qfhdaz_540.jpg)

Las mangas son puro detalle.

 ![](/tumblr_files/tumblr_inline_o7n8egYTAp1qfhdaz_540.jpg)

Nosotras recomendamos OhSoleil para jovencitas que quieran llevar su propio estilo de ropa, un poco hippie, un poco bohemio, pero siempre chic.

¿Te gusta?