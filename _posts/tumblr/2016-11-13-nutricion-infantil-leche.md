---
date: '2016-11-13T18:41:03+01:00'
image: /tumblr_files/tumblr_inline_oghak5MBuY1qfhdaz_540.png
layout: post
tags:
- nutricion
- crianza
title: Leche de vaca. Preguntas y respuestas en el Blog Trip Puleva
tumblr_url: https://madrescabreadas.com/post/153132564709/nutricion-infantil-leche
---

El pasado 22 de octubre fui invitada por el [Instituto Puleva de Nutrición](https://www.institutopulevanutricion.es/) a su ya tradicional Blog Trip para bloggers que, un año más, tuvo lugar en la maravillosa ciudad de Granada.

Allí tuve ocasión de asistir a una interesante jornada sobre nutrición infantil: “Grandes retos de la alimentación infantil en el nuevo milenio”, que tuvo como ponentes a D. Fernando Varela, presidente de la [Fundación Española de la Nutrición](http://www.fen.org.es/) y a Doña, Mercedes Gil Campos, de la Unidad de Metabolismo e Investigación Pediátrica del [Hospital Reina Sofía de Córdoba](http://www.juntadeandalucia.es/servicioandaluzdesalud/hrs3).

 ![](/tumblr_files/tumblr_inline_oghak5MBuY1qfhdaz_540.png)

Lo que recojo en este post son las ideas principales que se expusieron sobre el informe “La leche como vehículo de salud para la población” elaborado por la [Fundación Española de la Nutrición](http://www.fen.org.es/), y la [Fundación Iberoamericana de Nutrición](http://www.finut.org/).

Lo expongo a modo de preguntas y respuestas, muchas de ellas allí formuladas, para hacerlo m´ás dinámico.

 ![](/tumblr_files/tumblr_inline_ogh9mpLbeW1qfhdaz_540.png)
## Jornada de nutrición infantil

**¿Cuál es el mejor alimento para un bebé?**

 ![](/tumblr_files/tumblr_inline_ogh9hfz9Us1qfhdaz_540.png)

La leche materna, porque tiene una composición única:

 ![](/tumblr_files/tumblr_inline_ogh9uqMqS01qfhdaz_540.png)

**¿Por qué se considera la leche de vaca un alimento básico para la dieta de los niños?**

-Por su composición, ya que contiene nutrientes esenciales para la salud en cantidades relativamente elevadas.

-Porque es un alimento equilibrado.

-Porque es fácil de adquirir, es asequible y casi todo el mundo tiene acceso a ella

-Porque complementa las posibles carencias nutricionales de otras ingestas.

-Porque es un vehículo ideal para favorecer la ingesta de ácidos grasos Omega 3, aporta proteínas y calcio al llevar vitamina D, y fósforo, que facilita su absorción.

 ![](/tumblr_files/tumblr_inline_ogh9ogRjUc1qfhdaz_540.png)

**¿Es necesario que les demos a nuestros hijos leches de crecimiento?**

No es necesario si les proporcionamos una dieta variada y equilibrada.

**¿Cuál es el papel de los Ácidos grasos “de moda” Omega 3? ¿Por qué son tan importantes?**

Estamos en una sociedad grasofóbica, la grasa no es “fashion”, pero estos ácidos son buenos para nuestro organismo, ya que ingestas adecuadas de AGPI y Omega-3 contribuyen al desarrollo del cerebro, así como a la agudeza visual de los niños.

**¿Hasta qué punto los ácidos grasos Omega 3 tienen efectos sobre la cognición?**  

Han sido descritos mecanismos por los que podrían ejercer un efecto neuroprotector y ser de utilidad en la prevención del deterioro cognitivo, sin embargo no existen evidencias científicas actualmente para concluir que tengan un efecto clínico significativo.

**¿Previenen los ácidos estos ácidos grasos las enfermedades cardiovasculares?**

Efectivamente, ayudan a controlar determinados factores de riesgo cardiovascular, por lo que se pueden recomendar para su prevención .

**¿Además de la leche de vaca, qué más alimentos son ricos en ácidos grasos?**

En pescados grasos, mejor pequeños, ya que así evitaremos la ingesta de mercurio, que se encuentra en concentraciones mayores en peces de mayor tamaño.

Los frutos secos también son ricos en algunos ácidos grasos.

**¿Qué son los ácidos grasos trans?**

Son los tratados industrialmente, si los ingerimos en poca cantidad no pasa nada, pero si la aumentamos pueden aparecer desequilibrios en relación con la salud.

**¿Es necesario tomar productos enriquecidos con ácidos grasos?**

Si se toma una dieta variada y equilibrada, no es necesario.

Es alarmante que en los últimos tiempos se haya detectado una disminución de ingesta, sobre todo en niños y en jóvenes.

**¿Si la leche de vaca es tan beneficiosa, por qué ha disminuído su consumo en los últimos años?**

 ![](/tumblr_files/tumblr_inline_ogh9j6Kw0e1qfhdaz_540.png)

Efectivamente, ha descendido el consumo de leche de vaca desde 2014 porque se ha ido sustituyendo por bebidas vegetales (soja, arroz, almendras…)

**¿Son aconsejables las bebidas vegetales?**

Las bebidas vegetales son buenas, pero no aportan los mismos nutrientes que la leche de vaca, es decir son alimentos más pobres desde el punto de vista nutricional. No debemos pensar que la sustituyen.

 ![](/tumblr_files/tumblr_inline_ogh9scPRid1qfhdaz_540.png)

**Leche entera, semidesnatada, desnatada, enriquecida con calcio, Omega 3, isoflavonas… ¿Qué “leches” tomamos?**

En el marco de una alimentación equilibrada y variada, la leche normal sería suficiente. Si estamos tomando demasiadas grasas por otro lado, podríamos tomar las semi o las desnatadas, y en circunstancias en que, por cualquier motivo, nuestra ingesta de algún nutriendo sea inferior a la necesaria, podríamos recurrir a leches enriquecidas.

 ![](/tumblr_files/tumblr_inline_ogh9lh6PoD1qfhdaz_540.png)

**¿Es más aconsejable una leche ecológica?**

 ![](/tumblr_files/tumblr_inline_ogh9pihoo41qfhdaz_540.png)

Aunque está de plena moda, actualmente, de momento no hay estudios que favorezcan el consumo de leche ecológica por encima de la normal.

## Mesa redonda “Claves para comunicar con éxito en la blogosfera maternal”

En esta parte de la jornada tuvimos la oportunidad de debatir con tres ponentes muy interesantes temas relacionados con la comunicación digital y nuestro papel como bloggers a la hora de transmitir información a nuestros seguidores.

Leyre Artiz, directora de la revista [Ser Padres](http://www.serpadres.es/), incidió en la importancia de la veracidad y la honestidad a la hora de comunicar.

 ![](/tumblr_files/tumblr_inline_ogkwjtE7gc1qfhdaz_540.jpg)

Yolanda Velaz, autora del blog [Nadie como mamá](http://www.nadiecomomama.com/) nos hizo conscientes del impacto que pueden llegar a tener nuestras publicaciones, y la gran responsabilidad que tenemos con otras madres.

 ![](/tumblr_files/tumblr_inline_ogkwjtbsVN1qfhdaz_540.jpg)

Armando Bastida, editor de [Bebés y más](http://www.bebesymas.com/) nos contó sus secretos para llegar a hacer un post redondo. 

 ![](/tumblr_files/tumblr_inline_ogkwjwwea21qfhdaz_540.jpg)

Todo esto y más fue lo que se habló en esta interesante jornada a la que agradezco mucho que me invitara el [instituto Puleva de Nutrición](https://www.institutopulevanutricion.es/), no sólo por lo que aprendimos y compartimos, sino porque disfrutamos de un entorno incomparable en la mejor compañía.

## Visita a la Alhambra y Albaicín

Pero no todo fueron ponencias y trabajo, también tuvimos tiempo de disfrutar de las maravillas de [la Alhambra de noche](http://www.alhambra-patronato.es/index.php/Alhambra-Nocturna/239/0/), visita totalmente aconsejable si queréis que se os inunden los ojos de belleza.

 ![](/tumblr_files/tumblr_inline_ogkxnwDabJ1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_oghayxC5xE1qfhdaz_540.png)

Si vais a Granada y tenéis ocasión, no dejéis de cenar en el [restaurante Las Tomasas](http://lastomasas.com/), en pleno Albaicín, que tiene esta espectacular terraza con vistas a la Alhambra.

En la foto estoy con todas y todos mis compañeros bloggers con los que tuve la suerte de compartir este fin de semana tan especial.

 ![](/tumblr_files/tumblr_inline_ogh9krtFog1qfhdaz_540.png)

Otro imprescindible de Granada es disfrutar de un buen té en una de sus típicas teterías. 

En la foto estoy con Bea, de [Ni Blog ni Bloga](https://niblognibloga.wordpress.com/), y Beby, de [La casita de Bebybo](http://lacasitadebebybo.blogspot.com.es/) , dos grandes descubrimientos para mí.

 ![](/tumblr_files/tumblr_inline_ogh9jqqovP1qfhdaz_540.png)

Y hasta aquí mi primera experiencia en un blog trip. Gan experiencia, sin duda.