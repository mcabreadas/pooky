---
date: '2016-10-16T18:53:01+02:00'
image: /tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg
layout: post
tags:
- mecabrea
- colaboraciones
- gadgets
- ad
- eventos
- derechos
title: Legalitas contra el ciberbullying
tumblr_url: https://madrescabreadas.com/post/151889431269/actuar-ciberbullying-legalitas
---

Ciberbullying, acoso escolar, agresiones… son palabras que nos ponen en alerta a los padres, y que tememos cada día que puedan sufrir nuestros hijos.

Como abogada y madre me preocupa mucho si seré capaz de actuar correctamente si llega el caso.

Conozco el caso de una familia que lo he pasado realmente mal con este tema, ya que se ve afectada por una situación sobrevenida que, de repente, tambalea la estabilidad familiar y hace sufrir al menor que la sufre, y a los demás miembros quienes, normalmente, no saben cómo actuar.

Son situaciones muy duras, y debemos estar informados y preparados y, sobre todo, saber detectarlas a tiempo.

## ¿Qué es el ciberbullying?

El ciberbullying es el uso de los medios telemáticos (Internet, telefonía móvil y videojuegos online principalmente) para ejercer el acoso psicológico entre iguales.Estamos ante un caso de ciberbullying cuando un o una menor atormenta, amenaza, hostiga, humilla o molesta a otro/a mediante Internet, teléfonos móviles, consolas de juegos u otras tecnologías telemáticas.

El anonimato, la no percepción directa e inmediata del daño causado y la adopción de roles imaginarios en la red convierten al ciberbullying en un grave problema.

 ![](/tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg)
## ¿Cómo detectarlo?

Es muy importante hablar mucho con nuestros hijos y mostrarnos siempre receptivos para escucharlos. Darles confianza para que nos cuenten sus cosas desde bien pequeños para cuando lleguen a la pre adolescencia exista ya ese vínculo bien consolidado. El apego con los padres desde bien pequeños ayuda a forjar esos lazos que, aunque parezca que se pierden en algún momento, difícilmente desaparecerán.

Observar siempre a nuestros hijos, y percatarnos si:

- Se muestran tristes y nerviosos.  
- Se encuentran desmotivados para asistir al centro escolar.  
- De repente les cuesta relacionarse  
- Sienten ansiedad e irritabilidad cuando se sientan frente al ordenador.  
- Consulta obsesivamente el ordenador o el móvil.   
- Pierden el apetito.  
- Duermen mal.  

Y en general cualquier conducta extraña que nos llame la atención.

## ¿Qué hacer para prevenir el ciberbullying?

En la web [ciberbullying.com](http://www.ciberbullying.com/cyberbullying/) encontramos 10 consejos que nos pueden ayudar:

- No contestes a las provocaciones, ignóralas.  
- Cuenta hasta cien y piensa en otra cosa.  
- Compórtate con educación en la Red.  
- Usa la [Netiqueta](http://es.wikipedia.org/wiki/Netiquette).  
- Si te molestan, abandona la conexión y pide ayuda.  
- No facilites [datos personales](http://www.privacidad-online.net/).   
- No hagas en la Red lo que no harías a la cara.  
- Si te acosan, guarda las pruebas.  
- Cuando te molesten al usar un servicio online, pide ayuda a su gestor o administrador.  
- No pienses que estás del todo seguro/a al otro lado de la pantalla.  
- Advierte a quien abusa de que está cometiendo un delito.  
- Si hay amenazas graves pide ayuda con urgencia.  

Yo añadiría: introduce tú mismo a tus hijos en la navegación por internet, es decir, navega con ellos hasta que estés segura de que saben manejarse de forma correcta por la red y, después, supervisa y comparte con ellos su experiencia.

También es importante controlar el tiempo que dedican a internet, y ofrecerles [actividades de ocio alternativas en familia](/tagged/planni%C3%B1os).

## ¿Cómo actuar frente al ciberbullying?

Si, pese a estas prevenciones, se da un caso de ciberbullying podemos tomar medidas legales, pero necesitaremos pruebas, por eso es necesario ir guardándolas durante todo el tiempo del acoso. Es importante que podamos demostrar la continuidad, y que no se trata de un hecho aislado.

Existen peritos expertos que pueden emitir un certificado de veracidad que será de gran ayuda para probar los hechos en caso de juicio. 

Te dejo unos [consejos prácticos sobre los pasos a seguir para una víctima de ciberbullying](http://www.ciberbullying.com/cyberbullying/2010/09/01/decalogo-para-una-victima-de-ciberbullying/), que consisten tanto en recomendaciones técnicas, como en la actitud que se debe tomar ante el acosador. Me han parecido muy útiles y acertados.

## Presentación de dos apliaciones de Legalitas

La compañía de asistencia jurídica Legalitas ha presentado recientemente la aplicación “Legalitas protección hijos”, que ofrece una herramienta para ayudar a los padres en caso de dudas o cómo deben actuar en caso de que sus hijos sufran acoso escolar, ciberbullying o cualquier asunto relacionado con los menores en internet y las nuevas tecnologías.

Al evento de presentación asistió **Blanca Crespo** , colaboradora habitual de este blog, y ésta es su crónica:

> El pasado martes acudí a un evento de Legalitas compañía que desde 1999 es líder en seguros y servicios de defensa jurídica para particulares, autónomos y PYMES.
> 
> En este evento nos presentaron las dos nuevas aplicaciones que han lanzado para una mejor todavía comunicación con el ciudadano: LEGALITAS YA y LEGALITAS HIJOS.
> 
> Pero antes de entrar en detalles os diré que en esta presentación asistieron tres importantes ponentes: el Ministro de Justica Rafael Catalá, la presidenta de Microsoft ibérica Pilar López y el presidente de Legalitas Alfonso Carrascosa.

 ![](/tumblr_files/tumblr_inline_of2xq3grU91qfhdaz_540.jpg)

> Cada uno de ellos dijo unas breves palabras sobre la importancia que tiene en la actualidad que innovación y abogacía vayan de la mano. Os resumo lo que para mi entender (punto de vista de profesora de infantil) fue lo más interesante que dijeron.
> 
> - Ministro de Justicia: tiempos próximos son de acercar la justicia al ciudadano, y los abogados de Legalitas lo demuestran cada día.  
> 
> - Presidenta Microsoft ibérica: La misión de Microsoft es que las personas y empresas puedan hacer lo que se propongan con innovación tecnológica y Legalitas es una de esas empresas que abraza el cambio tecnológico. Por ello Microsoft quiere que los abogados tengan las herramientas suficientes para desempeñar lo mejor posible su trabajo.  
> 
> - Presidente de Legalitas: En Legalitas se trabaja por y para las personas.  
> 
> Después tuvo lugar la presentación de dos nuevas aplicaciones para móvil:

## Aplicación Legalitas hijos

> Ha sido creada para proteger a los jóvenes incorporando dos funciones, que avisarán a padres o tutores cada vez que su hijo esté en situación de peligro o amenaza.
> 
> **Botón del pánico** : Con sólo pulsar cuatro veces seguidas el botón de inicio de su móvil en Android o el de la misma aplicación en IOS, se emitirá una alerta automáticamente que será recibida por los padres o administradores de la aplicación para saber en que ubicación concreta se encuentran y señal de audio y video para que se pueda conocer lo que está pasando en todo momento.
> 
> **Radio de seguridad** : Con esta función los padres pueden delimitar las zonas que quieran y recibir alerta si el joven ha salido de dicha delimitación.
> 
> La aplicación también cuenta con servicio de asistencia jurídica sobre cualquier asunto que tenga relación con menores en internet y nuevas tecnologías.

 ![](/tumblr_files/tumblr_inline_of2z2wILij1qfhdaz_540.png)

Ofrece:

- Protección jurídica frente a casos de acoso escolar y ciberbullying, cometidos en colegios, redes sociales e Internet.  
- Asistencia psicológica proporcionada por profesionales especializados a disposición de las familias.  
- En caso de violencia digital a menores, un perito experto emitirá un informe con certificación de veracidad que podrá servir como documento de prueba en la presentación de una denuncia.  
- Asistencia jurídica sobre [reputación online](/2015/03/21/internet-intimidad-menores.html), difusión de imágenes a través de Internet, comentarios falsos o injuriosos sobre nuestros hijos, borrado o cancelación de datos de tus hijos en Internet… (recuerda que hablé de esto [aquí](/2015/03/21/internet-intimidad-menores.html).)  
- Asuntos penales: descargar o difusión de contenidos ilícitos o de terceros (videos, música), pornografía infantil, transmisión de imágenes de contenido sexual…  

Tienes [Legalitas hijos para IOS](https://itunes.apple.com/es/app/legalitashijos-boton-del-panico./id1122094783?l=en&mt=8)

y [Legalitas hijos para Android](https://play.google.com/store/apps/details?id=com.legalitas.legalitashijos)

## Aplicación Legalitas ya

> Esta está diseñada para facilitar más todavía el contacto entre ciudadano y abogado, pudiendo plantear cualquier duda a la vez de enviar documentación requerida para la mejor resolución del problema.
> 
> La simple descarga de la app permite disfrutar gratis de urgencias legales y desplazamiento de abogado.

Más información en [legalitas.com](https://www.legalitas.com/)

Quiero dar las gracias a Blanca Crespo por asistir al evento para contarnos todos los detalles, y a Legalitas por contar con este blog para tratar un asunto tan delicado e importante para las familias.

Sin duda en la familia está la clave para prevenir y afrontar estas situaciones.

¿Te has visto alguna vez en una situación parecida?