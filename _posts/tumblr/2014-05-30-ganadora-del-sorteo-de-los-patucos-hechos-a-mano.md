---
date: '2014-05-30T16:00:19+02:00'
image: /tumblr_files/tumblr_inline_pb455bcoTt1qfhdaz_540.jpg
layout: post
tags:
- premios
title: Ganadora del sorteo de los patucos hechos a mano Storkbay
tumblr_url: https://madrescabreadas.com/post/87298284907/ganadora-del-sorteo-de-los-patucos-hechos-a-mano
---

Como sé que estaréis deseando saber quién ha ganado los [patuquitos que sorteábamos esta semana](/post/86226902955/sorteo-styarkbay-patucos-madrescabreadas), os dejo el resultado. No diréis que no he sido rápida, eh? Me merezco una ola!

And the winner is….

![image](/tumblr_files/tumblr_inline_pb455bcoTt1qfhdaz_540.jpg)

Noelia!!!! Que eligió las botitas de color granate de lana. La verdad es que son una monada, con los botoncitos en el lateral, no les falta detalle. 

![image](/tumblr_files/tumblr_inline_pb455bHdjf1qfhdaz_540.jpg)

Enhorabuena, guapa! Ya sabes, pásame tus datos por privado para el envío: mcabreadas@gmail.com

Y a las demás, mil gracias por participar, y podéis seguir probando suerte con [otras cositas](/2014/05/29/sorteo-mascotas-virtuales-cupets-os-presento-a.html)que estoy preparando.