---
date: '2014-03-07T07:31:00+01:00'
image: /tumblr_files/tumblr_inline_n228o1rOT31qfhdaz.jpg
layout: post
tags:
- premios
- trucos
- recomendaciones
title: 'Ganadora del sorteo #MotivosParaSonreir de PHB'
tumblr_url: https://madrescabreadas.com/post/78827624058/ganadora-del-sorteo-motivosparasonreir-de-phb
---

Vamos a dar un motivo para sonreír a una mamá que, aunque ya tiene más que de sobra con los que les da su familia, no está de más darle otro. Me ha encantado organizar este [sorteo de un kit dental PHB de Pocoyó](/post/77682906583/sorteo-kit-dental-pocoyo-phb-motivosparasonreir)para los más pequeños porque ha estado cargado de optimismo al ser requisito que los participantes contarán su motivo para sonreír, y lo más curioso es que todos han coincidido en que lo que más felices les hace son sus hijos y su familia, por mucho que les hagan llegar a la cama exhaustos, o por muchos quebraderos de cabeza que les den. Eso me encanta!

![image](/tumblr_files/tumblr_inline_n228o1rOT31qfhdaz.jpg)

Otro motivo por el que he disfrutado con el sorteo es porque, como os dije, estamos viviendo en casa la iniciación a la higiene bucal de Leopardito, y he tenido la ocasión de investigar las nuevas recomendaciones para bebés desde que les salen los dientes que, como novedad, sugieren poner un poquito de pasta de la que tiene menos concentración de flúor en el cepillo (menos de un grano de arroz).

Os podéis imaginar cómo le gusta el saborcillo y sacarle el jugo al cepillo. Oye, y lo que se entretiene! Le encanta porque hace lo mismo que sus hermanos mayores. Bueno, dejad de mordernos las uñas. Os dejo el nombre de la ganadora ya, cuyo motivo para sonreír es doble! Dice: “ Mi #motivoparasonreír es tener la familia que tengo poder disfrutar dia a dia con ellos y además, dentro de 3 meses seremos uno más, así que no puedo pedir nada más”

![image](/tumblr_files/tumblr_inline_n214b4M6cN1qfhdaz.jpg)

Enhorabuena, Bárbara! Por el premio de PHB, y sobre todo por el otro premio que esperas. Ese sí que es gordo! Jajajaja… No olvides pasarme tus datos por privado. Y a los demás participantes, muchas gracias por compartir vuestros #motivosParaSonreir, y no os despistéis porque estoy preparando otra cosita muy cuca ;)