---
date: '2016-09-25T18:03:00+02:00'
image: /tumblr_files/tumblr_oe135aMLTN1qgfaqto1_400.gif
layout: post
tags:
- planninos
- familia
title: Buscamos tesoros con Geocaching
tumblr_url: https://madrescabreadas.com/post/150915223080/la-aventura-de-encontrar-tesoros-con
---

![](/tumblr_files/tumblr_oe135aMLTN1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_oe135aMLTN1qgfaqto2_500.gif)  
  

## La aventura de encontrar tesoros con Geocaching

## ¿Qué es el geocaching?

El [Geocaching](https://www.geocaching.com) es una aventura que empieza en tu móvil, donde te puedes bajar de forma gratuita la aplicación que te llevará a miles de tesoros escondidos por todo el mundo.

## Elige tu geocache

Gracias a la geolocalización podrás encontrar el punto exacto donde otro usuario aventurero ha escondido previamente su tesoro. Los hay por todo el mundo, sólo tienes que elegir uno cercano o que te guste su ubicación para aprovechar y explorar la zona.

## Busca el tesoro

Pero no creas que es tan fácil porque, una vez allí, tendrás que emplear todas tus dotes de sabueso para descubrir dónde está escondido, ya que no estará a la vista de cualquiera, sino que lo más seguro es que se halle en el sitio más insospechado.

 

Y esto es lo realmente divertido!

 

Aquí el ingenio y la Intuición son los protagonistas, y es cuando los niños se sienten verdaderos exploradores.

Déjalos imaginar!

## Anota la fecha y deja otro en su lugar

Cuando lo encuentres debes hacer dos cosas, la primera, cambiar el tesoro que encuentres por otro tuyo, y la segunda, firmar el libro con la fecha y un mensaje para el siguiente aventurero que lo encuentre.

 

Ah, y no olvides volverlo a esconder donde estaba!

Pero lo más interesante es que cualquier usuario puede crear su propio geocache y darlo de alta en la aplicación par que otros puedan encontrarlo.
## Nuestra aventura

Nosotros practicamos Geocaching desde que los niños empezaron a andar prácticamente, ya que hay varios niveles de dificultad, y algunos geocaches son de muy fácil acceso.

Pero el de este fin de semana era un multicache, lo que quiere decir que estaba compuesto de tres pasos o búsquedas: una pista que llevaba a una llave, que abría la caja del tesoro.

Mi enhorabuena al que lo creó este multigeocache, porque es realmente bueno y divertido, como podéis apreciar en los gifs, nos costó lo nuestro encontrarlo, pero los niños vivieron una aventura que no olvidarán fácilmente (ni los mayores tampoco).

Finalmente encontramos la caja con el tesoro, que contenía, entre otras cosas, un colgante con una inscripción y una ardillita de juguete.

Lo que dejamos nosotros no lo voy a revelar, sólo os diré que es lo que todo explorador querría hallar.

Esta experiencia en familia nos dio pie a enseñar a los niños cosas interesantes como qué comen las ardillas, el tipo de árboles que encontramos, incluso a orientarse en el bosque, y estas lecciones son de las que no se olvidan porque van más allá de los libros de texto.

Además, ya sabéis que el tiempo que regalamos a nuestros hijos es el mayor tesoro que pueden encontrar.