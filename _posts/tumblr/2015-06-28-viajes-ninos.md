---
date: '2015-06-28T17:00:21+02:00'
image: /tumblr_files/tumblr_inline_nqjh8yinFF1qfhdaz_540.jpg
layout: post
tags:
- trucos
- vacaciones
- planninos
- familia
title: Viajar con la familia. No te quedes en casa.
tumblr_url: https://madrescabreadas.com/post/122678280756/viajes-ninos
---

Reconozco que muchas veces nos cuesta arrancar y cambiar nuestra rutina diaria perfectamente organizada por el caos absoluto de equipajes, niños nerviosos corriendo por toda la casa a la espectativa de un viaje.

Me viene a la mente la famosa escena de la peli Solo en casa, y se me quitan las ganas de moverme de mi hogar.

<iframe src="//giphy.com/embed/Svqy0xIPSPXd6" frameborder="0" style="max-width: 100%" class="giphy-embed"></iframe>

Otras veces lo que nos hecha para atrás, sobre todo a las familias numerosas, es el tema económico, pero porque no hemos cambiado nuestra mentalidad anterior de viajar en avión , de hotel en hotel y de restaurante en restaurante. Sepan señores, que es posible pasar unas vacaciones estupendas viajando por carretera, con vuestra buena nevera en el maletero y durmiendo en casitas rurales la mar de cucas y hospitalarias. 

Si digo que os puede costar tres veces menos, creo que no me equivoco mucho.

No digo que no vayamos a algún restaurante en algún momento del viaje, pero ahorraréis mucho si vais comprando vuestra propia comida en el super de la zona.

A ver, ya sé que no son unas vacaciones ideales, como las que os contaba en este post… ¿o quizá sí…?

## Alojamiento en [Airbnb](http://www.airbnb.es/c/msanchez1919?s=8)

¿Por qué os recomiendo esta web/comunidad de red de alquiler de alojamientos en todo el mundo?

**Porque la he probado y funciona**

El verano pasado nos recorrimos el sur de Francia de pueblecito en pueblecito por 50/70 € la noche por casa rural para al menos cinco personas.

 ![image](/tumblr_files/tumblr_inline_nqjh8yinFF1qfhdaz_540.jpg)

El truco para conseguir esos precios es buscar destinos, si bien cercanos y bien comunicados con las ciudades importantes, pero no en la propia ciudad. De este modo, además de conocer las zonas rurales, algunas de gran belleza y encanto, podréis estar cerca de los sitios más turísticos sin que el bolsillo se resienta tanto.

Nosotros, al tener al bebé de dos años, no hacíamos más de 3 ó 4 horas al día, lo que nos dio un conocimiento bastante profundo de la Francia rural porque dormíamos cada día en un pueblecito, y aprovechábamos para conocerlo.

Se trata de una red de alquiler de alojamientos de particulares que, o bien les interesa alquilar una habitación de su propia casa, o una casa completa, incluso, algunos, la casita del jardín.

La garantía de que todo estará en orden te la da el propio funcionamiento de la comunidad, ya que cada usuario que disfrutad e un alojamiento puede compartir su opinión en la web, lo que puntuará a favor de ese sitio, influyendo en la decisión de otro nuevo posible usuario.

Además, existe una ficha de cada anfitrión, de manera que puedes conocerlo antes de reservar, comunicándote con él a través de la propia plataforma, de manera que todo queda supervisado por Airbnb.

Si existiera algún problema in situ por no ajustarse lo ofertado con la realidad, la empresa responde, y devuelve el dinero. Lo digo por experiencia.

 ![image](/tumblr_files/tumblr_inline_nqjhnoZ6XP1qfhdaz_540.jpg)

Para reservar, podéis hacerlo a través de 

[este enlace y obtendréis un **DESCUENTO EN AIRBNB**](https://www.airbnb.es/c/msanchez1919?s=8)

## Viajar por carretera

El avión normalmente sale mucho más caro, a no ser que podáis aprovechar alguna oferta que os cuadre. 

Las ventajas del coche, aparte de salir más barato, es que vosotros ponéis los horarios, flexibilizáis el viaje según os convenga, y os da independencia en el lugar de destino.

La tablet con dibujos animados fue de gran ayuda para los trayectos, y las áreas de servicio ayudaron a que las fieras se desfogaran en las zonas infantiles. La verdad es que en Francia están fenomenal.

Pero en España, cada vez van siendo mejores. Os recomiendo las de [Airea](http://www.areas.es/es/marcas/airea), porque tienen menú infantil con regalito, sillón cómodo de lactancia, WC para niños y cambiador,  sus zonas infantiles interiores con TV, y exteriores, potitos para un despiste…, además tienen wifi gratuíta.

 ![image](/tumblr_files/tumblr_inline_nqk6w3yff41qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_nqjllylVW71qfhdaz_540.jpg)

Podéis descargaros su [app para conducir por autopista aquí](https://www.autopistas.com/microsite/app/) para IOS y Android.

## Elige destinos especializados en familias

Cada vez más emplazamientos turísticos, hoteles y restaurantes cuentan con el [Sello de Turismo familiar del que os hablé en este post](/2014/07/28/vacaciones-en-familia-con-el-sello-de-turismo.html), lo que garantiza que una familia va a encontrarse cómoda y con instalaciones y actividades especialmente pensadas para niños.

 ![image](/tumblr_files/tumblr_inline_nqjlooFiwY1qfhdaz_540.png)

Búscalo o pregunta si lo tienen antes de reservar porque es una garantía.

## No olvides tu carnet de familia numerosa

La mayoría de museos y sitios oficiales hacen descuentos a las familias numerosas. 

Mi consejo es que preguntéis siempre que vayáis a entrar a un lugar donde haya que pagar entrada, ante la duda. Así, aunque no hayan contemplado este tipo de descuentos, quizá les deis la idea para el futuro…

¿No me digas que sigues pensando en quedarte en casa?