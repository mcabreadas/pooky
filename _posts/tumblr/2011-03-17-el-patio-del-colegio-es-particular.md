---
date: '2011-03-17T13:39:00+01:00'
image: /tumblr_files/patio.jpg
layout: post
tags:
- mecabrea
title: El patio del Colegio es particular...
tumblr_url: https://madrescabreadas.com/post/3919530299/el-patio-del-colegio-es-particular
---

Cuando pienso en cómo empezar se me viene a la cabeza la canción infantil “El patio de mi casa es particular…”, pero cambiando un poquito la letra, sería así: “El patio del Colegio es particular, cuando llueve se moja y a patinar…” Y es que el Colegio público de mis hijos es una joya antigua, pero tiene algunos problemas que deberían ser solventados por quien corresponda, parece ser que es al Ayuntamiento, pero no lo hace, a pesar de las numerosas cartas del AMPA a este respecto.

El caso es que en cuanto caen dos gotas de agua el suelo del patio se convierte en una pista de patinaje donde los niños se pegan culadas constantes, y las mamás y papás hacen extraños juegos de piernas para no caerse, incluso alguna abuelita llevando a su nieta al colegio se ha hecho un esguince. No es extraño ver a los más pequeños en el suelo llorando porque se han resbalado, mientras los mayores lo toman como un juego y cogen carrerilla para tirarse de rodillas al suelo y patinar sobre ellas para ver quién consigue llegar más lejos. En fin todo un número, al que si sumamos el lío normal de los días de lluvia, hace que la entrada y salida de los alumnos sea un caos total.

De momento no ha pasado nada serio, pero es que no queremos esperar a que pase para que lo arreglen, lo que queremos es evitarlo, porque lo que es un milagro es que nadie se haya hecho daño de verdad hasta ahora. Y lo que es una pena es que los niños no puedan salir al recreo ni hacer gimnasia cuando ha llovido recientemente y no ha dado tiempo a que se seque el suelo. Aunque seguiremos haciendo los escritos pertinentes desde el AMPA, me quejo aquí, y por lo menos me desahogo, ya me diréis si tengo razón.


*Photo by Jon Tyson on Unsplash*