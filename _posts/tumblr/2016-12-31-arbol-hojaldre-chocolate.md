---
date: '2016-12-31T10:23:02+01:00'
image: /tumblr_files/tumblr_oj12mzNNCK1qgfaqto6_400.gif
layout: post
tags:
- recetas
- navidad
- familia
title: Árbol de Navidad de hojaldre con chocolate
tumblr_url: https://madrescabreadas.com/post/155201783959/arbol-hojaldre-chocolate
---

## Árbol de Navidad de hojaldre y chocolate

Si no sabes qué poner de postre en Noche Vieja o en cualquier comida de Navidad que prepares en casa, y quieres sorprender con un dulce ligero, sencillo y original, y que puedas decorar a tu gusto, prueba a hacer este árbol de Navidad de hojaldre con crema de cacao, que se hace rapidísimo y te pueden ayudar tus hijos porque, además, es súper divertido de elaborar.

## Ingredientes del árbol de Navidad de hojaldre

-2 placas de hojaldre

-crema de cacao  
-1 huevo  
-azúcar glas para decorar  
-gominolas, frambuesas, lacasitos, conguitos… lo que tú quieras para decorar

## Cómo se hace el árbol de Navidad de hojaldre:
-precalienta el horno a tope de temperatura   

-En una bandeja de horno pon papel vegetal y extiende con cuidado las placas de hojaldre  

-Unta una de ellas con crema de cacao  

![](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto1_400.gif)

-cúbrela con la otra  

-diseña tu árbol haciendo un gran triángulo y un tronco rectangular en la base (márcalo con el dedo)  

![](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto3_400.gif)

-Corta la silueta con un cuchillo y retira lo que sobra. Resérvalo para hacer la estrella y otras figuras que te apetezcan.

 ![](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto4_400.gif)

-Haz franjas perpendiculares al tronco, pero sin llegar hasta el centro del mismo, que harán de ramas.

 ![](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto5_400.gif)

-Gira cada rama para que se quede torneada, como en el último gif.

![arbol de navidad de hojaldre con chocolate](/tumblr_files/tumblr_oj12mzNNCK1qgfaqto6_400.gif)

-Con el resto de masa que ha quedado al recortar el árbol haz estrellas, lacitos retorciendo tiras… o lo que se te ocurra, que puedes poner al pie del árbol como regalos o alrededor…

-Mételo al horno, previamente precalentando a tope de temperatura hasta que esté dorado.

-Déjalo enfriar, espolvorea con azúcar glass y decora con frambuesas, gominolas, lacasitos, conguitos, o lo que tengas por casa.

## Ideas

Puedes servirlo en una bandeja bonita, y poner nata de spray alrededor.

También puedes poner a cada comensal un cuenquito con chocolate fundido para que moje su trozo.

Con mis mejores deseos, que tengáis Feliz año nuevo!