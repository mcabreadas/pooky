---
date: '2017-04-17T21:47:31+02:00'
image: /tumblr_files/tumblr_inline_oo99t8RRVT1qfhdaz_540.png
layout: post
tags:
- moda
- familia
title: Moda infantil de primavera en Petit Style Walking
tumblr_url: https://madrescabreadas.com/post/159687764619/desfile-moda-infantil-primavera
---

¡Qué ganas tengo de primavera, de luz, de olor a azahar, de brisa marina! Y un soplo de todo ello fue el desfile solidario de moda infantil primavera verano 2017 al que asistí este fin de semana, que organizó la revista Petit Style, como ya viene siendo habitual cada temporada. 

(Podéis ver las anteriores ediciones a las que asistí aquí, aquí y aquí)

En una atmósfera absolutamente cuidada, que nos transportó a un bosque en floración, conocimos las propuestas de las principales marcas que vestirán a nuestros peques esta temporada.

 ![](/tumblr_files/tumblr_inline_oo99t8RRVT1qfhdaz_540.png)

El desfile solidario a favor de [Ayuda en Acción](https://www.ayudaenaccion.org/ong) fue presentado por Estefanía García, del blog [Con dos Tacones](http://estefaniapersonalshopper.blogspot.com.es/) y su marido, el humorista e imitador [Federico de Juan](https://twitter.com/fede_dejuan?lang=es), del programa radiofónico [Las Mañanas Kiss](http://www.kissfm.es/2016/04/04/equipo-las-mananas-kiss/), que nos hicieron reír para romper el hielo, y dar paso a la alegría y viveza de los niños, que se hicieron los amos de la pasarela una vez más vistiendo las siguientes marcas:

## Amaya: sofisticación 
 ![](/tumblr_files/tumblr_inline_pfx8uxQTi31qfhdaz_540.gif)

En [Amaya](http://es.artesania-amaya.com/) siempre han tenido muy claro que la moda infantil es mucho más que fabricar ropa para niños. Desde sus inicios han perseguido la esencia de la moda por el camino de la sencillos y la sofisticación, combinando diseño, tejidos y texturas.

## Nekenia: exclusividad
 ![](/tumblr_files/tumblr_inline_pfx8uyv1iK1qfhdaz_540.gif)

[Nekenia](http://www.nekenia.es/) nació en 2011, y se caracteriza por un diseño diferenciado, exclusivo y a la vez elegante, con tejidos de alta calidad a precios competitivos. Destaca la estampación propia y la fabricación en España.

## Paz Rodríguez: saber hacer y cariño
 ![](/tumblr_files/tumblr_inline_pfx8uzQljk1qfhdaz_540.gif)

Saber hacer y cariño. Ésa ha sido la seña de identidad de [Paz Rodríguez](https://pazrodriguez.com/) desde que empezaron a diseñar moda infantil en1970, y que han ido transmitiendo de generación en generación, lo que se refleja en sus prendas.

Esta labor de artesanos se complementa con tecnologóa avanzada, lo que permite ofrecernos la mejor calidad hecha en España y a un precio ajustado.

## Zippy: practicidad
 ![](/tumblr_files/tumblr_inline_pfx8uzU3S91qfhdaz_540.gif)

[Zippy](https://es.zippykidstore.com/) ayuda a los peques a cargar pilas y comenzar con fuerza y mucha alegría esta primavera. Infinidad de tejidos y los prints más divertidos es lo que se puede encontrar en la nueva colección  que busca comodidad y practicidad de los peques, sin olvidar que a ellos también les encanta estar a la última.

Ya te he hablado otras veces en en el blog de esta marca [aquí](/2015/12/09/moda-navidad-zippy.html), [aquí](/2016/03/20/chalecos-y-tirantes-con-zippy-nos-vamos-de-boda.html) y [aquí](/2016/06/22/ropa-fresca-zippy.html) porque en casa nos gusta mucho

## Meriché: personalidad
 ![](/tumblr_files/tumblr_inline_pfx8v0HbON1qfhdaz_540.gif)

Un gran conocimiento de la moda infantil clasica y las manos de la experiencia en la alta costura han dado como fruto una pasión escenificada en tela. [Meriché](http://www.meriche.es/index.html) es una nueva marca selecciona los tejidos más elegantes y los combina en forma, colorido y estampados de una forma muy personal.

**Por ser lectoras de mi blog tienes un CÓDIGO DESCUENTO para la tienda on line del 15%: AAM0804**
## Genuins: calzado inteligente
 ![](/tumblr_files/tumblr_inline_pfx8v1O2U21qfhdaz_540.gif)

[Genuins](https://genuins.com/) es especial por sus diseños únicos y por la calidad óptima de estos, pero además lo es por su labor de investigación en calzado inteligente o “bio”. Lo mejor es que no es necesario que el pie se adapte al zapato, sino que éste, desde la primera pisada, va poco a poco a nosotros como si fuera una prolongación de la planta de los pies. En esto consiste sus suela única.

## Neck & Neck: atención al detalle
 ![](/tumblr_files/tumblr_inline_pfx8v2XLSO1qfhdaz_540.gif)

El diseño atemporal de [Neck & Neck](http://www.neckandneck.com/) consiste en un estilo clásico que cuida los detalles, y cada temporada se va inspirando en las calles de las ciudades más cosmopolitas, en las pasarelas y ferias más importantes para ofrecer novedosos modelos.

## Bébécar: moda y seguridad
 ![](/tumblr_files/tumblr_inline_pfx8v3wtJy1qfhdaz_540.gif)

Ya sabéis que adoro [Bébécar](http://www.bebecar.com/) porque combina originalidad, últimos tejidos y diseños con resistencia, [seguridad](/2016/03/09/coleccion-primavera-bebecar.html) y confort. 

## Gioseppo Kids: comodidad y diseño
 ![](/tumblr_files/tumblr_inline_pfx8v4SPkV1qfhdaz_540.gif)

La nueva campaña de primavera verano de [Gioseppo Kids](http://gioseppo.com/es-es/kids/) cuenta una historia de amistad llena de buen rollo, música y diversión a través de un espectacular [fashion film](https://www.youtube.com/watch?v=6Yg1Aoebokc) que transmiten el gran salto de esta marca española de calzado infantil hacia el mundo de la moda.

A mis fieras [les encantan los zapatos de Gioseppo](/2016/10/11/comando-gioseppo-zapatos.html) porque van cómodos y tienen un diseño muy molón.

**CUPÓN ENVÍO GRATIS en la tienda on line por ser lectoras de mi blog: giomadres.cabreadas**
## Bóboli: frescura
 ![](/tumblr_files/tumblr_inline_pfx8v55jTn1qfhdaz_540.gif)

Curiosos, inquietos, traviesos, divertidos… Todo eso y más. Así son los niños y así es también [Bóboli](https://www.boboli.es/es/) porque quieren crecer con ellos, explorar, y acompañarlos en su descubrimiento del mundo.

La temporada pasada [Bóboli llevó a mis niños al bosque](/2016/10/20/moda-infantil-boboli-invierno.html), dónde iremos ahora?

## Tuc Tuc: originalidad
 ![](/tumblr_files/tumblr_inline_pfx8v5o3VZ1qfhdaz_540.gif)

En el divertido y colorista universo de [tuc tuc](https://www.tuctuc.com/es/) los originales estampados son su seña de identidad.

## Rochy: experiencia
 ![](/tumblr_files/tumblr_inline_oo99a3nxDZ1qfhdaz_540.png)

Desde 1954 esta casa fabrica prendas de baño infantil, pero en 2010 da el salto a la moda infantil especializada en tallas de 0 a 4 años bajo el nombre comercial de [Rochy](http://rochy.es/).

## Garvalín: calidad y confort
 ![](/tumblr_files/tumblr_inline_pfx8v6oqXH1qfhdaz_540.gif)

Para la marca de calzado infantil [Garvalín](http://www.garvalin.com/es/) ser niño es poder convertir cualquier cosa en un juego, encontrar el humor en cualquier situación, no estresarse por las pequeñas cosas, hacer amigos en cuestión de segundos, no tener vergüenza de preguntar, preguntar y seguir preguntando… decir las cosas como son, creer en imposibles y no tener límites. Así que hacen zapatos para niños que son niños, por eso acabaron el desfile jugando y corriendo por la pasarela.

**Por ser lectora de mi blog tienes un CODIGO DESCUENTO para la tienda on line en zapatos Garvalín del 30% hasta el 30/6/17: GARVALIN30**
## La Ormiga: romanticismo
 ![](/tumblr_files/tumblr_inline_pfx8v7IVly1qfhdaz_540.gif)

[La Ormiga](https://laormiga.com/) es una marca con linea junior ideal para adolescentes, muy femenina y elegante. Esta colección está llena de volantes, vuelos, flores y romanticismo.

**CODIGO DESCUENTO en la tienda on line hasta el 30-4-17: PETITMC**
## Charanga: diseño para todos
 ![](/tumblr_files/tumblr_inline_pfx8v8J8TI1qfhdaz_540.gif)

[Charanga](http://charanga.es/) es una de las firmas que más me gustó, con línea juvenil hasta la talla 16, con diseños originales y frescos, ideales para esa edad difícil en la que necesitan encontrar su propio estilo

**CÓDIGO DESCUENTO: PETITMC**

Si quieres obtener un 10% adicional al 25% de La Red Charanga por ser lectora de mi blog, sólo tienes que registrarte como clienta de La Red y luego, una vez logeada usar el código PETITMC antes de finalizar la compra.

## UBS2: dinamismo
 ![](/tumblr_files/tumblr_inline_pfx8v9LkCD1qfhdaz_540.jpg)Foto de Mario Agullo

[UBS2](http://www.ubs2.com/es)también tiene línea junior y crea moda urbana y alegre donde podemos encontrar desde looks cómodos de fin de semana, hasta vestidos de ceremonia. Sus diseños llenaron la pasarela de colores vivos y bonitos detalles.

## Nanos: dulzura y sencillez
 ![](/tumblr_files/tumblr_inline_pfx8v9qEJI1qfhdaz_540.jpg)

[Nanos](http://www.nanos.es/) tiene más de 50 años de experiencia, y ofrece ropa de primera puesta, de bebé e infantil. Su dulzura y sencillez nos conquistaron.

## Mc Gregor: sport de calidad
 ![](/tumblr_files/tumblr_inline_oo99b3lEjF1qfhdaz_540.png)

La firma [McGregor](http://www.mcgregor.es/es/ninos/) crea esta colección especial para niños con unos tejidos de excelente calidad y colorido que no pierde su intensidad tras las lavadas (lo he comprobado). Se trata de ropa práctica para que las madres no estemos preocupadas por si se manchan o se estropean la ropa, y muy cómoda para que se sientan libres para jugar. Su estilo es deportivo y está inspirado en la forma de vestir de los alumnos de la élite de los institutos americanos.

## Dadati: elegancia y dulzura
 ![](/tumblr_files/tumblr_inline_oo99bpI48k1qfhdaz_540.png)

Uno de mis descubrimientos de esta pasarela ha sido [Dadati](http://dadati.es/es/), con ropa de 0 a 10 años. Me han encantado sus diseños, su calidad y su dulzura tanto en combinación de colores como en tacto. Apuesta por líneas clásicas y elegantes con un toque especial de las últimas tendencias.

## Ágatha Ruiz de la Prada: alegría y locura
 ![](/tumblr_files/tumblr_inline_oo99ccDK6L1qfhdaz_540.png)

[Ágatha Ruiz de la Prada](http://www.agatharuizdelaprada.com/store/nina/) llenó la pasarela de locura con su colección “Crazy”, más alegre, dinámica y vital que nunca. “Crazy” hace un guiño a la imaginación y sorprende con leotardos en macro-motas multicolor, bitono y glitter metálico, tules, microfibras personalizadas, tejidos estructurados en algodón, materias gamuzadas, etc.

La guinda de la pasarela la puso [Leyre](https://www.instagram.com/leyre_cantante_official_/) y su maravillosa voz, que nos impresionó con el tema de Jennifer López Ain´t Your Mama.

Después, los niños asistentes pudieron participar en un taller de manualidades “eco” con [Canna](http://www.jardineriacanna.com/) altamente recomendable si vivís en Madrid porque lo pasaron pipa.

 ![](/tumblr_files/tumblr_inline_oo99ljnqra1qfhdaz_540.png)

Mi sincera enhorabuena a Ana, Antonio y todo el equipo de la revista Petit Style, y sus colaboradores porque desprendían cariño en todo lo que hicieron, y eso da como resultado eventos mágicos como el #PetitStyleWalkingMadrid.

(Y sí, Antonio… he puesto bien el hastag)