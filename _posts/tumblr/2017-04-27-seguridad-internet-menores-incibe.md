---
date: '2017-04-27T12:54:18+02:00'
image: /tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg
layout: post
tags:
- adolescencia
- tecnologia
- crianza
- seguridad
title: 'Conoce IS4K: Interenet Segura for Kids'
tumblr_url: https://madrescabreadas.com/post/160045351104/seguridad-internet-menores-incibe
---

Tengo hijos preadolescentes, y no les puedo negar el móvil, la tablet, el acceso a Internet o a las redes sociales porque no les puedo apartar del mundo. Eso lo tengo claro.

Además, creo que sería contraproducente porque si lo hiciera, cuando finalmente los usaran, seguramente serían algo hostil y desconocido para ellos, y no sabrían usarlos correctamente, con el consguiente riesgo para ellos.

Veo mejor opción que los [padres estemos al día en las nuevas tecnologías](/2015/03/21/internet-intimidad-menores.html) y vayamos por delante de ellos para poder guiarlos y aconsejarles, aunque a veces no es tan fácil porque ellos son nativos tecnológicos, y nosotros somos de otra generación.

Promover el uso seguro y responsable en Internet y las nuevas tecnologías entre los menores y su entorno, es el objetivo de un servicio puesto en marcha por el Ministerio de Energía, Turismo y Agenda Digital, a través de la Secretaría de Estado para la Sociedad de la Información y Agenda Digital (SESIAD) y del [Instituto Nacional de Ciberseguridad (INCIBE)](https://www.incibe.es/).

 ![](/tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg)
## ¿Qué es IS4K?

Internet Segura for Kids (IS4K) es el nombre de esta iniciativa a la que puede accederse a través de la web [https://www.is4k.es/](https://www.is4k.es/), un servicio que pretende proteger al colectivo más débil en la Red, puesto que la mitad de los menores españoles utiliza las redes sociales, concretamente usan Internet un 30% de los niños de 10 años, un 79% de niños de 13 años de edad y más del 90% de los chavales de 15 años.

## Heramientas gratuítas

Lo más útil es el catálogo de herramientas gratuitas y los materiales didácticos, que pueden descargarse directamente de forma gratuita.

También dispone de juegos para aprender ciberseguridad, catálogos de recursos, contratos familiares y multitud de herramientas más para que niños y padres se conciencien de forma amena sobre la importancia de usar Internet de forma segura y responsable.

## Formación gratuíta 

También tienen un programa cibercooperantes, que cuenta con más de 280 integrantes que de forma altruista contribuyen a concienciar y formar a niños, jóvenes y padres por todo el territorio nacional.

Su programa de **jornadas escolares gratuitas** es muy interesante, ya que incluyen charlas de sensibilización para el alumnado y un taller de capacitación para educadores sobre privacidad, identidad digital y reputación; netiqueta o comportamiento en línea; gestión de la información y acceso a contenidos inapropiados; ciberacoso escolar; protección ante virus y fraudes; uso excesivo de las TIC, mediación parental y uso seguro y responsable de las TIC.

## Para entender a nuestros hijos…

La sección ‘Necesitas saber’, el test de conocimientos o artículos sobre cómo pueden afectar los estados de WhatsApp a nuestros hijos o qué quieren decir los jóvenes con palabras como ‘trol, insta, follower’, son otras de las publicaciones más visitadas.

## Pide ayuda

Por otro lado, desde la línea de ayuda del Centro de Seguridad en Internet para menores en España, que próximamente dispondrá de un canal de atención telefónica, se han gestionado consultas relacionadas con herramientas de control parental, legislación y formación en ciberseguridad. Pero también se ha asesorado a menores, familias, educadores y profesionales del ámbito del menor sobre cómo hacer frente a riesgos de Internet como el grooming (acoso sexual a menores) o sexting (envío de fotografías o vídeos de carácter sexual).

## Por una Internet mejor para nuestros hijos

IS4K forma parte de las redes europeas INSAFE e INHOPE en el marco de la estrategia europea para una Internet Mejor para los Menores (BIK), también está presente en redes sociales como Facebook y Twitter, en las que ya cuenta con un total de 1.265 seguidores.

En reconocimiento de su labor, IS4K recibió hace unos días el Premio Zapping de la Junta Directiva de la Asociación de Consumidores de Medios Audiovisuales de Cataluña, como primer paso para impulsar con fuerza la defensa del menor en la red.

Ya no hay excusa, ponte al día y protege a tus hijos!