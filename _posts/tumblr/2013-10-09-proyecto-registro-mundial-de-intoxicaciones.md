---
layout: post
title: Por un registro mundial de intoxicaciones pediátricas
date: 2013-10-09T11:23:00+02:00
image: /images/uploads/depositphotos_111835820_xl.jpg
author: .
tags:
  - crianza
  - 3-a-6
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/63544927550/proyecto-registro-mundial-de-intoxicaciones
---
<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/QV7r8hTOC9Q?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

Las intoxicaciones agudas pediátricas constituyen un importante problema sanitario y social, además de un gran preocupación para los padres y madres.

Os presento un proyecto científico elaborado por pediatras españoles que pretende crear un registro mundial de intoxicaciones pediátricas para la prevención y tratamiento de las intoxicaciones infantiles.

## **¿Por qué es necesario un** Registro mundial de intoxicaciones pediátricas**?**

Se ha comprobado que las campañas de información llevadas a cabo hasta ahora no han logrado disminuir los casos de intoxicaciones de niños, lo que demuestra que este sistema de prevención no funciona.

## ¿En qué consiste el proyecto del Registro mundial de intoxicaciones pediátricas?

El proyecto está basado en el acercamiento a la sociedad para extraer la información necesaria de los centros de urgencias con el objetivo de disminuir las intoxicaciones recreacionales en los niños, y generar además un efecto multiplicador del mensaje.

## **¿Quién ha impulsado el proyecto** Registro mundial de intoxicaciones pediátricas**?**

El equipo médico del Servicio de Urgencias Pediátricas del Hospital Universitario Cruces lleva varios años trabajando en el tema, pero aun así les queda mucho por saber. Ningún colectivo médico antes había tenido acceso a un registro tan numeroso de intoxicaciones, más de 1000 episodios hasta la fecha. El análisis de los datos ayudará a tratar mejor a los pacientes y las diferencias encontradas servirán para consensuar tratamientos y diseñar estrategias preventivas más eficaces.

**Necesidad de difusión**

Con la ayuda de los datos obtenidos en el registro, y el apoyo de la población, se podrá invertir la situación. Por ello, el registro internacional es una herramienta fundamental. Pero la información es poder si se conoce, porque si no el trabajo realizado es en vano.

**¿Qué se ha logrado a nivel nacional?**

En el año 2008 se creó desde la SEUP (Sociedad Española de Urgencias de Pediatría ) el Observatorio Toxicólogico, que a día de hoy cuenta con 44 hospitales implicados a lo largo de la geografía española. Hoy todavía sigue en marcha, en 5 años de trabajo, han recogidos más de 500 episodios de intoxicación.

**A nivel internacional**

En un aproximadamente 6 meses, se consiguieron reunir a más de 130 servicios de urgencias pediátricas dispuestos a participar. En enero de 2013 comenzó el SISTEMA MUNDIAL DE VIGILANCIA TOXICOLÓGICA EN PEDIATRÍA DE URGENCIAS (urgenciaspediatria.hospitalcruces.com) . Tras 9 meses cuentan ya más de 1000 episodios y los primeros datos ya publicados.

**¿A quién va dirigido el proyecto?**

Es fundamental la implicación de la sociedad, ya que todos juntos (padres, madres, profesores/as, médicos/as…) podremos ejercer una presión mayor ante las autoridades, que juegan un papel muy importante en la reducción de estos episodios, por ejemplo mediante la creación de nuevas leyes como la que hubo en su día entorno a la lejía, y es que hoy en día su concentración máxima en las lejías domésticas no puede exceder el 5%, concentración a la que no es tóxica, evitando así numerosas consultas.Pero para llegar a las familias es necesario primero que los resultados se difundan en la comunidad científica médica y entre las autoridades.

**¿Para qué necesitan nuestra ayuda?**

Hasta ahora, el proyecto ha sido llevado a cabo por pediatras y epidemiólogos/as del Hospital Universitario de Cruces de una forma altruista y sin financiación alguna. Su desarrollo hasta que concluya la recogida de datos se seguirá haciendo de la misma forma, pero para poder difundir los resultados obtenidos tanto en el ámbito científico como al resto de la población necesitan financiación, y este es el objetivo principal de la campaña de crowdfunding.

Precisan fondos para:\
. Asistir a reuniones internacionales de Urgencias Pediátricas para poder dar a conocer los resultados\
. La creación de una guía de recomendaciones didáctica a partir de los resultados obtenidos en el estudio.\
. La creación de una plataforma on-line para la visualización pública de los resultados.

**¿Quieres colaborar?**

Dona desde un Euro pinchando [aquí](http://goteo.org/project/toxicosinfancia/invest) y conviértete en cofinanciador/a del proyecto, lo que te puede dar acceso a varias recompensas individuales como un diploma, una invitación VIP a la jornada de divulgación o una guía de recomendaciones ante una intoxicación entre otras.

Te apetece financiar este proyecto y contribuír a cambiar el mundo?

Más info [aquí](http://goteo.org/project/toxicosinfancia).



*Foto gracias a [sp.depositphotos](https://sp.depositphotos.com/)*