---
date: '2014-12-18T22:58:57+01:00'
image: /tumblr_files/tumblr_inline_nmpe5tekGs1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- familia
- seguridad
- bebes
title: 'Sistemas de retención para automóvil: la silla convertible Doona'
tumblr_url: https://madrescabreadas.com/post/105552759964/silla-automovil-doona
---

Ya os he hablado en otras ocasiones sobre [cochecitos para recién nacidos](/2014/12/06/claves-eleccion-cochecito-bebe.html), [sillas de paseo](/2014/11/27/testing-silla-paseo-peppluxx-nuna.html) ligeras para cuando son más grandecitos, [sistemas de retención para automóvil a contra marcha](/2014/11/12/sillas-automovil-seguridad-ninos.html), pero no os había hablado específicamente sobre sillas de grupo 0+ para viajar en coche.

Como ya sabéis estuve en la [Feria de Puericultura Madrid 2014](http://www.ifema.es/puericulturamadrid_01), y tuve la oportunidad de probar la Silla Doona para bebés de 0 a 13 Kg que, como ya veréis en el video, es algo más que una silla para el coche:

<iframe src="//www.youtube.com/embed/JQPf9nWzA0k?rel=0" width="100%" height="315" frameborder="0"></iframe>

## Yo misma la probé

Yo misma la plegué, la metí en un coche que había en el stand de [nikidom](http://www.nikidom.com/) para pruebas, y después la volví a sacar del vehículo y desplegué sus ruedas, y os puedo decir que lo pude hacer con facilidad y suavidad porque no tiene anclajes o mecanismos que requieran movimientos bruscos.

 ![](/tumblr_files/tumblr_inline_nmpe5tekGs1qfhdaz_540.jpg)

Quizá sus ruedas ocupen algo más que una silla grupo 0 normal, pero para un coche normal vale sin problemas.

Por algo la incluí entre mis [Siete Caprichos de la Feria](/2014/10/03/puericultura-madrid.html)

 ![image](/tumblr_files/tumblr_ncvj43PrzW1qgfaqto8_400.jpg)

## Seguridad

Pero lo que más me gusta de ella es que el bebé va en sentido contrario de la marcha, y además permite posicionar el asa de la silla contra el respaldo del asiento del coche, lo que lo evitando el efecto rebote.

Las dos capas de plástico resistente absorbe-impactos, junto a capas adicionales de EPS, espuma y tejidos  protegen aún más al bebé en caso de impactos laterales.

 ![](/tumblr_files/tumblr_inline_nmpe6lEmvb1qfhdaz_540.jpg)

## Sencillez

La sencillez de uso del sistema de anclado previene malas instalaciones.  Y además tiene memoria, es decir, cuando lo accionas y sueltas, no vuelve a su posición bruscamente, sino que va lentamente para que te de tiempo perfectamente a extraer la silla sin hacer movimientos bruscos que despierten al bebé si va dormidito, o que puedan hacer que te dañes la mano.

## Una curiosidad: la inventó un papá práctico

¿Sabéis que el sistema integrado de chasis dentro del grupo 0+ fue inventado por un papá que se volvía loco cada vez que enía que salir a hacer recados en coche con su peque?. Se hartó de tener que cambiar al bebé de la silla del coche al cochecito cada vez que paraba en el super, en la tintorería o en el cole de los mayores, y se hartó de plegar y desplegar cada vez el carricoche, y de que el bebé se le despertara cada dos por tres.

Entonces ideó este sistema que permite ese cambio sin tener que mover al bebé de sitio, y además,como se pliega y despliega con tanta suavidad, no tiene por qué despertarse si se ha quedado dormido.

Además, este papá pudo por fin llenar el maletero con la compra porque lo consiguió dejar libre al no necesitar el carricoche para ir a hacer recados.

No es una silla que se recomiende para sustituír el cochecito, ojo. Ya que la posición no es la idónea para pasar muchas horas el bebé, pero sí para tenerlo a ratitos.

**Colores:**

 ![image](/tumblr_files/tumblr_inline_ngsr2axhcX1qfhdaz.jpg)

 ![image](/tumblr_files/tumblr_inline_ngsr2on4f41qfhdaz.jpg)

 ![image](/tumblr_files/tumblr_inline_ngsr3wMACE1qfhdaz.jpg)

 ![image](/tumblr_files/tumblr_inline_ngsr4zxfes1qfhdaz.jpg)

## Complementos

Además tiene todo tipo de complementos: protector para la lluvia, para el sol, incluso una mosquitera por si vivís en una zona donde los mosquitos sean molestos, como es mi caso.

 ![image](/tumblr_files/tumblr_inline_ngssj3i2q51qfhdaz.jpg)

También me gustó el detalle del freno: para quitarlo no hay que levantarlo, sino pisar, lo que evita la incomodidad, sobre todo si usas sandalias.

 ![](/tumblr_files/tumblr_inline_nmpe76PBFL1qfhdaz_540.jpg)

Os lo he querido contar porque me resultó curioso este nuevo concepto de silla de coche por lo práctico que es.

También es cierto que el bebé lo usará durante un periodo de tiempo corto, pero os aseguro que os durará para sus futuros hermanitos. En mi caso, con tres, imaginaos si la hubiera amortizado… lástima que estos inventos tan modernos no estuvieran en mis tiempos!