---
date: '2014-09-29T09:10:00+02:00'
image: /tumblr_files/tumblr_inline_nclqdn8SuG1qfhdaz.jpg
layout: post
tags:
- crianza
- participoen
- local
- colaboraciones
- ad
- eventos
- lactancia
- familia
- parto
- bebes
title: La crianza consciente llega a Murcia
tumblr_url: https://madrescabreadas.com/post/98710101478/la-crianza-consciente-llega-a-murcia
---

Gran sorpresa al encontrar en Murcia una plataforma de profesionales dedicados a la maternidad y paternidad consciente. [Infancia consciente](https://www.facebook.com/infanciaconsciente) promueve una infancia más respetada, despierta y auténtica en los niñ@s, en los adultos y en la familia.

El 28 de septiembre tuvo lugar la I Jornada de Infancia Consciente en el [Cuartel de Artillería](https://www.facebook.com/cuartel.artilleria) de Murcia, donde se impartieron charlas y talleres durante todo el día, con un programa ambicioso y muy completo:

 ![image](/tumblr_files/tumblr_inline_nclqdn8SuG1qfhdaz.jpg)

Como soy mamá de tres fieras tuve que elegir entre toda la oferta, y me decidí por el taller de masaje infantil, y el de radio, al tiempo que escuchaba algo de las charlas de lactancia y parto mientras perseguía a Leopardito, que iba corriendo por todo el Pabellón 2 del Cuartel de Artillería como un loco.

 ![image](/tumblr_files/tumblr_inline_nclsza8bgS1qfhdaz.jpg)

También tuve la oportunidad de hablar con “Portea con el alma”

 ![image](/tumblr_files/tumblr_inline_nclt67N8qc1qfhdaz.jpg)

Princesita participó en el [taller de radio de Minicasters](https://minicastersradio.wordpress.com/2014/10/02/i-jornadas-de-infancia-consciente/), empresa que lleva la radio a los colegios como complemento de la formación académica. En grupos reducidos hacen su propio programa. Muy recomendable!

 ![image](/tumblr_files/tumblr_inline_nclr46dfJa1qfhdaz.jpg)

Desde aquí, todo mi apoyo a esta iniciativa que, aunque joven, arranca con mucha fuerza e ilusión de la mano de grandes mujeres como Sirena García, con la que tuve el gusto de reencontrarme. Gracias, por tu acogida.