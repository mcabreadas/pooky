---
date: '2016-03-22T09:55:07+01:00'
image: /tumblr_files/tumblr_inline_o4fnfuDPol1qfhdaz_540.jpg
layout: post
tags:
- libros
- planninos
- familia
title: Libros para leer y ayudar en familia
tumblr_url: https://madrescabreadas.com/post/141479516209/libros-ayuda-ninos
---

No soy partidaria de llenar a los niños de deberes en vacaciones, pero sí que es bueno que dediquen un rato al día a leer un libro que les guste, y si lo eligen ellos, mejor.

Debemos plantearlo, no como una obligación, sino como algo divertido. De este modo repasarán ortografía, gramática, y aprenderán mil cosas al hilo de la historia que están disfrutando.

Si os vais de viaje, no olvides llevar algún libro adecuado a su edad, y algún otro para ti porque si te ven leer, ellos también leerán.

Te voy a recomendar unos cuantos libros que me han gustado especialmente para darte algunas ideas.

## Los atrevidos, de Elsa Punset
 ![](/tumblr_files/tumblr_inline_o4fnfuDPol1qfhdaz_540.jpg)

Me encanta [escuchar hablar a esta mujer](https://www.youtube.com/user/ElsaPunset), de verdad. Parece todo tan sencillo cuando ella lo explica…

Pues no sé si sabes que ha sacado una serie de libros para ayudar a los niños a entrenar sus emociones porque no sólo se trata de enseñarlos a leer, escribir, sumar, restar… a veces nos olvidamos de lo importante que es que se desarrollen una buena inteligencia emocional para que sepan vivir felices y convivir.

Nosotros en casa hemos trabajado el tema de los miedos con [“Los atrevidos dan el gran salto”](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2603652), y el de la autoestima con [“Los atrevidos en busca del tesoro”](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2603651), de la editorial Penguin Random House

Además de la historia a modo de cuento con ilustraciones muy bonitas, ambos incluyen claves para padres y estrategias para vencer los miedos y mejorar la autoestima de los niños (aunque a mí me han servido también).

## Mamá, Papá, ¿qué es la muerte? 
 ![](/tumblr_files/tumblr_inline_o4fnfu1igt1qfhdaz_540.jpg)

Tarde o temprano lo preguntan. Quizá no con esas palabras, pero a los 6 ó 7 años los niños empiezan a plantearse cosas que no habían hecho hasta ahora. Nuestra actitud y nuestra respuesta es clave para que ellos lo vean como algo natural, y aprendan a afrontar la tristeza y comprender sus sentimientos.

 ![](/tumblr_files/tumblr_inline_o4fnfu88NI1qfhdaz_540.jpg)

Seguros Meridiano edita una guía infantil para ayudarnos en esta tarea que puedes [descargar gratis aquí](https://www.segurosmeridiano.com/es/prensa/mama-papa-que-es-la-muerte-guia-para-padres-por-meridiano/).

Los contenidos de esta guía han sido elaborados por un equipo de sicólogos expertos en el tema.

## Dialhogar en familia
 ![](/tumblr_files/tumblr_inline_o4fnfvwJ8S1qfhdaz_540.jpg)

Éste es para papás y mamás.

Esta joya de Mercedes y Marta Blasco defiende que desde la familia en su vida cotidiana se pueden poner los cimientos de los futuros talentos para mejorar la economía y, en consecuencia, la sociedad.

> Cuando toda la familia se implica desde el principio en compartir las tareas del hogar, se educa en corresponsabilidad, se resuelven las tareas entre todos y se forman mejores personas que son los que hacen historia.

[Aquí lo puedes descargar gratis](http://www.fundacionmteresarodo.org/wp-content/uploads/2007/11/Dialhogar-en-familia.-Soluciones-hoy-y-talentos-de-futuro.pdf).

Espero haberte dado buenas ideas.

_Si te ha gustado, compártelo. No te cuesta nada, y a mí me harás feliz :)_