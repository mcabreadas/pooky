---
date: '2014-08-18T10:00:00+02:00'
image: /tumblr_files/tumblr_inline_naeckr8jjp1qfhdaz.jpg
layout: post
tags:
- crianza
- vacaciones
- trucos
- planninos
- educacion
- familia
- bebes
title: 'Actividades con niños en la piscina: Natación para bebés.'
tumblr_url: https://madrescabreadas.com/post/95081099850/actividades-con-niños-en-la-piscina-natación-para
---

Este verano estamos aprovechando los largos días de piscina para enseñar a nadar nuestro bebé de dos años, a quien muchos conocéis como Leopardito.

Sus dos hermanos mayores tuvieron la suerte de asistir a clases de natación para bebés a partir de los 6 meses de edad, que consistían en unos baños conmigo en una piscina climatizada de un centro deportivo de nuestra ciudad que ofrece esta actividad muy recomendable.

Pero con el peque no he podido hacerlo, y he decidido aplicarle todo lo aprendido con sus hermanos mayores y darle yo misma clases por mi cuenta. Oye, y nos lo pasamos pipa!

En el centro deportivo donde llevaba a sus hermanos mayores una monitora nos daba pautas a las mamás o papás que nos bañábamos con los bebés para que aprendieran a mantenerse en el agua, salir a flote tras sumergirlos y a respirar en el agua sin tragar. 

Realmente no les enseñaban a nadar en sentido estricto, ya que esto no se logra hasta los 4 ó 5 años. Antes de esta edad los niños son demasiado pequeños para desarrollar autonomía en el agua y adquirir los movimientos de la natación. Más bien lo bebés aprendían a moverse en el agua, mantener el equilibrio, sumergirse sin tragar agua y salir a flote y buscar y trepar por el bordillo e la piscina para salir, además de pasarlo en grande con su papá o su mamá reforzando el vínculo entre ellos y sentirse más seguros de sí mismos.

Pero para mí, lo más importante y práctico de la natación para bebés es que desarrolla sus habilidades vitales de supervivencia, por ejemplo, girarse sobre su espalda y flotar ante una caída al agua.

Esto último lo comprobé hace unos días con Leopardito, cuando estabamos todos en la piscina y se tiró él solo y se hundió. Pero cerró la boca y no tragó agua. Ni que decir tiene que lo saqué en un nanosegundo, pero entonces me di cuenta de lo importane que es enseñarles desde bien pequeños a sobrevivir en el medio acuático porque esta primera reacción de ellos te da margen para alcanzarlos y sacarlos en caso de accidente.

Los ejercicios que practicamos son muy básicos y sencillos, y por supuesto hay muchos más. Pero sobre todo hay que hacerlos respetando las preferencias del bebé y lo que él vaya marcando ya que poco a poco irá pidiendo más. Es mejor mostrar el ejercicio pero si no le apetece hacerlo, no forzar, ya que en unos días seguramente será el niño quien pida más dificultad conforme vaya estando preparado.

**Postura básica**

Para empezar, la postura básica es la de la foto. Recomiendo no usar manguitos porque se creerán que flotan aunque no los lleven puestos y pueden llegar a tirarse, como Leopardito, con una falsa sensación de seguridad que se verá truncada con el susto que se llevarán al hundirse. 

 ![image](/tumblr_files/tumblr_inline_naeckr8jjp1qfhdaz.jpg)

Se sujeta al bebé por las axilas con ambas manos pero dejándolo libre sin pegar al nuestro cuerpo y con las piernas a nuestro lado para evitar que nos dé patadas.

También se le puede sujetar con un solo brazo pasándolo por delante del pecho del bebé y sujetando una sola axila con la mano, quedando la otra sobre nuestro brazo, sin más contacto que ese brazo, es decir, sin pegarlo a nuestro cuerpo para que él vaya dirigiendo.

**Ejercicio uno: Balanceo**

Otro ejercicio es tomarlo de los brazos mirando hacia nosotras y balancearlo de un lado a otro para que tome conciencia de la sensación de movimiento que va a tener en el agua y se acostumbre poco a poco a la inestabilidad del medio acuático.

**Ejercicio 2: De espaldas**

 ![image](/tumblr_files/tumblr_inline_naecmntst01qfhdaz.jpg)

Mi postura favorita es con la cabeza del bebé apoyada en mi pecho boca arriba, relajada para que dé patadas y avance hacia atrás. Ellos miran al cielo y se divierten salpicando, como se ve en la foto.

**Ejercicio 3: Salto**

Un paso más lo damos saltando desde el bordillo cogiéndolo de ambas manos y sin dejar que sumerja la cabeza en principio, aunque poco a poco, conforme vayan pasando los días, se irá sumergiendo un poquito más, hasta que se capuce del todo cuando estemos seguros de que cierra la boca.

 ![image](/tumblr_files/tumblr_inline_naecedptGI1qfhdaz.jpg)

Es importante contar hasta tres siempre para que él tenga claro cuando tiene que saltar, y recordarle que cierre la boca al caer. Al principio no la cerrará nunca, incluso se reirá y le entrará más agua, pero no desesperéis, el mensaje le cala cuando traga unas cuantas veces.

El contar hasta tres es importante para que no se tiren solos. Hay que repetirles que siempre se tiren con mamá o papá, y sólo cuando contemos hasta tres, además de que se pongan con los pies pegados al bordillo y salten hacia adelante para evitar que se golpeen con el al caer.

La siguiente fase de este ejercicio es que salten solos hacia ti y los cojas, y más adelante te podrás alejar un poco más para que buceen solos hacia ti.

**Ejercicio 4: Subir por el bordillo**

El último paso de este ejercicio es que cuando salten y lleguen buceando hacia ti los giremos y regresen al bordillo, subiendo y saliendo de la piscina.

 ![image](/tumblr_files/tumblr_inline_naech5Hged1qfhdaz.jpg)

Creo que entonces habremos logrado nuestro objetivo de lograr bebés supervivientes en el agua.

Recordad que no debéis dejarlo solo bajo ningún concepto. Para practicar estos ejercicios es necesario que el adulto esté todo el rato con el bebé por su seguridad.

No olvidéis empezar y terminar la sesión con sus juegos favoritos, sin presiones y respetando sus preferencias. Ah!, y mucho amor.

 ![image](/tumblr_files/tumblr_inline_naecp9QM7Q1qfhdaz.jpg)