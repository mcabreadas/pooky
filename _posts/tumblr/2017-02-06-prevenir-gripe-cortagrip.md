---
date: '2017-02-06T16:08:05+01:00'
image: /tumblr_files/tumblr_inline_ol3gxxU3sI1qfhdaz_540.png
layout: post
tags:
- salud
- trucos
- crianza
- recomendaciones
title: Mamá, ¿por qué tú no coges la gripe?
tumblr_url: https://madrescabreadas.com/post/156891349069/prevenir-gripe-cortagrip
---

Con esta pregunta me sorprendió el más pequeño de mis hijos, de 4 años, el otro día cuando los llevaba al cole una mañana fría de invierno.

Después del shock que me provocó la ocurrencia, y de darme cuenta de que tenía razón, porque este invierno no he caído enferma ninguna vez todavía (cruzo los dedos de las manos y de los pies), le contesté: 

> -“Hijo, porque las mamás y los papás no nos podemos poner malitos.
> 
> -(ojos como platos) Ah, no? Y por qué?
> 
> -Pues porque si no, ¿quién os iba a cuidar?”

Y me eché a reír ante la mirada atónita de los mayores, que no sabían si iba en serio o en broma.

Y la verdad, es que, aunque lo dije entre risas, luego lo pensé, y es bastante cierto lo que le contesté.

Y si no, dime cuándo fue la última vez que pasaste un día entero en la cama curándote de un resfriado. Yo… ni me acuerdo… porque las madres pasamos las enfermedades de pie, como campeonas, por eso le tenemos más miedo a la gripe que a un “nublao”, porque esa sí que te tumba quieras o no.

## Trucos para evitar resfriados

Además, buscamos todo tipo de remedios caseros y trucos para no constiparnos. Te cuento los míos:

-Un **zumo de naranja** al día (no es un mito, de verdad que me funciona)

-Llevo siempre el **cuello cubierto** , ya sea con pañuelo, fular, jerseys de cuello cisne o braga de lana o forro polar. Si estoy en casa, un pañuelo fino, pero nunca lo dejo al descubierto. Esto es fundamental porque enseguida se me irrita la garganta con los cambios de temperatura.

-Estoy atenta al menor síntoma de escozor o picor de garganta para intervenir rápidamente con sprays tipo **[Cortagrip](http://www.cortagrip.com/)**, éste forma una película protectora en la garganta, que ayuda a evitar que los virus entren para quedarse. En cuanto empieza el frío me lo meto en el bolso, y a la mínima, ya me lo estoy echando, pero es que mi marido me teme porque en cuanto abre la boca para toser, también se lo “chuflo”, y casi ni lo dejo que respire. 

 ![anti gripal cortagrip](/tumblr_files/tumblr_inline_ol3gxxU3sI1qfhdaz_540.png)

-Otra parte sensible, y traicionera de mi cuerpo son los pies. En cuando se me enfrían, ya la hemos liado, así que doble calcetín cuando salgo, y cuando estoy en casa, mi **calienta pies** no me falla.

 ![calienta pies](/tumblr_files/tumblr_inline_ol3gxy8PQP1qfhdaz_540.png)

 -La **batamanta** , esa amiga fiel. Antes me pasaba que después de andar súper atareada por casa haciendo mil cosas me sentaba a descansar mientras tuiteaba un rato, y me entretenía tanto que cuando me daba cuenta me había quedado helada por el cambio de temperatura al parar de moverme de golpe, y ahí no me escapaba de caer enferma. Así que comencé a usar mantas de las que me hacía mi abuela, pero era bastante difícil teclear en el móvil dentro de la manta, así que mi cuñado me regaló el invento del siglo: la bata manta.

 ![batamanta](/tumblr_files/tumblr_inline_ol3gxy7LP51qfhdaz_540.png)

¿La has probado? Si lo haces no podrás vivir sin ella.

-Ojo con la **calefacción** demasiado fuerte. Si la ponemos excesivamente alta, los cambios de temperatura que sufrirá nuestro cuerpo serán demasiado bruscos, y tendremos más probabilidades de constiparnos.

- **El ponche de mi abuelo**. Cuando veo que estoy cayendo, además de echarme el Cortagrip, me preparo [este ponche del que os hablé](/2013/10/14/remedio-casero-para-el-resfriado-el-ponche-del.html) que hacía mi abuelo para “sudar el resfriado”, y que nos enseñó a preparar.

Yo creo que, entre todos los rituales que hago, y que con tres fieras, no puedo permitirme el lujo de meterme en la cama, estoy tan mentalizada que de momento no he caído. 

¿Qué magia haces tú para estar al pie del cañón cada día?