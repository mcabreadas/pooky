---
date: '2016-06-27T10:00:23+02:00'
image: /tumblr_files/tumblr_inline_o9a0jen3IE1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- bebe
- puericultura
- ad
- eventos
- recomendaciones
title: Chicco apuesta por lo natural
tumblr_url: https://madrescabreadas.com/post/146544347372/chicco-cosmetica-tetinas-bebes
---

Lo natural está de moda. Cada vez más madres miramos las etiquetas de los productos que van a usar nuestros hijos para comprobar que no tengan esta o aquella sustancia que nos han dicho que es perjudicial. Estamos cambiando los hábitos, y huimos de los productos químicos para apostar por productos naturales. Y esto se extiende a todos los ámbitos: alimentación, tejidos para vestirnos, cosmética…

Las grandes marcas toman nota y se esfuerzan para darnos una versión de sus productos de puericultura lo más natural posible. 

[Chicco](http://www.chicco.es/) nos ha mostrado en su Showroom, que presentó hace poco a modo de “Babyshower”, sus nuevas líneas de productos en su versión más natural: una línea de cosmética para el bebé llamada “Natural sensation” y unas tetinas y chupetes con un nuevo sistema al que han llamado “Efecto mamá”. 

 ![](/tumblr_files/tumblr_inline_o9a0jen3IE1qfhdaz_540.jpg)

Nuestra colaboradora Blanca Crespo, asistió a la presentación para contaros estas novedades:

## Cosmética para bebés Natural Sensation

> La línea de cosmética: “Natural sensation” está inspirada al 100% en la piel del bebé que es mucho menos ácida que la nuestra y por ello tiene muchas más posibilidades de irritación. Por ello los ingredientes que contiene son materias primas naturales y están libres de parabenos, fenoxietanol, perfumes y otras sustancias como el SLS Y SLES.
> 
> Estas materias primas son vegetales y provienen, entre otros, del trigo y aguacate conocidos por sus propiedades calmantes e hidratantes.
> 
> Estos productos tratan de proteger la piel del bebé como lo hace la ”Vernix Caseosa” que es una capa que protege al niño durante los meses de embarazo compuesta en un 80% de agua, 10% de grasa y 10% de proteína. Ya que al nacer pierden esta capa de protección Chicco con sus productos quiere ayudarnos a que nuestro bebé siga hidratado y sobre todo protegido.

 ![](/tumblr_files/tumblr_inline_o9a0jx0fdo1qfhdaz_540.jpg)

> De todos los productos que nos mostraron me encantaron estos tres: 

- **Aceite de ducha:**  Como los bebés realmente no están sucios no es necesario utilizar jabón que, además, para puede ser incluso perjudicial porque arrastra además de suciedad, capas de la epidermis que pueden ocasionar irritaciones cutáneas. Por ello este aceite es ideal para el baño del bebé, ya que sólo elimina la posible suciedad superficial a la vez que hidrata la piel y deja una sensación confortable, tanto que querrás ducharte ¡tu también con el aceite! Lo probamos allí y la sensación fue espectacular.  

- **Crema de talco** : Esta crema tiene una textura muy ligera y la han lanzado para sustituir al talco de toda la vida, ya que al ser talco en formato líquido absorbe el sudor sin resecar. La crema nada más aplicarla se absorbe y deja sensación de frescor puro, ideal para usarla en verano para esos bebés achuchables que están llenos de repliegues por todas partes que, si bien son comestibles, los pobres pasan mucho calor y hay aliviarles . Esta crema no hace de aire acondicionado pero si ayuda a absorber sudor de esos repliegues.  

- **Pasta balsámica 4 en 1** : Esta crema para el culete y está hecha a base de zinc, pantenol y vitamina. No sólo ayuda a eliminar las irritaciones de la piel, sino que también las previene. Aplicando una capa muy fina después de cada cambio de pañal crearemos una barrera protectora que al mismo tiempo deja respirar la piel. Lo bueno que tiene también es que no tiene edad de utilización, es decir, al estar compuesta por ingredientes naturales es perfecta desde el primer pañal hasta el último.  

## Tetinas que pretenden un “Efecto Mamá”

De sobra sabéis que el mejor efecto mamá es el que produce la propia mamá, obviamente nada es comparable a la piel del pezón, pero siempre está bien tener alternativa para cuando ella no esté cerca, o se decida usar chupete o biberón.

 ![](/tumblr_files/tumblr_inline_o9a0koIu2k1qfhdaz_540.jpg)

> Con esta nueva gama de biberones y chupetes Chicco trata de que el contacto sea lo más similar posible al contacto materno. Están elaborados con siliconas completamente transparentes, incoloras, insípida y no se deforma. 

- **Biberones “Well Being”** : Diseñados para proteger al bebé de cólicos y reducir la irritabilidad. Las tetinas no tienen la típica forma ergonómica sino que su forma recuerda a la de una mamá pretendiendo que el bebé no note demasiada diferencia entre estar succionando del pecho de su madre y el biberón. La textura también es diferente ya que justo por la zona donde el bebé succiona es mucho más suave.  
- **Chupete Gommotto** : Es el clásico chupete de esta casa pero reinventado con un acabado extrasuave que no marca la carita del bebé mientras duerme. ¡Los chupetes los podéis encontrar en mil colores!¡ Todos ellos súper apetecibles!  
 ![](/tumblr_files/tumblr_inline_o9a0ryk8lP1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o9a0sbs6mw1qfhdaz_540.jpg)

Muchas gracias, Blanca por contarnos todos los detalles de las novedades de Chicco.

 ![](/tumblr_files/tumblr_inline_o9a0qptpGu1qfhdaz_540.jpg)

Me gusta que las marcas de puericultura tomen como referencia para innovar sus productos el cuerpo de la madre, ya que es evidente que es lo mejor que puede ofrecerse a un bebé.