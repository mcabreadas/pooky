---
layout: post
title: Mamá, ¿A que parezco mayor?
date: 2013-07-05T10:06:00+02:00
image: /tumblr_files/tumblr_inline_penq4w2GYB1qfhdaz_540.jpg
author: maria
tags:
  - 6-a-12
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/54660194367/preadolescentes
---
Princesita, ¿Cuándo fue el momento en que decidiste dejar de ser mi niñita pequeña?

Ayer, cuando te empeñaste en ponerte esos pendientes colgantes vi un cambio en tu carita. De repente, tus dientes nuevos asomando, tu pelo peinado por ti misma y a tu manera, y tu bolso de bandolera me revelaron que un cambio está surgiendo en ti. ¡Quieres ser mayor!!

¡Pero no tan pronto, hija! Si apenas han pasado 7 años desde que por primera vez te tuve en mis brazos y sentí la mayor alegría que puede sentir un ser humano, algo indescriptible que me cambió la vida para siempre.

Mi pequeña, mi bebé… ¿Por qué quieres ser mayor? ¿Por qué no lo ralentizas un poco? No corras, no hay prisa, quédate más tiempo donde estás. Eres muy chica aún. En la vida hay tiempo para todo, no te preocupes, que no se te hace tarde. Vas a vivirlo todo. Cada etapa. Y yo te voy a acompañar de la mano en cada paso, para que no te pierdas o te distraigas en los cruces del camino. Pero no corras, cariño, que aunque seas la hermana mayor, eres muy chiquitina todavía.

¿Es que te hemos hecho mayor nosotros al ponerte en cabeza de los otros dos fieras? ¿Te exigimos demasiada responsabilidad par tu edad? ¿Has tenido que crecer rápido para dejar tu hueco a Osito, y luego ayudarme a cuidar a Leoprdito? A veces nos olvidamos de que eres pequeña todavía… ¡Y luego me quejo de que te sientas mayor!

Pero no corras, hija, que no hay prisa, de verdad. Todo llega. Tendrás tu vida. La que tú escojas. Quiero que disfrutes cada tramo, no te saltes ninguno por llegar antes. Quiero que elijas en cada encrucijada con los valores y recursos que te damos en esta familia. Sé que te equivocarás, y yo lo veré venir, pero espero saber respetarte y esperar pacientemente a tu lado para recogerte en la caída. Sin reproches. Quiero quererte tanto que siempre lo sientas dentro de tus entrañas como un vínculo indestructible, incluso cuando las circunstancias nos separen o llegues a dudar… o a juzgarme…

Y sé que sufrirás desengaños, que te romperán el corazón y no habrá consuelo posible, que te sentirás triste y desolada, a veces desesperada. Entonces, acude a nosotros, acude a mí, por favor. Yo te reconfortaré e intentaré paliar tu dolor.

Y cuando sientas tanta ilusión que no tengas más remedio que saltar, te inunde la alegría, te sientas en las nubes o el éxito te haga sentir grande, compártelo conmigo también, que lo disfrutaré como si se tratara de mí misma… o más.

Ay, cariño… Sé que volarás, pero no tan deprisa, por favor, espera un poquito y quédate así con tu carita en la mía un ratito más…