---
date: '2018-05-02T09:00:15+02:00'
image: /tumblr_files/tumblr_p1oghyGOyo1s9a9yjo1_250.gif
layout: post
tags:
- mecabrea
- cosasmias
title: Enhorabuena, eres una buena madre
tumblr_url: https://madrescabreadas.com/post/173507252719/soy-buena-madre
---

Ayer ocurrió algo que me recordó que lo estoy haciendo bien. Que validó mi misión en esta vida como madre, y que me ha hecho reflexionar. No sé si alguna vez te ha pasado algo parecido o si, habiéndote ocurrido, le has dado la misma importancia que yo.

 ![](/tumblr_files/tumblr_p1oghyGOyo1s9a9yjo1_250.gif)

Al principio me quedé parada, sin saber reaccionar, pero cuando se repitió por segunda, incluso por tercera vez, me fui llena de orgullo a casa, y aún no se me ha borrado la sonrisa.

Es curioso cómo a las madres nos cuesta valorarnos y nuestra labor, y parece que necesitemos que alguien de fuera nos diga que lo estamos haciendo bien para creérnoslo.

Resulta que por primera vez dejamos solos a los tres fieras en un macro cumpleaños con un montón de padres y madres del cole, en un recinto cerrado y seguro (por eso lo hicimos, claro). 

 ![nina_parque_bolas_cumpleanos](/tumblr_files/tumblr_inline_p8097vMd2i1qfhdaz_540.jpg)

Y pasó lo que nunca pasa cuando me quedo a los cumples, que es casi siempre (la prueba está en que me hice experta y escribí estos [consejos para celebrar cumpleaños infantiles](/2014/02/04/tips-para-celebrar-cumplea%C3%B1os-de-los-ni%C3%B1os-mi.html)), que a uno le dio una lipotimia y el otro se raspó las piernas formando el consiguiente número. Vamos, que no fue nada de cuidado, pero el show lo dieron, ya me entiendes…

Total, que tuvimos que ir a por ellos antes y con tiempo.

Cómo suele pasar en estos casos, cuando entré al local del cumple me los encontré tan panchos disfrutando, como si no pasara nada (un clásico), me costó sacarlos de allí un mundo, y cuando nos despedimos, hasta 3 personas diferentes, una de ellas la responsable del local, me hicieron saber las joyas que tengo, cómo se cuidaron unos a otros, cómo se comportaron en todo momento, lo buenos que son, la educación que tienen…

Y no me lo decían por decir…no suelo ser presa del algo fácil (lo detecto al vuelo: ni me gusta recibirlo ni hacerlo), lo decían de verdad, y por separado (¿Ves? Me cuesta creer lo bueno a veces).

Y esto, señoras y señores es mérito mío y de su padre (parece que estoy en terapia de autoestima). Que sí, que sí! Que nos lo estamos currando y que vamos bien, que merece la pena las [renuncias profesionales y personales](/2017/01/24/maternidad-crianza-conciliacion.html) por ellos, que nuestra forma de hacer las cosas está bien, que parece que estamos acertando…

 ![](/tumblr_files/tumblr_onxm3cGlWT1s9a9yjo1_500.gif)

Te cuento esto no sólo por presumir de hijos (que a veces es necesario, aunque no soy de las que van enseñando fotos de ellos a la primera de cambio), sino porque me ha dado cierta rabia que me tengan que decir por fuera algo de lo que me debería haber dado cuenta yo misma y sentirme satisfecha.

A lo que voy, que lo estoy haciendo bien, tú también lo estás haciendo bien, las madres lo hacemos lo mejor que sabemos, damos lo mejor de nosotras, los queremos con locura y, también con nuestros fallos, los estamos cuidando y formando bien. Y estamos haciendo una gran labor en el mundo, aunque en silencio (aunque a veces gritamos), y no siempre se nos reconoce.

Qué no te pase como a mí, créetelo sin necesidad de que te lo diga alguien ajeo. Y si no, te lo digo yo: eres una buena madre, y estás haciendo una de las grandes proezas que puede hacer un ser humano, traer otro ser humano al mundo, cuidarlo, amarlo, formarlo, acompañarlo, ser su MADRE.

Enhorabuena!

Me quedo con esta imagen de la tarde de ayer en la que se ve a mi hijo pequeño despedirse de su mejor amigo con un abrazo y un beso espontáneo y lleno de cariño y amistad pura.

 ![ninos-abrazo](/tumblr_files/tumblr_inline_p83a4hLR4d1qfhdaz_540.gif)