---
layout: post
title: Los niños modelos tras la pasarela
date: 2016-02-21T17:00:19+01:00
image: /tumblr_files/tumblr_inline_o39yl6o9MJ1qfhdaz_540.jpg
author: .
tags:
  - moda
tumblr_url: https://madrescabreadas.com/post/139727604781/los-niños-modelos-tras-la-pasarela
---
Detrás del blog DESDETRES hay una mamá italiana que nos cuenta sus aventuras en España, con familia numerosa con cuatro niños, dos de los cuales mellizos, sus recetas italianas, la moda infantil, la lactancia, la crianza con apego y el bilingüismo.

 ![](/tumblr_files/tumblr_inline_o39yl6o9MJ1qfhdaz_540.jpg)

Conocí a Barbra en una pasarela de moda infantil, y me contó que sus 3 hijos mayores desfilaban en [FIMI](/2016/01/27/fimi-moda-infantil.html), y que la pequeña, de 3 años, ya apuntaba maneras.

Soy nueva en el mundo de la moda, y en ese momento sentí muchísima curiosidad por lo que hay detrás de cada evento de moda infantil, y cómo lo viven los verdaderos protagonistas, los niños y niñas que dan vida a las colecciones de ropa.

Siempre observo a los niños que pasan los modelos y los veo sonreír, pero me encantaría saber qué pasa por su cabecita, si les parece divertido, si a veces les resulta duro porque, no lo olvidemos, son niños adentrándose en un mundo de adultos.

Por eso tuve la osadía de pedirle a … una entrevista. Quería que me diera el punto de vista de una madre y, sobre todo, de los niños.

 ![](/tumblr_files/tumblr_inline_o39yl70Afj1qfhdaz_540.jpg)

Os dejo con la entrevista:

## ¿Por qué se iniciaron tus hijos en el mundo de la moda?

Se iniciaron de pura casualidad. El casting fue en un tienda muy cerca de mi casa, fui a comprar y me dieron un folleto. Así que fuimos. Yo no tenía blog aun ni conocía a nadie en el mundo de la moda.

El por qué tiene unas raíces más profundas, pero resumiendo, mis hijos eran y son, muy tímidos. Una llegaba a ser un problema por inseguridad y timidez,  así que nos lo tomamos como una extra escolar, como ir a un curso de teatro.

> **La razón fue enfrentarse a esta timidez y a una inseguridad debido a algunos acontecimientos que habían vivido, y para llenarse de experiencias que les diesen la autoestima que les faltaba.** 

Presenté a los tres y cogieron a los tres. ¡No sabía exactamente a lo que iba!

## ¿Te lo pidieron ellos?

No, ni sabían lo que era. Con 4 y 6 años aun guiaba yo sus actividades, y aún lo hago ahora en parte. 

Cuando creo que algo puede beneficiarles y gustarles les pido que prueben. Si después de haber probado no les gusta lo dejan y ya esta. En ese caso se lo propuse y dijeron que sí.

## ¿Les gusta desfilar?

Les encanta. Es una de estas cosas que esperan todo el año, y si quiero que hagan algo lo más fácil es decirles que no van a desfilar si no lo hacen. Al niño al principio le costaba más, se cansaba, pero ahora él también lo adora.

## ¿Ves que tengan que sacrificar cosas?

Los desfiles son una actividad muy puntual. Son 3 o 4 días 2 veces al año. Y se lo pasan muy bien. En los días de ensayos están como en un campamento de verano: juegan, de vez en cuando ensayan 10/15 minutos y vuelven a jugar, comen todos juntos, reencuentran amigos.

 ![](/tumblr_files/tumblr_inline_o39yl7EqCr1qfhdaz_540.jpg)

Desde fuera debe parecer que son horas y horas de ensayos. Pero no es así. Es verdad que pasamos allí unos días, pero no están todo el rato ensayando, lo hacen por grupos. Si tus hijos preparan una obra de teatro para los padres a final de año también ensayan, y si practican algún deporte o tocan un instrumento musical también tendrán que ir a entrenar o ensayar. Esto es lo mismo.

> **Mis hijos hacen muchas cosas, son muy deportistas, viajamos, hacemos mucha vida en familia.**  

Y de vez en cuando vamos a unos castings y hacemos unos desfiles o unas fotos. 

## ¿Cómo lo llevan cuando, por ejemplo no pueden ir a jugar por tener que ensayar?

Está claro que hay que saber medir, renunciar a algunas cosas y poner esto detrás de otras prioridades (el colegio, su deporte favorito, tiempo libre para el juego…)

> **Son los mismos niños que marcan los límites.** 

Te dicen: “no me apetece ir hoy a este casting”, yo me ahorro el viaje y empleamos ese tiempo libre haciendo algo divertido juntos.

Estas son cosas que se hacen porque a los niños les gusta, pero si no quieren no tiene sentido. 

> **Desde luego me gustaría subrayar que no es por dinero.**  

La mayoría de las cosas que hacen, algunas muy vistosas, no llegan ni a cubrirte los gastos de desplazamiento. 

[La Feria Internacional de Moda Infantil (FIMI)](http://www.fimi.es/) además se hace de manera del todo voluntaria, no se recibe compensación económica ni ningún regalo por parte de las marcas por las que desfilan.

Sólo alguna publicidad a veces llega a tener una buena compensación económica, y en este caso guardo el dinero en una hucha para hacer alguna actividad extra.

## ¿Son duros para ellos los ensayos?

En absoluto. Como te he dicho son unos días de juego, de amigos…

 ![](/tumblr_files/tumblr_inline_o39yl7ve9Y1qfhdaz_540.jpg)

Creo que desde fuera se tiene una visión muy distinta. Probablemente porque luego lo hacen tan bien que la gente cree que han estado repitiendo miles de veces.

A mí, por ejemplo, me sorprendió que a parte de las coreografías (por donde pasar, donde parar etc.) 

dejen a los niños absolutamente libres de hacerlo como saben y quieren, expresando su personalidad.

> **Nadie les dice como caminar, ni como parar para la foto.**

## ¿Tú como madre, cómo los convences si un día prefieren hacer otra cosa en vez de desfilar?

Si no quieren ir a FIMI, por ejemplo, directamente no van al casting. Y si los cogen yo le digo, sobre todo al pequeño, que tiene que decidir si quieren ir o no. 

Una vez que están allí nunca me pasó que dijesen que no querían. 

Pero si pasase supongo que hablaría con ellos haciéndoles ver que han dicho que querían ir y que está mal dejar las cosas a mitad. 

> **Aquí creo que el limite lo tienen que poner las madres, no se puede estar dispuesto a todo con tal de ver a tu hijo salir.** 

Yo por ejemplo pretendo estar en todo momento con mis hijos, que sepan donde estoy y puedan venir a verme si lo desean o, mejor, que me tengan a vista. Me parece fundamental. 

Es lo que me gusta de FIMI, las madres somos parte del juego, junto con ellos.

## ¿Ser modelos les quita mucho tiempo de estudiar o deberes, o lo compaginan bien?

A la hora de elegir si ir o no a un casting hay que valorar muchas cosas. Yo hago este trabajo previo, descarto lo que sea muy masivo y se que nos puede quitar mucho tiempo, decido lo que creo que no les aporta nada y lo que si. 

A veces me equivoco, claro, pero intento hacerlo lo mejor posible. Si el casting cae el día de un cumple o una extra escolar a la que no quieren faltar, directamente digo que no. Si han tenido uno la semana anterior, tampoco van. 

Intento buscar ocasiones para que haya selecciones por foto. Y si decido que el casting merece la pena se lo planteo. A veces dicen que sí y otras que no. Y yo lo respeto. Nunca les intento convencer.

## ¿Qué ventajas supone para tus hijos desfilar?

Para algunos de mis hijos ha supuesto un cambio muy grande. 

> **Han encontrado la autoestima que en el momento en el que empezaron y por razones varias, no tenían.** 

En el colegio siempre me han apoyado a seguir. Me dicen que les sirve mucho y les animan a contarlo.

 ![](/tumblr_files/tumblr_inline_o39yl8fz4J1qfhdaz_540.jpg)

Los niños necesitan autoestima, deben sentir que pueden, que si se esfuerzan lo hacen bien. Hay que buscar en qué son buenos y fomentárselo, ya sea en el deporte, en el colegio, en la música o en el ajedrez. 

\> ![](/tumblr_files/tumblr_inline_o39yl8rZ211qfhdaz_540.jpg)

No importa dónde pero es fundamental que sientan ser buenos en algo. A ellos les ha aportado mucho. A algunos más, a otros menos, pero les doy a todos la posibilidad de hacerlo.

## ¿Qué sientes cuando ves a tus hijos sobre la pasarela?

La primera vez que los vi fue una emoción muy fuerte. No me imaginaba que podían llegar a hacerlo tan bien, que se atreverían frente a tanta gente, fotógrafos, cámaras… 

 ![](/tumblr_files/tumblr_inline_o39yl8H9X71qfhdaz_540.jpg)

> **Sentí emoción y orgullo, y no podía parar de llorar.** 

Sobre todo los veía felices y sanos allí arriba, y me sentía orgullosa del trabajo hecho. Pasaron muchas cosas delante de mis ojos.

Está claro que ahora en mi doble rol de mama y de bloguera ya no disfruto igual. Me paso el tiempo haciendo fotos, a todos los niños, y a veces cuando salen los míos estoy haciendo fotos al anterior y me los pierdo.

## ¿Cómo los trata el personal de la organización de los desfiles? ?Suelen tener personal especializado en niños para tratar con ellos?

No se que formación tienen los componentes de [Happy kids Media](http://happykidsmedia.com/), que son los que gestionan los ensayos y las pasarelas. Lo cierto es que tienen experiencia para vender. Saben tratar con ellos, consiguen que estén siempre atentos cuando es necesario, sin saturarles, interesándoles, hablando como hay que hablarle a un niño, sacando de cada niño lo que puede dar, sin esperarse lo imposible. 

Los niños los adoran. Se lo pasan bien con ellos, se nota que les gusta estar en contacto con ellos, trabajan en esto. Son personas con mucha experiencia en entornos muy diferentes. Y me sorprende siempre cómo conocen a los niños. 

> **Sacan lo mejor de ellos. Y cuando digo lo mejor no quiere decir el mejor comportamiento o la mejor actuación, sino la mejor persona.**

No siempre los que trabajan con los niños son así. Una vez una persona, de una de las mayores empresas españolas, salió a recordar a las madres que estuviéramos alejadas de los niños, que esos niños en ese momento eran trabajadores….fue cuando me fui.

> Te agradezco esta pequeña entrevista porque me ha permitido pararme a pensar un momento sobre lo que están haciendo mis hijos, puesto que muchas veces me siento criticada por tenerles en este entorno. 
>
> Creo que no todo es bueno y hay que saber medir y ponerse muchos límites. No estoy dispuesta a todo y no se me va la vida por que mis hijos participen a una o otra cosa. Pero mientras se lo pasen bien, les aporte seguridad y desparpajo (que les vendrá bien en la vida)…no hay nada malo en sentirte feliz en ver a tus hijos en una buena foto, desenvolverse sin vergüenzas en una pasarela o aparecer de repente en una foto mientras compras en internet o en una publicidad a mitad de tu serie preferida.

Muchas gracias a Barbra por abrirme la puerta de este mundo tan desconocido por mucha gente.

Le agradezco la sinceridad y sensatez de sus respuestas, y creo que lo está enfocando de una manera inteligente y respetuosa con sus hijos que, por cierto, son estupendos.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.