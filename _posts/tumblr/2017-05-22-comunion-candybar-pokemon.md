---
layout: post
title: La segunda Primera Comunión
date: 2017-05-22T11:52:52+02:00
image: /tumblr_files/tumblr_inline_oqckpwZpSM1qfhdaz_540.png
author: .
tags:
  - 6-a-12
  - comunion
tumblr_url: https://madrescabreadas.com/post/160942777244/comunion-candybar-pokemon
---
Organizar Comunión del segundo hijo no es igual que [la primera vez](/2015/05/19/c%C3%B3mo-celebrar-la-comuni%C3%B3n-sin-arruinarse.html), eso todo el mundo lo sabe… porque la experiencia es un grado, te complicas menos para obtener similar resultado en la candybar Pokemon, y decoración, y te sientes más segura de que todo va a salir bien.

Pero te aseguro que el día de la Primera Comunión es igual de especial porque cada niño es un mundo, y las circunstancias de cada momento son diferentes.

 ![](/tumblr_files/tumblr_inline_oqckpwZpSM1qfhdaz_540.png)

## El comulgante

Mi hijo mediano, al que conociste como “Osito” porque ya son muchos años de blog y antes era como un osito, ahora es un tiarrón grande y fuerte, aunque sigue conservando la esencia de su nombre porque es cariñoso, tierno y tiene un corazón noble y bueno. Además, le sigue gustando que lo achuche, aunque yo le digo de broma que dentro de poco va a ser él quien me tome en brazos a mí.

[Los hermanos medianos](/2014/10/22/hermanos-familia-numerosa.html) son especiales, ya te lo dije en otro post, porque creo que desarrollan una personalidad con cierto carisma que logra atraer nuestra atención por sí misma sin el plus que otorga ser el mayor o el pequeño.

Mi niño nos hechizó a todos en su Primera Comunión con su sonrisa, sus ojos, su saber estar y la alegría e ilusión que nos contagió.

 ![](/tumblr_files/tumblr_inline_oqckhnYY2w1qfhdaz_540.png)

## Una celebración familiar

Agradeció y disfrutó desde el corazón todo lo que hicimos por él su familia y amigos para hacer de ese día el más especial dentro de la sencillez con la que nos gusta hacer las cosas para vivirlas de la forma más auténtica.

Elegimos un restaurante familiar donde se come de maravilla en pleno corazón de la huerta, con un gran patio lleno de flores y plantas vallado para que los más pequeños no corrieran peligro, y pasamos un día al aire libre en familia y con los amigos más íntimos.

## Decoración y candybar Pokemon

Toda la decoración y la mesa dulce la hicimos entre Princesita y yo poco a poco a ratitos los fines de semana. 

 ![](/tumblr_files/tumblr_inline_oqckjdRJFY1qfhdaz_540.png)

El photocall nos costó sudor y lágrimas, pero al final lo destrozaron en dos minutos logramos.

 ![](/tumblr_files/tumblr_inline_oqckooUlIr1qfhdaz_540.png)

## Tarta de Comunión Pokemon y galletas caseras

Las galletas de fondant decoradas para los invitados y la tarta también fue casera. Los recordatorios los hizo [Carmensr](https://carmensr.com/) con un retrato del comulgante precioso.

 ![](/tumblr_files/tumblr_inline_oqcl6cagl21qfhdaz_540.png)

El libro de firmas también fue hecho a mano con todo el cariño, regalo de la madrina. Yo aún conservo el mío, y de vez en cuando lo releo y recuerdo a las personas que me acompañaron ese día, pienso en sus palabras que muchos de ellos ya no me podrán decir, y me alegro infinito de guardar ese trozo de memoria en un cajón que me recuerda cosas que a veces se me olvidan.

 ![](/tumblr_files/tumblr_inline_oqckr5zzTF1qfhdaz_540.png)

Vivimos un día para el recuerdo, los niños disfrutaron como nunca con sus primos y amigos. 

 ![](/tumblr_files/tumblr_inline_oqckl2eO3g1qfhdaz_540.png)

No necesitaron monitores ni hinchables, y Princesita hizo de pintacaras, porque no olvidemos que ellos saben divertirse solos, lo han hecho toda la vida.

 ![](/tumblr_files/tumblr_inline_oqcksv8eTs1qfhdaz_540.png)

Gracias a todos los que nos acompañásteis en este gran día que estoy segura de que quedará grabado en el recuerdo de mi hijo para siempre.

(Dentro de 4 años, la siguiente…)

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).