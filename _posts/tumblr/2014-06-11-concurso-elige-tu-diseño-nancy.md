---
date: '2014-06-11T15:45:00+02:00'
image: /tumblr_files/tumblr_inline_n70ay4udpU1qfhdaz.jpg
layout: post
tags:
- crianza
- premios
- participoen
- colaboraciones
- ad
- familia
- juguetes
- moda
title: Concurso elige tu diseño Nancy
tumblr_url: https://madrescabreadas.com/post/88471994209/concurso-elige-tu-diseño-nancy
---

Me encantan las iniciativas originales, divertidas y, que además, tienen un fin solidario. Por eso me he decidido a colaborar con [Nancy](http://www.nancyfamosa.es/es/?utm_source=Madresfera.Post&utm_medium=Madresfera.Post&utm_campaign=Nancy.KMM) y la tienda on line Kiddy Mini Model (KMM) en esta acción, que pretende elegir un dibujo de entre los tres que propone una niña de 8 años diseñadora llamada Eva, para confeccionar un vestido inspirado en la moda de la muñeca Nancy.

La relación de Nancy con la moda viene de lejos, incluso la firma concede 3 becas para estudiantes de moda a través de un concurso llamado “Nancy con la moda”

¿A que son preciosos? A Princesita le encanta el del perrito.

Eva es una artista precoz, que dedica su tiempo de ocio a diseñar ropa porque le divierte un montón, y sus papás han creado la tienda on line KMM para apoyarla. Ha tenido la suerte de diseñar tres modelos inspirados en la moda de la muñeca Nancy que, por cierto, siempre va a la última.

¿Queréis ganar uno de los 10 kits con dos vestidos cada uno, uno de niña, y otro para su Nancy? 

**¿Os apetece participar? Podéis hacerlo de dos formas:**

**1-Primera forma** :

[![image](/tumblr_files/tumblr_inline_n70ay4udpU1qfhdaz.jpg)](https://www.facebook.com/nancyfamosa.es?sk=app_256593927709045&app_data=t-634754359)

Tenéis que [haceros fan de la página de Facebook](https://www.facebook.com/nancyfamosa.es) de Nancy y votar y  hasta el 29 de junio vuestro dibujo favorito. El que reciba más votos será el elegido para la prenda que se va a confeccionar.

Las votantes entrarán automáticamente en un sorteo de 10 kits con dos vestidos cada uno, uno de niña, y otro para su Nancy.

[![image](/tumblr_files/tumblr_inline_n705govr9S1qfhdaz.jpg)](https://www.facebook.com/nancyfamosa.es)

**2-Segunda forma** :

Además, si **me dejáis un comentario** explicando el vestido que más os ha gustado, y este blog es seleccionado por Nancy por su gran participación en el concurso, entraréis en un segundo sorteo siendo el premio otro kit de vestidos para niña y muñeca.

**Proyecto con un fin solidario**

Pero lo que más me gusta es el fin solidario de este proyecto, ya que en octubre se van a subastar 70 kits (nada menos) con motivo del III día internacional de la niña, a favor de la ONG “Plan Internacional”, y su proyecto “Por ser niñas”.

La subasta se llevará a cabo en la [tienda on-line de Famosa](http://famosatoystore.com/), y os espero por allí porque la causa lo merece.

¿Te animas a participar? ¿con qué diseño te quedarías?

_NOTA: Este post es patrocinado._