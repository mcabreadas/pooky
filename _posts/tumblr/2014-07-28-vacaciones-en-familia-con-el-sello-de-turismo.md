---
date: '2014-07-28T10:00:00+02:00'
image: /tumblr_files/tumblr_inline_pbyhkqvSQH1qfhdaz_540.jpg
layout: post
tags:
- crianza
- vacaciones
- planninos
- familia
title: Vacaciones en familia con el Sello de Turismo Familiar
tumblr_url: https://madrescabreadas.com/post/93095314588/vacaciones-en-familia-con-el-sello-de-turismo
---

Las familias numerosas no lo tenemos fácil a la hora de irnos de vacaciones, ya que el salto de dos a tres hijos supone un incremento en el precio proporcionalmente mayor de lo que lo que supone realmente que una personita más se una a las vacaciones.

Si normalmente es fácil encontrar buenas ofertas para dos adultos y dos niños por la facilidad de encontrar habitaciones para cuatro personas, en cuanto intentas añadir uno más ya te ves obligado a coger dos habitaciones, lo que dobla el precio inicial y hace, en muchas ocasiones inviable unas vacaciones de este tipo para grandes familias

Por eso me parece una buenísima idea, además de necesaria, la iniciativa la de la [Federación Española de Familias Numerosas](http://www.familiasnumerosas.org/) de crear un Sello de Turismo Familiar para distinguir y promocionar los establecimientos y destinos turísticos que apuesten por nosotros.

 ![image](/tumblr_files/tumblr_inline_pbyhkqvSQH1qfhdaz_540.jpg)

La Federación avala con este distintivo hoteles, centros culturales, restaurantes, emplazamientos turísticos, etc., que tengan instalaciones, oferta de actividades o entorno pensados para las familias.

El objetivo del **[Sello de Turismo Familiar](http://www.familiasnumerosas.org/turismo-familiar/) **es ofrecernos un servicio al localizar e identificar los mejores lugares para disfrutar del ocio y el tiempo libre con niños. De esta forma, cuando veamos el distintivo de “Turismo Familiar” sabremos que estamos en un lugar adecuado y orientado especialmente para nosotros.

A la vez, la FEFN pretende fomentar una mayor oferta turística para familias al promover la especialización y la competencia entre empresas del sector dedicadas a las familias; se favorecer una mayor oferta de turismo familiar y una mejora de la relación calidad-precio.

Aquí podéis consultar las primeras empresas con este distintivo de Turismo Familiar por si queréis hacer una escapadita este verano.

La que más me ha llamado la atención es ésta que organiza el Camino de Santiago en familia. 

[![image](/tumblr_files/tumblr_inline_pbyhkqsMkM1qfhdaz_540.jpg)](http://www.elcaminoenfamilia.es/)

Pero hay destinos de todo tipo, playa, monte, culturales, aventuras… 

 ![image](/tumblr_files/tumblr_inline_pbyhkrRzGB1qfhdaz_540.jpg)

Con cuál te quedas? 

_Fotos gracias a: _

_[Familias Numerosas](http://www.familiasnumerosas.org/)_

_[http://El camino en familia](http://www.elcaminoenfamilia.es/)_