---
layout: post
title: Las sombras de la lactancia
date: 2013-04-27T07:18:00+02:00
image: /tumblr_files/tumblr_mlwee7cAdO1qgfaqto1_400.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - lactancia
  - cosasmias
  - bebes
tumblr_url: https://madrescabreadas.com/post/48986579257/las-sombras-de-la-lactancia-los-niños-se-encelan
---
Los niños se encelan con la teta, cogen vicio, se despiertan a menudo por la noche porque quieren mamar por capricho, cuando tenga dientes te va a morder, cuanto más mayor, más difícil quitarle la teta, la lactancia exclusiva es para las suecas, los cereales no ligan bien con la leche materna, favorece el reflujo al no ser un alimento espeso, estás en las últimas y sin dormir porque decidiste irresponsablemente no darle ni un sólo biberón, y ahora nadie te puede ayudar por las noches porque rechaza la tetina, no puedes salir ni una noche, no puedes dejarlo con nadie a dormir, le has creado una dependencia, te has convertido en una loca de la teta, la LME es incompatible con un trabajo, es lo mejor para el bebé, pero no para la madre, el niño ya necesita otro tipo de alimento que le llene más… 

![madre en suelo amamantando](/images/uploads/depositphotos_176245178_xl.jpg)

Que no alimenta no me lo pueden decir porque mi Leopardito rebosa salud y está prieto, lustroso y rosado como un lechoncete, pero si hubiera perdido un mg de peso en alguna ocasión, también me hubieran martirizado con eso.

## ¿Por qué nos acechan las sombrasde la lactancia?

En los momentos de sombra, cuando todo parece hundirse, cuando te queman los ojos por dentro, cuando no te quedan lágrimas, cuando la soledad te asola, cuando el cansancio es una losa… las sombras de la lactancia se hacen fuertes, y te hacen dudar, incluso arrepentirte, te derrotan y empiezas a creer en fantasmas que sobrevuelan tu cabeza, te atrapan y te poseen, y todo se derrumba a tu alrededor, todo en lo que creías se tambalea y deja de tener sentido.

Entonces un gemidito te saca de tu pozo, un gemidito que no entiende de trabajos, ni de vida social, ni de horarios ni prisas, ni de reflujo ni cereales, ni de vicios ni de suecas, ni te tetinas ni de biberones, que se engancha a ti como a la vida y te mira con esos ojos vivarachos de almendra que se clavan en los tuyos en una mirada de arrobo entre agradecida y admirada de que sólo tú seas capaz de alimentarlo, saciar su sed, consolarlo, protegerlo, aliviarlo, reconfortarlo… AMARLO de una forma tan instintiva, natural y pura.

![bebe mamando con mano](/images/uploads/depositphotos_12611612_xl.jpg)

Entonces toma lo que necesita, dormita, se recrea, es feliz, por fin se abandona en tus brazos y se duerme, confía en ti todo su cuerpo y su alma, nada malo puede pasarle así.

Escuchas su respiración relajada, hueles su aroma divino, notas su calorcito, sientes su piel suave y nueva. La paz te invade.

## ¿Ha merecido la pena la lactancia materna exclusiva?

Yo no sé si tendrán razón o no las sombras de la lactancia, lo único que sé es que no aportan nada, sólo miedo a dejarnos llevar por lo que somos: mujeres, madres, ríos de vida y de alimento.

¿Por qué negarlo? 

¿Por qué negarnos a nosotras mismas? 

¿Por qué negarle lo mejor a nuestros cachorros? 

Y quien decida otra cosa por favor, que no sea por miedo, que no sea por las sombras de la lactancia.

*Foto de portada gracias a mis amigas de la tribu CRYA, en un momento d elos mas delicados de mi vida.*

*Fotos el cuerpo del post gracias a* [sp.depositphotos](https://sp.depositphotos.com/)