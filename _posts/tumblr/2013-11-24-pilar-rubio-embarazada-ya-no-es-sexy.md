---
layout: post
title: Pilar Rubio embarazada ya no es sexy
date: 2013-11-24T18:30:00+01:00
image: /tumblr_files/tumblr_inline_p98q77glDN1qfhdaz_540.jpg
author: maria
tags:
  - embarazo
tumblr_url: https://madrescabreadas.com/post/67969325080/pilar-rubio-embarazada-ya-no-es-sexy
---
Estoy muy cabreada con en enfoque que ha dado El Mundo al tema del embarazo de Pilar Rubio en[ el artículo](http://www.elmundo.es/loc/2013/11/16/52866cbf61fd3d26558b457a.html):  **“El cuerpo diez de Pilar Rubio se toma una excedencia por su embarazo. Decálogo de los cambios que sufrirá el cuerpo de la presentadora durante la dulce espera.”**

Primero empieza suponiendo que ya sólo por estar embarazada Pilar Rubio deja automáticamente de ser sexy, y que otra mujer ocupará su lugar como la más sexy de España. Luego se recrea en todos y cada uno de los cambios “negativos” que experimentan los cuerpos de las mujeres embarazadas. Vamos, que lee esto una adolescente, y va corriendo a esterilizarse. Ni que quedarse embarazada fuera coger la lepra. Por Dios. Además, está comprobado que muchas mujeres embarazadas se sientes desprendes sensualidad por todos sus poros, las redondeces y los cambios hormonales les hacen sentirse más sexis y atractivas, incluso más activas sexualmente, lo que las hace irresistibles.

Parece como si la autora se alegrara de que la famosa presentadora pierda las medidas standard de los diseñadores de moda y deje de ser “perfecta”. Pero se equivoca, porque ahora será más perfecta todavía porque tendrá toda la perfección que la naturaleza exige para que una mujer sea portadora de vida.

 ![image](/tumblr_files/tumblr_inline_p98q77glDN1qfhdaz_540.jpg)

*Foto gracias a http://www.elmundo.es*

Mira, Pilar, no tienes por qué preocuparte. Te explico por qué:

**1** - Que “ **AUMENTARÁS DE PESO”?**. Pues hombre, lógicamente. El bebé irá engordando, la placenta irá aumentando de tamaño, cada vez habrá más líquido amniótico, y … oH! Horror! Acumularás algo de grasa y agua para alimentar al bebé cuando nazca si decides dar el pecho!! Claro que aumentarás de peso, lo raro sería que no lo hicieras. Pero no pasa nada porque en el parto pierdes casi todo, y luego, si decides dar lactancia materna, te quedas nueva en cuestión de semanas.

**2** -Que tendrás que decir “ **ADIÓS ABDOMINALES”** , mujer, con un hasta luego creo que vale, porque si una es deportista, seguramente continuará siéndolo durante y después del embarazo. tampoco hay que ponerse trágicos.

**3** -Que deberás decir  **"CIAO CIAO CINTURA"** , tampoco es eso, con decir, nos vemos luego, es más que suficiente porque, es verdad que conforme va creciendo la barriga la cintura se va desdibujando, pero deja lugar a otra bonita curva que, además a ti te sienta genial.

**4** -Que experimentará un “ **ENSANCHAMIENTO DE CADERAS”**? Pues tú verás, si tiene que crecer un bebé dentro de tu cuerpo, y luego salir, si no lo experimentaras sería terrible. Pero no sufras, que luego las cosas vuelven a su sitio. Y por la grasa que éstas acumulan, lo dicho, el bebé se la toma toda si decides dar Lactancia materna. Además, seguro que las caderas te hacen estar más sexy aún.

**5** -Respecto a la “ **FLACCIDEZ Y CELULITIS”,**  Pilar, no te preocupes, que si decides dar el pecho, la liposucción te sale gratis porque el bebé se lleva toda la grasa acumulada en tu cuerpo… Sí, sí la aque tenías antes de quedar embarazada también. Te lo digo por propia experiencia.

En cuanto a la flaccidez, hija mía, con el ejercicio que seguro que haces, y las cremas que hay en el mercado, no creo que haga mucha mella en ti.

**6** -Que te saldrán “ **ESTRÍAS”?**. Bueno… la piel se estira, sí, pero también está preparada para ello, y la verdad es que las mujeres la tenemos bastante elástica. A mí me fue muy bien la Nivea de la caja azul de toda la vida, Pilar. Y no me salió ni una en la barriga. Oye, y si sale alguna, pues nada, no se acaba el mundo, cuando la mires recordarás orgullosa como una herida de guerra, como cuando alguien hace una gran cosa en la vida y le queda una marca.

**7** -“ **VARICES”?**. Pues tampoco tienen por qué salir si llevas una vida activa, cuidas tu circulación y no engordas demasiado. Yo, la verdad no tengo ninguna nueva que no tuviera al margen de mis tres embarazos. Pero vamos, si te sale alguna y te preocupa, hay tratamientos que las quitan sin problemas. No se hunde el mundo.

**8** -Que tu  **PECHO no volverá a ser el mismo?**. “Si bien el embarazo es un momento ideal para lucir un bonito escote – aumenta considerablemente su volumen-, el pecho es una de las partes del cuerpo que más se resiente tras este periodo. Después de la maternidad los pechos quedan flácidos, ya que el estiramiento de la piel hace que se rompan las fibras colágenas que los sostienen.”

Pues sí, pero te digo una cosa, si decides dar lactancia materna lucirás escote el tiempo que tú quieras, como si quieres reenganchar con un segundo embarazo. Además, las mujeres tenemos una fuente de alimento en ellos, somos capaces de amamantar a nuestros hijos, casi que hacemos magia. Así que, si luego se quedan un poco flácidos, pues habrá valido la pena porque la lactancia materna es una de las experiencias más maravillosas que una mujer puede vivir, y que un bebé puede gozar. Hacemos una flexiones, nos damos unas cremas y pa alante!

**9** -Que se te quedará el **CABELLO** más seco? Más bien la sequedad se daría en el postparto, pero es totalmente recuperable con buenas mascarillas, y una alimentación saludable. Lo que más he notado yo es la caída, por eso te recomiendo, Pilar que tomes vitaminas durante el embarazo y la lactancia. Es cierto que se pierde cabello, pero enseguida empieza a nacer nuevo, ya lo verás. Además, con la mata de pelo que tienes, hija, seguro que ese no va a ser tu problema.

**10-ROSTRO**. “Las hormonas pueden propiciar la aparición de manchas oscuras y granos en la zona de la frente, el cuello y las mejillas.” Nada que no se pueda prevenir con un buen protector solar y evitando la exposición directa prolongada. Yo te puedo decir que soy de piel blanca y con muchos lunares, y que las manchas que tengo en la piel, ninguna me salió consecuencia de ningún embarazo.

 ![image](/tumblr_files/tumblr_inline_p98q78faHc1qfhdaz_540.jpg)

*Foto gracias a miembarazosano.com*

Precisamente las embarazadas os veis resplandecientes, os brillan los ojos e irradiáis hermosura por toda vuestra piel. Las hormonas? Quizá, pero la felicidad de ser madre, y de saber que ya nunca vais a estar solas hace que brilléis por donde vais.

Así que , Pilar, no te preocupes ni un ápice por ninguno de estos cambios que va a experimentar tu cuerpo, porque a la redactora se le han olvidado los más importantes. Esos que no se ven. Esos que te convertirán en una mujer nueva. En una mujer más completa. En una mujer plena. Disfrútalos todos.

Y a ti? Te preocupan los cambios físicos que se producen en el embarazo?

Spoiler:

Pilar Rubio tras dar a luz siguio sindo sexy, es mas, escribio un [libro sobre cuidados en el embarazo y postparto](https://madrescabreadas.com/2016/06/08/libro-pilar-rubio/), que no te lo puedes perder.