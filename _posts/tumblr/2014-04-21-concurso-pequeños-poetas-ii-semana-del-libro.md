---
date: '2014-04-21T08:00:00+02:00'
image: /tumblr_files/tumblr_inline_mm0dneS9Ce1qz4rgp.jpg
layout: post
tags:
- crianza
- premios
- libros
- planninos
- educacion
- familia
title: Concurso “Pequeños poetas II". Semana del libro.
tumblr_url: https://madrescabreadas.com/post/83387119183/concurso-pequeños-poetas-ii-semana-del-libro
---

Arranca la semana del libro, y me encanta celebrarla con vosotros organizando otro año más el concurso “Pequeños Poetas”.

La celebración del  **día del libro**  se remonta a principios de siglo, llevándose a cabo  **actividades literarias**  en toda España en colegios, bibliotecas, centros culturales… El día 23 de Abril se celebra en todo el mundo, el día del libro internacional.

El origen del día del libro se remonta a 1930. El 23 de abril de 1616 fallecían Cervantes y Shakespeare. Por este motivo, esta fecha tan simbólica para la literatura universal fue la escogida por la Conferencia General de la UNESCO para rendir un homenaje mundial al libro y sus autores, y alentar a todos, en particular a los más jóvenes, a descubrir el placer de la lectura. Pero sólo si nuestros hijos nos ven leer ellos tendrán el gusto de hacerlo también.

El concurso consiste en que vuestros hijos escriban un pequeño poema.

La poesía desarrollará su sensibilidad y hará que se paren a observar el mundo y vean la belleza que hay en él. 

Os sorprenderá la frescura y espontaneidad de los niños. Quizá al principio esté temerosos, pero si les ponéis algunos ejemplos sencillos veréis qué cosas tan lindas se les ocurren. Y si no, mirad la ganadora del año pasado, con su poema “Se puede”: 

“Se puede ser artista

**o equilibrista,**

**se puede conducir un camión**

**o aprender la lección.**

**Se puede viajar en avión**

**o en tu imaginación,**

**pero no se pueden olvidar,**

**los peces en el mar.”**

 ![image](/tumblr_files/tumblr_inline_mm0dneS9Ce1qz4rgp.jpg)

**PARA CONCURSAR**

Podéis dejar vuestro poema en un comentario a este post hasta el domingo 4 de mayo.

Entre todos los participantes sortearé el libro en formato papel de [Doña María Felisa Martínez Talavera](/post/4062118966/el-sueno-de-una-maestra) y sus alumnos, ”Grandes Poesías de Pequeños Poetas” con una dedicatoria personalizada de la autora.

Pero si no podéis esperar a ganar, el libro también se puede descargar en la web 1 libro = 1 Euro, de Save the Children.

[![image](/tumblr_files/tumblr_inline_mlsz04BelW1qz4rgp.jpg)](http://dl.dropboxusercontent.com/u/1942174/grandes_poes%C3%ADas_de_peque%C3%B1os_poetas.pdf)

Se trata del trabajo de clase realizado a lo largo de un curso por niños de 8 y 9 años de edad con su maestra, y su primera aproximación al mundo de la poesía. Son poemas llenos de espontaneidad y frescura en sus rimas poco ortodoxas y la sencillez y originalidad de los niños.

[![image](/tumblr_files/tumblr_inline_mlsz41tJy11qz4rgp.jpg)](http://dl.dropboxusercontent.com/u/1942174/grandes_poes%C3%ADas_de_peque%C3%B1os_poetas.pdf)

¿No me digas que no es una oportunidad para que tus hijos descubran la poesía?

¡Vamos! ¡Ponlos a escribir y participa!