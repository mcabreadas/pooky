---
date: '2014-12-30T17:22:00+01:00'
image: /tumblr_files/tumblr_inline_nhv9wqyg561qfhdaz.png
layout: post
tags:
- ad
- revistadeprensa
- colaboraciones
title: 'Blogs de papás y mamás: la primera Navidad, cuentos, catarros y viajes'
tumblr_url: https://madrescabreadas.com/post/106617329364/blogs-de-papás-y-mamás-la-primera-navidad
---

[Blogs de papás y mamás: la primera Navidad, cuentos, catarros y viajes](http://www.bebesymas.com/bebes-y-mas/blogs-de-papas-y-mamas-la-primera-navidad-cuentos-catarros-y-viajes)  

> Comenzamos como cada semana con el resumen semanal a los blogs de papás y mamás y abrimos esta vez con [Madres Cabreadas](/2014/12/23/la-primera-navidad.html) que aunque su nombre…

![](/tumblr_files/tumblr_inline_nhv9wqyg561qfhdaz.png)

[Bebés y Más](http://www.bebesymas.com/bebes-y-mas/blogs-de-papas-y-mamas-la-primera-navidad-cuentos-catarros-y-viajes), revista on-line sobre embarazo, infancia y m/paternidad, hace mención del post “[La Primera Navidad](/2014/12/23/la-primera-navidad.html)” en su recopilatorio de blogs de mamás y papás de esta semana.

[![](/tumblr_files/tumblr_inline_nhel404J471qfhdaz.jpg)](http://www.bebesymas.com/bebes-y-mas/blogs-de-papas-y-mamas-la-primera-navidad-cuentos-catarros-y-viajes)

Muchas gracias a [@PapaLobox](http://historiasdepapalobo.blogspot.com.es/) por tenerme en cuenta!