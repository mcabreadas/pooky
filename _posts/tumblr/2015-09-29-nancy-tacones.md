---
date: '2015-09-29T10:00:22+02:00'
image: /tumblr_files/tumblr_inline_o39zc7SYW91qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- ad
- recomendaciones
- familia
- juguetes
- moda
title: Nancy también juega con tacones
tumblr_url: https://madrescabreadas.com/post/130121169239/nancy-tacones
---

Dime una cosa, ¿de pequeña jugabas a ponerte los tacones de tu madre?

Creo que no hay niña que no sueñe con ser mayor para poder ponerse tacones. Yo recuerdo el armario zapatero blanco que teníamos en la galería, y cómo me colaba a escondidas para probarme todos los zapatos de mi madre. ¡Me encantaba jugar a ser mayor!

 ![](/tumblr_files/tumblr_inline_o39zc7SYW91qfhdaz_540.jpg)

Ahora veo que mi hija hace lo mismo y no puedo evitar enternecerme, por eso me ha encantado la nueva Nancy que ha sacado Famosa, con rodillas, tobillo y empeine articulados para que pueda ponerse tacones.

 ![](/tumblr_files/tumblr_inline_o39zc8DgZS1qfhdaz_540.jpg)

Le encanta vestirse igual que su Nancy, jugar a combinarse la ropa como ella. Y es que las Nancys de ahora se han modernizado y se han vuelto super fashion porque se le pueden poner cantidad de conjuntos con diseños muy actuales. 

 ![](/tumblr_files/tumblr_inline_o39zc8zRpt1qfhdaz_540.jpg)

Se les pueden combinar diferentes shorts, camisetas, chaquetas, sudaderas con dos tipos diferentes de tacones, los sneakers y los stilettos, para crear looks más informales, o más elegantes, según prefieras.

 ![](/tumblr_files/tumblr_inline_o39zc8WZxn1qfhdaz_540.jpg)

Además Nancy tiene su propia [App Look my look](http://www.nancyfamosa.es/look-my-look/) para jugar a ser diseñadora de moda. Cada pack de ropa Look my look contiene un código para canjear por nuevos contenidos de la App.

¿A que dan ganas de ponerse a jugar?

_NOTA: Post no patrocinado._

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.