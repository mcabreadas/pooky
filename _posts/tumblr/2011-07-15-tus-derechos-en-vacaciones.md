---
date: '2011-07-15T13:23:00+02:00'
image: /tumblr_files/tumblr_lodi4bdZHF1qfhdaz.jpg
layout: post
tags:
- mecabrea
- derechos
- vacaciones
- planninos
title: Tus derechos en vacaciones
tumblr_url: https://madrescabreadas.com/post/7648496933/tus-derechos-en-vacaciones
---

Como lo prometido es deuda, y si algo no quiero es tener ninguna con vosotros, ahí va el post con contenido jurídico que os pueda ser de interés. Os agradezco las sugerencias que me habéis dado, pero al final he hecho lo que he querido porque creo que son cosas útiles que necesitáis saber para vuestras vacaciones.

He recopilado una serie de consejos sobre diversas materias. Os advierto de que al final os voy a hacer un examen a ver si lo habéis aprendido todo bien, así que aplicaos.

Para empezar,como la mayoría nos vamos próximamente de vacaciones, incluso algún@s ya las estáis disfrutando, el “abc” de los alquileres de verano nos lo da CincoDías.com vía @kontsumo con una Guía para evitar timos al alquilar por Internet [http://bit.ly/nBsQe4.](http://bit.ly/nBsQe4.)

![image](/tumblr_files/tumblr_lodi4bdZHF1qfhdaz.jpg)

![image](/images/posts/pp.jpg)

Foto de [/tumblr\_files/dejadas-6.jpg](/tumblr_files/dejadas-6.jpg)

Para los que, en lugar de alquilar un apartamento se decanten por hacer un viajecito @sanidadgob nos dice nuestros derechos y garantías a la hora de contratar un viaje combinado [http://j.mp/n2JVW1.](http://j.mp/n2JVW1.)

Pero si lo que queréis es organizarlo vosotros y reservar el hotel por internet, tened en cuenta estos 10 consejos [http://bit.ly/rlQdOJ](http://bit.ly/rlQdOJ) de [www.ocu.org](http://www.ocu.org).

Si, aún así tenéis algún problema “No clames, reclama”, nos dice @sanidadgob con una guía para reclamaciones de los consumidores [http://t.co/Fbkhtmd.](http://t.co/Fbkhtmd.)

 Ahora llegó la hora del examen a ver si conocéis vuestros derechos. Podéis hacer este test gentileza de [www.ocu.org](http://www.ocu.org) vía @consumidores: [http://bit.ly/njutAL.](http://bit.ly/njutAL.)[www.ocu.org](http://www.ocu.org) vía @consumidores: [http://bit.ly/njutAL.](http://bit.ly/njutAL.)

 Espero que hayáis aprobado y que paséis unas felices vacaciones!