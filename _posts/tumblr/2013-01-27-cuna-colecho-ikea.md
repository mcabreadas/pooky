---
layout: post
title: Convierte una cuna de Ikea en cuna de colecho
date: 2013-01-27T11:06:00+01:00
image: /tumblr_files/tumblr_inline_p98q4988vN1qfhdaz_540.jpg
author: maria
tags:
  - crianza
  - lactancia
  - bebes
  - puericultura
tumblr_url: https://madrescabreadas.com/post/41598512975/cuna-colecho-ikea
---
Desde la [primera ecografia](https://madrescabreadas.com/2021/05/25/ecografia-4-semanas-no-se-ve-nada/) de mi tercer hijo tuve claro que quería hacer algunos cambios en el [dormitorio de matrimonio](https://amzn.to/45ZrXNI) para ganar espacio con un canape para amacenar ropa de hogar, y adaptarle una cuna de colecho, pero las minicunas de colecho o cunas de colecho prefabricadas se salian de nuestro presupuesto.

Como no existe una cuna de cohecho de IKEA ya fabricada, preferimos transformar la [cuna GULLIVER de Ikea](https://c.mtpc.se/tags/link/3625768) en una estupenda cuna de colecho donde nuestro bebé durmiera a pierna suelta a mi lado, favoreciendo la lactancia materna por la noche y, lo que es mejor, ¡mi sueño!

![cuna de ikea para fabricar cuna de colecho](/images/uploads/gulliver-cuna-blanco__0637927_pe698612_s5.jpg.avif)

## ¿Qué es la cuna de colecho?

Las cunas de colecho o minicunas de colecho son cunas normales. Algunas son más pequeñas tipo minicuna, pero con la característica de que uno de sus lados se desliza hacia abajo o simplemente no existe. Así puede permanecer pegado a la cama de los padres para que el bebé esté lo más próximo a la madre de forma segura mientras ambos duermen, favoreciendo así la lactancia materna y el descanso.

[![](/images/uploads/chicco-next2me.jpg)](https://www.amazon.es/gp/product/B00M434IJK/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B00M434IJK&linkId=9eb9c640b743504089ca000f321e8d5e)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B00M434IJK/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B00M434IJK&linkId=9eb9c640b743504089ca000f321e8d5e)

## ¿Cuánto mide una cuna de colecho?

Este dato es importante antes de lanzarnos a fabricar o comprar una cuna de colecho, ya que debemos asegurarnos de que quepa en nuestro dormitorio.

Pero el factor principal a tener en cuenta para elegir la minicuna de colecho es el tamaño de nuestro bebé. Para los más pequeños, las hay desde 45 x 25, pero la medida estándar es de 70 x 50.

En nuestro caso, como adaptamos una cuna normal, fue más grande.

## ¿Cuál es el mejor colecho?

La mejor cuna de colecho para tu bebé es la que mejor se adapte a vuestras necesidades por tamaño, facilidad de manejo y, sobre todo, es conveniente que tenga colchón reclinable por la cabeza para levantarlo un poquito después de las tomas para evitar atragantamientos si el bebé regurgita.

[![](/images/uploads/bebe-confort.jpg)](https://www.amazon.es/Confort-regulable-posiciones-reclinable-Geometric/dp/B09JCMVW61/ref=sr_1_31?keywords=cuna+colecho&qid=1642527473&s=baby&sprefix=cunas+col%2Cbaby%2C487&sr=1-31&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Confort-regulable-posiciones-reclinable-Geometric/dp/B09JCMVW61/ref=sr_1_31?keywords=cuna+colecho&qid=1642527473&s=baby&sprefix=cunas+col%2Cbaby%2C487&sr=1-31&madrescabread-21)

## Fabricar una cuna de colecho casera paso a paso

A continuación os explico cómo lo hicimos paso a paso. Bueno, para ser justa, lo hizo mi marido mientras yo lo observaba sentada con una barriga de 8 meses de embarazo.

1- Mide la altura del colchón de tu cama desde el suelo.

2- Una vez montada la cuna con el colchón puesto, mide la altura del colchón desde el anclaje del somier de la cuna.

3- Resta la medida del paso 2 a la medida del paso 1.

4- Señala la medida que da como resultado la anterior operación en la estructura de la cuna.

5- En ese punto tienes que hacer 2 agujeros simétricos a los que ya hay de serie en la estructura de la cuna.

De este modo ya puedes anclar el somier en ellos y colocar el colchón de nuevo, que quedará a la altura de tu cama.

Para unir la cuna a tu cama lo más sencillo es pegarla a la pared y poner a continuación tu cama. Pero si no te es posible tienes más opciones:

\-Con canapé: Con el canapé abierto pasa dos veces unas correas por debajo de la tapa, por debajo del canapé y por el larguero de la cuna y átalas (idea de Vanesa, del blog [Una Madre como tú](http://www.laorquideadichosa.com/)).

\-Sin canapé: puedes usar unas [correas con gancho o pulpos](https://amzn.to/2GEZ9AY) y unir ambos somieres: idea de Mousik, del blog Una mirada al otro lado.

Y ya tenéis vuestra cuna de colecho por unos 130 €.

## Importante cambiar de lado al bebé en la cuna de cohecho

Para evitar que el bebé duerma siempre del mismo lado, ya que siempre tenderá a inclinar la cabeza hacia el lugar donde duerma la madre, es conveniente acostarlo de vez en cuando con la cabeza en los pies de la cuna. De este modo conseguiremos que la cabecita se desarrolle de igual modo por ambos lados, y que el cuello se fortalezca por igual por la parte derecha que por la izquierda evitando la plagiocefalia.

## ¿Cuánto dura una cuna de colecho?

Las minicunas de colecho en teoría duran hasta los 6 meses, pero mi experiencia con hijos grandes es que se me quedan pequeñas a los 4 meses, pero depende del tamaño del bebé. Por eso esmero escoger una cuna de colecho más grande, sobre todo si tu bebé es de los grandotes.

Lo ideal es ir adaptando el colecho de forma evolutiva conforme el bebé va creciendo y desarrollándose.

[![](/images/uploads/interbaby-cuna.jpg)](https://www.amazon.es/Interbaby-Madera-Blanca-Dreams-AZCUNA12/dp/B096C1GXMR/ref=sr_1_23_sspa?keywords=cuna+colecho&qid=1642527473&s=baby&sprefix=cunas+col%2Cbaby%2C487&sr=1-23-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFKNVpRR1NOQjlXTkYmZW5jcnlwdGVkSWQ9QTA0ODQ5OTNNVDVQNTNPSkM4VUwmZW5jcnlwdGVkQWRJZD1BMDk2MzUwN1RFTzJaWTBJNUFCRyZ3aWRnZXROYW1lPXNwX210ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Interbaby-Madera-Blanca-Dreams-AZCUNA12/dp/B096C1GXMR/ref=sr_1_23_sspa?keywords=cuna+colecho&qid=1642527473&s=baby&sprefix=cunas+col%2Cbaby%2C487&sr=1-23-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFKNVpRR1NOQjlXTkYmZW5jcnlwdGVkSWQ9QTA0ODQ5OTNNVDVQNTNPSkM4VUwmZW5jcnlwdGVkQWRJZD1BMDk2MzUwN1RFTzJaWTBJNUFCRyZ3aWRnZXROYW1lPXNwX210ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=&madrescabread-21)

### Colecho cuando el bebé ya puede sentarse

El bebé ya ha crecido, ronda los 6 meses y puede sentarse. Llegó la hora de poner una barrera para evitar accidentes, [@Ireyazp](https://twitter.com/Ireyazp) me dio la solución en twitter. Se trata de una [barrera para cunas](https://amzn.to/2KcUQip) (unos 30 €), que iría anclada bajo nuestro colchón.

Fácilmente extraíble levantando un poco nuestro colchón ,sin necesidad de mover la cuna, permite quitarla cuando estemos durmiendo, y ponerla cuando nos queramos levantar y dejar al bebé sólo en su cuna, quedando perfectamente protegido para que no se escape gateando por nuestra cama.

![cuna-colecho-casera](https://d33wubrfki0l68.cloudfront.net/52cb281d50c7290f037863e2b5da9f5943d654d6/20769/tumblr_files/tumblr_inline_p98q4aneek1qfhdaz_540.jpg)

![cuna-junto-cama](https://d33wubrfki0l68.cloudfront.net/c84d6d2da35849aad99fc7ad2e85f48d3d6f369b/30c41/tumblr_files/tumblr_inline_p98q4ajfdu1qfhdaz_540.jpg)

![barrera-cuna](https://d33wubrfki0l68.cloudfront.net/d37298e15f1725ae136cbf8690af40166fd9245e/aeb0e/tumblr_files/tumblr_inline_p98q4bbttb1qfhdaz_540.jpg)

Fotos gentileza de @Ireyazp, a quien agradezco en el alma que haya compartido su idea que, sin duda nos ayudará a más de una.

### Colecho cuando el bebé se desplaza por sí mismo

Ya tiene unos 20 meses y es un terremoto. Ya puede elegir dónde duerme, y tiene sus preferencias. Yo le he dado libertad con el [colecho interactivo](https://madrescabreadas.com/2014/06/26/sueno-feliz-madrescabreadas.html) y, de momento, logramos dormir todos felices hasta que decida irse a su cuarto con su hermano.

![image](https://d33wubrfki0l68.cloudfront.net/2c062422879efb87d38a5e7bab7b5e460c641acb/59eb1/tumblr_files/tumblr_inline_p98q4bidzw1qfhdaz_540.jpg)

Pusimos una camita de IKEA junto a la nuestra, heredada de su hermano, entre la pared y nuestro canapé que, aunque quede más baja no es problema porque el bebé sube y baja de la suya a la nuestra a su antojo durante la noche, se acomoda y se duerme donde le da la gana por su cuenta y nos deja descansar porque no llora al tenernos con él.

Él elige con quién se acurruca, o si le apetece estar más ancho en su espacio. Le hemos dado independencia y la posibilidad de que duerma en su propia cama cuando lo decida.

Él está encantado porque tiene una cama de “mayó”, y se lo cuenta a todo el mundo. A veces desaparece y nos lo encontramos metido en ella jugando a taparse y destaparse. Si te preocupa [hasta cuándo vais a practicar colecho te recomiendo este post](https://madrescabreadas.com/2022/01/10/hasta-cuando-colecho/).

[![](/images/uploads/roba-cuna.jpg)](https://www.amazon.es/Baumann-Roba-GmbH-0176W-auxiliar/dp/B07ZG1849D/ref=sr_1_17?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=213B89XIRT900&keywords=cama+colecho&qid=1642528068&s=baby&sprefix=c+colecho%2Cbaby%2C310&sr=1-17&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Baumann-Roba-GmbH-0176W-auxiliar/dp/B07ZG1849D/ref=sr_1_17?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=213B89XIRT900&keywords=cama+colecho&qid=1642528068&s=baby&sprefix=c+colecho%2Cbaby%2C310&sr=1-17&madrescabread-21)

### Solución para la unión entre los colchones en la cuna de colecho

Los **conectores de camas** tipo puente con correas de ajustes son una solucion muy buena.

[![](/images/uploads/vivilinen-kit.jpg)](https://www.amazon.es/VIVILINEN-convertidor-Espacios-Conector-viscoel%C3%A1stica/dp/B08X43QFMD/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=SEOMR8PKMQGB&keywords=SIGGERUD&qid=1642528433&sprefix=siggerud%2Caps%2C320&sr=8-4&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/VIVILINEN-convertidor-Espacios-Conector-viscoel%C3%A1stica/dp/B08X43QFMD/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=SEOMR8PKMQGB&keywords=SIGGERUD&qid=1642528433&sprefix=siggerud%2Caps%2C320&sr=8-4&th=1&madrescabread-21)

¿Te animas a probar? Por favor, si tienes más ideas déjame un comentario, ¡serán bienvenidas!

Pero si no eres muy manitas, actualmente hay mucha variedad de cunas de colecho en el mercado. Aquí, unos ejemplos:

## Cuna colecho convertible

Lo ideal es que la cuna de colecho se pueda convertir en cuna normal, con medidas normales para cuando el bebé crezca.

[![](/images/uploads/maxi-cosi.jpg)](https://www.amazon.es/VIVILINEN-convertidor-Espacios-Conector-viscoel%C3%A1stica/dp/B08X43QFMD/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=SEOMR8PKMQGB&keywords=SIGGERUD&qid=1642528433&sprefix=siggerud%2Caps%2C320&sr=8-4&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/VIVILINEN-convertidor-Espacios-Conector-viscoel%C3%A1stica/dp/B08X43QFMD/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=SEOMR8PKMQGB&keywords=SIGGERUD&qid=1642528433&sprefix=siggerud%2Caps%2C320&sr=8-4&th=1&madrescabread-21)

## Cuna colecho Chicco

Chicco fue la primera marca en España en sacar una cuna de colecho asequible justo después de que nosotros fabricáramos la nuestra . Tiene la posibilidad de elevar el colchón por la parte de la cabeza, lo cual es imprescindible para mí.

[![](/images/uploads/chicco-next2me2.jpg)](https://www.amazon.es/Chicco-Next2Me-Dream-colecho-balanc%C3%ADn/dp/B07GJGKMVR/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1BGQAX5QYMWH9&keywords=Cuna%2BChicco&qid=1642531762&sprefix=cuna%2Bcolecho%2Bchicco%2Caps%2C804&sr=8-1&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Chicco-Next2Me-Dream-colecho-balanc%C3%ADn/dp/B07GJGKMVR/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1BGQAX5QYMWH9&keywords=Cuna%2BChicco&qid=1642531762&sprefix=cuna%2Bcolecho%2Bchicco%2Caps%2C804&sr=8-1&th=1&madrescabread-21)

Pero también tenéis otras marcas que han apostado por esta solución que cada vez más madres eligen, que es dormir con su bebé cerca, por ejemplo:

## Cuna colecho Micuna

## Cuna colecho carrefour

Si te ha sido útil, comparte para ayudar a otras familias, y ayudar a seguir escribiendo este blog.