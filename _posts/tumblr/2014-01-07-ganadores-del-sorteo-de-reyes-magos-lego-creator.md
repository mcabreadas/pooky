---
date: '2014-01-07T22:11:00+01:00'
image: /tumblr_files/tumblr_inline_mz1v1eSmQ91qfhdaz.jpg
layout: post
tags:
- premios
- trucos
- recomendaciones
title: Ganadores del sorteo de Reyes Magos LEGO CREATOR
tumblr_url: https://madrescabreadas.com/post/72585292264/ganadores-del-sorteo-de-reyes-magos-lego-creator
---

Bueno, bueno, bueno… ayer pasaron por el blog los Reyes Magos y ya tenemos los ganadores de los [3 juegos Lego Creator](/2013/12/22/sorteo-de-reyes-magos-lego-creator.html).

![](/tumblr_files/tumblr_inline_mz1v1eSmQ91qfhdaz.jpg)

Tengo que agradeceros la alta participación y lo bien que habéis cumplido con los requisitos para participar, lo que ha hecho que no tenga que robar demasiadas horas de sueño para realizar el sorteo.

También agradezco a LEGO que me haya cedido los juegos para premiar a mis lectores y darles una alegría por Reyes, lo que me ha hecho especial ilusión, ya que es una marca que me encanta por lo educativa, y sobre todo porque a mis hijos les chifla.

No os hago sufrir más. Los ganadores son:

![image](/tumblr_files/tumblr_inline_mz1ukkkQUI1qfhdaz.jpg)

Enhorabuena a:

-Bárbara Llana

-Pedro Daniel Marcos Martínez

-Pablo Sánchez

Espero que lo disfrutéis, y os invito a pasaros por aquí a contarnos!

Por favor, pasadme vuestro nombre completo y dirección postal por privado para el envío.

Os ha hecho ilu?