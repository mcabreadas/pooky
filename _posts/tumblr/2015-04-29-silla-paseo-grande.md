---
date: '2015-04-29T08:00:25+02:00'
image: /tumblr_files/tumblr_inline_nndek5rYVJ1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- seguridad
- bebes
title: Qué busco en una silla de paseo para un bebé grande
tumblr_url: https://madrescabreadas.com/post/117669989876/silla-paseo-grande
---

Mi bebé está en esa edad en la que empieza a dejar de serlo (lo que los anglosajones llaman, un toddler), es decir, ya camina, come solito, comienza a controlas esfínteres poco a poco, ha ganado bastante autonomía en ciertas cosas, pero todavía no se puede considerar que haya abandonado esta primera etapa de su vida.

Cuando salimos le gusta andar, pero enseguida se cansa, por lo que todavía necesitamos llevar la sillita. Además, su sueño de media mañana, y su siesta después de comer son sagrados, y nos surgió la necesidad de encontrar una silla de paseo ligera, pero no tanto como para perder en comodidad y anchura para él.

¿Qué buscamos en una silla de paseo para que sirva a un bebé de dos años y medio?

## Anchura y respaldo alto

El problema de algunas sillas de paseo que probamos es que se le quedaban pequeñas porque ya, con dos años y medio, su cuerpo es bastante grande, y no va cómodo en cualquiera. Algunas son demasiado estrechas, y no puede dormir bien en ellas, o el respaldo no es lo suficientemente alto como para sujetarle la cabeza entera.

Así que elegimos la [Spot de Bebécar](http://www.bebecar.com/bebecar/es/puntos-venta) porque, aunque no es de las más ligeras del mercado (sí es lo suficiente), a cambio es ancha y grande para que el bebé vaya cómodo y pueda dormir en ella, se reclina totalmente y el asiento es acolchado. Aguanta hasta 23 Kg de carga. 

 ![image](/tumblr_files/tumblr_inline_nndek5rYVJ1qfhdaz_540.jpg)
## Maniobrabilidad con poco esfuerzo

Esto es importante porque el peso del niño es considerable, por lo que la subida y bajada de escalones se puede convertir en un suplicio para la madre o el padre.

Nos gusta mucho cómo se maniobra con esta silla porque tiene un sistema que, si el niño pesa más de 8 Kg, se acciona un sistema que hace que el peso deje de recaer sobre las dos ruedas traseras principalmente, y se reparta por toda la base. Sólo hay que presionar esos cuadraditos que veis al lado de ambas ruedas (en la foto he dejado uno subido y otro bajado para que lo veais).

 ![image](/tumblr_files/tumblr_inline_nnddrmbTdF1qfhdaz_540.png)

Además, tiene amortiguación en las ruedas, y un manillar lo suficientemente alto como para poder hacer palanca y levantar las ruedas delanteras sobre las traseras sin necesidad de un gran esfuerzo.

 ![image](/tumblr_files/tumblr_inline_nnddq1olgz1qfhdaz_500.gif)
## Suave deslizamiento

Las ruedas llevan cojinetes de acero, no de plástico, lo que hace que la duración de las mismas sea casi eterna.

 ![image](/tumblr_files/tumblr_inline_nndctu3cHv1qfhdaz_540.png)

Las delanteras son giratorias, pero se pueden poner fijas con un click. Esto es muy útil cuando quieres dormir al bebé meciéndolo hacia adelante y hacia atrás, lo digo por experiencia.

Un buen freno fácil de accionar también es importante. éste frena las cuatro ruedas, y se acciona presionando con el pie.

 ![image](/tumblr_files/tumblr_inline_nndd7lz4nl1qfhdaz_540.png)
## Reclinación total

Como ya es mayor, y el problema de reflujo ha desaparecido, la verdad es que duerme mucho mejor acostado completamente, lo que se consigue bajando el respaldo a tope, y elevando el reposa pies.

 ![image](/tumblr_files/tumblr_inline_nndd18dZNz1qfhdaz_540.png)

Esta característica hace que la silla esté homologada desde el nacimiento, por lo que lleva un sistema de seguridad anti hermanos celosillos a quienes se les pueda ocurrir levantar el respaldo de golpe cuando el bebé duerme empujando hacia arriba (me ha pasado), que consiste en una palanca de seguridad.

 ![image](/tumblr_files/tumblr_inline_nnddqoZJ2z1qfhdaz_540.png)
##   

## Una gran capota

En mi ciudad hay un sol de justicia, y en esta época, más, así que para nosotros era esencial una capota grande, que cubriera bastante al bebé, sobre todo cuando se queda dormido.

La silla Spot tiene una capota extensible, y además, se regula su altura, con lo que, si reclinas al niño mientras duerme, la puedes bajar a tope y dejarlo bien protegido.

 ![image](/tumblr_files/tumblr_inline_nndd2i31Ik1qfhdaz_540.png)

## Plegado fácil y compacto

Aquí viene lo sorprendente de esta silla, y es que, a pesar de que es bastante grande y completa, se pliego en bastón sin necesidad de quitar la barra protectora.

Normalmente, las sillas más anchas y grandes que habíamos visto se plegaban en libro, por lo que ocupan más espacio en el maletero del coche, por eso para nosotros era importante que se plegara en bastón, porque para viajar somos 5, y tenemos un coche normalito con un maletero también normalito, por lo que hay que optimizar el espacio, ya me entendéis.

 ![image](/tumblr_files/tumblr_inline_nndcucHEZ41qfhdaz_540.png)

Todo indica que contamos con los ingredientes para una silla que cubre las necesidades que tenemos en esta etapa, así que vamos a testearla, y dentro de unos meses [os cuento cómo nos ha ido](/2015/09/20/test-spot-bebecar.html).

Quiero agradecer al personal de la tienda [Yoyó Bebé](http://yoyobebe.es/es), de Murcia, la atención recibida, y lo bien que nos explicaron todas las características y posibilidades de la silla, y todas las dudas que nos surgieron.

¿Qué es lo que más valoras tú en una silla de paseo?