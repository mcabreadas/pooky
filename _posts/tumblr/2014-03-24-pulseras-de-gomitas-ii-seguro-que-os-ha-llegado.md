---
layout: post
title: Como hacer pulseras de gomitas
date: 2014-03-24T11:00:00+01:00
image: /tumblr_files/tumblr_inline_pdxw9gY4hI1qfhdaz_540.jpg
author: .
tags:
  - planninos
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/80564016400/pulseras-de-gomitas-ii-seguro-que-os-ha-llegado
---

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/Z8wnT6wWRiQ?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

## Pulseras de gomitas (II)

Seguro que os ha llegado a casa la nueva moda de las pulseras con gomitas “Cracy looms”. En el colegio de Princesita es el furor, y hoy ha salido de clase pegando saltos porque por fin ha aprendido a hacerlas, y ha querido compartirlo con vosotros.

Me ha impresionado que, con lo tímida que es, ha explicado perfectamente ante la cámara de mi móvil (perdón por la calidad) la manera de hacerlas con una soltura pasmosa. Nunca lo hubiera imaginado.

![](/tumblr_files/tumblr_inline_pdxw9gY4hI1qfhdaz_540.jpg)

Acto seguido se ha puesto a hacer pulseras para toda la familia, la primera una especial para su padre con los colores de la bandera de España. Y luego para Osito ha hecho dos, una igual que la de su padre y otra en monocolor, azul, a juego con sus zapatillas deportivas.

Os animo a probar, porque engancha a niños y a mayores, y las posibilidades son infinitas. De momento hemos explicado la de dos, pero queda pendiente hacerla con cuatro. Ya os subiré el video al recién estrenado [canal de Youtube de Madres Cabreadas](http://www.youtube.com/channel/UCMkzI7f4qxHj3xEmluf4K3Q), al que espero que os suscribáis, dicho sea de paso ;)

Te animas?