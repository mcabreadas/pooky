---
date: '2018-04-01T16:40:33+02:00'
image: /tumblr_files/tumblr_inline_p62dk68i6Q1qfhdaz_1280.jpg
layout: post
tags:
- colaboraciones
- local
- ad
- eventos
- moda
title: Döret inaugura tienda en Murcia
tumblr_url: https://madrescabreadas.com/post/172482491589/döret-inaugura-tienda-en-murcia
---

Esta semana hemos estado en la inauguración de la tienda que la franquicia Dörett de moda infantil y júnior ha abierto en Murcia, en la calle Sociedad.

 ![](/tumblr_files/tumblr_inline_p62dk68i6Q1qfhdaz_1280.jpg)

¿Conoces la marca? Tiene su base en Murcia, y empezó a abrir tiendas por diferente puntos de España y del extranjero hace unos años, y tiene previstas próximas aperturas en Castellón, Valencia, Barcelona y Casablanca.

 ![](/tumblr_files/tumblr_inline_p62dk79EWR1qfhdaz_1280.jpg)

Pero no ha sido hasta ahora cuando, tras conquistar otras tierras, por fin han desembacado en Murcia, a pesar de tener sunorigen aquí!

Por eso lo celebraron con una gran fiesta en la calle con una banda de músicos en directo, champán para los adultos una increíble mesa dulce para los niños (y los no tan niños…).

 ![](/tumblr_files/tumblr_inline_p62dk62VTq1qfhdaz_1280.jpg)

Globos, animación y una decoración de cuento nos esperaban cuando llegamos. Todo cuidado hasta el último detalle, y un personal atento y entusiasmado con esta nueva aventura para Dörett en Murcia.

 ![](/tumblr_files/tumblr_inline_p62dk6sDSa1qfhdaz_1280.jpg)

La joven firma del Grupo Queens Boutiques apuesta por su innovador concepto de moda para bebé y niños hasta la talla 14. Se diferencia por sus precios en bloque, es decir, artículos de ­­5, 10, 15, 20, 25 y + 30 euros.

 ![](/tumblr_files/tumblr_inline_p62dkatItv1qfhdaz_1280.jpg)

La apertura de Dörett en Murcia ha sido todo un acontecimiento por la presentación de su nueva de línea de moda infantil “Kids”, más sport, con un amplio catálogo de prendas de diseño y calidad para diferentes tipos de públicos pero conjugando siempre la delicadeza y elegancia del estilo chic europeo tan característico de la firma, y a precios muy competitivos.

 ![](/tumblr_files/tumblr_inline_p62dk6LQOA1qfhdaz_1280.jpg)

Asistí a la inauguración con mis hijos, y lo pasaron de maravilla, aunque el pequeño casi acaba con la.meaa dulce él solo (bueno, yo Le ayude un poco…)

 ![](/tumblr_files/tumblr_inline_p62dk7RyT21qfhdaz_1280.jpg)

Pero fue mi Princesa la que disfrutó a tope haciendo fotos y stories con mi cuenta de Instagram contando las prendas que más Le gustaban y sus looks favoritos.

 ![](/tumblr_files/tumblr_inline_p62dk7cZXN1qfhdaz_400.gif)

Me hizo mucha gracia, y por eso he dejado las stories como destacadas en mi perfil de Instagram. Te dejo el enlace aquí por si quieres verlas.

 ![](/tumblr_files/tumblr_inline_p62dk9iZu21qfhdaz_400.gif)

A las chicas de Döret, que me atendieron de maravilla y me enseñaron las colecciones con mucho agrado les doy las gracias.

Nos vemos pronto!