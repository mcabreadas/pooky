---
date: '2016-11-30T13:51:55+01:00'
layout: post
tags:
- libros
- planninos
title: 'Planes caseros con niños: 150 actividades sin pantallas'
tumblr_url: https://madrescabreadas.com/post/153860329109/planes-caseros-con-niños-150-actividades-sin
---

Llegan unos días de fiesta ideales para pasar tiempo en familia haciendo todo tipo de actividades, pero como en muchos sitos hace frío o llueve, os voy a sugerir 150 cosas que podéis hacer en casa.

Todas vienen recogidas en este libro de la editorial Juventud, que pretende alejar a los niños de las pantallas dándoles un montón de alternativas para divertirse.

Os dejo la video reseña de “[150 actividades para jugar sin pantallas](http://www.boolino.es/es/libros-cuentos/150-actividades-para-jugar-sin-pantallas/)”, de Asia Citro:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vgKNG1vNxII" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

¿Os ha gustado? Comparte para ayudar a otras familias y suscríbete para no perderte nada!