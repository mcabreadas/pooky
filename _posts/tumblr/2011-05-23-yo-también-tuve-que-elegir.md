---
layout: post
title: Yo también tuve que elegir
date: 2011-05-23T16:47:00+02:00
image: /images/posts/elegir/p-elegir.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/5767664057/yo-también-tuve-que-elegir
---
Otro problema de trabajar en casa es que los demás tienen la impresión de que siempre estás disponible, como si no trabajaras realmente, o al menos esa es la impresión que yo tengo. Es fundamental el respeto de los demás en este aspecto. Es bastante duro, sobre todo al principio, hasta que no te adaptas al cambio. Supongo que como todos los cambios en la vida, cuestan.

## El teletrabajo

Yo pasé de echar mil horas al día en un despacho de abogados con compañeros, comidas, salidas… a encontrarme en casa todo el día prisionera de mi propia decisión y casi sin darme cuenta. Pero es que si me iba a trabajar era peor porque me deprimía tanto no estar con mi bebé que pasé casi un año llorando por las esquinas porque llegaba a casa a las 9 de la noche y ya estaba acostadita.

Iba por la calle corriendo siempre para llegar a tiempo de verla un rato. Por eso, cuando la oí llamar a su abuela “mamá” pensé: “hasta aquí hemos llegado”. Y decidí ponerme el despacho en casa y a luchar por criar a mi hija yo, con ayuda, pero yo. Luego llegó el niño y así sigo.

Ya totalmente adaptada, pero me ha costado años encontrar mi lugar porque no me veía ni como ama de casa al margen de mi carrera porfesional de Abogada, ni como super Abogada dedicadísima a su carrera y observadora desde fuera de la crianza de sus hijos.

## El coste de la conciliación

Así que, como dijo Aristóteles, en el punto medio está la virtud. 

El problema es encontrarlo, claro, y yo creo que lo encontré, pero nadie sabe los nervios que he pasado, y que le transmití, sobre todo a mi primera hija cuando todo se me vino encima y tomaba pecho mientras yo negociaba asuntos por teléfono con compañeros, o escribía demandas en el ordenador con una mano mientras le ayudaba con los gases con la otra, o … en fin, qué os voy a contar… sólo me arrepiento (y me cabrea sobremanera) de no haber sabido mantener la calma porque quizá la perjudiqué, pero me pilló una época fortísima de trabajo y era  mi responsabilidad, no podía delegarlo, los clientes confiaban en mí. 

Fue una auténtica locura hasta que encontré el equilibrio. Aquella primera época no se la deseo a nadie. Pero creo haberlo logrado y, aunque sé que sé renunciado a cosas, estoy contenta de haber puesto a mis hijos por delante siempre.

*Photo by Green Chameleon on Unsplash*