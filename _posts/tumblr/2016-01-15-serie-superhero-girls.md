---
date: '2016-01-15T09:42:08+01:00'
image: /tumblr_files/tumblr_inline_o39yr5Ufo21qfhdaz_540.jpg
layout: post
tags:
- hijos
- trucos
- crianza
- adolescencia
- colaboraciones
- ad
- recomendaciones
- familia
- mujer
title: Todas somos Super Hero Girls
tumblr_url: https://madrescabreadas.com/post/137337498924/serie-superhero-girls
---

¡Ya era hora! Las niñas también quieren ser heroínas, como Wonder Woman, Supergirl o Batgirl, por eso [DC Comics](http://www.dcsuperherogirls.com/es-es?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=DCG_Home_blog3) ha sacado una [Serie On line](https://www.youtube.com/channel/UCRdVJ9XqHCb6BqApCIO_TYg?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=YouTube_blogpost3) con todas ellas como alumnas del instituto Super Hero High. Os adelanto este video:

<iframe class="embedly-embed" frameborder="0" scrolling="no" allowfullscreen src="//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Ft6gPIGnOMZo%3Ffeature%3Doembed&amp;url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dt6gPIGnOMZo&amp;image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Ft6gPIGnOMZo%2Fhqdefault.jpg&amp;args=showinfo%3D0&amp;key=b1268f588afd4698bd3e71a47efd182a&amp;type=text%2Fhtml&amp;schema=youtube" width="854" height="480" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></iframe>

## Las heroínas cotidianas

No es de extrañar que el mundo de los héroes se acerque cada vez más a la figura femenina, sólo hay que vernos a las madres corriendo todo el día de un sitio para otro intentando llegar a todo, cuadrando horarios imposibles al tiempo que planeamos la fiesta de cumpleaños del pequeño y el disfraz de carnaval de los mayores.

¡Desde luego, la [conciliación laboral y familiar](/2011/04/28/al-rescate-de-supermam%C3%A1.html) es propia de Súper heroínas!

 ![](/tumblr_files/tumblr_inline_o39yr5Ufo21qfhdaz_540.jpg)

Pero ojo, “ser súper no significa ser perfecta”, como dice la directora del Instituto de super heroínas, Amanda Waller. Y es lo que yo pienso: no nos exijamos demasiado, y seamos comprensivas con nuestros errores, porque cada día se aprende algo nuevo, como nuestras Hero Girls cuando van a clase.

 ![](/tumblr_files/tumblr_inline_o39yr5sgQT1qfhdaz_540.jpg)
## Las niñas también pueden ser héroes

Me encanta que se familiarice a las niñas con este tipo de personajes, cuyas virtudes son la confianza en sí mismas, la valentía, y la seguridad. Creo que es importante que se les muestre un prototipo de mujer independiente, autosuficiente, fuerte y empoderada. 

Y creo que una de las mejores maneras de inculcar esos valores en nuestras hijas es a través de series de dibujos como ésta que os presento, que les gusten y tengan gancho por estar ambientadas en un entorno cotidiano para ellas, como es el escolar, y con situaciones similares a las que viven cada día en las relaciones con sus compañeros, profesores… 

## ¿Qué Super Heroína eres?

Seguro que tú misma te sientes identificada con alguna de las protagonistas: Wonder Woman (líder), Supergirl (fuerte), Batgirl (inteligente), Harley Quinn (ingeniosa), Bumblebee (enérgica), Poison Ivy (científica), o Katana (creativa).

 ![](/tumblr_files/tumblr_inline_o39yr6EtYr1qfhdaz_540.jpg)

¿Tienes curiosidad por saber a qué heroína te pareces más? Te propongo este divertido test, y que me cuentes en los comentarios la qué resultado te ha salido:

<script src="//dcc4iyjchzom0.cloudfront.net/widget/loader.js" async></script>

A mí me ha salido Bumblebee, y creo que ha acertado bastante porque la verdad es que no paro quieta ni un momento y siempre me estoy ilusionando con proyectos nuevos.

Además de este test, en la [Web de DC Super Hero Girls](http://www.dcsuperherogirls.com/es-es?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=DCG_Home_blog3) puedes encontrar juegos, [fotos](http://www.dcsuperherogirls.com/es-es/gallery), videos y muchas más cosas. Y si quieres estar al día de los capítulos de la serie, puedes seguir su [Canal de Youtube aquí.](https://www.youtube.com/channel/UCRdVJ9XqHCb6BqApCIO_TYg?utm_source=Blog&utm_medium=Web&utm_campaign=DCG%20FALL15_ES&utm_content=YouTube_blogpost3)

¡Pero hay más sorpresas!

Esta primavera saldrá una colección de muñecas, libros, juegos, material escolar, y hasta ropa de las Super Hero Girls que va a molar mucho.

¡Las mujeres al poder!

Cuéntame, ¿Qué super heroína eres?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.