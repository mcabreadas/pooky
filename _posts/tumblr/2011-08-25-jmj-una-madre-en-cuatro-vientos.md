---
layout: post
title: JMJ. Una madre en Cuatro Vientos
date: 2011-08-25T10:33:00+02:00
image: /tumblr_files/tumblr_lqh687cB9z1qfhdaz.jpg
author: maria
tags:
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/9369389665/jmj-una-madre-en-cuatro-vientos
---
A mis treinta y cuatro años es la primera vez que participo en una JMJ, y ahora me doy cuenta de lo que me perdí, y de que no es lo mismo con quince que con treinta, lógicamente. Yo siempre había presumido de que he disfrutado cada etapa de a tope, sin saltarme ninguna y aprovechando al máximo las cosas buenas que me ofrecía la vida en cada una de ellas. Pero me equivocaba, se me olvidó quizá la más importante: esa inyección de Fe que dan los encuentros con el Papa que reúnen a tanta gente, tantos jóvenes rezando juntos y creyendo lo mismo.

Es un sentimiento difícil de explicar, de verdad. Es extraño sentirse tan cerca de alguien sin conocerlo de nada y sin ni siquiera hablar su idioma, pero una mirada o un gesto lo dicen todo, no hace falta más. Todos vamos a una, y eso, con casi dos millones de personas es muy fuerte. Es emocionante. Y claro que cada uno tenemos nuestro carisma o movimiento, o algunos no tenemos ninguno, pero en la esencia todos creemos lo mismo.

![image](/tumblr_files/tumblr_lqh6bl6fZp1qfhdaz.jpg)

Yo fui con mi familia, en mi papel de madre, que es lo que toca ahora, pero no podía evitar imaginar cómo lo hubiera vivido con 15 años menos. Vi la alegría de los jóvenes, sus miradas de ilusión, sus rostros cansados y quemados por el sol, que más tarde se mojarían por la lluvia y se helarían por el frío de la madrugada y pensé que algo muy importante los había llevado allí como para que no salieran corriendo todos, sobre todo después de la tormenta huracanada que sufrimos en la Vigilia con el Papa.  Pero… ¿Cómo es que no se van? ¿Cómo es que corean gritos al Papa y saltan de alegría en vez de escapar o quejarse por la lluvia? ¿No ven que es peligroso, que caen rayos, que se ha derrumbado una gran estructura que hay en la salida y ha herido a 7 peregrinos? Lejos de eso ¡se ayudan los unos a los otros! No se ven escenas así en la vida cotidiana, o quizá sí, si nos fijamos bien, no sé.

![image](/tumblr_files/tumblr_lqh6a4SQrd1qfhdaz.jpg)

El caso es que yo abracé a mi hija y la protegí con mi cuerpo tapándole la cabeza con las manos para evitar que la golpeara algún objeto volante y mi marido se puso de barrera cortándonos el aire con un paraguas y tragándose él solito el agua y el viento, mientras mi pequeño saltaba y gritaba ajeno a toda preocupación. Cuál fue mi sorpresa cuando, de repente alguien  empezó a protegernos con colchonetas de camping a modo de iglú, y quedamos totalmente aislados. Luego, cuando escampó pude ver a los voluntarios que nos habían tapado, que estaban hechos una sopa y preocupados por los niños, incluso una chica nos dio su única camiseta seca para que se la cambiáramos a la peque y evitar que cogiera frío. Ese espíritu yo no lo he visto en ningún otro sitio, y hablo de mi experiencia , claro está. Yo temía avalanchas de gente gritando ¡sálvese quién pueda!”, o un desalojo masivo… no sé. Pero no fue así, todos se ayudaban entre sí con alegría, como si tuvieran la certeza de que nada malo les iba a ocurrir.  

Yo, sinceramente, temí por los niños y a la caída de objetos. Yo sí pasé miedo durante unos instantes. De hecho hubo varios heridos leves, incluso en el escenario, en el que un Obispo se llevó un moratón de recuerdo, y el Papa se mojó bastante porque la lluvia le venía de frente, pero no abandonó, a pesar de la insistencia de las personas que lo asistían. La orquesta tuvo que guardar a toda prisa sus instrumentos y, de la treintena de piezas que tenía  preparadas, fruto de ensayos diarios desde marzo, sólo pudieron interpretar tres. Todos los jóvenes lectores, incluída una amiga nuestra, se quedaron sin llevar a cabo su cometido, después de dos jornadas de pesados ensayos al solanero de Cuatro Vientos. En fin, no vi ni una sola mala cara, de verdad. Al contrario, y pensé, si el Reino de los Cielos existe, tiene que ser un lugar parecido a esto, donde cada uno se dé al otro olvidándose de sí mismo.

Me quedo con las primeras palabras del Papa cuando terminó la tormenta:  “Gracias, amigos!  Gracias por vuestra alegría!”.

Sentí que todos éramos uno.