---
date: '2015-12-01T19:03:49+01:00'
image: /tumblr_files/tumblr_inline_nymkqnWXFr1qfhdaz_540.png
layout: post
tags:
- navidad
- trucos
- reyesmagos
- regalos
- premios
title: Concurso “Tu Carta a los Reyes Magos” 2016: Mochila y estuche a juego Lois
tumblr_url: https://madrescabreadas.com/post/134342049859/mochila-navidad-lois
---

![](/tumblr_files/tumblr_inline_nymkqnWXFr1qfhdaz_540.png)

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

[Aquí tenéis todos los regalos entre los que podéis elegir](/2015/12/01/juguetes-navidad-gratis.html).

Uno de los regalos que podéis incluír en vuestra carta a los Reyes Magos para participar en el concurso es este juego de mochila y estuche para el cole de la marca [Lois](http://www.loisjeans.com/es/ES/home), ideal para que los niños vayan al cole perfectamente equipados.

Además el material es de alta calidad, lo que hace que duren varios cursos.

Me encanta el diseño porque es muy moderno, ideal para esa edad en la que empiezan a querer cosas diferentes, un poco más de mayores.

El **estuche** portatodo triple lleva la marca bordada en el exterior, es de poliéster, y mide 21x11.

 ![](/tumblr_files/tumblr_inline_nylku3aLKd1qfhdaz_540.jpg)

La **Mochila** es de poliéster, y ** ** tiene un bolsillo delantero con cremallera, como el resto de cierres. Las asas son regulables y es también adaptable a carro.

Lleva culera reforzada de poliéster y cremallera invertida.

Las medidas son 32x44x16.

 ![](/tumblr_files/tumblr_inline_nylku0M88q1qfhdaz_540.jpg)
## Para conseguir el juego de mochila y estuche de Lois

-Deja un comentario en este post con tu carta a los Reyes Magos. Se admiten fotos.  Sé original porque ganará la carta más original y auténtica, a elegir por la marca. Pon tu alias e indica la red social donde sigas a Lois.

-Da “me gusta” a la [página de Facebook de Lois](https://www.facebook.com/LoisJeans), o sigue a [Lois Jeans en Twitter](https://twitter.com/LoisJeans), o sigue a [Lois Jeans en Instagram](https://www.instagram.com/lois_jeans/)

-El plazo para subir tu carta finaliza el 15 de diciembre, y el resultado se publicará en este blog en los días sucesivos.

-El ámbito del concurso es la península.

¡Tienes hasta el 15 de diciembre!