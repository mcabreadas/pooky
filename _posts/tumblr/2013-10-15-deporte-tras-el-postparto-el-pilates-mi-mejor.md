---
date: '2013-10-15T22:05:00+02:00'
image: /tumblr_files/tumblr_inline_p98q494e7l1qfhdaz_540.jpg
layout: post
tags:
- crianza
- postparto
- familia
- conciliacion
- mujer
title: 'Deporte tras el postparto. El Pilates: mi mejor medicina'
tumblr_url: https://madrescabreadas.com/post/64138534512/deporte-tras-el-postparto-el-pilates-mi-mejor
---

Por fin he logrado sacar tiempo y organizarme para poder asistir a clases de Pilates dos veces por semana.  
 Antes del embarazo de Leopardito solía practicarlo en el Centro Deportivo donde llevaba a mis mayores a hacer natación mientras ellos nadaban. Pero desde entonces no había encontrado el momento ni el lugar para retomarlo. Y lo estaba deseando! Dos años sin hacer ejercicio! El día que me crujió la espalda decidí que no podía dejar pasar más tiempo y me volví a apuntar.  
 Llevo varias sesiones y ya lo noto. Después de la cesárea los músculos de la zona abdominal se me quedaron algo atrofiados. Oye, que hasta cuando me río a carcajada limpia me dan tirones! Será posible?! Y la espalda machacada de tomar al bebé (de 12 Kg) en brazos, y el suelo pélvico, para qué contaros…

La mejor medicina para todo eso ha sido el pilates, sin duda.   
 Eso sí, a quien quiera hacerlo, aconsejo sesiones de 4 ó 5 personas como mucho, impartidas por alguien especializado que revise cada ejercicio con lupa, así como la respiración.   
 Yo practiqué en una clase multitudinaria con anterioridad. Y ahora que estoy en un grupo reducido me doy cuenta de que la mitad de los ejercicios no los ejecutaba correctamente, con el consiguiente riesgo de lesión. 

![image](/tumblr_files/tumblr_inline_p98q494e7l1qfhdaz_540.jpg)

Hacer los ejercicios bien requiere una gran consciencia del propio cuerpo, y coordinación, que se pueden adquirir con la práctica y una meticulosa supervisión por parte del instructor.

_“Los ejercicios están fundamentalmente compuestos por movimientos controlados, muy conscientes, y coordinados con la respiración, con el fin de crear un cuerpo armonioso, coordinado, musculado y flexible. A través de la práctica, la mente va tomando conciencia de las capacidades, limitaciones, fortalezas y debilidades del cuerpo para mejorar el estado físico y mental. Es una actividad física muy técnica, donde la correcta ejecución de los distintos elementos que componen cada ejercicio es más importante que el número de repeticiones._

_![image](/tumblr_files/tumblr_inline_p98q4anvpJ1qfhdaz_540.jpg)_

_El powerhouse, traducido en castellano como centro de poder, centro de energía, centro o neutro, fue situado por Pilates en la parte inferior del tronco, como una faja que rodea toda la zona lumbar y abdominal._

_La respiración pura también cumple un papel primordial en el método. Los resultados de la buena práctica son muy significativos: mayor capacidad pulmonar y mejor circulación sanguínea son los primeros fines perseguidos, para traducirlos en fuerza, flexibilidad, coordinación mental y buena postura._

_Se practica una respiración intercostal. Al inhalar se debe notar como las costillas se separan. En la exhalación, que suele coincidir con la mayor intensidad del ejercicio, se cierran primero las costillas y después se hunde el powerhouse, con la sensación de pegar el ombligo a la columna. Algunos profesores añaden a esto el cierre del perineo_.”([Wikipewdia](http://es.wikipedia.org/wiki/Pilates))

![image](/tumblr_files/tumblr_inline_p98q4b0iut1qfhdaz_540.jpg)

Mi Princesita se suele venir conmigo e imita las posturas, pero lo que mejor le viene es aprender a controlar su respiración, ya que le sirve para relajarse cuando no se puede dormir por las noches.  
  
 Yo, por mi parte, me siento nueva, hasta estoy más relajada y duermo mejor. Lo recomiendo sobre todo para mujeres que han dado a luz, después de un tiempo prudencial del parto o cesárea para recuperar la fortaleza del suelo pélvico y abdominal que ayuda tanto a la incontinencia urinaria, como a las relaciones sexuales.

Para mí, es una actividad imprescindible para la calidad de vida de la mujer. 

Y tú? Practicas Pilates?

![image](/tumblr_files/tumblr_inline_p98q4bBgdb1qfhdaz_540.jpg)

Fotos gracias a [The physiocompany](http://www.thephysiocompany.com)