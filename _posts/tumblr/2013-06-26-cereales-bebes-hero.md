---
date: '2013-06-26T14:45:00+02:00'
image: /tumblr_files/tumblr_inline_mozyu4Wlkg1qz4rgp.jpg
layout: post
tags:
- crianza
- trucos
- recomendaciones
- familia
- nutricion
- bebes
title: Alimentación complementaria para bebés. Prueba de cereales Hero Nanos
tumblr_url: https://madrescabreadas.com/post/53925711281/cereales-bebes-hero
---

![image](/tumblr_files/tumblr_inline_mozyu4Wlkg1qz4rgp.jpg)

Menuda revolución cuando saqué a la mesa los cereales que me envió Hero para probar. Se supone que eran para el bebé, pero mis dos hijos mayores, de 5 y 7 años también se apuntaron al desayuno del pequeño y se tomaron un buen tazón de leche con los nuevos cereales chocolateados con miel Hero Nanos.

Quise comprobar si realmente contienen menos contenido en azúcares que los demás cereales de desayuno con cacao del mercado, tal y como reza el evase. Pero como no pude verificarlo con todos los que existen, lo hice con los que tenía más a mano, que son los que toman normalmente mis hijos mayores, los Chocapic, de Nestlé y, efectivamente, tienen menos azúcar los Hero Nanos, con 15,7/100g, frente a los 26,3/100g de los Chocapic.

Además, siempre me gusta ver la cantidad de grasas saturadas de lo que le doy a los niños, ya que intento evitarlas en la medida de lo posible (aunque exterminarlas de la dieta es imposible, porque normalmente todo lo que les encanta las contiene en mayor o menor proporción, y prohibiendo algo he comprobado que se logra el efecto contrario, lo desean más).Y en este sentido parecen bastante más sanos los Hero Nanos, ya que contienen 0,4/100 g de grasas saturadas, frente a los 2,8/100 g de los Chocapic.

Me piden que los valore del 1 al 5 según los siguientes parámetros, siendo 1 la puntuación más baja, y 5 la más alta:

-Comodidad de uso/formato: Al ser formato diferente del resto de cereales para bebés, resulta más atractivo, más novedoso, incluso para los niños mayores. Y que tengan forma de números a los bebés no les importa demasiado porque no los entienden, pero a sus hermanos sí les ha llamado la atención.. Mi puntuación es 4/5

-Sabor: A mis niños les han encantado. Le doy un 4/5

-Aroma: el aroma es atractivo y suave. Al bebe le agrada. Mi puntuación. Es 5/5

-Relación calidad/precio: El precio es 2,99 € en la tienda on line de Hero, quizá algo caro porque la caja contiene poca cantidad. Mi puntuación es 2/5

NOTA:No os lo vais a creer, por lo casual del asunto, pero mientras escribía este post, mis hijos mayores han sacado los cereales que quedaban en la bolsa y se los han comido a escondidas. Mi primer impulso ha sido regañarles, pero luego he pensado que no era para tanto, y me ha hecho gracia la coincidencia.