---
date: '2014-12-17T08:05:00+01:00'
image: /tumblr_files/tumblr_inline_ngp1muasDc1qfhdaz.jpg
layout: post
tags:
- crianza
- premios
- colaboraciones
- ad
- moda
- familia
- bebes
title: 'Concurso Tu carta a los Reyes Magos: Tarjeta regalo 50€ de Ketekelo'
tumblr_url: https://madrescabreadas.com/post/105422778496/concurso-tu-carta-a-los-reyes-magos-tarjeta
---

ACTUALIZACIÓN 28-12-14

Con el fin de que los regalos lleguen a su destino a tiempo para Reyes  **se cerrará el concurso el 29-12-14 a las 8:00 am.**

Los cinco participantes cuyas cartas hayan obtenido más comentarios de personas diferentes tendrán que mandarme un email a  **mcabreada@gmail.com**  antes de las 12:00 am del día 29 con

-su nombre y apellidos, dirección y teléfono y email para el envío

Si algún participante no manda el email con su lista antes del 29-12-14 a las 12:00 el premio pasará al concursante cuya carta sea la siguiente en número de comentarios. Todo ello con la finalidad de que el envío de los regalos se haga lo antes posible.

ACTUALIZACIÓN 27-12-14

**SE ADELANTA LA FECHA TOPE PARA EL CONCURSO AL 29 -12**  
Me han propuesto adelantar la fecha de fin del concurso “Tu carta a los Reyes Magos” para que dé tiempo a que lleguen los regalos para esa noche tan especial, y creo que es muy buena idea. Así que se adelanta la fecha tope para subir vuestras cartas al lunes 29 de diciembre a las 00:00 h.

Vamos! Corre, corre!!

_Post originario:_

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

Uno de los regalos que podéis incluír en vuestra cata a los Reyes Magos para participar en el concurso es una tarjeta regalo por valor de 50 € de la tienda on-line [Ketekelo](http://www.ketekelo.es/) en la que puedes comprar ropa de 0 a dos años y cuando ya no le sirva a tu bebé te la re compran por hasta el 40% de su valor para que puedas adquirir otras prendas que vaya necesitando.

 ![image](/tumblr_files/tumblr_inline_ngp1muasDc1qfhdaz.jpg)

No sé a vosotros, pero a mí se me queda la ropa de los bebés nueva porque necesitan mucha ya que se manchan continuamente, y además cambian de talla con mucha frecuencia.

El resultado es que te juntas con un montón de ropa que, de repente , no te sirve. Y es una pena … Pues Ketekelo ha pensado la solución ideal, no crees?

Por eso ha querido que los lectores de mi blog prueben este sistema de ahorro en la ropa de lospeques, y han querido colaborar en el concurso Tu carta a los Reyes Magos cediéndonos una tarjeta regalo por valor de 50€ para que el ganador la gaste en ropita para bebés hasta 24 meses.

Para optar a este regalo tenéis que cumplir los 3 siguientes requisitos:

-

**Seguir a @Ketekelo**

en Twitter:

[aquí](https://twitter.com/ketekelo?lang=es)

-

**escribir que un tuit**

con el enlace de este post y el hastag #ConcursoKetekelo, por ejemplo: “Yo ya he pedido a los Reyes Magos mi tarjeta regalo de 50€ de ketekelo, ¿y tú? #ConcursoKetekelo (+ enlace de este post)

-Escribir** [ “Tu carta a Los Reyes Magos”](/portada-regalos-reyes-magos)** pinchando [aquí](/portada-regalos-reyes-magos)

Las condiciones para participar están en el [post explicativo del concurso.](/portada-regalos-reyes-magos)

Tenéis hasta el 5 de enero!