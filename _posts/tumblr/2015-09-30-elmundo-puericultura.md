---
date: '2015-09-30T12:19:54+02:00'
image: /tumblr_files/tumblr_inline_o39zbwcaBq1qfhdaz_540.jpg
layout: post
tags:
- crianza
- revistadeprensa
- colaboraciones
- puericultura
- ad
- familia
- bebes
title: Nos vamos a la Feria de Puericultura
tumblr_url: https://madrescabreadas.com/post/130193363294/elmundo-puericultura
---

Mañana asistiré como blogger invitada a la [Feria de Puericultura Madrid 2015](http://www.ifema.es/puericulturamadrid_01/) en IFEMA para empaparme muy bien de las novedades que nos ofrecen las mejores marcas del sector para este año, y después contároslo todo todito.

¡Aviso de que se avecinan muchas sorpresas y cosas interesantes en el blog!

Estoy muy ilusionada y deseando compartirlo con vosotros.

Ayer me hicieron una entrevista para el Diario El Mundo con motivo de la feria junto con otras compañeras blogueras, Sara Palacios, de [mamisybebes.com](http://www.mamisybebes.com/) y Mª José Cayuela, de [blogmodabebe.com](http://www.blogmodabebe.com/).

Un lujo compartir cartel con vosotras, chicas!

 ![](/tumblr_files/tumblr_inline_o39zbwcaBq1qfhdaz_540.jpg)

¡Nos vemos a la vuelta!

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.