---
date: '2016-12-07T13:34:18+01:00'
image: tumblr_files/tumblr_inline_ohte1puPEu1qfhdaz_540.jpg
layout: post
tags:
- puericultura
- crianza
- mis-premios
title: Puericultura Madrid entrega el premio “I love your Blog”
tumblr_url: https://madrescabreadas.com/post/154160035099/premio-puericultura-ifema
---

[Puericultura Madrid entrega el premio “I love your Blog” | Blog Oficial Puericultura Madrid que organiza Ifema](http://www.blogpuericulturamad.ifema.es/el-blog-madrescabreadas-com-recibe-el-premio-i-love-your-blog/)  

![](/tumblr_files/tumblr_inline_ohtef1b1541qfhdaz_540.png)

No os lo había contado por aquí, pero hace unas semanas [IFEMA me hizo entrega del III Premio I Love your Blog](http://www.blogpuericulturamad.ifema.es/el-blog-madrescabreadas-com-recibe-el-premio-i-love-your-blog/), al mejor blog de puericultura por la [guía de compras](/2016/10/09/puericultura-madrid-guia.html) que publiqué sobre mis recomendaciones tras la visita a la feria de puericultura de Madrid de este año 2016.

Se trata de una guía audiovisual donde explico los productos que más me gustaron, de entre las novedades que presentaron las marcas para el año que viene.

 ![](/tumblr_files/tumblr_inline_ohte1puPEu1qfhdaz_540.jpg)

Para mí este premio significa mucho porque siento un gran respeto por vosotros, quienes me leéis, y siempre procuro hacerlo lo mejor posible para daros un contenido de calidad, aunque a veces es duro sacar tiempo o fuerzas cuidando de tres fieras.

Va por vosotros!

Quiero agradecer a IFEMA que haya elegido mi blog entre los que se presentaron al concurso, y la atención recibida durante mi estancia en Madrid para el acto de entrega del premio.

Me siento muy orgullosa de estar a la altura de mis predecesoras Sara, de [Mamis y Bebés](http://www.mamisybebes.com/), y Johannes Ruiz, de [Mimitos de Mamá](https://mimitosdemama.es/), dos grandes profesionales a las que admiro y quiero mucho.

¡Ahora, a seguir trabajando para no bajar el listón!

Gracias de corazón por leerme y darme cada día ilusión para seguir escribiendo.