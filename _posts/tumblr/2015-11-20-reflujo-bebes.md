---
layout: post
title: Tips para aliviar el reflujo de tu bebé
date: 2015-11-20T13:05:46+01:00
image: /tumblr_files/tumblr_inline_o39z22x3861qfhdaz_540.jpg
author: .
tags:
  - crianza
  - trucos
  - lactancia
  - nutricion
  - salud
  - bebes
tumblr_url: https://madrescabreadas.com/post/133588780489/reflujo-bebes
---
Casi todos los bebés sufren regurgitaciones, lo que se suele llamar echar bocanadas de leche; de vez en cuando al ratito de haber terminado la toma, incluso algunos hacen un guiño porque lo que expulsan les sabe agrio.\
\
Esto es normal y no siempre tiene que asociarse al reflujo del bebé, ni tiene porqué ser un reflujo ácido. Pero en nuestro caso sí que lo fue con mi tercer hijo, que estuvo en tratamiento farmacológico del reflujo con ranitidina y posteriormente, en tratamiento del reflujo con esomeprazol “Nexium”, un derivado del omeprazol. Así que en este post os voy a contar mi experiencia con **el reflujo silencioso en bebés**: síntomas y tratamiento.

Es conveniente **dormir cerca del bebé para vigilar los posibles atragantamientos** mientras duerme. En nuestro caso practicamos el [colecho](https://madrescabreadas.com/2022/02/16/colecho-seguro/) con esta cuna muy económica que fabricamos nosotros mismos a partir de una cuna de Ikea, y con unos sencillos materiales. Aquí te explico [cómo fabricar tu propia cuna de colecho a partir de una cuna de Ikea](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/)

## ¿Qué provoca el reflujo en bebés?

Algunos bebés padecen lo que se llama [reflujo gastroesofágico](https://madrescabreadas.com/2022/01/11/reflujo-bebes-sintomas/), que puede darse en mayor o menor grado. Esto se debe, por lo general, a la inmadurez del sistema digestivo, que hace que una válvula no se cierre del todo. El alimento, cuando está digiriéndose en el estómago vuelva a subir por el esófago, **llegando incluso a producirse el vómito en los casos más severos,** y a lesionarlo por la acción abrasiva de los ácidos que intervienen en el proceso digestivo. (Lo he explicado tal y como yo lo entendí, y creo que lo pueden entender otros padres, que me perdonen los médicos las inexactitudes).

![](https://d33wubrfki0l68.cloudfront.net/32146dd05401e0d8bd29bd230509d3359adf3da2/70fe7/tumblr_files/tumblr_inline_o39z22x3861qfhdaz_540.jpg)

En nuestro caso, a Leopardito le diagnosticaron reflujo gastroesofágico en grado moderado cuando tenía 2 meses. Tuvieron sospechas de esofagitis, pero quedó descartada tras una prueba.

## ¿Qué es el reflujo silencioso en bebés?

Mi bebé no llegaba a vomitar, pero **le dolía mucho cada vez que el alimento le subía del estómago hacia el esófago** (reflujo silencioso), lo que le impedía dormir y, en general, era una odisea cada vez que lo teníamos que tumbar, por ejemplo, para cambiarle el pañal.

### ¿Cómo saber si tu bebé tiene reflujo silencioso?

Si el bebé vomita mucho y llora cuando lo hace, es fácil darse cuenta, pero si no vomita, a veces es complicado percatarse porque no tenemos ninguna señal externa que nos lo indique, salvo que llora mucho. Claro, los bebés pueden llorar por muchas causas.

En nuestro caso nos dimos cuenta por varias cosas:

* El bebé se atragantaba al mamar

**Se atragantaba al mamar** con frecuencia y tosía. Prácticamente en cada toma.

* El bebé lloraba mucho cuando lo tumbábamos

**Lloraba mucho cuando lo tumbábamos** para cambiarle el pañal.

* El bebé se despertaba continuamente por la noche llorando

**Se despertaba continuamente por la noche llorando**, normalmente, el primer despertar era al ratito de haber terminado la toma.

* El bebé no toleraba la posición “cuna” para mamar

**No toleraba la [posición “cuna”](http://lactanciamaterna.aeped.es/tecnicas-de-lactancia-materna/) para mamar**, creemos que porque le presionaba el estómago.

## ¿Cómo curar el reflujo en los bebés?

En uno de los atragantamientos que sufrió acudimos a urgencias y le realizaron una **prueba de deglución** en la que le diagnosticaron reflujo moderado y le prescribieron un tratamiento con un antiácido llamado ranitidina, de fórmula magistral, a fabricar en la farmacia.

"Actualmente [los medicamentos vía oral con ranitidina están retirados del mercado](https://www.aemps.gob.es/informa/notasinformativas/medicamentosusohumano-3/2019-muh/retirada-del-mercado-de-medicamentos-que-contienen-ranitidina-via-oral/)."

Por lo general se les suele practicar una prueba llamada Peachimetría, pero en nuestro caso, tras consultar varias opiniones, no consideramos necesaria hacérsela.

### Tratamiento farmacológico del reflujo en bebés

La ranitidina, que actualmente está retirada del mercado, tenía el inconveniente de que perdía su eficacia a los pocos días de su elaboración por el farmacéutico, así que no nos funcionó muy bien, y acudimos a un [especialista de gastro](https://madrescabreadas.com/2013/05/03/visita-al-pediatra-muy-rebelde-a-la-exploraci%C3%B3n.html) quien nos cambió el tratamiento por otro a base de esomeprazol, un derivado del omeprazol, que viene en polvos dentro de unos sobres (Nexium).

La dosis para bebés no estaba comercializada entonces, así que tuvimos que medir nosotros qué cantidad le dábamos en cada toma, **siempre siguiendo las instrucciones de su médico.**

Este último tratamiento le fue mejor, pero no fue la panacea, ya que su alimentación era líquida todavía, y a la hora de dormir lo pasaba el pobre muy mal.

### ¿Cómo curar el reflujo de manera natural?

En el hospital nos dieron unas sencillas recomendaciones basadas en un **cambio de hábitos básicamente posturales** que mejoraron bastante los síntomas de reflujo gastroesofágico.

Siempre bajo la supervisión de su pediatra y complementando estas recomendaciones posturales con el tratamiento farmacológico.

#### Usar un portabebés ergonómico para paliar el reflujo

Hay que procurar **tumbarlo lo menos posible**, así que os recomiendo trasladarlo, en lugar de en un cochecito, en un portabebés ergonómico.

Os **recomiendo por experiencia propia los de Manduca,** ya que se pueden usar desde el nacimiento y duran bastante tiempo, dependiendo del tamaño y peso del bebé. Es decir, es un portabebés que dura bastante, y su material es lo suficientemente blandito para que el bebé se encuentre cómodo en él desde los primeros días de vida.

[![](/images/uploads/manduca-first-baby.jpg)](https://www.amazon.es/dp/B077D5W5SX/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=8cc64cd5ea2181eea893b954fec2ff74&creativeASIN=B077D5W5SX&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B077D5W5SX/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=8cc64cd5ea2181eea893b954fec2ff74&creativeASIN=B077D5W5SX&th=1)

Además, tiene un suplemento para sujetar la cabecita. Es adaptable a cualquier cuerpo, así que lo puede usar también el papá u otra persona. Recomiendo **usar el portabebés a última hora de la tarde**, cuando los bebés empiezan a llorar sin motivo aparente y a ponerse incómodos. Os aseguro que os va ahorrar fatigas.

#### Usar un carrito de bebé con colchón elevable

De todos modos, si lo ponéis a ratos en el cochecito, mirad que se pueda elevar la parte de la cabeza, y si no, **ponedle debajo del colchón toallas** para dejarlo en cuña de un ángulo de unos 20º, igual que os he recomendado para la cuna.

Si vas a comprar un coche de bebé, **asegúrate de que tiene la opción de levantarle la parte de la cabeza** y poder regularlo.

#### Usar una hamaca para aliviar el reflujo

En casa, tenerlo en hamaquita es mejor que en la cuna todo el rato. Nosotros testeamos esta hamaca ajustable en altura de Babymoov para que nuestra espalda no sufriera, que además es ergonómica y muy cómoda para el bebé, y nos dio muy buen resultado.

Te dejo la [reseña de la hamaca Swoonup de Babymoov aquí](https://madrescabreadas.com/2015/11/24/hamaca-swoonup-babymoov/)

[![](/images/uploads/babymoov-swoon.jpg)](https://www.amazon.es/gp/product/B00N8YZ7CG/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B00N8YZ7CG&linkId=6820448ac7da74375a04c26ec444dc2d&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B00N8YZ7CG/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B00N8YZ7CG&linkId=6820448ac7da74375a04c26ec444dc2d&th=1)

#### Cambiar el pañal medio incorporado

Cambiar el pañal es un momento crítico. Procurad hacerlo rápido, y elevarle **siempre la cabeza por encima del tronco.**

Yo, en cuanto aprendió a andar, desarrollé una habilidad especial para **cambiarle el pañal de pie**. Así, un problema menos.

### ¿Cómo amamantar a un bebé con reflujo gastroesofágico?

Mi pequeño se alimentaba con lactancia materna exclusiva, así que intentaba que no comiera demasiado en cada toma. En mi caso, sólo le ofrecía un pecho, no los dos. Esto hacía que comiera con más frecuencia, pero cantidades más pequeñas.

Modifiqué mi [postura de amamantamiento](http://albalactanciamaterna.org/lactancia/claves-para-amamantar-con-exito/posturas-y-posiciones-para-amamantar/) de la posición cuna a la posición biológica. Es decir, yo, recostada hacia atrás, y él sobre mí, con lo que lográbamos que estuviera con el estómago estirado, no encogido como antes, y su tronco inclinado, no horizontal, de manera que **la cabeza siempre quedaba por encima del estómago**.

![](https://d33wubrfki0l68.cloudfront.net/ffebc5bff97b46515942809fc151c8e468aa9bb1/38e52/tumblr_files/tumblr_inline_o39z23lsjd1qfhdaz_540.jpg)

He de decir que si no llega a ser por el servicio ALMA (Atención a la Lactancia Materna) de la Arrixaca de Murcia no hubiera logrado seguir amamantando a mi hijo, porque llegó a rechazar el pecho, ya que lo asociaba con el dolor. Muchas gracias, Mónica.

Tras las tomas, posición vertical. Normalmente les va genial un [portabebés ergonómico](http://www.amazon.es/gp/product/B00Q9C0T7M/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00Q9C0T7M&linkCode=as2&tag=madrescabread-21), pero en el caso de Leopardito, rechazó todos y cada uno de los que probé, y eso que tengo amigas asesoras de porteo que me aconsejaron genial. Pero a mi bebé le iba mal porque le hacía vomitar. Pero ya os digo, que en general va fenomenal, así que probadlo.

![bebe comiendo naranja](https://d33wubrfki0l68.cloudfront.net/975b14d0d7b3d8d34697f4f13abc6d2558bd73e0/e0015/tumblr_files/tumblr_inline_o39z24ip7a1qfhdaz_540.png)

En cuanto a la mejor leche de fórmula para el reflujo, existen el el mercado **leches específicas más espesas cuyo objetivo es reducir el reflujo**. Os dejo este post sobre [comparativas de distintas marcas de leche de fórmula](https://madrescabreadas.com/2021/04/13/cu%C3%A1l-es-la-mejor-leche-de-f%C3%B3rmula/).

## ¿Cómo evitar el reflujo en bebés?

Cuando se empiezan a introducir alimentos diferentes a la leche, a mí me fue muy bien no triturarlos, sino, como mucho, machacarlos. El método [Baby lead weaning todo terreno](https://madrescabreadas.com/2013/11/26/alimentaci%C3%B3n-complementaria-sin-papillas-para.html), como yo lo llamo, y que ya os expliqué, fue el que practicamos.

Hay que procurar **evitar los alimentos ácidos**, como las naranjas (lástima porque le encantaban como se aprecia en la foto), el tomate…

![bebe hamaca](https://d33wubrfki0l68.cloudfront.net/6b42b7456864cae93a292341e74d40f6b360c1c6/56fdb/tumblr_files/tumblr_inline_o39z24f87n1qfhdaz_540.png)

El chocolate también les suele sentar bastante mal.

Pero **cada niño es diferente, y hay que probar qué alimentos les sientan mal para eliminarlos de la dieta.**

Por ejemplo, en nuestro caso, el yogur no le sentaba bien, sin embargo, le iba genial la patata cocida, la manzana, el plátano…

Las cenas deben ser muy suaves, y evitar el tomate frito.

Aconsejo **llevar un diario donde anotemos cada día lo que come** y vayamos aislando los alimentos que le sienta mal creando una especie de lista negra.

## ¿Cómo poner a dormir a un bebé con reflujo?

Éste es el caballo de batalla de los niños con reflujo ya que lo más fácil sería colocarlo de lado o boca abajo para evitar ahogamientos, pero esto choca con aposición recomendada por los pediatras a día de hoy, que es boca arriba.

En cualquier caso, **la posición totalmente horizontal para dormir se debe evitar** para prevenir atragantamientos y siempre debe tener el torso más elevado que el estómago.

Mi consejo es que el bebé duerma cerquita nuestro para que podamos estar pendiente de eventuales vómitos y atragantamientos.

### **Una hamaca**

Mi criterio, aunque yo no lo hice porque reaccioné demasiado tarde después de probar varias cosas, es **que el peque se acostumbre desde el principio a dormir en una hamaca.**

![nino en hamaca nicaragua hecha a mano](https://d33wubrfki0l68.cloudfront.net/be04da9c9c2e0f9fbe78f45b111bfb5e5d0288c6/3ff8c/tumblr_files/tumblr_inline_o39z25zwzo1qfhdaz_540.jpg)

Las hay en el mercado de varias clases, y creo que **en ellas la posición que adopta el bebé es la ideal** para minimizar las subidas de alimento del estómago al esófago ya que la cabeza siempre queda por encima del estómago.

### **Cuna con colchón elevable**

Hay cunas que tienen la posibilidad de elevar el colchón por la parte de la cabezada bebé.

[![](/images/uploads/chicco-next2me3.jpg)](https://www.amazon.es/dp/B08KFKGQ1C/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=6ec4f49913299502ce7cdbb8260851b7&creativeASIN=B07GJGJDND&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B08KFKGQ1C/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=6ec4f49913299502ce7cdbb8260851b7&creativeASIN=B07GJGJDND&th=1)

Otra opción es **poner debajo del colchón una cuña o cojín antireflujo** de un ángulo de unos 20º, o meter toallas para elevarlo, y poner una enrollada a modo de cilindro debajo del culete del bebé para hacer de tope, pero por debajo de la sábana para que no se mueva, y así evitar que se vaya resbalando hacia abajo.

Al final siempre acaba mal puesto y escurrido, sobre todo cuando va siendo mayor y teniendo más movilidad.

Ésta es la opción que tomamos nosotros, aunque ya os digo, que si volviera atrás, seguramente probaría con una hamaca.

Si quieres ahorrarte los gastos de envio de los productos que te recomiendo te dejo este trucazo para que te lleguen al día siguiente sin gastos de envío. Atenta: Puedes aprovecharte durante 1 mes de la [prueba gratuita de Amazon Prime](www.amazon.es/pruebaprime?tag=madrescabread-21) , que te da envíos urgentes gratuitos en un montón de productos (yo, una vez pedí una cosa por la noche antes de dormirme, y al día sigueinte me despertó el repartidor a las 9:00 de un timbrazo; me quedé muerta...)

[![](/images/uploads/pekitas-cojin-antireflujo.jpg)](https://www.amazon.es/dp/B08N5CBGD7/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=2942be239650abb711e8c328bc0b3c6e&creativeASIN=B08N5CBGD7&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B08N5CBGD7/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=2942be239650abb711e8c328bc0b3c6e&creativeASIN=B08N5CBGD7&th=1)

### **Elevar las patas traseras de la cuna**

Se pueden poner unos elevadores, tacos de madera, o lo que so os ocurra, para **elevar ligeramente la parte de atrás de la cuna**, donde el bebé apoya su cabeza al dormir.

### **Posición de la madre amorosa**

La cuarta opción es la de la madre amorosa, aunque no creo que se pueda aguantar toda la noche por la madre (o padre) pero sí un ratito, al menos el sueño después de la toma.

![bebe con mama](https://d33wubrfki0l68.cloudfront.net/1fa7d14ff34582cef2e048fe9ed15db4b77f4849/0d682/tumblr_files/tumblr_inline_o39z25osv71qfhdaz_540.png)

Consiste en **poner al bebé sobre nuestro pecho, y permanecer semi incorporadas** en la cama con almohadones en la espalda e intentar dormir así nosotras, porque ellos desde luego, así sí que duermen a pierna suelta.

Ésta opción también la practicamos bastante.

## ¿Cuándo desaparece el reflujo en los bebés?

Salvo casos excepcionales, en los que la solución es la cirugía, se suele curar con el tiempo, cuando el sistema digestivo termina de madurar, **sobre los 2 años de edad aproximadamente.**

Nuestro Leopardito tiene ya es un niño mayor que no tiene ningún síntoma de reflujo. A los 3 años y medio y, salvo alguna molestia ocasional, sobre todo cuando dormía la siesta, o por las noches, ya estaba como una rosa, y ya podía comer chocolate, que le encanta (como a su madre). En la foto, un cumpleaños poniéndose hasta las cejas literalmente.

![fuente chocolate ninos](https://d33wubrfki0l68.cloudfront.net/e1b30bfb64eb0cff57e9e86b4cb45998d74a4136/02c78/tumblr_files/tumblr_inline_o39z26yyd21qfhdaz_540.png)

Si sospechas que tu bebé padece de reflujo, **lo mejor es que lo comentes con su pediatra par salir de dudas**, y si se confirma, la mejor medicina es la paciencia y el amor porque seguramente se terminará curando con el tiempo.

Más información: [Las 4 respuestas que estabas buscando sobre el reflujo de tu bebé](https://madrescabreadas.com/2022/01/11/reflujo-bebes-sintomas/).

¿Me cuentas tu experiencia con el reflujo?

Si te ha gustado compártelo, así ayudarás a más familias y también a que sigamos escribiendo este blog.

Suscríbete para no perderte nada.