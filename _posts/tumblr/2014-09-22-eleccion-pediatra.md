---
date: '2014-09-22T10:00:28+02:00'
image: /tumblr_files/tumblr_inline_pee3e3n7il1qfhdaz_540.jpg
layout: post
tags:
- crianza
- derechos
- familia
- salud
- bebes
title: ¿Tengo derecho a elegir el pediatra de mi hijo?
tumblr_url: https://madrescabreadas.com/post/98131979835/eleccion-pediatra
---

Una de las cuestiones que más preocupan a los padres y madres cuando tienen un bebé es el pediatra que los atenderá a lo largo de su infancia. No siempre estamos de acuerdo con el facultativo que nos asigna el sistema público de salud por diversos motivos.

## ¿Qué podemos hacer si no nos gusta el pediatra que nos han asignado?

La libre elección de facultativo de atención primaria está garantizada por la Ley General de Sanidad 14/86 de 25 de abril en su art. 10.13, para núcleos de población superiores a 250.000 habitantes, la elección se podrá hacer dentro del conjunto de la localidad, y para los núcleos de población de menos de 250.000 habitantes, se podrá optar por profesionales dentro del área de salud.

![](/tumblr_files/tumblr_inline_pee3e3n7il1qfhdaz_540.jpg)

## Elección de pediatra según la edad del niño

Para el caso de los niños, el Real Decreto 1575/93, de 10 de septiembre, que desarrolla la Ley anterior, establece la siguiente casuística:

-Los menores de 7 años pueden elegir entre los pediatras de su zona.

-Los niños de 7 años en adelante, hasta los 14, pueden elegir entre médico general o pediatra de los de su zona.

-Los niños de 14 años o más, pueden elegir entre los médicos generales de su zona.

Queda bien entendido que la elección se hará por sus representantes legales.

## ¿Pero qué sucede en las zonas donde no haya asignado un pediatra por su escasa población infantil?

-Los menores de 14 años podrán elegir entre médicos generales de su zona básica de salud, pediatras de su área de salud o pediatras del núcleo de residencia si éste supera los 250.000 habitantes.

-los niños de 14 años en adelante, sólo podrán optar a médicos de medicina general.

## ¿Es necesario alegar un motivo justificado para cambiar de pediatra? 

En todos los casos la elección se puede llevar a cabo en cualquier momento y sin necesidad de justificación, incluso tenemos derecho a una entrevista con el facultativo antes de elegirlo.

## Los motivos del pediatra para rechazar un paciente pueden ser:

-Cuando el cupo del profesional supere el número óptimo (pediatras: 1.250-1500) médicos generales 1.250 -2.000, aunque en ambos casos se puede llegar a incrementar en un 20%)

-Cuando existe una razón justificada y aprobada por la Inspección médica-

-Cuando el facultativo no esté destinado en la zona básica de salud del paciente. En cuyo caso se requiere la previa conformidad del mismo, con el fin de asegurar la asistencia domiciliaria.

## ¿Qué hacer si nos rechazan la elección de pediatra?

En este caso, a falta de regulación específica del proceso de reclamación en cada Comunidad Autónoma, se aplicaría el régimen general común a todas las reclamaciones administrativas en general de la Ley 30/92 de 26 de noviembre. Esto quiere decir que, en primer lugar, deberemos solicitar la elección por escrito formal obteniendo copia sellada. En segundo lugar, exigir una respuesta motivada por escrito. Y en tercer lugar, cuando la tengamos, interponer un recurso ante el órgano competente.  Caso de que no obtengamos contestación por escrito, también podremos interponer el recurso.

Lo que hemos examinado vale para aquellas Comunidades autónomas que no tengan un régimen específico para la elección de pediatra, y para completar el régimen de las que sí lo tengan, pero no contemplen algunos aspectos de los anteriores. Pero algunas otras, tienen normas propias para ello, aplicables sólo en el ámbito geográfico de la Comunidad en cuestión.

En próximas entregas analizaremos la normativa específica de la Comunidad de Madrid sobre el procedimiento a seguir para la elección de pediatra.

¿Te gusta tu pediatra?