---
date: '2015-04-01T08:00:39+02:00'
image: /tumblr_files/tumblr_inline_nlbc6zTcnY1qfhdaz.png
layout: post
tags:
- premios
- planninos
- familia
title: 'Viajar con niños: Concurso Segovia turismo familiar'
tumblr_url: https://madrescabreadas.com/post/115190216483/viajar-ninos-segovia
---

ACTUALIZACIÓN

## Por decisión de Turismo de Segovia y de este blog, queda ampliado el plazo para participar en el concurso hasta el 30-4-15.

La emblemática ciudad de Segovia ha obtenido recientemente el [Sello Turismo Familiar](http://www.familiasnumerosas.org/notas-de-prensa/notas-de-prensa/detail/la-fefn-firma-un-convenio-con-turismo-para-impulsar-el-sello-de-turismo-familiar/) que otorga la [Federación Nacional de Familias Numerosas](http://www.familiasnumerosas.org/turismo-familiar/) a emplazamientos turí­sticos, hoteles, centros culturales, restaurantes, etc., que tengan instalaciones, oferta de actividades o entorno pensados para las familias. 

 ![image](/tumblr_files/tumblr_inline_nlbc6zTcnY1qfhdaz.png)

Ya os hablé aquí­ de la [creación de este Sello de Turismo Familiar](/2014/07/28/vacaciones-en-familia-con-el-sello-de-turismo.html), que nos ofrece a las familias un interesante servicio al localizar e identificar los mejores lugares para disfrutar del ocio y el tiempo libre con niños. De esta forma, cuando veamos el distintivo de ““Turismo Familiar” sabremos que estamos en un lugar adecuado y orientado especialmente para nosotros.

 ![image](/tumblr_files/tumblr_inline_nlbc9fSH7W1qfhdaz.jpg)

Segovia tiene una amplia oferta de actividades para todas las edades para realizar en familia, es por ello que ha obtenido este distintivo.

 ![image](/tumblr_files/tumblr_inline_nlbce9fbwu1qfhdaz.jpg)

Algunas de las actividades más divertidas son:

## Visita guiada por el arriero Claudio

Para descubrir Segovia de la mano del arriero más entrañable y famoso de la ciudad, Claudio

Este divertido personaje os guiará por la historia de Segovia a través de acontecimientos, leyendas, juegos y muchas más sorpresas que tendrán lugar a lo largo de todo el recorrido.

[Consultar las fechas de la visita guiada por el arriero Claudio](http://ninos.turismodesegovia.com/es/noticias/generales/353-el-arriero-claudio-te-guia-por-segovia).

## Con ojos de poeta

El sábado 25 de abril a las 12:00 h, en la Casa-Museo de Antonio Machado, podréis disfrutar de un espectáculo único en el que recorreréis la vida y obra del poeta a través de un personaje de ficción y música en directo.

## El arte de la cetrería

Aves rapaces procedentes de todos los rincones del mundo llegan a Segovia para mostraros el milenario arte de la cetrería, Patrimonio Cultural Inmaterial de la Humanidad.

Podrás incluso fotografiarte con un ave rapaz

[Consultar fechas de la actividad de cetrería](http://ninos.turismodesegovia.com/es/actividades/actividades-para-familias-con-ninos/486)

## Una aventura en la judería

Un misterioso personaje se esconde en las calles del barrio judío. Se trata de Bimba, que acaba de regresar a Segovia en busca de algo que dejó olvidado cuando tuvo que marcharse a otro país hace muchos años. Â¿Te gustaría ayudar a Bimba a encontrar “su tesoro”?

Juegos, historias y música nos acompañarán en este divertido recorrido por la Judería.

[Consultar fechas de la actividad Una aventura en la judería](http://ninos.turismodesegovia.com/es/actividades/actividades-para-familias-con-ninos/434)

Aquí podéis consultar el [folleto completo de actividades de la ciudad de Segovia](http://ninos.turismodesegovia.com/es/documentos).

Este año, como novedad, también han publicado una nueva [gymkhana gratuita denominada Descubre los museos de Segovia](http://ninos.turismodesegovia.com/es/juega-con-nosotros/descubre-los-museos-de-segovia), con el objetivo de que las familias conozcan aún mejor esta histórica ciudad.

Además, están buscando [una mascota para Segovia](http://ninos.turismodesegovia.com/es/noticias/generales/488), que la identifique como destino de Turismo Familiar.

Cómo mola Segovia, ¿eh? No me extraña que le hayan dado el Sello de Turismo Familiar! Por eso han querido celebrar esta distinción con un concurso muy especial a través de este blog dirigido a familias con niños que quieran disfrutar en ella.

**Gana entradas para las actividades**

Para que una familia tenga la oportunidad de participar en una de estas actividades, este blog ha organizado un concurso en colaboración con Turismo de Segovia. Para participar hay que hacerlo a través de Twitter donde tenéis que publicar una foto o dibujo de algún monumento o rincón de la ciudad de Segovia que os guste, utilizando el hastag 

**[#ConcursoSegoviaFamiliar](https://twitter.com/hashtag/ConcursoSegoviaFamiliar?src=hash)** 

Así­ de sencillo!!

El premio consistirá en un **pack de entradas familiar** para dos adultos y todos los hijos de la familia para **una de las actividades** que os he explicado anteriormente, a elegir por el ganador (será necesario exhibir el libro de familia), más un pack de publicaciones infantiles que contiene:

Un **álbum de cromos sobre el Acueducto**

 ![image](/tumblr_files/tumblr_inline_nlbcuvXePK1qfhdaz.jpg)

La **Guí­a para niños y padres**  

 ![image](/tumblr_files/tumblr_inline_nlbcvdaHZj1qfhdaz.jpg)

y **Segovia en Colores** , para los más pequeños

 ![image](/tumblr_files/tumblr_inline_nlbcvu4o9m1qfhdaz.jpg)

además de las **Gymkhanas** para conocer los museos.

El ganador será elegido por un jurado compuesto por miembros de Turismo de Segovia, y se valorará la originalidad.

El plazo para concursar finalizará el 20 de abril, y el resultado se hará público en este blog en los días sucesivos.

Te animas?

Sube tu foto a Twitter con el hastag #ConcursoSegoviaFamiliar y participa!