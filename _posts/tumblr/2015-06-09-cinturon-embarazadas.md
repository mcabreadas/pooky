---
date: '2015-06-09T19:44:14+02:00'
image: /tumblr_files/mMCs6wG.jpg
layout: post
tags:
- crianza
- seguridad
- embarazo
title: Cinturón de seguridad y embarazadas
tumblr_url: https://madrescabreadas.com/post/121118964739/cinturon-embarazadas
---

Ya tenemos la cabeza en las vacaciones, y es que a más de uno nos quedan pocas semanas para evadirnos de la rutina diaria y cambiar de aires. La mayoría de los desplazamientos los haremos por carretera, por eso he querido tener una especial atención con las embarazadas que me seguís, porque me he dado cuenta de que no todas os colocáis el cinturón de seguridad de forma correcta.

La correcta colocación del cinturón de seguridad de una mujer embarazada podría evitar riesgos para el feto. Fijaos si es importante lo que os voy a contar.

En España los accidentes de tráfico ocasionan la interrupción de entre 200 y 700 embarazos cada año según la Fundación Mutual de Conductores. La conducción durante el embarazo es un tema que se debería tomar en serio debido  al alto riesgo de daños irreversibles para el feto.

Matrona y Neurópata Marina Fernández Martín informa que “el 50% de las muertes fetales en accidentes de tráfico se producen por mal uso del cinturón por parte de la mujer embarazada.” Por lo tanto, estos fallecimientos pueden ser evitados simplemente con la educación e información adecuada.    

El grupo de seguros Halo ha creado una guía práctica para mujeres embarazadas, que os recomiendo seguir, y difundir para que llegue al mayor número de embarazadas:

 ![image](/tumblr_files/mMCs6wG.jpg)[Fuente de la infografía](https://www.aseguramicochedealquiler.es/centro-de-informacion/blog/el-riesgo-de-conducir-embarazada-guia-para-mujeres)  
  

Yo, además, os recomiendo usar un [Adaptador Cinturón para Embarazadas](http://www.amazon.es/gp/product/B0014BOG1I/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B0014BOG1I&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B0014BOG1I) que sujeta la banda que queda por debajo de la barriga por abajo, de manera que queda entre los muslos, evitando cualquier presión en el vientre en caso de que ésta se tense bruscamente por cualquier motivo.

Os recomiendo usarlo 100%, merece la pena. Yo lo he amortizado de sobra con mis tres embarazos, sobre todo a partir del tercer o cuarto mes de gestación, cuando ya se nota la barriguita, y además, lo he prestado a varias amigas y familiares porque es muy resistente. 

[Éste es el mío,](http://www.amazon.es/gp/product/B0014BOG1I/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B0014BOG1I&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B0014BOG1I) pero podéis usar cualquiera que esté homologado

No te la juegues, y ya sabes, como cantaba Silvia Padilla, en el programa de televisión Factor X: “Ponte el cinturón, protege su vida, su seguridad es muy importante”

<iframe width="100%" height="315" src="https://www.youtube.com/embed/XdtVfi9LGsQ" frameborder="0"></iframe>

¿Estás de acuerdo?