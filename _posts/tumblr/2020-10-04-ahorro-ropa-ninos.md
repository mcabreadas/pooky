---
date: '2020-10-04T16:11:26+01:00'
image: /tumblr_files/tumblr_inline_pc7s7f7DWP1qfhdaz_540.jpg
layout: post
tags:
- crianza
- hijos
- trucos
- colaboraciones
- ad
- familia
- moda
title: Familias numerosas. 7 trucos para ahorrar en la ropa de los niños.
tumblr_url: https://madrescabreadas.com/post/112226203989/ahorro-ropa-ninos
---

En tiempos de crisis las familias numerosas desarrollamos nuestra imaginación a tope para frenar el gasto y ahorrar en todo lo que podemos. Es cierto que lo tenemos más difícil que una familia con menos hijos, pero no es imposible.

Una de las partidas del presupuesto familiar que se pueden bajar más fácilmente es el gasto en ropa de niños.

Si vuestros hijos tienen primos mayores lo tenéis muy fácil, pero en nuestro caso, son los primeros niños de la familia, y vamos abriendo camino… por eso hemos tenido que buscar otros trucos.

## Herencia entre hermanos

La suerte es que la ropa de los niños pequeños se usa durante muy poco tiempo porque crecen rápido, y la mayoría se queda casi nueva. Una buena clasificación por tallas y género, será crucial para luego poder usarla con los hermanos que les siguen. Las **cajas** de plástico duro transparente apilables, **etiquetas** , y un buen desván o trastero son una buena solución.

 ![image](/tumblr_files/tumblr_inline_pc7s7f7DWP1qfhdaz_540.jpg)

Foto gracias a [https://childgearreviews.wordpress.com](https://childgearreviews.wordpress.com)

## Quitamanchas

Un quitamanchas también es importante para quitar las que salen con el paso del tiempo en la ropa, aunque la hayamos guardado limpia.

 ![image](/tumblr_files/tumblr_inline_pc7s7gEJYE1qfhdaz_540.jpg)
## Máquina para quitar bolas
.Otro imprescindible es una máquina para quitar bolas. Os sorprenderá cómo quedan los jerseys de lana tras una pasada con la maquinita. ¡Quedan como nuevos!  Y no son caras. Las hay de dos clases: las que van a pilas, y las que llevan cable. Os aconsejo que lleve cable, porque las pilas se gastan enseguida, y no ganas para cambiarlas cada dos por tres. ![image](/tumblr_files/tumblr_inline_pc7s7grqUd1qfhdaz_540.jpg)
## Rodilleras en los pantalones

Las descubrí hace poco, porque mi mediano le dio por tirarse al suelo jugando al fútbol, y me destrozó tres pantalones en 2 semanas. Después de la taquicardia inicial se me ocurrió una solución de toda la vida, poner unas rodilleras. Las compré adhesivas, pero lo mejor es, además de pegarlas con la plancha, coserlas alrededor porque si no se despegan, al menos en mi caso. Oye, y pantalones nuevos!.

 ![image](/tumblr_files/tumblr_inline_pc7s7ggY6D1qfhdaz_540.jpg)
## Abusar de los pantalones cortos

Cuando llegue el buen tiempo, si los cortamos, tendremos unas bermudas estupendas. Por aquí tenemos suerte porque el invierno es corto.

Yo procuro llevarlos la mayor parte del tiempo con pantalones cortos porque se estropean menos, sobre todo al más pequeño, que va tan mono, incluso en invierno, con sus leotardos.

 ![image](/tumblr_files/tumblr_inline_pc7s7hfWnE1qfhdaz_540.jpg)
## Visitar mercadillos para comprar básicos

5 pares de leotardos por 10 € me llevé yo el otro día, camisetas térmicas interiores a 2,50 €, o 6 braguitas de algodón por 10 € son sólo algunos ejemplos. 

No os imagináis el ahorro en ropa interior que supone cuando tienes que comprar para 3 ó más niños y niñas.

 ![image](/tumblr_files/tumblr_inline_pc7s7hClzj1qfhdaz_540.jpg)
## Las rebajas

Es cuestión de cogerle el truco a la evolución de la talla de tus hijos, pero aprovechar esta época para comprar la ropa del año siguiente es mi mejor truco. 

Cada niño es diferente, los habrá que aumenten una talla, y los habrá que aumenten dos. El ojo de la madre es el mejor consejero. Quizá la primera vez os equivoquéis, pero le vais a coger el truco, y el gusto. Además, de un año a otro no cambia tanto la moda, e irán impecables.

Lo que no tiene precio es encontrarte de repente con el frío y no tener que salir corriendo a comprar abrigos a precio de temporada arruinando tu bolsillo, cuando has sido previsora y tienes abrigos flamantes de la temporada anterior esperando perfectamente guardados en el armario.

En las rebajas de verano se puede incluso encontrar ropa de manga larga, chaquetas y algún jersey de entretiempo. Mi zona es muy calurosa, y siempre quedan en rebajas un montón de estas prendas porque la gente no las compra para verano porque no se usan, quizá en el norte se compren chaquetas en verano, pero aquí, ni una. así que me surto para el entretiempo, ya sea primavera u otoño.

 ![image](/tumblr_files/tumblr_inline_pc7s7hcK301qfhdaz_540.jpg)

Foto gracias a [http://www.pegatinasdefamilia.com/](http://www.pegatinasdefamilia.com/)

## Tiendas que te recompran la ropa usada

Como [Ketekelo](http://www.ketekelo.es/) son también una buenísima opción, como ya os comentaba en otro post.

## Los zapatos

No es aconsejable que se hereden, salvo que se hayan usado en pocas ocasiones, por ejemplo en una ceremonia, por eso hay que buscar ofertas. Carrefour saca en agosto zapatos básicos para uniforme. Son de piel, y resistentes, doy fe. Valen menos de la mitad que en la zapatería, así que nosotros compramos dos pares por niño, uno de la talla que usan en ese momento, y otro de la siguiente. Gracias a esto, no hemos tenido que hacer un desembolso a mitad de curso cuando han cambiado de talla. Merece mucho la pena.

Para el resto de zapatos y deportivos, lo mejor es recurrir de nuevo a mercadillos. Encontrar puestos que tengan género español  (de Villena, por ejemplo), y que sean de piel no es difícil. Puedes ahorrar hasta un 50%. No te exagero.

 ![image](/tumblr_files/tumblr_inline_pc7s7iRxka1qfhdaz_540.jpg)

Pero seguro que tenéis más trucos para ahorrar en la ropa de los niños.

¿Los compartís?