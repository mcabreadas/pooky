---
date: '2015-04-14T16:05:06+02:00'
image: https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00PM7Y0NE
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- seguridad
- bebes
title: Seguridad infantil en automóvil. Testing silla Exo de Jané.
tumblr_url: https://madrescabreadas.com/post/116382863069/seguridad-infantil-en-automóvil-testing-silla-exo
---

Tras nuestra [visita al Crash Test Center de Jané](/2015/02/18/seguridad-retencion-infantil.html) de la mano de [David Lay](https://ypapatambien.wordpress.com/), he tenido la ocasión de realizar una prueba de producto a su [silla de automóvil Exo del Grupo 1](http://www.amazon.es/gp/product/B00PM7Y0NE/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00PM7Y0NE&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00PM7Y0NE).

 ![image](/tumblr_files/tumblr_inline_nmmsyqhl0F1qfhdaz_540.png)

Se trata de un sistema de retención con anclaje isofix de tercera generación, que se instala en el mismo sentido de la marcha, para bebés de entre entre 9 y 18 Kg de peso.

Podéis consultar la [ficha técnica aquí](http://www.jane.es/es/catalogo-jane/para-coche/grupo-1/exo), pero yo os voy a hablar de los detalles que me han gustado según mi experiencia en sillas de este tipo que he usado con mis tres hijos.

Hace poco escribí sobre las [sillas de auto a contra marcha](/2014/11/12/sillas-automovil-seguridad-ninos.html) con ocasión de una charla a la que asistí, en la que se presentaban como la opción más segura, pero no he tenido ocasión de utilizarlas con mis hijos. 

Las que sí conozco bien son las sillas que se instalan en el sentido de la marcha, así que voy a valorar ésta de Jané destacando varios detalles que hacen que la recomiende antes que otras sillas que he probado en el sentido de la marcha.

## Instalación e instrucciones
 ![image](/tumblr_files/tumblr_inline_nmmtts5tFQ1qfhdaz_540.png)

Para qué os voy a decir otra cosa, yo me hago un lío para instalar las sillas en el coche. Ahora, después de 9 años, los que tiene mi hija mayor, ya logro hacerlo con más soltura, pero recuerdo los sudores de las primeras veces, sobre todo cuando tenía prisa por llegar a algún sitio y me enredaba con el cinturón de seguridad.

Por eso se agradece muy mucho que las instrucciones vengan claras, con dibujos y gráficos pero, sobre todo, que en la propia silla haya indicativos, que te guíen sobre el terreno a la hora de la verdad.

 ![image](/tumblr_files/tumblr_inline_nmmtst8TPs1qfhdaz_540.png)

En esta silla, unas cintas rojas muestran por dónde debe pasar exactamente el cinturón de seguridad y un dibujo deja claro cómo debe quedar la silla sujeta correctamente:

 ![image](/tumblr_files/tumblr_inline_nmmu0yEyV01qfhdaz_540.png)

Además, lleva un compartimento para guardar el libro de instrucciones para que no se pierda, detalle que agradezco especialmente porque yo soy un despiste andante.

 ![image](/tumblr_files/tumblr_inline_nmmu5sR8HY1qfhdaz_540.png)

Pero si aún así lo perdiéramos, no nos tendríamos que preocupar porque lleva un código QR impreso que nos dirige a las instrucciones on line. O sea, que ya no hay excusa para realizar una instalación correcta.

 ![image](/tumblr_files/tumblr_inline_nmmu96gnXr1qfhdaz_540.png)

## Isofix Top Tether
 ![image](/tumblr_files/tumblr_inline_nmmubqtbYX1qfhdaz_540.png)

Se trata de un sistema de triple anclaje que proporciona una fijación más segura. Eso sí, tu coche tiene que estar preparado para ello.

 ![image](/tumblr_files/tumblr_inline_nmmur6tFi41qfhdaz_540.png)

Una silla con Isofix de tres puntos de anclaje instalada en un automóvil que no lleva tercer punto de anclaje puede ser muy peligrosa porque no está homologada para el vehículo. Y a la inversa, una silla sin Top Tether podría no estar homologada para un automóvil con tercer punto de anclaje y podríamos correr el mismo riesgo.

Es muy importante consultar el manual del usuario del automóvil, en la sección de seguridad pasiva, encontraremos un apartado que nos habla de qué dispositivos de retención infantil podemos instalar en qué asientos.

 ![image](/tumblr_files/tumblr_inline_nmmuqaL5G51qfhdaz_500.gif)

## Si no está correcto se enciende la luz roja

Algo que sin duda nos deja más tranquilos a los padres es que alguien te confirme que el isofix está correctamente instalado. En en este caso, la silla lleva incorporado un sistema electrónico de detección de isofix, que va a pilas.

Si todo está correcto se enciende la luz verde, y si algo falla, la roja.

 ![image](/tumblr_files/tumblr_inline_nmmv37fBqe1qfhdaz_540.png)

## Regulación del arnés

Esto me ha encantado. No tenemos que sacar los tirantes del arnés y volver a meter por otra ranura más alta cuando el niño crece. Con la silla Exo es mucho más fácil, simplemente hay que subir la pieza interior del respaldo tirando de una anilla hacia arriba, y los tirantes suben con ella así:

 ![image](/tumblr_files/tumblr_inline_nmmvbqnZkz1qfhdaz_540.png)

¿No es increíble? Menuda comodidad! Para bajarlos, sólo hay que presionar hacia abajo.

Por otra parte, el arnés se regula sincronizadamente con la cinta central entre piernas:

 ![image](/tumblr_files/tumblr_inline_nmmvfz0pOi1qfhdaz_540.png)

## Reclinable 

Otra cosa que valoro muy positivamente es que se pueda reclinar muy fácilmente y sin movimientos bruscos cuando el bebé se duerme.

## Tejido antimanchas y transpirable

Esto para mí es fundamental porque nuestra ciudad es muy calurosa, y un tejido que transpire logra que el bebé esté mucho más cómodo.

 ![image](/tumblr_files/tumblr_inline_nmmvljPE761qfhdaz_540.png)

Además, es antimanchas y repele el agua, ¿qué os voy a contar sobre vómitos, manchas y caídas fortuítas de zumo en el coche que no sepáis… Si nos facilita la vida a los padres, pues mejor que mejor.

<h>Comodidad para el bebé<p>A nuestro peque le encantó, creo que la encontró mullidita y la suavidad del tejido le gustó mucho porque no paraba de acariciarla. Creo que se siente muy a gusto en la silla y me atrevo a decir que es muy confortable para ellos, que son realmente los protagonistas!!!</p>
<img src="/tumblr_files/tumblr_inline_nmmvy3SjBO1qfhdaz_540.png" alt="image" data-orig-width="250" data-orig-height="185"><p>Adiós! Nos vamos!!</p>
<a rel="nofollow" href="http://www.amazon.es/gp/product/B00PM7Y0NE/ref=as_li_qf_sp_asin_tl?ie=UTF8&amp;camp=3626&amp;creative=24790&amp;creativeASIN=B00PM7Y0NE&amp;linkCode=as2&amp;tag=madrescabread-21" target="_blank"><img src="/tumblr_files/tumblr_inline_nmmwc9j08H1qfhdaz_540.jpg"><img src="https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&amp;l=as2&amp;o=30&amp;a=B00PM7Y0NE" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;"><p>Gracias a Jané por darme la oportunidad de probar su silla Exo y compartir con vosotros mis impresiones.</p>
<p>¿Y a ti qué te parece? ¿La has probado?</p> </a> </h>