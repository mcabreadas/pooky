---
layout: post
title: Moda infantil de invierno en FIMI
date: 2016-01-27T22:05:28+01:00
image: /tumblr_files/tumblr_inline_o39yp56ddq1qfhdaz_540.jpg
author: .
tags:
  - eventos
  - moda
tumblr_url: https://madrescabreadas.com/post/138170504934/fimi-moda-infantil
---
Este año he asistido por primera vez a [FIMI (Feria Internacional de Moda Infantil y juvenil)](http://www.fimi.es/), que celebra ya su 82 edición en el Pabellón de la Casa de Campo de Madrid, donde conocimos las colecciones para la próxima temporada Otoño-Invierno 2016-17 de 270 firmas nacionales y extranjeras procedentes de Alemania, Bélgica, Canadá, Dinamarca, España, Francia, Holanda, Italia, Reino Unido y Portugal.

 ![](/tumblr_files/tumblr_inline_o39yp56ddq1qfhdaz_540.jpg)

## Pasarela FIMI KIDS FASHION WEEK

Esta gran feria abarca moda infantil y juvenil, calzado, complementos, regalos y decoración… además de una de las pasarelas más importantes de moda infantil de España, FIMI Kids Fashion Week, donde se dan cita marcas como las que siguen:

**Agatha Ruiz de la Prada**

Las niñas de [Agatha Ruiz de la Prada](http://www.agatharuizdelaprada.com/) llenaron la pasarela de color y alegría.

 ![](/tumblr_files/tumblr_inline_o39yp5krFZ1qfhdaz_540.jpg)

**Barcarola**

Barcarola juega con el blanco y el negro y toma como protagonistas las flores.

 ![](/tumblr_files/tumblr_inline_o39yp59H7C1qfhdaz_540.jpg)

**Bóboli**

[Bóboli](http://www.boboli.es/) apuesta por el amarillo en una divertida puesta en escena.

 ![](/tumblr_files/tumblr_inline_o39yp6v3c91qfhdaz_540.jpg)

**Dolce Petit**

Las niñas de Dolce Petit al más puro estilo tradicional están para comérselos.

 ![](/tumblr_files/tumblr_inline_o39yp6HV4v1qfhdaz_540.jpg)

**Foque**

El verde que nos sugiere [Foque](http://www.foque.es/) me parece de los más favorecedor.

 ![](/tumblr_files/tumblr_inline_o39yp7ViqP1qfhdaz_540.jpg)

**J.V. José Varón**

La puesta en escena de [José Varón](http://www.jvjosevaron.com/) me pareció de una sensibilidad exquisita. como protagonista, un niño leyendo. 

 ![](/tumblr_files/tumblr_inline_o39yp7Vbno1qfhdaz_540.jpg)

**Kiddy Mini Model**

[Kiddy Mini Model](http://www.fimi.es/kiddy-mini-model-la-primera-marca-de-ropa-creada-por-ninos/), la única marca creada por los niños, nos trasladó a su universo de fantasía con sus niñas vestidas como sus muñecas.

 ![](/tumblr_files/tumblr_inline_o39yp8vC9I1qfhdaz_540.jpg)

**La Ormiga**

[La Ormiga](http://www.laormiga.com/) presenta señoritas al más puro estilo romántico. Una de mis favoritas.

 ![](/tumblr_files/tumblr_inline_o39yp995Xj1qfhdaz_540.jpg)

**Lion of Porches**

El estilo british que presenta Lion of Porches marcó la diferencia en la pasarela.

 ![](/tumblr_files/tumblr_inline_o39yp9fVKt1qfhdaz_540.jpg)

**Lea Lelo**

Los zapatos de Lea Lelo y los conjuntos de Susana Mazzarino nos trasladaron a la fría Rusia.

 ![](/tumblr_files/tumblr_inline_o39ypaO5jU1qfhdaz_540.jpg)

**Losan**

El estilo urbano de Losan, que combina el negro con colores vivos me parece ideal.

 ![](/tumblr_files/tumblr_inline_o39ypak54W1qfhdaz_540.jpg)

**Mayoral**

También urbano, pero con un punto macarra-chic se presenta [Mayoral](http://www.mayoral.com/es/espana/chicas-8-a-16-anos).

 ![](/tumblr_files/tumblr_inline_o39ypbPmqi1qfhdaz_540.jpg)

**N+V Villalobos + Nieves Álvarez**

N+V Villalobos + Nieves Álvarez apuestan por la piel y el pelo también para las niñas.

 ![](/tumblr_files/tumblr_inline_o39ypbTz1Y1qfhdaz_540.jpg)

**Oca-Loca**

Esta marca de zapatos tan loca como su nombre [Oca Loca](http://www.oca-loca.com/) encanta a las niñas teens… y a las mamás también!

 ![](/tumblr_files/tumblr_inline_o39ypcyo6X1qfhdaz_540.jpg)

**Pilar Batanero**

Variedad de estilos con [Pilar Batanero](http://www.pilarbatanero.es/). Me encantan los ponchos y las combinaciones de colores que propone tan originales.

 ![](/tumblr_files/tumblr_inline_o39ypc0Ku91qfhdaz_540.jpg)

**Tete y Martina**

[Tete y Martina](http://www.teteymartina.com/) apuesta por el rosa palo sobre negro con unos espectaculares abrigos de pelo.

 ![](/tumblr_files/tumblr_inline_o39ypcVxLD1qfhdaz_540.jpg)

## Showroom

Dedicaré un post exclusivo para el Showroom porque descubrí cosas muy interesantes, y quiero contároslo con detalle.

Para abrir boca os dejo estas fotos:

 ![](/tumblr_files/tumblr_inline_o39ypdgkeb1qfhdaz_540.jpg)

 ![](/tumblr_files/tumblr_inline_o39ypdHB1P1qfhdaz_540.jpg)

 ![](/tumblr_files/tumblr_inline_o39ypdi0Hx1qfhdaz_540.jpg)

## Nuditos con los nuevos talentos

Me encanta que FIMI sirva también como plataforma de lanzamiento\
de jóvenes promesas del diseño del mundo infantil a través de su espacio “Nuditos”, donde los jóvenes estudiantes de diseño tienen la oportunidad de mostrar sus colecciones arropados por diseñadoras como\
[Agatha Ruiz de la Prada](http://www.agatharuizdelaprada.com/), que impartió una conferencia sobre su larga experiencia en el mundo de la moda y el diseño bajo el título “La moda a todo color”

 ![](/tumblr_files/tumblr_inline_o39ypePqxS1qfhdaz_540.jpg)

## Solidaridad en FIMI

Pero en FIMI la solidaridad tiene también su espacio de la mano de varias iniciativas:

\-La **[Fundación Carmen Pardo-Varcarce](http://www.pardo-valcarce.com/)**, cuyo fin es integrar a las personas\
con discapacidad intelectual en la sociedad, y que presentó su nueva línea de decoración juvenil fabricada en su propia carpintería, y chuches en diferentes presentaciones realizadas en su obrador en una pequeña muestra\
del mundo mágico de Fundaland, su parque infantil de ocio inclusivo en el que los niños aprenden valores mientras se divierten.

 ![](/tumblr_files/tumblr_inline_o39ypffASJ1qfhdaz_540.jpg)

* La **Fundación pequeño deseo y N+V** , en su espacio ARCO Kids ofrecieron a los niños una divertida actividad bajo el lema “Jugando puedes ayudar a que otros niños jueguen” y, además se llevaron unos delantales vintage especialmente diseñados para ellos por el equipo de N+V con Nieves Álvarez y Belén Villalobos a la cabeza.

\-La acción **“Una sonrisa para Lucía”** mediante la que FIMI pretende ayudar a la pequeña Lucía, modelo de pasarela FIMI, a conseguir un tratamiento paulatino para su enfermedad.

Y hasta aquí, mi crónica de lo que fue un día intenso en FIMI, rodeada de moda, color, arte y buena compañía.

 ![](/tumblr_files/tumblr_inline_o39ypfKtPc1qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.