---
layout: post
title: Visita al pediatra. Muy rebelde a la exploración
date: 2013-05-03T21:54:00+02:00
image: /tumblr_files/tumblr_inline_mm46ixg6Sv1qz4rgp.jpg
author: maria
tags:
  - salud
  - crianza
  - bebes
tumblr_url: https://madrescabreadas.com/post/49531138787/visita-al-pediatra-muy-rebelde-a-la-exploración
---

“Muy Rebelde a la exploración”, así comienza el informe del pediatra especialista en gastroenterología y nutrición al que hemos ido para tratarle el reflujo gastroesofágico a Leopardito. Y así es él, salvaje, primario anárquico y libre… no le gustan las ataduras ni ser manipulado al antojo de nadie. Por eso luchó como una “fiera”, en palabras del propio médico y ofreció resistencia durante toda la exploración.

Seguramente te interese este post donde cuento [tips para aliviar y curar el reflujo silencioso de bebés](https://madrescabreadas.com/2015/11/20/reflujo-bebes/).

![image](/tumblr_files/tumblr_inline_mm46ixg6Sv1qz4rgp.jpg)

Imagen gracias a [http://t0.gstatic.com](http://t0.gstatic.com)

Y, la verdad, no me extraña porque cuando entramos a la consulta yo me tuve que contener para no salir corriendo. Pareciera que hubiéramos retrocedido en el tiempo 50 años, este hombre había mantenido intacta la decoración y seguramente los utensilios de exploración, desde que se licenció en medicina… en serio. Me sentí como en un museo, y una angustia se empezó a apoderar de mí y de mis ideas de crianza natural. Sería capaz de responderle o al menos explicarle y justificarle mi manera de criar al bebé? Me ridiculizaría? o, lo que es peor, me culpabilizaría por seguir dándole el pecho o por no seguir estrictamente el calendario de introducción de alimentos?

“Muy Rebelde a la exploración”. Claro, y es que Leopardito no entiende de pesos, ni de tallas, ni de índice de masa corporal, de perímetro craneal o braquial. Tampoco de pliegues tricipital, subescapular, bicipital, suprailiaco o mandibular, ni mucho menos de la manera en que se los midieron, cual si fuera un ternerillo, y usando una especie de pinzas como las del Sr. Cangrejo que regalaron una vez a mis hijos mayores en el Burguer King.

![image](/tumblr_files/tumblr_inline_mm45x8Uxog1qz4rgp.jpg)

Índices, medias, percentiles… de verdad hace falta todo eso para dictaminar que mi cachorro “está de dulce” (de nuevo cito al Doctor). Pero si no hay más que verlo, que aunque sufra de reflujo, todo lo que le sube le vuelve a bajar y no desaprovecha nada el pobrecillo. Ni siquiera el Índice de Mc. Laren, el Cociente perímeto braquial/perímetro craneal ni la medición con Infrarrojos (vaya susto se llevó el chiquillo) consiguieron desmentir lo que era evidente.

Después de la concienzuda exploración (desde luego el pediatra sudó la camiseta), llegó la hora de las preguntas (entonces empecé a sudar yo). Pluma en mano, instrumento de escritura tradicional donde los haya, para ir acorde con la decoración sesentera de la consulta, tomó nota concienzudamente de todas nuestras respuestas. Y cual fue mi sorpresa, cuando le hablé de la lactancia exclusiva durante los 6 primeros meses y su respuesta fue otra pregunta:

-“Con qué leche le ayudaba?”

- “Con ninguna”

- “Sólo pecho? (ojos cómo platos)”

- “Sí” (trago saliva, casi me derrumbo)

- “Ya, pero cuando tenía que ayudarle, qué marca le daba?”

-“no, no… nunca ha tomado leche de fórmula”

-“pero qué marca de leche le da ahora?”

- “ninguna, rechaza todo tipo de leche que no sea la teta” (interviene mi marido con una sonrisa)

-“¡es Ud. una artista!” , exclamó.

![image](/tumblr_files/tumblr_inline_mm46ptpWtU1qz4rgp.jpg)

Reconozco que al principio no sabía cómo tomármelo, pensé que podía tratarse de una ironía (iba predispuesta, “mea culpa”), pero a lo largo de la entrevista me percaté de que lo había dicho en serio, y me di cuenta de la gran valía profesional de este hombre y la humildad con que aceptó y respetó todo lo que le conté, a pesar de su larga trayectoria profesional y gran experiencia seguramente en otro sentido (o quizá no), incluso me aconsejó que diera el pecho al niño porque parecía que tenía hambre.

Dos horas y media de consulta con un trato exquisito y respetuoso, a pesar de la edad del pediatra y lo tradicional de los métodos y el instrumental, y a pesar de haberle roto los esquemas. El resultado fue la prescripción de un tratamiento razonable, más otro alternativo por si no funcionara bien el primero, nada de pruebas invasivas e innecesarias, y una tranquilidad que nos quedó a mi marido y a mí que se la debimos de trasladar a Leopardito, porque esta noche todos hemos dormido mucho mejor.

Y tú, has tenido experiencias con pediatras de la antigua escuela?

Cómo te ha ido?