---
date: '2016-09-13T12:33:24+02:00'
image: /tumblr_files/tumblr_inline_ocomd5z7QO1qfhdaz_540.png
layout: post
tags:
- crianza
- colaboraciones
- participoen
- ad
- salud
title: Mamá, no puedo con la mochila del cole
tumblr_url: https://madrescabreadas.com/post/150351160044/como-llevar-mochila
---

Quizá los primeros años de colegio no nos planteamos esta cuestión en serio, pero conforme van subiendo de curso, de cantidad de libros, y de peso de la mochila, el tema de cómo los transportan nuestros hijos nos debe preocupar y mucho.

Si bien no está demostrado médicamente que niños con dolores de espalda se conviertan en adultos con dolores de espalda, para mí **está muy clara la relación causa-efecto,** y con mis hijos no me la juego.

No me la juego porque Princesita (mi hija mayor), ha sufrido contracturas en la espalda, a la altura del omoplato en varias ocasiones, siendo necesaria incluso una sesión de fisioterapia.

La razón está clara, y además ella lo dice así:

“Mamá, no puedo con la mochila”

Al final del post tienes una encuesta que han creado un grupo de pediatras españoles para comprobar el peso que llevan nuestros hijos, y que te animo a contestar para colaborar con el movimiento #sinmochilas.

Los niños **no deben llevar un peso superior al 10% del suyo**. Es decir, que si mi hija pesa 30 Kg, podrá llevar 3 Kg de libros en la mochila en teoría, pero te diré que cada día del curso pasado ha superado esta cifra.

También hemos probado con las mochilas de ruedas, de las que se tira de ellas, pero la postura es más forzada para la espalda si no se lleva correctamente, lo que es difícil si pesa demasiado, porque provoca una torsión al tirar de ella que perjudica.

## ¿Mochila de ruedas o a la espalda?

Parece que los expertos recomiendan la mochila a la espalda, porque reparte mejor el peso si se coloca correctamente, como te explico en el siguiente vídeo:

<iframe width="640" height="480" src="https://www.youtube.com/embed/qXtsfSGXhRU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Por contra, parece que si usan carritos se suelen confiar y porque meten más peso, sobrepasando la mayoría de las veces el 10% del suyo por el 20% y 30%.

Además, si lo llevan tirando de él, en lugar de empujándolo, al ser trasladado con el hombro en extensión y rotación (brazo hacia atrás) e ir salvando todos los obstáculos que existen en el recorrido, esta articulación podría **sufrir leves traumatismos con el consiguiente dolor** y problemas de movilidad.

Otra pega es que, una vez que el niño llega a clase a veces debe acceder a las aulas por las escaleras, por lo que el carrito suele ser trasladado de una manera muy perjudicial para el hombro y la columna vertebral.

También hay que tener en cuenta que la articulación de la muñeca con sus ligamentos, tendones y musculatura sufre constantemente por el peso que arrastra.

La buena noticia es que hay mochilas de ruedas que parecen salvar todos estos impedimentos, al estar diseñadas también para empujarlas, no sólo para tirar de ellas, como te explico más abajo, y para subir escaleras.

## ¿Cómo elegir mochila a la espalda?

\-No debe primar tanto el diseño como el tamaño adecuado.

\-Que el material sea resistente, **preferiblemente loneta de algodón plastificado**, ya que son las más impermeables. Asegúrate de que pueda ser lavada fácilmente en la lavadora.

\-Que el cierre sea de calidad: **cremallera gruesa y resistente, y tirador sólido**. Si se rompe la cremallera, te habrás quedado sin mochila, por muy resistente que sea el resto del material.

\-Las mochilas con dos partes y bolsillos pequeños en los laterales y frontales facilitan la clasificación del material escolar.

\-Que los tirantes sean regulables, anchos y acolchados.

\-Que el respaldo sea acolchado.

\-Que los ajustes sean firmes.

\-Que no exceda en altura de la zona donde va a ir apoyada, es decir de la zona dorsal alta.

![](https://d33wubrfki0l68.cloudfront.net/49966758afd7eddd0d41077a9149a4e211afcfe5/6143e/tumblr_files/tumblr_inline_ocomd5z7qo1qfhdaz_540.png)

\-La anchura no debe ser mayor que el de su tronco (debe quedar 5 cm por encima del culete una vez puesta).

\-Que lleve cinta de sujeción para la cintura y para la parte externa.

[![](/images/uploads/wenlia-mochila.jpg)](https://www.amazon.es/Wenlia-almuerzo-estudiantes-astronauta-dinosaurio/dp/B093BS9MLR/ref=sr_1_3?keywords=bolso%2Bde%2Bruedas%2Bniños&qid=1642703950&sprefix=bolso%2Bde%2Brue%2Caps%2C610&sr=8-3&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Wenlia-almuerzo-estudiantes-astronauta-dinosaurio/dp/B093BS9MLR/ref=sr_1_3?keywords=bolso%2Bde%2Bruedas%2Bniños&qid=1642703950&sprefix=bolso%2Bde%2Brue%2Caps%2C610&sr=8-3&th=1&madrescabread-21)

## ¿Cómo elegir mochila de ruedas?

\-Que sea ambivalente, es decir, **que tenga la opción de llevarla a los hombros** si fuera necesario, por ejemplo, para subir escalones. Hay algunas que incluso llevan un cubre ruedas para no manchar la ropa.

O bien, que tenga las ruedas lo suficientemente grandes que permitan subir escalones de forma controlada cargando en todo momento el peso sobre las ruedas, y si que el niño tenga que pegar tirones bruscos o cargar en su espalda el peso. En este caso se recomienda subirla hacia atrás, y cogiéndola de las dos manos, como te muestro en el vídeo más adelante.

\-Que no pese demasiado.

\-Que esté diseñada para poder empujarla, ya que ésta es la manera idónea de llevarla porque al arrastrar un objeto se debe vencer la fuerza de la gravedad y al empujar se debe vencer sólo la mitad de la gravedad.

Además, es mejor empujar, ya que se utiliza parte del peso del cuerpo para alcanzar la fuerza necesaria para mover el objeto, a su vez te acercas a la carga disminuyendo la distancia horizontal carga-cuerpo lo que disminuye la tensión a nivel de las lumbares.

Empujando se lesiona menos la columna, los brazos y los hombros.

\-Que el manillar sea regulable en altura y el agarre sea ergonómico.

![](https://d33wubrfki0l68.cloudfront.net/4066d487206eb200a6d5de55055f12a9d188c9f7/c68a5/tumblr_files/tumblr_inline_odfu4mxr7x1qfhdaz_540.png)

Este curso vamos a probar la [mochila Nikidom Roller](http://www.nikidomroller.com/09000001/COLECCION-ROLLER.htm), ya que Princesita ha sufrido contracturas otros años por el gran peso que tiene que transportar. Anteriormente hemos probado la mochila a la espalda, pero dado que su complexión es delgada y, aunque es una niña fuerte, su espalda es estrechita, no aguanta la mochila colgada con tanto peso.

Tampoco mejoró tirando del carrito, como os comentaba antes, así que la Roller parece la mejor alternativa.

Hemos hecho este vídeo donde ella misma te cuenta qué tal le va con su nueva mochila:

<iframe width="640" height="480" src="https://www.youtube.com/embed/NOY4pLOXfVY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ella está muy contenta, pero de todos modos, te hablaremos de los resultados a largo plazo, y ojalá te podamos decir que ha superado sus dolores de espalda.

[![](/images/uploads/safta.jpg)](https://www.amazon.es/Safta-612041860-Mochila-Evolution-Mandalorian/dp/B089DXWTMJ/ref=psdc_2007988031_t1_B093BS9MLR&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Safta-612041860-Mochila-Evolution-Mandalorian/dp/B089DXWTMJ/ref=psdc_2007988031_t1_B093BS9MLR&madrescabread-21)

## Peso que debemos meter

En España no existe una normativa al respecto, pero siguiendo las existentes en países como Alemania, Austria o Italia, **no es aconsejable transportar más de un 10-15% del peso corporal.**

![](https://d33wubrfki0l68.cloudfront.net/798cce19bd184632a454ce6878c3af49455d90be/cd535/tumblr_files/tumblr_inline_ocomce8aae1qfhdaz_540.png)

Por ejemplo, un niño de unos 40 kg no debería cargar con más de 4-6 kg en la espalda. No debemos meter más del 10% del peso del cuerpo del niño, como mucho el 15%.

## Limitar el tiempo de carga

Evitar que transporten el la mochila con mucho peso más de 15 minutos. En este punto los padres tenemos mucha responsabilidad, aunque a veces suponga tener que cargarla nosotros.

## Consejos para manipular la mochila

Utilizar las dos manos para coger la mochila, doblar las rodillas e inclinarse para levantarla.

Para colocarla **es mejor subirla a una mesa primero**, ponernos de espaldas a ella, y después colgarla de los hombros

## ¿Cómo llevar la mochila a la espalda?

\-Usar siempre los dos tirantes, nunca uno.

![](https://d33wubrfki0l68.cloudfront.net/be34c2027b575a73a62e9d2549fc6cdc975d92ef/d4b1f/tumblr_files/tumblr_inline_odfu5hnlae1qfhdaz_540.png)

\-Ajustar y abrochar las correas para la cintura y para la zona esternal (por encima del pecho)

\-La base de la mochila debe quedar 5 cm por encima del culete, pero no debemos subirla demasiado a los hombros porque si no sobrecargaría la zona de los trapecios.

**Lo que no hay que hacer**

\-De un hombro

\-Colgandera

[![](/images/uploads/cerda-life-s.jpg)](https://www.amazon.es/CERDÁ-LIFES-LITTLE-MOMENTS-Oficial-Warner/dp/B099MX43BD/ref=sr_1_25?keywords=mochila+escolar+con+ruedas&qid=1642705589&sprefix=mochila+escolar+%2Caps%2C859&sr=8-25&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/CERDÁ-LIFES-LITTLE-MOMENTS-Oficial-Warner/dp/B099MX43BD/ref=sr_1_25?keywords=mochila+escolar+con+ruedas&qid=1642705589&sprefix=mochila+escolar+%2Caps%2C859&sr=8-25&madrescabread-21)

## ¿Cómo llevar la mochila de ruedas?

\-Procurar llevarla empujando el mayor tiempo posible, en vez de tirando.

![](https://d33wubrfki0l68.cloudfront.net/c9a297d5ba6de1e9388be4dba8e7ec7a71abab12/69f04/tumblr_files/tumblr_inline_odftt3fi8k1qfhdaz_540.png)

\-Si la llevan tirando de ella, colocar el brazo pegado al tronco, con el pulgar hacia la cadera para evitar torsiones perjudiciales.

\-Mantener siempre la columna recta, no curvada

![](https://d33wubrfki0l68.cloudfront.net/5a43948dd379929f6be6ce3f475eee419bd3bbf4/96ca2/tumblr_files/tumblr_inline_odfu1zm4ar1qfhdaz_540.png)

\-Si tiran de ella, asegurarnos de que van cambiando de brazo

**Lo que no hay que hacer**

\-no tirar de ella para subir o bajar escalones

\-no sobrecargarla de peso

[![](/images/uploads/roll-road-rose-.jpg)](https://www.amazon.es/Mochila-ruedas-Roll-Road-Rose/dp/B07N25XP2Z/ref=sr_1_33?keywords=mochila+escolar+con+ruedas&qid=1642705589&sprefix=mochila+escolar+%2Caps%2C859&sr=8-33&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Mochila-ruedas-Roll-Road-Rose/dp/B07N25XP2Z/ref=sr_1_33?keywords=mochila+escolar+con+ruedas&qid=1642705589&sprefix=mochila+escolar+%2Caps%2C859&sr=8-33&madrescabread-21)

## ¿Cómo colocar libros y material escolar?

Los libros más pesados deben ir en la parte más pegada a la espalda.

Debemos distribuir el material en los distintos compartimentos de manera que no vayan moviéndose.

Es mejor llevar parte de los cuadernos o libros en las manos, como abrazándolos, por ejemplo, la carpeta más gruesa, así aliviaremos el peso de la espalda, al tiempo que compensaremos la carga de atrás con la delantera.

![](https://d33wubrfki0l68.cloudfront.net/a3ce2b338eac6ca794880ff74180d9ceaefc00c7/a119f/tumblr_files/tumblr_inline_ocomc2kjhc1qfhdaz_540.png)

## Ejercitar la musculatura de la espalda

El mejor seguro de salud para la espalda de nuestros hijos presente y futura es que practiquen ejercicio para fortalecerla. Así será más difícil que se lesionen,

Nuestra labor como padres es muy importante en este tema. Lo mejor es educar desde pequeños en este sentido, haciéndoles ver que es una cuestión de salud, más allá de modas.

Así, cuando sean mayores, vayan al instituto y vean a un compañero con la mochila mal colocada porque queda más guay quizá le corrijan amablemente en vez de imitarlo.

Si te ha servido y piensas que puede ayudar a otros padres, compártelo :)

*Gráficos gracias a [ergológico.com](http://www.ergologico.com/)* 

## Actualizacion 15/9/16: Movimiento #sinmochilas

Actualizo el post porque paralelamente a su publicación ha surgido la iniciativa [\#sinmochilas](https://twitter.com/hashtag/sinmochilas?src=hash) promovida por la [Dra. Blanca Usoz](http://doctorablancausoz.com/mochilas-dolor-espalda/), y apoyada por numerosos pediatras, de la que me informó mi amiga la [Dra. Amalia Arce](http://www.dra-amalia-arce.com/2016/09/vuelta-al-cole-sin-mochilas), a quien conoces como La mamá Pediatra, y con quien he hecho varios post colaborativos en este blog.

Estos profesionales sanitarios defienden la enseñanza a través de las nuevas tecnologías, y se abandone el traslado de libros, que tanto daño puede hacer a la espalda de nuestros hijos, y nos proponen el mismo experimento que sugería yo en Twitter ayer, que pesemos la mochila de nuestros hijos y rellenemos esta [encuesta](https://docs.google.com/forms/d/e/1FAIpQLSfPtLpd-ImVJyXKmn7MBYnPNzPV-iLnpGzd4GTXz9i6TM0wsQ/viewform).

Únete al movimiento #sinmochilas”: comparte la encuesta en tus redes sociales con el hastag para que llegue a muchas madres y padres, profesores…!

¡Queremos hacernos oír, ayúdanos!

![](https://d33wubrfki0l68.cloudfront.net/33d5eeaec2520ab62fae836c76800ee72244f86e/e56f2/tumblr_files/tumblr_odjogoezut1qgfaqto1_540.jpg)

*NOTA: Post NO patrocinado. Nikidom me ha cedido una mochila para testear y ofrecerte mi opinión.*