---
layout: post
title: El sueño de una maestra
date: 2011-03-24T12:21:00+01:00
image: /tumblr_files/tumblr_lik8gorsvO1qfhdaz.jpg
author: maria
tags:
  - 6-a-12
  - libros
  - planninos
  - eventos
  - educacion
tumblr_url: https://madrescabreadas.com/post/4062118966/el-sueño-de-una-maestra
---
![image](/tumblr_files/tumblr_lik8gorsvO1qfhdaz.jpg) ![image](/tumblr_files/tumblr_lik8boFcoW1qfhdaz.jpg)

Érase una vez una maestra que quería que sus alumnos de 8 y 9 años aprendieran a disfrutar la poesía y tuvieran un acercamiento intuitivo a la misma para que fuesen capaces de desarrollar la sensibilidad para entenderla y, ¿por qué no? para escribirla. Tanto motivó a sus alumnos, y tantos poemas escribieron que consiguió crear con sus propios medios un libro con todos esos poemas.

Pero no conforme con esto siguió luchando incansable para que este libro llegara a cuantos más niños mejor, por eso recorrió Bibliotecas, Ludotecas, Colegios, Administraciones Públicas y cuantas librerías encontraba a su paso.

Gracias a su tesón el libro se va a traducir al braille por la ONCE para que los niños invidentes también puedan leerlo.

Además lo ha donado a la ONG Save the Children para su campaña 1libro1euro.com, y así ayudar a los que más lo necesitan.

Estos han sido sus últimos logros, pero estoy segura de que no dejará de sorprendernos porque no piensa parar hasta que una editorial se embarque con ella en esta aventura.

El libro se llama “Grandes Poesías de Pequeños Poetas”, y tiene la espontaneidad y la frescura de la infancia. Ya veréis como os arranca alguna sonrisa. Os dejo con alguno de los poemas:

<a href="[](https://mailchi.mp/cd4b077ecacf/nete-a-madres-cabreadas)https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c" class='c-btn c-btn--active c-btn--small'>Descargar gratis el libro de poemas infantiles</a>

“Cuando yo era pequeña,

y parece que fue ayer

no le decía seño a mi profe

sino su nombre y de usted.

¿Por qué le dedico esta poesía a D. Ángel?

por una razón especial:

era amable, cariñoso,

era muy culto y formal.

Nos contaba cosas suyas

a veces se ponía seriote

una vez nos contó que

pensó en ser sacerdote.

Era de Tudela

un chicarrón del norte

¡Vaya tela!

De él aprendí justicia, tolerancia y respeto,

también aprendí latín y griego.

Me daba muchos y buenos consejos.

El ya no está entre nosotros

de una forma material

pero como podéis comprobar

conmigo siempre va a estar.”

Mª Felisa

“Mi padre es camionero

y es tan feliz

como un heladero.

Él quiere que yo obedezca

para que así buena parezca.

Yo quiero llegar a ser tan buena como él,

y trabajadora aunque sea una soñadora.

Mi padre es el mejor

porque es un ganador”

Leticia Alpanez Ramírez

“A mi padre le encanta dormir la siesta

y estar de fiesta.

La Semana Santa se acerca,

mi padre se viste de nazareno

porque es moreno.

Mi padre juega mucho conmigo

y le encantan los higos.

Mi padre trabaja con el ordenador

y le encantaría montar en transbordador.

Mi papá me trae siempre en coche

y los jueves viene de noche.

Mi padre quiere mucho a mi madre,

la cuida hasta el final de su vida.

A mi papá le gustan los restaurantes

pero más los de antes.”

Rosana Iniesta

“Mi madre me quiere como un tesoro,

pero no porque estoy hecha de oro.

Mi madre trabaja sin parar,

pero no le gusta nadar.

A mi madre le duelen los codos

y la cuidamos entre todos.

Mi madre es guapa

y se pone una capa.

A mi madre le gusta leer

y adquiere saber.

A mi madre le encantan los gatos

y odia a los patos.

A mi madre no le gusta estar de fiesta,

sino dormir la siesta.”

Rosana Iniesta

“A mi madre le gusta comprar,

pero no lavar.

Mi madre lava camisas y

camisones a montones.

Mi madre es trabajadora,

honrada y ordenada.

Yo le regalo mi amor

y una flor.

Yo a veces protesto,

pero siempre lo acepto.

Mi madre es encantadora

y cantadora.

Si mi madre está triste,

le cuento un chiste.

Si mi madre está contenta,

le sobra el amor

y lo acumula

en una flor.”

Mario López Garcerán

“Mi abuela es la más bella

entre todas ellas

porque cuando su boca ríe,

sus ojos sonríen.

Mi abuelo es tan formal

como un oficial.

A mi abuelo le gusta cantar

como a los pájaros trinar.

Mis abuelos son los mejores,

porque cocinan en los fogones y

me ayudan con los ordenadores.

Mi abuela es muy buena

y también perfecta.

Cuando le pido algún caprichillo

ella siempre me lo compra,

sea un helado o algún dulce,

aunque sea algo de luces.

Mi abuela es la mejor

y siempre compra turrón.

Dentro de su corazón hay

amor, simpatía y mucha alegría .”

Javier Benito López

“La Constitución

Madre de la democracia

Que tienen las dos

Mucha tolerancia.

Si la Constitución y la democracia

Se meten en tu cuerpo

Tendrás más libertad

Que un pájaro contento.

Constitución gracias a ti

Afirmamos la paz

La guerra se terminó

Y el mundo muy bien vivió.

La solidaridad,

El respeto,

La libertad,

La generosidad

Y la tolerancia

Son los ingredientes

Para vivir la democracia.”

A.D.