---
date: '2016-10-20T09:45:35+02:00'
image: /tumblr_files/tumblr_inline_of3kff9gSm1qfhdaz_540.png
layout: post
tags:
- ad
- colaboraciones
- moda
- familia
title: Bóboli nos lleva al bosque
tumblr_url: https://madrescabreadas.com/post/152061324499/moda-infantil-boboli-invierno
---

Parece que tímidamente se va adentrando el frío por estas tierras sureñas, y por fin podemos estrenar la ropa de otoño que con tanta ilusión elegimos, y que no veíamos camino de usar.

Y es que el verano por aquí suele prolongarse más de la cuenta.

Por fin os puedo presentar de la mano de mis tres fieras la colección de otoño invierno de [Bóboli](https://www.boboli.es/es/) que, creedme, os va a encantar.

La calidad de las prendas de esta marca es excelente, de esa que nos gusta a las [madres de familia numerosa](/2015/02/27/ahorro-ropa-ninos.html), de esa que cuando tocamos una camiseta nos sonreímos por dentro y pensamos: “ésta dura para los tres”.

Para enseñaros un poco de esta nueva colección allá que nos adentramos en el bosque y soltamos a las fieras, como os decía, a su libre albedrío.

 ![](/tumblr_files/tumblr_inline_of3kff9gSm1qfhdaz_540.png)

Se subieron a los árboles, practicaron artes marciales con palos, corrieron por la hierba, jugaron al escondite… Y todo ello sin que la ropa les impidiera disfrutar a tope.

Ya veis que son prendas a prueba de fieras. cómoda, muy ponible y con mucho estilo. Parece diseñada para acompañar a los niños en sus tardes de aventuras y juegos.

 ![](/tumblr_files/tumblr_inline_of3kkt36mP1qfhdaz_540.png)

A mí me evoca libertad, diversión, alegría e ilusión.

Esta temporada Bóboli enmarca su colección en cuatro temas principales: 

**North Way**

El colorido marino/rojo/gris son protagonistas.

 ![](/tumblr_files/tumblr_inline_of3kh6J6E11qfhdaz_540.png)

Para la línea Baby, huyen del clásico marinero de invierno, para ambientar este colorido desde un romántico vintage con un juego divertido de coches retro de carreras para los niños.

 ![](/tumblr_files/tumblr_inline_of3kbbgCO61qfhdaz_540.png)

Para la línea Kids, el ambiente londinense se hace visible en los gráficos y estampaciones.  El niño está inspirado en la temática college con un aire divertido y casual saliendo de las aulas y tomando la calle como tendencia.

 ![](/tumblr_files/tumblr_inline_of3keaCd2L1qfhdaz_540.png)

En esta ocasión, las niñas se vuelven rockeras, trabajamos cuadros madrás en camisas, faldas y mallas, cuero, calaveras, jeans rotos con parches… Añadimos prendas de pelo que aportan calidez y moda al conjunto.

En la línea chic, las pequeñas se acompañan de ositos y corazones en prendas de tricotosa trabajadas con intarsias, juegos de rayas, aplicaciones de strass y swarovsky que enriquecen esta temática.

**Silver Snow**

Silver Snow refleja la calidez suave en un paisaje nevado. La paleta pasa de tonos pálidos y fríos como el blanco, rosa y verdes agua, a contrastes de grises tormentosos con un toque de resplandor metálico plateado.

 ![](/tumblr_files/tumblr_inline_of3k98500E1qfhdaz_540.png)

Las prendas van adornadas con puntillas y volantes de voile.

 ![](/tumblr_files/tumblr_inline_of3kd0Mumu1qfhdaz_540.png)

El denim gris lavado adquiere total importancia y es el protagonista en todas las líneas.

**Jazz Melody**

Como en una melodía, se mezclan diferentes estilos, pero siempre unidos por el juego bicolor… Como el contraste de las notas musicales en una partitura.,

**Into the Woods**

Para esta última tendencia, Bóboli se adentra en lo más profundo del bosque…

Con la calidez de un ambiente boscoso, introduce los colores verdosos de los árboles, verdes hierba, kakis, rojizos de los frutos silvestres mezclados con los anaranjados de las hojas.

Pero esta vez, quienes nos adentramos en el bosque fuimos nosotros, y pasamos un día disfrutando en familia de la naturaleza y de mil y un juegos.

¿A que no adivinas qué juego era éste?

 ![](/tumblr_files/tumblr_inline_of3kj8Fb4u1qfhdaz_540.png)