---
date: '2017-11-20T11:17:20+01:00'
image: /tumblr_files/tumblr_inline_ozgj5qe24m1qfhdaz_540.png
layout: post
tags:
- crianza
- colaboraciones
- puericultura
- ad
- familia
- bebes
title: Portea a tu estilo con Teo y Leo
tumblr_url: https://madrescabreadas.com/post/167693248524/portabebes-personalizados-teoyleo
---

Portear es de las cosas que más echo de menos de cuando mis hijos eran pequeños. Primero en el fular, luego en mi mei tai de flores hecho a mano, más tarde en la mochila ergonómica… Por eso os quiero enseñar [Teo y Leo, una tienda on line que tiene todo tipo de portabebés ergonómicos](http://www.teoyleo.com/es/) con los estampados más originales y personales que te puedas imaginar para que puedas portear a tu estilo.

## Fulares de tejidos naturales

Cuando son recién nacidos, nada como el [fular elástico](http://www.teoyleo.com/es/74-fulares-portabebes) que te permite adaptarlo fenomenal a su cuerpo y es bastante fácil de usar si eres primeriza. Yo usé uno de bambú con mi tercer hijo porque nació en agosto y era el tejido más fresquito que encontré… Pero me duró poco porque enseguida pesó demasiado y tuve que irme a uno semi elástico, pero igualmente agradable.

 ![](/tumblr_files/tumblr_inline_ozgj5qe24m1qfhdaz_540.png)

Existen mil formas de anudarlo: por delante, en posición cuna para que pueda mamar, a la espalda, a la cadera…

En Teo y Leo te enseñan a llevarlo y te asesoran sobre el tipo de portabebés que más se adecúa a tus necesidades y las de tu hijo en función de su peso, el uso que le vayas a dar, el clima de tu ciudad… y además, puedes elegir entre multitud de estampados a cuál más original para ser fiel a tu estilo, incluso puedes elegir la tela de tu mei tai para que el tuyo sea único, como tu bebé.

## Mei Tais personalizados

La primera vez que oí hablar de este tipo de porteo no sabía ni lo que era, pero me fascinó su historia, su procedencia, y sobre todo, lo fácil y cómodo que es de anudar. 

Sin duda, es mi favorito!

Un [mei tai es una mochila portabebés](http://www.teoyleo.com/es/6-mei-tai) de tela cuyo origen está en el sur de china, donde las madres que trabajan en los campos de arroz, llevan a sus hijos a la espalda, atados con unas bonitas telas llamadas beidao o mei tai.

Una vez puesto el mei tai, los tirantes quedan cruzados y el peso se reparte por todo el cuerpo, por lo que casi no notas el peso del bebe.

 ![](/tumblr_files/tumblr_inline_ozgjbr6bhM1qfhdaz_540.png)

En Teo y Leo están continuamente incorporando nuevas telas a su catálogo para poder ofrecer un amplio abanico de colores y dibujos para que elijas y te lleves un mei tai exclusivo totalmente a tu gusto. Los tejidos son de alta calidad y están confeccionados con todo el cariño de un equipo que cree en lo que hace.

 ![](/tumblr_files/tumblr_inline_ozmk8nx3aY1qfhdaz_540.png)
## Llévate todo a juego

Además, puedes elegir complementos, como patucos, sacos o fundas con el mismo diseño, o haciendo juego, y te confeccionan toda la línea de productos que vayas a necesitar a tu gusto.

 ![](/tumblr_files/tumblr_inline_ozgjgkYL451qfhdaz_540.png)
## Las mochilas ergonómicas más cañeras

A la hora de comprar una mochila tenemos que fijarnos en que sea ergonómica, y no una colgona. Para no confundirte debes fijarte en que el bebé quede en posición ranita, no con las piernas colgando.

Suelen ser colgonas también las que en las instrucciones dan la opción de ponerlo mirando hacia fuera o hacia a ti, las que no llevan sujeta cabezas, y su estructura es rígida, no adaptable a la columna vertebral del bebé, que deberá quedar en forma de “C” para que su posición sea correcta.

Todas las [mochilas que tienen en Teo y Leo son ergonómicas](http://www.teoyleo.com/es/98-mochila-portabebes), pero nada convencionales. Si sois una familia cañera, ésta de calaveras os va a encantar.

 ![](/tumblr_files/tumblr_inline_ozgj479YmH1qfhdaz_540.png)

Mira qué bolsito para hacer juego

 ![](/tumblr_files/tumblr_inline_ozmkc25PdI1qfhdaz_540.png)

Y ya, si les encargas estos patucos, quedará de miedo!

 ![](/tumblr_files/tumblr_inline_ozgjnaIPjU1qfhdaz_540.png)

Pero tienen cantidad de telas para que elijas los que más te gusten. Te los hacen exclusivos para ti!

 ![](/tumblr_files/tumblr_inline_ozmkar3QGE1qfhdaz_540.png)
## Gran variedad de bandoleras

Teo y Leo también tiene gran variedad en [bandoleras con anillas](http://www.teoyleo.com/es/109-bandolera-anillas), que se pueden usar desde el nacimiento hasta el año, por si prefieres llevar a tu bebé de esta forma, o para determinadas ocasiones.

 ![](/tumblr_files/tumblr_inline_ozgjr93PGb1qfhdaz_540.png)

Las bandoleras son súper prácticas. Suelen usarse como portabebés de apoyo, ya que cargan el peso en diagonal desde un hombro, pasando por la espalda, hasta la cadera contraria, por lo que no suele ser cómodo para usarlas durante muchas horas, pero sí que viene genial para combinarlas con carrito o con otros porta bebés.

Incluso hay unas [bandoleras geniales para bañarse](http://www.teoyleo.com/es/112-portabebes-pouchs)en el mar o en la piscina con el peque.

Como ves, hay una solución para cada mamá o papá que quiera portear. Si has elegido esta forma de llevar a tu bebé, o la quieres combinar con el carrito, pásate por la [tienda de Teo y Leo on line](http://www.teoyleo.com/es/) o, si vives en Zaragoza, acércate a verlos, te ayudarán a encontrar tu portabebés ideal.