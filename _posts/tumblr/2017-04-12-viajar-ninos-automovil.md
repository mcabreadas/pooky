---
date: '2017-04-12T18:08:31+02:00'
image: /tumblr_files/tumblr_inline_ooazjuSdtr1qfhdaz_540.png
layout: post
tags:
- crianza
- seguridad
- planninos
- familia
title: Cómo viajar con los niños en el coche
tumblr_url: https://madrescabreadas.com/post/159493689259/viajar-ninos-automovil
---

Raro es que alguna familia se quede en casa en vacaciones porque, aunque no hagamos grandes viajes, seguro que alguna [escapada o excursión hacemos con los niños](/2015/06/28/viajes-ninos.html) para cambiar de aires y desconectar un poco de la rutina diaria.

Todos los años en fechas señaladas como las vacaciones de Navidad, la [Semana Santa](/2016/03/14/comprar-maleta-viajar.html), el verano, o puentes como el puente de la Constitución o de la Inmaculada se espera mayor intensidad de circulación de vehículos, ya que muchas familias optan por viajar en coche, a las mismas horas, días e incluso destinos parecidos, mayormente, destinos de playa. 

El año pasado en Semana Santa, se esperaban más de 14 millones de desplazamientos de largo recorrido según la DGT, y esta Semana Santa aún se esperan más. 

Entre estos pasajeros vamos a tener muchos niños a los que los padres vamos a tener que entretener durante largas horas de viaje.

## ¿Viajes largos en coche con niños?

Depende de a qué se denomine largo. En general claro que es posible realizarlos, pero siempre recordando que los niños se cansan, se aburren y se marean. 

Y que en función de su edad hay que cuidar una serie de aspectos importantes, como el número de paradas a realizar.

Se recomienda hacer paradas para que los niños puedan moverse, relajarse e incluso desahogarse un rato corriendo o simplemente disfrutando del juego. 

La frecuencia de las paradas además va a depender de la edad del niño y de sus horarios de alimentación. 

Por ejemplo, un lactante de meses necesitará paradas más frecuentes que un adolescente de catorce años.

## ¿Pantallas sí o no?

Muchos padres intentamos entretenerlos durante el viaje con video juegos u ordenadores En general no es aconsejable utilizar pantallas en cualquier menor de dos años. 

En niños mayores y si estas van bien fijas o incluso forman parte de la estructura del vehículo, se pueden utilizar en los asientos traseros, ninca enlos delanteros para no despistar al conductor. 

Lo que no es recomendable es que haya objetos sueltos y menos en manos de los niños, pues un simple frenazo puede hacer que impacten sobre cualquiera de los ocupantes.

## Juegos y entretenimientos

Como en cualquier edad o situación, juegos adaptados a su edad. Suele ayudar mucho llevar canciones que le gusten y que conozcan, porque así puede cantarlas. 

 ![](/tumblr_files/tumblr_inline_ooazjuSdtr1qfhdaz_540.png)

Cuentos para leerle o jugar a juegos como el «Veo, veo». 

Pero lo más útil es planificar bien el viaje para poder parar cuando los niños muestren signos de fatiga, cansancio o mareo.

## ¿Y si se marean?

Que realicen viajes cortos o los planifiquen bien para dividirlos en tramos o poder parar si la situación lo requiere. Muchos de estos niños logran adaptarse con viajes cortos y frecuentes, pero otros no, por lo que tampoco se debe forzar la situación.

## Seguridad también en desplazamientos cortos

Pero, aunque se trate de un desplazamiento corto, no es menos importante procurar que las condiciones del viaje sean las más seguras posibles para los niños.

 ![](/tumblr_files/tumblr_inline_ooayharlCr1qfhdaz_540.png)
## La importancia de la planificación

Es esencial la planificación, por ejemplo como cuidar el confort, la comida e incluso la ropa de los niños para que viajen cómodos. 

Pero lo más importante es planificar el viaje adaptándonos a ellos, porque ellos difícilmente se van a adaptar a nuestras necesidades. Y nadie conoce mejor a nuestros hijos que nosotros.

Si el viaje se planifica mal, la culpa no es del niño.

## Sistema de retención adecuado

Usar un [sistema de retención infantil adecuado a su estatura y peso](http://www.race.es/seguridadvial/formacion-race/sillas-infantiles/la-nueva-normativa-i-size) es fundamental. Si tienes dudas, lo mejor es buscar asesoramiento en tiendas especializadas. 

**Nueva normativa sobre SRI: Abril 2017**

- El cambio más importante es la consideración de la estatura para la clasificación de las sillitas de seguridad infantil.   
- Desaparece la clasificación por grupos (como la conocíamos ahora: grupo 0, I, II, etc.) para nombrar a las sillas.  Éstas a partir de ahora pasarán a clasificarse por tallas (peso y altura).   
- La posición de la sillita con respecto al sentido de la marcha también dependerá de la altura del niño:  
- Si ésta es igual o menor de 71 cm deberán colocarse en sentido contrario a la marcha.  
- Los niños con estatura mayor de 71cm podrán ir mirando hacia delante.  
- Los niños de estatura entre 71 y 83 cm pueden viajar en los dos sentidos, aunque se recomienda el sentido contrario a la marcha según la norma. pero ya sabéis que yo recomiendo siempre que viajen a contra marcha el mayor tiempo posible, según te cuento [aquí](/2014/11/12/sillas-automovil-seguridad-ninos.html), [aquí](/2015/11/29/acontramarcha-minikid.html) y [aquí](/2016/02/18/espejo-protector-contramarcha.html).  
- Otro de los cambios va afectar a los elevadores sin respaldo, ya que estos sólo serán adecuados para niños mayores, niños que pesen más de 22 kg y superen la altura de 125 cm.  
 ![](/tumblr_files/tumblr_inline_ooayp1Ffer1qfhdaz_540.jpg)
## ¿Niños en el asiento delantero?

Los niños/bebés sólo podrán viajar en el asiento delantero cuando midan 135 cm o más, o bien se dé una de estas circunstancias: 

- Que el vehículo no tenga plazas traseras. Es el caso de los biplaza o las furgonetas.  
- Que los asientos traseros estén ocupados por menores en la misma situación (estatura de 1,35 o menor).   
- Que no sea posible instalar en dichos asientos traseros todos los sistemas de retención infantil.
 ![](/tumblr_files/tumblr_inline_ooayd9zIm71qfhdaz_540.png)
## Llevar dentro del vehículo lo necesario en caso de accidente
  

El chaleco reflectante es obligatorio si se sale del vehículo en caso de avería y que quede estacionado en el arcén de la autovía. Dada la alta velocidad a la que se circula es más aconsejable salir del turismo y permanecer al otro lado del quitamiedos, pero antes habrá que señalizar el vehículo con el triángulo de emergencia. 

En el caso de avería en carreteras nacionales se procede de igual manera, pero en este caso. 

Los bebés no tienen por qué llevar chaleco reflectante, los niños si el vehículo dispone de él, sí.

 ![](/tumblr_files/tumblr_inline_oob03a73dH1qfhdaz_540.jpg)

_Foto: [https://www.aseguramicochedealquiler.es](https://www.aseguramicochedealquiler.es)_

Ah! Y un consejo, los viajes en familia, y el tiempo que paséis todos juntos en el coche son una oportunidad de oro para dialogar, escucharos unos a otros y compartir tiempo, que ya sabes que es el regalo más valioso que puedes ofrecer a tus hijos.

¡Buen viaje!