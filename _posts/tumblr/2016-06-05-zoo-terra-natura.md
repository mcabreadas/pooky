---
date: '2016-06-05T19:01:58+02:00'
image: /tumblr_files/tumblr_o87j5yVZgn1qgfaqto1_400.gif
layout: post
tags:
- crianza
- planninos
- local
- educacion
- familia
- gif
title: zoo terra natura
tumblr_url: https://madrescabreadas.com/post/145460666013/zoo-terra-natura
---

![](/tumblr_files/tumblr_o87j5yVZgn1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_o87j5yVZgn1qgfaqto2_400.gif)  
  

## Aventuras salvajes en Terra Natura Murcia. Atrévete

Osito, mi segundo hijo es un apasionado del mundo animal, le encanta aprender formas de vida, costumbres, alimentación y todo tipo de curiosidades sobre las distintas especies.   
Últimamente me sorprende a menudo con comentarios y anécdotas de animales, y los cuenta fascinado.  
Por eso, cuando se enteró de que nos habían invitado al Safari VIP de Terra Natura Murcia su emoción fue extrema, ya te puedes imaginar…  
Contando los días por fin llegó la fecha prevista para tamaño evento. 

Fue una pena no poder ir todos, pero justo el pequeño, Leopardito, se puso enfermo, y su papá se quedó en casa cuidándolo, mientras el resto de la familia nos fuimos de safari.

 ![](/tumblr_files/tumblr_inline_o8b2ji4TtM1qfhdaz_540.jpg)

**Qué significa Safari VIP**

La idea era mostrarnos de primera mano, a través de la experiencia de los cuidadores de los animales, el parque zoológico desde otro punto de vista, es decir lo que no se ve en una visita normal, lo que hay detrás de Terra Natura Murcia.

 ![](/tumblr_files/tumblr_inline_o8b2jiI1xC1qfhdaz_540.jpg)

Así, Nicolás, Richi, Víctor y y Gema, acompañados por la responsable del parque, Carmina, nos mostraron desde dentro las maravillas de este zoo.

**Los cuatro pilares del zoo**

Pero antes, Manolo, con más de 20 años de experiencia en este mundo, nos contó su historia, y los pilares básicos en los que se basa la labor que allí se hace: conservación y solidaridad medioambiental, acompañados de educación y respeto hacia los animales; 

“todo por y para los animales”

**Una idea de zoológico equivocada**

Afortunadamente el concepto actual de zoológico es muy diferente al que conocimos en nuestra niñez que consistía en animales enjaulados para el deleite de los visitantes.

 ![](/tumblr_files/tumblr_inline_o8b2jjjthf1qfhdaz_540.jpg)

No se trata de capturar animales para encerrarlos, sino que se acogen o rescatan incluso para curarlos, llegando incluso a criar ejemplares en peligro de extinción para proteger su especie y luego soltarlos a su hábitat natural para repoblar.

También ha cambiado la forma de los espectáculos o exhibiciones, ya que ahora no se basan en la “humanización” de los animales, ni se les ridiculiza:

Seguro que recuerdas haber visto de niña osos tocando la trompeta o loros montando en bicicleta.

Ahora, comenta Richi (veterinario):

Afortunadamente, esto ya no se hace

**Responsabilidad medioambiental**

El parque tiene una responsabilidad con los animales,

A veces les llegan algunos ejemplares heridos para que los curen, y todos los que nacen en el parque son acogidos y cuidados.

**Chiky vino de un circo**

Esta “labor social” dentro del mundo animal queda patente en el caso de Chiky, la rinoceronte más cariñosa que hemos conocido, que estuvo comiendo de la mano de mis hijos zanahorias y manzanas, y que en una vida pasada, trabajó en un circo, y llegó a Terra Natura Murcia en un estado lamentable, sólo sabía dar vueltas, y estaba llena de heridas…

 ![](/tumblr_files/tumblr_inline_o8b2jjZiw11qfhdaz_540.jpg)

Aquí la acogieron y cuidaron, y ahora tiene un hogar donde se la respeta y quiere.

¡Mis hijos se encariñaron mucho con ella!

**Una gran familia**

Es suficiente escuchar la forma de hablar de los cuidadores sobre los animales para percatarse de que los consideran parte de su propia familia.

Me di cuenta de que el suyo es un trabajo puramente vocacional y, de que los cuidan y alimentan como si fueran sus hijos.

**Fui una de las primeras visitantes**

El origen de Terra Natura Murcia es el antiguo zoo municipal.

Con una gran superficie de 126.000 metros cuadrados, y más de 50 especies distintas en su hábitat natural, abrió sus puertas en 2007, y he de decir que debí de ser una de las primeras en visitarlo, porque recuerdo que llevamos a mi hija mayor, Princesita, con un añito, cuando yo estaba embarazada de Osito, hace 9 años.

Actualmente ya superan el millón y medio de visitantes, muchos de ellos, escolares, ya que la labor educativa del parque es uno de sus pilares.

**Una lección de ciencias naturales en vivo**

Los niños aprenden de una forma espontánea cosas que no están en los libros, y que difícilmente olvidarán porque las han vivido Aprenderán que **cebras** y **gacelas** conviven en la sabana africana. 

 ![](/tumblr_files/tumblr_inline_o8b2jjVupC1qfhdaz_540.jpg)

Que el **puercoespín** usa sus púas para protegerse de los depredadores, y que es muy difícil que le hinquen el diente porque las abre como si fueran plumas por su parte trasera. Por eso, siempre nos dará la espalda.

 ![](/tumblr_files/tumblr_inline_o8b2jkjBPv1qfhdaz_540.jpg)

Que las **tortugas** terrestres pueden llegar a vivir hasta 150 años

 ![](/tumblr_files/tumblr_inline_o8b2jlTB9A1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o8b2jlvb4a1qfhdaz_540.jpg)

Que los huevos de las **avestruces** son más insípidos que los de gallina

 ![](/tumblr_files/tumblr_inline_o8b2jmoFG41qfhdaz_540.jpg)

Que las **jirafas** pueden llegar a medir 5 metros, que nunca se llegan a tumbar del todo cuando duermen, no se sabe exactamente por qué, y que su lengua mide 50 cm.

Ah, y también se comen todo el apio que puedas darles.

Cara a cara me parecieron adorables, y sus preciosos ojos me conquistaron

También aprendieron que los **lémures** de cola anillada proceden de la isla de Madagascar, aunque esto ya lo sabían por el famoso Rey Julian, y que les gustan las pasas. 

Osito estuvo alimentándolos, y dice que aprietan fuerte si te cogen la mano.

 ![](/tumblr_files/tumblr_inline_o8b2jnKB5E1qfhdaz_540.jpg)

Que los **leopardos** son fieros e indomables, aunque tremendamente bellos. Nuestro amigo está a la espera de una hembra para que le haga compañía. en breve le mejorará el humor.

 ![](/tumblr_files/tumblr_inline_o8b2jo7Ue41qfhdaz_540.jpg)

Y que Terra Natura es parte de un programa europeo para protección del **mono** Bazza.

 ![](/tumblr_files/tumblr_inline_o8b2joZ03s1qfhdaz_540.jpg)

Aprendieron que los **suricatos** comen gusanos, por eso Timón es el encargado de convencer a Simba de que se deje la carne y se pase a los gusanos en la película El Rey León.

**La guarida de los leones**

Y, lo más impresionante que he visto, además, en primicia porque nunca antes ninguna persona ajena al zoo había entrado a la guarida de los leones

Aquí tenéis la [retransmisión que hice en directo a través de Periscope](https://www.periscope.tv/w/aiH59jFlZGpuTGdHWk1NUW98MXZBeFJSV3JnemF4bKmxt6dYQV2Nq1N7YB-r9hq5XITcWM7JIIEbcXftScwN), y los sobresaltos que me llevé cada vez que me rugían.

 ![](/tumblr_files/tumblr_inline_o8b2jqMzgn1qfhdaz_540.jpg)

¡Qué impresión! 

**Gran oferta de actividades**

Te dejo un enlace con las [actividades que ofrece Terra Natura Murcia](http://murcia.terranatura.com/es/terranatura/11-educacion.html) para familias y también para grupos escolares:

- Puedes celebrar tu Comunión o tu cumpleaños  
- Vivir la experiencia de un safari nocturno  
- Bañarte con leones marinos  
- Entrar en la guarida de los leones  
- convertirte en entrenador de aves o leones marinos  
- Jugar con los lémures  
- Celebrar de una forma especial carnaval, Halloween o Navidad.  
- Además, hay también un parque acuático para los meses de calor  
- y cada año organizan una escuela de verano para cuando los niños terminan las clases en el cole  

La experiencia ha sido inolvidable. Además, la compañía fue la mejor.

¡[Ana](http://mimundolosmios.es), gracias por las fotos! ![](/tumblr_files/tumblr_inline_o8b2jrSNhO1qfhdaz_540.jpg)

Muchas gracias a Terra Natura por habernos enseñado tantas cosas y habernos dado la oportunidad de vivir una tarde tan animal.

Gracias Carmina, Richi, Nicolás, Víctor, Gema, Miriam, y a todos los que trabajásteis con tanto cariño para que saliera todo tan bien.

## Si quieres ver todas las fotos del safari:

Para acceder a la [galería de fotos completa del safari pincha aqu](https://www.flickr.com/photos/142190598@N06/sets/72157669284933185/with/27406594456/)í.

 ![](/tumblr_files/tumblr_inline_o8b2jrkPif1qfhdaz_540.jpg)

¡Volveremos!

¿Y tú, te atreves?

## Video de la experiencia
<iframe width="100%" height="315" src="https://www.youtube.com/embed/SBUGRcFxjeQ?rel=0" frameborder="0" allowfullscreen></iframe>