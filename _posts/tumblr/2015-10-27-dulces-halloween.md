---
date: '2015-10-27T10:00:13+01:00'
image: /tumblr_files/tumblr_inline_o39z7vhEre1qfhdaz_540.jpg
layout: post
tags:
- halloween
- planninos
- fiestas
- familia
- recetas
title: El top ten de los dulces de Halloween
tumblr_url: https://madrescabreadas.com/post/132007988354/dulces-halloween
---

Ya tenemos la cabeza en Halloween un año más. Queramos o no, estamos atrapados en esta vorágine anglosajona de importación que nos lleva a disfrazarnos, comer dulces y, salir a la calle en busca de truco o trato con los niños.

Este año te propongo que hagas tú mismo los dulces, y sorprendas a tus hijos y a sus amigos con una merendola digna de este día.

He seleccionado los diez dulces que más me han gustado, y te he puesto un enlace en cada uno, que te lleva a la explicación paso a paso de cómo se hacen y, aunque esté en inglés, no tendrás problema en entenderlo, ya que viene con muchas fotografías, incluso videos.

Vamos a ello:

## 1-Tortitas con la cara de Jack Skeleton

Si quieres que tus tortitas recuerden al protagonista de la película Pesadilla antes de Navidad, sólo tienes que seguir [estos sencillos pasos](http://www.instructables.com/id/Jack-Skelleton-Halloween-Pancakes/?ALLSTEPS), y las tendrás en unos minutos.

 ![](/tumblr_files/tumblr_inline_o39z7vhEre1qfhdaz_540.jpg)
##   

## 2-Manos de palomitas

Tan sencillo como original. Darás la campanada este Halloween con estas terroríficas manos de palomitas con uñas y todo. ¡Me encantan!

 ![](/tumblr_files/tumblr_inline_o39z7vgTGF1qfhdaz_540.jpg)

[Aquí tienes cómo hacerlas](http://www.instructables.com/id/Halloween-Popcorn-Hands/). Sólo se necesitan guantes transparentes, palomitas y, para las uñas, [candy corn que, si no encontráis, podéis comprarlos aquí](http://www.aceroymagia.com/refrescos-snacks/dulces-y-caramelos/bolsa-sathers-candy-corn-dulces).

## 3-Cupcakes de fantasmas

¿No os resultan adorables? Estos dulces llevan algo más de trabajo, pero quedan ideales.

Lo que sujeta a los fantasmas de fondant son chupachups. ¿No son geniales?

 ![](/tumblr_files/tumblr_inline_o39z7vzQpQ1qfhdaz_540.jpg)

Os dejo el [paso a paso](http://www.instructables.com/id/Haunted-Pumpkin-Patch-Ghost-Cupcakes/?ALLSTEPS) para quien se atreva.

## 4-Cupcakes de Frankenstein

Sin duda darán el toque de color a vuestra fiesta estos cupcakes de Frankenstein, que además, tienen que estar buenísimos.

 ![](/tumblr_files/tumblr_inline_o39z7wj3EQ1qfhdaz_540.jpg)

[Aquí se explica cómo hacerlo al detalle](http://www.instructables.com/id/Frankenstein-Cupcakes/?ALLSTEPS).

## 5-Tumbas de brownies

Estos Brownies con lápida son super originales y tienen una pinta terroríficamente exquisita (hasta les han puesto plantas, menuda imaginación).

 ![](/tumblr_files/tumblr_inline_o39z7wJok11qfhdaz_540.jpg)

[Cómo hacerlos, aquí](http://www.instructables.com/id/Graveyard-Brownies/).

## 6-Sandía cerebro de Frankenstein

Quizá tus hijos coman fruta ese día gracias a este original cerebro de Frankenstein hecho con una sandía.

De verdad que es facilísimo, para el resultado que se obtiene. Sólo hay que quitarle la corteza dejando la capa blanca que tiene debajo, haqcerle los dibujos, y el color interior propio de la sandía hará el resto.

 ![](/tumblr_files/tumblr_inline_o39z7wJr1W1qfhdaz_540.jpg)

[La forma de hacerlo es muy ingeniosa, aquí la tienes](http://www.instructables.com/id/Frankensteins-Watermelon-Brain/?ALLSTEPS).

## 7-Manzanas envenenadas

Tan sencillas como deliciosas, quedan muy resultonas.

[Aquí te dejo el paso a paso](http://www.instructables.com/id/Candied-Poison-Apples/?ALLSTEPS) por si quieres envenenar a tus invitados

 ![](/tumblr_files/tumblr_inline_o39z7w1fYz1qfhdaz_540.jpg)

El truco para que queden con ese acabado brillante es la [purpurina roja comestible que podéis encontrar aquí](http://www.marialunarillos.com/4487-purpurina-comestible-rd-roja.html).

## 8-Ojos de chocolate

Estos ojos de chocolate darán colorido propio de Halloween a la mesa.

 ![](/tumblr_files/tumblr_inline_o39z7xV5Hb1qfhdaz_540.jpg)

La clave está en decorarlos con [fideos de azúcar naranjas, que puedes encontrar aquí](http://www.caprichosdegoya.com/reposteria-ingedientes-colorantes-articulos-de-decoracion-tartas/reposteria-decoracion-silicone-gold-fideos-azucar-naranja).

Los ojos de caramelo dan muchísimo juego en estas fechas, los puedes usar incluso para decorar tartas. [Aquí he encontrado ojos pequeños](http://latartienda.com/tienda/halloween/1682-ojos-de-caramelo.html), pero hay de varios tamaños.

[Aquí tienes cómo se hace](http://www.instructables.com/id/One-Eyed-Sticky-Chocolate-Balls/?ALLSTEPS).

## 9-Arañas de galletas

Simplemente con unas Oreo, o cualquier otra galleta de color negro doble con crema dentro, y unos regalices negras, tendrás unas deliciosas galletas de araña.

 ![](/tumblr_files/tumblr_inline_o39z7xrC0L1qfhdaz_540.jpg)

[Cómo hacerlas, aquí](http://www.instructables.com/id/Super-Simple-Spider-Cookies/).

## 10-Cupcake zombie

El plato fuerte lo he dejado para el final, no fuera a ser que no terminaras de leer el post, porque hay que tener estómago para comerse esto, aunque no digo yo que no esté bueno.

 ![](/tumblr_files/tumblr_inline_o39z7xN3KQ1qfhdaz_540.jpg)

Por si alguien se anima, aquí os dejo las [instrucciones](http://www.instructables.com/id/Zombie-Mouth-Cupcake/?ALLSTEPS).

Espero que disfrutéis de estas terroríficas delicias y tengáis una divertida noche de Halloween.

Ah! Y que me contéis cómo os han salido, y si es con foto, mejor.

¿Quien se atreve?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.