---
layout: post
title: "Conciliación y vacaciones: los abuelos piden auxilio"
date: 2014-04-12T09:29:00+02:00
image: /images/uploads/depositphotos_4763467_xl.jpg
author: .
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/82460025137/conciliación-y-vacaciones-los-abuelos-piden
---
Ya no saben cómo hacernos saber que no pueden más… entre la artrosis, el colesterol, y la astenia primaveral, necesitan un respiro de nietos porque, aunque los quieren con toda su alma, hemos de reconocer que agotan al más pintao.

Y ahora se ven encima las “vacaciones” de Semana Santa sin cole y se echan a temblar porque, claro, los papás nos tomaremos unos días y podremos estar con los peques, en mi caso, sólo las fiestas de guardar, pero, ¿y el resto de jornadas? ¿Qué hacemos con los niños? ¿Nos los comemos, [como dice un amigo mío](http://losangelesdepapi.blogspot.com.es/2014/02/papa-mama-manana-no-hay-colegio.html)? ¿Nos gastamos el sueldo en ludotecas-talleres-campamentos de ciudad-guarderías-expediciones a la selva? Noooooo…. Teniendo abuelos, con quién van a estar mejor…

Menos mal que nos lo dicen con humor, al menos nuestra abuela… pero el mensaje está muy claro. Imaginaos cómo me quedé cuando abrí el guasap y mi madre me había enviado este video…. glup!

Y eso que no sabía mandar videos hace unos días… ni usar guasap… ni móvil casi… ejem… 

Tomo nota, mamá… intentaré no abusar y no reñirte por las chuches…