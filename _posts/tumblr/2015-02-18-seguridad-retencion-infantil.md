---
date: '2015-02-18T09:30:00+01:00'
layout: post
tags:
- crianza
- colaboraciones
- puericultura
- ad
- familia
- seguridad
- bebes
title: Seguridad infantil. Normativa I-Size sobre sistemas de retención en automóvil.
tumblr_url: https://madrescabreadas.com/post/111360074484/seguridad-retencion-infantil
---

La seguridad infantil es una de las obsesiones de los padres. Desde el momento en que volvemos a casa desde el hospital con el bebé recién nacido, y lo metemos en su sillita en el coche a contra marcha nos damos cuenta de lo vulnerables que son, y nos da un vuelco el estómago porque nos surge la duda de si lo vamos a hacer bien, si seremos capaces de protegerlo correctamente.

Por eso no podía dejar pasar la invitación de Jané para informarme de primera mano, gracias a David Lay, del blog [Y papá también](https://ypapatambien.wordpress.com/) (altamente recomendable), de la normativa europea para los sistemas de retención, y visitar el Crash Test Center, donde sus productos pasan pruebas de impacto de las que fuimos testigos.

David Lay nos cuenta lo que allí aprendió:

“El pasado jueves día 12 tuve el placer de asistir como “corresponsal” de María, del blog Madres Cabreadas (gracias por la oportunidad), al evento que organizó Jané en Barcelona para enseñar a unas cuantas (y un cuanto) bloggers, el centro de distribución que tiene la empresa en la Palau de Plegamans y en el que tienen instalado el Crash Test Center donde realizan las pruebas de seguridad de las sillitas para coche.

Primero de todo agradecer a Jané su atención desde que nos vinieron a recoger en un autobús para llevarnos a sus instalaciones hasta que nos volvieron a dejar en el mismo lugar y muy especialmente a Natalia y Anna del departamento de comunicación y a Joan Forrellad, vicepresidente de Jané y nuestro cicerone en la visita al complejo.

## Un buen recibimiento

Para iniciar la jornada nos recibieron con un almuerzo para recargar pilas tras el desplazamiento y luego nos dieron una pequeña charla acompañada de una presentación sobre la nueva normativa para los sistemas de retención infantil para automóvil, que poco a poco se está implementando.

## Cifras que hablan por sí solas

En la presentación (muy interesante por cierto), nos comentaron algunos datos que tenemos que conocer y que nos tienen que concienciar a la hora de hacer un correcto uso de los sistemas de seguridad infantiles:

-        En Europa mueren al año 700 niños, y 80.000 resultan heridos en accidentes de tráfico.

-        Uno de los principales causantes de esas cifras es el Misuse (mal uso de los sistemas de fijación por parte del usuario). Bien sea por desconocimiento o por falta de información y/o preparación de las tiendas que comercializan las sillas (punto para Jané por reconocer su error).

## El reglamento europeo I-SIZE

Si hasta ahora la normativa conocida era la R44, ésta poco a poco va a ir desapareciendo para dejar paso a la I-Size. El reglamento ECE R129, también conocido como [“i-Size”, es el nuevo reglamento Europeo sobre sillas de seguridad infantiles.](http://www.jane.es/files/recursos/i-size/ISIZE_es.pdf)

## Su objetivo

El objetivo de la nueva normativa I-Size es precisamente ése, reducir el mal uso de los sistemas de seguridad para niños, creando una manera mucho más sencilla y explícita de explicar las normas de instalación (no dejando lugar a dudas).

## Obligatoriedad del Isofix

Otro de los puntos que incluye la nueva normativa es el uso obligatorio de Isofix desde los 0 a los 4 años (105 cm.) para todas las sillas que se instalen en el vehículo, 

## Obligatoriedad de la contramarcha

así como la olbigatoriedad de la contramarcha (RF) hasta los 15 meses (71 cm.)

## Desaparecen los grupos

Otra dos novedades interesantes en relación al I-Size es que desaparece la clasificación por grupos (como la conocíamos ahora: grupo 0, I…), para nombrar a las sillas.  Éstas a partir de ahora pasarán a clasificarse por tallas (peso y altura). 

## Desaparecen los cojines elevadores

Y también desaparecerán los alzadores que actualmente se utilizan, quedando sólo para casos especiales y niños a partir de 10 años.

Los productos deben superar una prueba de impacto lateral adicional para obtener la homologación. 

## Crash Test Center de Jané

Tras la presentación de la nueva normativa I-Size y que se irá implementando poco a poco para acabar de estar totalmente operativa a partir de finales de 2016, asistimos a una demostración de cómo funciona el Crash Test Center donde realizan las pruebas de seguridad de los sistemas.

<iframe src="https://www.youtube.com/embed/f2Ou-m41Cy0?rel=0" width="560" height="315" frameborder="0"></iframe>

<iframe src="https://www.youtube.com/embed/E17tDE_c6gA?rel=0" width="560" height="315" frameborder="0"></iframe>

## El dato curioso

Una curiosidad: ¿Sabéis cuánto cuesta un muñeco (dummies) con los que se realizan las pruebas de impacto? Pues la nada cercana cifra de 70.000 euros!!

## Comida en buena compañía

Para acabar la jornada en Jané, nos llevaron a comer a un restaurante de Barcelona ciudad y luego nos volvieron a dejar con el autobús que nos hizo compañía durante toda la jornada para volver a nuestro lugar de residencia.

Sin duda fue una gran experiencia el hecho de poder conocer cómo funciona el Crash Test Center de Jané y la cantidad de pruebas que realizan para que puedan llegar a nuestras manos los mejores sistemas de retención infantil para nuestro vehículo y así poder llevar a nuestros pequeños con la mayor seguridad posible.Aunque nunca me cansaré de repetir: El único y final responsable de nuestros hijos en el coche somos nosotros, nunca juguemos con la seguridad de ellos y hagamos las cosas bien. De nuevo gracias a Madres Cabreadas por dejarme ser su corresponsal en la jornada y permitirme aprender sobre seguridad infantil de la mano de Jané.

Gracias a ti, David, por acudir a la cita con Jané y con la seguridad de nuestros pequeños.

Y como tengo deformación profesional, y eso de los plazos me preocupa bastante, creo que debemos tener en cuenta esto:

## Calendario de aplicación de la norma I-SIZE

Actualmente conviven la normativa anterior, y la I-Size, pero¿ hasta cuándo?

**Hasta el año 2018** se podrán fabricar, por parte de los fabricantes acreditados, sillas homologadas según la normativa i-Size. A partir de ese momento todas las sillas que se fabriquen tendrán que estar homologadas según la normativa i-Size.

**Hasta el año 2023** los grandes distribuidores y tiendas podrán vender sillas sin estar homologadas según la normativa i-Size. A partir de ese momento no se nos permitirá vender ninguna silla que no cumpla con esto. En caso de que alguna tienda siga vendiendo sillas sin los estos requisitos el podrá ser fuertemente sancionada.

**Hasta el año 2028** los usuarios podrán usar sillas auto sin la homologación del regamento i-Size en sus automóviles. A partir de ese momento quedará totalmente prohibido el uso de sillas auto no homologadas según esta norma y las autoridades de tráfico tendrán competencia para para sancionar a todos aquellos padres que tengan montadas sillas auto en sus vehículos que no cumplan la norma i-Size

¿Estabas al tanto de las novedades en materia de sistemas de retención infantil para automóvil?