---
date: '2014-12-16T08:05:00+01:00'
image: /tumblr_files/tumblr_inline_ne1l9xdmhO1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- regalos
- premios
- colaboraciones
- puericultura
- ad
- recomendaciones
- familia
- bebes
title: 'Concurso Tu carta a los Reyes Magos: Hamaquita de lunares de Babyjörn'
tumblr_url: https://madrescabreadas.com/post/105337458489/concurso-tu-carta-a-los-reyes-magos-hamaquita-de
---

ACTUALIZACIÓN 28-12-14

Con el fin de que los regalos lleguen a su destino a tiempo para Reyes  **se cerrará el concurso el 29-12-14 a las 8:00 am.**

Los cinco participantes cuyas cartas hayan obtenido más comentarios de personas diferentes tendrán que mandarme un email a  **mcabreada@gmail.com**  antes de las 12:00 am del día 29 con

-su nombre y apellidos, dirección y teléfono y email para el envío

Si algún participante no manda el email con su lista antes del 29-12-14 a las 12:00 el premio pasará al concursante cuya carta sea la siguiente en número de comentarios. Todo ello con la finalidad de que el envío de los regalos se haga lo antes posible.

ACTUALIZACIÓN 27-12-14

**SE ADELANTA LA FECHA TOPE PARA EL CONCURSO AL 29 -12**  
Me han propuesto adelantar la fecha de fin del concurso “Tu carta a los Reyes Magos” para que dé tiempo a que lleguen los regalos para esa noche tan especial, y creo que es muy buena idea. Así que se adelanta la fecha tope para subir vuestras cartas al lunes 29 de diciembre a las 00:00 h.

Vamos! Corre, corre!!

_Post originario:_

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

Uno de los regalos que podéis incluír en vuestra cata a los Reyes Magos para participar en el concurso es esta hamaca con diseño exclusivo en edición limitada de [Babybjörn](http://www.babybjorn.es/)

![image](/tumblr_files/tumblr_inline_ne1l9xdmhO1qfhdaz.jpg)

Cuando nación mi Princesita me regalaron una hamaca, y puedo decir que es de los pocos regalos que he usado con los tres y que encuetnro super práctico. Quizá sea porque al trabajar desde casa con el ordenador mientras los cuidaba, me permitía darle a la tecla mientras los mecía con el pie para que se durmieran después de darles el pecho. 

Esta hamaca de Babyjörn se puede usar desde los 0 meses porque proporciona apoyo adecuado de la cabeza y espalda de un bebé recién nacido. Además, el asiento de tela se amolda a su cuerpo distribuyendo el peso uniformemente. 

Se balancea de forma natural con los movimientos del peque, además de poder mecerlo tú con movimientos suaves incluso con el pie mientras los tienes cerquita mientras trabajas o haces otras cosas.

![image](/tumblr_files/tumblr_inline_ngiv4pPUV81qfhdaz.jpg)

Las hamaquitas se presentan para este otoño en un diseño exclusivo en edición limitada que me encanta.

Vivan los lunares!

Pero lo que más me gusta es que son versátiles y se puede usar hasta la edad de dos años. Una vez que el peque haya aprendido a andar y sentarse sin ayuda, puedes darle la vuelta a la tela para utilizarla como un cómodo asiento.

![image](/tumblr_files/tumblr_inline_ne1lzuO3121qfhdaz.jpg)

¿A que es ideal?

Para ganar este regalo tienes que participar en el concurso[“Tu carta a Los Reyes Magos”](/portada-regalos-reyes-magos) escribiendo tu carta pinchando [aquí](/portada-regalos-reyes-magos), y explicar  en la carta la edad que tiene tu peque, y si usarás la hamaquita como hamaca o asiento.

Las condiciones para participar están en el [post explicativo del concurso.](/portada-regalos-reyes-magos)

Tenéis hasta el 5 de enero!