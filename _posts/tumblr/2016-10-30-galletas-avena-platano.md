---
date: '2016-10-30T17:28:39+01:00'
image: /tumblr_files/tumblr_inline_ofvc0tnt641qfhdaz_540.png
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: Galletas de avena y plátano sin harina
tumblr_url: https://madrescabreadas.com/post/152513384259/galletas-avena-platano
---

¿No te ha pasado que de repente te un ansia irrefrenable de tomar algo dulce y acabas atiborrándote de lo primero que pillas por casa, ya sean galletas, chocolate, helado…?

Adelántate al desastre y ten preparadas cosas saludables que puedan satisfacer tu apetito dulcífero.

Tengo un truco para que los daños colaterales de estos ataques no sean demasiado gravosos para la báscula. Y es que recientemente he probado a hacer galletas con avena, en vez de con harina, y me ha encantado la idea! Hay mil maneras de hacerlas según los ingredientes que más te gusten 

Además, si las haces con frutas dulces, por ejemplo, plátanos maduros, te evitas poner demasiada azúcar, lo que es mucho más saludable.

Te cuento la receta de mis últimas galletas:

## Ingredientes
 ![](/tumblr_files/tumblr_inline_ofvc0tnt641qfhdaz_540.png)
- 3 plátanos maduros (de esos que no se come nadie por ser su aspecto marrón)  
- 1 ½ vaso de avena  
- pasas  
- azúcar vainillada  
- canela  

## Cómo se hace

- Precalienta el horno a 190ºC  
- Chafa los plátanos en un bol  
- Añade la avena poco a poco y ve mezclando hasta lograr una pasta  

 ![](/tumblr_files/tumblr_inline_ofvc2heknw1qfhdaz_540.png)
  

Añade las pasas

 ![](/tumblr_files/tumblr_inline_ofvc6dstv31qfhdaz_540.png)

- Espolvorea una cucharada de azúcar vainillada  
- Espolvorea una cucharadita de canela en polvo  
- Mézclalo todo: la pasta será muy viscosa y nada consistente porque no lleva harina.  
- Cubre una bandeja de horno con papel vegetal y ve haciendo bolitas con la masa y aplastándolas y poniéndolas sobre el papel vegetal. Lo normal es que salgan sin forma definida, no te preocupes, es normal.  
- No las hagas muy gruesas para que se hagan bien por dentro, aunque también están muy ricas con textura blandita.  
- Mételas al horno durante 20-25 min
 ![](/tumblr_files/tumblr_inline_ofvc3z1Cum1qfhdaz_540.png)

Al sacarlas retíralas de la bandeja del horno y déjalas enfriar en una rejilla.
 ![](/tumblr_files/tumblr_inline_ofvc9bh1bZ1qfhdaz_540.png)

Consérvalas en una caja metálica.

## Consejos prácticos
  

Si te gusta el chocolate (y no lo puedes evitar, como a mí), puedes añadir una cucharada de cacao en polvo del que te recomendé cuando te hablé del chocolate sin remordimientos, que le va a dar un sabor exquisito, y no añadiremos tantas calorías como si nos tomáramos un brownie, quién lo pillara.

Deja que tus hijos te ayuden a mezclar los ingredientes y a hacer las bolitas, les encanta!

Son ideales para poner de almuerzo para el cole, como tentempié a media tarde para nosotras o cuando te da un ataque nocturno de hambre, porque son bastante saciantes (tampoco te las comas todas de una como hice yo).

Bon appetit!