---
date: '2014-11-07T13:54:00+01:00'
image: /tumblr_files/tumblr_inline_pdpcltSSkV1qfhdaz_540.jpg
layout: post
tags:
- crianza
- colaboraciones
- ad
- eventos
- familia
- nutricion
- bebes
title: Últimas recomendaciones en nutrición infantil. Consejos prácticos.
tumblr_url: https://madrescabreadas.com/post/102004876384/nutricion-infantil-hero
---

Estar al día en las últimas recomendaciones sobre alimentación infantil es importante, al margen de que cada familia tenga sus propios criterios y preferencias a la hora de hacerlo. Por eso me interesó la idea de acudir a una charla sobre nutrición impartida por María Dolores Iniesta, del Instituto de Nutrición Infantil Hero a un grupo de madres y padres en las instalaciones de su fábrica en Murcia. Desde aquí agradezco la invitación y el exquisito trato recibido.

**Al día en los últimos estudios**

La charla estuvo a la altura esperada ya que se nos ofreció toda la información sobre introducción de alimentación complementaria a los bebés actualizada con los últimos estudios, como por ejemplo, la nueva recomendación de que el pescado se empiece a dar a los 6 meses, siempre que no haya antecedentes de alergias en la familia, lo que incluso evitaría futuras intolerancias.

También se hizo hincapié en que el orden de introducción de los alimentos no está estandarizado, dependiendo mucho de factores culturales.

**España, entre los primeros países con obesidad infantil.**

Pero lo que más me llamó la atención, por lo preocupante, fue que España se encuentra entre los primeros países con obesidad infantil, a pesar de que tenemos al alcance de la mano (y del bolsillo) los mejores alimentos que podríamos tener y de los que se compone la famosa dieta mediterránea. ¡Pero no los aprovechamos! ¿Cómo puede ser?

 ![image](/tumblr_files/tumblr_inline_pdpcltSSkV1qfhdaz_540.jpg)

Debemos tomar consciencia de ello en las familias porque en los colegios ya se empieza a trabajar en este aspecto, por ejemplo en el de mis hijos se ha establecido un día a la semana en el que el almuerzo de todos los niños debe ser fruta, y otros dos días deben llevar bocadillo de pan de barra, y así evitar tanta bollería industrial y galletas. Además, una vez al año los papás y mamás preparamos un “desayuno saludable” a base de pan tostado con aceite y frutas (los niños se ponen las botas).

Un grupo de madres y padres estuvimos analizando con Mª Dolores Iniesta el ABC de la nutrición infantil. Os voy a resumir los datos más interesantes que allí se pusieron sobre la mesa:

 ![image](/tumblr_files/tumblr_inline_pdpclvwHP51qfhdaz_540.jpg)

**La leche.**

Para Hero la leche materna es inimitable porque es cambiante en su composición y se adapta a las necesidades del niño con compuestos bioactivos que nada tiene que ver con la leche de fórmula. Recomiendan la lactancia materna exclusiva hasta los 6 meses como alimento idóneo para el lactante.

Que una marca de leche infantil como Hero arranque una charla de nutrición infantil de este modo te predispone a creerte todo lo demás porque en ningún momento insinuaron que su leche se podría parecer a la materna, o que podría haber casos en que fuera mejor… ni siquiera igual. Eso sí, allí están ellos por si la madre no puede o no quiere dar el pecho ofreciendo la mejor calidad. Me pareció muy correcto y me sorprendió gratamente.

Otro dato interesante es que cuando decidamos dar a los bebés leche de vaca es mejor hacerlo en su variedad de entera hasta que cumplan al menos los 3 años de edad, ya que el aporte de grasas saturadas que necesitan en esa etapa de su desarrollo en muy grande. Si queremos, a partir de los 3, les podemos darles semidesnatada si no son niños muy delgaditos, como es el caso de mi Princesita.

 ![image](/tumblr_files/tumblr_inline_pdpclwRHBM1qfhdaz_540.jpg)

**La introducción de los sólidos.**

A partir de los 6 meses ya se deben ir introduciendo otros alimentos a la dieta de los bebés. Se entiende por “sólidos” la alimentación complementaria a la leche independintemente de que esté tritutado o chafado.

No hay un orden establecido en la introducción de estos alimentos, sino que depende de factores culturales y costumbres de cada país o familia. En principio si no hay antecedentes de alergias o intorerancias, no habría problema en ir dándoles de todo, pero es aconsejable ir introduciendo los alimentos nuevos de uno en uno y dejando 2 o tres días entre uno y otro para observar posibles reacciones y poder identificar cuál es el que le ha sentado mal llegado el caso.

 ![image](/tumblr_files/tumblr_inline_pdpclwvRGX1qfhdaz_540.jpg)

Yo, con mi hijo practiqué el Baby lead-weaning pero a mi manera, en [este post](/post/68157636006/alimentacion-complementaria-sin-papillas-para) os lo explico, pero cada niño es un mundo y el sentido común y las circunstancias de cada familia son los factores que nos tienen que guiar.

Para niños que les cuesta comer recomendaron el libro del Dr. Estivill “A comer”, lo que ya sabéis que no me convence en absoluto al tratarse de un método conductista.

**El hierro**

En cuanto al hierro, el que mejor se absorbe es el de la carne roja, y nos dieron un consejo práctico, que yo he visto hacer a mi padre toda la vida, y es que cuando pongamos lentejas les echemos un chorro de limón, ya que el ácido ascórbico que éste contiene ayudará a asimilar mejor el hierro que contienen estas legumbres. Aunque este alimento, al contrario de lo que se cree, no contiene mucho hierro, sino que más bien es rico en fibra.

**La importancia de un buen desayuno.**

 ![image](/tumblr_files/tumblr_inline_pdpclx9Seo1qfhdaz_540.jpg)

El desayuno debería ser la comida más importante del día porque con ella arrancamos el motor para ponernos en marcha. Lo ideal es tomar fruta, un lácteo y cereales, tostadas o galletas.

Un consejo para meter la fruta en el desayuno: podemos levantarlos con un zumo de naranja o mandarina, y más tarde, cuando acudan  a la mesa a desayunar, que tomen la leche con las tostadas o galletas. Así será más fácil que lo tomen todo.

 ![image](/tumblr_files/tumblr_inline_pdpclxFv9i1qfhdaz_540.jpg)

La fruta

Ya sabéis que se recomiendo tomar 5 raciones de fruta o verdura al día. Es bueno aprovechar la estacionalidad de la fruta e ir cambiándola a lo largo del año, lo que les hará que prueben de todas las clases, y además en el momento en que están más ricas.

Otro truco para que coman fruta es usar la manzana como espesante en las comidas.

**El pescado**

 ![image](/tumblr_files/tumblr_inline_pdpclyJNOs1qfhdaz_540.jpg)

Para el pescado, si les cuesta, lo mejor es echarle un buen chorro de aceite de oliva para disimular el sabor, y de paso aportamos grasa de la buena.

No tenemos por qué consumirlo fresco, sino que congelado o en conserva también es una buena opción. Pero ojo con los pesacdos grandes, no conviene abusar, lo que no quiere decir que los eliminemos de la dieta, porque pueden contener acúmulo de metil mercurio. Especielmente los deben evitar las embarazadas y mujeres en edad fértil y los niños menores de 3 años.

 ![image](/tumblr_files/tumblr_inline_pdpclzDxVL1qfhdaz_540.jpg)

**¿La fecha de caducidad y de consumo preferente son lo mismo?**

No se debe consumir los productos después de la fecha de caducidad, porque a partir de ella aumenta el número de microorganismos perjudiciales para el cuerpo humano.

En cambio, la fecha de consumo preferente sí se puede sobrepasar un poco sin que nos afecte, aunque la textura, color y sabor del producto puede variar respecto del original.

**Congelación/descongelación de los alimentos en casa.**

Es recomendable congelarlos lo más frescos posibles (en cuanto los compremos), y descongelarlos o bien en el frigorífico, para lo que hay que ser previsor y sacarlos con dos días de antelación, o directamente en el microondas, en la opción de descongelado. No se recomienda hacerlo a temperatura ambiente porque se multiplican los microorganismos perjudiciales.

La conclusión que saco de esta interesante charla es que los padres y madres tenemos una gran responsabilidad con la alimentación de nuestros hijos, que comienza desde que se están gestando en el interior de la madre, y que no debemos descuidar en ningún momento de su desarrollo porque en cada etapa tienen unas necesidades distintas. Debemos tener entre nuestras prioridades ofrecerles una alimentación variada y equilbrada, y fomentar en ellos hábitos alimenticios saludables.

Y no tenemos excusa porque tenemos a la mano la mejor dieta, la dieta mediterránea.