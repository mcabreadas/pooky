---
date: '2017-05-15T13:17:06+02:00'
image: /tumblr_files/tumblr_inline_nhz60ofWFN1qfhdaz.jpg
layout: post
tags:
- derechos
title: Deducción por maternidad en la declaración de la renta 2016
tumblr_url: https://madrescabreadas.com/post/160691640534/deduccion-marternidad-renta
---

Es de justicia que el tener hijos ayude a que el IRPF sea menos gravoso para las familias, ya que a través de nuestros hijos aportamos un gran activo a la sociedad sobre todo si son como los míos, que no paran en todo el día.

 ![](/tumblr_files/tumblr_inline_nhz60ofWFN1qfhdaz.jpg)

Bromas aparte, te voy a dar las claves para que te beneficies de la deducción por maternidad si estás preparando tu declaración. Ya sabes que tienes la opción de solicitar el abono anticipado para percibirla mes a mes, o aplicarla en la declaración anual de IRPF.

## Tienes derecho

- si tienes hijos menores de 3 años (hasta 1.200 Euros por hijo)  
- y trabajas por cuenta propia o ajena  
- y estás dada de alta en la Seguridad Social o Mutualidad  
- y no has estado percibiendo los importes mensualmente  

## Cómo se calcula

Se calculará de forma proporcional al número de meses (100 euros cada mes por cada hijo) en que se cumplan de forma simultánea los requisitos para su aplicación.

## Excedencia, baja o desempleo?

Durante la **excedencia** pierdes el derecho a este beneficio, ya que, aunque sigas de alta en la Seguridad Social o Mutualidad, no estarás realizando una actividad por cuenta propia o ajena.

Si estás de **baja por enfermedad común**  podrás disfrutar de la deducción por maternidad y, en su caso, de su abono anticipado, siempre que se cumpla el resto de requisitos.

Si te quedas **en paro** pierdes este derecho, ya que te faltaría el requisito de estar realizando una actividad por cuenta propia o ajena.

Caso de que se dé alguna circunstancia que te haga perder este derecho, deberás comunicarlo a la Agencia Tributaria.

## ¿La prestación por maternidad tributa?

Ya os hable de la posible [exención de la prestación por maternidad](/2016/11/22/deduccion-irpf-maternidad.html) cuando estalló la noticia, pero sigue  siendo la pregunta del millón, por eso, antes de que me la hagáis, me adelanto y os digo que, a pesar de que [Tribunal Superior de Justicia de Madrid](http://www.poderjudicial.es/search/contenidos.action?action=contentpdf&databasematch=AN&reference=7795027&links=) entiende que las prestaciones por maternidad del INSS están exentas, la  [Dirección General de Tributos](http://petete.minhafp.gob.es/consultas/?num_consulta=V3404-13) avalada por el [TEAC](http://serviciostelematicos.minhap.gob.es/DYCteac/criterio.aspx?id=00/07334/2016/00/0/1&q=s%3d1%26rn%3d%26ra%3d%26fd%3d%26fh%3d%26u%3d%26n%3d%26p%3d%26c1%3d%26c2%3d%26c3%3d%26tc%3d1%26tr%3d%26tp%3d%26tf%3d%26c%3d0%26pg%3d) interpretan que no procede la exención.

Te preguntarás cómo puede ser eso; que unos digan una cosa y otros, la contraria, pero el caso es que Hacienda no está dispuesta a renunciar a su parte del pastel de la prestación por maternidad aunque madre no hay más que una, y si no la pagas, te pueden multar, incluso embargar.

Mi consejo de amiga es que no te la juegues mientras se aclaran, y pagues como una señora y, después, si te queda ansia en pleno puerperio, reclames la devolución.

Si quieres que explique en otro post cómo reclamarla, déjame un comentario aquí abajo.