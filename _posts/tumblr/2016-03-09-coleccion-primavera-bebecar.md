---
date: '2016-03-09T11:07:54+01:00'
image: /tumblr_files/tumblr_inline_pap7g9rvnI1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- bebes
title: Estrenamos la primavera con Bébécar
tumblr_url: https://madrescabreadas.com/post/140737115734/coleccion-primavera-bebecar
---

Hace poco nació Chiquita, mi primera sobrina (los demás son chicos) y me hace mucha ilusión presentárosla porque es un amor. 

 ![](/tumblr_files/tumblr_inline_pap7g9rvnI1qfhdaz_540.jpg)

Nos estamos convirtiendo en una gran familia, y mis hijos tienen la suerte de tener un montón de primos con los que jugar. Ellos son los mayores, y como tal, ejercen cuidándolos genial.

 ![](/tumblr_files/tumblr_inline_pap7gaXQQT1qfhdaz_540.png)
##   

## Carrito en primicia

Ya os conté que el [cochecito que elegimos](/2015/12/28/cochecito-bebe-bebecar.html) para Chiquita fue el [Ip Op de la nueva colección de primavera de Bébécar](/2016/02/15/montajecarritobebecar.html), que se ha presentado recientemente, y no sabéis la ilusión que me hace que mi sobrina haya estrenado en primicia este carrito tan novedoso, con tejidos totalmente diferentes y un color tan bonito.

 ![](/tumblr_files/tumblr_inline_pap7gaBC6Y1qfhdaz_540.jpg)
##   

## Materiales de primera  

Mi cuñada quería un cochecito que le sirviera para los futuros hermanitos también, por eso eligió [Bébécar](http://www.bebecar.com/bebecar/es/), porque son de fabricación europea, y con materiales de primera calidad y muy duraderos.

 ![](/tumblr_files/tumblr_inline_pap7gbi7vW1qfhdaz_540.jpg)

## El tejido

Tanto la eco piel como el acolchado son las principales novedades de la nueva colección de esta firma, que apuesta por la moda, la comodidad y la seguridad por encima de todo, como he tenido ocasión de comprobar en múltiples ocasiones, incluso en [visita a sus instalaciones de Toledo](/2015/01/04/premio-bebecar.html).

##   

## El color

Nos decantamos por una combinación de colores unisex que hacen un contraste muy bonito: eco piel de color topo, y blanco para el acolchado de la base y la visera de la capota y solapa de la cubierta,  que combinan genial con los complementos que le quieras poner.

Con chiquita estamos usando complementos en rosa (quién sabe si más adelante usaremos el azul…).

 ![](/tumblr_files/tumblr_inline_pap7gby69G1qfhdaz_540.jpg)

## El capazo

El capazo es el XL combinado con el chasis Ip Op, que le da una manejabilidad comodísima.

El interior es de algodón y es totalmente mullido, confortable y fácilmente desenfundable para lavar.

 ![](/tumblr_files/tumblr_inline_pap7gcadpm1qfhdaz_540.jpg)

## Tres en uno 

El carrito también tiene la opción de convertirse el **silla** para cuando chiquita tenga 6 meses, aunque está homologada desde los 0 meses.

 ![](/tumblr_files/tumblr_inline_pap7gcqxW41qfhdaz_540.jpg)

Además, lleva un **grupo 0** para el automóvil de lo más cómodo, que se puede anclar también en el chasis para paseos cortos ya que, como sabéis, la posición de la columna ideal para niños de 0 a 6 meses es horizontal. Como máximo se aconseja usar el grupo 0 durante 2 horas.

 ![](/tumblr_files/tumblr_inline_pap7gdRgFm1qfhdaz_540.jpg)

## Plegado de emergencia

Ya os conté la facilidad con la que se pliega el chasis Ip Op, pero quizá pensásteis que a la hora de la verdad, cuando estás mal aparcado, la niña llorando, los coches pitando, y con los nervios a flor de piel, sería diferente.

 ![](/tumblr_files/tumblr_inline_pap7gd4S1e1qfhdaz_540.jpg)

Pues, como podéis comprobar en este video, el plegado del Bébécar es a prueba de nervios.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Pi_e5rs89QY?rel=0" frameborder="0" allowfullscreen></iframe>

## Otros modelos de la nueva colección

Os advierto que si seguís leyendo os van a entrar ganas de tener otro bebé sólo para poder tener uno de estos cochecitos.

 ![](/tumblr_files/tumblr_inline_pap7genL9D1qfhdaz_540.png)

¡Quien avisa no es traidora!

Os dejo este aperitivo, de la presentación en primicia de la nueva colección primavera/verano a la que tuve la suerte de asistir en los desfiles de Pettit Walking Valencia y Madrid para que os hagáis una idea de las novedades.

 ![](/tumblr_files/tumblr_inline_pap7gegL191qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_pap7gfE2lo1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_pap7gfc4Ku1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_pap7ggNCJA1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_pap7ggXqHK1qfhdaz_540.png)