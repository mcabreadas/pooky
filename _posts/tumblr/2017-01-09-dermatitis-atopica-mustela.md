---
date: '2017-01-09T12:24:20+01:00'
image: /tumblr_files/tumblr_inline_oj990jicjm1qfhdaz_540.jpg
layout: post
tags:
- salud
- crianza
- bebe
title: Qué es la dermatitis atópica. Consejos de Mustela
tumblr_url: https://madrescabreadas.com/post/155621259359/dermatitis-atopica-mustela
---

![](/tumblr_files/tumblr_inline_oj990jicjm1qfhdaz_540.jpg)

La pile atópica es uno de los quebraderos de cabeza más frecuentes de las madres, y unos de los temas que más nos consultamos unas a otras en las redes, por eso no quise perderme el evento de presentación de la nueva gama de Mustela: [STELATOPIA](http://amzn.to/2izJiIU).

Al mismo acudió nuestra colaboradora Blanca Crespo, quien nos lo resume así:

¿Sabías que 1 de cada 5 niños tiene la piel atópica, o que la probabilidad de aparición de la dermatitis atópica aumenta en caso de antecedentes familiares? Así empezó el evento presentación de la nueva línea de Mustela para pieles atópicas.

Todo esto nos contó el Doctor Raúl de Lucas ( jefe de dermatología pediátrica del [Hospital La Paz](http://www.madrid.org/cs/Satellite?language=es&pagename=HospitalLaPaz/Page/HPAZ_home), de Madrid). 

## ¿Qué es la dermatitis atópica?

Es una disfunción de la barrera cutánea. Lo normal es que los queratinocitos formen la epidermis se llenen de queratina se unan y nos protejan de todos los factores externos. En el caso de a piel atópica las células están más separadas, dejando huecos que permiten que entren más factores a la vez que deja salir agua, lo que provoca sequedad y sensibilidad.

## ¿Por qué se produce?

La DA puede aparecer por los siguientes factores: genéticos, inmunológicos, medioambientales o por disfunciones de la barrera cutánea.

El factor genético es muy importante, ya que aunque en la actualidad no tengamos ningún síntoma de DA los brotes que hayamos podido tener durante nuestra infancia pueden influir en nuestros hijos en un 10%.

Infecciónes, bacterias y virus. 

Productos iirritantes, como por ejemplo jabones con PH inadecuado.

El estrés y la contaminacón son factores agravantes de la dermatitis atópica. 

## ¿Cuándo se produce?

El 90% en la infancia.

El 19% de o a 16 años.​

El 10% continúan teniendo DA después de los 18.

## ¿Cómo se diagnóstica?

Tiene que aparecer más de un brote para que sea DA.

## ¿Cómo abordamos la Dermatitis Atópica?

Debemos:

- Recuperar la piel en su integridad.  
- Controlar la inflamación.  
- Luchar contra las infecciones.  
- Mejorar la calidad de vida.  
- Que el tratamiento elegido por el médico tenga los menos efectos adversos posibles.  
- Adherencia al tratamiento.  

Si tomamos corticoides debemos tener en cuenta que no son productos hidratantes, por lo que habrá que combinarlos con éstos.

También debemos elegir productos de higiene que se adecúen a las caracterísicas de este tipo de piel, ya que un mal producto puede agravar o incluso provocar alteraciones en la piel.

## ¿Se puede curar?

 Esta enfermedad no tiene cura, pero se pueden hacer varias cosas para paliar los efectos.

Utilizar un buen emoliente.

Tratar la inflamación con los productos que aconseje el dermatólogo. \* Reparar la barrera cutánea.

Calmar el picor.

Evitar factores desencadenantes.

Intentar humedecer los ambientes.

Utilizar ropa de tejidos naturales evitando las lanas que producen más picor.

Evitar todos los productos que contengan alcohol.

## La nueva linea de Mustela para pieles atópicas

Mustela es una marca que lleva 60 años cuidando de nuestra de piel, pero hace 15 años comenzaron con un estudio llamado E.V.E.I.L.S ( Evaluation of the early infant life skin).

Cada piel es diferente por lo que esta firma ha decidido organizar sus productos por tipo de piel, para poder cubrir cada necesidad. Con la particularidad de que todas las gamas tienen un activo funcional común avalado por la profesinalidad de los laboratorios Expanscience.

La [GAMA STELATOPIA](http://amzn.to/2izJiIU)está especialmente indicada para niños de 0 a 2 años. Y se se caracteriza por ser muy agradable y sensorial.

Se compone de varios productos:

 ![](/tumblr_files/tumblr_inline_oj98quKSXv1qfhdaz_540.png)
- Emolientes: [bálsamo](http://amzn.to/2i9Tp3P) (nutrutivo) y [crema](https://www.amazon.es/gp/product/B00GSUDP6C/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00GSUDP6C&linkCode=as2&tag=madrescabread-21) (hidratante).  
- Productos de higiene: gel sin jabón y [aceite de ducha](https://www.amazon.es/gp/product/B01BM67VB6/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01BM67VB6&linkCode=as2&tag=madrescabread-21) (para aportar ese extra de hidratación en el baño).

Todos estos productos están pensados para utilizarlos diariamente y así poder luchar de una manera más efectiva contra la DA.

La composición de estos productos está delicadamente pensada para rehidratar la piel por eso sus componentes principales son:

- Aceite de aguacate (el aguacate es un hidratante natural muy conocido)  
- Oleodestilado de girasol a través de una destilación pura, que ayuda devolver líquidos a la piel y la calma.  

Los productos son seguros e inocuos por lo tanto en su composición no se han incluido ni parabenos ni perfumes, esta última cualidad es exclusiva de la gama Stelatopia.

Todos los porductos stelatopia son fabricados en Francia y han sido fabricados para tener el menos impacto medioambiental. Por eso:

- Contienen activos patentados de origen natural obtenidos a través de canales de aprovisionamiento responsables bajo el control de la UEBT( Unión para el biocomercio Ético).  
- Envases y los estuches son reciclables, fabricados con cartón procedente de bosques sostenibles e imprimidos con tintas a base de aceites de origen vegetal.  
- Fórmulas espumantes tienen aclarado biodegradable. ​  

Sin duda, lo mejor es ir probando rutinas de baño y cremas hasta dar con la más acertada para nuestro hijo, y la experiencia de otras madres nos puede dar ideas para aliviar los síntomas tan molestos de esta anomalía en la piel. Espero haberte aclarado algunas cosas y que te sirvan estos consejos.