---
date: '2017-01-24T12:06:50+01:00'
image: /tumblr_files/tumblr_inline_ok86vvrSut1qfhdaz_540.png
layout: post
tags:
- maternidad
- crianza
- mecabrea
title: Crianza vs. trabajo. Mi decisión
tumblr_url: https://madrescabreadas.com/post/156306056544/maternidad-crianza-conciliacion
---

Recuerdo perfectamente el momento en el que supe que nunca iba a poder ser como lo había planeado.

En mi cabeza siempre había estado claro cómo sería mi vida, y todo iba según lo previsto, y por su justo orden: terminar mis estudios, tener un buen trabajo, casarme, y ser madre lo antes posible, no sé muy bien si por razones biológica o porque siempre lo había deseado tanto que estaba impaciente.

 ![](/tumblr_files/tumblr_inline_ok86vvrSut1qfhdaz_540.png)

Yo siempre había imaginado mi vida con una gran familia esperándome al llegar a casa por la noche, satisfecha por haber tenido un día brillante en la oficina, perfectamente maquillada y trajeada. Sería una gran profesional, y tendría una familia perfecta y sonriente aguardándome tras mi jornada laboral.

Así de ingenuos eran mis pensamientos cuando era joven. Todo estaba perfectamente calculado y separado, sin ser consciente en absoluto de la verdadera realidad de una madre trabajadora.

Te preguntarás cuál fue ese momento en el que me di cuenta de que no iba a ser capaz de ejecutar mi plan vital como lo había diseñado.

No creas que fue un proceso de maduración, o algo que el tiempo fue poniendo en su sitio. No.

Fue un instante en el que me invadió tal sentimiento de plenitud y felicidad, que supe que no tendría agallas para soltar a mi bebé jamás de mis brazos.

 ![](/tumblr_files/tumblr_inline_ok86rzzEtA1qfhdaz_540.png)

Y no vayas a pensar que no intenté rebelarme contra este instinto primario al que empecé a temer y rechazar por partes iguales porque sabía que si me rendía a él, me arrebataría todo por lo que había trabajado tanto.

Me negaba a dejar que una “debilidad” me apartara del camino correcto del desarrollo y superación profesional, porque para eso había dedicado tanto tiempo y esfuerzo en mis estudios. Y ahora que ya estaba en un buen momento, y con toda una carrera por delante, no podía flaquear, y me decía a mí misma que no podía acostumbrarme a ese [vaivén de arrumacos, calorcito y olor a nubes](https://madrescabreadas.com/2013/05/20/las-horas-contigo-contigo-las-horas-se-llenan-de/) que se repetía constantemente desde que mi bebé nació porque era demasiado bonito para ser bueno.

Cuando mi primer bebé cumplió 8 meses dejé de trabajar desde casa (con ella en brazos) para incorporarme a la oficina con el mismo horario de siempre, o sea, sin horario, dejándola a cargo de mi madre y mi marido (mejor cuidada, imposible). Me hice violencia a mí misma, como todas las mujeres que se incorporan al mundo laboral tras su permiso de maternidad, y salí por la puerta de casa.

 ![](/tumblr_files/tumblr_inline_ok86sxfbqP1qfhdaz_540.png)

Pero la angustia del primer día no mermó el segundo, ni el tercero, ni meses después, e hizo que me odiara a mí misma por no ser lo suficientemente fuerte como para vencer esa “debilidad” primaria que me ataba de una forma química a mi cachorro.

Simplemente, no fui capaz.

Al principio pensaba que me acostumbraría, que era cuestión de echarle narices y continuar, pero el tiempo pasaba, y no me acostumbraba, no. Al contrario, me estaba sumiendo en una profunda tristeza, y todo empezó a dejar de tener sentido.

El día que mi hija llamó “mamá” a su abuela, y prefirió los brazos de su padre a los míos cuando tenía fiebre decidí que tenía que ser yo la que ocupara ese lugar en su vida. Quería asiento VIP para verla crecer, evolucionar, dar sus primeros pasos y traducir sus primeros balbuceos. Necesitaba estar presente, y así lo he hecho con cada uno de mis hijos.

Cambié mis ambiciosos objetivos laborales, y empecé a trabajar desde casa adaptando el tipo y el ritmo de mi trabajo a mi situación familiar.

Ahora, intento valorar con perspectiva si mi decisión fue la correcta cuando veo a otras madres con gran éxito profesional, y que saben perfectamente separar el ámbito familiar del laboral sin aparentemente tambalearse, y me pregunto porqué yo no fui capaz de hacerlo. Y las admiro, porque sé a lo que renunciaron, y el desgarro que debió suponer para ellas romper ese lazo natural instintivo.

Y llego a la conclusión de que tanto coraje demuestra una madre que renuncia a su carrera profesional por sus hijos, como la que renuncia a criarlos en primera persona por su carrera profesional.

Es más, creo que cada mujer tiene todo el derecho a elegir sin ser juzgada, porque bastante conflicto interno tiene ya, y tendrá el resto de su vida decida lo que decida, porque no se puede tener todo, y las madres no dejaremos nuca de cuestionarnos a nosotras mismas, auto justificarnos, culpabilizarnos… 

Porque sí, porque va en el lote, porque amamos de una forma irracional a nuestros hijos, pero también somos mujeres ambiciosas, y con metas e inquietudes, y esa lucha es la que nos asemeja a todas, más allá de la decisión que hayamos tomado sobre la crianza de nuestro hijos.

 ![](/tumblr_files/tumblr_inline_ok86uzhAM71qfhdaz_540.png)