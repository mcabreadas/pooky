---
date: '2018-10-07T18:19:29+02:00'
image: /tumblr_files/tumblr_inline_ocmo3jHlxw1qfhdaz_500.jpg
layout: post
tags:
- Maternidad
- derechos
title: La prestación por maternidad ya no tributa a Hacienda
tumblr_url: https://madrescabreadas.com/post/178823618614/exención-irpf-prestacion-maternidad
---

Por fin podemos afirmarlo con todas las del la ley, porque así lo ha dicho el [Tribunal Supremo en su Sentencia de 3 de octubre de 2018](https%3A%2F%2Fs03.s3c.es%2Fimag%2Fdoc%2F2018-10-05%2Fsentencia-prestacion--maternidad-exenta-IRPF.pdf&t=MjQ0Y2YwYzBlYjk1ZWFhNTZkYTVlMTdmMDU0M2IxY2VmMWM3ZGE5MyxjZDE2MDU5N2RkY2JmOWE2YWNhMzdlNGFjYTIxNTUxNWZhMzhlYmJl): La prestación por maternidad no tributa en el IRPF, o lo que es lo mismo, no tenemos que pagar impuestos por ella a Hacienda cuando hagamos la declaración de la renta a partir de ahora. 

Esta Sentencia, que confirma otra del TSJ de Madrid, y que [ya os anuncié en este blog](https%3A%2F%2Fmadrescabreadas.com%2Fpost%2F153507209859%2Fdeduccion-irpf-maternidad&t=Y2EyYjZiNWY0MGY3ZmEzMDlhZGI2MDZlNzYzNDBmOTczOTFhMzBlNSwwNWMzNmRkNTVkMzBlNzkxMjI2ZWRlNmQyNzc5Zjc2ZWRlZDI1NjBi), esperada como agua de mayo, y gratamente aceptada por casi todos aunque (ya he escuchado a algún funcionario del sector decir quedes un disparate), y sobre todo por nosotras las madres, viene a poner fin a una dura batalla iniciada por una mujer que pensó que había tributado por su baja maternal, y que no debería de haber sido así.

Voy a tratar explicarlo de un modo sencillo evitando dejarme llevar por tecnicismos jurídicos, que odiáis.

Que a partir de ahora no tendremos que declarar este ingreso y pagar por él está claro, pero qué pasa con lo que hemos pagado hasta ahora, nos lo van a devolver?, cómo , desde cuándo? Estas son algunas dudas que seguro que os surgirán, y que trataré de resolver.

## ¿Qué pasa con lo que hemos pagado a la Agencia Tributaria hasta ahora indebidamente? 

Sólo tendremos derecho a reclamar por las prestaciones que hayan tributado de 2014 a 2017, ambos inclusive. Es decir, las cantidades de la prestación incluidas en las declaraciones de la renta de 2014, 2015, 2016 y 2017.

O sea, que se beneficiarán también las madres de los bebés nacidos a partir del tercer trimestre de 2013, ya que parte de la baja la cobrarían en 2014, yes sólo esta parte la que podrán reclamar.

 ![mama con bebe](/tumblr_files/tumblr_inline_pap7g9rvnI1qfhdaz_500.jpg)

El plazo para solicitar la rectificación de la declaración termina 4 años después de la finalización del plazo para presentarla, por ejemplo, la de 2014 terminaría el 30 de junio de 2019.

Para lo tributado por las prestaciones percibidas con anterioridad a 2014 no habrá devolución porque existe un plazo legal de prescripción de 4 años en los que se tiene derecho a reclamar, y si no lo hemos hecho durante el mismo, hemos perdido el derecho a hacerlo para siempre, a no ser que ya hubiéramos iniciado una reclamación con anterioridad, en cuyo caso el plazo de prescripción ya quedó interrumpido.

## Quién puede reclamar

La Hacienda Pública tiene motivos para temblar porque se estima que alrededor de 1.000.000 de personas tendrán derecho a devolución. En concreto:

- -Las madres que hayan percibido una prestación por maternidad a cargo del Instituto Nacional de la Seguridad Social 
- -Que la hayan declarado en el año 2014 o posteriores hasta 2017, o las anteriores para las que hubieran iniciado una reclamación con anterioridad.
- -Que no hayan tenido una comprobación por parte de Hacienda que haya negado la rectificación de dichas declaraciones y que las haya declarado firmes .
- -Los padres que hayan disfrutado de la prestación por maternidad porque se les haya cedido en parte.

## Cómo reclamar

Hay que solicitar una rectificación de la auto liquidación y devolución de ingresos indebidos de la declaración de la renta en la que hayamos metido la prestación por maternidad. Caso de que las hayamos declarado en dos ejercicios diferentes por haber nacido el bebé en el último trimestre del año, habría que solicitar la rectificación de dos declaraciones.

Podemos pedir también la devolución de los intereses de demora generados.

Tendremos que hacerlo mediante papel para los ejercicios 2014 y 2015, y para 2016 y 2017 podremos hacerlo mediante la [web de la AEAT](https%3A%2F%2Fwww.agenciatributaria.es&t=MDdjZjJkM2JiZjNjOTE2NjhjNTk2MmYxYTkyNTFlMmM2ZjEyMjlmMSxhODQ4ZGYzOGY4ZTNjMDRhYzI1M2FkOWQ0Njk3NjJkNTU3YjkzMmJj).

## Cuánto me van a devolver

La cantidad depende del importe de la prestación y del tipo impositivo que se aplique a cada contribuyente, que depende de sus ingresos, pero puede oscilar entre los 1.000 y 2.600 Euros

## Los padres también pueden reclamar

Sí, siempre que hayan disfrutado de la prestación por maternidad porque se les haya cedido en parte.

 ![papa con carrito de bebe](/tumblr_files/tumblr_inline_ocmo3jHlxw1qfhdaz_500.jpg)

Espero haberos servido de ayuda, y que recuperéis pronto  lo que pagasteis de más .

Si os ha servido, compartid! Pensad que podría ayudar a otras muchas madres.