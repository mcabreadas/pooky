---
date: '2017-06-25T18:41:05+02:00'
image: /tumblr_files/tumblr_inline_os1mt5yBMs1qfhdaz_540.png
layout: post
tags:
- mecabrea
- crianza
- ad
- nutricion
- salud
title: ¿La obesidad infantil es genética?
tumblr_url: https://madrescabreadas.com/post/162242196699/prevenir-obesidad-infantil
---

Hace unos días tuve la suerte de asistir a una jornada sobre nutrición infantil de esas que me gustan a mí para mantenerme al día de las últimas novedades y poder compartirlas con vosotras porque cada día estoy más convencida de la importancia de una correcta alimentación de nuestros hijos como apuesta de futuro, y más después de todo lo que nos contaron en estas charlas organizadas por [Nutribén Innova](http://www.nutriben.es/productos/innova), e impartidas por la Dra. Aurora Molina Romero, pediatra de atención primaria, y el Dr. Jorge Martínez, del hospital Niño Jesús en el excepcional marco del Museo del Traje de Madrid.

## La obesidad infantil es un problema grave

Fijaos la importancia que tiene lo que damos de comer a nuestros hijos, que la obesidad infantil es uno de los problemas más graves de nuestros tiempos, llegando incluso a estar catalogada de pandemia.

 ![](/tumblr_files/tumblr_inline_os1mt5yBMs1qfhdaz_540.png)

Nosotros como padres y madres tenemos la responsabilidad de cambiar esta tendencia, porque según los últimos estudios, en 2025 tendremos en el mundo 70 millones de niños obesos entre 0 y 5 años.

## Una buena noticia: podemos cambiar esta tendencia  

Estos datos nos los brindó la Dra.Molina Romero, quien además nos contó cómo influyen los factores genéticos y ambienales, y en qué proporción para que un niño llegue a ser obeso.

Para mi sorpresa y la de la mayoría de los asistentes, los factores genéticos sólo influyen en un 20%, siendo el fenotipo, o factor ambiental, hábitos… el que en mayor medida determina el que una persona termine siendo obesa hasta en un 80%:

 ![](/tumblr_files/tumblr_inline_os1n01L50T1qfhdaz_540.png)

Esto, a pesar de todo, es una buena noticia porque, según mi forma de ver las cosas, los padres tenemos el poder de darle la vuelta a la tortilla y desde el momento de la gestación, empezar a alimentar correctamente al bebé a través de la madre gestante.

## 1000 primeros días para reprogramar el metabolismo del bebé

Una de las novedades en materia de nutrición que nos transmitieron en esta jornada, y que más me sorprendió fue el concepto de epigenética, o programación nutricional temprana del sistema metabólico del bebé a través de la alimentación, en la que los 1000 primeros días del bebé desde la concepción son cruciales, y que podrá cambiar la tendencia de éste a la obesidad y a padecer enfermedades metabólicas, si la tuviera, a partir de la tercera generación.

 ![](/tumblr_files/tumblr_inline_os1n5bSUTB1qfhdaz_540.png)

La epigenética es la ciencia que estudia las modificaciones que se producen en la expresióndel ADN sin modificar su secuencia. Con ella se demuestra cómo diferentes estímulos ambientales pueden producir modificaciones en el fenotipo independientemente de su genotipo.

Es decir, si una madre gestante se alimenta de forma correcta, y su bebé, cuando se convierta en madre gestante lo hace también, el nuevo bebé habrá sufrido cambios en su metabolismo que invertirán la tendencia negativa que pudiera tener. 

 ![](/tumblr_files/tumblr_inline_os1mvzd69P1qfhdaz_540.png)

De esta forma es cómo se puede atajar esta pandemia que nos llevará a tener cifras tan preocupantes en el futuro.

Fíjate, la alimentación de la madre gestante influye directamente en la cantidad y el tamaño de [adipocitos](https://es.wikipedia.org/wiki/Adipocito) del bebé, o células que constituyen el tejido graso y que almacenan grasa, que a su vez, sufren un aumento importante en número durante sus dos primeros años de vida.

## No hay excusa: alimentémoslos bien

Pero ya hemos dicho que el condicionante genético sólo influye un 20% en la posible obesidad de un niño. El otro 80% es cuestión de hábitos, así que no hay excusa.

 ![](/tumblr_files/tumblr_inline_os1n2nfS8z1qfhdaz_540.png)

Los últimos estudios sobre nutrición infantil apuntan a que consumimos demasiadas proteínas animales, sal, grasas saturadas y azúcares refinados, y deberíamos tomar más ácidos grasos de los que llamamos buenos, pasta, legumbres y vitamina D a través del sol.

**La importancia de la vitamina D**

Curiosamente, España es el cuarto país más concienciado con la fotoprotección solar, lo que hace que no absorbamos la suficiente cantidad de vitamina D.

El debate está servido, y así se lo hicimos saber a la ponente, porque realmente no sabíamos qué opinaría un dermatólogo de todo esto, ya que llevamos grabado a fuego, y yo la primera, que debemos usar cremas protectoras solares durante toda la exposición.

Quizá, si dejáramos de usarlas un ratito en momentos en los que el sol es menos intenso podríamos recargar los depósitos de vitamina D sin riesgo para la piel… pero me encantaría que un dermatólogo opinara al respecto.

**Lo mejor, la lactancia materna exclusiva hasta los 6 meses**

Otra sorpresa fue que resulta que los bebés suelen tomar demasiadas proteínas durante los dos primeros años, lo que les puede causar un efecto rebote que les lleve a tender a la obesidad en un futuro.

 ![](/tumblr_files/tumblr_inline_os1n9iytrV1qfhdaz_540.png)

La lactancia materna es la mejor manera de prevenir la obesidad infantil, ya que su composición es perfecta para el bebé. Ya sabéis que la recomendación de la OMS es que les demos lactancia materna exclusiva hasta los 6 meses, y combinándola con la alimentación complementaria hasta los dos años, o hasta que mamá y bebé lo decidan. Pero si se opta por una leche artificial, debemos comprobar que sea baja en proteínas.

**La alimentación complementaria**

Las distintas recomendaciones oficiales aconsejan no iniciarla antes de los 4 meses ni después de los 6, pero en cuanto a la forma de introducirla, texturas… la verdad es que los asistentes en lo único que estuvimos de acuerdo fue en que no había acuerdo, ya que cada pediatra o centro de Salud hace recomendaciones diferentes, lo que genera bastante ansiedad, sobre todo en las madres primerizas.

Yo sólo os puedo hablar de mi experiencia, y os recuerdo [este post sobre el Baby Lead Weaning](/2013/11/26/alimentaci%C3%B3n-complementaria-sin-papillas-para.html) todo terreno que practicamos con mi hijo pequeño.

A modo de conclusión os dejo este decálogo para prevenir la obesidad infantil:

 ![](/tumblr_files/tumblr_inline_os1n8konE11qfhdaz_540.png)
## Visita a la fábrica de Nutribén

Ya el hecho de que una marca de alimentación infantil nos abra las puertas de su fábrica y nos muestre el proceso desde que se recibe la materia prima hasta que sale el producto envasado y etiquetado es una garantía para mí de su compromiso con el consumidor.

No nos estuvo permitido tomar fotos o video del interior para proteger el secreto industrial, pero os puedo contar que los protocolos de higiene son extremos, y que está todo controlado al milímetro y mecanizado, y los controles de calidad a lo largo del proceso son casi obsesivos.

Un equipo de 30 personas se ocupan de que esta planta, en la que sólo se fabrican leches de fórmula para bebés de hasta 10 tipos, funcione correctamente.

Dos de ellos nos acompañaron durante la visita, y respondieron con rigor a todo lo que les preguntamos a pesar de que no éramos precisamente un grupo precisamente fácil. Sino unas madres exigentes e informadas, y además, blogueras que, obviamente habíamos ido allí a aclarar dudas y preguntar mucho para después compartirlo con nuestras lectoras y lectores.

 ![](/tumblr_files/tumblr_inline_os3fsvJ54P1qfhdaz_540.png)

**El aceite de palma**

La valentía que tuvo Nutribén al invitarnos es más que evidente, sobre todo si tenemos en cuenta que la visita ha tenido lugar en medio de la polémica del aceite de palma, tema que afrontaron con honestidad informándonos de que están investigando para tratar de sustituír este ingrediente de sus leches de fórmula por otro, pero sin ocultar la dificultad que esto les supone, puesto que su propósito es que su composición sea lo más parecida a la la leche materna, y ésta contiene [aceite beta palmítico](https://nutritionj.biomedcentral.com/articles/10.1186/s12937-016-0145-1).

En cuanto al impacto que causa al medio ambiente extraer el aceite de palma, aseguran que se surten de proveedores locales, y que son muy exigentes con los controles.

Sin duda, y a modo de sugerencia, usar sólo [aceites de palma con sello de sostenibilidad](http://www.lavanguardia.com/politica/20170404/421447613685/eurocamara-pide-sello-de-sostenibilidad-para-aceite-de-palma-que-entre-en-ue.html) ayudaría a paliar este problema, aunque dicho sello tampoco está carente de polémica por el momento.

Con esta visita a las instalaciones de Nutribén, a quien agradezco que contara conmigo para ese evento, terminó para mí esta jornada donde aprendí cosas, afiancé otras que ya sabía, aclaré otras que tenía dudosas, y ahora comparto con vosotras.

Fue un placer pasar este día con compañeras tan queridas y tan profesionales con las que siempre es un placer trabajar. 

 ![](/tumblr_files/tumblr_inline_os3geckTBU1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_os3gf9RBnD1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_os3ghj2mma1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_os3gv6gLPO1qfhdaz_540.png)