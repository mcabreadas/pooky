---
date: '2014-07-11T10:00:00+02:00'
image: /tumblr_files/tumblr_inline_n6399zSMjT1qfhdaz.jpg
layout: post
tags:
- mecabrea
- crianza
- trucos
- vacaciones
- planninos
- recomendaciones
- familia
- salud
- bebes
title: Protección solar para la piel de niños y bebés en verano.
tumblr_url: https://madrescabreadas.com/post/91437628993/crema-solar-bebes-madrescabreadas
---

Hemos empezado la temporada de vacaciones escolares, playa y piscina con muchas ganas de sol y agua, pero desde pequeños debemos enseñar a los niños que la crema solar no es opcional, sobre todo cuando tienen la piel tan blanquita como mis fieras, y cuando son bebés de 0 a dos años, como Leopardito, ya que la barrera cutánea de protección está sin formar del todo todavía y son más vulnerables.

**Precauciones bajo el sol**

Además de embadurnarlos de crema con un factor solar adecuado para bebés, hemos de tomar otras precauciones porque esto solo no es suficiente:

-No exponerlos directamente al sol entre las 11:00 hasta las 16:00 h.

-Buscar sombras en caso de sol intenso

-Vestir a tu bebé adecuadamente: ropa holgada de algodón de trama estrecha que filtre bien la radiación UV, sobreo o gorra de ala ancha, y gafas de sol homologadas.

-Hidrátalo con agua o zumos, aunque no pida de beber. A mí me gusta llevarme fruta a la playa porque les quita el gusanillo sin dejarlos sin hambre para la comida y les hidrata un montón. La sandía es su favorita.

 ![image](/tumblr_files/tumblr_inline_n6399zSMjT1qfhdaz.jpg)

¿Os digo un truqui para mamás y papás? Probad a tomar pastillas de [betacaroteno](http://es.wikipedia.org/wiki/Caroteno) tres meses antes de iros de vacaciones para que la piel se prepare para recibir el sol y ya esté protegida cuando llegue el momento de las primeras exposiciones. Esto no sustituye a la crema solar, pero estaréis más protegidos y obtendréis el tono bronceado más rápido y estaréis guapísimos.

Nosotros o hemos probado este año y yo, que soy blanquita, he perdido el color folio enseguida. (También podéis comeros 3 zanahorias al día durante tres meses para obtener el mismo resultado. Es más barato y más saludable).

 ![image](/tumblr_files/tumblr_inline_n8jhumBqtT1qfhdaz.jpg)

**Cómo aplicar correctamente la crema solar**

Para que les proteja adecuadamente tenemos que seguir unas sencillas pautas,pero necesarias para que la crema haga su función y evitar quemaduras:

-Aplicar 20 minutos antes de la exposición al sol. Para ser sinceros, mis hijos no esperan tanto, pero lo que tienen claro es que no se pueden meter al agua recién echada la crema.

-Repetir la aplicación después de cada baño, tras secar con una toalla, o cada dos horas incidiendo especialmente en zonas sensibles como nariz, frente, pómulos y orejas, sobre todo en los chicos, que no tienen pelo largo.

-Usar para los labios un stick especial para esta zona.

**Los peligros del sol**

En esta infografía de Mustela se explica claramente los peligros que correríamos de no tomar estas precauciones.

[![image](/tumblr_files/tumblr_inline_n8abcq4c981qfhdaz.jpg)](http://www.mustela.es/)

**Mi experiencia con la gama solar de [Mustela](http://www.mustela.es/)**

En general fue positiva, usamos la crema solar para el cuerpo factor 50 y la específica para la cara toda la familia y ninguno nos quemamos. Las aplicaciones aguantaron bien el agua, y no dejaron la piel blanquecina, además de no tener demasiado perfume, lo que las hace agradables. Lo que vi un poco difícil es aplicar la crema corporal con el pulverizador, ya que no es muy cómodo de usar, sobre todo cuando ya tienes crema en las manos y se resbala al pulsar. Por lo demás, son fáciles de extender, y lo que más me gustó fue que la crema para la cara, cuando se moja no escuece en los ojos. Yo los tengo muy sensibles y me escuecen con todas las cremas que he probado cuando me capuzo, pero esta vez no me ocurrió. Me encanta que Mustela haya sacado esta línea de productos solares, y animo a la marca a que siga trabajando en ese sentido, ya que me merece mucha confianza porque la he usado para la higiene y escoceduras del pañal con todos mis hijos y nos encanta. Y a ti, qué crema solar te funciona?