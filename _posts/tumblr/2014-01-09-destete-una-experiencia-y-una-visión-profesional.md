---
layout: post
title: "Destete: Una experiencia y una visión profesional."
date: 2014-01-09T07:09:00+01:00
image: /tumblr_files/tumblr_inline_mz3qvkcgzu1qfhdaz.jpg
author: maria
tags:
  - crianza
  - lactancia
  - bebes
tumblr_url: https://madrescabreadas.com/post/72744260791/destete-una-experiencia-y-una-visión-profesional
---
Esta entrada es muy especial para mí porque la comparto con alguien a quien seguramente conozcáis, y porque os abro mi corazón y mi casa compartiendo mi experiencia y la de mi familia con el destete de Leopardito. Algo que yo soñaba con que esta vez ocurriría más tarde, pero que  la realidad y nuestras propias circunstancias impidieron.\
Ya sabéis que para mí fue muy difícil tomar la decisión. Y aún [lo echo de menos.](/2013/11/07/no-hace-tanto.html)\
Tengo el privilegio de ofreceros (en cursiva), alternada con mi experiencia personal, una opinión cualificada por tratarse de una pediatra que, además, es mamá. Y así se hace llamar. Se trata de [@lamamapediatra](https://twitter.com/lamamapediatra).\
La doctora Amalia Arce escribe un blog súper imprescindible para toda madre y padre, ya que cuenta de forma clara todo lo que nos puede interesar sobre la salud de nuestros niños con un sentido común y sencillez fuera de lo normal en su profesión. Podéis leerla en [Diario de una mamá pediatra](https://www.dra-amalia-arce.com/blog/).  

Gracias por todo, Amalia.

[![image](/tumblr_files/tumblr_inline_mz3qvkcgzu1qfhdaz.jpg)](http://www.dra-amalia-arce.com/)

No ha sido fácil, no señor. [Una lactancia complicada](https://madrescabreadas.com/2013/05/11/lactancia-con-familia-numerosa-es-posible-sin/), al menos para mí. Más que por las dificultades de la propia lactancia, que también las ha habido (frenillo, reflujo, rechazo en determinadas posturas), por las de mis limitaciones personales (falta de fortaleza para aguantar noches sin dormir, a pesar del colecho, el agarre continuo y todos los consejos de asesoras de lactancia), circunstancias familiares (tres hijos y un marido), y laborales.

> *“La lactancia materna no siempre es fácil. Me atrevería a decir que la excepción es que sea sencillo. La información previa al momento de lactar y la experiencia previa con otros hijos son importantes, pero a veces no son suficientes. Cada bebé es diferente y cada momento de la vida de la mujer es incomparable: las circunstancias que acompañan a cada crianza son únicas.”*
>
> Este cúmulo de dificultades hizo que poco a poco me fuera [desgastando física y emocionalmente](https://madrescabreadas.com/2013/04/27/las-sombras-de-la-lactancia-los-niños-se-encelan/), hasta el punto de ver peligrar seriamente mi equilibrio mental y, lo más importante, la estabilidad de mi matrimonio y de mi familia. 

Cuando me di cuenta de que no era justo para mi marido y mis otros dos hijos dejarlos en un segundo plano empecé a mentalizarme de que lo más sensato era el destete.

> *“La mejor alimentación para un bebé los primeros meses de vida es la lactancia materna. La teoría nos la sabemos bien. Una de las principales maravillas de la lactancia es el binomio indisoluble entre madre e hijo. Pero, ¿puede ser esto también un problema? Pues depende. Lo que sí es cierto es que el binomio indisoluble suele convivir con otras personas, muchas veces también dependientes de la misma madre. En cuanto a los hombres –y ahora generalizaré- , suelen ser mucho más pragmáticos que nosotras las mujeres, y quizá equilibran las fuerzas emocionales de otra manera (haced vuestra propia lectura….)”*

Además de que la ingesta de líquidos por la noche agravaba considerablemente los síntomas de reflujo y el sufrimiento de mi bebé, que se despertaba continuamente llorando, se consolaba con el pecho, volvía a empeorar, y así sucesivamente en un círculo vicioso que no éramos capaces de parar.

> *“Algunos bebés tienen un reflujo gastroesofágico durante los primeros meses de vida. Está producido por cierta inmadurez en el esfínter que une el esófago con el estómago, de forma que el contenido del estómago vuelve hacia la boca, produciéndose las regurgitaciones. En mayor o menor medida casi todos los niños regurgitan ocasionalmente, pero en algunos el problema es de mayores dimensiones originando dificultades para ganar peso y cierta irritabilidad. Favorecen el reflujo la alimentación líquida –por eso suelen mejorar al introducir la alimentación complementaria- y la posición en decúbito (estirado). Así los bebés con reflujo importante que maman en posición horizontal suelen empeorar sus síntomas.”* 

Digo comencé a mentalizarme porque mis entrañas se negaban en redondo. Tuve que luchar contra ellas varios meses, hasta que por fin logré ser fuerte para tomar la decisión.

Siempre tuve claro que no iba a hacerlo de forma radical. Así que, consulté [asesoras de lactancia](http://www.maternidadcontinuum.com/), me informé leyendo experiencias de otras mamás, que es lo que más me ayudó, y me hice de dos compinches para que me acompañaran en el camino: mi marido y mi madre. 

> *“El instinto es fuerte. Pero también las convicciones, las expectativas, los deseos. La dualidad interior que muchas madres sienten cuando se plantean el destete, es tremenda. Especialmente cuando la lactancia ha sido prolongada y no se ha visto interrumpida por ejemplo, con la incorporación de la mamá al trabajo tras el permiso maternal. Al igual que ocurre con la instauración de la lactancia, muchas madres necesitarán acompañamiento en el destete”.*

La teoría estaba clarísima, lo íbamos a hacer progresivamente. Tras varios intentos fallidos de destete nocturno con el “Plan Padre” (me empeñaba en agarrarme a la esperanza de poder seguir amamantándolo, aunque fuera un poquito durante el día), y viendo que lo que hacía era provocar al bebé más ansia de mamar, y que empeoraba sustancialmente la relación con mi esposo y mi paciencia para cuidar de mis hijos al día siguiente, por fin lo tuve claro.

> *“El bucle. A veces en la consulta llegan madres en esta situación: metidas en un bucle, del que es difícil salir. El acompañamiento y la comprensión del entorno son fundamentales. Las madres con buenas relaciones afectivas y familiares son más capaces de sobrellevar los momentos más tensos. El entorno puede facilitar tanto la toma de decisiones y hacer de colchón emocional, como por otro lado poner más palos en las ruedas. Cada familia es un mundo.”*

Hablé con una amiga pediatra, que más bien me hizo de psicóloga, y me ayudó a ver las cosas desde fuera, de un modo objetivo. Creo que fue en ese momento cuando realmente empecé a convencerme de que lo mejor para mi familia, mi matrimonio y mi salud era el destete total, pero progresivo.

> *“Jejeje, ésa soy yo. Pero es cierto: le hablé más como una amiga que como pediatra. Aunque eso es lo que también intento hacer en la consulta: sin estigmatizar, sin juzgar, con flexibilidad. Para utilizar la palabra de moda: la gracia de la cuestión es “empoderar” a las madres. Primero desterrando la culpa, que es tan aniquiladora. Segundo, intentando representar el panorama de la forma más objetiva. Algo no siempre fácil cuando se está cansado y las emociones están a flor de piel.”*

Empecé con la consigna “no ofrecer, no negar” durante el día, lo cual me fue dando resultado muy poco a poco. Era fácil porque con un añito, aprendiendo a caminar, experimentando nuevos sabores y texturas con los alimentos lo iba entreteniendo y lograba evitar muchas de las situaciones en las que, normalmente, me hubiera pedido pecho (que eran muchas, ya que tomaba a demanda). Así que evitaba tumbarme con él, tomarlo en brazos mirando hacia mí, dormirlo yo en sus siestas diarias… 

> *En los temas relacionados con la alimentación y los hábitos, hay niños que aceptan los cambios de forma asombrosamente rápida. Otros necesitarán un tiempo mucho más dilatado, dando la sensación de que no se avanza nada.*
>
> *“La clave es tener el objetivo claro de a dónde queremos llegar: sea destetando, sea dando trocitos, sea dando alimentos que son en principio rechazados,…. Siendo perseverantes, unidos padre y madre en el objetivo y forma de hacer, mostrando seguridad (adulta) y con pocos dramas ante los “fracasos”, con frecuencia y con más o menos días, acabamos llegando a cumplir el objetivo planteado.”*
>
> La verdad, he de decir, que el llevarlo un par de horas por la mañana a la guardería, el entretenimiento en casa con sus dos hermanos mayores, Pocoyó, y las largas tardes de paseo han sido mis grandes aliados, ya que me di cuenta de que muchas de las veces quería mamar porque se aburría.

Cuando ya casi estaba logrado el destete diurno empezamos con el nocturno, y ahí entró en juego la abuela, con su sabiduría y experiencia en el cuidado de bebés. Tres noches el bebé con ella en su casa era el plan. He de confesar, que la primera vez que lo llevé a dormir con mi madre tenía una angustia que no me dejaba respirar, las lágrimas me empañaban los ojos cada dos por tres, y me sentí como si un cordón que nos unía se rompiera.

> *“¿Qué haríamos las madres recientes sin nuestras propias madres? La experiencia, el cuidado exquisito de nuestros hijos –sus nietos- y la mano a la que agarrarse cuando el cansancio o la desesperación nos invaden….”*

A la mañana siguiente, cuando fui a buscarlo bien temprano, lo encontré dormidito y me senté a su lado a esperar a que despertara. Cuando abrió los ojitos y se sentó de un brinco sonrió al verme y lo abracé eternamente cantándole “campeón, campeón, ohé, ohé, ohé…!”, lo vestí, le di el desayuno, y lo llevé a la guardería un ratito.

_Foto de amosermama.com.ar

A partir de ese momento ya no le di más pecho aunque pidiera durante el día. Grabé a fuego en mi memoria la última vez que lo amamanté sentada en mi cama con sus ojos de almendra clavados en los míos y su manita acariciándome la cara. Al terminar jugueteó con el pecho, le hizo unas pedorretas de las suyas y le dio un suave mordisco. Ahí terminó nuestra lactancia de 14 meses.

> *“La imagen que muchas mujeres tenemos de nuestra propia lactancia está grabada a conciencia en nuestra memoria. Muchas mujeres despiertan su instinto maternal precisamente viendo como otra mujer lacta a su bebé. Y otras muchas a veces desearían volver a ser madres por unos momentos, aunque solo fuera por volver a sentir esa intimidad tan especial. El ciclo de la vida.”*

La segunda noche de destete fue más complicada, ya que Leopardito se empezó a hacer a la idea de que la cosa iba en serio. La paciencia y saber hacer de mi madre, con la ayuda de mi padre, fueron la clave. Entre susurros, arrumacos, caricias y palotes en el culete transcurrió sin rabietas. Un bibe de leche consiguió la abuela que se tomara metiéndole el dedo en la boca al mismo tiempo que la tetina para que notara piel  y no le resultara tan fría la soledad de la silicona. Tras saciarse se durmió mi Leopardito y, entre sueños, cuenta mi madre que le tocaba sus pechos y decía muy bajito “tata no, tata no”. Mi angelico lo estaba asumiendo, se estaba resignando.

La tercera noche fuera de casa fue mejor, y la vivimos su padre, sus hermanos y yo con ilusión porque era la última que estaríamos sin él. Pero el plan destete nocturno seguía, por supuesto, aún no estaba conseguido.

El siguiente paso consistía en que durmiera en su entorno habitual, pero sin mamá en casa (para no flaquear). Aprovechando el fin de semana que papá no trabajaba, se hizo cargo dos noches, y yo me exilié a casa de mis padres. La primera noche estuvo más inquieto y durmió regular, pero la segunda sólo tuvo un despertar. Eso sí, a las 6:30 a.m. estaba tomándose un super desayuno de campeones.

Pero el gran reto llegó cuando salí del exilio y pase mi primera noche en casa con él. Me costó atreverme, quería huir porque no sabía si iba a ser fuerte para no enganchármelo como siempre. Pero ese día lo dormí por primera vez acurrucándolo y meciéndolo sin pecho, y me dio la seguridad que me faltaba para afrontar la noche. La cual transcurrió sin problemas, con un solo despertar que solventó su papá, y un madrugón para tomar un súper desayuno que le dio su mamá tras comérselo a besos.

Creo que nadie ha resultado herido en este trance. Leopardito no ha llorado casi, ni se ha enrabietado, lo ha ido entendiendo poco a poco y ha notado el amor de toda la familia, abuelos  incluidos.

Es cierto que a mí se me ha quedado un [vacío](/https://madrescabreadas.com/2013/09/29/nuevos-caminos-hoy-me-siento-triste-me-has/) que tendré que sobrellevar poco a poco, pero también un lazo inquebrantable con mi hijo, un recuerdo precioso y una gran ilusión con esta nueva etapa en mi relación con mi él, con mi marido y con mis otros dos hijos. Por no hablar de mi trabajo, que ahora no será una carga insufrible, sino algo con lo que solía disfrutar e ilusionarme.“

> *"La lactancia prolongada suele ser una experiencia muy reconfortante tanto para las madres como para los bebés. De hecho la OMS recomienda lactancia materna exclusiva hasta los 6 meses, y continuar con ella junto con otros alimentos hasta los 2 años (o más si madre y niño así lo quieren).*
>
> *Aunque esto que comentaré, sé que fácilmente puede herir susceptibilidades, en mi experiencia he visto como el hecho de mantener la lactancia en niños más mayores, en algunas ocasiones –y esto no es generalizable- origina vínculos que pueden ser un poco asfixiantes entre madres e hijos. Y me explico. Por un lado, a veces la relación se vuelve muy dependiente (y no solo por parte del niño…), pudiendo excluir a otros miembros de la familia y originando situaciones que pueden parecer surrealistas. El apego seguro en realidad no es eso….. En algunas circunstancias parece que el pecho es a la vez la solución y el origen de los conflictos. El huevo y la gallina.*
>
> *Por otra parte, a veces se malentiende la introducción de la alimentación complementaria. A partir de los 6 meses, la lactancia materna es insuficiente desde el punto de vista nutricional, así que hay que enfrentarse a ofrecer otros alimentos aunque algunos niños a priori los rechacen, por mucho que la alternativa sea más fácil, cómoda y conocida.*
>
> *En cuanto a la historia de Madres Cabreadas, obviamente criar a un bebé es muy importante, quizá la cosa más importante que hagas en tu vida. Pero me parece indispensable también cuidar otras facetas de nuestra vida y a otras personas que también caminan a nuestro lado. Las decisiones en este sentido, nunca serán fáciles.”* 

NOTA: No quería dejar de compartir con vosotros alguna anécdota “post destete” muy curiosa. Al menos para mí lo es por no haber comprobado nunca hasta dónde llega la memoria de los bebés. Me he dado cuenta de que, aunque sabemos que cuando son mayores no se acuerdan de cosas que les pasaron con anterioridad a los 3 años, siendo bebés sí tienen memoria a “largo plazo”.\
Me explico:\
Pasados 3 meses del destete Leopardito me vio desnuda y mirando fijamente mis pechos sonrió con cara de reconocerlos como suyos. Levantó los brazos para que lo tomara, y dijo “agua, agua” (luego, sabía que él de ahí había saciado su sed muchas veces). Acercó su boquita y me volvió a hacer una pedorreta de las que me solía hacer cuando terminaba de mamar.\
Imaginaos mi emoción y mi sorpresa!.

Has pasado por algo parecido?