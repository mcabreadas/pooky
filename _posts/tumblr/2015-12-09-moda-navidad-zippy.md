---
date: '2015-12-09T20:42:45+01:00'
image: /tumblr_files/tumblr_inline_o39yxbvQ6Z1qfhdaz_540.jpg
layout: post
tags:
- navidad
- trucos
- participoen
- colaboraciones
- ad
- recomendaciones
- moda
title: '#Loveinabox: Navidades solidarias y a la moda con Zippy'
tumblr_url: https://madrescabreadas.com/post/134870812264/moda-navidad-zippy
---

Los niños nos hacen ver la Navidad de una forma diferente, pero no sólo eso, sino que el tiempo de espera se llena también de magia con ellos.

Nosotros lo estamos viviendo este año de una forma muy especial porque el pequeño está tan impaciente esperando la Nochebuena, que cada día pregunta varias veces si ya ha llegado.

Cada noche se acerca a la ventana del salón y espera a que se enciendan las luces de Navidad de nuestra calle, y salta de alegría cuando por fin lo hacen como si fuera la primera vez que las ve.

Por eso hemos disfrutado tanto este año adornando el árbol.

 ![](/tumblr_files/tumblr_inline_o39yxbvQ6Z1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yxb9zcV1qfhdaz_540.jpg)
##   

 ![](/tumblr_files/tumblr_inline_o39yxbGe9P1qfhdaz_540.jpg)
##   

## Iniciativa solidaria #Loveinabox de Zippy  

Esta época de espera es especial, por eso cada año surgen iniciativas solidarias muy bonitas. Me gustan, sobre todo aquellas en las que los niños pueden participar.

 ![](/tumblr_files/tumblr_inline_o39yxcmlKb1qfhdaz_540.jpg)

Este año hemos colaborado en la [campaña #loveinabox de Zippy y la ONG Save the Children.](https://www.savethechildren.es/actualidad/zippy-impulsa-los-programas-de-save-children-gracias-la-iniciativa-loveinabox)

Consiste en comprar una caja como ésta, y llenarla de material escolar o cuentos para ayudar a niños desfavorecidos.

 ![](/tumblr_files/tumblr_inline_o39yxcnvk31qfhdaz_540.jpg)

Por cada caja que compremos, Zippy donará 0,50 € a Save The Children.

Se pueden adquirir en cualquier tienda Zippy, o en la [tienda on-line Zippy.es](https://es.zippykidstore.com/)

Las cajas, una vez llenas, se pueden depositar en cualquier tienda Zippy hasta el 10 de enero.

También puedes usarlas como envoltorio de tus regalos de Navidad porque son muy chulas, e invitar a quien la reciba a ser solidario y colaborar.

 ![](/tumblr_files/tumblr_inline_o39yxd3FTA1qfhdaz_540.jpg)

Todo el material recaudado a través de esta acción se destinará a los programas de educación infantil que Save the Children coordina en España.

Las cajas están disponibles en dos tamaños (de 35x25x9 cm y de 25x12x8,5 cm), tienen un coste de 1,5 € y 1 €, respectivamente.   
  
Las pasadas Navidades, #loveinabox permitió donar 13.000 € a Save the Children, destinados a apoyar la labor de su centro en Illescas (Toledo). Se consiguió además recaudar 7.000 kilos de material escolar que se distribuyó entre los más de 5.000 niños a los que la organización atiende en España. 

## Colección Zippy especial Navidad

Zippy adora la Navidad, por eso también  ha lanzado una colección de ropa especial para estas fechas a un precio genial. 

Se trata de prendas súper ponibles y cómodas que permiten a los niños moverse y jugar con total libertad, además de tener un punto navideño muy chulo.

Fíjate que mis fieras la estrenaron en el parque y se subieron a todo lo que se podían subir.

 ![](/tumblr_files/tumblr_inline_o39yxdxBwd1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yxdhF7J1qfhdaz_540.jpg)

Es una línea donde predomina el azul y el rojo para las niñas y los colores más oscuros para los niños.

 ![](/tumblr_files/tumblr_inline_o39yxenOi01qfhdaz_540.jpg)
## Línea Zippy Brothers & Sisters

Pero lo que es mi debilidad (y creo que la de la mayoría de madres de familia numerosa) es vestir a todas mis fieras coordinadas. Y es que verlos a todos conjuntados me da una ternura…

Para eso te recomiendo la línea especial Brothers & Sisters de esta colección de Navidad, donde tienes arreglo tanto para los recién nacidos, como para los bebés grandes, y para niñas y los niños, y te permite jugar con los rojos y azules para que vayan todos para comérselos. Y a muy buen precio.

 ![](/tumblr_files/tumblr_inline_o39yxecfjZ1qfhdaz_540.jpg)

Me ha encantado el jersey chaleco trenzado azul marino que, combinado con la camisa de cuadros queda genial, la chaqueta de lana de un navideño color granate, y el vestido que eligió mi Princesita azul turquesa oscuro, con un volante abajo y mangas de tul, que no quiere quitarse ni para lavarlo.

 ![](/tumblr_files/tumblr_inline_o39yxfGRpC1qfhdaz_540.jpg)

Ya huele a Navidad, ¿no lo notas?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.