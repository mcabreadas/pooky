---
date: '2016-02-18T13:26:46+01:00'
image: /tumblr_files/tumblr_inline_o3j5akbTNg1qfhdaz_540.png
layout: post
tags:
- crianza
- hijos
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- seguridad
- bebes
title: 'Sorteo “Por qué viajar #acontramarcha”'
tumblr_url: https://madrescabreadas.com/post/139538924454/espejo-protector-contramarcha
---

## Resultado del sorteo

Muchas gracias a todos por participar en este sorteo tan especial donde cada uno habéis dado vuestro proncipal razón para que vuestros hijos viajes a contramarcha, o por la que os gustaría que lo hicieran. Es un placer haber colaborado una vez más con Axkid en la concienciación de las familias del uso de la contramarcha como sistema de retención infantil idóneo para nuestros hijos. Y sin más dilación, el ganador es:

![](/tumblr_files/tumblr_inline_o3j5akbTNg1qfhdaz_540.png)

¡Enhorabuena, Manuel Rodrigo! Por favor ponte en contacto conmigo y dame tus datos para el envío.

Post originario:

Cada vez somos más las familias que, preocupadas por la seguridad infantil, hemos optado por los [sistemas de retención infantil para automóvil a contra marcha por las razones que ya os expliqué en este blog](/2014/11/12/sillas-automovil-seguridad-ninos.html).

Hace unos meses que estamos [testeando la silla Minikid de Axkid con un resultado favorable, tal y como os conté aquí](/2015/11/29/acontramarcha-minikid.html).

## Mis razones

Desde entonces me he convertido en una especie de consejera sobre seguridad infantil en mi entorno más cercano porque a la gente le choca mucho ver que mi hijo viaja de manera diferente que el resto de niños, y quieren saber por qué. Es entonces cuando les explico mis razones, y explico argumentos como éstos:

- La cabeza de un bebé es proporcionalmente muy grande y pesada respecto al resto de su cuerpo. Hasta el 25% de su peso.  
- En un accidente en un asiento en el sentido de la marcha se producen tremendas fuerzas sobre el cuello, hombros y cabeza del pequeño.  
- El cuello es sometido a una fuerza equivalente a 300 - 320 kg. Si la médula espinal se estira tan solo algo más de 6 mm puede ocasionar lesiones muy graves.

- En un asiento contrario a la marcha la fuerza es distribuida sobre más zonas del cuerpo. La fuerza sobre el cuello es equivalente a 50 kg, que es igual a 6 veces menos.
 ![](/tumblr_files/tumblr_inline_o2qsheOMo71qfhdaz_540.png)
##   

## Los reparos de las familias

No obstante, la mayoría de los padres tienen los mismos reparos a la hora de optar por la contramarcha. Reparos que en realidad son falsos mitos como que los niños van incómodos con las piernas encogidas, que si se marean, que los niños no tienen visibilidad, que les agobia no poder tenerlos a la vista, que si la tapicería del coche se les va a estropear por pisadas o arañazos, que si se marean…

En realidad si lo piensas, los niños normalmente cuando se sientan en una silla las piernas se le quedan estiradas porque no le llegan al suelo, pero a ellos les encanta sentarse de cuclillas o a lo indio. Observa a tus hijos y ya verás. Te puedo decir que a mi hijo le resulta comodísimo.

 ![](/tumblr_files/tumblr_inline_o2qs0kUAk11qfhdaz_540.jpg)

En cuanto a la visibilidad, la verdad es que pasan de ver la nuca de sus padres a tener todo un mundo por descubrir tras el cristal trasero del coche. y si no, mirad qué bien se lo pasa mi peque contando coches en este video que ya os enseñé:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/FbKp9WY_jjU?rel=0" frameborder="0" allowfullscreen></iframe>

Os puedo decir que, padres que se informan de la seguridad que aportan estas sillas de auto a sus hijos, padres que hacen lo imposible por cambiarlos a una a contra marcha, porque los datos hablan por sí solos.

Y cada vez más, los [falsos mitos de la contramarcha que os contaba en este otro post](http://Desmontando%20mitos%20sobre%20seguridad%20vial%20infantil.%20Las%20sillas%20de%20coche%20a%20contramarcha.), se van cayendo por su peso, porque los niños, resulta que van encantados en sentido contrario de la marcha, y los padres, también, entre otras cosas por accesorios como éstos, que proporcionan una comodidad tremenda al permitirnos vigilar al bebé mientras conducimos sin necesidad de girar el cuello para echar un vistazo lo que, desde luego, nuestras cervicales agradecerán, además de que evitaremos desviar nuestra atención de la carretera mientras conducimos.

 ![espejo automovil bebe](/tumblr_files/tumblr_inline_o2qs323Skv1qfhdaz_540.jpg)

## Soluciones prácticas

Este **espejo retrovisor** de Axkid se coloca fácilmente con una correas en el reposa cabezas, y queda muy bien fijado, sin necesidad de ventosas que se caen cada dos por tres. Como veis, la instalación no puede ser más cómoda y práctica.

 ![](/tumblr_files/tumblr_inline_o2qs4qXGGH1qfhdaz_540.jpg)

Su gran tamaño, y su rotación de 180 grados permiten de un vistazo a través de nuestro retrovisor, comprobar que nuestro pequeño está bien.

Pero hay padres que son reacios a instalar estas sillas por miedo a que la tapicería del coche sufra las inclemencias de los pequeños salvajes al apoyar los pies contra el respaldo del asiento.

Pues este handicap también quedaría resuelto colocando este **protector de asientos** simplemente enfundándolo en el reposa cabezas que, además, cubre no sólo respaldo, sino también asiento, y sobre el que se coloca la silla de manera que lo protege tanto de la base de la silla como de las pisadas del bebé o niño cuando apoye sus pies.

 ![](/tumblr_files/tumblr_inline_o2qs5tfCdR1qfhdaz_540.jpg)

Además, tiene unos prácticos bolsillos para guardar un botellín de agua, toallitas o sus juguete favorito.

 ![](/tumblr_files/tumblr_inline_o2qs9iNHrD1qfhdaz_540.jpg)

La verdad es que a mi hijo le encanta descalzarse en cuanto sube al coche. Creo que lo toma como su momento de relax porque la postura con las piernas encogidas y apoyadas en el respaldo le encanta, además, se dedica a contar coches y jugar con sus hermanos al juego de los colores que consiste en que cada uno elige un color, y va contando los coches que ve por la carretera de ese color. al final gana quien más coches haya visto de su color. 

¡No os podéis imaginar los piques que se cogen, y lo que se entretienen así!

Y ahora dime, por favor, si todavía te queda alguna duda de que la contramarcha es la manera de viajar más segura para tu hijo.

## Sorteo

Axkid quiere obsequiar a las lectoras de mi blog con dos accesorios para vuestra silla a contra marcha. 

Se trata de un [espejo retrovisor Axkid con giro de 180 grados como éste](http://www.axkid.com/es/product/axkid-baby-mirror/):

 ![](/tumblr_files/tumblr_inline_o2qsba6tSB1qfhdaz_540.png)

Y un [protector de asiento Axkid como éste](http://www.axkid.com/es/product/seat-protection/):

 ![](/tumblr_files/tumblr_inline_o2qsaqnpwC1qfhdaz_540.png)

Para participar, sólo tienes que:

- Dejarme un comentario en este post explicando el **principal motivo por el que tu hijo viaja a contra marcha** , o si no lo hace, por qué querrías que lo hiciera, y la red social y alias.  
- **Compartir el sorteo** en Twitter o Facebook (marca la opción de público para que podamos comprobarlo) con el hastag #acontramarcha, e indicar dónde lo has compartido y tu alias para que podamos comprobarlo.  

El sorteo tiene alcance nacional.

El plazo finaliza el 3 de marzo.

Se eliminarán las participaciones que considere fraudulentas según mi criterio.

¡Cuéntame y participa!

¿Y tu peque, por qué viaja a contra marcha?