---
date: '2017-01-03T01:11:52+01:00'
image: /tumblr_files/tumblr_inline_oj6gmqxyyf1qfhdaz_540.jpg
layout: post
tags:
- navidad
- crianza
- trucos
- decoracion
- participoen
- planninos
title: 'Taller Ikea: Adornos de Navidad'
tumblr_url: https://madrescabreadas.com/post/155318590449/tallerikeaninos
---

Esta Navidad he tenido la suerte de impartir un taller de adornos para el árbol en Ikea, y fue una experiencia muy especial porque creo que logré el objetivo que me propuse de motivar a las familias para que se implicaran en hacer una actividad juntos disfrutándola a tope.

 ![](/tumblr_files/tumblr_inline_oj6gmoHvpr1qfhdaz_540.jpg)

Dentro de la serie de talleres de “Let’s play”, [Ikea](http://www.ikea.com/es/es/) me encargó que me hiciera cargo de uno donde los pequeños con sus familias se expresaran artísticamente y compartieran tiempo juntos.

Con LET’s PLAY, Ikea persigue defender el derecho de los niños y niñas a jugar y fomentar los beneficios del juego en su desarrollo emocional, social y físico convirtiéndoles en personas más creativas, fuertes y activas.

De hecho, la ONU en su Convención sobre los Derechos del Niño reconoce “el derecho del niño al descanso y el esparcimiento, al juego y a las actividades recreativas”.

Dentro de esta campaña, las 16 tiendas de IKEA en la Península celebraron dos jornadas, 16 y 17 de diciembre, dedicadas a fomentar las actividades lúdicas en familia. Y es que, según se desprende del estudio de IKEA PLAY REPORT de 2015, casi la mitad (49 %) de los padres sienten que no tienen tiempo suficiente para jugar con sus hijos.

Aún así, 9 de cada 10 padres que participaron en el estudio están de acuerdo en que el juego es esencial para el desarrollo del niño, así como para su bienestar y felicidad.

IKEA pretende con esto inspirar a las familias para que disfruten en sus casas del juego.

Y como estamos en Navidad, me marqué como objetivo lograr que el espíritu navideño invadiera la actividad… y creo que lo conseguí!

Era una tarde lluviosa, de las pocas que hay en Murcia, pero ésta, cayó el diluvio por aquí.

 ![](/tumblr_files/tumblr_inline_oj6gmpdVJs1qfhdaz_540.jpg)

Me acompañó Princesita, mi hija mayor, que me fue de gran ayuda durante la preparación del taller y en la ejecución (gracias, cariño).

 ![](/tumblr_files/tumblr_inline_oj6gmpkP9P1qfhdaz_540.jpg)

Verás, hicimos varios adornos para el árbol de Navidad con distintos grados de dificultad, según las edades, para que todos los miembros de la familia se sintieran cómodos y pudieran disfrutar de la manualidad como si fuera un juego.

## Materiales

Usamos materiales sencillos:

-[Kit MALA de papeles decorativos](http://www.ikea.com/es/es/catalog/products/20193489/) para manualidades de Ikea, que contiene varios tipos de papel, y de varios colores, como cartulina ondulada, papel de seda, papel charol…

-[Pinturas MALA de Ikea dorada y plateada](http://www.ikea.com/es/es/catalog/products/70266299/)

-Spray dorado y plateado

-Cola blanca

-Cordón

-Taladro

-Tjeras

 ![](/tumblr_files/tumblr_inline_oj6gmqxyyf1qfhdaz_540.jpg)
 
## La estrella

-Con estas [plantillas de estrellas](http://papelisimo.es/2014/12/plantilla-estrellas-para-imprimir/) las dibujamos, y recortamos dos y las pegamos por la cara lisa (no ondulada).

 ![](/tumblr_files/tumblr_inline_oj6gmqd0zf1qfhdaz_540.jpg)

-Hicimos un agujero con el taladro

-Pintamos los bordes por delante y por detrás (tarda un poquito en secarse)

 ![](/tumblr_files/tumblr_inline_oj6gmr8pZM1qfhdaz_540.jpg)

-Metimos el cordón por el agujero

## El pompón

-Con 10 papeles de seda MALA partidos por la mitad hicimos un abanico doblándolos en zigzag.

-Con un trocito de cordón, los atamos por el centro, y dejamos un trozo largo para atarlo al árbol.

-Luego separamos cada papel con cuidado y paciencia hasta formar el pompón.

-Una vez terminado rociamos de spray

 ![](/tumblr_files/tumblr_inline_oj6gmrFbzX1qfhdaz_540.jpg)
 
## La cajita de regalo

-Con esta [plantilla de caja](/tumblr_files/Plantillaparacubo2-2.jpg) recortamos la silueta.

 ![](/tumblr_files/tumblr_inline_oj6gmsBMHr1qfhdaz_540.jpg)

-La pintamos con las pinturas MALA

-La montamos y pegamos

 ![](/tumblr_files/tumblr_inline_oj6gmsYroW1qfhdaz_540.jpg)

-La atamos con el cordón y pusimos cinta decorativa

Estábamos tan atareados que el tiempo pasó rápido, y sé que les hubiera gustado seguir haciendo adornos más tiempo porque se sintieron como en casa, y disfrutaron los unos de los otros.

El objetivo era hacer un adorno para regalarlo a otra persona de la familia para que éste lo colgara el árbol.  
Así, cuando los terminaron cada familia se acercó al árbol de Navidad, se regalaron unos a otros los adornos, los colgaron y se hicieron una foto de recuerdo en el árbol.

 ![](/tumblr_files/tumblr_inline_oj6gmsrDoQ1qfhdaz_540.jpg)

Mirad qué trabajo en equipo más bien hecho, y qué bonito quedó el árbol.

 ![](/tumblr_files/tumblr_inline_oj6gmtmzis1qfhdaz_540.jpg)

Hasta los más pequeños quisieron dejar su huella.

 ![](/tumblr_files/tumblr_inline_oj6gmuDiI01qfhdaz_540.jpg)
 
## Mural de deseos navideños

 ![](/tumblr_files/tumblr_inline_oj6gmumOA21qfhdaz_540.jpg)

Como broche final del taller, cada participante escribió en un mural su deseo navideño para que quedara de recuerdo.

Fue precioso cómo se implicaron los niños y la inocencia de sus deseos, algunos de los cuales fueron:

> Deseamos que todos los niños estén calentitos y junto a sus familias.

 ![](/tumblr_files/tumblr_inline_oj6gmvvrCP1qfhdaz_540.jpg)

> Yo deseo tener un perrito

> Yo deseo nunca dejar de desear pedir deseos

 ![](/tumblr_files/tumblr_inline_oj6gmvaeYN1qfhdaz_540.jpg)

> Yo deseo que mi familia tenga una feliz Navidad

> Yo deseo estar con mi familia en Navidad

> Que todos estemos felices en Navidad

 ![](/tumblr_files/tumblr_inline_oj6gmvUHA01qfhdaz_540.jpg)

> 

Que los Reyes traigan toda la ilusión y todos los regalos del mundo para todos los niños

Creo que logramos una atmósfera cálida en la que todos nos inspiramos, nos sentimos como en casa y redescubrimos el juego en familia, la importancia de compartir tiempo y construír algo en equipo.

Estoy muy agradecida a Ikea por haberme dado la oportunidad de desarrollar un taller y pasar un rato tan agradable, y en especial a Mercedes, de Ikea Murcia, por apoyarme y ayudarme en todo.

Ya sabes que puedes consultar más [talleres interesantes y apuntarte a través de la web de Ikea](https://www.family.ikea.es/eventos-y-seminarios/) de forma gratuita. 

¡Cada semana hay cosas nuevas!