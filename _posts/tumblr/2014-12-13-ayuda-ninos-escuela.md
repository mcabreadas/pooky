---
date: '2014-12-13T08:00:30+01:00'
layout: post
tags:
- ad
- colaboraciones
- crianza
- participoen
title: Ayuda a que los niños de Zambia puedan ir al cole. Que no paren de bailar.
tumblr_url: https://madrescabreadas.com/post/105070756915/ayuda-ninos-escuela
---

Hagamos #queNoParenDeBailar porque para algunos niños cada día que pueden ir a la escuela es una fiesta:

<iframe frameborder="0" height="315" src="//www.youtube.com/embed/-c6M9U-eKjA" width="560"></iframe>

¿Tegusta la canción? Pues bájatela por el precio que estimes oportuno y podrás así ayudar a miles de niños a #queNoParenDeBailar ningún día por no poder ir al cole.

El objetivo de esta campaña es conseguir fondos a favor de los programas de educación de las Naciones Unidas y ayudar así a que  **miles de niños puedan ir al colegio en Zambia**.  

“Que no paren de bailar” se enmarca dentro del programa ING Chances for Children que el [Grupo ING](http://www.ingdirect.es/) y [UNICEF](http://www.unicef.es/) Comité Español desarrollan conjuntamente a nivel mundial desde 2005, con el objetivo de trabajar por el derecho a la educación de niños en países como Zambia, Etiopía, Nepal e India.

Desde su puesta en marcha ha logrado recaudar 26,7 millones de euros para escolarizar a casi 1 millón de niños en situación de riesgo.

Juntos conseguiremos que #noParenDeBailar