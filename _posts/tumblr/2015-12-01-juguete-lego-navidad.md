---
date: '2015-12-01T19:03:56+01:00'
image: /tumblr_files/tumblr_inline_nyovb5wYz71qfhdaz_540.png
layout: post
tags:
- navidad
- crianza
- trucos
- reyesmagos
- regalos
- premios
- familia
- juguetes
title: 'Concurso “Tu Carta a los Reyes Magos” 2016: Lego Ultra Agents'
tumblr_url: https://madrescabreadas.com/post/134342055429/juguete-lego-navidad
---

![](/tumblr_files/tumblr_inline_nyovb5wYz71qfhdaz_540.png)

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

[Aquí tenéis todos los regalos entre los que podéis elegir](/2015/12/01/juguetes-navidad-gratis.html).

Uno de los regalos que podéis incluír en vuestra carta a los Reyes Magos para participar en el concurso es este [set de Lego Ultra Agents](http://www.lego.com/es-es/ultraagents).

 ![](/tumblr_files/tumblr_inline_nyovi0K8W71qfhdaz_540.jpg)

A quién no le gusta jugar a los detectives: buscar pistas, resolver misterios… Por eso los Ultra Agents de Lego son ideales para niños y niñas a niños a partir de 9 años y una de sus principales características es que combina la construcción con aplicaciones móviles en las que se podrán encontrar misiones secretas. 

Así podrán utilizar los vehículos y armas de los sets en su tablet gracias a un código que poseen los productos en su parte inferior, legible por el dispositivo electrónico. 

Con esta nueva línea LEGO busca dar una dosis de mayor realismo a sus productos, haciendo que los niños se sumerjan en las apasionantes aventuras de los súper agentes. 

De repente recibirán una llamada urgente para el superagente novato Max Burns!: 

“Adam Ácido está intentando robar cajas de sustancias tóxicas de los muelles de Astor City para la supervillana Toxikita.”

Deberán atraparlo con el asombroso quad y deslizador a reacción “2 en 1” y hacer fracasar el maléfico plan disparando los cañones del vehículo o desmontándolos y usándolos como armas de mano. 

También podrán activar el deslizador a reacción para trasladar la emocionante persecución al agua fijando el gancho trasero para retener el deslizador a reacción y, cuando hayan localizado las cajas que faltan, llevarlas hasta un lugar seguro. Incluye 2 minifiguras: superagente Max Burns y Adam Ácido.

## Para conseguir la el set Lego Ultra Agents

-Deja un comentario en este post con tu carta a los Reyes Magos. Se admiten fotos.  Sé original porque ganará la carta más original y auténtica, a elegir por la marca.

-El plazo para subir tu carta finaliza el 15 de diciembre, y el resultado se publicará en este blog en los días sucesivos

-El ámbito del concurso es nacional.

¡Tienes hasta el 15 de diciembre!

¿Lo quieres para tu peque?