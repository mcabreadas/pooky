---
layout: post
title: La soledad de la crianza
date: 2013-06-22T11:01:00+02:00
image: /tumblr_files/tumblr_pe2csi7cT41qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/53583998391/la-soledad-de-la-crianza
---
Sí. Soledad. Aunque estés rodeada de gente y manos para ayudar. No sé cómo explicarlo…

A veces parece que Leopardito y yo fuéramos “dos contra el mundo”. Que nos hubiéramos quedado fuera mientras éste gira y gira sin detenerse ni un momento, ni siquiera para [esperarse a que le de una toma, y eso que alguna vez lo he intentado (y casi lo he logrado)](http:///2013/05/11/lactancia-con-familia-numerosa-es-posible-sin.html-morir-en) porque soy rebelde por naturaleza, pero no… no se para en realidad.

Todo sigue mientras crías, y tú lo ves pasar y piensas, ya llegará mi momento. Y es cierto que llega, pero a veces se hace duro vivir en una realidad paralela. Que conste que no me estoy quejando de nada, eh? Ni me cabrea… bueno, algunas situaciones concretas, sí.

Pero no sólo aísla una determinada forma de criar más que otra por exigir más o menos dedicación, sino la crianza en general. Por lo común, quedas fuera de la mayoría de eventos sociales (más aún si son nocturnos o requieren pernoctación fuera del hogar, y das teta). 

Pero no se trata ya sólo de eso, sino de que no tienes tiempo ni de estar informada de lo del DNI de la Infanta ni siquiera de lo que pasa en tu entorno más cercano. Te vuelves un poco invisible y tus amistades dejan de llamarte para quedar… total, como estas criando no vas a poder a no ser que te lleves a la tropa, vas a estar tan cansada que vas a decir que no o, en el mejor de los casos, si finalmente haces los juegos malabares pertinentes y logras aparecer con unas ojeras mal disimuladas con corrector, hasta los pies, quizá te pases el tiempo hablando de lo que mejor se te da en esta etapa de tu vida: criar hijos. 

Llega un momento en que no cuentan contigo porque estás en tu isla, y les encanta organizar eventos nocturnos porque la noche (les confunde) es más guay, y ya ni se planten el que tu puedas ir o no porque, total, perteneces a otra dimensión, estás criando.

Que no digo yo que se tenga que adaptar todo el mundo a las mamás con bebés, pero de vez en cuando tenerlas en cuenta y que nos sintamos parte de él no estaría mal. Me siento mal, si. Me siento fuera porque ni del funeral del familiar de una amiga me he enterado. Y eso sí que me ha dolido. 

No me han avisado, cierto, pero mi aislamiento informativo no ayudó (lo de la realidad paralela que os decía antes). Te quedas off line, y si trabajas desde casa, aún más. Parece que volvieras del destierro cuando te acercas a la oficina a algún menester. A la gente le extraña verte, y aún más verte sola, sin los churumbeles colgando.

Es así, y no me quejo, ni culpo a nadie, simplemente explico cómo me siento. Huelga decir que a mis hijos los adoro y lo haría una y mil veces, pero la soledad a veces pesa. 

Es así.… ¿Soy la única?