---
date: '2015-02-09T07:00:33+01:00'
image: /tumblr_files/tumblr_inline_njgucmGYua1qfhdaz.png
layout: post
tags:
- salud
- crianza
- seguridad
title: 'Atragantamiento y disfagia en niños: cuando la medicina sola no basta.'
tumblr_url: https://madrescabreadas.com/post/110517721868/disgagia-ninos-atragantamiento
---

Hace un tiempo Princesita se atragantó comiendo un bocadillo. No fue grave, en todo momento respiró, pero se asustó muchísimo, tosió, lloró, y seguramente se le quedó la garganta irritada o algo dañada por el esfuerzo.

Quiero compartir con vosotros nuestra experiencia, contando con la opinión experta de la Dra. Amalia Arce, autora del blog “[Diario de una mamá pediatra](http://www.dra-amalia-arce.com/)”, y que recientemente ha publicado un libro con el mismo nombre, que aprovecho para recomendaros y que, además, lo regaléis a papás y mamás recientes porque es un “must” para toda librería de casa con niños.

 ![libro-mama-pediatra](/tumblr_files/tumblr_inline_njgucmGYua1qfhdaz.png)

La Dra. Arce ha querido colaborar en este post para dar su visión profesional, a pesar de su apretada agenda, haciendo un gran esfuerzo, lo que le agradezco enormemente.

El texto está escrito entre las dos. Las partes en cursiva y entre comilladas son de la Mamá Pediatra:

_“Los atragantamientos se pueden producir en cualquier momento y en cualquierpersona. Se produce por una incoordinación entre la vía digestiva y la víarespiratoria, que convergen en la faringe. La madurez para deglutir alimentossólidos se va produciendo en los primeros meses de vida. Por eso laalimentación del recién nacido y lactante pequeño al inicio es necesariamente líquida._

_Personas más mayores o con algún problema neurológico o por problemas anatómicos, también pueden tener más dificultades para la alimentación y atragantarse con más frecuencia.”_

Este episodio, que nunca anteriormente le había sucedido, se repitió alguna vez más en las dos siguientes semanas, desembocando finalmente en  un miedo atroz a tragar sólidos.

La hora de la comida le producía verdadero pavor a mi niña, hasta el punto de que se le “olvidó” tragar. Tenía que concentrarse para deglutir correctamente, y todo el rato carraspeaba, tosía y bebía agua para ayudar a pasar los alimentos. Lo hacía llorando y asustada.

_“Cuando ocurre algún evento adverso en una actividad que teníamos automatizada, puede ocurrir que parezca que haya que volver a aprender de nuevo. Esto por ejemplo les ocurre a los bebés cuando justamente se sueltan a caminar. Si tienen una caída importante pueden coger miedo y aquellos primeros pasos que empezaban a estar automatizados, deben volver a “pensarse”. A veces tardará bastantes semanas en perder el miedo y volverlo a intentar.”_

Al tercer día sin ingerir sólidos la llevé al médico muy preocupada porque es una niña muy delgadita, sin un átomo de grasa en su cuerpecillo. Temía que se desnutriera. 

_“Bueno aquí ya sabéis que los pediatras –por lo menos de mi escuela- somos poco alarmistas. Un niño no se desnutre en poco tiempo, y los delgaditos no tienen por qué estar desnutridos, a veces justo lo contrario!”_

Le intentaron hacer una fibrolaringoscopia, sin éxito, para descartar cualquier lesión en la garganta que le estuviera impidiendo tragar, aunque ya me adelantaron que parecía más bien de tipo nervioso, por el miedo al atragantamiento provocado por el que sufrió semanas antes.

_“La fibrolaringoscopia es una prueba complementaria de imagen, que permite a través de una cámara que va insertada en un pequeño tubo, recorrer la anatomía de la faringe y la laringe. Al ver directamente estas zonas anatómicas, habitualmente ocultas, puede visualizarse si existe alguna causa “física” que cause la dificultad para deglutir.”_

La niña no soportó el dolor del tubito bajando por la nariz hasta la garganta, y no la forzamos ni un poco. Yo estuve todo el tiempo con ella agarrándola de la mano, y cuando vi que sufría, le dije al doctor que parara, y no le practicó la prueba, quedando pendiente de observar la evolución.

El papel de los médicos fue, más bien tranquilizar a Princesita e infundirle confianza en ella misma para que perdiera el miedo a tragar. Le explicaron que no estaba enferma, que no tenía nada malo en la garganta, y que no iba a atragantarse más, que sólo le había pasado una vez en su vida. Fueron encantadores.

_“Aunque la laringoscopia es la prueba que permite visualizar la anatomía, realmente por la historia clínica, era poco probable que hubiera un factor anatómico causante del problema. Entiendo que por eso no se insistió en realizar la prueba. En caso de sospechar una masa o algún objeto enclavado, no hubiera habido mucha alternativa….Eso sí, en pruebas dolorosas o que generan ansiedad, siempre hay posibilidad de utilizar sedación.”_

Yo tenía claro mi importante papel en el problema. Sabía que mi apoyo, y mi proximidad le darían la seguridad necesaria para vencer el miedo. Le dije que juntas íbamos a aprender a comer de nuevo, como ya lo hicimos una vez cuando era bebé, y lo volveríamos a conseguir.

_“Cuando el origen de los problemas es psicógeno, o no lo es, pero se añade un componente psicógeno, desatender esta necesidad es sinónimo de fracaso. No solo los fármacos curan….”_

Para que lograra tragar automáticamente de nuevo, me sentaba con ella en cada comida y veíamos dibujos animados con el Ipad para que estuviera distraída, o le hablaba de cosas interesantes, mientras ella hacía el movimiento de tragar exageradamente y se concentraba para hacerlo correctamente y no atragantarse. A veces las lágrimas le caían por la cara, pero yo le decía que tarde o temprano lo conseguiríamos, que yo iba a estar siempre con ella.

Teníamos una canción para pasar el rato de la comida. A ella le gustaba poner siempre el video en la mesa, y nos reíamos juntas. Se trata de cinco monitos pequeños que saltan encima de una cama y cada vez se va cayendo uno, y van quedando menos. Es una canción infantil en inglés para que los niños pequeños aprendan a restar. Se convirtió en nuestro talismán.

<iframe src="https://www.youtube.com/embed/b0NHrFNZWh0?rel=0" width="500" height="315" frameborder="0"></iframe>

También le conté un cuento sobre una niña a la que le daba miedo tragar, y cómo lo superó. Le encantó y le ayudó mucho. Durante esos días fui su sombra, incluso me acostaba una ratito con ella por las noches antes de irme a mi cama.

Una semana estuvimos con los monitos saltando en la cama acompañándonos en todas nuestras comidas. Y mi niña fue mejorando. Cada pequeño logro lo celebrábamos, fue cogiendo confianza otra vez y se dio cuenta de que podía hacerlo y finalmente lo consiguió.

_“Paciencia, plantearse pequeñas metas cada día y sobretodo confiar en que lo van a conseguir y que vamos a estar a su lado para acompañarles.”_

Aprendió a comer de nuevo. Tan sólo tenía que vencer el miedo, y mi pequeña valiente lo logró superar. Sólo necesitaba que su madre la cogiera de la mano y le dijera que podía y que no iba a estar sola.

Hasta la fecha no ha vuelto a tener problemas para tragar alimentos, y de esto hace ya casi un año.

_“El papel de los padres es fundamental cuando el origen de los problemas es psicógeno. No obstante, en ocasiones puede ser de ayuda consultar con algún profesional. Y no tiene por qué darnos vergüenza, saber pedir ayuda también es importante cuando aparecen dificultades._

_Por último y no menos importante, recordad padres recientes, que aunque el caso del atragantamiento no fue a más, es interesante tener cierta formación en técnicas de desobstrucción de la vía área en casos de atragantamiento. Saber algunas pequeñas maniobras, puede salvar una vida!”_

¿Te ha pasado algo parecido?

Por si acaso, te dejo estos dos videos del [Hospital de Nens](http://www.hospitaldenens.com/), de Barcelona, que explican cómo actuar en caso de atragantamiento de un lactante, el primero, y de un niño, el segundo, No dejéis de verlos porque podéis salvar vidas.

<iframe src="https://www.youtube.com/embed/_Zve7uXSQVk?rel=0" width="560" height="315" frameborder="0"></iframe><iframe src="https://www.youtube.com/embed/4V3LYMRGge4?rel=0" width="560" height="315" frameborder="0"></iframe>