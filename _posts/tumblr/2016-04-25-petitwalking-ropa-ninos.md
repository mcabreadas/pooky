---
date: '2016-04-25T16:39:59+02:00'
image: /tumblr_files/tumblr_o65ixoX9Gf1qgfaqto1_1280.jpg
layout: post
tags:
- planninos
- eventos
- moda
- familia
title: petitwalking ropa ninos
tumblr_url: https://madrescabreadas.com/post/143378242274/petitwalking-ropa-ninos
---

![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto6_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto7_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto8_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto9_1280.jpg)  
 ![](/tumblr_files/tumblr_o65ixoX9Gf1qgfaqto10_1280.jpg)  
  

## Madrid Petit Walking Primavera/Verano (Parte II)

He tenido la suerte de poder asistir al desfile de moda infantil primavera/verano Madrid Petit Walking, organizado por la revista Petit Style y Miramami.

Además, me fui con mis tres fieras, quienes disfrutaron de lo lindo en una tarde en la que burlamos a la lluvia, y finalmente pudimos ver en pasarela a todas las marcas

Te dejo algunas fotos de las marcas que desfilaron a modo de avance. Más adelante te contaré todo con más detalle:

Mami María Cóndor, IDO, Nanos, Name it, Rosalita Señoritas, Neck & Neck, Genuins, Cóndor Swim, Zippy.

## Galería completa de fotos del desfile

Para acceder a la galería completa de fotos del desfile pincha en la siguiente foto:

[![Madrid Petit Walking](/tumblr_files/26346501770_a8ea242c38_z.jpg)](https://www.flickr.com/photos/142190598@N06/albums/72157665221467953 "Madrid Petit Walking")<script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>