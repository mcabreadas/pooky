---
date: '2014-09-12T10:00:31+02:00'
layout: post
tags:
- crianza
- hijos
- maternidad
- postparto
- embarazo
- familia
- bebes
title: A una mamá reciente del segundo | MAMÁ Orquídea Dichosa blog
tumblr_url: https://madrescabreadas.com/post/97287857908/a-una-mamá-reciente-del-segundo-mamá-orquídea
---

[A una mamá reciente del segundo | MAMÁ Orquídea Dichosa blog](http://www.laorquideadichosa.com/2014/09/mama-reciente-del-segundo.html#comment-618719)  

Ésta es la carta que escribí a mi amiga Vanesa cuando dio a luz a su segundo retoño. Estoy emocionada porque la ha publicado en su blog, uno de los que más me gustan de la blogosfera maternal: [La Orquidea Dichosa. Ahora mamá](http://www.laorquideadichosa.com/). Arriba tenéis el enlace a su post.

_“Querida amiga:_

_Acabas de vivir por segunda vez una de las experiencias más maravillosas de la vida, y sin embargo ha sido tan diferente de la primera vez que casi te sientes como primeriza._

_Pero no lo eres, tienes la seguridad y el aplomo de quien ya ha pasado por lo mismo hace un tiempo y lo vivirás todo con más serenidad._

_Tienes un mundo nuevo por descubrir, ser mamá de dos es ser el doble de mamá de lo que has sido hasta ahora. La vida te dará un giro de 180 grados y la familia tendrá que adaptarse al nuevo miembro. Hasta que todos re descubran su nuevo lugar pasará bastante tiempo, y los temidos celos de su hermanito mayor no tardarán en aparecer, aunque no te preocupes demasiado porque van por rachas._

_Tu vida de pareja también sufrirá algunos cambios importantes porque, sobre todo al principio, no tendréis tiempo casi ni de hablar a solas, ya que el mayor demandará tanta atención como el pequeño, y el papá jugará aquí un papel crucial para evitar que se sienta desplazado._

_Cuando acuda a tu mente el miedo o la impotencia piensa que es el cansancio que hace mella en ti. Pide ayuda y déjate ayudar, no quieras hacerlo todo tú. Eres una mujer valiente y valiosa, pero no te exijas ser una heroína. No tienes que demostrar nada. Date permisos, perdonare los errores y tomate un respiro._

_Piensa que el amor es lo único que viola las matemáticas. Descubrirás que cuanto más das, más tienes!!! Y que eres capaz de amar a los dos con todo tu corazón, y dar a cada uno lo que necesita en cada momento._

_Le has hecho a tu hijo mayor el mejor regalo que nadie le hará jamás. Ahora es demasiado pequeño, pero dentro de un tiempo descubrirá su mejor compañera de juegos, su amiga, su confidente y alguien con quién ir de la mano por la vida._  
_Enhorabuena, amiga.”_