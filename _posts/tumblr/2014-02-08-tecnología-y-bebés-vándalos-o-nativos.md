---
layout: post
title: "Tecnología y Bebés: ¿vándalos o nativos tecnológicos?"
date: 2014-02-08T22:12:00+01:00
image: /tumblr_files/tumblr_inline_n0p3qkkEFb1qfhdaz.jpg
author: maria
tags:
  - crianza
  - tecnologia
  - bebes
tumblr_url: https://madrescabreadas.com/post/76034656215/tecnología-y-bebés-vándalos-o-nativos
---

Que la de nuestros hijos es una generación de nativos tecnológicos nadie lo pone en duda, pero que estos pequeños vándalos de la tecnología son capaces de campar a sus anchas por nuestros teléfonos móviles, portátiles, ordenadores y demás artilugios, no deja de entrañar ciertos riesgos para la integridad física de los mismos… y de nuestro corazón.

![image](/tumblr_files/tumblr_inline_n0p3qkkEFb1qfhdaz.jpg)

Sé muy bien de qué hablo, o si no, mira la conversación que ha surgido esta tarde en Twitter sobre las trastadas de nuestros vástagos con nuestros artilugios.

Casi lloro de la risa, aunque algunos casos son sólo para llorar, porque de risa tienen poca, como el del churumbel de Ruth, que se gastó la friolera de 70 € en créditos para un juego de Android que, para colmo no gustaba a su madre. No me extraña que aún le dure el cabreo, sobre todo teniendo en cuenta que también le suele arrancar las teclas del portátil cada dos por tres.

> [@madrescabreadas](https://twitter.com/madrescabreadas) me compro 74,99,€ en créditos para un juego de m.. De Android con el móvil. Todavía cabreada
> 
> — ruth arenas (@rutharenasm)
> [febrero 8, 2014](https://twitter.com/rutharenasm/statuses/432200793710940160)

> [@madrescabreadas](https://twitter.com/madrescabreadas) Y me arranco las teclas del portátil… 3 veces
> 
> — ruth arenas (@rutharenasm)
> [febrero 8, 2014](https://twitter.com/rutharenasm/statuses/432200947138588672)

 ![](/tumblr_files/tumblr_inline_n0rxf8Blvr1qfhdaz.jpg)

Parece generalizado la afición de esta nueva generación a comprar juegos y aplicaciones para móviles… :

> [@madrescabreadas](https://twitter.com/madrescabreadas) el mío compró una app d 8€ de juegos infantiles! Y encima ahora “nobuta”… [#melocomo](https://twitter.com/search?q=%23melocomo&src=hash)
> 
> — laura\_pkfdly (@Laura\_pkfdly)
> [febrero 8, 2014](https://twitter.com/Laura_pkfdly/statuses/432165739861397506)

> [@madrescabreadas](https://twitter.com/madrescabreadas) ah, si, una, peli de dibujos me compró también! Y Estamos hablando de un mico de 16meses! [@Laura\_pkfdly](https://twitter.com/Laura_pkfdly)
> 
> — En Paro Biológico (@EPBiologico) [febrero 8, 2014](https://twitter.com/EPBiologico/statuses/432171111170977792)

…y también para borrarlas, en mi caso, Leopardito liquidó la de Youtube, pero es peor si te pasa como a Krisamant:

> [@madrescabreadas](https://twitter.com/madrescabreadas) el whatsapp… casi lloro al perder toooodas las conversaciones y todos los grupos [#jodioniño](https://twitter.com/search?q=%23jodioni%C3%B1o&src=hash)
> 
> — Krisamant (@Krisamant)
> [febrero 8, 2014](https://twitter.com/Krisamant/statuses/432088879269564416)

…o si les da por escribir mensajes misteriosos en el whatsapp:

[ ](https://twitter.com/madrescabreadas)

> [@madrescabreadas](https://twitter.com/madrescabreadas) un vez envió. Uno que era “mmmmm” y claro todos preguntandome que barruntaba, jajaja [@ypapatambien](https://twitter.com/ypapatambien)
> 
> — En Paro Biológico (@EPBiologico)
> [febrero 8, 2014](https://twitter.com/EPBiologico/statuses/432170666948042752)

<script charset="utf-8" src="//platform.twitter.com/widgets.js" type="text/javascript"></script>

![image](/tumblr_files/tumblr_inline_n0p3rgKTwE1qfhdaz.jpg)

Pero si hay algo que es la debilidad de los bebés son los portátiles. Con ellos pierden la cabeza totalmente y les sale su instinto tecnológico irreflenabe: esa pantalla brillante, esas teclas suaves y cuadraditas, esa sensación de poder al abrirlo y cerrarlo… :

> [@madrescabreadas](https://twitter.com/madrescabreadas) Yo conozco una media naranja que un día se encontró el teclado en árabe xDD [#WTFamilia](https://twitter.com/search?q=%23WTFamilia&src=hash)
> 
> — Y papá también (@ypapatambien)
> [febrero 8, 2014](https://twitter.com/ypapatambien/statuses/432087895231705088)

> [@madrescabreadas](https://twitter.com/madrescabreadas) Arrancó la barra de debajo de la pantalla del portátil. Estaba movida pq se había apoyado en la pantalla y la había vencido
> 
> — LaMamádelaBebé (@laBebedePucca) [febrero 8, 2014](https://twitter.com/laBebedePucca/statuses/432171017491193856)

En fin… estos son sólo unos ejemplos de lo que son capaces de hacer estos vándalos nativos tecnológicos.

Pero seguro que tú tienes alguna historia parecida que contar. A que sí?

*Fotos gracias a:*

*vidayestilo*

*pedagogayculturadigital.blogspot.com*