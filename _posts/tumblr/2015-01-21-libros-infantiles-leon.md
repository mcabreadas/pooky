---
date: '2015-01-21T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_nic6flF2Vg1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- colaboraciones
- ad
- recomendaciones
- educacion
- familia
title: 'Libros infantiles: Cómo esconder un león a la abuela'
tumblr_url: https://madrescabreadas.com/post/108717867293/libros-infantiles-leon
---

![image](/tumblr_files/tumblr_inline_nic6flF2Vg1qfhdaz.jpg)

Seguimos trabajando la pre-lectura con nuestro pequeños de 2 años y medio, [tal y como os contaba](/2014/11/21/libros-cuentos-bebes.html). Para ello los libros de  Helen Stephens son ideales porque las ilustraciones son una pasada, y recuerdan a los clásicos.

[“Cómo esconder un león a la abuela”](http://www.boolino.es/es/libros-cuentos/como-esconder-un-leon-de-la-abuela/) narra una historia entrañable y muy divertida que le sucede en una familia que convive con un león muy cariñoso y bueno cuando invitan a la abuela a pasar un fin de semana y se ven obligados a esconderlo para que ésta no se asuste.

Pero lo más gracioso es que ella trae algo aún más sorprendente en un baúl, que también tratará de ocultar.

 ![image](/tumblr_files/tumblr_inline_nic6o6OQ8y1qfhdaz.jpg)

Me gustan los valores familiares, y el vínculo abuela-nietos tan natural, afectivo y cómplice que transmite el libro que, sin duda, ha hecho que mi hijo adore la historia porque está muy unido a mi madre, incluso llama a la abuela del cuento por el nombre de su abuela real.

Mi pequeño ha comprendido perfectamente la historia sólo mirando las ilustraciones sin necesidad de que yo le lea el libro. Es más, es él quien me la cuenta a mí una y otra vez, porque le encanta explicarme dónde se esconde el león cada vez para que la abuela no lo encuentre.

¡Creo que le gusta tanto porque se imagina a mi madre como protagonista!

¿Habéis probado ya a dar un libro a vuestro hijo?