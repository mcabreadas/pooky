---
date: '2015-04-26T17:00:45+02:00'
image: /tumblr_files/tumblr_inline_nnbipshj9J1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- bebes
- familia
title: Las tareas domésticas con un bebé grande en casa
tumblr_url: https://madrescabreadas.com/post/117428353548/tareas-domesticas-toddler
---

Las tareas domésticas en una familia numerosa de 5 personas se multiplican. Tenía razón [un buen amigo](http://losangelesdepapi.blogspot.com.es/) cuando decía el otro día que con dos hijos hay el doble de trabajo que con uno, pero con tres, se rompe la proporcionalidad porque no hay el triple de trabajo, sino mucho más.

La sensación de agobio cuando ves la casa patas arriba, el lavaplatos sin poner, los cestos de la ropa sucia desbordados, y mil juguetes desperdigados por todos los rincones de la casa hace que la primera reacción que tengamos sea la de salir corriendo, cerrar la puerta de casa tras nosotros, y no parar como si fuéramos Forrest Gump.

Una buena organización, y plantearse metas pequeñas y continuas, como decía Beppo, el amigo de Momo:

 ![image](/tumblr_files/tumblr_inline_nnbipshj9J1qfhdaz_540.jpg)

_“Ves, Momo, a veces tienes ante ti una calle que te parece terriblemente larga que nunca podrás terminar de barrer. Entonces te empiezas a dar prisa, cada vez más prisa. Cada vez que levantas la vista, ves que la calle sigue igual de larga. Y te esfuerzas más aún, empiezas a tener miedo, al final te has quedado sin aliento. Y la calle sigue estando por delante. Así no se debe hacer. Nunca se ha de pensar en toda la calle de una vez, ¿entiendes? Hay que pensar en el paso siguiente, en la inspiración siguiente, en la siguiente barrida. Entonces es divertido: eso es importante, porque entonces se hace bien la tarea. Y así ha de ser. De repente se da uno cuenta de que, paso a paso, se ha barrido toda la calle. Uno no se da cuenta de cómo ha sido, y no se queda sin aliento. Eso es importante”_

(Extracto de Momo. Michael Ende)

Pero la faena se complica si tienes un toddler haciéndote compañía, y persiguiéndote donde quiera que vas por toda la casa. Entonces no nos queda otra que convertirlo en nuestro cómplice, porque si no, no nos va a dejar dar ni un barrrido.

Os pongo algunos ejemplos de tareas que se pueden compartir con un bebé grande, de entre dos y tres años. creedme que lo pasan de miedo porque les hace tanta ilusión sentirse útiles y colaborar, y celebran tanto cada pequeño logro, que es una gozada hacerles partícipes de algunas tareas domésticas, por ejemplo:

## Hacer las camas
 ![image](/tumblr_files/tumblr_inline_nnbiyp9K121qfhdaz_540.png)

Sin duda, la tarea que más carcajadas le provoca a mi pequeño porque al sacudir las sábanas hacemos aire y salta y agita los brazos como un loco mientras se descacharra de risa (lo malo es que nunca quiere parar).

 ![image](/tumblr_files/tumblr_inline_nnbj3maHRt1qfhdaz_540.png)

Las almohadas son su especialidad. Él se encarga de sacudirlas y colocarlas.

 ![image](/tumblr_files/tumblr_inline_nnbj5uJvsp1qfhdaz_540.png)

## La colada

Mientras yo echo el quitamanchas en cada prenda él va metiendo las que ya están revisadas en la lavadora.

 ![image](/tumblr_files/tumblr_inline_nnbj8ouHi61qfhdaz_540.png)

Luego pongo el detergente y suavizane en el cajetín, y llega la parte más divertida para él: darle a los botones para programarla!

 ![image](/tumblr_files/tumblr_inline_nnbjahO6vf1qfhdaz_540.png)

Pero la cosa no acaba aquí, cuando la ropa ya está lavada, él me va dando las pinzas para tenderla.

 ![image](/tumblr_files/tumblr_inline_nnbjclZRl51qfhdaz_540.png)

Y cuando ya está seca, yo la doblo y el peque empareja calcetines, y después la va distribuyendo en montoncitos por las habitaciones,

 ![image](/tumblr_files/tumblr_inline_nnbjf2MUjO1qfhdaz_540.png)

y la suya, la cuelga en su armario, que está a su altura.

 ![image](/tumblr_files/tumblr_inline_nnbjfxATtx1qfhdaz_540.png)

## Los mejores pinches

Pelar dientes de ajos o los huevos duros son de las pequeñas tareas que podéis dar a un toddler en la cocina, porque les va a encantar porque sus pequeños dedos tienen facilidad para quitar las pieles. 

 ![image](/tumblr_files/tumblr_inline_nnbjizaSKz1qfhdaz_540.png)

Y lo del rodilo ya es delirio…

 ![](/tumblr_files/tumblr_inline_nnfcsin4WM1qfhdaz_540.jpg)

Otra cosa que le fascina es pasar la mopa, lo que pasa es que a veces la cosa acaba saliéndose de madre y acaba haciendo el gamberro con el palo, pero lo hace bastante bien.

Pero seguro que a ti se te ocurren muchas tareas domésticas más que compartir con un bebé grande. 

¿A que sí?