---
layout: post
title: Vestidos de primera Comunión. Nuevos estilos
date: 2018-05-09T19:59:33+02:00
image: /tumblr_files/tumblr_inline_nojmj4gr6g1qfhdaz_540.jpg
author: .
tags:
  - 6-a-12
  - comunion
  - moda
tumblr_url: https://madrescabreadas.com/post/173740404284/vestidos-primera-comunion-nina
---
El Día Mágico by FIMI, única feria internacional y profesional especializada en moda de comunión y ceremonia, ha celebrado su sexta edición del 4 al 6 de mayo. Por primera vez, después de cinco ediciones, el certamen tuvo lugar en Valencia donde reunió en su escaparate comercial un total de **140 firmas especializadas en moda** y complementos para vestir a los niños en los días más señalados.

Me encanta esta feria, recuerdo cuando fui con mi hija hace dos años al [Paseo Mágico FIMI ](https://madrescabreadas.com/2016/05/08/comunion-ceremonia-2017.html), y las [propuestas para organizar la primera comunión](https://madrescabreadas.com/2016/04/08/comunion-vestido-recuerdos.html) que compartí en el blog.

## ¿Cuánto nos gastamos en las Comuniones?

En el año 2016 se celebraron en España 240.094 comuniones. Un sector con una gran tradición en nuestro país y que, según estimaciones basadas en datos oficiales, llegaría a mover más de 500 millones de euros al año, entre el traje, complementos y los diversos gastos originados por la ceremonia.

Si ponemos la lupa en el gasto global para celebrar una comunión, éste varía dependiendo de la comunidad autónoma. A la cabeza en este ranking se sitúan Madrid, Comunidad Valenciana y Catalunya, coincidiendo sus capitales como las más caras, donde **el coste medio es de unos 3.171 euros.**

Si nos vamos al otro extremo, se sitúa Canarias y en concreto Tenerife, la ciudad más barata para celebrar una comunión, con casi la mitad del gasto total.

![vestido comunion nina](https://d33wubrfki0l68.cloudfront.net/16efb0c4fedcf1a956719e08f17692fb72875a71/c699f/tumblr_files/tumblr_inline_nojmj4gr6g1qfhdaz_540.jpg)

## Nos gastamos más en las comuniones de las niñas

Si entramos en detalle a los gastos, atisbamos diferencias entre los gastos para el vestido de las niñas y el traje de los niños. Atendiendo a los datos ofrecidos por la Unión de Consumidores, **la media en el coste del vestido de la niña estaría alrededor de 480€**

Esta cantidad depende de la calidad de los tejidos, el trabajo de elaboración que lleven o si el modelo elegido es de algún reconocido diseñador, por lo que podemos encontrar en el mercado con **trajes de comunión de hasta de 1.235€**

Todo ello sin contar con una larga serie de complementos necesarios para que la niña luzca estupenda en su Día Mágico. Entre los complementos extra podemos añadir cancanes, guantes, diademas, flores, lazos, zapatos, medias, adornos para el pelo… pudiendo sumar entre 50 y 300 euros al coste total

## Los trajes de comunión de niño son más económicos

En el caso de los niños, el traje será más económico. De media, según la misma fuente, un traje de almirante **puede costar desde 350€ hasta los 500€.** Lo mismo ocurre con otras opciones, traje de marinero de 300€ de media o trajes de chaqueta que según la firma pueden llegar a costar unos 400€

Además, al traje se le puede añadir chaleco, corbata, zapatos, calcetines, tirantes y demás complementos, sí el niño va vestido de marinero o almirante, los galones, sin añadir joyas ni relojes, el precio de los complementos puede llegar a superar los 300 euros

![traje comunion nino](https://d33wubrfki0l68.cloudfront.net/4357e2554f04176945561f11032b7a6c78111893/c01b8/tumblr_files/tumblr_inline_oqckhnyy2w1qfhdaz_500.png)

## ¿Vestido de Comunión tradicional o moderno?

Los estilos para vestir a la niña el día de su comunión se renuevan, evolucionan o se reinterpretan… pero el vestido clásico de comunión está presente todas las temporadas. Las firmas utilizan los tejidos más nobles en este estilo para dar a las prendas ese **toque de distinción** que marca la tradición.

Lorzas y entredoses, combinados con colores cálidos y rematados con mínimos detalles, hacen del vestido clásico de comunión, un vestido sencillo y a la vez atemporal **capaz de dejar una huella indeleble en el tiempo**. Son trajes elegantes, con clase y elaborados de forma artesanal para conseguir efectos y terminaciones propias de la alta costura.

Detalles florales, encajes de guipur y valencié, delicadas puntillas, entre otros detalles, todos ellos trabajados por manos expertas y que no dejan nada al azar, con lo que consiguen que cada vestido sea una prenda única y especial.

### Vestidos de Comunión Boho-chic

La evolución natural y la adaptación a los nuevos estilos de vida dieron pie a la aparición de colecciones con un aire romántico, más bohemio y boho-chic que desprenden elegancia a la vez que alegría y funcionalidad.

En este estilo se utilizan nuevos **tejidos vaporosos y coloridos** que dan a la colección una sensación de frescura; diseños con tules delicados, lisos y bordados; batistas y muselinas también bordadas, sin olvidarse de las nobles y siempre exquisitas sedas. Todo ello complementado con los mejores encajes de valenciennes, guipures, tules, etc, que otorgan la sofisticación necesaria que requiere este estilo.

### Vestidos de Comunión Vintage

Otro de los estilos más de tendencia es el “vintage”. Son principalmente colecciones confeccionadas con una cuidada selección de tejidos y pasamanerías. Todas sus prendas son cuidadas con mucho mimo y hasta el más mínimo detalle, incluso algunas de ellas han sido elaboradas de manera artesanal con bordados especiales a mano.

Este estilo **se caracteriza por sus líneas sencillas y elegantes,** siempre con innovación en sus diseños; tejidos vaporosos y con caída de sus tules bordados, sedas, linos naturales, plumeti, encajes, colores empolvados, suaves, etc. Y donde no faltan detalles florales, sobre todo en los complementos como diademas de flores con velos, mantillas o capotas con velos cortos muy novedosos.

### Vestidos de Comunión de corte imperio

Además, existen los vestidos de corte imperio donde las firmas consiguen un **equilibrio perfecto con la combinación de los tejidos** más vaporosos y la utilización de lazadas y flores creando efectos que enamoran.

La confección con linos y tules, y el empleo que hacen de los bordados y encajes, hacen que las prendas conviertan a las niñas en bellezas etéreas y delicadas.

### Flores. El complemento de comunión perfecto

Los complementos de las niñas se han convertido en algo fundamental para terminar el look de comunión, sin duda el complemento estrella de esta temporada son los realizados con flores o con materiales que hacen referencia a estos elementos, ya que ayudan a endulzar y hacer más natural y frescos los looks de las niñas, así como bonitos canotiers, coronas, tocados y pequeños detalles irresistibles para los recogidos.

## Un desfile de comunión mágico

Amaya Comunión, Barcarola, Beatriz Montero, Hortensia Maeso, Javilar, Marita Rial, Mercedes de Alba y Querida Philippa han sido las firmas que han mostrado durante la celebración de Día Mágico by FIMI sus propuestas en el único **desfile profesional e internacional especializado** en este sector de moda infantil.

Os dejo una muestra, aunque al final del post tenéis un enlace con la galería completa de fotos de Marcos Soria:

### Vestidos de Primera Comunión Amaya

![vestido comunion amaya charleston](https://d33wubrfki0l68.cloudfront.net/ecb93f5162b307c404e050103ad15decfe7aae7c/32d0f/tumblr_files/tumblr_inline_p8f6lolzh51qfhdaz_540.jpg)![vestido comunion amaya charleston blanco](https://d33wubrfki0l68.cloudfront.net/cbc55a668665a9bce5ac19b969cce01ae3d3df84/96768/tumblr_files/tumblr_inline_p8f6lw9oq81qfhdaz_540.jpg)

### Vestidos de Primera Comunión Barcarola

![vestido primera comunion barcarola rosa](https://d33wubrfki0l68.cloudfront.net/61c07e02ad291c5537ac0bea94b443d2296ce079/96fa2/tumblr_files/tumblr_inline_p8f6jf3xce1qfhdaz_540.jpg)![vestido comunion barcarola chaqueta](https://d33wubrfki0l68.cloudfront.net/ecb48dba1b83f3f874a1189fa2a90bdbcf6e8071/d7b75/tumblr_files/tumblr_inline_p8f6jtprdh1qfhdaz_540.jpg)

### Vestido de Primera Comunión de Beatriz Montero

![vestido comunion beatris montero](https://d33wubrfki0l68.cloudfront.net/01e2dd8887be4cf9669756592609781467846438/10fb6/tumblr_files/tumblr_inline_p8f6htlmv91qfhdaz_540.jpg)

### Vestido de Primera Comunión de Hortensia Maeso

![vestido comunion moderno hortensia maeso](https://d33wubrfki0l68.cloudfront.net/a05bc5d1a8932f8ed0644cf5b44d1c7fb2b267c9/21388/tumblr_files/tumblr_inline_p8f6g57iil1qfhdaz_540.jpg)

### Vestido de Primera Comunión de Javilar

![vestido comunion javilar moderno](https://d33wubrfki0l68.cloudfront.net/684fccad090799ed8ad15ac767e58f5d8628b1e0/498ff/tumblr_files/tumblr_inline_p8f61tb7wa1qfhdaz_540.jpg)

### Vestidos de Primera Comunión de Mercedes de Alba

![vestido comunion romantico mercedes alba](https://d33wubrfki0l68.cloudfront.net/4a128413c1448872d2744ad2a41ec43005a491c0/2bb8d/tumblr_files/tumblr_inline_p8f694q9xb1qfhdaz_540.jpg)![vestido comunion manga encaje](https://d33wubrfki0l68.cloudfront.net/fd7467a355b2b620c0de6cc61c351498ba8e7207/1f4ab/tumblr_files/tumblr_inline_p8f66nmrqx1qfhdaz_540.jpg)

### Vestido de Primera Comunión de Querida Philippa

![vestido comunion querida hilippa](https://d33wubrfki0l68.cloudfront.net/00817686d587965fa20622d61f527f0c999b78e5/4fd11/tumblr_files/tumblr_inline_p8f63oet5s1qfhdaz_540.jpg)

### Vestido de Primera Comunión de Marita Rial

![vestido comunion marita rial](https://d33wubrfki0l68.cloudfront.net/015696f8c3e1d768e16fd1b465fd26ced1920197/b0368/tumblr_files/tumblr_inline_p8f6e1d1ai1qfhdaz_540.jpg)![vestido comunion cuello alto marita rial](https://d33wubrfki0l68.cloudfront.net/cbba113687e96d1b8488b939458f20d6cd109cda/52c97/tumblr_files/tumblr_inline_p8f6d2loox1qfhdaz_540.jpg)

Todas las fotos son de Marcos Soria.

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/c%C3%B3mo-celebrar-la-comuni%C3%B3n-sin-arruinarse/).

Aquí tienes consejos para [comprar tu vestido de comunión ideal](https://madrescabreadas.com/2020/12/09/comprar-vestido-comunion-murcia/).