---
date: '2017-02-15T15:39:49+01:00'
image: /tumblr_files/tumblr_inline_olf7e18PHx1qfhdaz_540.png
layout: post
tags:
- familia
- eventos
- moda
- local
title: Nuevos aires de primavera en Trasluz
tumblr_url: https://madrescabreadas.com/post/157274441869/moda-primavera-trasluz
---

Este fin de semana [Trasluz](http://www.trasluz.net/) ha presentado en sus tiendas su nueva colección de primavera/verano, y tuve la suerte de asistir al evento en Murcia con mis hijos pasando una mañana muy divertida, ya que mientras ellos disfrutaron de pinta caras y mesa dulce, yo pude conocer en detalle las nuevas prendas. 

 ![](/tumblr_files/tumblr_inline_olf7e18PHx1qfhdaz_540.png)

Sabéis que [Trasluz](http://www.trasluz.net/) es [una de mis marcas favoritas para teens](/2015/11/04/pasarela-moda-madrid.html), ya que no me gusta que mis hijos se vistan de mayores antes de tiempo y, la verdad es que a ellos tampoco les gusta verse con el mismo estilo de cuando eran más pequeños.

Ese punto intermedio tan difícil, lo logra Trasluz de forma natural en cada colección ya que , aunque las tallas van desde la 0 a la 16, y las prendas son similares para todas las edades, saben diferenciar la ropa en cada franja de edad para que los chicos y chicas se sientan cómodos, lo que permite que los hermanos puedan ir coordinados, pero no iguales si no quieren, logrando para cada edad una personalidad distinta.

 ![](/tumblr_files/tumblr_inline_olf7e1duU61qfhdaz_500.gif)

Incluso, ahora, y como novedad, han cambiado su sistema de tallas junior o tennager, haciendo la XS, S, M y L, que incluso, esta última, puede servir para alguna mamá que le guste ir a juego con sus niños.

 ![](/tumblr_files/tumblr_inline_olf7e2JY981qfhdaz_540.png)

La nueva colección me encantó. Pude verla de cerca, tocar sus tejidos de alta calidad, incluso probar algunas prendas a mis hijos.

 ![](/tumblr_files/tumblr_inline_olf7e3ty5I1qfhdaz_540.png)

Tiene una clara influencia mediterránea, reforzando así el carácter español de sus diseños. El colorido, con mucha luz, nos lleva a paisajes con aire claramente mediterráneos.

 ![](/tumblr_files/tumblr_inline_olf7e4XDpD1qfhdaz_540.png)

Presenta una gama muy extensa de tonalidades y combinaciones: de los mandarinas a los verdes agua,

![](/tumblr_files/tumblr_inline_olf7e4lT5D1qfhdaz_500.gif)

de los rosas y tonos pasteles a los azules,

![](/tumblr_files/tumblr_inline_olf7e7ZHuu1qfhdaz_500.gif)

de los verde kaki a los alteza, tonos marineros…

![](/tumblr_files/tumblr_inline_olf7e8n44b1qfhdaz_500.gif)

Los tejidos se adaptan a las necesidades de la primavera y del verano. Los linos, algodones, voiles y tejidos con mucha caída son los básicos en la colección.

Las prendas de punto presentan nuevas texturas e hilaturas muy adaptadas también a este periodo. Toda la colección va acompañada de bordados y detalles cuidados, fiel reflejo de la marca Trasluz.

 ![](/tumblr_files/tumblr_inline_olf7e9uTJi1qfhdaz_540.png)

Pero los aires nuevos de la primavera también han soplado para la imagen corporativa de la marca Trasluz y sus tiendas que  ha estrenado logotipo, la púa, que siendo innovador respeta al anterior y esencia de la marca.

 ![](/tumblr_files/tumblr_inline_olf7eaMi9h1qfhdaz_540.png)

También cambian los colores corporativos, el packaging, la imagen de la tienda, la comunicación… 

 ![](/tumblr_files/tumblr_inline_olf7eaRCUv1qfhdaz_540.png)

La actualización de la imagen y marca es una vuelta a los orígenes e historia de la firma. La calidad, el cuidado, la atención por el detalle, sus diseños estudiados, la fusión de lo clásico con lo moderno y la apuesta por la innovación tecnológica, proporcionan a sus prendas un sello especial e inconfundible que habla de Trasluz como marca y que claramente se ha respetado y potenciado en esta  nueva colección que acabamos de conocer y que me ha enamorado.

 ![](/tumblr_files/tumblr_inline_olf7ebZ3fh1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_olf7eceqdd1qfhdaz_540.png)

Muchas gracias, Mª José, de [Trasluz](http://www.trasluz.net/)Murcia, por el cariño con el que nos tratásteis, y por lo bien que lo pasamos jujto con Ana, del blog [¿Mi Mundo? Los míos](http://mimundolosmios.es/) y su pequeña rubia que es para comérsela. 

¡Hasta otra!

 ![](/tumblr_files/tumblr_inline_olf7ecGGds1qfhdaz_540.png)