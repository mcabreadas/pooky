---
date: '2016-03-27T19:00:25+02:00'
image: /tumblr_files/tumblr_inline_o4945acBNB1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- recomendaciones
- familia
- juguetes
title: Sé un caballero con Lego Nexo Knights
tumblr_url: https://madrescabreadas.com/post/141785355916/lego-nexo-knights
---

“Hola, valiente… ¿Qué crees tener para poder decir #soycaballeroporque?”

¿Te imaginas retar de esta forma a tu hijo?

 ![](/tumblr_files/tumblr_inline_o4945acBNB1qfhdaz_540.jpg)

¿Qué crees que contestaría?

Te voy a contar la respuesta de Osito, mi hijo mediano cuando le mostré esta caja.

Después de leer el reto atentamente contestó sin pensar demasiado:

> #soycaballeroporque ayudo a los niños que se meten con ellos los abusones

Entonces sentí un gran orgullo de madre y le di un abrazo. 

Verdaderamente los niños tienen un corazón noble, y serían los mejores caballeros, lo demuestran porque nos suelen dar lecciones a los adultos cada día.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Vmu2koqex6Q" frameborder="0" allowfullscreen></iframe>

Tras superar el reto, y siendo digno de ser un verdadero caballero, Osito abrió la caja mágica de [Lego](http://www.lego.com/es-es/nexoknights), y encontró este set de Nexo Knights con un nuevo concepto de juego de construcciones basado en un reino medieval ambientado en un mundo futurista donde cinco jóvenes ‘descargan’ poderes especiales a sus escudos para luchar contra el malvado Jestro de la corte real y un elenco de monstruos, para restaurar la paz. 

 ![](/tumblr_files/tumblr_inline_o494ilVaRx1qfhdaz_540.jpg)

La historia cobra vida a través de los sets de construcción LEGO, una aplicación de juegos digitales que alimenta la historia basada en el propio juego y una serie de televisión a partir de seis años. 

 ![](/tumblr_files/tumblr_inline_o494k2LMcJ1qfhdaz_540.jpg)

Hay 14 sets de construcción incluyendo los cinco caballeros del futuro que completan sus poderes con los escudos escaneables en todos los sets de LEGO Nexo Knights y que activan poderes adicionales en la aplicación: 

[LEGO Nexo Knights - Set El vehículo malvado de Jestro, multicolor (70316)](http://www.amazon.es/gp/product/B013GYABSC/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYABSC&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYABSC)

[LEGO Nexo Knights - Set Espada tronadora de Clay, multicolor (70315)](http://www.amazon.es/gp/product/B013GYADD0/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYADD0&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYADD0)

[LEGO Nexo Knights - Set de juego Fortrex, multicolor (70317)](http://www.amazon.es/gp/product/B013JQDXKA/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013JQDXKA&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013JQDXKA)

[LEGO Nexo Knights - Set Triturador de lava de Moltor, multicolor (70313)](http://www.amazon.es/gp/product/B013GYAC0Y/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYAC0Y&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYAC0Y)

[LEGO NEXO KNIGHTS Robot de combate del rey - juegos de construcción (Multi)](http://www.amazon.es/gp/product/B013GYABXM/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYABXM&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYABXM)

[LEGO Nexo Knights - Set Caballo mecánico de Lance, multicolor (70312)](http://www.amazon.es/gp/product/B013GYA5BA/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYA5BA&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYA5BA)

[LEGO Nexo Knights - Set Carro del caos del Maestro de las bestias, multicolor (70314)](http://www.amazon.es/gp/product/B013GYAC6S/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYAC6S&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYAC6S)

[LEGO Nexo Knights - Set Catapulta del caos, multicolor (70311)](http://www.amazon.es/gp/product/B013GYA5RE/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYA5RE&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYA5RE)

[LEGO Nexo Knights - Set Destructor de combate de Knighton, multicolor (70310)](http://www.amazon.es/gp/product/B013GYA6R8/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013GYA6R8&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013GYA6R8)

[Lego 70324 NEXO KNIGHTS - Biblioteca de Merlok 2.0](http://www.amazon.es/gp/product/B019C4SJ48/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B019C4SJ48&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B019C4SJ48)

 ![](/tumblr_files/tumblr_inline_o493g2QrFZ1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o493gaX7zv1qfhdaz_540.jpg)

La aplicación LEGO Nexo Knights: Merlok 2.0 es el nuevo componente que anima a los niños a centrarse en la búsqueda de más de 170 escudos Nexo en distintos formatos: los digitales, que aparecerán en videos y en la serie de televisión, los impresos en revistas o libros y en los nuevos sets de construcción. 

Mediante la búsqueda de escudos en el ‘mundo real’, los niños podrán activar los nuevos poderes Nexo en el juego digital, que ayudará a los caballeros virtuales a proteger el reino de Knighton. La aplicación ya está disponible vía App Store, Google Play, Samsung Apps y Amazon Store completamente gratuita.

 ![](/tumblr_files/tumblr_inline_o493ft8Dc31qfhdaz_540.jpg)

Lego siempre encuentra la manera de innovar en los juegos para que los niños, no sólo se diviertan, sino que también desarrollen la mejor faceta de sí mismos y aprendan valores, en este caso, la caballerosidad, la excelencia, la fortaleza, la valentía.

Mi hijo se considera a sí mismo un caballero gracias a Lego.

 ![](/tumblr_files/tumblr_inline_o494vjX1S41qfhdaz_540.jpg)