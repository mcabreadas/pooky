---
date: '2014-10-27T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petodayxcA1qfhdaz_540.jpg
layout: post
tags:
- premios
- trucos
- recomendaciones
title: Kaiku te regala un viaje tan lejos como quieras
tumblr_url: https://madrescabreadas.com/post/101074849506/concurso-viaje-kaiku
---

Hoy os traigo un concurso divertido y multicultural de la mano de Kaiku Sin Lactosa, una marca de productos lácteos (leche, yogures, nata para cocinar…) que no contienen lactosa, ideales para personas con intolerancia o con dificultades para digerirla. Yo los usé con Leopardito en la época en que tuvo reflujo y le ayudó bastante.

**¿Cómo nos podemos ir de viaje?**

Yo ya he participado  y es muy divertido; se trata de acumular Kilómetros jugando al Quiz aquí. Cuantas más preguntas aciertes, más Kilómetros consigues, y puedes viajar si resultas ganador a:

 ![image](/tumblr_files/tumblr_inline_petodayxcA1qfhdaz_540.jpg)

Paris: 1.000 ksl (1 pregunta acertada)

Nueva York: 2.000 ksl (2 preguntas acertadas)

Costa Rica: 3.000 ksl (3 preguntas acertadas)

Japón: 4.000 Ksl (4 preguntas acertadas)

Nueva Zelanda: 5.000 ksl (5 preguntas acertadas)

Yo, de momento tengo 4.000 Km, y me iría a Japón, pero antes creo tengo que estudiar un poco más su cultura,  porque he fallado alguna preguntilla… ejem…

Además de animaros a participar en el Quiz, **os quiero hacer un regalo** para que probéis los productos de Kaiku Sin Lactosa, así que si me dejáis un comentario en este post **antes del 9 de Noviembre**  a las 00:00 h diciéndome a qué país os gustaría viajar y por qué, podréis ganar este lote de productos que Kaiku mandará de mi parte a quien diga la respuestas más ingeniosa.

 ![image](/tumblr_files/tumblr_inline_petodbWT2y1qfhdaz_540.jpg)

¿A dónde quieres viajar? ¿París, Nueva York, Costa Rica, Japón o Nueva Zelanda?

 ![image](/tumblr_files/tumblr_inline_petodcKfXE1qfhdaz_540.jpg)