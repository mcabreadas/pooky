---
date: '2014-12-23T18:58:00+01:00'
image: /tumblr_files/tumblr_inline_nh1bw2GzmS1qfhdaz.jpg
layout: post
tags:
- mecabrea
- navidad
- cosasmias
title: La primera Navidad
tumblr_url: https://madrescabreadas.com/post/105976324709/la-primera-navidad
---

![image](/tumblr_files/tumblr_inline_nh1bw2GzmS1qfhdaz.jpg)

Luces de colores, escaparates engalanados, alegres villancicos, sabrosos dulces y manjares, bebidas espirituosas para regar esas entrañables y tradicionales comidas…

Todo invita a la ilusión y a la felicidad, pero también a la nostalgia y añoranza, decepción…

Es tiempo de tener la cara de los domingos durante demasiados días. Pero contigo es fácil porque es tu primera Navidad en que eres consciente de cuanto pasa a tu alrededor.

Con tus dos añitos me descubres en cada luz una ilusión nueva porque cada anochecer esperas con ojitos vivos al pie de la ventana del salón a que se enciendan las luces que adornan la calle, y saltas de alegría gritando: “¡Navidad, Navidad!”. Porque sólo tú eres capaz de hacer que esa magia inunde la casa y nos llene de vida a todos.

Ni siquiera sabes bien qué significa la Navidad, y sin embargo nos la inyectas en vena cada vez que escuchas un villancico, ves algún muñeco de Papá Noel colgado de una puerta, algún Belén o algún árbol con bolas.

![image](/tumblr_files/tumblr_inline_nh1bwpYwIb1qfhdaz.jpg)

Me pregunto qué es lo que pasa por tu cabecita…

Creo que simplemente percibes alegría, luz, color, besos y abrazos espontáneos en las calles, familias reunidas… e intuyes que tiene que ser algo bueno, y ¿sabes qué? pienso dejarme llevar por ti. Me pienso contagiar de tu magia y la pienso expandir con quienes me rodean.

Si estas Fiestas tus días se tornan grises y te invade la añoranza y la tristeza busca un niño y abrázalo. Estarás abrazando la Navidad.

Feliz Navidad!

![image](/tumblr_files/tumblr_inline_nh1bxaxh4l1qfhdaz.jpg)