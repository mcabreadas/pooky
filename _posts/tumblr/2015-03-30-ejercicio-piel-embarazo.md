---
date: '2015-03-30T08:00:12+02:00'
layout: post
tags:
- crianza
- trucos
- recomendaciones
- embarazo
- familia
- salud
- bebes
title: 'Durante el embarazo: ejercicio físico y cuidado de la piel'
tumblr_url: https://madrescabreadas.com/post/115009657978/ejercicio-piel-embarazo
---

Por lo que veo en las redes sociales tenemos un baby boom entre las lectoras del blog. A ellas dedico este post con un video de ejercicios que podéis realizar durante el embarazo, realizado por matronas del Hospital Vall d´Hebrón de Barcelona y la firma Isdin, y unos consejos para cuidar la piel.

## Ejercicio físico

Se trata de una completa tabla que tiene como finalidad reforzar musculatura y ganar elasticidad ejercitando distintas partes del cuerpo como hombros, cuello, cintura o piernas. A partir de la semana 30 de embarazo, esta tabla afianzará vuestra preparación y bienestar y os acompañará hasta el parto.

<iframe width="500" height="315" src="https://www.youtube.com/embed/2kuhKJjvn64?rel=0" frameborder="0"></iframe>

Además de cuidaros con ejercicio físico también es muy recomendable que os cuidéis la piel, ya que la piel experimenta durante el embarazo una serie de cambios que hay que conocer.

## Cuidado de la piel

El cambio más bonito consiste en un rostro más luminoso provocado por el aumento del volumen de sangre en el cuerpo, hasta un 50%,que hace que la piel tenga un aspecto más sonrosado dándole un resplandor especial que seguro que os han comentado más de una vez y que además de esta razón científica estoy convencida de que la felicidad que sentimos por dentro se refleja en nuestra cara.

Pero también se producen otros cambios que debemos tener en cuenta para actuar de la manera más conveniente:

## Controlar las manchas 

Las hormonas del embarazo producen una hiperpigmentación que no suele ser uniforme, de ahí que aparezcan manchas en distintas partes del cuerpo. En el rostro suelen darse en frente, mejillas y labio superior -el denominado cloasma o máscara de las embarazadas-. Son más oscuras en torno a los senos y pezón y puede formarse, en la parte inferior del abdomen, una línea denominada nigra -desde el ombligo hasta el hueso púbico- que aparece en la semana 14. La mayor parte de estas manchas desaparecen tras el parto. Para prevenir las del rostro conviene utilizar a diario un buen fotoprotector de ciudad, hacedme caso, y si os bañáis en la playa ahora que empieza el buen tiempo, fijaos que sea resistente al agua. Yo soy blanca y pecosa, y no tengo ninguna mancha de ninguno de mis embarazos porque tomé esto como un dogma. Os recomiendo el protector de Isdin, que no falla, y que a mí me ha ido siempre fenomenal.

## Prevenir las estrías

Entre el 60 y el 90% de las mujeres desarrollan estrías durante el embarazo debido al estiramiento cutáneo y al aumento de hormonas esteroideas. Existen cremas específicas para prevenirlas, aunue yo creo que influye mucho el tipo de piel de cada mujer y la genética. Yo no tengo ninguna estría en la barriga, sin embargo en el pecho si me salieron… Os puedo recomendar la crema Velastisa Antiestrías, o también, una crema que sea muy hidratante y untuosa aplicada en abundancia sobre la barriguita y los pechos, a excepción del pezón, ya que éste tiene su propia grasa protectora.

## Cuidar cabello y uñas
Cambios hormonales y posibles carencias de componentes bioquímicos: hierro, aminoácidos y vitaminas pueden llevar a una pérdida y afinamiento del cabello hasta cuatro meses después del parto. Utilizar productos específicamente desarrollados con melatonina, ginkgo biloba y biotina crearán un microambiente protector del folículo piloso; estimularán la repoblación de nuevos cabellos y fortalecerán su estructura. Por otro lado, “las uñas se tornan frágiles y quebradizas a partir del tercer mes de embarazo. Los alimentos ricos en biotina: queso, soja, plátanos, huevos…, paliarán las alteraciones”, apunta. A mí se me sigue cayendo el pelo muchísimo a pesar de que hace más de dos años que di a luz, y estoy tomando unas vitaminas específicas para frenar la caída, y de paso, a ver si las uñas se me ponen más fuertes porque se me siguen quebrando mucho.

Así que, amigas, ya sé que tenéis un alubión de cosas en la cabeza en este momento tan especial, pero os aconsejo que os cuidéis porque sólo tenéis un cuerpo, y os tiene que durar toda la vida

¿Tengo razón?