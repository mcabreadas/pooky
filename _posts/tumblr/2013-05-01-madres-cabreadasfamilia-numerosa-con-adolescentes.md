---
date: '2013-05-01T13:18:00+02:00'
image: /tumblr_files/tumblr_n3hp7lEwtQ1qgfaqto2_400.jpg
layout: post
tags:
- revistadeprensa
- colaboraciones
- participoen
- ad
- conciliacionrealya
- conciliacion
title: Permisos parentales transferibles
tumblr_url: https://madrescabreadas.com/post/49352222888/madres-cabreadasfamilia-numerosa-con-adolescentes
---

[Madres Cabreadas.Familia numerosa con adolescentes](/)  

![](/tumblr_files/tumblr_n3hp7lEwtQ1qgfaqto2_400.jpg)

## Permisos parentales transferibles

Mi colaboración junto con [Colo Villén](http://buceandoenmi.blogspot.com.es/p/sobre-mi.html):

“En los últimos años se ha venido hablando  cada vez más de la ** importancia del papel del padre** , como miembro activo en el sostén de la unidad familiar, también dentro de casa.

Esta faceta del  **padre implicado en el cuidado y atención de sus hijos desde los primeros meses de vida**  ha impulsado a gobiernos, administraciones, asociaciones y empresas a replantearse sus políticas y filosofías para poder así, acoger un ** modelo familiar más acorde a los tiempos que vivimos.**  
  
Sin embargo, no nos engañemos,  **queda mucho camino por recorrer**  porque la ampliación del permiso por paternidad cuya entrada en vigor pospone año tras año nuestro gobierno actual, y el “ **mobbing** ” que sufren, por parte de superiores, o incomprensión de compañeros, amigos, incluso familiares, ** aquellos hombres que desean acogerse a una excedencia** o bien adoptar otras medidas legales que les permitan dedicarse al cuidado de sus hijos, son  **trabas para que los hombres puedan ser corresponsables**.  
  
**En CRYA deseamos mirar un poco más allá y asomarnos a la realidad de muchas familias**  a la hora de poner en práctica acciones supuestamente conciliadoras. A menudo nos preguntamos:  **¿Qué hace difícil la corresponsabilidad ?**  
  
Para nosotros se trata, principalmente, de un problema de  **concienciación ** y  **aceptación**. Deseamos partir de la base de que  **los niños precisan de la atención y cuidado de un adulto** , quién pasará a convertirse en su ** figura de referencia y apego**  hasta que desarrolle por completo su autonomía. Lógicamente la relación irá madurando y adaptándose a las distintas etapas que se sucederán. Si estas figuras de referencia y cuidado recaen en uno de sus padres, o tanto mejor en ambos, beneficiará que ** los menores lleven consigo** , calándoles poco a poco y no impuesta ni forzada,  **aquellos valores y actitudes que sus padres o tutores destilen** , no sólo a través de las directrices que marquen en su cuidado, sino principalmente a través de sus propios actos.  
  
Por este motivo  **encontramos importante e inmensamente enriquecedor que el padre tome parte activa en el seno familiar en cuanto al cuidado y atención de sus hijos**. A la vez que favorecerá enormemente la reincorporación de la madre al mercado laboral, si ella lo desea, y ayudará a consolidar una ** mentalidad social más empática, moderna y realista**.  
  
En CRYA creemos y apostamos firmemente por la  **corresponsabilidad**. Somos conscientes de que los  **permisos** actualmente establecidos son  **insuficientes ** y luchamos para que la ampliación de éstos repercuta en la ** mayor flexibilidad**  posible que permita que cada familia adapte su tiempo, prioridades y recursos de la manera que mejor convenga a favor del bienestar familiar.  
  
Creemos en la corresponsabilidad pero consideramos que debe ser entendida como una corresponsabilidad sana, ante todo. **Una corresponsabilidad que nazca y fomente el deseo y derecho a aportar y no de la obligación de hacerlo**. Una corresponsabilidad que se cultive y mime y no que se imponga.  
  
Atender y cuidar a los hijos desde la más tierna infancia, debe ser un sentimiento que nazca en el mismo seno familiar y se valore, facilite y refuerce con medidas que potencien su desarrollo y cabida en el ámbito laboral, político y social. Sólo así se velará realmente por el bienestar de los más pequeños y se garantizará una  **implicación de calidad**  dentro y fuera de casa por parte de padres o tutores.  
  
Esto supone también, despojarnos de determinados tapujos o prejuicios. Supone, ante todo, una cuestión de  **confianza y responsabilidad**.  
  
**¿Cumpliría un permiso de paternidad de igual duración al materno e intransferible su cometido dando como resultado más hombres implicados en la crianza?**  
  
**Lo dudamos**  porque creemos que  **sólo lo tomarían aquellos cuya motivación sea latente** , porque será dicha motivación la que hará palpable y real esa faceta como padre y no la imposibilidad de transferirlo a la madre, porque el disfrutar del permiso no implica necesariamente dedicarlo al cuidado de los hijos, puesto que no hay manera de regular dicho cumplimiento. De ahí la brecha que esta medida inflexible posee y hace que no se sostenga.  
  
Por todo lo expuesto, ** no creemos que la intransferibilidad de los permisos parentales conduzca necesariamente a un mayor bien familiar**. Y más teniendo en cuenta que actualmente, el permiso para los padres es de 15 días, y renuncian a él  más del 30% de los hombres, lo que nos hace pensar que, cuando sea como mínimo de 6 semanas obligatorias, lo van a disfrutar un menor número de padres.  
  
En cuanto a corresponsabilidad y permisos somos rotundos.  **No creemos en la intransferibilidad de dichos permisos, sino en la aprobación de sus ampliaciones y su flexibilidad** , es decir, que tanto el padre como la madre puedan transferir parte de los mismos al otro miembro de la pareja con el fin de que quede cubierta de la mejor manera posible la atención de los menores en función de las circunstancias de cada familia, siempre primando el interés más necesitado de protección, en este caso, el del bebé. ** **

Y quién mejor que sus padres para decidir la mejor forma de conseguirlo repartiendo el tiempo de permiso entre los dos miembros de la pareja libre, consciente y corresponsablemente.

Creemos en reforzar y fomentar medidas que velen por el respeto empresarial ante las decisiones de los empleados en materia de conciliación, independientemente del género. Creemos en gobiernos que sostienen el equilibrio empresarial para que esto sea posible. Y creemos en la  **responsabilidad adquirida por cada individuo**  para comprometerse con su trabajo, la sociedad, cada uno de los miembros de su familia y, sobre todo, consigo mismo.  
  
Creemos que el  **bienestar ** de cada pequeño hogar acabará conformando una sociedad más  **empática, comprometida, responsable, libre y humana.”**