---
date: '2013-12-26T08:00:00+01:00'
image: /tumblr_files/post-unicef-def.jpg
layout: post
tags:
- mecabrea
- colaboraciones
- participoen
- ad
title: Filipinas te necesita esta Navidad
tumblr_url: https://madrescabreadas.com/post/71182587503/filipinas-te-necesita-esta-navidad
---

![](/tumblr_files/post-unicef-def.jpg)

Ya os conté que para mí la Navidad tiene

[dos caras](/2013/12/22/las-dos-caras-de-la-navidad.html)

. He aquí un ejemplo vivo de la cara amarga, de la cara que os decía que, quienes estamos en la buena, tenemos que paliar de algún modo. Os lo voy a poner fácil.

En estas fechas tan señaladas 

**Xanit Hospital Internacional**

ha querido aportar su granito de arena a una buena causa uniéndose a 

**UNICEF**

 para ayudar a Filipinas.

El pasado 8 de noviembre el tifón Haiyán-Yolanda arrasó Filipinas. Se estima que 

**14 millones de personas**

 se han visto 

**afectadas**

 por la tragedia, de las cuales 

**casi 6 millones son niños**

. Niños como los tuyos, como los míos, como los nuestros…

Ante la situación de emergencia que viven muchas familias se han fijado un reto solidario: recaudar un total de 1.000€ que se destinarán a ayuda humanitaria. Desde

** Xanit se comprometen a donar 1€ por cada euro que tú puedas aportar**

, para entre todos, conseguir devolverles la sonrisa.

Para colaborar puedes entrar en esta plataforma

El dinero que entre todos consigamos recoger, será destinado a:

- proporcionar medicamentos básicos, alimentos y refugio para las familias desplazadas
- proporcionar agua potable, higiene y medicamentos antidiarreicos

Qué te parece?