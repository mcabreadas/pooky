---
date: '2014-11-21T14:04:00+01:00'
image: /tumblr_files/tumblr_inline_petodgau5i1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- recomendaciones
- educacion
- familia
- bebes
title: Iniciación a los bebés en la lectura
tumblr_url: https://madrescabreadas.com/post/103198535699/libros-cuentos-bebes
---

![image](/tumblr_files/tumblr_inline_petodgau5i1qfhdaz_540.jpg)

Mi hijo pequeño ya cumplió los dos años, y nos hemos propuesto en casa iniciarlo en la lectura. No es que ya sepa leer, ni mucho menos, pero sí sabe pasar las páginas de un libro e identificar los dibujos que ve en los libros. No os imagináis lo bien que se lo pasa aprendiendo palabras nuevas!

Este mes Boolino nos ha ofrecido dos libros ideales para esa edad.

 ![image](/tumblr_files/tumblr_inline_petodg782C1qfhdaz_540.jpg)

La páginas son de cartón duro, material 100% orgánico, y se deshacen en contacto con líquidos. No se doblan fácilmente, y las puntas son redondeadas, ideales para ser manipulados por los más chiquitines sin ningún peligro.

Los colores de las ilustraciones son suaves, y los dibujos fácilmente identificables para que aprendan a nombrar cada uno que ven. 

Se muestran dos entornos naturales distintos que permiten trabajar juntos el vocabulario que ayudará al bebé a situarse y a relacionar lo aprendido cuando nos encontremos en un espacio natural. Les encanta aprender palabras nuevas! Y los animales vuelven loco a mi peque.

 ![image](/tumblr_files/tumblr_inline_petodgLL4W1qfhdaz_540.jpg)

Os recomiento [Mi Pequeño Bosque](http://www.amazon.es/gp/product/8494230530/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8494230530&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=8494230530)y [Mi Pequeño Jardín](http://www.amazon.es/gp/product/8494230549/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8494230549&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=8494230549)