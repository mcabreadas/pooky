---
date: '2015-10-14T11:23:41+02:00'
image: /tumblr_files/tumblr_inline_o39za7Fkwy1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- bebes
- 
title: Cómo limpiar el pipí del colchón
tumblr_url: https://madrescabreadas.com/post/131146323754/limpiarpipicolchon
---

Aunque parezca mentira, existe una manera de dejar un colchón prácticamente igual que estaba antes de una súper meada de bebé/niño o lo que sea. Incluso el olor desparecerá si sigues estos sencillos pasos.

A nosotros nos ha pasado, y ya dábamos por perdido el colchón, porque fue una inundación de pipí de las gordas (falló el cubre colchones, no pudo contener la marea), y pensamos: “de perdidos, al río, vamos a experimentar”.

Y funcionó. De verdad que el cerco desapareció, y el olor a pipí, también.

<!-- START ADVERTISER: from tradedoubler.com -->
<script type="text/javascript">
var uri = 'https://impfr.tradedoubler.com/imp?type(iframe)g(25181138)a(3217493)' + new String (Math.random()).substring (2, 11);
document.write('<iframe src="'+uri +'" width="300" height="250" frameborder="0" border="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>');
</script>
<!-- END ADVERTISER: from tradedoubler.com -->

## Necesitas
 ![](/tumblr_files/tumblr_inline_o39za7Fkwy1qfhdaz_540.jpg)
- Amoniaco  
- Cepillo no muy duro, ni muy suave  
- Polvos de Talco con el mínimo perfume posible  
- Aspiradora

## Cómo hacerlo

1. Pon en un cubo amoniaco y un poquito de agua.  
2. Moja el cepillo y frota la mancha de pipí insistentemente incidiendo en los bordes.  
3. Aplica los polvos de talco generosamente sobre toda la superficie mojada para que absorban la humedad, y déjalos hasta que los notes secos.  
4. Después retira los polvos de talco con una aspiradora  
5. Deja que se termine de secar al aire el colchón, o con las ventanas abiertas del dormitorio un par de días.   
6. Si lo necesitas para que siga durmiendo el niño, puedes ponerlo en el somier con la parte húmeda hacia abajo por la noche, y al día siguiente volver a darle la vuelta y dejarlo al aire para que termine de secar.  
 ![](/tumblr_files/tumblr_inline_o39za8C2Vm1qfhdaz_540.jpg)

El olor no se irá del todo hasta una semana después más o menos, pero ya verás cómo te que queda genial. No te voy a decir que como nuevo, pero seguramente evites tener que comprar otro por el momento.

¿Te ha pasado alguna vez?  
.

Si te ha gustado compártelo para ayudar a otras familias.