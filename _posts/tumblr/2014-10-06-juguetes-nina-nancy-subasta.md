---
date: '2014-10-06T09:00:30+02:00'
image: /tumblr_files/tumblr_inline_pdpcmyKcZJ1qfhdaz_540.jpg
layout: post
tags:
- crianza
- participoen
- colaboraciones
- ad
- familia
- igualdad
- juguetes
- moda
title: Subasta solidaria de Nancy por el Día Internacional de la Niña
tumblr_url: https://madrescabreadas.com/post/99302071439/juguetes-nina-nancy-subasta
---

Hace unos meses os [anunciaba](/post/88471994209/concurso-elige-tu-diseno-nancy) que con motivo del  **Día Internacional de la niña,**  [Nancy de Famosa](http://www.nancyfamosa.es/es/) iba a poner en marcha una subasta benéfica, donde se subastarían 70 “kits” compuestos por 2 vestidos (uno para niña en talla única y otro para su Nancy) con diseños exclusivos creados por Eva, una niña de 8 años, de la tienda on line Kiddy Mini Model. 

 ![image](/tumblr_files/tumblr_inline_pdpcmyKcZJ1qfhdaz_540.jpg)

**Ha llegado el momento! **

**Del 6 al 10 de Octubre, podrás participar en la subasta on line a través de esta web.**

**Día Internacional de la niña**

Todos los fondos recaudados se destinarán íntegramente a la ONG Plan Internacional, en particular a la campaña “[Por Ser Niña”](http://www.porserninas.org/) con motivo de la celebración del **Día Internacional de la Niña** , que se celebrará el próximo 11 de Octubre, y cuyo objetivo es acabar con la discriminación de género que afecta a millones de niñas en todo el mundo garantizándoles al menos 9 años de educación gratuita y de calidad y rompiendo las principales barreras que les impiden ir a la escuela, como son el matrimonio temprano, el trabajo y la violencia en el entorno escolar.

 ![image](/tumblr_files/tumblr_inline_pdpcmzxBTF1qfhdaz_540.jpg)

**Cómo participo en la subasta solidaria**

Para poder participar en la subasta de los kits de vestido de niña a juego con el de la Nancy, debes registrarte en la web indicada y pujar por los 14 kits que se subastarán cada día (desde el 6 al 10 de octubre de 10:00 a 22:00 horas).

El precio de salida será de 15€ y puedes pujar las veces que quieras si ves que el precio sube. Las 14 pujas más altas de cada día conseguirán el kit exclusivo.

Los 14 ganadores se comunicarán al día siguiente,  antes de las 10:00 horas. Así, si no has recibido la comunicación de que has ganado, puedes volver a intentarlo cada día.

Es una  gran oportunidad para conseguir un pack único para que tu pequeña y su muñeca combinen a la perfección a la vez que estarás colaborando en una causa solidaria.

Pincha en el corazón y puja!

 ![image](/tumblr_files/tumblr_inline_pdpcmzPUFw1qfhdaz_540.jpg)

_NOTA: Este post es patrocinado_