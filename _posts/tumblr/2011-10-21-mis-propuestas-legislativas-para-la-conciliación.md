---
date: '2011-10-21T13:27:00+02:00'
layout: post
tags:
- mecabrea
- colaboraciones
- participoen
- ad
- conciliacion
title: Mis propuestas legislativas para la Conciliación laboral y familiar
tumblr_url: https://madrescabreadas.com/post/11729203276/mis-propuestas-legislativas-para-la-conciliación
---

Os quiero dejar mis propuestas personales en este post.

**1) Ampliación de la duración de las bajas por nacimiento, acogimiento o adopción:**

·          ** Baja materna** l  **de 6 meses como mínimo para asegurar la lactancia exclusiva que recomienda la OMS.**

 Actualmete la baja es de 16 semanas, lo que es insuficiente para proporcionar al bebé lactancia materna exclusiva, lo que evitará, entre otras cosas,asma y alergias, además de los múltiples beneficios para el desarrollo tanto físico como psíquico del recién nacido.

·          **Baja paternal**   **de 1 mes como mínimo, transferible la mitad a la madre.**

Actualmente los papás tienen 15 días de permiso por este motivo. El plantear 1 mes como mínimo es por ser realista, pero si fuesen dos sería mejor, claro. 

Respecto a la transferibilidad, tema polémico, yo defiendo que se pueda ceder la mitad a la madre porque, aunque, a partir de los 6 meses se empiezan a introducir alimentos diferentes a la leche materna, esto lleva su proceso, no es de golpe, y el bebé necesita todavía tener cerca la leche de su madre.

** **

**2) Permisos laborales **

Actualmente sólo son de dos días.

Yo propondría hasta 7 días justificados por cuidado de familiar de primer grado por consanguinidad o afinidad. Es decir, hijo/a, yerno o nuera, padre/madre o suegra/suegro. En este punto no sólo hablamos de menores, sino también de nuestro mayores (creo que es justo)

** 3) Vacaciones:**

**   Prioridad para madres/padres** trabajadores a la hora de elegir las vacaciones, con el objetivo de hacerlas coincidir con las vacaciones escolares,

   En caso de que la empresa tenga como política las vacaciones forzadas (y aunque no sea esta la política de la empresa, porque las vacaciones legales nunca serán tan amplias como las escolares) en determinadas épocas del año, se dará al padre o madre la posibilidad de ausentarse del puesto de trabajo los días que coincidan con las vacaciones escolares, fiestas escolares, revisiones pediátricas y enfermedad leve que impida al niño asistir al colegio.

**      4) Aumentar la duración de las excedencias** con reserva de puesto y cotización a la Seguridad Social, así como la edad del niño a cuidar.

**      5)** Que se considerara legalmente como **causa de discriminación** en las relaciones laborales por razón de proposición o ejercicio de medidas de conciliación

  **6**** ) Preferencia para elegir turnos** de trabajo para trabajadores con hijos menores de 8 años o discapacitados o ascendientes dependientes.

  **7)** Que la conciliación familiar sea un criterio decisivo a la hora de decidir sobre el **traslado** o no de un trabajador.

  **8)** Que se considere como **causa de nulidad del despido** si es por causas de discriminación por razón de peticion o ejercer medidas de conciliación familiar por parte del trabajador.

**9)** Introducir como **causa de despido disciplinario** el **acoso** por razón del ejercicio de medidas de conciliación familiar por parte del empresario o cualquier persona que trabaje en la empresa.

**10)** Tipificar como **falta** susceptible de sanción el fraude llevado a cabo por el trabajador en la petición o ejercicio de medidas de conciliación familiar.

**11)** Introducir como **causa de despido disciplinario el fraude** llevado a cabo por el trabajador en el ejercicio de medidas de conciliación familiar.

**12)** Fomentar la  adopción de amplias medidas de conciliación en los **convenios colectivos.**

**         13) **Incentivar a aquellas empresas que  **hagan nuevos contratos laborales**  a padres/madres que deseen una jornada continuada para el cuidado de hijos o mayores dependientes.

  **14)** Por parte de la administración fomentar la  **contratación de trabajadores con necesidad de medidas de conciliación**  familiar tanto para descendientes como mayores dependientes

**  15)** Beneficios fiscales y bonificación de cuotas en SS para empresas que fomenten la  **jornada continua con flexibilidad horaria, racionalización de horarios, teletrabajo y trabajo por objetivos.**

  **16)** Incentivar o premiar a empresas que tengan ** guarderías en las propias instalaciones**  y/o  **sala de lactancia**  con frigorífico 

  **17)** Fomentar la **formación on-line** a distancia de **trabajadores en baja** por m/paternidad y enviarles por medios telemáticos **información periódica** relacionada con su puesto de trabajo.

**18)** Premiar a empresas que promocionen o asciendan a trabajadores “conciliantes”

**        19) **Incentivar a aquellas empresas que tengan en plantilla a **trabajadores con reducción de jornada**  que aleguen este horario por motivos de conciliación. La ley permite reducción de jornada hasta los 8 años del niño, yo propondría la ampliación hasta los 12 años

Pero muchas de las anteriores medidas dependen exclusivamente de la voluntad de las empresas, por ejemplo:

-Banco de horas

-Preferencia para elegir turnos de trabajo para trabajadores con hijos menores de 8 años o discapacitados o ascendientes dependientes.

-Que la conciliación familiar sea un criterio decisivo a la hora de decidir sobre el traslado o no de un trabajador.

- jornada continua con flexibilidad horaria, racionalización de horarios, teletrabajo y trabajo por objetivos.

- guarderías en las propias instalaciones o bonificaciones en las más cercanas al centro de trabajo.

-Sala de lactancia con frigorífico

-formación on-line a distancia de trabajadores en baja por m/paternidad y enviarles por medios telemáticos información periódica relacionada con su puesto de trabajo.

-no obstaculizar la promoción o ascenso de trabajadores “conciliantes”

Quedaría por concretar medidas específicas para los Autónomos, que abordaré más adelante en otro post.

Ya sé que pido mucho.

¿Qué opináis?