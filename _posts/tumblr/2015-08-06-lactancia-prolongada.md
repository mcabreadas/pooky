---
date: '2015-08-06T17:00:14+02:00'
layout: post
tags:
- crianza
- participoen
- colaboraciones
- ad
- lactancia
- familia
- bebes
title: 28 madres hablan sobre la Lactancia materna prolongada
tumblr_url: https://madrescabreadas.com/post/126015353249/lactancia-prolongada
---

[28 madres hablan sobre la Lactancia materna prolongada](http://maternidadfacil.com/lactancia-materna-prolongada/)  

Justo en la [semana internacional de la lactancia materna](http://worldbreastfeedingweek.org/pdf/wbw2015-cal-spa.pdf) os dejo mi colaboración en este magnífico blog [maternidadfacil.com](http://maternidadfacil.com/lactancia-materna-prolongada/), donde participo junto con otras 27 mamás blogueras en una recopilación de experiencias reales sobre la lactancia materna prolongada llevada a cabo por [María José Madarnás](https://twitter.com/klamajama).

Espero que lo disfrutéis porque no tiene desperdicio. Hay opiniones variadas según las circunstancias de cada mujer.

Es muy interesante, sobre todo si vas a ser madre, o ya lo eres y te planteas la lactancia materna prolongada.

Qué opinas?