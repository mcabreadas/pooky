---
date: '2014-05-20T16:00:26+02:00'
image: /tumblr_files/tumblr_inline_p7pkxjab5q1qfhdaz_540.jpg
layout: post
tags:
- crianza
- participoen
- colaboraciones
- ad
- familia
- parto
- bebes
title: Semana mundial del parto y nacimiento respetados
tumblr_url: https://madrescabreadas.com/post/86308236490/semana-mundial-del-parto-y-nacimiento-respetados
---

Este año, la semana del parto respetado pasa a llamarse “Semana Mundial del Parto y Nacimiento Respetados”, ya que en el título original en inglés está recogido- con la palabra “childbirth” y en España por primera vez se integra en esta semana la celebración del Día de Derechos del Nacimiento, que es el martes, 20 de mayo.

El año pasado por estas fechas os contaba la [experiencia de mis tres partos](/2013/05/23/tres-partos-tres-semana-del-parto-respetado.html), muy diferentes entre sí, al igual que mis hijos también son diferentes unos de otros, como también yo fui una mujer diferente en el momento de cada alumbramiento.

Este año la ENCA (European Network of Childbirth Associations – Red Europea de Asociaciones sobre el Nacimiento) propone para la semana internacional del parto y nacimiento respetados el tema: “ **Birth is Empowering”** , “Parir es poder” en su versión española, y sugiere la siguiente imagen: 

![image](/tumblr_files/tumblr_inline_p7pkxjab5q1qfhdaz_540.jpg)

Nos recomiendan también plantearlo desde la reflexión sobre el origen social del empoderamiento de la mujer:

_“Parir como un acto de “poder frente a” implica una actitud de resistencia ante una fuerza opresora, evoca la necesidad de ser fuertes, invulnerables, frente a un enemigo pues toda heroína que se precie requiere de un buen villano o un sistema corrupto contra el que luchar”_

Yo, sinceramente, y hablo desde la experiencia de mis tres partos, pienso que lo más importante que tiene una mujer en su cabeza en el momento de dar a luz es su hijo. Verme como una heroína que tiene que luchar contra un sistema de patriarcado y que por eso la obligan a parir tumbada, siendo su obligación exigir parir de pie para demostrar su empoderamiento, de verdad, no lo veo. O al menos no lo he sentido así en ninguno de mis tres partos.

![image](/tumblr_files/tumblr_inline_p7pkxj7SI31qfhdaz_540.jpg)

Otra cosa es que sea firme defensora del parto respetado, con la menor instrumentalización posible, en este sentido comparto [la opinión de la Dra. Amalia Arce](http://www.dra-amalia-arce.com/2012/06/parto-respetado-en-medio-hospitalario.html), [@lamamapediatr](https://twitter.com/lamamapediatra)a, siempre en aras de beneficiar lo máximo posible al bebé quien, no lo olvidemos, es el verdadero protagonista del acontecimiento, no para demostrar que tengo poder sobre mi cuerpo y que tengo que doblegar al ginecólogo villano que me quiere poner oxitocina porque su turno acaba en unas horas y quiere dejarme lista antes de irse.

Por supuesto que hay que enfrentarse a quienes nos impongan cosas que nosotras no queremos para nuestro parto, y yo recomiendo hacerse un [plan de parto](http://www.dra-amalia-arce.com/2011/12/plan-de-parto-y-nacimiento.html) y entregarlo al equipo que nos va a asistir. Mi segundo hijo nació de forma totalmente natural y fue la experiencia más intensa de mi vida. Es lo ideal para los dos, pero lo más importante es que es lo mejor para el bebé, independientemente de la satisfacción personal que me produjo y lo orgullosa que lo cuento cada vez que puedo, como os lo estoy contando ahora. Pero no perdamos el norte.

![image](/tumblr_files/tumblr_inline_p7pkxkrySa1qfhdaz_540.jpg)

Os cuento lo que me pasó con mi tercer hijo. Yo iba super mentalizada de mi empoderamiento y al ser el tercero, pues no iba yo chula ni nada… ya me las sabía todas… no quería vía, ni enema, quería monitor portátil para moverme, lo de parir de pie no lo veía, pero si surgía, por qué no, nada de oxitocina, ni epidural, ni romperme la bolsa, ni episiotomía, ni [maniobra de Kristeller](http://www.elpartoesnuestro.es/blog/2011/09/07/maniobra-de-kristeller), ni por supuesto ventosa ni forceps (sí, se siguen usando, con mi primera hija usaron ventosa sin encomendarse a nadie). También quería hacer piel con piel con mi bebé nada más nacer, y el [método canguro](http://www.quenoosseparen.info/documentos/canguro.pdf) para que él solito reptara sobre mi cuerpo y se enganchara al pezón para no separarnos ni un instante, y favorecer la lactancia materna.

Fantaseaba con este parto ideal, y creía que así sería después de tener dos hijos mediante parto vaginal, y con todo lo que sabía yo de partos por lo que había leído de las compañeras de [El parto es nuestro](http://www.elpartoesnuestro.es/blog), y toda la información que circula por la red.

Así llegué yo al hospital, pero las complicaciones empezaron a surgir y a mitad de dilatar el bebé comenzó a sufrir y me informaron de que me tenían que practicar una cesárea urgentemente porque su vida corría peligro.

En ese momento, os puedo asegurar que se me olvidó el empoderamiento, el parto natural, y la lucha contra el patriarcado. Recé todo lo que sabía, y quien se convirtió en superhéroe fue el ginecólogo que me intervino, sacando a mi Leopardito en perfecto estado.

La verdad es que sí que me empoderé un poco cuando vi que todo había ido bien y exigí el piel con piel, ya que había leído que [también se puede hacer cuando hay cesárea,](http://www.quenoosseparen.info/articulos/casos/cesarea.php) pero no me dejaron abrazar a mi niño hasta pasadas unas horas del nacimiento. Momento que que me lo enganché a la teta y ya no me separé de él ni un segundo, logrando una lactancia materna exclusiva durante seis meses. 

La conclusión de todo esto, es que es bueno que la mujeres tengamos claras las opciones a la hora de parir y que estemos muy bien informadas de las consecuencias de cada una de ellas para el bebé y para nosotras. Pero no perdamos el norte, por favor, que lo más importante aquí es el bebé y lo que es bueno para él, no tanto el sentirnos realizadas nosotras como mujeres libres y superheroínas que luchamos contra un villano que nos quiere someter.

Por eso, me gusta mucho más la imagen que nos sugerían el año pasado:

![image](/tumblr_files/tumblr_inline_p7pkxkum0h1qfhdaz_540.jpg)

¿Y a ti?

Fuentes:

Educer

quenoosseparen

elpartoesnuestro