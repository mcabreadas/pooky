---
date: '2016-03-15T12:53:40+01:00'
image: /tumblr_files/tumblr_inline_o42vbwxeYU1qfhdaz_540.jpg
layout: post
tags:
- libros
- planninos
- familia
title: Llegan los libros de la Patrulla Canina
tumblr_url: https://madrescabreadas.com/post/141084933204/libro-patrulla-canina
---

No te voy a descubrir ahora que la [Patrulla Canina](http://www.nickelodeon.es/programas/la-patrulla-canina/l3uen8) son los dibujos de moda de la primera infancia, sobretodo si tienes hijos de entre 3 y 6 años lo sabrás de sobra, o incluso más, porque mis mayores se enganchan igual que el pequeño a los capítulos de estos simpáticos perritos.

Pero lo que sí te quiero descubrir son estos libros de la editorial Beascoa, que he encontrado en la web megustaleer.com porque a mi pequeño le han encantado.

Son una forma genial de iniciarlos en la lectura.

Aunque no sepan leer todavía, el hecho de que tengan un libro entre sus manos y se habitúen a pasar páginas, ver las ilustraciones, incluso jugar a que leen señalando las letras con sus deditos es fenomenal para iniciarlos.

Y si tienen hermanos mayores que les lean las historias, mucho mejor, porque no hay nada que me inspire más ternura que cuando veo a mis hijos mayores leyendo para el pequeño.

En casa hemos leído [Salvemos el tren](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2800643), donde Rubble es el protagonista, 

 ![](/tumblr_files/tumblr_inline_o42vbwxeYU1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o42vc6VWAa1qfhdaz_540.jpg)

y [El mejor cachorro bombero](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2800644), donde el personaje principal es el favorito de mi peque, el cachorro Marshal.

 ![](/tumblr_files/tumblr_inline_o42vd08OFS1qfhdaz_540.jpg)
Las tapas son duras, y la edición está muy chula. Mira cómo les llaman la atención las ilustraciones.  

 ![](/tumblr_files/tumblr_inline_o42wu5cVLi1qfhdaz_540.jpg)

Me gusta que mis hijos sean fans da la Patrulla Canina porque cada cachorro es diferente, y todos son importantes, cada uno con su punto fuerte. Se respetan y ayudan unos a otros colaborando en cada misión fomentando  a los niños el trabajo en equipo, y le dan mucha importancia al reciclaje y respeto al medio ambiente. Ya sabes:

¡Antes de tirarlo, reciclarlo!

¿En casa sois fans de la Patrulla canina?

_Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz :)_