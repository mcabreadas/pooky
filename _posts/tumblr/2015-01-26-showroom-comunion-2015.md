---
layout: post
title: "Organizar la Comunión en un día: Showroom 2015 de Compritas Comunión"
date: 2015-01-26T08:00:28+01:00
image: /tumblr_files/tumblr_inline_nin66sfScQ1qfhdaz.jpg
author: .
tags:
  - 6-a-12
  - comunion
  - moda
tumblr_url: https://madrescabreadas.com/post/109185313045/showroom-comunion-2015
---
Sólo una cosa ronda mi cabeza durante estos días. Son tantas las cosas que hay que preparar cuando una hija hace la Comunión, que se había convertido en una auténtica locura de idas y venidas, llamadas telefónicas, pruebas… Os confieso que me estaba resultando muy estresante porque, aunque ya os comenté que no pretendo un evento ostentoso, sí quería que fuera un día muy especial para mi Princesita y para toda la familia, organizado con todo el cariño del mundo.

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).

Por eso, cuando Elena, del blog [La Guinda de Limón](http://laguindadelimon.blogspot.com.es/), que ya ha colaborado en esta casa en [alguna otra ocasión](/2014/11/28/beauty-day-mujer-hoy.html), me informó del Showroom de Comunión que organizaba Compritas para los Peques, vi los cielos abiertos.

¿Os imagináis organizar una Comunión en un solo día y sin salir de una sala?

Por eso le pedí el favor a Elena de que, cámara en mano, visitase “Compritas Comuniones” por mí, y me echara una mano con los preparativos de la Comunión de la peque.

Esto fue lo que allí encontró:

*“El viernes pasado estuve en el Showroom de  **Compritas Comunión** , un evento que organiza Maria Pacheco, de [Compritas para los Peques](http://compritasparalospeques.com/), un blog de referencia de moda infantil en España.*

*Cuando María, del blog Madres Cabreadas, me propuso ser sus ojos en el Showroom, me pareció una idea genial. Ya tenía ganas de ir yo a un evento organizado por  **Compritas para los Peques**  porque me quedé con las ganas de asistir al mercadillo que organizó el pasado octubre.*

 *![image](/tumblr_files/tumblr_inline_nin66sfScQ1qfhdaz.jpg)*

 *![image](/tumblr_files/tumblr_inline_nin676bOh31qfhdaz.jpg)*

\_ \_

*Lo que mas me gustó de  **Compritas Comunión**  es que en una sola visita se puede organizar una Comunión. Desde el vestido, complementos, los recordatorios, fotógrafo, regalos para los invitados, hasta todo lo necesario para animar la fiesta.*

*En realidad fue como una mini feria dedicada al 100% a la Primera Comunión, sólo faltaban los restaurantes y las Iglesias.*

*En la zona dedicada a los trajes y complementos podías encontrar vestidos de todo tipo, desde los más clásicos, a los románticos, pasando por los más modernos y los diseñados a gusto del pequeño protagonista.*

*Estos son algunos ejemplos…*

[*![image](/tumblr_files/tumblr_inline_nin6u9FUCq1qfhdaz.jpg)*](http://www.palomaensenat.com/)

[*![image](/tumblr_files/tumblr_inline_nin6uuuJkP1qfhdaz.jpg)*](http://www.donpisoton.com/)

[*![image](/tumblr_files/tumblr_inline_nin6v5AjWn1qfhdaz.jpg)*](http://unvestidoparati.com/)

[*![image](/tumblr_files/tumblr_inline_nin6vs84dv1qfhdaz.jpg)*](http://www.patuquitosonline.es/shop/index.php)

[*![image](/tumblr_files/tumblr_inline_nin6w5AvG31qfhdaz.jpg)*](http://www.eltallerdelaabuela.es/)

[*![image](/tumblr_files/tumblr_inline_nin6wg1zBi1qfhdaz.jpg)*](http://nonospijamas.com/)

*Algo que me llamó mucho la atención y que me enterneció sobremanera fue ver a las niñas vestidas de Comunión como auténticas princesas, con sus caras iluminadas, y los ojos brillantes de sus madres viendo a sus pequeñas, seguro, embriagadas de un sentimiento de orgullo, satisfacción, nostalgia y alegría, …, porque eso fue lo que yo sentí sin conocerlas, pero me imaginaba el momento de ver a mi Guinda de Comunión y … Ay! Qué emoción! Además de ese sentimiento del recuerdo de aquel día en que la hice yo.*

*Además de los vestidos, los zapatos y demás complementos, en  **Compritas Comuniones**  podías encontrar todo lo necesario para que ese día fuera perfecto.*

*Recordatorios, libros personalizados, algo que me encantó, marca páginas, sus primeros rosarios, galletas decoradas…*

[*![image](/tumblr_files/tumblr_inline_nin789jqjd1qfhdaz.jpg)*](http://www.mifabula.com/)

[*![image](/tumblr_files/tumblr_inline_nin78nS7E51qfhdaz.jpg)*](http://kedulce.es/)

[*![image](/tumblr_files/tumblr_inline_nin799sBkS1qfhdaz.jpg)*](http://www.herreronunez.es/)

[*![image](/tumblr_files/tumblr_inline_nin7a6yJ6v1qfhdaz.jpg)*](http://elpinceldelossuenos.blogspot.com.es/)

*En uno de los stands estaba Alejandra del blog **[Mama Multitarea](http://mamamultitarea.blogspot.com.es/)**, una súper mamá que conocí en uno de los eventos al que asistí hace poco (estas son las cosas buenas de este mundo, conocer a gente tan interesante como ella) y que además de su vida profesional, vida familiar, se dedica a la bisutería y diseño de regalos de comunión y bautizo, ¿de donde sacara el tiempo?* 

*Os dejo algunos ejemplos de las maravillas que hace, aunque puedes encontrarlos todos en su blog, ([pincha aquí](http://mamamultitarea.blogspot.com.es/)).*

[*![image](/tumblr_files/tumblr_inline_nin7dux8e91qfhdaz.jpg)*](http://mamamultitarea.blogspot.com.es/)

*Tengo que confesaros que a pesar de que nos queda mucho para ese día, hubiera comprado más de un vestido y más de un recordatorio.*

*Para más info sobre expositores, no dudéis de consultar la [web](http://compritasparalospeques.com/) y [Facebook](https://www.facebook.com/compritasparalospeques?fref=ts) de Compritas para los Peques, yo no pude hacer muchas más fotos, había mucha gente! E iba acompañada de mi Guinda que no paraba de decirme que quería vestirse también de princesa, salimos cargadas de tarjetas, con un globo y con otra buena experiencia vivida!* 

 *![image](/tumblr_files/tumblr_inline_niokphMqtA1qfhdaz.jpg)*

 *![image](/tumblr_files/tumblr_inline_nin7iiUDy61qfhdaz.jpg)*

*Felicidades a Maria, de Compritas  por el éxito de esta nueva edición, el año que viene más!”*

Ya tengo material más que de sobra para elegir y organizar cada detalle, y espero que si estáis en mi misma situación, esta selección os oriente y os ayude con los preparativos para lo que va a ser uno de los días más bonitos en la vida de vuestros hijos. Pinchando cada foto, os llevará a la web de la tienda donde podréis encontrar lo que os muestro y ver más cositas. Desde aquí agradezco infinito a Elena, de La Guinda de Limón, el tremendo esfuerzo que hizo asistiendo al evento, incluso con su hija, quien se portó de maravilla, para echarme una mano. Gracias también a María, de Compritas para los Peques, por su disposición y ayuda, y a Alejandra, de Mamá Multitarea, por su gran trabajo y buenos consejos. ¿Y tú, estás liada con los preparativos de la Comunión?

NOTA: Si estás, como yo, organizando la Comunión, apúntate al grupo de Facebook de mamás y papás que estamos en esa misma situación, donde nos ayudaremos y compartiremos ideas y experiencias.

[Solicita aquí entrar en el grupo “Organizando la Primera Comunión”](https://www.facebook.com/groups/1377576085877092/)

 ![image](/tumblr_files/cleardot.gif)