---
date: '2015-10-22T17:00:26+02:00'
image: /tumblr_files/tumblr_inline_o39z8njCLX1qfhdaz_540.jpg
layout: post
tags:
- crianza
- participoen
- planninos
- colaboraciones
- ad
- eventos
- educacion
- familia
- moda
title: Semilla en Petite fashion week de Charhadas
tumblr_url: https://madrescabreadas.com/post/131686852322/petitecharhadassemilla
---

Durante los días 16, 17 y 18 de octubre ha tenido lugar en Madrid uno de los Eventos para niños más importantes del año “The Petite Fashion Week”, organizado por [Charhadas](http://charhadas.com/) en la impresionante Galería de Cristal del Ayuntamiento de Madrid.

 ![](/tumblr_files/tumblr_inline_o39z8njCLX1qfhdaz_540.jpg)
## Infinidad de actividades para los peques

**Pasarelas de moda infantil** de firmas como [Chicco](http://www.chicco.es/), [Gocco](http://www.gocco.es/), [Neck & Neck](http://www.neckandneck.com/), [Kiabi](http://www.kiabi.es/), [ Eli](http://www.boutique-eli.com/es/index), [Nieves Alvarez](http://nievesalvarez.com/es/), [Casilda y Jimena](http://casildayjimena.es/)…

 ![](/tumblr_files/tumblr_inline_o39z8njjwA1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39z8oO4dd1qfhdaz_540.jpg)

**cuenta cuentos** de [Nivea](http://www.nivea.es/),

 ![](/tumblr_files/tumblr_inline_o39z8orxjs1qfhdaz_540.jpg)

También pudimos disfrutar de los últimos finalistas de **Master Chef junior** gracias a [El Corte Inglés](https://www.elcorteingles.es/). 

 ![](/tumblr_files/tumblr_inline_o39z8ptqQx1qfhdaz_540.jpg)

Nuestra colaboradora Rosa, de [Pipifuchas](https://www.facebook.com/pipifuchas), ha asistido con sus peques y, además de pasarlo en grande con la actuación dedicada al público infantil del **Carlos Jean** , ha descubierto [Semilla](http://semillaespaciocreativo.com/), un espacio diferente, que nos ha fascinado. Nos lo cuenta ella misma:

 ![](/tumblr_files/tumblr_inline_o39z8pFmhe1qfhdaz_540.jpg)

## Qué es Semilla

> He tenido la oportunidad de conocer de primera mano, a través de la directora del Proyecto, Elisa Toloso, a ” **Semilla Espacio creativo infantil** ”, una nueva forma de aprendizaje y de terapia para los más pequeños.

 ![](/tumblr_files/tumblr_inline_o39z8qcEaJ1qfhdaz_540.jpg)

> Semilla, junto a MAZETO, su mascota, surgió de la experiencia de Elisa y de sus ganas de dar una alternativa al tratamiento de los “peculiaridades infantiles”. 

 ![](/tumblr_files/tumblr_inline_o39z8qL4of1qfhdaz_540.jpg)

> Durante tiempo había estado desarrollando su labor dentro del terreno educativo, en el que durante los últimos años se ha facilitado la integración de los “niños diferentes”, definimos niños diferentes como aquellos que no cumplen los paradigmas estipulados, niños hiperactivos, más creativos, déficit de atención, autistas…

## Por qué Semilla es un espacio diferente

> Sin embargo, Elisa quería algo más; crear un espacio donde los niños pudieran superar sus dificultades sin encontrarse en un ambiente poco cómodo para ellos.

 ![](/tumblr_files/tumblr_inline_o39z8rTfds1qfhdaz_540.jpg)

> De ese sueño, y de esas ganas de mejorar las terapias infantiles nace Semilla, un espacio que los niños ven como diversión y sin embargo lo que se está tratando son desordenes que deben corregirse, todo ello a través del juego. En Semilla cada niño es DIFERENTE y a la VEZ  IGUAL a los demás. 

## Terapia, arte y diversión

> A través del conocimiento del pequeño a través de unas sesiones con los especialistas y de las preguntas que se realizan a los padres se le crea un tipo de juego y de desarrollo diferente, no todos tienen las mismas capacidades musicales, ni pictóricas…… el tratar a todos por igual puede crear en el niño inseguridades y rechazo a sus propias habilidades, debido a esto, que en SEMILLA identifican individualmente el niño desarrolla sus habilidades en lo que a él le motiva, le gusta y le aporta. 
> 
> De esta forma mediante un juego vence la timidez, la hiperactividad y cualquier otra situación.

 ![](/tumblr_files/tumblr_inline_o39z8rKqrS1qfhdaz_540.jpg)
## Como estar en casa

> Los niños entran en SEMILLA y se descalzan, hablan con sus compañeros (no grupos superiores a 8 niños), se relajan y comienzan con su terapia.
> 
> Algo que me ha encantado como madre es la mezcla de edad, no los separan por edades, me parece una idea genial. Tenemos la idea de que los mayores pueden hacer daño a los pequeños, que los mayores se aburren con los pequeños; nada más lejos de la realidad, cada edad aporta algo a la otra y de esta manera todos se relacionan y aprenden. 
> 
> Hay una diversidad de edades en el grupo que sin duda les ayudará a su integración en su vida futura.

 ![](/tumblr_files/tumblr_inline_o39z8sTeak1qfhdaz_540.jpg)
## Valores

> Desde SEMILLA buscan que la Semilla que es cada niño germine de la mejor forma posible, dándola a cada una los nutrientes necesarios (no los mismos para todas) y que en un futuro sea el árbol, la flor o el arbusto que quiera ser, pero desde la plenitud como Persona.

Quiero agradecer a Rosa, nuestra cronista, por habernos descubierto este espacio tan especial, y a Elisa, de Semillas, por atendernos tan amablemente, y por poner en marcha junto a Carlos este proyecto tan comprometido con el mundo infantil.

Gracias también a las chicas de Charhadas por la magnífica organización y el magnífico evento. Les deseo que ahora descansen y tomen fuerzas, porque ya estamos deseando que llegue el del año que viene!

¿Nos vemos allí?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.