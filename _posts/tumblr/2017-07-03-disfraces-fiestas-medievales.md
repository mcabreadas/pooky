---
date: '2017-07-03T11:45:20+02:00'
image: /tumblr_files/tumblr_inline_osiaheQT751qfhdaz_540.png
layout: post
tags:
- disfraces
- trucos
- planninos
- familia
title: Las mejores fiestas medievales de España
tumblr_url: https://madrescabreadas.com/post/162545061129/disfraces-fiestas-medievales
---

El verano no es sólo para ir a la playa o a la piscina. Si buscamos un poco encontraremos otro tipo de plan familiar muy interesante y que a los niños les va a encantar porque a qué niño no le gusta disfrazarse de Rey Arturo, arquero, caballero o cualquier otro [disfraz medieval](https://www.disfracessimon.com/disfraz-medieval-complementos-94.html)?

Te sorprendería la cantidad de pueblos que celebran precisamente en verano sus fiestas, y muchas de ellas están inspiradas en el medievo, lo que sumerge a los pequeños en un mundo mágico a la vez que les permite aprender parte de nuestra historia de un modo lúdico.

Otras veces se organizan de forma privada fiestas temáticas donde los asistentes incluso pueden ir ataviados con un [disfraz de vikingo](https://www.disfracessimon.com/disfraces-vikingos-adulto-hombre-mujer-71.html), un [disfraz de romana](https://www.disfracessimon.com/disfraces-romanos-griegos-adulto-60.html) o cualquier atuendo que vaya con la temática, donde se ofrece una cena a la vez que se puede disfrutar de un espectáculo. El Desafío Medieval en Alfàs del pi es un ejemplo de lo que te digo.

Me encanta ver a familias enteras disfrazadas de época. Yo, desde luego si asistiera a una de estas fiestas no me faltaría mi disfraz porque pocas cosas me parecen mas divertidas (además, es que yo me meto en el papel totalmente).

Las fiestas, ferias y mercadillos medievales se extienden a lo largo de toda nuestra geografía, pero en cada punto tienen su singularidad, y todas son ideales para ir con niños. Aquí algunas de las que me han hablado muy bien, que se celebran **este verano** , y que os puedo recomendar:

**Aragón**

- **Calatayud** , Zaragoza: Las Alfonsadas de Calatayud en junio recrean la conquista de la ciudad por Alfonso I.    
- **Albarracín** , Teruel: Mercado Medieval de Albarracín en julio. Albarracín es un pueblo único que conocí bien de chica, y al que tengo muchísimo cariño. Es como retroceder en el tiempo.   

**Castilla La Mancha**

- **Hita** , Guadalajara: El Festival Medieval de Hita en julio es un festival de teatro y una fiesta pionera ligada a una época de nuestra historia y a un personaje universal como es Juan Ruiz, Arcipreste de Hita y a su obra “El Libro de Buen Amor”.

 ![fiesta medieval hita](/tumblr_files/tumblr_inline_osiaheQT751qfhdaz_540.png)
- **Sigüenza** , Guadalajara: Las Jornadas Medievales de Sigüenza en julio conmemoran el destierro de Doña Blanca de Borbón que, prisionera de su esposo, el rey Pedro I de Castilla, pasó largos años recluída en el Castillo de Sigüenza, actualmente Parador de Turismo.  
- **Cañete** , Cuenca: Feria de la Tapa Medieval de Cañete a finales de julio principios de agosto   
 ![feria medieval canete](/tumblr_files/tumblr_inline_osiairOiaB1qfhdaz_540.png)

**Castilla León**

- **Ponferrad** a, León: Noche Templaria de Ponferrada en julio.  
- **Covarrubias** , Burgos: La Fiesta Medieval y de la Cereza de Covarrubias en julio  saca sus pendones, y sus gentes visten sus trajes del medievo… Este día se celebra la fiesta medieval y de la cereza.    
- **Sanabria** , Zamora: El Mercado Medieval de Puebla de Sanabria en el puente de agosto es precioso, el entorno y el ambiente son únicos, ya que cuenta con numerosos puestos repartidos a lo largo del Conjunto Histórico de La Villa y está considerado como uno de los tres mejores de España debido al marco en el que se ubica ya que transporta a los miles de visitantes que recibe a la Edad Media.    
- **Sepúlveda** , Segovia: Fiestas de los fueros de Sepúlveda en julio   
- **Ávila** : Las Jornadas medievales de Ávila en septiembre son ideales para ir en familia , con amigos… hay un ambientazo y con las murallas de fondo la ambientacion esta súper lograda.     

**Galicia**

- **Ribadavia** , Orense: La Festa da istoria de Ribadavia, en Agosto nos lleva atrás en el tiempo hasta la judería medieval, recuperando las tradiciones antiguas e históricas que han formado parte de la historia de la región. El entorno es espectacular y está pensado para todas las edades, es sin duda una de las mejores fiestas de Galicia.  

 ![fiesta medieval ribadavia](/tumblr_files/tumblr_inline_osiaj9Izno1qfhdaz_540.png)

** Andalucía**

- **Huelva** : En las Jornadas medievales de Cortegana en agosto este pueblo se convierte en un auténtica Villa Medieval por cuatro días, en los que tienen lugar espectáculos de teatro, shows magia, música, danza y exposiciones. Igualmente, hay tabernas del medioevo y mucho más…

**Cataluña**

- **Tortosa** , Tarragona: Fiesta del Renacimiento de Tortosa en julio. La  Festa del Renaixemen celebra el esplendor de una ciudad en el siglo XVI. Esta festividad tiene como objetivo rememorar uno de los momentos históricos más importantes de la localidad a través de actividades culturales.  

- **Girona** :  Mercado medieval de Castell d’Aro en agosto  

- **Besalú** , Girona: Besalú Medieval en septiembre  vuelve mil años atrás para transformar el centro histórico en un plató único, donde la fantasía y la historia transportan al visitante al antiguo Condado de Besalú. El valor del conjunto arquitectónico e histórico de la Villa la convierte en una de las muestras más importantes y singulares de los conjuntos medievales de Cataluña y durante dos días, las calles, la gente, las tiendas, los edificios, el paseo fluvial, las murallas, plazas y rincones, no se escapan de la influencia de la Feria Medieval.  

**Navarra**

- **Olite** , Navarra: Las  Fiestas Medievales de Olite en agosto nos trasladan en el tiempo a la época de mayor grandeza del Reino de Navarra.

**Fuera de la temporada de verano**

Te dejo otros eventos que se celebran **fuera de la temporada de verano** , pero también muy recomendables:

**Comunidad Valenciana**

- **Benidorm** : [B](https://lifeinbenidorm.com/2016/04/28/benidormmedieval/)enidorm Medieval suele tener lugar durante el mes de mayo y su espectáculo con verdaderos especialistas es de los más llamativos.

**Andalucía**

- **Santa Fe** , Granada: Las Capitulaciones de Santa Fe celebran la fiesta municipal de este municipio de Granada en las que podremos disfrutar de muchas más actividades además de las de temática medieval propiamente dichas. Se celebran en marzo y abril.

**Cataluña**

- **Vic** : El mercado mediaval de Vic suele ser en diciembre, y es uno de los más auténticos. Los niños podrán descubrir oficios antiguos y disrutar de exhibición de aves rapaces  
- **Hostalric** : Feria medieval de Hostalric en Semana Santa  
- **Montblanc** , Tarragona: Semana Medieval de Montblanc en abril   
- **Barcelona** : Feria del Bosque Medieval de Canyamars en octubre   

**Madrid**

- **El Álamo** , Madrid: En la Feria Medieval de El Álamo incluso te puedes casar en una boda del medievo. Suele ser en marzo y abril  
- **Alcalá de Henares** , Madrid: En la Semana Cervantina en octubre el centro de la ciudad retrocede unos cuantos años, hay mil puestos con todo tipo de artesanos y las atracciones infantiles están genial.   

**Aragón**

- **Teruel** : Boda Amantes de Teruel en febrero

**País Vasco**

- **Balmaseda** , Vizcaya: Feria medieval Balmaseda en mayo

**Castilla León**

- **León** : Mercado Medieval las Tres Culturas de León en octubre  
- **Valladolid** : Mercado Medieval de Quintanilla del Onésimo en octubre y Mercado Medieval de Peñafiel en Mayo   

**Andalucía**

- **Palos de la Frontera** , Huelva: Feria Medieval del Descubrimiento en marzo

**Murcia**

- **Caravaca de la Cruz** , Murcia: Mercado Medieval de Caravaca de la Cruz en diciembre

**Castilla la Mancha**

- **Oropesa** , Toledo: Jornadas Medievales de Oropesa en abril

**Aragón**

- **Zaragoza:** El nacimiento de un Rey en marzo   
- **Teruel** :[](http://www.bodasdeisabel.com/W3/bodas/index_bodas.aspx)Bodas de Isabel en febrero recrea la historia de los amantes de Teruel, y todo el mundo se engalana y viste de medieval con representaciones y actos por toda la ciudad.Todo Teruel se vuelca en la recreación. Hay grupos, jaimas, para poder formar una hay que presentar un proyecto de quiénes son…hay aguadores, parteras, herreros, caballeros…son grupos de amigos que viven la fiesta en la calle, vestidos acorde a lo que representan…monjes…prostitutas, leprosos, muyaidines…  
 ![fiesta medieval](/tumblr_files/tumblr_inline_osiaqj8ItP1qfhdaz_540.png)

Foto gracias a Eva José Muñoz Lopera

Como ves, la oferta es inmensa, y se extiende por todos los puntos de España, así que seguro que algún evento te quedará cerca de casa. Anímate, hazte con unos disfraces medievales para todos y a disfrutar!

Estas recomendaciones están hechas con todo el cariño a través de mi comunidad de Facebook, que se ha volcado contándome sus experiencias en familia en los distintos enclaves que menciono, a la que doy las gracias de corazón… Ahora me han entrado unas ganas enorme de visitar todos los sitios!

Si te ha servido esta información, puedes compartirla para que ayude a más gente!