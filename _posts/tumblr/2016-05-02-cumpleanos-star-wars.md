---
date: '2016-05-02T11:04:37+02:00'
image: /tumblr_files/tumblr_inline_o6hkyt34qG1qfhdaz_540.jpg
layout: post
tags:
- decoracion
- trucos
- planninos
- familia
title: Fiesta de cumpleaños infantil de Star Wars
tumblr_url: https://madrescabreadas.com/post/143731104574/cumpleanos-star-wars
---

Este año decidimos hacerle un regalo original de cumpleaños a mi hijo mediano, y nos animamos a montarle una fiesta casera temática de Star Wars.

Fue un poco locura, porque invitamos a todos sus amigos, y nos juntamos en casa con casi veinte niños, pero mereció la pena porque pasó una tarde inolvidable. Así que si tienes en casa un pequeño fan de Star Wars, sigue estar ecomendaciones, y verás qué éxito:

## Invitaciones

Puedes descargarte la [tipografía de Star Wars aquí](http://www.fontspace.com/boba-fonts/star-jedi), y diseñar tus propias invitaciones para entregarlas con unos días de antelación a los amiguitos.No olvides indicar que vengan disfrazados o traigan sus espadas laser, si has pensado que la fiesta sea de disfraces.

## Decoración

Saca todos los juguetes y [LEGO de Star Wars](/2015/10/18/xwing-lego-starwars.html) que tengas por casa. Si a tu peque le gusta, seguro que tiene mil cosas, sólo tienes que ser minuciosa y encontrarlas. Yo dediqué un buen rato a recopilar, y encontré un montón de figuritas.

 ![](/tumblr_files/tumblr_inline_o6hkyt34qG1qfhdaz_540.jpg)

Usa tu imaginación y coloca todo para ambientar el espacio lo más galácticamente posible.

## Photocall

Lo hicimos con dos paneles gigantes que pedimos en una gran superficie cuando vimos que estaban desmontando un stand (fue una gran suerte).

 ![](/tumblr_files/tumblr_inline_o6hohnhNCm1qfhdaz_540.jpg)
## Regalitos para los invitados

Encontramos estos cubos de chuches en Mercadona que nos vinieron genial. Había de varios personajes de la saga, pero el que más triunfó sin duda fue Darth Vader.

 ![](/tumblr_files/tumblr_inline_o6hoi8FMqJ1qfhdaz_540.jpg)

Por si no los encuentras ya que, aunque siempre hay de estos cubos, van cambiando de tema, y puede que justo cuando los vayas a necesitar no tengan de Star Wars, la marca Chupa Chups tiene una edición especial muy molona que incluye una figurita de regalo en su interior.

 ![](/tumblr_files/tumblr_inline_o6hoiyMmFG1qfhdaz_540.jpg)
## Tarta

La tarta que hice fue tan sencilla como rica. El truco está en usar dos colores: el blanco y el negro, y una forma poligonal para darle un aspecto espacial.

 ![](/tumblr_files/tumblr_inline_o6hojdbQqA1qfhdaz_540.jpg)

**Necesitas:**

- Un bizcocho casero de yogur  
- Conguitos o bolitas de chocolate blancos y negro  
- Kit Kat de chocolate blanco  
- Crema de cacao  
- Una cinta negra  

**Cómo se hace:**

- Haz un [bizcocho casero](http://www.bizcochocasero.net/Bizcochos/BizcochoYogur/BizcochoYogur.html) de los que sueles hacer  normalmente como más te guste.  
- Cuando se enfríe, cúbrelo con una capa fina de crema de cacao.  
- Pégale las bolitas de chocolate jugando con los colore blanco y negro. Puedes hacer un círculo  en el centro, o una estrella… Eso ya, depende de tu imaginación.  
- Coloca alrededor los Kit Kat sin separar las barritas, rodéalas con una cinta o cuerda negra, y haz un lazo.  
 ![](/tumblr_files/tumblr_inline_o6hojyntjm1qfhdaz_540.jpg)

Si no tienes tiempo de hacerlo, también puedes probar suerte, porque a veces hay tartas temáticas en Mercadona a buen precio. Yo encontré ésta, y estuve dudando si cogerla o no, pero al final opté por hacerla casera. Pero si vas mal de tiempo, y la encuentras, es una buena opción.

 ![](/tumblr_files/tumblr_inline_o6hoa4iLHy1qfhdaz_540.jpg)
## Mesa

Con cosas sencillas y jugando con los colores se puede lograr un resultado chulísimo. No hace falta comprar mucho merchandising de Star Wars.

El mantel es clave. Yo encontré uno negro de usar y tirar en una tienda china, pero también puede ser azul marino, con estrellitas….

Usa platos blancos de usar y tirar y coloca encima servilletas de cocktail negras.

También queda muy chulo las galletas Oreo, que son negras, sobre los platos blancos.

 ![](/tumblr_files/tumblr_inline_o6hoo1Fh9G1qfhdaz_540.jpg)

Yo usé vasos de plástico transparentes y puse pajitas cortas negras dentro. 

Si te es más fácil, pon el mantel blanco, y usa platos y vasos negros para hacer el contraste (según lo que te sea más fácil de encontrar).

Usé cuencos naranjas para los snacks, y así darle un toque de color a la mesa.

 ![](/tumblr_files/tumblr_inline_o6hoovSycT1qfhdaz_540.jpg)

También puedes hacerte un unos stickers para decorar los vasos, si te gusta. Tienes este [Album de Stickers](http://www.amazon.es/gp/product/1405364408/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=1405364408&linkCode=as2&tag=madrescabread-21) que está genial.

Los botellines de Font Vella edición limitada de Star Wars con motivo del estreno de la película El Despertar de la Fuerza los compré del súper, pero si no los encuentras, siempre puedes comprarlos on line[aquí](https://www.ulabox.com/producto/font-vella-agua-mineral-kids-star-wars-caja-35-botellas-x-33cl/27524).

 ![](/tumblr_files/tumblr_inline_o6hky9Mk0X1qfhdaz_540.jpg)

Pero hay mil ideas más:

Mira qué [cupcakes de Star Wars](http://sucreart.lasprovincias.es/recetas-magdalenas/cupcakes-vainilla-star-wars) más chulos.

En [este blog](http://sucreart.lasprovincias.es/recetas-paso-paso/como-organizar-fiesta-infantil-star-wars) y en [éste](http://sermadreunaaventura.com/2015/06/fiesta-de-cumpleanos-de-star-wars/)  tienes más ideas geniales.

Si te gustan los globos [aquí tienes un pack de héroes y villanos de Star Wars](http://www.amazon.es/gp/product/B00LZV8YMY/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00LZV8YMY&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00LZV8YMY) 

Aquí tienes una [guirnalda para fiestas Star Wars](http://www.amazon.es/gp/product/B013FV7FSU/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B013FV7FSU&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B013FV7FSU) y juegos de[servilletas, vasos y platos para fiesta](http://www.amazon.es/gp/product/B009OEIR4Y/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B009OEIR4Y&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B009OEIR4Y).

## Disfraces

Pero lo más importante es que los niños se sientan metidos en el ambiente de la fiesta, y nada mejor que vengan disfrazados para sentirse auténticos Jedi. Yo les improvisé estos disfraces caseros 

Si decides hacerlo así, no olvides especificarlo en la invitación.

 ![](/tumblr_files/tumblr_inline_o6hpd6WBoW1qfhdaz_540.png)

Que lo paséis muy bien, y ¡que la fuerza os acompañe!

 ![](/tumblr_files/tumblr_inline_o6hkyz7NWV1qfhdaz_540.jpg)