---
date: '2018-03-18T13:34:49+01:00'
image: /tumblr_files/tumblr_inline_p6m7ttXLe11qfhdaz_540.gif
layout: post
tags:
- familia
- eventos
- moda
- local
title: La primavera Aliaga desembarca en Murcia
tumblr_url: https://madrescabreadas.com/post/171995066264/desfile-aliaga-murcia
---

La primavera desembarcó ayer en Murcia con los 4 ejércitos de [Fernando Aliaga](https://www.facebook.com/aliagabyaliaga) y sus colores rojo, amarillo, verde para culminar con la luz del blanco, que se impusieron al tejido de pata de gallo, animal que representa lo bélico.

 ![](/tumblr_files/tumblr_inline_p6m7ttXLe11qfhdaz_540.gif) ![](/tumblr_files/tumblr_inline_p6m7tvVwEy1qfhdaz_540.gif)  
Inspirado en la época y estética de la Segunda Guerra Mundial, nos presentó unos modelos de corte y linea retro masculina con guiños a su tierra, Murcia.  
 ![](/tumblr_files/tumblr_inline_p6m7twgDxB1qfhdaz_540.gif) ![](/tumblr_files/tumblr_inline_p6m7twIuxg1qfhdaz_540.gif)  
La lucha y la exclusión pierden frente a la armonía de #arialiaga, la nueva era de la moda, donde tienen cabida cualquier color, forma, raza, sexo y condición.  
 ![](/tumblr_files/tumblr_inline_p6m7ty0USA1qfhdaz_540.gif) ![](/tumblr_files/tumblr_inline_p6m7tzubHX1qfhdaz_540.gif)  
Un evento organizado por la revista [Shopper Magazine](http://shoppermagazine.es/) de Murcia, con motivo de su aniversario, a la que tengo que dar la enhorabuena porque fue impecable.