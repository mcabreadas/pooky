---
date: '2016-10-09T11:37:32+02:00'
image: /tumblr_files/tumblr_inline_oeis5s5zxy1qfhdaz_540.jpg
layout: post
tags:
- crianza
- colaboraciones
- bebe
- puericultura
- ad
- eventos
title: 'Novedades en puericultura: la feria en video'
tumblr_url: https://madrescabreadas.com/post/151554577114/puericultura-madrid-guia
---

Un año más este blog de maternidad ha asistido a la [Feria de Puericultura de Madrid](http://www.ifema.es/puericulturamadrid_01/) para contarte todas las novedades que tendremos en los escaparates esta temporada, recomendarte lo que me ha parecido más interesante, y hacerte sugerencias por si estás buscando algún producto para tu bebé o para hacer algún regalo.

Ya es tradición hacer una guía de recomendaciones tras visitar la feria, como ya hice en anteriores ediciones ([guía de puericultura 2014](/2014/10/03/puericultura-madrid.html), [guía de puericultura 2015)](/2015/10/02/feria-puericultura.html).

Este año 2016, las marcas se han superado y han seguido innovando, investigando nuevos materiales, tejidos, tecnología… y también me han sorprendido por los diseños tan diferentes y rompedores que he descubierto.

He dividido el post por categorías para que sea más fácil consultar cosas concretas, y en lugar de fotos, he editado un pequeño video de cada una para que tengáis una visión de los productos más real.

## Paseo

<iframe width="100%" height="315" src="https://www.youtube.com/embed/GwQb0VT2epw" frameborder="0" allowfullscreen></iframe>

**Spottech Bébécar**

[Bébécar](http://www.bebecar.com/bebecar/es/) reinventa su silla de paseo, tras la [Spot, que ya probamos](/2015/09/20/test-spot-bebecar.html) en este blog, y Spot Plus, crea la Spottech, cuya característica principal es que se puede incorporar al mismo chasis del cuco y del grupo 0. Nuevo chasis que es más ligero que los tradicionales de esta marca, pero con las mismas funcionalidades y calidad.

Las ruedas delanteras se pueden poner fijas o giratorias, es fácil de manejar con una mano, y su capota es extensible.

Está homologada desde el nacimiento porque se reclina 180º.

Su brazo es extraíble y su cesta amplia y de facil acceso.

Es de fácil plegado en paraguas. 

La recomiendo porque me parece muy buena idea usar una silla compacta desde el principio sin tener que invertir cuando crezca en una silla ligera de segunda edad, ya que ocupa poco, tanto el chasis, que pliega fácilmente en paraguas, como el capazo, que es plegable también.

**Mios Cybex**

Uno de los carros más ligeros y completos que he visto es el [Mios de Cybex](http://cybex-online.com/es), es estrecho para meterlo por donde quieras, tejido especial en el asiento y reposapiés de red de malla, ruedas ligeras y flexibles, para pasear por superficies irregulares, la estructura es de aluminio ligero, y pesa 9 kg, se le puede adaptar capazo.

 ![](/tumblr_files/tumblr_inline_oeis5s5zxy1qfhdaz_540.jpg)

**Stylego Chicco**

Este elegante carro de [Chicco](http://www.chicco.es/) se cierra en periscopio y su estructura se sostiene sola una vez plegada, las ruedas se recolocan solas, se pliega con una sola mano.

Lo tenemos en versión trío, con silla de paseo reversible, totalmente reclinable, y reposapiés regulable.

Sus acabados son de primera calidad, el reposapiés y la barra de protección van ecopiel, los arneses forrados, la capota es extragrande y va forrada, además tiene una ventana de transpiración por detrás.

El capazo es extragrande, con forma de habichuela, más ancho por la zona de los pies, lo que da movilidad al bebé y homologado para ir en automóvil.

**Minuum Jané**

Coche moderno y urbano de [Jané](http://www.jane.es/es/), de ruedas pequeñas, freno trasero, suspensiones independientes delante y atrás, hamaca homologada desde el nacimiento y reversible, muy ligero, manlilar y respaldo regulables en altura.

Pesa con la hamaca 6 kg, y tiene una gran cesta.

Se puede anclar el cuco o el grupo 0 y tiene suspensión delantera y trasera, neumáticos de PU y las ruedas traseras son extraíbles.

Su tejido repele el agua, es transpirable y antimanchas.

## Portabebés

<iframe width="100%" height="315" src="https://www.youtube.com/embed/46Zv0A68BiE" frameborder="0" allowfullscreen></iframe>

**Yemaya Cybex**

Yemaya es la Diosa de la fertilidad del Caribe, y da nombre a estos [portabebés de Cybex](https://birdsofparadise.cybex-online.com/index.php#collection) con forma de flor porque las solapas se van abriendo conforme el bebé crece. Es ergonómico y está diseñado con matronas y especialistas.

Hay varios modelos que se adaptan al estilo de la madre antes de ser madre: ejecutivo, más cañero, camuflaje…

Aunque sus materiales son propios de la moda, como la polipiel, lo que va en contacto con bebé es algodón y suave. Es adecuado desde el nacimiento hasta los dos años, y admite las tres posiciones: delantera, lateral y a la espalda.

Tiene multitud de funcionalidades ocultas, para lograr un diseño limpio de líneas elegantes.

**Wmm **

Me han encantado estos portabebés de Wmm por su tejido de malla para zonas calurosas.

Los Mei Tai para usar desde los 18 a los 36m y de 11 a 25kg, es perfecto hasta los 3 años de edad. Es fácil de colocar y ligero, colorido, fresco y divertido, está en diferentes modelos y dos tallas. Yo usé uno con mi hijo pequeño, pero artesanal, la verdad es que es la primera vez que lo veo comercializar. Me encanta esta forma de porteo.

## Textil

<iframe width="100%" height="315" src="https://www.youtube.com/embed/wuy0lHkiYQI" frameborder="0" allowfullscreen></iframe>

**Dooky Saro**

Accesorios prácticos de [Saro](http://www.sarobaby.com/) para facilitarnos la vida. Una práctica cortinilla enganchada con anillas a la capota de la sillita para desplegarla cuando esté el bebé dormido para que no le entre la luz, y enrollable cuando no se necesite.

También es muy práctica la funda para el grupo 0 ajustable y universal de algodón.

**Esther Gili Tutete**

La línea de bolsita, mochilita y babi diseñada por [Esther Gili](http://www.39semanas.com/) para [Tutete](https://www.tutete.com/tienda/index.php?mainSearch=esther+gili&language=es)es muy especial, como todas sus ilustraciones, y con un diseño precioso. La calidad de los materiales es excepcional, digna de Tutete.

**Colección Silence de Elodi Details**

Inspirada en los bosques nórdicos, [Elodi Details](https://www.elodiedetails.com/) presenta esta colección inhundada de flores, colores de la naturaleza, caquis…Bolsos maternales, mantitas, neceseres zipp and go con gran abertura para encontrar fácilmente, gorros, sacos para carritos universales muy calentitos, capas e baño baberos, chupetes y cadenitas.

**Muselinas animales de Wmm**

Estas originales muselinas con diseños de animales vienen en diferentes tamaños para todo tipo de bebés. 

Son súper suaves y el material es absorbente, perfectas para taparles, limpiarles o jugar. 

Como si de una marioneta se tratara, puedes hacer que el bebé se divierta con ellas. 

Útil. Bonita. Ingeniosa. y divertidas, y prácticas por su plegado.

## Viaje

<iframe width="100%" height="315" src="https://www.youtube.com/embed/ZfKf0sNZza8" frameborder="0" allowfullscreen></iframe>

**Silla Youniverse Chicco G I, II y III**

Esta [silla de auto de Chicco](http://www.chicco.es/productos-chicco/de-paseo-y-de-viaje/sillas-de-auto.html) es muy práctica porque sólo tendrás que comprar una silla, y te servirá hasta los  10 años.

Es regulable en altura , y está tanto en versión fix plus como en versión tres puntos, y tiene 3 niveles de reclinación.

 ![](/tumblr_files/tumblr_inline_oekj95BaJm1qfhdaz_540.jpg)

**Gravity Jané**

Ya [testeamos en el blog la anterior silla EXO](/2015/04/14/seguridad-infantil-en-autom%C3%B3vil-testing-silla-exo.html), Hace unos meses tuve el honor de [visitar el Crash Test Center de Jané](/2015/02/18/seguridad-retencion-infantil.html), y quedé impresionada con lo importante que es invertir en seguridad para salvar vidas.

Ahora [Jané](http://www.jane.es/es/) lanza una silla homologada con la normativa I size a contra marcha reversible, isofix y pata delantera, desde 40 a 60 cm tiene que ir con reductor acontra marcha, de viscoelástica, y a partir de los 60 hasta 105 sin reductor a contra marcha, de 72 a 105 se puede dar la vuelta (aunque no lo recomiendo, como ya os expliqué aquí, sirve hasta los 4 años.

Se reclina muchisimo , capacidad, el giro desde cualquiera de los dos lados con una sola mano. en la posición de carga la silla n queda anclada, una vez que pone al niño, su peso hace que gire y se ancle, de esta forma se evitan despistes.

## Descanso

<iframe width="100%" height="315" src="https://www.youtube.com/embed/BHmAF31meFU" frameborder="0" allowfullscreen></iframe>

**Cojín Comfy Nest Chicco**

Este [cojín de descanso ergonómico de Chicco](http://www.chicco.es/productos-chicco/relax-y-descanso.html) hace la función de hamaca, pero no bascula, aguanta hasta 18 kg y sirve como puf cuando el bebé ya se sienta.

Tiene arneses de retención regulables, reposa cabezas, y está fabricada con viscoelástica con memoria, con un diseño ergonómico estudiado en la Universidad de Pavia, que respeta la anatomía del bebé.

Su tejido es transpirable y lavable.

**Cuna de viaje Ligth Babybjörn**

Esta [cuna de viaje de Babyjörn](http://www.babybjorn.es/minicuna-cuna-de-viaje/cuna-de-viaje-light/) es de las más ligeras que he visto, ya que sólo pesa 6 kg, también sirve de parque, el colchón hace de base para que la cuna se sostenga firme. 

El plegado es muy fácil, como puedes ver en el video.

**Peluche calentador Snoogy Babymoov**

Suave [peluche de Babymoov](http://www.babymoov.es/) para cólicos y relajar. Es muy suave, y el bebé lo tomará como un juguete, en su interior contiene lavanda natural y semillas de colza, que desprenden un olor suave y relajante. 

**Hamaca UpLift Nikidom**

Esta bonita [hamaca de Nikidom](http://www.nikidom.com/Ficha00.asp?id=3154) te servirá también para decorar el salón.

Regulable en todas las alturas, diseño moderno y se balancea con los movimientos del niño. 

Se eleva hasta los 86 cm con una rueda que permite regularla a la altura deseada.

Es plegable y ocupa poco espacio cuando no la vayas a usar.

Sirve hasta los 11 kg del bebé

## Comer

<iframe width="100%" height="315" src="https://www.youtube.com/embed/QiM5efniLEg" frameborder="0" allowfullscreen></iframe>

**Trona Marcel Wanders Cybex**

Esta [trona de Cybex](http://cybex-online.com/es) destaca por su diseño totalmente rompedor. Todavía no está en el mercado, tendremos que esperar hasta primavera, pero es acolchada y muy decorativa.

**Vajilla Nature Saro**

[Vajilla de Saro](http://www.sarobaby.com/saro/productos/vajilla-bambu-nature) fabricada con bambú, con diseños originales, y que se puede lavar en lavavajillas, pero no meter en microondas.

**Squiz**

[Bolsitas Squiz](http://www.squiz.co/decouvrez-squiz#quiz-gourde-reutilisable) reutilizables lavables para meter los purés. Ideal para cuando salimos de casa a la hora de comer o merendar, y para que los niños puedan tomarlos de forma autónoma, ya que sólo tienen que succionar.

Hay varios diseños, las nuevas colecciones está inspiradas en el carnaval, y en la Jirafa Sophie.

## Juego y desarrollo

<iframe width="100%" height="315" src="https://www.youtube.com/embed/vSpIVz94rtw" frameborder="0" allowfullscreen></iframe>

**Gimnasio Bright Starts Pretty in Pink**

Este gimnasio para bebés me ha parecido una monada. Es cierto que lo usan durante unos meses sólo como tal, pero a mis hijos siempre les ha venido genial para estimularlos cuando empiezan a moverse. 

Después pueden usarlo como mantita sólo para sentarse. 

**Orinales tutete**

Estos [orinales rockeros de Tutete](https://www.tutete.com/tienda/es/90_bano/301_operacion-panal/9109_orinal-rock-star.html) son del tamaño perfecto y tan sencillos que no tendrán complicación para sentarse.

Además son muy originales, para los bebés más rockeros.

**Conejos luz Olala**

Tres [conejitos de Olala](https://bbgrenadine.com/) que se cargan en una sola base, y que iluminarán el camino de tu bebé al baño por la noche. Puedes poner uno en su mesita, otro en el pasillo, y otro en el baño. Así no se despistará y fomentaremos su autonomía.

**Monos Olala**

Los [monos de Olala](https://bbgrenadine.com/) sujeta todo son ideales para que empiecen a aprender a dejar cad cosa en su sitio. Se pegan con imanes, y se puede colgar el cepillo, o lo que se te ocurra.

**Mordedores The Little big Things**

Los [mordedores The Little big Things](https://bbgrenadine.com/97-mordedores-doudous-y-sonajeros) están fabricados en Alemania con materiales muy cuidados, madera barnizada al agua, y plástico BPA, estos mordedores son ideales para la época de la dentición.

## Maternal

<iframe width="100%" height="315" src="https://www.youtube.com/embed/kkbkfMR0Cy4" frameborder="0" allowfullscreen></iframe>

**Dream belt Babymoov**

El primer cinturón que he visto para embarazadas que sujeta la barriga y facilita el descanso.

[Babymoov](http://productos-puericultura.babymoov.es/) lo ha desarrollado con matronas y tiene 2 cintas elásticas para sostener el vientre que cieran con velcro, 2 refuerzos de espuma con memoria para rellenar el hueco que queda entre la barriga y el colchón en posición lateral.

**Bolsos Ioi**

Lisa, la creadora de la marca, trabajaba en una reconocida firma de moda cuando dio a luz a su hija Isabella y entonces alguien le dijo: “Necesitas una bolsa”. Quería decir que necesitaba un bolso que además de las cosas del bebé le permitiera llevar sus muestras de tejidos y otras prendas de trabajo. Prada, la marca para la que trabajaba, utilizaba mucho la microfibra y este es el material que utilizó para darle un look más urbano a su nuevo bolso maternal. No quería estar llevando un bolso que su pareja no quisiera llevar por lo que dejó atrás los grabados, osos de peluche y el resto de elementos infantiles. El nombre de OiOi viene del padre de Isabella y su abuelo que era italiano y solía decir "Oi oi!”.

## Gadgets

<iframe width="100%" height="315" src="https://www.youtube.com/embed/kcfmsU6ayWQ" frameborder="0" allowfullscreen></iframe>

**Oveja Ewan de Saro**

Esta adorable ovejita de [Saro](http://www.sarobaby.com/saro/) tiene 4 sonidos de baja frecuencia, como el mar, el latido del corazón… y una suave luz rosada que parece su corazón.

El muñeco es dulce y tierno, y ayudará al bebé a relajarse antes de dormir, ya que puede agarrarlo y acurrucarse si quiere. 

**Intercomunicador TQT**

Tiene tres cosas que me gustaron, la primera es que tiene proyector, la segunda, es que costa de un solo dispositivo, ya que el segundo sería tu propio móvil, y la tercera, es que es libre de emisiones.

**Humidificador Hygro [Babymoov](http://www.babymoov.es/)**

Creo que es el humidificador más completo que he visto porque tiene:

Ajuste de la tasa de higrometría a la hora deseada

Salda de vapor orientable 360º (vapor frío)

Pantalla digital retroiluminada, donde puedes ver el grado de humedad, la emperatura y la hora

22 h de autonomía

## Diseños rompedores

<iframe width="100%" height="315" src="https://www.youtube.com/embed/yYqQB_6f3C4" frameborder="0" allowfullscreen></iframe>

La feria este año nos ha traído un derroche de originalidad.

En este video puedes ver los diseños más impactantes que vi en la feria de la mano de:

**[Cybex](http://cybex-online.com/es) y Moschino**

 ![](/tumblr_files/tumblr_inline_oeqstod5G51qfhdaz_540.jpg)

[Bébécar](http://www.bebecar.com/)

[kiddy](http://www.kiddy.de/es/index.html)

[Colección People Tuc Tuc](https://www.tuctuc.com/es/colecciones/puericultura/453-people)

[Carritos Disney Easywalker](http://easywalker.tienda/)

## Pasarela MIMO

Por primera vez se celebra en la Feria de Puericultura de Madrid una pasarela donde las marcas participantes han exhibido sus productos en movimiento de esta forma tan original:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/gOmveehfcbc" frameborder="0" allowfullscreen></iframe>

Participaron:

[Babyhome](http://www.babyhome.es/es/es)

[Baby Monster](http://www.baby-monsters.com/)

[Bebé Due](http://www.bebedue.com/esp/index.php)

[Bonjour Bebé](http://www.bonjourbebe.net/)

[Pielsa](http://www.pielsa.es)

[Pirulos](http://www.coimasa.com/)

[Bebecar](http://www.bebecar.com/)

[Kiddy](http://www.kiddy.de/es/index.html)

[Tuc tuc](https://www.tuctuc.com/es/)

[Cybex](http://cybex-online.com/es)y GB

[Micuna](http://micuna.com/nueva-coleccion-de-micuna-disenada-por-agatha-ruiz-de-la-prada/)

Como ves este año la [Feria de Puericultura de Madrid](http://www.ifema.es/puericulturamadrid_01/) ha sido todo un reto para mí, he probado muchas cosas por mí misma, y he disfrutando investigando, descubriendo, preguntando y compartiéndola contigo.