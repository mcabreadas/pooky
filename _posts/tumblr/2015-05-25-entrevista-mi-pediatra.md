---
date: '2015-05-25T19:38:38+02:00'
image: /tumblr_files/tumblr_inline_nox27jMZeJ1qfhdaz_540.jpg
layout: post
tags:
- revistadeprensa
- colaboraciones
- ad
- familia
- conciliacion
title: Me entrevistan en la revista Mi pediatra
tumblr_url: https://madrescabreadas.com/post/119863928839/entrevista-mi-pediatra
---

De nuevo este humilde blog da el salto a los medios en papel, y le dedican unos párrafos en la revista “Mi pediatra”.

 ![](/tumblr_files/tumblr_inline_nox27jMZeJ1qfhdaz_540.jpg)

Muchas gracias, Virginia, por esta oportunidad de llegar a más mamás y papás!

¿Qué os parece?