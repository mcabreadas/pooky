---
layout: post
title: Excursión con niños por la Senda del Dinosaurio, Murcia
date: 2013-10-28T07:35:00+01:00
image: /tumblr_files/tumblr_mvc90am6v11qgfaqto1_1280.jpg
author: .
tags:
  - planninos
  - 3-a-6
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/65317479201/excursión-con-niños-senda-del-dinosaurio-sierra
---
Este fin de semana fue muy especial. Desde que nació Leopardito él y yo no habíamos podido salir de excursión con el resto de las fieras de la familia porque yo andaba siempre tan cansada por las tetadas nocturnas, que no era persona para echarme al monte a hacer el cabra.\
 Pero ayer fuimos todos juntos a buscar la huella de un dinosaurio! Sí, en serio, hay una mini ruta de senderismo ideal para los niños a unos Kilómetros de casa, donde el objetivo es encontrar un gran socabón, que dicen que lo hizo un dinosaurio al pisar hace millones de años.\
 Imaginaos la aventura que supone encontrarlo para los niños! ![](/tumblr_files/tumblr_mvc90am6v11qgfaqto2_1280.jpg) 

 Además, hay que cruzar un puente volado, de esos de Indiana Jones, que si resulta que papá lo mueve mientras cruzamos, es mucho más emocionante (al principio da miedo, pero luego todos quieren repetir y pasar una y otra vez).\
 También hay un surco en la montaña, de donde sale un canal, que en su día debió de llevar agua, que también da mucho juego porque, entrar no se atreven a entrar, pero mientras prueban a ver quien se mete y quien no, pasan un rato divertidísimo.

![](/tumblr_files/tumblr_mvc90am6v11qgfaqto3_1280.jpg)  

Si a esto le añadimos que hay que consultar un plano que está en el sendero, y averiguar por dónde hay que ir para descubrir la huella, tenemos todos los ingredientes para hacer volar la imaginación de los peques hasta el punto de que casi que ven el dinosaurio si te descuidas.\
 A nosotros nos gustan mucho este tipo de excursiones donde los peques tienen que cumplir un objetivo. 

 Otras veces vamos a buscar un tesoro, pero un tesoro de verdad que alguien escondió previamente , y nosotros,dejamos otro para que el siguiente que llegue lo encuentre. Conocéis el [Geocaching](https://madrescabreadas.com/2016/09/25/la-aventura-de-encontrar-tesoros-con/)? Os lo recomiendo cien por cien.\
 Cuál es la última aventura que habéis vivido en familia?