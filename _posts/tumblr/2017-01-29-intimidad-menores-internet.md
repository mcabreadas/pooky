---
date: '2017-01-29T18:05:11+01:00'
image: /tumblr_files/tumblr_inline_okjwqysUEp1qfhdaz_540.png
layout: post
tags:
- tecnologia
- revistadeprensa
- derechos
title: Derecho a la intimidad de los menores en Internet y responsabilidad de los
  padres | El club de las madres felices
tumblr_url: https://madrescabreadas.com/post/156538510664/intimidad-menores-internet
---

[Derecho a la intimidad de los menores en Internet y responsabilidad de los padres | El club de las madres felices](http://elclubdelasmadresfelices.com/intimidad-menores-internet/)  

![](/tumblr_files/tumblr_inline_okjwqysUEp1qfhdaz_540.png)

Sabía que tarde o temprano iba a ocurrir, y ocurrió. Hace unas semanas conocíamos la noticia de que una [joven austríaca de 18 años denunció a sus padres](http://www.antena3.com/noticias/mundo/joven-denuncia-sus-padres-publicar-fotos-cuando-era-pequena-facebook_2016092257e415b90cf21f619134d239.html) por haber estado subiendo fotos suyas a Internet de todas las etapas de su infancia, tras haberles pedido en multitud de ocasiones, sin éxito, que las eliminaran.

Este caso puede resultar anecdótico, pero para mí es la punta del iceberg de algo muy serio, de lo que los padres somos totalmente responsables y debemos salvaguardar: el derecho al honor, a la intimidad y a la propia imagen de nuestros hijos.

Por eso, he escrito este post para mis amigas de el Club de las Madres Felices de Suavinex, donde cuento, desde el punto de vista legal, lo que supone compartir la imagen de nuestros hijos en la red.

Podéis leerlo [aquí](http://elclubdelasmadresfelices.com/intimidad-menores-internet/).