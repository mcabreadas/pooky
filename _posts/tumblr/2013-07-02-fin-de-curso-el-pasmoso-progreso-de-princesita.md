---
date: '2013-07-02T13:28:00+02:00'
image: /tumblr_files/tumblr_inline_mpb3q3d9Fs1qfhdaz.jpg
layout: post
tags:
- adolescencia
- crianza
- educacion
- familia
title: 'Fin de curso: El "pasmoso" progreso de Princesita en el cole.'
tumblr_url: https://madrescabreadas.com/post/54421957310/fin-de-curso-el-pasmoso-progreso-de-princesita
---

![image](/tumblr_files/tumblr_inline_mpb3q3d9Fs1qfhdaz.jpg)

Hoy saco mi orgullo de madre, me lo vais a permitir, para presumir de mi hija mayor. Una niña a la que le ha costado mucho el cambio de infantil a primaria, y que ha tenido sus primeros desengaños con las amiguitas este curso.

Para colmo, la revolución de un nuevo hermanito en casa no le ayudó a centrarse en el cole precisamente, y mucho menos en casa a la hora de hacer los deberes. Además de afectar a su relación con sus amigas del cole, pasando momentos de gran angustia, incomprensión y soledad. Lo que, para una niña de 7 años, con una sensibilidad extraordinaria, ha sido muy duro.

De repente, mucha menos atención de papá y mamá, irritabilidad por nuestra falta de sueño y, por primera vez, bastantes deberes diarios para hacer en casa.

Atravesó una dura etapa durante el primer trimestre de este curso, que se agravó en el segundo. 

Me enteré por casualidad por un comentario de su profesor en un encuentro fortuito, que hizo que se me encendieran todas las alarmas. Me sentí mal porque no había sido consciente hasta ese momento. Mi niña lo estaba pasando realmente mal en el cole y yo no me había percatado porque estaba totalmente absorbida por el bebé, su reflujo, la lactancia, la falta de sueño…

No era justo para ella que no le hubiera prestado la suficiente atención, así que me puse manos a la obra. Hablé con mi marido y mantuvimos una reunión con el tutor de la niña. Todos cooperamos para ayudarla, incluída su abuela, que nos ayuda mucho con los peques. 

Cada semana yo iba a hablar con su profesor, y lentamente comenzó el progreso.

Una conversación con su padre fue la clave para que Princesita reaccionara y comenzara a confiar en que ella podía superar las dificultades, y que si se lo proponía, podía conseguir lo que ella quisiera. Tenía que darse cuenta de que era capaz de todo. Sólo tenía que creérselo, proponérselo, y trabajar en ello.

La labor de su tutor a nivel académico, y de su profesora de Religión a nivel humano y de ayuda en su relación con los demás niños fue inestimable. Dos profesionales que pusieron todo su empeño en sacar lo mejor de mi hija.

Y como ingrediente final, y esencial, la gran valía de mi Princesita, que gracias a su esfuerzo y constancia ha conseguido terminar el curso mejorando en todas las asignaturas, totalmente feliz e integrada con sus compañeros de clase, y con una felicitación muy especial de su tutor que resalta el “pasmoso" progreso de una niña que lo único que necesitaba era que confiaran en ella y que le dieran un empujoncito.

Felicidades, Princesita! 

Vales mucho, no lo olvides!