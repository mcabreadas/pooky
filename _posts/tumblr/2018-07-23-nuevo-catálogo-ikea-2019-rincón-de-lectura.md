---
date: '2018-07-23T07:14:09+02:00'
image: /tumblr_files/tumblr_inline_pca1vfNDeT1qfhdaz_1280.png
layout: post
tags:
- decoracion
- trucos
- familia
title: Nuevo catálogo IKEA 2019. Rincón de lectura infantil
tumblr_url: https://madrescabreadas.com/post/176183695464/nuevo-catálogo-ikea-2019-rincón-de-lectura
---

El nuevo catálogo de Ikea 2019 llegará a tu buzón en septiembre, pero si eres una adicta a la decoración de tu salón, dormitorios o te encanta introducir elementos decorativos nuevos en la cocina, baños o en las habitaciones infantiles, seguro que no podrás esperar y estarás deseando conocer las novedades que nos tiene preparadas Ikea para 2019.

Aquí te dejo un adelanto con una sugerencia para montar un rincón de lectura infantil para fomentar este hábito tan positivo para nuestros hijos, ya que si tienen su propio espacio, aunque sea pequeñito, y les permites elegir sus propios libros según sus preferencias y gustos, seguro que terminarán por incorporar la lectura a su rutina diaria con toda naturalidad (por cierto, yo os sugiero estos l[ibros para toda la familia](https%3A%2F%2Fmadrescabreadas.com%2Fpost%2F141479516209%2Flibros-ayuda-ninos&t=YzIwM2U2YjAyMDliNTU4ZTU5NjkxZGUyN2RlMDdhYWZiNGE5ZWYzZSxiYzcyYjFjMDFlZjJhNGIzZThmMjc3OGMxN2MwMGVmNWNhMjJjMTJj) estas vacaciones)

Estas sonáis sugerencias:

Rincón de lectura infantil con el nuevo catálogo de Ikea

Ikea incorpora a su nuevo catálogo un sillón infantil ideal para crear el rincón de lectura queso comento. Parece de lo más cómodo, verdad? Así se sentirán y sentarán como mamá cuando lee.

 ![](/tumblr_files/tumblr_inline_pca1vfNDeT1qfhdaz_1280.png)

Yo le podría un puf de estos para apoyar los pies y que estén aún más cómodos, cuál elegirías?

 ![](/tumblr_files/tumblr_inline_pca1xtn5W41qfhdaz_1280.png)

Para poner los los libros, podemos poner un par de estanterías de estas a la altura de los niños para que tengan facilidad para alcanzarlos y dejarlos ordenados una vez terminen de leer..

 ![](/tumblr_files/tumblr_inline_pca20klrCq1qfhdaz_1280.png)

O si no tienes ganas de hacer agujeros en la pared, o la zona donde vas a poner el rincón de lectura no se presta a ello, puedes poner los libros en este carrito que, además, tu peque podrá desplazar por la casa, ya que está diseñado para que los niños puedan arrastrarlo con facilidad.

 ![](/tumblr_files/tumblr_inline_pca2hpyYTT1qfhdaz_1280.jpg)

Y para iluminar la lectura, ¿qué me dices de este flexo de jirafa? Me parece de los más original.

 ![](/tumblr_files/tumblr_inline_pca24jkhNo1qfhdaz_1280.png)

Y si quieres darle un toque de calidez al rincón y definir mejor su espacio de lectura, esta alfombra de puntos bicolor en blanco y negro resulta  ideal, no crees?

 ![](/tumblr_files/tumblr_inline_pca2o5dOF61qfhdaz_1280.jpg)

¿Te ha gustado la idea? 

Os iré mostrando más novedades por aquí, y en las redes sociales de nuevo catálogo de Ikea 2019.

Sígueme!