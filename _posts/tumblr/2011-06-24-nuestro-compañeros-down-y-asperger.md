---
layout: post
title: Nuestro compañeros Down y Asperger
date: 2011-06-24T12:13:00+02:00
image: /images/uploads/nathan-anderson-fhijwobodrs-unsplash.jpg
author: maria
tags:
  - mecabrea
  - educacion
tumblr_url: https://madrescabreadas.com/post/6860960934/nuestro-compañeros-down-y-asperger
---
Como lo prometido es deuda y, aunque no llevamos mucho tiempo de curso como para llegar a conclusiones más formales, los primeros comentarios de mi niño respecto de sus dos compañeros ya han aparecido. Yo, la verdad, estaba deseando saber de ellos a través de los ojos de mi pequeño, pero no quería quitarle espontaneidad. Así que nohabía preguntado abiertamente.

 Pero hoy, hablando de todo un poco en el desayuno, me ha hablado de su amigo L, que tiene síndrome de Down y de su compañero A, con síndrome de Asperger.

Dice que A hace pintarrajos y coge el lápiz con el puño, que se quita los zapatos y que se escapa. “Mamá, por qué habla así?” Y añade, es bueno… no pega (mi hijo mide la bondad de los niños y niñas según le peguen o no, es muy práctico). Lo dice con naturalidad y sin extrañeza.

Dice que “L es muy bonico, es como un bebé… pero no. Ese sí que me quiere” le pregunto, -“y tú, lo quieres a él?” y contesta -“sí, lo quiero” y añade “no sabe dibujar, pero la profe lo enseña. A veces se duerme y está cansado”- “tú lo cuidas?” - “sí, lo cuido”.

Me ha emocionado que hablara con naturalidad de A., y con esa ternura de L.

Ya os contaré más cosas.

## Post originario 24-6-11:

Vengo desolada del colegio de mi hija donde el curso que viene va a empezar mi pequeño. La madre de una nińa que empieza con el mío, con la cara desencajada, me ha informado de que van a tener dos compañeros con necesidades especiales: uno nińo con síndrome de Down y otro con síndrome de Asperger.

Ella lo ve como algo negativo que va a bajar el nivel de la clase porque son nińos que preisan muchas atenciones especiales y una persona dedicada exclusivamente a ellos, mientras, los demás nińos quedarán desatendidos o, al menos, no tan atendidos como debieran.

Yo le he respondido, un poco aturdida, y sin entender bien la gravedad del asunto, que veía una oportunidad de que nuestros hijos crezcan como personas, y que se aporten cosas unos a otros.

En el caso de mi hijo, creo que tiene mucho que aportar porque es un nińo muy carińoso y protector, que empatiza mucho con otros, a la vez que es muy sociable, extrovertido, comunicativo y divertidísimo. Creo que se pueden ayudar mutuamente y puede ser muy enriquecedor para todos, incluso para desarrollar la sensibilidad, tolerancia y respeto hacia los demás.

Creo que nuestros hijos van a ser mejores personas gracias a su dos compañeros especiales. Pero también va a enriquecer a los padres, que parece ser que son quienes más problemas ponen, y para el centro que, si bien no ha tenido  hasta ahora muchos casos de este tipo, tendrá que aceptar el reto de adaptarse y aunar esfuerzos para que estos nińos se integren y se abran al mundo.

Os dejo con una reflexión: ¿cómo os sentiríamos si fuera nuestro hijo el que se ve rechazado, incluso antes de conocerlo? Esto es lo que me ha cabreado hoy. Ya me diréis si tengo razón.

Este vídeo lo dice todo. Miradlo, por favor, si puede ser con vuestros hijos. Yo se lo voy a poner a los míos en cuanto lleguen del cole/guarde. Gracias a @ClaraGrima por pasármelo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/K0usZT3LGOQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Os dejo también dos enlaces para estar informados:

<http://es.wikipedia.org/wiki/S%C3%ADndrome_de_Asperger>

<http://es.wikipedia.org/wiki/S%C3%ADndrome_de_Down>