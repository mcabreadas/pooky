---
date: '2016-07-06T18:48:11+02:00'
image: /tumblr_files/tumblr_oag8pmXpx91qgfaqto1_540.png
layout: post
tags:
- premios
- colaboraciones
- planninos
- ad
- eventos
- familia
title: Primera sala de cine para niños. Sorteo
tumblr_url: https://madrescabreadas.com/post/147000647024/sala-cine-junior
---

## Actualización 17-7-16

He realizado el sorteo mediante la web Sortea2, y sin más demora os digo el resultado. And the winner is……………

![](/tumblr_files/tumblr_oag8pmXpx91qgfaqto1_540.png)

Tat Fdez!!

 

Enhorabuena a la ganadora y gracias a todos por participar !!

Las entradas se podrán disfrutar hasta el 2 de agosto de 2016, y sólo en la Sala Junior de Yelmo Cines en Centro Comercial Islazul, Madrid, así que, Pat, pásame tus datos por privado, que te las enviemos lo antes posible.

Post originario

La [sala Junior de Yelmo Cines](http://www.yelmocines.es/sala-junior) se encuentra en el Centro Comercial Islazul de Madrid, y es la primera de Europa en ofrecer juegos inegrados en la propia sala, y comodidades especiales para los más pequeños.  

 ![](/tumblr_files/tumblr_inline_oag9fkJdXe1qfhdaz_540.jpg)

Nuestra colaboradora Rosa, de [Paraído Kids](http://www.paraisokids.es/), asistió a la presentación de esta novedosa forma de ver cine para nuestros hijos, acompañada de la suya, quien se lo pasó pipa disrutando de los juegos y de la proyección de la película Buscando a Dory, de Disney Pixar:

 ![](/tumblr_files/tumblr_inline_oag9fkmE4j1qfhdaz_540.jpg)

> De nuevo colaboramos con este blog, y esta vez disfrutamos de la presentación de la apertura de la nueva sala de cine Junior en el CC Islazul, una apuesta muy importante para Yelmo Cines.

 ![](/tumblr_files/tumblr_inline_oag9flaYZx1qfhdaz_540.jpg)

> ¡Bárbaro fue lo que estábamos viendo mi pequeña y yo!

Un nuevo concepto innovador, que en España no habíamos podido disfrutar hasta ahora porque no existía.

## Un concepto diferente

Se trata de una sala dedicada a las familias, a los más peques de la casa, porque… ¡Qué embarazoso es a veces ir al cine con ellos! ¿o no? Algunos se cansan a la media hora y no paran quietos en la silla mientras intentas retenerlos como puedes para que molesten lo menos posible a los demás espectadores. 

¡Pues esto se ha terminado! 

 ![](/tumblr_files/tumblr_inline_oag9flYWxW1qfhdaz_540.jpg)

El Community Manager de la compañía Yelmo cines, Fernando Évole, fue el encargado de mostrarnos e informarnos de los motivos y el funcionamiento de esta sala. (Muchas sois las que os habéis intresado a través de las redes sociales).

## La sala está diferenciada en cuatro bloques
 ![](/tumblr_files/tumblr_inline_oag9flf2qB1qfhdaz_540.jpg)
- El primer bloque, el más cercano a la pantalla de cine, goza de tumbonas.  
- En el segundo bloque, están situados los cojines dobles y los puff dobles.  
- En el tercer bloque las butacas tradicionales, aunque con un diseño más molón.  
- He dejado lo mejor para lo último, ¡el parque de ocio infantil!, con tobogán, colchonetas, balancines…
 ![](/tumblr_files/tumblr_inline_oag9fl3rCc1qfhdaz_540.jpg)

Como todo centro de ocio hay unas normas que cumplir: no olvidar los calcetines, los adultos no pueden disfrutar del parque, igualmente no pueden entrar a la sala si no es con un menor de 13 años, ni viceversa.

## ¿Vemos la peli o jugamos?
 ![](/tumblr_files/tumblr_inline_oag9fmsWq01qfhdaz_540.jpg)

Y os estaréis preguntando… ¿Al cine se va a ver la peli y no a jugar? Pues os decimos que hay tiempo para todo. Antes de comenzar, los niños pueden disfrutar del parque.

 ![](/tumblr_files/tumblr_inline_oag9fmTubB1qfhdaz_540.jpg)

A mitad de peli, para evitar cansancio, agotamiento o indiferencia (normalmente les pasa a los niños más peques), hay un descanso para que se desfoguen en el centro de ocio infantil.

 ![](/tumblr_files/tumblr_inline_oag9fm8wqf1qfhdaz_540.jpg)
## Pero, ¿y los adultos?

Pero no todo va a ser para ellos, que no, que no. 

A la entrada del cine esta el Coffee Tree, donde los papás pueden comprar café, tea, muffins ¡Ainss que hambre!

 ![](/tumblr_files/tumblr_inline_oag9fmNU141qfhdaz_540.jpg)

Yelmo Cines apuesta por incluir alimentos que no son habituales en las salas españolas pero que sí lo son cuando estamos en casa viendo una peli.

Deseamos que el proyecto tenga buena acogida por el público madrileño y así pueda extenderse a toda España, y poder disfrutar de #BuenRolloEnFamilia!!

Muchas gracias, Rosa por informarnos de esta idea revolucionaria para que los niños vean cine sin cansarse de estar quietos.

Esta idea, respetuosa con la actividad natural de los niños, que necesitan moverse cada poco tiempo, pero no porque se porten mal, sino porque va en su naturaleza (lo raro sería que no se movieran), me parece de lo más acertada.

La sala junior, pensada exclusivamente par ellos, para que conjuguen su dinamismo con la tarea estática de ver una película me parece el entorno más adecuado para que, sobre todos los más pequeños, aguanten la película entera, ya que además, la proyección para a mitad para que se desfoguen con los juegos.

Yo, los llevaría un rato antes para que jugaran un poco, se cansaran, y lograr que les apeteciera sentarse un rato, así creo que conseguiría que el pequeño no se cansara, y podría llevármelo al cine con sus hermanos siempre.

## Sorteo de 3 entradas para una sesión en la sala junior

Y para que no os quedéis con los dientes largos, [Yelmo cines](http://www.yelmocines.es/sala-junior), [Paraíso Kids](http://www.paraisokids.es/), y este blog quieren que una de vosotras asistáis a una sesión de cine en la sala junior.

Para participar en el sorteo:

- Deja un comentario en este post contándonos a qué edad llevaste al cine por primera vez a tus hijos. También deja tu nick de Facebook.  

- Da un “me gusta” a la página de Facebook de Paraíso Kids pinchando [aquí](https://www.facebook.com/Paraiso-KIDS-1070385076325169/?fref=ts).  

- Comparte el sorteo en Facebook en modo público para que podamos comprobarlo caso de resultar ganadora.  
- El plazo para concursar termina el 16 de julio, y se publicará lo antes posible en este mismo post.
- El premio consiste en 3 entradas para asistir a una sesión en la sala junior de Yelmo en Isazul, Madrid.
- Me reservo el derecho de eliminar cualquier participación que me parezca fraudulenta sin previo aviso y según mi criterio.

Os dejo este video para que veáis lo que os espera!

Vamos, participa!

¿A qué edad llevaste a tus hijos al cine por primera vez?

<iframe src="https://www.youtube.com/embed/mVqXOcCRiws" width="100%" height="315" allowfullscreen="" frameborder="0"></iframe>