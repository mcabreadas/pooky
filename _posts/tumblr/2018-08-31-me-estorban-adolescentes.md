---
date: '2018-08-31T14:22:00+02:00'
image: /tumblr_files/tumblr_pe26uzp0gh1qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- familia
title: Me estorban los niños (adolescentes)
tumblr_url: https://madrescabreadas.com/post/177585093754/me-estorban-adolescentes
---

El otro día invitaron los niños una fiesta de cumpleaños, de esas de toda la vida, que sabéis que me gustan tanto, y al final acabamos los padres también tomando algo y charlando de la vida, de la educación, dela crianza de nuestros hijos… Y como siempre, este tipo de reuniones de adultos me suelen inspirar para escribir posts… para mí son una mina. De hecho, gran parte de mis publicaciones iniciales, cuando empecé el blog, salieron de conversaciones en el patio del colegio, en el parque, reuniones de clase o cumpleaños infantiles.

Vamos al tema buenos ocupa.

## Comentario de madre de niña adolescente

Comentario de madre random:

> “Ay, esta últimamente más pegada a mí, con 13″ años ya podría dejarme un poco, que yo la quiero mucho pero también necesito mi espacio… bla bla bla…”

Y yo pensé (al principio sólo pensé, luego os diré lo que le contesté): o esta madre no se ha enterado de lo que es la adolescencia o no se ha parado a pensar la gran ventaja que supone que su hija, con esa edad quiera estar con ella, no para sobre protegerla, sino para tener la oportunidad de guiarla y acompañarla en esa edad tan difícil en la que lo normal es todo lo contrario, es decir, que quieran estar distanciados de los padres y que nos opongan bastante difícil el acercamiento.

Además, si una chica adolescente necesita estar tan pegada a su madre, creo que podría ser por algo importante, y habría que indagar en qué eso que le preocupa o qué le esta pasando para tener esa necesidad. Creo que lo último que debería preocupar a esa madre es si ella misma se siente agobiada por tener a su hija tan apegada.

## Contestación a madre de niña adolescente

Por eso le contesté que seguramente sería una fase, y que seguramente le quedará poco de tener a su hija tan cerca, que lo aprovechara porque a esas edades empezaban a volar.

 ![adolescente-moto-rosa](/tumblr_files/tumblr_pe26uzp0gh1qfhdaz_540.jpg)

(Lo sé, soy única haciendo amigas).

Yo lo veo como cuando el Principio se va ganando al zorro poco a poco y con paciencia infinita, hasta que logra que confíe en él y coma de su mano; Creo que los momentos de aproximación que un adolescente nos regala a los padres debemos tomarlos como tesoros, y mostrarnos lo más abiertos y empáticos posible para ayudarles si lo necesitan y demostrarles que pueden acudir a nosotros cada vez que quieran, aunque no siempre busquen consejo, sino sólo desahogo o poner en orden sus ideas hablando con nosotras.

 ![adolescente-oliendo-flores](/tumblr_files/tumblr_pe26uxrd7T1qfhdaz_540.jpg)

No sé si me pasé o me metí donde no me llamaban, pero me salió del alma, qué queréis que os diga. Es cierto que a veces soy muy impulsiva… ¿Qué pensáis?