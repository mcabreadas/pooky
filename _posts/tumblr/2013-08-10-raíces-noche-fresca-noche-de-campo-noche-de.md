---
layout: post
title: Mis raíces
date: 2013-08-10T21:44:00+02:00
image: /tumblr_files/tumblr_mrb6xqZkNg1qgfaqto1_1280.jpg
author: maria
tags:
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/57902031524/raíces-noche-fresca-noche-de-campo-noche-de
---


Noche fresca, noche de campo, noche de estrellas, noche de paseos, de risas de niños, de calor de familia… Noche de pueblo, de raíces y de los que ya no están pero los siento en el alma como cuando era niña y los veranos pasaban lentos entre girasoles, botijos y almendros.

\
 Cierro los ojos, respiro profundo y me inundan sus recuerdos. Tantos que no me caben; mondando lentejas, partiendo piñones, cogiendo higos, buscando níscalos… Siempre con un afán, nunca quieto, ese era mi abuelo.

\
 Él me inyectó en vena el amor por esta tierra dura. Dura, pero era la suya. 

\
 Ellos están más vivos aquí. La gente los recuerda, los nombra y me enorgullezco cuando me llaman “la nieta de…” Es como si siguieran estando… en cierto modo me reconforta estar aquí, siento que me envuelve este aire seco que respiraron y que piso los caminos salpicados de cantos que tantas veces recorrieron, que tantas veces recorrimos juntos de la mano, con los ojos de niña llenos de pinos, viñas, higueras…

\
 Qué suerte haber gozado esta tierra con mis abuelos. Imposible entenderla si no. Ellos le daban sentido, y hoy su esencia la impregna haciéndola bella, haciendo que quiera volver cada verano, como las golondrinas que anidan en el porche de su casa año tras año.

\
 Ellas también los vieron, y vuelan con su recuerdo.

\
 Os quiero, abuelos.