---
date: '2015-12-24T10:00:32+01:00'
image: /tumblr_files/tumblr_inline_o39yuiOLGc1qfhdaz_540.jpg
layout: post
tags:
- ad
- revistadeprensa
- colaboraciones
- familia
title: Entrevista Revista Federación Española de Familias Numerosas
tumblr_url: https://madrescabreadas.com/post/135836497695/revista-familias-numerosas
---

Comparto con vosotros un reportaje sobre mamás blogueras que ha publicado la revista de la [Federación Española de Familias Numerosas](http://www.familiasnumerosas.org/la-revista-de-las-familias-numerosas/), en su número de invierno, que incluye una entrevista que me hicieron hace unas semanas.

El título me encanta “El empoderamiento de las mamás blogueras”.

Me siento orgullosa de ser mamá, de ser bloguera, y de ser familia numerosa.

 ![](/tumblr_files/tumblr_inline_o39yuiOLGc1qfhdaz_540.jpg)

Gracias por darme voz!

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.