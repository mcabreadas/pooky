---
date: '2014-10-14T10:00:00+02:00'
image: /tumblr_files/tumblr_inline_pdpcmyk4lH1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- bebes
- premios
- colaboraciones
- puericultura
- ad
- recomendaciones
- familia
- juguetes
- moda
title: Sorteo muñecos para bebés Doudou Notsobig
tumblr_url: https://madrescabreadas.com/post/99981219506/sorteo-muñecos-para-bebés-doudou-notsobig
---

Os traigo un sorteo que es un primor. ¿Conocéis los Doudous? Yo los descubrí en la tienda on-line Les Petits Chéris y me encantaron. 

Son unos animalitos de tela con diferentes estampados ideales por delante, y con peluche suavecito por detrás para que a los bebés les encante tocarlos y jugar con ellos. Incluso a la hora de dormir creo que les puede ayudar a relajarse juguetear un ratito con ellos.   

 ![image](/tumblr_files/tumblr_inline_pdpcmyk4lH1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pdpcmz0UVL1qfhdaz_540.jpg)

He pensado probarlos con Leopardito, ahora que ha decidido dormir en su cama de mayor en la habitación de su hermano. Creo que le pueden encantar!

Además de estos originales y dulces muñequitos, en Les Petits Chéris tienen ropa de diseño para niños y niñas modernos de 0 a 5 años. Echad un vistazo a su [web](http://lespetitscheris.com/es/) porque os vais a enamorar, y a su recién estrenada revista gratuíta “Born” para padres modernos, que podéis descargar aquí.

 ![image](/tumblr_files/tumblr_inline_pdpcmz3IVy1qfhdaz_540.jpg)

**¿Qué sorteamos?**

Sorteamos dos kits “Funny Kids”, compuestos por 1 Doudou ouahouah + 1 porta chupete de la marca Notsobiga elegir entre estos cuatro.

 ![image](/tumblr_files/tumblr_inline_pdpcmzveG21qfhdaz_540.jpg)

¿A que son un primor?

**Cómo participar en el sorteo:**

1-Tenéis que dejar un comentario a este post diciendo qué estampado elegiríais de resultar ganadores: corazones, lunares, cuadritos o flores. Además de vuestro nick de Facebook o Twitter o un email de contacto.

2-Ser fans de la página de Facebook de Les Petit Chéris aquí.

3-Ser seguidores de Twitter de Les Petit Chéris aquí.

Si queréis podéis suscribiros a este blog en la columna de la derecha para estar al día de las publicaciones. Esto no es obligatorio para participar, pero podría ser divertido.

El sorteo se llevará a cabo mediante Sortea2 el **21 de octubre de 2014** , y el ganador o ganadora se publicará en este blog durante los días siguientes.

El ámbito del sorteo es el territorio español.

¿Te decides? ¿Cuál te gusta más? Participa!