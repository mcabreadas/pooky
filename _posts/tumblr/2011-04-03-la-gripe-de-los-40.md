---
date: '2011-04-03T22:22:00+02:00'
image: /images/posts/gripe/p-gripe.jpg
layout: post
tags:
- conciliacion
title: La gripe de los 40
tumblr_url: https://madrescabreadas.com/post/4319632035/la-gripe-de-los-40
---

Los que me seguís en twitter sabéis que he pasado toda la semana con gripe y, claro he podido dedicar más tiempo a este menester, lo cual me ha dado grandes satisfacciones, como debatir en directo con @angelesbrinon y con @aliachahin, conocer nuevas personas, y llegar a los 40 followers, lo cual me sorprende porque nunca lo pensé cuando empecé esta aventura de twitear (hace unos meses no sabía ni cómo se hacía, y ahora todavía estoy aprendiendo).

Además de por haber llegado a los 40 seguidores , me ha encantado la experiencia de la gripe, no por los 40 grados de temperatura, no… no soy masoca, sino porque he comprobado como la corresponsabilidad en mi hogar funciona a las mil maravillas, y eso sí que es un motivo de alegría. No sólo la casa no se ha hundido, sino que los niños han ido al colegio todos los días, y vestidos, peinados y aseados perfectamente, incluso la mayor fue a ballet con todo su equipo completo, hemos comido todos los días, la casa está más o menos limpia y la ropa… bueno, con la ropa ha habido un poco de caos, pero hemos sobrevivido.

Estoy muy contenta porque ahora sé que me puedo poner enferma y salir airosa! Por lo menos en el ámbito doméstico. En el trabajo es otro cantar, porque soy abogada por cuenta propia, y lo que no haga yo… hombre, siempre hay un compañero que te echa una mano, pero tampoco es cuestión de abusar. Oye, y qué casualidad, que los mayores “marrones” se presentan o cuando estás de viaje o cuando estás enferma; tuve que ir a firmar a una Notaría con fiebre oyendo al Sr. Notario hablar como a lo lejos en un sueño. Pero bueno, esto sería objeto de otro post.

El caso es que en el trabajo no me puedo poner enferma, todavía queda mucho por conseguir, sobre todo para los autónomos y trabajadores por cuenta propia, pero la noticia es que en el ámbito doméstico es posible que hombre y mujer se impliquen por igual y, no sólo se repartan las tareas, sino que cuando a uno le toca hacer, además, las del otro por cualquier motivo, es posible llevarlas a cabo con bastante solvencia. Vamos, que un hombre puede planchar, poner una lavadora, preparar la mochila de los niños, tender… en fin… todo. Lo que no podemos hacer es exigir al otro que lo haga igual que lo haríamos nosotros, es decir, que cada uno tiene su manera de hacer las cosas y hay que respetarla.

Por eso, no me queda más que agradecer a mi marido el ser como es, y a mis 40 followers que me hayan ayudado a sobrellevar los 40 grados de fiebre en esta “Gripe de los 40”.



*Photo by David Mao on Unsplash*