---
date: '2014-09-15T16:30:10+02:00'
image: /tumblr_files/tumblr_inline_nbuqrjvYun1qfhdaz.jpg
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: 'Receta de una merienda ideal para niños: horchata casera de chufa'
tumblr_url: https://madrescabreadas.com/post/97566492436/receta-ninos-horchata
---

Parece que el calor se resiste a abandonarnos, al menos por mi zona, así que viene bien una merienda fresquita y muy nutritiva que, además, podéis hacer con vuestros hijos fácilmente y pasar una tarde divertida.

La [horchata de chufa](http://es.wikipedia.org/wiki/Horchata_de_chufa) tiene su origen en La Comunidad Valenciana, aunque ya se usaba en el antiguo Egipto, habiéndose hallado vasos que contienen chufas como parte del ajuar funerario de los faraones. 

¿Os habéis preguntado alguna vez de dónde viene el nombre horchata? Por lo visto hay una leyenda que cuenta que una aldeana llevó al Rey Jaime I el conquistador un vaso de horchata explicándole que era leche de chufa, a lo que él contestó “esto no es leche, es **_oro, chata_** ”. De ahí lo de Hor-chata. 

Y no me extraña, porque [es considerada como un alimento completo](http://www.chufadevalencia.org/ver/19/Propiedades.html), ya que tiene propiedades digestivas muy saludables por su alto contenido en almidón y aminoácidos.

 ![image](/tumblr_files/tumblr_inline_nbuqrjvYun1qfhdaz.jpg)

Se trata de una bebida energética y nutritiva, de origen completamente vegetal y con propiedades cardiovasculares similares al aceite de oliva, contribuyendo a disminuir el colesterol y los triglicéridos, por su alto indice de ácido oleico

No contiene lactosa ni fructosa.

**Ingredientes**

-250 g de chufas

-1 L de agua

-Azúcar al gusto

 ![image](/tumblr_files/tumblr_inline_nbupjrRmrc1qfhdaz.jpg)

**Cómo se hace**

1-Se dejan las chufas a remojo durante una noche

2-Se baten las chufas con 1/4 L de agua

 ![image](/tumblr_files/tumblr_inline_nbuplq6EHZ1qfhdaz.jpg)

3-Se añade el resto del agua y se cuela con un colador de tela preferiblemente.

4-Se añade azúcar al gusto y se remueve. La cantidad es muy personal, a mí, personalmente no me gusta muy dulzona.

Recomiendo disolver el azúcar antes en un poco de agua templada para que sea más fácil.

Si estáis a dieta podéis hacerla con sacarina.

 ![image](/tumblr_files/tumblr_inline_nbuppwKu4Y1qfhdaz.jpg)

5-Meter en la nevera durante varias horas y un rato en el congelador justo antes de servirla. Está mucho más rica bien fresquita.

**Consejos**

Si se acompaña con fartons es una merienda mucho más completa, y se pueden mojar en la horchata. a los peques les encanta! Los de Mercadona están bastante buenos.

 ![image](/tumblr_files/tumblr_inline_nbuqk2OwEu1qfhdaz.jpg)

Recordad consumirla en el mismo día.

Gracias, Emilio Ruiz, por la receta y por dejarme espiarte mientras la hacías ;)

¿Qué me dices? ¿Te atreves a hacer oro, chata?