---
date: '2014-11-12T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_pdpclsK4zE1qfhdaz_540.jpg
layout: post
tags:
- crianza
- colaboraciones
- puericultura
- ad
- familia
- seguridad
- bebes
title: Desmontando mitos sobre seguridad vial infantil. Las sillas de coche a contramarcha.
tumblr_url: https://madrescabreadas.com/post/102437597573/sillas-automovil-seguridad-ninos
---

Tengo una buena noticia: las consecuencias graves de un accidente de tráfico en un niño menor de 4 años son evitables.

Así de rotunda se mostró Cristina Barroso, consultora de seguridad vial infantil en [Klippan](http://www.klippan.es/) al inicio de su charla sobre sistemas de retención para bebés y niños en automóviles a la que tuve la suerte de asistir en el MeetingMum organizado por las chicas de la web [Guía de Papás Novatos](http://guiaparapapasnovatos.wordpress.com/) el pasado 10 de noviembre.

Con esta afirmación logró captar el interés del público, en su mayoría padres y madres quienes, lógicamente, haríamos lo que fuera por proteger a nuestros hijos.

Éste fue el primer mito que nos desmontó.

**Silla de auto homologada no siempre es igual a silla de auto segura**

El segundo fue el de que una silla de automóvil homologada es equivalente a una silla segura. Y aquí fue donde se me cayeron los palos del sombraje porque no podía creer que haya llevado a mis niños toda la vida inseguros en el coche.

Seguro que a vosotros os pasa lo mismo, vamos a comprobarlo. De las siguentes imágenes decidme qué sistema creéis que es el único seguro.

 ![image](/tumblr_files/tumblr_inline_pdpclsK4zE1qfhdaz_540.jpg)

Solamente cuando un bebé/niño viaja a contra marcha se miniminiza casi absolutamente el riesgo de lesiones, y por supuesto, de muerte. Vamos, que si sientas a tu hijo al revés le puedes estar salvando la vida. Así de claro.

**Un poco de historia**

Pero entonces, ¿por qué no viaja ningún niño así? Para entenderlo tenemos que hacer un poco de historia y preguntarnos el objetivo con el que se inventaron las sillas de coche.

Tuvieron su origen en EEUU con el fin de que los niños no se subieran encima del conductor cuando empezaban a moverse. Por eso las primeras invenciones consistieron en esto:

 ![image](/tumblr_files/tumblr_inline_pdpcltPNj71qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pdpcltb9ke1qfhdaz_540.jpg)

El precursor de la silla con arnés:

 ![image](/tumblr_files/tumblr_inline_pdpclt9HcR1qfhdaz_540.jpg)

Con escudo de frente:

 ![image](/tumblr_files/tumblr_inline_pdpcluXI5J1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pdpclu0KIl1qfhdaz_540.jpg)

El objetivo en su origen no fue la seguridad de los menores, sino la comodidad de los padres a la hora de viajar con ellos en el coche. Así se entiende que no lo estemos haciendo bien. 

**¿Por qué la tasa de mortalidad en Suecia es 0?**

Bueno, a excepción de los suecos, que hace años que su tasa de mortalidad infantil en accidente de tráfico es casi 0 porque ninguna de las sillas anteriores, ni tampoco en su versión actual, se ha usado nunca en sus carreteras.

Suecia es líder en seguridad pasiva, es decir en los sistemas que lleva el automóvil para minimizar lesiones caso de accidente (la seguridad activa de un coche consistiría en los mecanismos que lleva para evitar accidentes)

Para desarrollar un sistema de retención verdaderamente seguro se preguntaron cómo proteger al adulto más frágil, y escogieron un astronauta.

 ![image](/tumblr_files/tumblr_inline_pdpcluEgVA1qfhdaz_540.jpg)

Y la coclusión fue, sentarlo en sentido contrario de la marcha, y si esto es lo más seguro para un adulto, para un niño lo será con más motivo porque su cabeza le supone un 30% del peso total de su cuerpo, además de no tener la musculatura adecuada para sujetarlo ante un movimiento brusco. Así, el respaldo de la silla le sujetará el cuello caso de impacto.

 ![image](/tumblr_files/tumblr_inline_pdpclvtpA91qfhdaz_540.jpg)

**Las pruebas de impacto**

Las pruebas de impacto que vimos en la charla lo dicen todo. Mientras en un impacto con la sillita colocada de frente el muñeco hace esto:

 ![image](/tumblr_files/tumblr_inline_pdpclvaWnm1qfhdaz_540.jpg)

Con una silla colocada en sentido contrario de la marcha, apenas se mueve la cabeza, sólo se encogen las piernas, y por muy brusco que sea esto, dado el caso, sería más fácil arreglar una pierna que un cuello.

Fijaos en estos datos de extensión del cuello con los distintos tipos de sillas consecuencia de un impacto porque hablan por sí solos:

 ![image](/tumblr_files/tumblr_inline_pdpclw0dxC1qfhdaz_540.jpg)

**Los miedos de los padres**

La verdad es que a las mamás y papás nos asaltaban dudas ante tal ruptura de esquemas mentales. Nos preocupaba que **se marearan al viajar de espaldas** , lo que parece ser que no se relaciona con el sentido de la marcha, según nos aclaró Cristina.

También nos preguntábamos si en el **caso de choque trasero** también es más seguro ir de espaldas a la marcha, y la respuesta es que sí porque la mayor parte del impacto lo absorbe el coche de atrás. Mirad la foto:

 ![image](/tumblr_files/tumblr_inline_pdpclwPXwD1qfhdaz_540.jpg)

Otra cosa que nos cuestionamos fue la comodidad de los niños al **no poder estirar las piernas** , y la verdad es que si pensamos que ellos casi nunca se sientan con las piernas estiradas y que es mejor llevarlas encogidas o cruzadas a lo indio que colgando, porque no les llegan los pies al suelo, salimos de dudas enseguida.

 ![image](/tumblr_files/tumblr_inline_pdpclw51xU1qfhdaz_540.jpg)

La **falta de visibilidad** tampoco debe preocuparnos porque tienen más visión por el cristal de atrás del coche que mirando las nucas de sus padres si miraran hacia delante.

En cuanto a la zozobra que nos causa el **no ver la cara de nuestros hijos** y vigilarlos, resulta más peligroso estar girándonos cada dos por tres para mirarlos, lo que nos distrae de la conducción, que instalar un espejo que nos permita tenerlos controlados con un solo movimiento de ojos sin necesidad de girar nuestro cuello.

 ![image](/tumblr_files/tumblr_inline_pdpcm0wE6g1qfhdaz_540.jpg)

A mí me convenció el sistema de contra marcha para las sillas de automóvil, no sé a vosotros.

 ![image](/tumblr_files/tumblr_inline_pdpcm0pLlP1qfhdaz_540.jpg)

Más información en [http://www.saquitodecanela.com/contramarcha-seguridad-vial-infantil](http://www.saquitodecanela.com/contramarcha-seguridad-vial-infantil)

¿Qué opináis?