---
layout: post
title: Tarta de la abuela
date: 2014-01-31T12:27:00+01:00
image: /tumblr_files/tumblr_n02v87GzRC1qgfaqto1_1280.jpg
author: .
tags:
  - recetas
  - planninos
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/75142246792/recetas-con-niños-tarta-de-la-abuela-no-os-voy-a
---

![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_n02v87GzRC1qgfaqto6_1280.jpg)  
  

## Recetas con niños: Tarta de la abuela

No os voy a contar la receta de la tarta de la abuela porque no soy yo muy cocinillas y seguro que me podéis dar lecciones a mí… Bueno… si queréis os explico cómo la hicimos Princesita y yo, pero no es eso lo que quería compartir con vosotros realmente, sino la ilusión que le hizo a mi niña preparar la tarta de su cumple conmigo.

Hasta ahora los ratos las dos a solas han escaseado bastante por la gran demanda de atención y cuidados que exige Leopardito, pero como cada vez se entretiene más con Osito, aprovechamos una tarde (abusando de Pocoyó, todo hay que decirlo), para emprender la aventura de la tarta de galletas que tantas veces me había pedido, y que nunca había tenido tiempo de hacer.

Parece increíble, pero los niños siempre dan ciento por uno. No os podéis imaginar cómo me lo agradeció y lo cariñosa que se muestra desde entonces. Incluso en el colegio se siente más segura porque nuestra relación se ha intensificado. Me cuenta más cosas y hemos creado una complicidad especial. Se ha abierto como una flor y expresa mucho más las cosas. Se siente muy contenta.  
 Muchas veces los problemillas que puedan sufrir los peques tienen una solución más sencilla de lo que creemos. Yo lo he comprobado. Nosotros, los padres, solemos tener la llave, sólo hay que pararse y escucharlos, pero no sólo con los oídos, sino con el corazón y ponernos en su lugar. Ellos suelen manifestar lo que necesitan, pero a veces nosotros, desde nuestro mundo de adultos no lo percibimos.

Bueno, y después de haberos contado las propiedades de esta tarta mágica os digo cómo la hicimos:

## Ingredientes de la tarta de la abuela

1. -1 paquete de galletas cuadradas María
2. -1 L y ½ de leche
3. -Canela
4. -1 sobre de polvos para hacer flan
5. -2 tabletas de chocolate para postres
6. -Cositas para poner por encima de adorno

## Cómo se hace la tarta de la abuela

- -Lo primero, se hace el flan, pero con 1/4 L más de leche del que pone en las instrucciones. Yo lo hice en el microondas, pero se puede hacer en el fuego en una olla.
- -Derretimos el 1 tableta de chocolate con un poco de leche en el microondas o en un cazo.
- -Mezclamos el chocolate con el flan antes de que se solidifique éste y removemos bien.
- -mientras vamos mojando las galletas en leche con canela, pero primero recomiendo tomarle el pulso a la galleta. Si vemos que empapa demasiado la mojaremos ligeramente, y si vemos que son de las que no empapan casi al principio, las dejaremos más rato a remojo. Esta el la clave de la tarta, porque en el punto medio está la virtud y, ojo, que hay galletas que engañan y parece que no absorben, y luego se te desintegran de repente. Si son galletas traicioneras, cuidado que pueden hacer que fracase la tarta.
- -Colocamos una primera capa de galletas en un recipiente cuadrado (ésta es la parte que más les gusta a los niños, bueno y guarrear con las galletas mojadas chorreándoles hasta el codo, también)
- -las cubrimos con el flan chocolateado que no esté ni muy líquido ni ya cuajado del todo.
- -ponemos otra capa de galletas, y las cubrimos con otra capa de flan, y así hasta que queramos o nos quede flan.
- -para la capa final o cobertura derretimos la otra pastilla de chocolate con un poco de leche, que puede mezclarse con una tableta de chocolate blanco para que quede más vistosa, y la extendemos con una cuchara.
- -Y ahora viene la parte más creativa para los peques: decorar la tarta. He de decir que Princesita abusó de las bolitas, corazones y fideos rosas y lilas, de manera que se emocionó y hasta que quedó uno en el bote estuvo poniendo. Pero quién soy yo para frenar su arte? Así que el resultado fue otra capa rosa/lila un poco dura de más muy consistente y un poco empalagosa que derrochaba dulzor.
- -Para terminar, tapa con film transparente y se deja 24 h en la nevera.

Ya veréis que está deliciosa, y la peque se lo pasó en grande!

Sueles cocinar con tus hijos?