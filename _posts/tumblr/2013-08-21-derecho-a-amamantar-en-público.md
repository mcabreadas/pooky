---
layout: post
title: "Derecho a amamantar en público: #AmamantarEnLibertad porque
  #AmamantarEsNormal .¿También en Primark?"
date: 2013-08-21T18:49:00+02:00
image: /tumblr_files/tumblr_inline_mrw3dbQ7WH1qz4rgp.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - derechos
  - lactancia
  - bebes
tumblr_url: https://madrescabreadas.com/post/58925611646/derecho-a-amamantar-en-público
---
De nuevo he comprobado la magia de las redes sociales, que hace que una madre corriente, aparentemente insignificante, logre movilizar al resto de mamás internautas del país:

El otro día me echaron de PRIMARK VALLADOLID por dar el pecho a mi hijo, diciéndome que podía incomodar a otros clientes.Vergonzoso.

Un solo tuit bastó para que [@KrmenssitaVega](https://twitter.com/KrmenssitaVega) revolucionara a las “familias lactantes” y las pusiera al pie del cañón. En este post nos cuenta ella misma cómo la echaron de Primark Valladolid cuando estaba amamantando a su bebé porque podía incomodar a otros clientes.

 ![image](/tumblr_files/tumblr_inline_mrw3dbQ7WH1qz4rgp.jpg)

Pero lo curioso es que, gracias a esta denuncia, han salido a la luz más casos en España: en febrero de 2012 le pasó algo similar a [una mujer en Primark Murcia](https://m.facebook.com/TenemosTetas/posts/340333966011952), además de otros dos casos más.

Por lo visto es algo habitual en dicha cadena de tiendas, ya que [en Reino Unido se vio obligada a pedir disculpas a una madre](http://www.bbc.co.uk/news/uk-wales-south-west-wales-11427171) por haberla expulsado de uno de sus locales por dar de mamar a su bebé. Cosa que aquí no ha sucedido, ya que Carmen no ha recibido noticia alguna de Primark, salvo un tuit de uno de sus empleados diciéndole que exagera.[](http://centromimame.com/blog/?p=180)

Aunque, he de decir que también he leído testimonios de madres que aseguran haber dado el pecho a sus hijos paseándose tranquilamente por una tienda Primark sin problema alguno.

Después de que Carmen Vega hiciera público el trato que recibió se produjo una reacción viral en Facebook, y se formó un [grupo](https://www.facebook.com/groups/153882781484312/) que en un día consiguió 600 miembros, cuyo objetivo es organizar la protesta. (Actualmente lo componen 786 personas).

La idea es que las madres y familias que quieran apoyar la normalización de la lactancia materna en público se reúnan en la puerta de las tiendas Primark de distintas ciudades de España, y hagan entrega de una reclamación. Lógicamente, si hay madres en periodo de lactancia con sus bebés, con toda la naturalidad del mundo lo amamantarán, como lo harían en cualquier otro momento.

[\#amamantaresnormal](https://twitter.com/search?q=%23amamantaresnormal&src=hash) ¡Y necesario!

Se trata de un acto simbólico para llamar la atención de que dar de mamar en público es normal y, como dice Eva Hache, necesario. Porque si no, no hay quien pueda sobrellevar la lactancia materna si tenemos que ir escondiéndonos por las esquinas y montar tenderetes con pañuelos para que no nos vean cada vez que el niño pide, o salir corriendo a buscar una sala de lactancia con la criatura colgando y berreando a más no poder porque los bebés no entienden de formalidades, pudores, horarios o políticas de empresa. Tienen hambre o sed y lloran porque lo quieren ya, ahora, en este preciso instante y lugar, sin pedir permiso y sin demora (instinto de supervivencia, que se llama).

Aquí encontraréis todas las convocatorias de las, llamadas coloquialmente, “tetadas” para el día 23 de agosto de 2013 en diferentes puntos de España por si queréis acudir, seáis lactantes o no.

También os dejo la [página web](http://tetadaprimark.wordpress.com/) creada al efecto, con nota de prensa incluída y contacto para cualquier aclaración o información.

Por supuesto se ha creado también una [cuenta de Twitter](https://twitter.com/tetadaprimark), y una [página de Facebook](https://www.facebook.com/tetadaprimark?fref=ts) para que estemos al día de las novedades.

Podéis colaborar también usando el Hastag en Twitter [\#AmamantarEsNormal](https://twitter.com/search?q=%23amamantaresnormal&src=hash) , con el que se ha conseguido ser Trending Topic esta misma mañana.

A raíz de esto la prensa digital empieza a hacerse eco de la movilización, pero todavía tenemos que hacer mucho más ruido!

Únete para que dar el pecho en público se considere algo normal!

 ![image](/tumblr_files/tumblr_inline_mrxg5hjndO1qz4rgp.jpg)

**ACTUALIZACIÓN 22-8-13**

Hoy se está tuiteando con el Hastag #AmamantarEnLibertad, puedes colaborar usándolo!

El País digital se hace eco de la noticia: [“Primark cabrea a las madres lactantes.”](http://smoda.elpais.com/articulos/primark-cabrea-a-las-madres-lactantes/3772)

**ACTUALIZACIÓN 23-8-13**

\-Hoy por fin **Primark se pronuncia sobre el suceso en un comunicado** , en el que lo califica de “malentendido”, e insiste en que dentro de su tienda el personal amablemente proporcionará un lugar privado para dar el pecho a las madres que deseen hacerlo, no libremente donde ellas decidan:

“*En Primark trabajamos cada día por ofrecer todo el confort y las facilidades posibles a nuestros clientes. Por lo tanto, lamentamos el malentendido que se ha generado sobre este tema. Nuestro personal estaba simplemente tratando de proporcionar información acerca de la disponibilidad de espacios especiales dentro del centro comercial para garantizar la seguridad y comodidad tanto de la madre como del niño. La política de Primark es bastante clara en este sentido: Primark no prohíbe a las madres lactantes dar el pecho. Cualquier cliente puede solicitar una zona tranquila y privada dentro de la tienda para amamantar a su hijo. Estamos seguros de que nuestro equipo hará todo lo que esté en su mano para atender tal solicitud. Por favor, no duden en preguntar al personal de tienda para lo que necesiten”.*

\-Beatriz Moreno es **otra mujer que fue invitada a abandonar una tienda Primark** en las Palmas de Gran Canaria por 3 guardias de seguridad. [](http://truquimoki.blogspot.com.es/2013/08/te-pensabas-que-me-habia-olvidado-de-ti.html)

\-Nayeli Fonseca nos cuenta en Facebook que fue expulsada por una mujer de seguridad en un Primark de Asturias al dar de manar a su pequeña porque daba “mala imagen a la tienda”. Ella lo relata así:

“quería contar mi experiencia ya que yo también fui discriminada. Estaba en la tienda, junto con mi hermana y mis dos niñas, cuando la pequeña que tenía dos meses empezó a llorar y como es lógico yo cogí a mi pequeña y de la forma más discreta, no se veía absolutamente nada, me puse a darle de comer. Yo seguía caminando por la tienda normalmente, en este espacio de tiempo no vi a nadie que se sintiera incómodo, ni note que estuviera montando ningún escándalo público,con algo tan natural como es amamantar. Cuando para mi sorpresa se me acerca una mujer de seguridad y me dice: ” por favor no estés aquí dando de comer a la niña porque te ve todo el mundo y da mala imagen a la tienda, sal afuera.” Yo me quede sin palabras la verdad,me pillo totalmente por sorpresa era lo último que me esperaba. Pero mi hermana no se calló!y le dijo:” ¿que la estas echando por dar de mamar a su hija?si no esta enseñando nada no se le ve nada”. La de seguridad lo reconoció que si que no se veía casi nada pero que eran normas de la empresa. Mi hermana se indignó todavía más y le dijo: “si pero a las que entran con unos escotes hasta el ombligo enseñando todas las tetas y faldas extra cortas enseñando el culo, a esas no les decís que dan mala imagen a la tienda, es una vergüenza! La chica de seguridad ya no supo que decir. En ese medio tiempo de discusión mi niña ya había terminado entonces la mujer no insistió en el tema! Seguimos en la tienda para terminar las compras y os podéis creer que nos estuvo siguiendo todo el tiempo. ¡Como si fuéramos unas delincuentes! ¡No vaya a ser que se le ocurra la tremenda locura de dar de comer otra vez a su hija de dos meses que no entiende de horarios ni de sitios! Por favor! En cuanto vi que nos estaba siguiendo nos marchamos de la tienda! Me quede con las ganas de poner una reclamación pero las niñas ya estaban cansadas y nos marchamos! Me parece estupenda esta iniciativa! No hay derecho a que hagan eso! Los derechos de los niños donde quedan?”

\-Numerosos medios digitales se hacen eco de la movilización:

\-Abc: “Madres lactantes contra Primark

\-[El Comercio: “Primark cabrea a las madres lac](http://www.elcomercio.es/20130822/mas-actualidad/sociedad/primark-cabrea-madres-lactantes-201308222117.html)[tantes”](http://www.elcomercio.es/20130822/mas-actualidad/sociedad/primark-cabrea-madres-lactantes-201308222117.html)

\-El diario.es

\-Selenus actuaiza su información a diario

\-[Hoy.es: “Primark enfada a las madres”](http://www.hoy.es/rc/20130823/mas-actualidad/sociedad/primark-enfada-madres-201308222038.html)

\-tercerainformacion.es: “Tetada de protesta de madres lactaqntes contra Primark”

\-Las televisiones empiezan a hacerse eco de las movilizaciones: Cuatro, La Sexta y Antena3

\-La blogosfera está que arde:

\-Sermamas

**NOTICIAS RELACIONADAS** :

\-[Prohiben a una mamá dar el pecho en una piscina municipal](http://www.elcorreo.com/vizcaya/20130705/local/erandio-madre-pecho-201307050801.html). Y posteriormente, una sentada pacífica con sus bebés a las puertas del recinto deportivo.

\-[Una azafata de American Air Lines obliga a taparse a una mamá que daba el pecho a su bebé en pleno vuelo](http://www.huffingtonpost.com/2013/08/07/american-airlines-breastfeeding-flight-attendant_n_3719655.html)

 ![image](/tumblr_files/tumblr_inline_mrzj8bFe5l1qz4rgp.jpg)