---
date: '2015-06-19T12:59:47+02:00'
image: /tumblr_files/tumblr_inline_nq6tqhZtcz1qfhdaz_540.jpg
layout: post
tags:
- crianza
- planninos
- colaboraciones
- puericultura
- ad
- familia
- juguetes
title: 'Te apuntas a las #ChiccoBabyOlimpiadas ?'
tumblr_url: https://madrescabreadas.com/post/121912306569/chicco-olimpiadas
---

Si hay un plan que me encante para los niños (de 3 a 5 años) es el que consista en divertirse haciendo deporte, así que si estáis en Madrid este fin de semana, aprovechad para llevar a las fieras a las Baby Olimpiadas, una iniciativa llevada a cabo por Chicco durante los próximos 19, 20 y 21 de junio en el Centro Comercial Parque Sur de Madrid, de 11:30 h a 21:30 h.

Ya sabéis la importancia de fomentar la actividad física de los niños desde los primeros años de vida para evitar el sedentarismo. Con este objetivo se organiza esta divertida actividad donde los más pequeños podrán disfrutar de todos los beneficios del deporte de la mano de los juguetes Chicco. 

A través de cada una de las actividades programadas, los pequeños desarrollarán sus capacidades motoras y habilidades, a la vez que se divierten y adquieren hábitos de vida saludables. 

 ![image](/tumblr_files/tumblr_inline_nq6tqhZtcz1qfhdaz_540.jpg)
## Cómo participar

Los niños participarán en pequeños grupos, realizando una serie de pruebas en base a las cuales irán sumando puntuación dependiendo de su destreza y habilidad. 

Podrán ganar un montón de premios!

En todo momento, un grupo de monitores acompañarán y guiarán a los pequeños a la hora de realizar las actividades.

## A qué van a jugar

Para estudiar las necesidades de los niños, Chicco ha desarrollado [El Observatorio Chicco](http://www.chicco.es/landing/Observatorio-Chicco.html), un centro de investigación dedicado en exclusiva al conocimiento del niño de 0 a 3 años de edad y sus necesidades psico-físicas, emocionales y sociales. 

Su objetivo es alcanzar un profundo conocimiento de la infancia para asesorar a los padres en el cuidado diario de sus hijos.

Como resultado de esta labor de investigación y experimentación, se han desarrollado los juguetes que usarán vuestros peques durante las Baby Olimpiadas, se trata de 5 juguetes diferentes adaptados a cada grupo de edad, comprendido **entre los 18 meses y los 5 años** : 

- **Mini Bowling** : La coordinación y la precisión serán necesarios para derribar los divertidos bolos de colores Monkey Strike 
- **Mini Liga Chicco** : Toda la diversión del deporte rey de la mano de la portería Fit&Fun Gol League para desarrollar la motricidad, la coordinación y la precisión. 
- **Mini Pesca** : La precisión y el equilibrio serán fundamentales para convertirse en el rey de la Isla de Pesca. 
- **Mini Golf Club** : Donde los pequeños desarrollarán su coordinación y precisión. 
- **Carril First Bike** : Para los fans de la velocidad y para ayudarles a mantener el equilibrio necesario para caminar sobre dos ruedas. 

Seguro que mi peque triunfaría en la Mini Liga de fútbol, ya que últimamente no para de practicar las chilenas, con sólo tres añitos, por imitación a su hermano mayor. Oye, y no le salen nada mal!.

Os apuntáis?