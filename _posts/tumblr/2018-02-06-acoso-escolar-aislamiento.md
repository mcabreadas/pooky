---
date: '2018-02-06T13:17:23+01:00'
image: /tumblr_files/tumblr_inline_n1e6bfE5Js1qfhdaz.jpg
layout: post
tags:
- adolescencia
- crianza
- educacion
- familia
title: Acoso escolar. El daño silencioso
tumblr_url: https://madrescabreadas.com/post/170571570689/acoso-escolar-aislamiento
---

Ha llegado a mis manos un testimonio de una madre que sufre casi más que su hijo por el [acoso escolar](/2017/03/26/bullying-acoso-escolar.html) o bullying. Con su permiso he intentado plasmar por escrito su experiencia porque, tanto ella como yo, queremos hacer visible de algún modo el drama que viven muchos niños y niñas cada día y que, aunque no se traduzca en moratones o golpes, deja unas heridas en el alma difíciles de superar.

Desde aquí, un abrazo a quienes estáis viviendo una situación parecida:

_[Teléfono contra el acoso escolar](https://www.mecd.gob.es/prensa-mecd/actualidad/2016/10/20161020-telf.html) 900 018 018_

 ![](/tumblr_files/tumblr_inline_n1e6bfE5Js1qfhdaz.jpg)

> “Estoy deseando recogerlo del colegio porque intuyo que está sufriendo… que algo va mal… o quizá no, quizá hoy tenga suerte y se olviden de él, de hacerle más daño, de hundirlo más… Pero no es un daño físico, no… ni tampoco evidente ante los ojos de los demás…. sino un daño que sólo nota él, un daño silencioso y amargo que sólo sufre su alma y también mis entrañas. Aquellas que lo sostuvieron durante 9 meses y que aún hoy lo siguen y seguirán sosteniendo mientras me quede un poco de fuerza.
> 
> Sé fuerte, le digo… y sé que lo es, que no se achanta… pero desea ser aceptado, ser uno más, o al menos, que no le impidan hacer nuevos amigos…
> 
> Cada día es una estratagema para impedir que se relacione con otros niños, a quienes atraen hacia el grupo fuerte para dejarlo aislado y pensando que es él el que falla… y seguramente tenga mucho que aprender, y a eso lo estamos ayudando, pero el camino de sufrimiento diario mientras aprende se le está haciendo demasiado duro… a él y a mí.
> 
> Aunque espero que hoy cambie algo… que alguien lo ayude… que alguien le tienda una mano, le deje un sacapuntas o lo elija cuando hagan equipos sin que se quede el último, como siempre, a pesar de ser el más rápido de la clase.
> 
> Pero todavía quedan dos horas… dos horas interminables para ir a buscarlo al colegio.”