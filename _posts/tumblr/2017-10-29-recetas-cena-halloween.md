---
date: '2017-10-29T22:24:30+01:00'
image: /tumblr_files/tumblr_inline_oylrsxpjIP1qfhdaz_540.png
layout: post
tags:
- recetas
- familia
title: Menú para la cena de Halloween
tumblr_url: https://madrescabreadas.com/post/166922759429/recetas-cena-halloween
---

Queramos o no, la costumbre de celebrar Halloween en España está ya tan extendida que no sólo no podemos escapar de ella, sino que estamos empezando a cogerle el gustillo.

Yo misma preparé el año pasado algunos de estos [dulces típicos de Halloween](/2015/10/27/dulces-halloween.html), a cuál más asqueroso.

Bueno, es que a mi me gusta más una fiesta que a un tonto un lápiz (permíteme la expresión), y si conlleva disfrazarse, pues apaga y vámonos… me apunto la primera!

Por eso este año he ideado este terrorífico menú para la cena de la noche de Halloween compuesto de un plato principal y un postre, que espero que te guste:

**Patatas de Halloween rellenas de arañas y bizcochos de la noche de los muertos**

## Patatas de Halloween rellenas de arañas

**Ingredientes**

1 patata grande por persona

½ lata de atún por persona

Mayonesa

Queso rallado

Sal

1 lata aceitunas negras

**Cómo se hace**

Lavamos y ponemos las patatas en un boll y lo tapamos con con film transparente.

Las ponemos en el microondas unos 10 minutos a máxima potencia.

Comprobamos si están cocidas.

Dependiendo de la clase de la patata, quizá debamos ponerlas 5 minutos más.

Una vez cocidas esperamos a que estén templadas, cortamos las patatas por la mitad y con una cuchara las vaciamos dejando un poquito de patata para que no quede la piel sola.

 ![](/tumblr_files/tumblr_inline_oylrsxpjIP1qfhdaz_540.png)

Con la mitad de la patata que hemos extraído aproximadamente hacemos un puré al que añadimos el atún, sal y Mayonesa haciendo una masa uniforme.

 ![](/tumblr_files/tumblr_inline_oylru6USB01qfhdaz_540.png)

Rellenamos cada patata con esta mezcla, y la cubrimos con queso rallado especial para gratinar.

 ![](/tumblr_files/tumblr_inline_oylrvcAg7M1qfhdaz_540.png)

Metemos al horno hasta que el queso se dore.

Un vez gratinado, sacamos y esperamos a que se enfríen un poco para colocar una arañas sobre cada mitad de patata.

Las aceitunas con forma de arañas se hacen con una mitad transversal de una aceituna negra para el cuerpo, y 8 tiras finas para las patas, según ves en la foto.

 ![](/tumblr_files/tumblr_inline_oylrwhqSOG1qfhdaz_540.png)

Ya ves que queda muy conseguido, y el plato les encanta a los niños!

 ![](/tumblr_files/tumblr_inline_oylrx5FCJ51qfhdaz_540.png)

También puedes rellenas las patatas con carne picada y tomate, con bacon y bechamel… o con lo que te apetezca!

## Bizcochos de la noche los muertos

Yo hice los [bizcochos con cacao](/2017/10/15/bizcocho-integral-de-cacao-puro.html) según la receta que ya te conté aquí, cuya tasa da para los dos.

 ![](/tumblr_files/tumblr_inline_oyls4ibur41qfhdaz_540.png)

 Usé un [molde de silicona de calavera de la noche de los muertos](http://amzn.to/2z15C6d) , y otro [molde para repostería con forma de cerebro](http://amzn.to/2zYwfa1).

 ![](/tumblr_files/tumblr_inline_p6m7ttbXuX1qfhdaz_540.gif)

Después les puse una cobertura de chocolate blanco y decoré.

Para la cobertura:

**Ingredientes**

260 g de [chocolate blanco de repostería](http://amzn.to/2xxB1d4)

150 g de nata para montar 35% materia grasa

**Cómo se hace**

Se calienta la nata en un cazo hasta que esté a punto de hervir.

Se apaga el fuego, se echa el chocolate blanco en onzas y se remueve hasta que esté derretido.

 ![](/tumblr_files/tumblr_inline_p6m7tu2sWk1qfhdaz_540.gif)

Se deja enfriar hasta unos 30 grados centígrados aproximadamente (se puede medir la temperatura con un [termómetro de cocina](http://amzn.to/2xwZO0W)), y se vierte sobre el bizcocho que habremos colocado sobre una rejilla para que escurra el sobrante.

![](/tumblr_files/tumblr_inline_p6m7tvjwkC1qfhdaz_540.gif)

A continuación metemos al frigorífico durante 10 minutos para que se endurezca un poco la cobertura.

 ![](/tumblr_files/tumblr_inline_oyls329boP1qfhdaz_540.png)

Sacamos y decoramos la calavera con los conguitos para poner los ojos y los dientes de la calavera.

 ![](/tumblr_files/tumblr_inline_oyls72dJBA1qfhdaz_540.png)

¡Realmente espeluznantes!

Y ya tenemos nuestro menú especial para la noche de Halloween con el que sorprender a todos!

Te atreves?