---
date: '2014-10-22T10:00:19+02:00'
image: /tumblr_files/tumblr_inline_paa8v3fmmf1qfhdaz_540.jpg
layout: post
tags:
- familia
title: El lugar de los hermanos en las familias numerosas
tumblr_url: https://madrescabreadas.com/post/100654227088/hermanos-familia-numerosa
---

Los niños se adaptan al medio, y al puesto que ocupan entre los hermanos que forman la familia. Tienen capacidad de adaptación, pero esto no significa que no lo pasen mal en determinados momentos hasta que logran hacerse un hueco, una identidad característica, hasta que encuentran su “papel” dentro de la familia. Por ejemplo, cuando el pequeño deja de serlo normalmente (aunque no en todos los casos) siente celos de su hermano menor que, aunque a veces, le duren toda la vida, no quiere decir que no se quieran con locura. Son relaciones curiosas las que se establecen entre los hermanos. A mí me resultan fascinantes.

## El rol de hermano o hermana mayor

en una familia grande es muy importante porque aunque no lo pretendamos, recae sobre él una gran responsabilidad con respecto de sus hermanos menores. No conozco ninguna familia numerosa en la que el hermano mayor no se comporte como protector y guía del resto. Son niños con una gran madurez y sensatez, que crecen más rápido que los demás, y que muestran una dedicación al resto de sus hermanos maravillosa. Son personas verdaderamente especiales.

![image](/tumblr_files/tumblr_inline_paa8v3fmmf1qfhdaz_540.jpg)

En casa tenemos cuidado porque nuestra niña mayor es muy respopnsable y perfeccionista, y a veces no nos damos cuenta, pero la cargamos con obligaciones que no son propias de su edad. Os pongo un ejemplo, que me hizo reflexionar:

El menor de mis hijos, cuando tenía 16 meses sufrió un accidente al caerle un mueble encima. Gracias a Dios no le pasó nada grave, pero su hermana rompió a llorar cuando llegamos a casa del hospital y vio los moratones. Entonces dijo que si ella hubiera estado con nosotros en el momento del golpe, lo hubiera estado vigilando, y no le hubiera pasado nada.

Inmediatamente la liberé de la culpa y le quité de la cabeza cualquier tipo de responsabilidad en lo sucedido ya que ella estaba en el cole, no con su hermano y, aunque hubiera estado, ella es pequeña y no es la responsable de cuidarlo y vigilarlo, aunque a veces mamá y papá le pidan el favor.

Pero es difícil no cargar a los mayores con demasiado, sobre todo si estamos cansados y el día a día nos va venciendo.

## El hermano menor

de una familia numerosa (en mi caso de tres) es el más privilegiado bajo mi punto de vista. Es cierto que tiene algunas desventajas, porque los mayores le toman el pelo, lo chinchan, le hacen rabiar… pero es el pequeño, y el mimado, y además tienen una capacidad asombrosa de desarrollar mecanismos de defensa contra pellizcos, tobas, tirones de pelo y todas las gamberradas del resto.

![#MiércolesMudo&#160;: compañeros de camino](/tumblr_files/tumblr_inline_paa8v3JS8q1qfhdaz_540.jpg)

![image](/tumblr_files/tumblr_inline_paa8v4aKCC1qfhdaz_540.jpg)

Son espabilados como ellos solos porque han sobrevivido en la jungla de una casa llena de niños gritando y corriendo mientras él estaba recién nacido e intentaba dormir o tomar el pecho. Han aprendido a comer enseguida porque mamá tenía que ocuparse de mil cosas a la vez que le daba de comer, y era más práctico tomar la cuchara y probar a metérsela en la boca ellos solos. Se han vestido, duchado y peinado a edad más temprana que el resto por imitación a sus hermanos, y están deseando salir de casa por las mañanas al cole con la tropa en vez de quedarse en casa con mamá.

No sólo tienen allanado el camino por los que han ido delante, sino que tienen un pase VIP para la vida, por no hablar de que sus padres tienen un master en crianza con varias prácticas incluídas (aunque cada uno es diferente, y nos seguimos equivocando), pero la torpeza con que nos comportamos con el primer retoño, el tercero ni la huele.

## El hermano mediano

de tres es quien más mérito tiene, según mi experiencia porque el mayor tiene su papel claro desde que nace, el menor es el pequeño y tiene un rol muy determinado sin casi esforzarse, pero el del medio… ese sí que se lo tiene que currar para captar la atención de papá y mamá o los abuelos y tíos. 

![image](/tumblr_files/tumblr_inline_paa8v4QlmZ1qfhdaz_540.jpg)

En nuestro caso, mi mediano es mi héroe. Ha desarrollado una personalidad fascinante que nos tiene a todos prendados. Destaca por sí mismo, a pesar de estar en tierra de nadie. Brilla con luz propia porque tiene un carisma que a nadie deja indiferente.

Es cierto que algo de esa personalidad llevaba innata, y que la crianza con apego que recibió desde que nació le ha ayudado a desarrollar seguridad en sí mismo, empatía, y mostrarse cariñoso con los demás, pero estoy segura que la necesidad de captar su cuota de atención dentro de la familia por encima de su hermana mayor, y del pequeño, ha hecho que sea un niño excepcional que me tiene totalmente enamorada (y no es pasión de madre… bueno, sí, ¿y qué?).

Me encantaría que siempre durara ese cariño y complicidad que se existe entre los tres…

![image](/tumblr_files/tumblr_inline_paa8v5W8Hi1qfhdaz_540.jpg)

¿En el caso de tu familia, ha afectado a la personalidad de tus hijos el lugar o puesto que ocupan entre los hermanos?