---
date: '2016-03-14T09:33:52+01:00'
image: /tumblr_files/tumblr_inline_o40rxhseym1qfhdaz_540.jpg
layout: post
tags:
- trucos
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: ¿Viajas esta Semana Santa? Claves para elegir maleta
tumblr_url: https://madrescabreadas.com/post/141022272054/comprar-maleta-viajar
---

Esta Semana Santa, que este año se nos adelanta, los “españolitos”, como decían los Mecano, nos vamos de vacaciones buscando desconectar de la rutina, descansar, y recargar la pilas.

Serán más o menos días, pero seguro que todos tenemos una escapada en mente para estas fechas porque nos lo merecemos, ¿o no?.

A nosotros se nos plantea el dilema de elegir una maleta o maletas para viajar todos, por eso te voy a contar mis pesquisas y la que yo considero la mejor opción para el equipaje de la familia.

Antiguamente no había tanta opción, y no se complicaban la vida, pero ahora, con la variedad de maletas que hay en el mercado nos podemos volver locos para encontrar la mejor opción para nuestro caso.

 ![](/tumblr_files/tumblr_inline_o40rxhseym1qfhdaz_540.jpg)

Grandes, medianas, pequeñas, duras, blandas, semi duras, con ruedas, sin ruedas… 

¿Cuál escojo? 

¿Compro una gigante para todos, o una pequeña para cada uno?

Las opciones son casi infinitas, por eso intentaré dejarte claro qué cosas tienes que plantearte antes de tomar tu decisión de compra, ya que merece la pena invertir en unas buenas maletas porque te tienen que hacer un buen papel durante muchos años proporcionándoos comodidad y movilidad 

## Duración de los viajes que soláis hacer

A ver, sé realista. Yo también me imagino en las Bahamas en cuanto me nombran la palabra vacaciones, pero no por ello me voy a comprar una maleta gigante por si alguna vez cumplo mi sueño.

Echa la vista atrás y piensa con frialdad dónde habéis ido desde que tenéis niños, que seguramente hayan sido más bien escapadas de unos días a ver a la familia o viajes no muy largos…

Si es así, lo ideal es haceros con una maleta mediana para todos.

Si, por el contrario, soléis viajar más de una semana, entonces podéis completar la mediana (para vosotros) con una pequeña (para la ropa de los niños).

Hay quien viaja con una grande para todos, pero yo lo desaconsejo por experiencia propia, porque son armatostes de muy difícil manejo, y te condiciona mucho a la hora de trasladarla y acoplarla tanto en el medio de transporte que utilicéis.

Tampoco me gusta la idea de tantas maletas como personas, y que cada uno lleve la suya porque creo que se desperdicia espacio y se multiplica la dificultad para encajarlas en el medio de transporte.

## Medio de transporte

Si viajáis en avión, lo ideal es haceros con una maleta mediana para facturar, más una pequeña de cabina, pero ojo con [algunas compañías lowcost, que sólo admiten unas medidas especiales](http://www.triandgo.com/es/blog/medidas-maletas-avion/).

También será conveniente que sea rígida para evitar deformaciones en la maleta, y desperfectos e los enseres que contenga, ya que viajará en la bodega del avión con otro montón encima, y probablemente también sufrirá golpes.

La ruedas se hacen imprescindibles en estos casos, y también si viajáis en tren o en autobús, porque los paseos por la terminal o estación se os harán más llevaderos.

Sin embargo, si viajáis en coche, quizá quepan mejor las maletas sin ruedas, dependiendo de la capacidad del maletero, y blandas, porque se adaptan mejor.

## Número de viajeros

Si logras que alguien se quede con los niños (aprovecha si tienes uno o dos, porque con tres ya nadie quiere), y te vas de escapada romántica de fin de semana con tu pareja, con un par de mochilas, o una maleta pequeña tendrás suficiente.

Si os vais con los niños un fin de semana, con una maleta pequeña y una mochila, si optimizas el espacio, no necesitas más.

Si os vais una semana todos, lo mejor es, como te decía antes, una mediana para los adultos y una pequeña para los niños que, seguro que se pelearán por llevarla si tiene ruedas.

## Época del año

Lógicamente, en verano se necesita menos ropa, y menos voluminosa, con lo que necesitarás menos espacio, y te apañarás muchas veces con la pequeña.

En invierno, aunque los abrigos los llevéis en la mano, la ropa abulta mucho más, y necesitaréis más de un bulto.

## Maleta rígida vs. blanda

Cada una tiene sus ventajas e inconvenientes.

Las rígidas son ideales para viajar en avión, como te he comentado para evitar desperfectos, y si tienen llave, mejor para evitar sorpresas de robos.

Pero las blandas suelen tener más capacidad al ser más flexibles e incluso algunas llevan una cremallera que permite ampliar el espacio, y se adaptan mejor a maleteros de los coches.

## Ruedas vs. tradicional

Creo que aquí todos tenemos claro que la rueda fue un gran invento que nos hace más llevadero el traslado del equipaje. 

En este punto hubo una curiosa [evolución de la maleta](http://www.maletasviajeras.com/blog/la-increible-evolucion-de-la-maleta/) desde las iniciales dos ruedas un tanto inestables, hasta las cuatro ruedas que proporcionaros estabilidad, para acabar en la cuatro ruedas que se deslizan en todas las direcciones.

## El interior

Tan importante es el interior como el exterior de la maleta.

Lo ideal es que esté forrada y separados los dos departamentos principales, y si tiene algún bolsillo para zapatos, mejor.

 ![](/tumblr_files/tumblr_inline_o40s6vtVgZ1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o40s8qcy1M1qfhdaz_540.jpg)

## Nuestra elección

Nosotros hemos optado por estas dos maletas, una [mediana de 72 cm](http://www.amazon.es/gp/product/B00WIJ6ZNW/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00WIJ6ZNW&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00WIJ6ZNW) y otra [pequeña de 60 cm](http://www.amazon.es/gp/product/B01BCKXJXQ/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01BCKXJXQ&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B01BCKXJXQ), rígidas y con ruedas porque creemos que es la opción más versátil que nos permite ir combinando tanto si hacemos viaje corto todos o en pareja, como si hacemos viaje largo todos, aunque tengamos que llevar cada uno una mochila.

 ![](/tumblr_files/tumblr_inline_o40skg2Tki1qfhdaz_540.jpg)

Hemos elegido una buena marca que garantice calidad para que nos duren muchos años como es [Lois](http://www.loisjeans.com/es/ES/home), y además, nos encanta el diseño con un guiño a los años cincuenta, cuando el progreso empezó a permitir vacaciones exóticas, y los turistas empezaron a forrar sus maletas con con pegatinas de los lugares visitados. Cuantas más mejor y no podía faltar un “Aloha from Hawaii”.

 ![](/tumblr_files/tumblr_inline_o40sqyhNyN1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o40sr2uIRO1qfhdaz_540.jpg)

Sea como sea, y elijas la maleta que elijas, lo importante es seguir viajando en familia porque en cada viaje que hagáis los niños aprenderán más que en una semana de cole, y vivirán experiencias que no olvidarán.

¿Te vas de viaje? ¿Tienes ya la maleta lista?

_Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz :) _