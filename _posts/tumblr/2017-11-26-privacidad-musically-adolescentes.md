---
date: '2017-11-26T22:07:08+01:00'
image: /tumblr_files/tumblr_inline_p01kq0njMf1qfhdaz_540.png
layout: post
tags:
- adolescencia
- tecnologia
title: Cómo configurar Musical.ly de forma segura  para los adolescentes
tumblr_url: https://madrescabreadas.com/post/167916042704/privacidad-musically-adolescentes
---

ACTUALIZACIÓN

Musically ahora se llama Tik Tok

De unos meses a esta parte se ha extendido como la pólvora la Aplicación Musical.ly entre los adolescentes y preadolescentes españoles. Ya llevaba tiempo dando guerra en Estados Unidos, pero ahora ha dado el salto al otro lado del charco, con España a la cabeza por número de usuarios, o musers, como se hacen llamar los miles de jóvenes que se auto graban videos de 15 segundos haciendo playback y coreografías varias para compartirlas con sus amigos. 

 ![](/tumblr_files/tumblr_inline_p01kq0njMf1qfhdaz_540.png)

Pero, ojo, estos videos permanecen en el perfil de usuario, no desaparecen, como en el caso de la red social [Snapchat](https://es.wikipedia.org/wiki/Snapchat).

La gran estrella de Musical.ly es Baby Ariel, una joven de Florida, con más de 10.000.000 de seguidores

Los  público se compone de chicos y, sobre todo, chicas de 9 a 16 años, aunque al entrar les hacen manifestar su edad, y según la propia aplicación, no es apta para menores de 12 años.

 ![](/tumblr_files/tumblr_inline_p01kpc8IBS1qfhdaz_540.png)
## Por qué la instalamos

Yo la conocí por Princesita, mi hija mayor, que me pidió en varias ocasiones que se la instalara, pero como en casa tenemos el uso de móvil y tablet muy restringido, y de momento no usan redes sociales, me negué en varias ocasiones a hacerlo, hasta que me picó la curiosidad ante tanta insistencia, y me decidí a probarla en mi móvil en secreto…

Una tarde me sorprendió en mi cuarto gesticulando y haciendo playback con el móvil en la mano cuando creía que nadie me veía, y me preguntó entusiasmada:

> “¿Te has instalado Musical.ly?”

Me pilló de lleno! Y ya tuve que tomar una decisión. 

## Opciones de privacidad 

Juntas estuvimos mirando las opciones de privacidad, y decidimos probarla unos días en mi teléfono para asegurarnos de que comprendíamos bien el funcionamiento ya que, de momento, la cuenta sería privada, de modo que sólo quien nosotras aceptáramos podría ver nuestros videos.

 ![](/tumblr_files/tumblr_inline_p01krxv48V1qfhdaz_540.png)

También configuramos las notificaciones para que no nos avisaran de publicaciones de gente a la que no seguimos, como videos en directo (live.ly) u otro tipo de acvtividad de desconocidos.

 ![](/tumblr_files/tumblr_inline_p01kuts94r1qfhdaz_540.png)
## Mejor en un entorno controlado

Mi idea es que, de momento, todo quede en familia y amigos más cercanos, de manera que compartimos los videos sólo con ese círculo, sin que nadie externo pueda verlos ni mandarnos mensajes (direct.ly).

 ![](/tumblr_files/tumblr_inline_p01l05tNkC1qfhdaz_540.png)

La idea de Musucal.ly es muy atractiva, y los niños se lo pasan muy bien con la actividad en sí de grabarse haciendo playback con canciones de moda y coreografías, sobre todo con las manos, pero lo que no me gusta es la vertiente social de exposición que tiene, sobre todo para los niños más pequeños, pero creo que les puede servir para ir aprendiendo con nuestra ayuda a comprender cómo funciona y manejar una red social, configurar las opciones de privacidad y ser conscientes de su exposición.

Por eso cada uno tenemos nuestra cuenta, y podemos escribirnos mensajes y darnos likes. Además, nos hemos hecho BBF (Best Fan Forever)!

## Aprovecha para educar en el uso de rrss

Mi intención es educar a mis hijos en el uso consciente y responsable de las redes sociales, para lo que no veo buena idea la prohibición, sino que prefiero empezar con ellos (ahora que me dejan), e ir soltando cuerda poco a poco hasta que llegue un punto en que estén preparados para volar.

## ¿Es recomendable la red social Musical.ly para adolescentes?

Mi opinión es que la actividad que ofrece me parece muy divertida y estimulante para ellos, pero la vertiente social debe estar supervisada por los padres y mantenerla, al menos en edades tempranas, en un entorno controlado.

Pero cuidado… si la pruebas te vas a enganchar!

<iframe width="100%" height="315" src="https://www.youtube.com/embed/0vNsCtuRnbo?rel=0" frameborder="0" allowfullscreen></iframe>

No sé si tú cómo afrontas el tema de las redes sociales con tus hijos. ¿Se las prohibes? ¿Les das libertad total? ¿Los controlas de alguna manera?

Si quieres saber más sobre cómo proteger a tus hijos en el uso de las nuevas tecnologías, te recomiendo la [web IS4K, que te expliqué en este otro post](/2017/04/27/seguridad-internet-menores-incibe.html).