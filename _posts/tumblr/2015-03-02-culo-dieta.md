---
date: '2015-03-02T16:40:54+01:00'
layout: post
tags:
- mecabrea
- humor
title: Ha llegado el buen tiempo...Y yo con este culo!
tumblr_url: https://madrescabreadas.com/post/112517321049/culo-dieta
---

Esto es una tragedia! Hasta ayer mismo iba tan mona con mis leggins, mis jerseys largos, mi abrigo que, si bien costaba un poquito abrochar, no se notaba casi…

 Pero hoy, al ir a recoger a los niños al cole, la tragedia se iba mascando por el camino, cuando las gotas de sudor me iban recorriendo la cara, y sabía que la solución más sencilla sería quitarme el abrigo, pero no me atrevía.

No sé por qué en invierno me relajo, como más chocolate, pan, guisos contundentes, buenos cocidos… y me olvido de la báscula, la verdad. Y si engordas un poco  suele pasar bastante desapercibido porque no te estás viendo las carnes todo el rato… y cuando te las ves, pues haces como que no, y pasas de refilón por delante del espejo del baño o, como está empañado, pues prefieres creer que es una ilusión óptica…

Pero no. Cuando los primeros calores te hacen despojarte de la ropa que camuflaba tus excesos, entonces los cristales de los escaparates de la calle te delatan… y te preguntas entre sorprendida y escandalizada por lo indecente:

¿Y ese culo?

¿De dónde ha salido?

Entonces vienen las madres mías, los propósitos de gimnasio para los que no tengo tiempo, de dietas imposibles porque, no nos engañemos, cuando se está criando no se puede hacer dieta, al menos yo, porque si la hago no me da la energía. Y, eliminar el chocolate no se plantea, porque para un placer instantáneo que tiene una cuando le apetece, aunque esté rodeada de fieras gritando y con una pila de ropa para lavar ante sus ojos, sería muy cruel quitárselo.

Así que, de momento, mi culo y yo aquí nos quedamos, nos lo tomaremos con calma, y ya le iremos dando salida conforme se vaya pudiendo.

¿Tienes ya buen tiempo?

¿Te ha pasado?

_Foto gracias a [http://www.fotoshumor.com/](http://www.fotoshumor.com/)_