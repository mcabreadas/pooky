---
date: '2015-11-27T07:53:48+01:00'
image: /tumblr_files/tumblr_inline_nygohlVyb41qfhdaz_540.png
layout: post
tags:
- trucos
- colaboraciones
- ad
- recomendaciones
- moda
title: El Black friday llega a España con grandes descuentos
tumblr_url: https://madrescabreadas.com/post/134045456874/black-friday-espana
---

Estos días estamos viendo cantidad de marcas de ropa, calzado, deporte, juguetes, regalos… que bajan sus precios con motivo del Black Friday, que en algunos casos, no se reduce sólo al viernes 27 de noviembre, sino que se extiende a toda la semana.

He recopilado una serie de descuentos que me han parecido interesantes, pero hay muchos más, y otros que están sin concretar porque las tiendas se esperan al último día para anunciarlo para jugar con el efecto sorpresa.

Yo os doy un consejo. 

Haced una lista de lo que necesitáis, y no compréis a lo loco. Incluso podés aprovechar para haceros con los regalos de Papá Noel o Reyes Magos de toda la familia porque es una buena oportunidad.

## Family & Friends
 ![](/tumblr_files/tumblr_inline_nygohlVyb41qfhdaz_540.png)
##   

## Zippy
 ![](/tumblr_files/tumblr_inline_nygohmPs4k1qfhdaz_540.jpg)
##   

## [Tutete.com](https://www.tutete.com/tienda/)
 ![](/tumblr_files/tumblr_inline_nygohm0sgX1qfhdaz_540.jpg)
##   

## [What´s Up! Kids](http://www.shopwspkids.com/)

CÓDIGO DESCUENTO : BLACK

DESCUENTO DEL 50% EN TODA LA TEMPORADA OTOÑO INVIERNO

Disponible hasta el domingo 29 de Noviembre

 ![](/tumblr_files/tumblr_inline_nygohmFCTU1qfhdaz_540.jpg)
##   

## [Alondra Outlet](http://is.cinco.purlsmail.com/sendlink.asp?HitID=1448019603466&StID=8699&SID=7&NID=375638&EmID=30909187&Link=aHR0cDovL3d3dy5hbG9uZHJhb3V0bGV0LmNvbQ==&token=d64d6cb8a69a65f40ff144eb9ed1f0078a61387b)
 ![](/tumblr_files/tumblr_inline_nygohm3GmS1qfhdaz_540.jpg)
##   

## Decathlon 
 ![](/tumblr_files/tumblr_inline_nygohntTLL1qfhdaz_540.jpg)
##   

## Toys R´us
 ![](/tumblr_files/tumblr_inline_nygohn4MJy1qfhdaz_540.jpg)
##   

## Carrefour
 ![](/tumblr_files/tumblr_inline_nygohnztg21qfhdaz_540.jpg)
##   

## Amazon

del 23 al 30 de diciembre habrá 3.500 productos en oferta. Cada día habrá productos en oferta pero a medida que se acerque el 27 habrá muchos más productos (el 27 habrá 25 nuevas ofertas cada 10 minutos, 2.300 en total ese día).

 ![](/tumblr_files/tumblr_inline_nygohn1Z8v1qfhdaz_540.jpg)
##   

## Ikea
 ![](/tumblr_files/tumblr_inline_nygohon9oq1qfhdaz_540.jpg)
##   

## Ópticas Affelou
 ![](/tumblr_files/tumblr_inline_nygohobMlx1qfhdaz_540.jpg)

¿Qué os ha parecido? si tenéis más descuentos, por favor decírmelo y los añado.