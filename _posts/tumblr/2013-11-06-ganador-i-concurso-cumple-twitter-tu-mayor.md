---
date: '2013-11-06T22:28:53+01:00'
image: /tumblr_files/tumblr_inline_mvuauje8UH1qfhdaz.jpg
layout: post
tags:
- premios
- trucos
- recomendaciones
title: 'Ganador I Concurso Cumple Twitter: "Tu mayor locura"'
tumblr_url: https://madrescabreadas.com/post/66215131261/ganador-i-concurso-cumple-twitter-tu-mayor
---

Muchas gracias por los buenos ratos que me habéis hecho pasar los que habéis participado en el [I Concurso](/post/65320768035/triple-concurso-tercer-cumpletwitter-tu-mayor-locura) para celebrar mi tercer Cumple Twitter: “Tu mayor locura”.

En realidad era una excusa para echarnos unas risas recordando nuestras noches de fiesta con los amigos y amigas… Qué tiempos!  
Lo he pasado genial leyendo vuestras historias. Gracias a todos por haber participado!

La verdad es que quien lea las locuras que habéis escrito comprenderá lo difícil que me ha sido decidirme por una, pero como soy una romántica incorregible, me ha conquistado la historia de [@aquicabedetodo](https://twitter.com/aquicabedetodo) , y la intentona que hizo con sus amigos de rondar a una chica haciéndose pasar por una tuna, con capa incluida…No debieron de dar ni una nota porque poco menos que los confunden con Batman, jajajaja…

Así qué el DVD, Blu-ray y copia digital de la peli R3SACON (la tercera parte de Resacón en las Vegas) va para él, que tiene 24 h para facilitarme su e mail por privado y darme los datos de envío dentro del territorio nacional.

![image](/tumblr_files/tumblr_inline_mvuauje8UH1qfhdaz.jpg)

Sin embargo, no puedo menos que hacer una mención de honor a [@mamanenufar](https://twitter.com/mamanenufar) , y su espectaCULO con intervención de la benemérita incluída, por habernos hecho pasar un buen rato de risas, a quien enviaré una sorpresita.

Si os apetece leer las hazañas nocturnas de los participantes, podéis hacerlo en los comentarios del post que anunciaba el concurso: [http://tmblr.co/ZeZnqxzgkhXz](http://tmblr.co/ZeZnqxzgkhXz)

Pero no se vayan todavía, aún hay más! Seguimos de celebración durante todo el mes de noviembre! En breve la segunda entrega de los 3 concursos que tengo preparados, esta vez, con la peli El Hombre de Acero”, o sea, Supermán, pero a lo moderno ;)

Os espero!