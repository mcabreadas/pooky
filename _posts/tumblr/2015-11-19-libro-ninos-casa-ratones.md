---
date: '2015-11-19T10:00:33+01:00'
image: /tumblr_files/tumblr_inline_o39z3a25dT1qfhdaz_540.jpg
layout: post
tags:
- trucos
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: 'Libros para niños: La Casa de los Ratones'
tumblr_url: https://madrescabreadas.com/post/133520453442/libro-ninos-casa-ratones
---

Mirad qué maravilla de libro ha caído en mis manos. Se trata de [La casa de los ratones](http://www.boolino.es/es/libros-cuentos/la-casa-de-los-ratones/),  la historia de los ratones que viven en una casa de ratones parecida a las casas de muñecas antiguas, hecha a mano por la autora, Karina Schaapman, con cajas de zapatos, papel maché y mucha mucha imaginación.

 ![libro ratones](/tumblr_files/tumblr_inline_o39z3a25dT1qfhdaz_540.jpg)

Cada habitación cuenta una historia, y cada detalle invita a perder la noción del tiempo observándolo.

A los niños les encanta descubrir objetos cotidianos en miniatura, muchos de ellos hechos con materiales reciclados. Por ejemplo, las lámparas están hechas con tapones de botellas, y las botellas son las pequeñas bombillas de los faros de las bicicletas. El suelo es de parquet hecho con palos de polo, y en el almacén hay troncos de madera, que son corchos de botellas.

Lo que empezó con una caja de zapatos y una cunita, se ha convertido en toda una obra de arte con más de cien habitaciones, a cual más ideal. Y, lo que más me asombra, es que da la sensación de que la casa realmente está habitada.

La edición, de tapa dura, es una maravilla, y las fotografías que ilustran la historia de a Sam y Julia, los ratoncitos protagonistas, en diversas situaciones en las diferentes estancias de la casa son una pasada. 

Me parece super divertida la lavandería, en la que una vez hicieron una trastada y la llenaron de espuma.

La habitación del músico es ideal, con piano y todo, y con discos de vinilo por todas partes.

 ![ninos mirando libro](/tumblr_files/tumblr_inline_o39z3aRNJu1qfhdaz_540.jpg)

Mis hijos pasan el tiempo muerto mirando las fotos, que son una maravilla.

La edición es preciosa, ideal para hacer un regalo para alguien muy especial.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.