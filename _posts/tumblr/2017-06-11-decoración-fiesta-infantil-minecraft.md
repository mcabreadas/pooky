---
date: '2017-06-11T18:47:14+02:00'
image: /tumblr_files/tumblr_inline_ore6mg2abN1qfhdaz_540.png
layout: post
tags:
- trucos
- decoracion-fiestas
- planninos
- comunion
- familia
title: Decoración fiesta infantil Minecraft
tumblr_url: https://madrescabreadas.com/post/161700318964/decoración-fiesta-infantil-minecraft
---

Mi amiga, la misma que me pasó la [receta del tiramisú fácil](https://madrescabreadas.com/2015/03/27/recetas-ninos-tiramisu/) que compartí hace tiempo, es una mina. Mirad qué decoración hizo para la Comunión de su hijo amante de Minecraft. Advierto que es una pasada, y que llevarlo a cabo tal y como se muestra en las fotos puede perjudicar seriamente la salud porque es simplemente todo lo que un niño Minecraft podría soñar.

Ella ha tenido la generosidad de pasarme las fotos de su obra y los imprimibles necesarios para llevar a cabo todas las ideas que nos propone, que os podrán servir para cualquier cumpleaños o fiesta infantil en la que queráis sorprender a vuestros pequeños frikis del mundo Minecraft.

No hace falta que lo hagáis todo porque es imposible. Algunas de estas ideas bastarán para conseguir una fiesta especial.

Al final del post tienes todos los imprimibles necesarios que podrás descargarte.

## La tipografía

Lo primero que tienes que hacer es bajarte la [letra “minecraftia”](http://www.dafont.com/es/minecraftia.font) para ver los formatos tal cual quedan impresos. Si no la tienes se ve en otra tipografía y lo mismo queda raro.

Las cabezas con cajas de cartón

Los archivos creeper 1,2… y Steve 1, 2… que encontrarás al final del post son para hacer las cabezas con una caja y que los niños metan su cabeza dentro. Se quedan genial! 

Se pueden poner en la mesa dulce en principio:

 ![](/tumblr_files/tumblr_inline_ore6mg2abN1qfhdaz_540.png)

Y después las pueden coger para el photocall: 

 ![](/tumblr_files/tumblr_inline_ore6mhSJon1qfhdaz_540.png)
## Su nombre con letras Minecraft

Se puede encargar en algunas webs o papelerías una [impresión del nombre del niño en 3D en porexpan](https://www.muralesyvinilos.com/letras-decorativas/personalizadas) con la letra de minecraft (no la minecraftia, que es la que se usa en el juego para las minúsculas, sino las mayúsculas que son como letras grises rotas). 

 ![](/tumblr_files/tumblr_inline_ore6miYzsA1qfhdaz_540.png)

Luego lo puedes pintar de gris con pintura al agua (cualquier otra se come el porexpan).

Las [espadas Minecraft](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_tl?ie=UTF8&tag=madrescabread-21&keywords=espadas%20minecraft&index=aps&camp=3638&creative=24630&linkCode=ur2&linkId=0e99f876e619d3ee6f1f59d3a355d34e) para decorar la pared le dan un toque auténtico.

## La mesa dulce
 ![](/tumblr_files/tumblr_inline_ore6mjH6tj1qfhdaz_540.png)

Las chuches se ponen en bandejas con bordes transparentes con el fondo con goma E.V.A. verde y se pega en la parte de atrás un marco con la imagen de la chuche que sea. 

El **oro** son monedas de chocolate y la obsidiana mini oreo: 

 ![](/tumblr_files/tumblr_inline_ore6mkT9Cb1qfhdaz_540.png)

El **carbón** son trufas: 

 ![](/tumblr_files/tumblr_inline_ore6ml5fM21qfhdaz_540.png)

Los **diamantes** son moras azules:

 ![](/tumblr_files/tumblr_inline_ore6mlJVZg1qfhdaz_540.png)

Las **hamburguesas** son chuches de hamburguesa y lo demás son gominolas del color correspondiente.

 ![](/tumblr_files/tumblr_inline_ore6mmAe8v1qfhdaz_540.png)
## La mesa de los niños

Hazte con un mantel verde, [platos de usar y tirar cuadrados negros](https://www.amazon.es/gp/product/B00UTITQ2Q/ref=as_li_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B00UTITQ2Q&linkId=982c820b03f76128423553b223e2300b) y servilletas amarillas, globos verdes, amarillos y rojos, [vasos amarillos](https://www.amazon.es/gp/product/B01DDSS1BU/ref=as_li_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B01DDSS1BU&linkId=28538de3732715bd4331b1aead4fc5dd) y verdes, y píntales la **cara del creeper,** y pajitas y [cubiertos verdes](https://www.amazon.es/gp/product/B01LHHXQZU/ref=as_li_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B01LHHXQZU&linkId=b5f2032dcee9763c3d6bf38bd702ebfb).

 ![](/tumblr_files/tumblr_inline_ore6mns3dj1qfhdaz_540.png) 
 ![](/tumblr_files/tumblr_inline_ore6mobH221qfhdaz_540.png)
## Los detalles para los invitados

Paquete de palomitas de Creeper:

 ![](/tumblr_files/tumblr_inline_ore6moqj0i1qfhdaz_540.png)

Una bolsita con un broche de hama, un boli forrado con goma E.V.A. y cinta de la que se usa para forrar los tallos de las flores, una [pulsera de goma de Minecraft](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_il_tl?ie=UTF8&tag=madrescabread-21&keywords=pulsera%20minecraft&index=aps&camp=3638&creative=24630&linkCode=xm2&linkId=b89a30f69f0171d264276c4a72782556)…

 ![](/tumblr_files/tumblr_inline_ore6mpKnpe1qfhdaz_540.png) 
 ![](/tumblr_files/tumblr_inline_ore6mqbCdY1qfhdaz_540.png)[Descarga aquí todos los imprimibles necesarios](https://www.dropbox.com/s/js9zj0d3wwe53k6/comuninminecraft.zip?dl=1)

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).