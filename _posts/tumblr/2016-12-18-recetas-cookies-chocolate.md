---
date: '2016-12-18T11:55:35+01:00'
image: /tumblr_files/tumblr_oidnopR2ym1qgfaqto1_1280.png
layout: post
tags:
- recetas
- familia
title: recetas cookies chocolate
tumblr_url: https://madrescabreadas.com/post/154627358464/recetas-cookies-chocolate
---

![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto1_1280.png)  
 ![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto2_1280.png)  
 ![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto3_1280.png)  
 ![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_oidnopR2ym1qgfaqto6_1280.jpg)  
  

## Galletas Americanas 
## 

Estas receta de cookies de chocolate típicas americanas es para hacerla en la Thermomix, pero la puedes hacer mezclando todo en un bol amasando

## Ingredientes:
1 huevo  
60 gr de azúcar  
1 pizca de sal  
180 gr de harina  
60 gr de mantequilla  
1 cucharadita de levadura  
2 cucharaditas de azúcar vainillado  
1 tableta de chocolate con leche 
## Preparación:
1.- Colocar todos los ingredientes, excepto el chocolate en el vaso y mezcla durante 15 segundos en velocidad 5.  
2.- Agregar la tableta partida en trocitos tamaño pepitas de chocolate y mezclar 5 segundos en velocidad 4.  
3.- Formar bolas con la mano y aplastar (si queda la masa pegajosa, enharinar las manos para trabajarla más facilmente). poner sobre una bandeja engrasada o papel de horno y hornear de 10 a 12 minutos a 180ºC. Tienen que quedar poco tostadas

¿Os animáis a hacerlas con los peques?

 ![](/tumblr_files/tumblr_inline_oiegt8I9mi1qfhdaz_540.gif)