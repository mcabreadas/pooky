---
date: '2014-02-04T08:59:00+01:00'
image: /tumblr_files/tumblr_inline_n0dtmznvee1qfhdaz.jpg
layout: post
tags:
- mecabrea
- crianza
- trucos
- planninos
- recomendaciones
title: 'Tips para celebrar cumpleaños de los niños: mi experiencia en la Bola de Cristal,
  Zig Zag Murcia'
tumblr_url: https://madrescabreadas.com/post/75572284868/tips-para-celebrar-cumpleaños-de-los-niños-mi
---
Es bien sabido que soy fan de los [cumpleaños caseros de toda la vida](/2011/06/20/aquellos-cumpleaños-felices/), y que [odio los parques de bolas](/2013/05/31/el-valor-de-los-regalos-de-cumpleaños/). Pero tampoco cierro los ojos a la realidad de estos tiempos de prisas, horarios dispares, agotamiento, y por qué no, de modas muy diferentes a las de nuestra época.


 ![image](/tumblr_files/tumblr_inline_n0dtmznvee1qfhdaz.jpg)

Cuando tus niños te dejan bien claro desde dos meses antes de su cumple que su mayor ilusión es ir a un parque de bolas con tooooda la clase tienes dos opciones:  
-O intentas convencerlos de lo contrarío (dos contra uno…y Princesita puede ser muy convincente…)  
-O respiras hondo y empiezas a devanarte los sesos para ver cómo puedes lograr no arruinate en tal hazaña.  
 En mi caso es especialmente grave porque Princesita y Osito comparten cumple y ya sabéis: doble alegría, doble gasto!

 ![image](/tumblr_files/tumblr_inline_n0dtc6TvG01qfhdaz.jpg)

Así que comparto con vosotros mis conclusiones, y **ALGUNOS CONSEJOS** para que la economía familiar no se vea demasiado mermada, como me sucedió a mí este año:

**1-Compartir fiesta con otros compañeros**

de clase que cumplan los años en el mismo mes. En nuestro cole lo tenemos fácil porque la profesora nos dio al principio del curso un listado con los cumpleaños de cada niño clasificados por meses.

Tampoco recomiendo celebración con más de tres cumpleañeros a la vez porque pierden demasiado protagonismo cada uno.

Informaos sobre el aforo del local y cuántos cumpleaños más se celebrarán ese día. En nuestro caso tuvimos suerte porque al ser un día entre semana estábamos solos y pudimos disfrutar de las estupenda instalaciones de que dispone el sitio que escogimos exclusivamente para nosotros. Esto es importante porque he estado en cumpleaños masificados y es una auténtica pesadilla. Y si tus hijos son pequeños no los puedes tener controlados porque se te diluyen en la masa y el agobio que se pasa no es poco.

**2-Cuando hagáis la reserva:**

Intentad que todo quede concretado perfectamente por escrito y quedaos con copia. Si introducís algún cambio posterior porque habéis cambiado de opinión sobre algún extremo, intentad que quede constancia por escrito del mismo (vía email, por ejemplo), evitad que sea sólo por teléfono.

En mi caso el local me cambió el día del cumpleaños por teléfono, y yo lo acepté, pero sin embargo yo hice un cambio en el menú de los padres en la misma conversación telefónica confiando en la buena fe de todo el mundo, y luego, al no constar por escrito en la reserva, no me lo respetaron y me tocó pagar el pato

**3-Menú para los niños:**

Siempre contratad menú con precio cerrado.

Si el local te da opciones, (en mi caso no fue así, y se quedaron la mayoría de meriendas en la bandejas), escoged un menú discreto porque, seamos realistas, menos contadas excepciones, como es el caso de Osito, que hasta que no se come la última miga no se levanta, casi todos los niños parece que están sentados sobre un alfiler y pegan un bote de la silla en cuanto han acabado con los gusanitos y algunos bocados de sandwich.

**Dato sobre la bebida** : estad atentos porque los peques pasan mucha sed al haber estado saltando como posesos y revolcándose en las piscinas de bolas. Hay que estar muy pendientes porque suelen acabar con la bebida en un suspiro, y los responsables no dan abasto rellenando todos los vasos porque son muchos niños. Yo suelo echarles una mano. Al estar la bebida incluída en el precio del menú cerrado, los niños pueden tomar la que necesiten.

 ![image](/tumblr_files/tumblr_inline_n0dtpnQrA71qfhdaz.jpg)

**4-Confirmad siempre el número de niños**

que van a asistir el día de la fiesta dentro del horario que el local os diga. Poned siempre niños de menos porque suelen fallar, y en caso de que vayan más, nunca serán muchos más, y a tiempo de preparar más meriendas siempre están.

Si no lo hacéis así os tocará pagar incluso por niños que no han ido, como fue mi caso. Pero lo paradójico fue que vinieron algunos niños que no esperaba, y faltaron otros que sí que esperaba, por lo que pedí algunas meriendas extra, pero aún así no las compensaron con las de los niños que no asistieron, y tuvimos que pagar al final las extras, más las de los niños que fallaron.

**5-Menú para padres**

… sí, sí, que también existe. Y esto es lo que encarece el asunto, y si no está perfectamente concretado podéis encontraros con un disgusto como el que yo me llevé. Porque cuando los niños son pequeños los papás y mamás suelen permanecer en el lugar de celebración hasta que termina la fiesta, y entre charlas, risas y buen rollito como el que tenemos en nuestro cole… pues la cosa se anima y entre el “comercio” y el “bebercio” la cosa se puede ir de madre.

Por eso os aconsejo que ofrezcáis unas bebidas y un picoteo. Que no se tienen por qué ir cenados, oiga. Y que fijéis el precio de la bebida. Incluso si no queréis llevaros sorpresas se me ocurre, aunque yo no lo hice, que pidáis que os avisen cuando paséis de cierta cantidad, más que nada para ir llevando un control.

En mi caso pasó lo que os comentaba. En principio me presentaron una única opción de menú para padres que iba más allá de unos refrescos y unos panchitos, así que, pensando que no había alternativa, la acepté pero, tras comentarlo con los otros padres de los cumpleañeros, supimos que también existía la opción económica y decidimos dejarlo en unas bebidas, panchitos y olivas. Pero al decirlo por teléfono no me lo respetaron, y el día de la fiesta sacaron toda la artillería. Los padres se fueron cenados, eso sí, pero me sentí engañada.

**6-A la hora de pagar:**

exigid factura oficial y comprobad muy bien que vaya detallado cada concepto, y que los precios coincidan con los acordados. Si no es así pagad sólo lo que pactásteis en un principio.

En mi caso, además de un menú de padres más caro, y de no compensar las meriendas extras con las de los niños que fallaron, también me cobraron de más por el menú de los niños, ya que me lo pusieron a precio de fin de semana, cuando precisamente el cumpleaños se celebró un día entre semana.

Imaginaos qué situación tan violenta con los papás y mamás del resto de cumpleañeros, y todos los invitados por allí despidiéndose encantados porque se iban cenados, claro, discutiendo e intentando llegar a un entendimiento con el responsable del local sin éxito.

Os puedo decir que fue una tarde horrible para mí, y que toda la ilusión que puse con el [cumple de mis pequeños](/2014/01/25/celebración-de-cumpleaños-solidario-un-cumpleaños/) se me transformó en un gran disgusto.

Menos mal que los niños de estas cosas no se enteran y se lo pasaron en grande. Ése es el consuelo que me queda.

Espero que estos tips os ayuden a no ir de incautos y que no os pase como me pasó a mí.

Os ha pasado algo parecido?

Fotos gracias a:

Mujer

Groupon

El Mundo


