---
date: '2016-05-10T19:49:08+02:00'
image: /tumblr_files/tumblr_o7ocdcadDf1qgfaqto1_400.jpg
layout: post
tags:
- cine
- premios
- planninos
- colaboraciones
- ad
- eventos
- familia
title: Pre estreno de Angry Birds, la película, y sorteo.
tumblr_url: https://madrescabreadas.com/post/144155963539/pre-estreno-de-angry-birds-la-película-y-sorteo
---

## Resultado sorteo gafas Angry Birds

Gracias a todas por participar!! Ya tenemos la ganadora del sorteo, y no os voy a hacer esperar más.

![](/tumblr_files/tumblr_o7ocdcadDf1qgfaqto1_400.jpg)

La ganadora es Dácil Isabel Muñoz Porta. Enhorabuena!! Mándame por privado tus datos para el envío, guapa.

Qué ilusión le hizo a mis fieras cuando les dimos la sorpresa de que iríamos a Madrid a la premier de la película de los Angry Birds gracias a la invitación de Sony Pictures.

 ![](/tumblr_files/tumblr_inline_o77x5tguBf1qfhdaz_540.jpg)

Mis hijos son fans de estos pajarillos desde hace tiempo, y les encanta jugar al videojuego. Incluso a mí me gusta derribar las construcciones de los cerdos, reconozco que me relaja.

La película es divertida, y los doblajes están muy conseguidos. Mi voz favorita, sin duda, es la de Matilda. Desde luego Cristina Castaño la borda, creo que ha hecho un gran trabajo.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/nsYuDG0_MU4" frameborder="0" allowfullscreen></iframe>

La historia cuenta con humor el origen del mal genio de Red, el pájaro rojo de cejas gruesas. El pobrecito no tenía padres y fue un inadaptado toda su infancia. Más adelante, él mismo alejaba a la gente de su lado, a pesar de que creo que en el fondo estaba deseando que lo quisieran.

Pero todo cambia cuando llegan los cerdos a su isla natal con oscuras intenciones que sólo él parece ver.

Pero es en Red donde estará la clave para evitar que los cerdos acaben con todos los huevos de sus habitantes.

Mi parte favorita es cuando va a terapia junto con Bomb y Chuk para aprender a controlar la ira con la profesora Matilda. De verdad, que se dan situaciones tronchantes.

 ![](/tumblr_files/tumblr_inline_o77x5uagWB1qfhdaz_540.jpg)
## La opinión de mis hijos

**Princesita** :

> Mi personaje favorito es Chuk ,porque es muy rápido y muy mono.            También me gustó que antes de entrar al cine nos dieran unas gafas de sol y un pájaro igual que Red. La bomba era súper guay porque cuando se enfadaba explotaba y quemaba todo.
> 
> En resumidas cuentas, ¡me ha gustado mucho la pelicula de los Angry birds!

**Osito:**

Mi mediano se queda con Bomb porque dice que tiene buen corazón, y le hace mucha gracia que explote cuando se enfada, y cuando menos se lo espera.

Su escena favorita es cuando Chuk es lanzado contra el castillo de Cochino, el rey de los cerdos, y pasa de habitación en habitación a cámara lenta entre aros de fuego, cactus… no le falta de nada al pobre. La verdad es que ahí nos reímos todos mucho.

**Leopardito:**

Mi pequeño dice que su personaje favorito es Red, por eso no soltó en todo el día el muñeco que le regalaron al llegar al cine, ni se quitó las gafas de sol con cejas enfadadas incluídas.

Al día siguiente me dijo que quería volver a ver la peli de nuevo.

La estrenan este viernes, y la recomendamos para niños y mayores. Y si sois fans de los videojuegos, entonces tenéis que verla sí o sí.

Os dejo unas fotos que nos hicimos con los protagonistas en “persona”, y un sorteíto.

En realidad me tendría que haber puesto con Red por ser el pájaro más cabreado, la verdad (aunque ese día yo no estaba nada cabrada, sino todo lo contrario).

 ![](/tumblr_files/tumblr_inline_o77x5upFhj1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o77x5uqhSe1qfhdaz_540.jpg)
## Sorteo de unas gafas de los Angry Birds
 ![](/tumblr_files/tumblr_inline_o77x5vdvp11qfhdaz_540.jpg)

Estas gafas tan molonas que imitan a Red, el pájaro más gruñón de todos, y uno de los protagonistas de la película, pueden ser tuyas.

**Requisitos:**

-Deja un comentario en este post diciéndome qué es lo que te cabrea y te hace fruncir el ceño como a Red, además de tu nick de Twitter o Facebook.

-Comparte el sorteo en Twitter escribiendo algo así: 

**“He participado en el sorteo de @madrescabreadas de unas gafas de sol de #AngryBirdsLaPeli (enlace de este post)”**

O en Facebook de forma pública

-Si quieres suscribirte al blog, puedes hacerlo en la columna de la izquierda, aunque no es obligatorio.

-El sorteo es de ámbito nacional, y el plazo para participar finaliza el 20 de mayo de 2016, y se publicará en este blog.

-Me reservo el derecho de anular las participaciones que considere fraudulentas según mi criterio.

¿Te apuntas? ¿Qué es lo que te hace fruncir el ceño y cabrearte como a Red?