---
date: '2016-01-09T15:42:48+01:00'
image: /tumblr_files/tumblr_inline_o39yrlB6Dj1qfhdaz_540.jpg
layout: post
tags:
- trucos
- regalos
- adolescencia
- colaboraciones
- ad
- recomendaciones
- familia
- moda
title: Oh Soleil. Moda para niños, y no tan niños
tumblr_url: https://madrescabreadas.com/post/136946827204/ropa-adolescentes-ohsoleil
---

Este año los Reyes Magos han tenido un reto con mi hija mayor de 10 años, más conocida en estos medios como Princesita.

 ![](/tumblr_files/tumblr_inline_o39yrlB6Dj1qfhdaz_540.jpg)

Ella es una niña que [todavía es niña, pero ya no tanto](/2013/07/05/preadolescentes.html), y empiezan a gustarle otro tipo de cosas no tan infantiles. Pero todavía no se siente mayor como para abandonar de golpe esta etapa.

Yo creo que está en una edad de transición en la que sus padre tendremos que saber acompañarla y vivir con ella los cambios que poco a poco va a experimentar.

Este año los Reyes lo han tenido difícil, pero han acertado de lleno porque lo que se hace con cariño, al final triunfa.

 ![](/tumblr_files/tumblr_inline_o39yrlnX3y1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_o39yrmtyTK1qfhdaz_540.png)

Una de las cosas que Pricesita pidió en su carta fue ropa. Así, en general, sin concretar nada. Y, claro, los Reyes tuvieron que darle al coco para ver qué tipo de prendas le haría especial ilusión, y qué estilo le gustaría.

 ![](/tumblr_files/tumblr_inline_o39yrm5Faf1qfhdaz_540.jpg)

Entonces sus Majestades descubrieron [Oh Soleil](http://ohsoleil.com/), una firma española, nacida en Tenerife hace 15 años que tiene una línea específica para adolescentes. Bueno, más bien yo diría que abarca desde la preadolescencia hasta los 16 años, porque incluye tallas desde los 8 hasta los 16 años.

Cada vez más marcas prestan especial atención a esta etapa de los niños y niñas, lo que considero un acierto total porque recuerdo mi experiencia a esa edad, cuando salía de compras con mi madre, como frustrante porque yo era muy alta, y me tenía que vestir de mayor para que la talla me fuese bien, y siempre nos costaba horrores encontrar ropa adecuada para mi edad, pero de tallas grandes. 

Oh Soleil combina diseño, sofisticación, comodidad y calidad a la perfección, dentro de un rango de precio medio.

La [colección de este otoño-invierno 2016](http://ohsoleil.com/coleccion-oi-2015-16/) se inspira en un estilo boho chic donde priman las mariposas, plumas, flores, animal prints, camuflajes, pelo e incluso escamas de lentejuelas. El espíritu más urbano se fusiona con elementos propios de la naturaleza.

Así de guapa se puso mi Princesita la mañana de Reyes con su vestido rosa con lunares negros y blancos, y su abrigo calentito de pelo y borrego, para ir a ver a sus abuelos. No sin antes devorar algunos de los adornos de chocolate que aún quedaban en el árbol de Navidad:

 ![](/tumblr_files/tumblr_inline_o39yrmmmwG1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yrnLIMh1qfhdaz_540.jpg)

El abrigo le encanta porque no pesa casi, y es de tacto dulce. Lleva borrego por dentro y pelo por fuera, lo que hace que sea ideal para los días de frío.

 ![](/tumblr_files/tumblr_inline_o39yrn5ibd1qfhdaz_540.jpg)

Si queréis ver más sobre esta colección de invierno os dejo algunos modelos:

 ![](/tumblr_files/tumblr_inline_o39yroNivc1qfhdaz_540.jpg)

Podéis pinchar en [este enlace](http://ohsoleil.com/coleccion-oi-2015-16/) para más información.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.