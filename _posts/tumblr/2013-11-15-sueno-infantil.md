---
date: '2013-11-15T12:30:00+01:00'
image: /tumblr_files/tumblr_inline_phkk97euTd1qfhdaz_540.jpg
layout: post
tags:
- salitaabuela
- crianza
- familia
title: 'La Salita de la Abuela: Con los nietos a dormir'
tumblr_url: https://madrescabreadas.com/post/67050049504/sueno-infantil
---

Este fin de semana las fieras mayores se quedan a dormir en casa de los abuelos. A veces se les revelan, pero la abuela tiene recursos para todo. Hoy nos cuenta cómo consigue que se duerman:

“Queridos amigos y amigas, hay algo que práctico a menudo con mis nieto y mi nieta mayores cuando tienen dificultad para dormirse por cualquier motivo.

Hago con ellos unos sencillos ejercicios de relajación que consisten en 5 respiraciones profundas tomando aire por la nariz y exhalándolo por la boca, todo ello con la luz baja. A continuación les hablo con voz susurrante, repasando las distintas partes del cuerpo, empezando por los dedos de los pies y acabando por la boca. Les digo: mueve tus dedos de los pies despacito, y ahora suéltalos y déjalos descansar, pies, piernas, muslos, barriga, pecho, dedos de las manos, muñecas, brazos, cuello, cara y boca.

Al final les sugiero que piensen en un lugar tranquilo, bonito, con una brisa suave, agradable… que les quita tristeza, agobios, miedo y les da paz y serenidad (la playa, el campo, un prado…)

![image](/tumblr_files/tumblr_inline_phkk97euTd1qfhdaz_540.jpg)

No creáis que esto nos ocupa mucho tiempo. Dependiendo del día, a veces 5 minutos, otras 10, como mucho 15. Sólo encuentro dificultad cuando soy yo la que estoy alterada o estresada, y al final me siento mucho mejor.

Las primeras veces les daba risa, pero ahora son ellos quienes me lo piden.

Mi nieto tiene una facilidad especial para quedarse prácticamente durmiendo antes de terminar con el ritual, y mi nieta tarda algo más, pero me dice que se siente muy bien. 

He comprobado que sirve para todas las edades, y resulta más efectivo cuanto más se practica.

Os invito a que lo intentéis y ya me diréis.“