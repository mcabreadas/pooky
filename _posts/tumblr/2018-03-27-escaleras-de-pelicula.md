---
date: '2018-03-27T09:11:27+02:00'
image: /tumblr_files/tumblr_inline_p68mmsAfrt1qfhdaz_540.png
layout: post
tags:
- mecabrea
- cosasmias
title: Anoche soñé con escaleras
tumblr_url: https://madrescabreadas.com/post/172301855304/escaleras-de-pelicula
---

Desde pequeña tengo un sueño recurrente, que si bien va cambiando de escenario, el concepto sigue siendo el mismo. Voy bajando una escalera, y por más que bajo nunca llego al final. A veces son escaleras normales de un edificio que reconozco porque he estado anteriormente, pero otras veces son escaleras extrañas de formas o estilos diferentes; antiguas de algún caserón viejo, [escaleras modernas de interior](http://es.fontanotshop.com/escaleras.html), o de caracol, como las de mi colegio que bajaba deslizándome sentada en la barandilla cuando no me veían las monjas, oincluso [escaleras escamoteables](https://es.fontanotshop.com/escaleras-escamoteables.html).

Lo mío con las escaleras es una especie de amor-odio que creo que viene de un pequeño trauma infantil, pero que sigo sin entender bien cómo se convirtió en tal trauma, puesto que el edificio donde surgió sólo tiene 4 plantas, pero cada vez que las bajaba sentía miedo de no llegar nunca abajo porque todos los pisos me parecían iguales, y las puertas de las casas también, de manera que a veces me confundía y desorientaba al buscar la puerta donde vivían mis primos.

Nunca lo dije a nadie, pero lo cierto es que las escaleras desde entonces me han generado incertidumbre y al mismo tiempo cierta fascinación, y no debo de ser la única porque si lo piensas, son muchas las escenas de grandes películas en las que las escaleras adquieren gran protagonismo bien sea por misteriosas, mágicas, terroríficas, o por su belleza o grandiosidad, Te pongo varios ejemplos.

## Escaleras de terror 

Seguro a se te vienen a la cabeza enseguida las de El Exorcista, 

 ![](/tumblr_files/tumblr_inline_p68mmsAfrt1qfhdaz_540.png)

o las de Al final de la Escalera.

 ![](/tumblr_files/tumblr_inline_p68mmtxsP71qfhdaz_540.png)
## Escaleras de cuento

Son muchísimas las que nos han conquistado de niñas, y lo siguen haciendo de mayores, como las de La Cenicienta, 

 ![](/tumblr_files/tumblr_inline_p68mmuw0nh1qfhdaz_540.png)

La Bella y la Bestia 

 ![](/tumblr_files/tumblr_inline_p68mmuGdrF1qfhdaz_540.png)

o Eduardo Manostijeras

 ![](/tumblr_files/tumblr_inline_p68mmygtfh1qfhdaz_540.png)
## Escaleras misteriosas

¿Y quién no recuerda aquella peli ochentera de David Bowie Dentro del Laberinto?

 ![](/tumblr_files/tumblr_inline_p68mmyJSqH1qfhdaz_540.png)
## Escaleras de vértigo 

Las de la peli Vértigo sin ir más lejos.

 ![](/tumblr_files/tumblr_inline_p68mmzAIgG1qfhdaz_540.png)
## Escaleras de risa

Aunque al final te pone un poco nerviosa la situación, las de Esta Casa es una Ruina nos hicieron reír a base de bien a toda una generación.

 ![](/tumblr_files/tumblr_inline_p68mmzim001qfhdaz_540.png)
## Escaleras mágicas

Sin duda las de Hogwarts, en Harry Potter son un claro ejemplo.

 ![](/tumblr_files/tumblr_inline_p68mn0NB3o1qfhdaz_540.jpg)
## Escaleras imponentes

Sin duda, el mejor ejemplo son las de la finca Los Doce Robles, de una de mis películas favoritas, Lo que el Viento se Llevó. 

 ![](/tumblr_files/tumblr_inline_p68mn1fcKw1qfhdaz_540.png)

Aunque las que más protagonismo cobran en la historia son las de la casa de Atlanta donde Escarlata vive con Rhett Butler, por desarrollarse en ellas varias escenas clave.

 ![](/tumblr_files/tumblr_inline_p68mn2va9x1qfhdaz_540.png)

Pero hay muchas más escaleras en el cine que ayudan a contar historias por el gran simbolismo que encierran, y las múltiples interpretaciones que se pueden hacer según se muevan por ella los personajes, ya que el subirlas no siempre es señal de éxito o logro, sino que a veces llevan a la parte más oscura de nosotros mismos.

Y bajarlas tampoco tiene por qué llevar a ninguna parte concreta, quizá a veces sólo al temor de no llegar nunca abajo del todo, o a no realizar nuestros sueños…

Y tú ¿Sueñas con escaleras?