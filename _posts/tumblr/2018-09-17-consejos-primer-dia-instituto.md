---
date: '2018-09-17T10:47:49+02:00'
image: /tumblr_files/tumblr_pe26v0bAtW1qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- crianza
- educacion
- familia
title: Adaptación al Instituto
tumblr_url: https://madrescabreadas.com/post/178173806129/consejos-primer-dia-instituto
---

Mucho han cambiado los tiempos desde que los chavales salíamos del colegio para comenzar el Instituto con 14 años, y aún así nos veíamos pequeños y nos llamaban pipiolos. Pues imaginaos ahora, que el cambio se produce con 12 años, cómo se ven de peques, o al menos así vi yo a mi hija y sus compañeros el primer día de Instituto la semana pasada.

Mi opinión sobre la edad a la que los niños dejan el colegio la tenéis en el post [Adolescentes: los 12 son los nuevos 14](/2017/10/06/adolescentes-los-12-son-los-nuevos-14.html), pero a pesar de ella, nosotros como familia hemos afrontado este cambio siempre siempre con optimismo y desde el punto de vista optimista, y logramos cambiar la incertidumbre por ilusión ante un mundo nuevo lleno de oportunidades.

Princesita asistió a clase su primer día de Instituto con mucha ilusión y bastante seguridad a pesar de no conocer a nadie a priori. Al llegar coincidió con la hermana de una compañera de mi hijo mediano, lo que le sirvió de gran apoyo.

El segundo día ya eran tres amigas, y pasaron el recreo juntas, así que la cosa en ese sentido pinta bien, ya que ella va contenta, y parece que se está integrando  muy bien, pero sólo lleva dos días de clase, y creo que en esta primera etapa de adaptación debemos estar muy atentos a cualquier señal que nos indique que algo no va bien, ya que se están enfrentando a uno de los cambios más importantes de su vida.

## Adaptación al Instituto

Los primeros días deInstituto son muy importantes para lograr una buena adaptación, y debemos observar si todo transcurre con normalidad dialogando mucho con ellos, sobretodo , escuchando para detectar cualquier dificultad siempre desde la empatía y acordándonos de cuando nosotros pasamos por eso.

Yo le conté a Princesita mi experiencia cuando entré al Instituto, y le di algunos consejos para hacer amigas los primeros días, pero sin caer en la comparación o extrapolación de mi experiencia , ya que cada persona es un mundo, y tanto las circunstancias como la realidad son totalmente diferentes. Ya sabéis que las comparaciones tanto con nosotros como cónsul hermanos o compañeros de clase son odiosas, y podrían dañar su autoestima.

La acompañamos su padre y yo el primer día para darle seguridad. Quizá más adelante no la llevemos hasta la puerta si ella lo prefiere así, o si le da vergüenza porque se sienta ya mayor para eso (todos hemos pasado por ahí, y hay que comprenderlo).

 ![adolescente en arbol](/tumblr_files/tumblr_pe26v0bAtW1qfhdaz_540.jpg)
## La semilla del vínculo

Este post interesa no sólo a madres y padres de adolescentes, sino a quienes tengan hijos pequeños, ya que, como os contaba es el post [Mejor, una adolesencia positiva](/2017/10/22/adolescencia-positiva.html), la semilla que sembremos en ellos de autoestima, confianza en sí mismos, autonomía, y el vínculo de seguridad y amor que tengamos con ellos les fortalecerá para afrontar esta nueva etapa.

Por otra parte, la educación que hayan recibido hasta este momento juega un papel muy importante, ya que «os niños que sean muy dependientes de sus padres, por ejemplo, al hacer las tareas o llevar al día el estudio tendrán más dificultades que los que sean más autónomos.

## Amistades nuevas

Una de las cosas que más miedo nos da a los padres, y así lo he podido comprobar en las múltiples conversaciones de estos días, son las amistades que harán nuestros hijos en el Instituto porque en esta edad el pertenecer a un grupo y ser aceptado son cosas muy importantes para ellos, y se suelen apartar de la familia para integrarse más con sus iguales.

 ![](/tumblr_files/tumblr_pf6sz8FxPF1qfhdaz_540.jpg)

Por eso debemos darles las herramientas para que sean capaces de elegir sus compañías. Sobre todo habilidades sociales, asertividad, saber decir que no, aprender a hacer cumplidos y críticas, aceptarlas… Yo lo resumiría en educarlos  con sentido común, mostrándonos cercanos, pero sin agobiarlos, respetando su espacio y su privacidad y sin que sientan que no confiamos en ellos. 

## Consejos para primer día deInstituto

**Dale seguridad y confianza**

Ponle ejemplos en los que haya superado conejito situaciones similares, como aquel día que fue a una fiesta sin conocer a nadie y acabó pasándolo en grande, por ejemplo…

Si le cuesta hacer amigos, yo aprovecharía el verano para que practique hacer nuevas amistades, aunque le supongan esfuerzo, y sobre todo hay que transmitirle que conocer gente nueva siempre abre la posibilidad de encontrar a personas más afines a él o ella.

**Eleva las dosis de cariño habituales**

Las sonrisas y la amabilidad siempre triunfan y es un consejo válido tanto para la relación con tu hijo como la relación de tu hijo con los compañeros con los que empiece el curso. Al final, las personas actúan ante los demás como las perciben, así que si dejas que los demás te conozcan, no les pones trabas y te acercas a ellos, ellos tienes más posibilidades que los demás actúen del mismo modo.

**Sé todo oídos**

Haz preguntas abiertas y no esperes respuestas concretas. Si empieza a contar cosas, déjalo sin interrumpir mostrando interés y dándole la importancia que para él o ella tiene el asunto que le preocupa. 

**No extrapoles tu experiencia**  

No lo agobies con tus propios miedos y advertencias. Mejor pocos consejos útiles que muchos… Como por ejemplo, que no se deje llevar por sus primeras impresiones o estereotipos y se dé una oportunidad para conocer a las personas más allá de las apariencias o que al principio todos están en una situación similar…

Mucha suerte si estás en esa etapa, y si todavía la ves lejana, créeme, llegará antes de lo que crees, pero tienes ente mano todas las herramientas para sembrar en tu hijo los valores para que aunque en algún momento se sienta perdido, sepa encontrar el camino.