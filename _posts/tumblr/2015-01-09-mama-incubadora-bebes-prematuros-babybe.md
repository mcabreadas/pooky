---
date: '2015-01-09T12:24:00+01:00'
image: /tumblr_files/tumblr_inline_nhwpcedPsy1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- gadgets
- ad
- tecnologia
- familia
- salud
- bebes
title: 'BabyBe: Nueva mamá-incubadora para los bebés prematuros'
tumblr_url: https://madrescabreadas.com/post/107589178699/mama-incubadora-bebes-prematuros-babybe
---

Bebés prematuros, tengo una buena noticia que daros. Han inventado un sistema para que estéis menos tiempo en la incubadora y así podáis estar lo antes posible con vuestra mamá. Además, el tiempo que paséis allí metiditos, será más llevadero para vosotros y para vuestras mamis porque estaréis sobre una superficie blandita y calentita que simula el torso de vuestra mamá tanto en forma, como en temperatura, tacto y otras sensaciones como respiración, latidos, olores, incluso su voz.

![image](/tumblr_files/tumblr_inline_nhwpcedPsy1qfhdaz.jpg)

Se llama [BabyBe](http://www.babybemedical.com/), y se ha presentado en la CES 2015, Feria Internacional de Electrónica de Consumo, donde se muestran los nuevos productos de electrónica que pueden, o no, aparecer para los consumidores, y se cierran contratos entre empresas del sector. Tiene lugar anualmente en Las Vegas.

Pero lo más increíble es que imita en tiempo real los movimientos de su pecho al respirar y los latidos de su corazón, lo que se capta a través de un sensor con forma de tortuga que abrazará ella, y que hace de receptor de esta información para transmitirla al módulo donde se acuesta el bebé. Lo que se intenta conseguir es que éste tenga sensaciones parecidas a estar tumbado sobre el pecho de la madre.

![image](/tumblr_files/tumblr_inline_nhwp1jIShq1qfhdaz.jpg)

Pero aún hay más. Es tal el interés de los creadores en imitar el contacto bebé-mamá, que también han introducido unos altavoces para que el recién nacido escuche su voz dentro de la incubadora.

También hay unos huecos para introducir toallas usadas por la madre en la zona que simula sus pechos para que el bebé se familiarice con su olor, y lo relacione con la zona donde más adelante se alimentará, con lo que este invento parece favorecer la lactancia materna.

![image](/tumblr_files/tumblr_inline_nhwpal5LJ31qfhdaz.jpg)

No sé realmente cómo funcionará en la práctica, pero la idea me parece brillante, sobre todo porque toma como base la evidencia científica de los enormes beneficios que tiene para los bebés prematuros y para su rápida recuperación y desarrollo el contacto y las sensaciones que provienen de su progenitora.

Si bien es verdad que cuando un niño nace prematuro normalmente dejan a las mamás un rato al tocarlos y acariciarlos dentro de la incubadora, con este sistema se pretende que los beneficios del contacto materno sean permanentes durante todo el tiempo que permanezca en ella.

Evidentemente nunca podrá ser lo mismo que estar con su madre, pero si se consigue reducir el tiempo que están apartados de ella se habrá logrado el objetivo de que estén cuanto antes en sus brazos.



_Fotos gracias a _

[http://www.engadget.com/](http://www.engadget.com/) 

[http://www.babybemedical.com/](http://www.babybemedical.com/)