---
layout: post
title: Las horas contigo
date: 2013-05-21T00:04:00+02:00
image: /images/uploads/depositphotos_6487832_xl.jpg
author: maria
tags:
  - crianza
  - lactancia
  - cosasmias
  - bebes
tumblr_url: https://madrescabreadas.com/post/50937762197/las-horas-contigo-contigo-las-horas-se-llenan-de
---
Contigo las horas se llenan de risas, 

contigo las horas son todo ternura, 

son todo caricias y besos de leche, 

olor a dulzura y aliento de nube.

Contigo las horas son de terciopelo, blanditas y calientes.

Tu ritmo y el mío se juntan en uno;

Respiras, respiro.

Succionas, suspiro.

Lates, lato.

Me miras, te admiro.

Te cuido, me mimas.

Me añoras, te busco.

Me dices, te digo.

Duermes, duermo.

Sueñas, te sueño.

Despiertas, despierto.

Sonríes, sonrío.

Lloras, lloro.

Sufres, sufro.

Te duele, me duele.

Te doy, me llenas.

Son horas de siestas, de arrullo y calor, 

tu olor las envuelve, me invade tu amor.

Y así vamos marcando las horas, 

nuestras horas juntos.

Las horas contigo son una delicia de juegos, cosquillas, carcajadas y besos. 

Qué suerte la mía, que suerte la tuya, 

son nuestras horas, no son las del mundo, 

nadie las marca, sólo nosotros y nuestros corazones.

*Foto gracias a [sp.depositphotos](https://sp.depositphotos.com/)*