---
layout: post
title: "Yoga/Pilates en tu parto y maternidad: Las sombras de la Lactancia"
date: 2014-08-01T09:45:18+02:00
image: /tumblr_files/tumblr_n9f2lnjDGz1qgfaqto1_500.jpg
author: .
tags:
  - mecabrea
  - crianza
  - revistadeprensa
  - participoen
  - colaboraciones
  - ad
  - lactancia
  - familia
tumblr_url: https://madrescabreadas.com/post/93482106454/yogapilates-en-tu-parto-y-maternidad-las-sombras
---
[Yoga/Pilates en tu parto y maternidad: Las sombras de la Lactancia](http://www.tupartoymaternidad.com/2014/07/las-sombras-de-la-lactancia.html)  

[![](/tumblr_files/tumblr_n9f2lnjDGz1qgfaqto1_500.jpg)](http://www.tupartoymaternidad.com/2014/07/las-sombras-de-la-lactancia.html)

El blog “[Yoga/Pilates en tu parto y maternidad](http://www.tupartoymaternidad.com/2014/07/las-sombras-de-la-lactancia.html)“ ha publicado mi post ["Las sombras de la lactancia”](https://madrescabreadas.com/2013/04/27/las-sombras-de-la-lactancia-los-niños-se-encelan/), precisamente la semana que celebramos la [Semana Mundial de la Lactancia Materna](http://albalactanciamaterna.org/general/semana-mundial-de-la-lactancia-materna-2014/).

![](/tumblr_files/tumblr_inline_n97nkiWLqk1qfhdaz.png)

Este post lo escribí en plena lactancia de mi hijo menor, Leopardito, cuando el cansancio y la falta de sueño me invadían y todo mi mundo parecía derrumbarse a mi alrededor.  

Pero ya sabéis. Toda sombra tiene una luz detrás…