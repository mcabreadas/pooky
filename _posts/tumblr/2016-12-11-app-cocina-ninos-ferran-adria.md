---
date: '2016-12-11T20:18:48+01:00'
image: /tumblr_files/tumblr_inline_ohtvz4N1fd1qfhdaz_540.png
layout: post
tags:
- libros
- planninos
- colaboraciones
- ad
- eventos
- familia
- recetas
title: Tu cuento en la cocina de Ferran Adrià
tumblr_url: https://madrescabreadas.com/post/154340439609/app-cocina-ninos-ferran-adria
---

Te cuento en la cocina es el proyecto que ha unido a [Disney](http://www.disney.es/te-cuento-en-la-cocina/proyecto) y a Ferran Adrià contando con los mejores colaboradores: [Telefónica](https://ferranadria.fundaciontelefonica.com/) aportando todo el soporte tecnológico, [Carrefour](http://www.carrefour.es/-te-cuento-en-la-cocina-ferran-adria-y-disney-ensenan-a-cocinar-a-padres-e-hijos/a550038/a) ayudando para que las familias encuentren todos los productos con calidad, la [Obra Fundación la Caixa](https://obrasociallacaixa.org/es/) aportando su experiencia en el fomento de hábitos saludables, y el [Hospital La Paz de Madrid](http://www.madrid.org/cs/Satellite?cid=1354582567488&language=es&pageid=1159444389315&pagename=PortalSalud%2FCM_Actualidad_FA%2FPTSA_pintarActualidad&vest=1159444389315) avalando los menús, recetas y los mensajes sobre la salud.

A la presentación de la App asistió en nombre de este blog nuestra colaboradora Mónica, de [Paraíso Kids](http://www.paraisokids.es/) para contárnoslo todo:

 ![](/tumblr_files/tumblr_inline_ohtvz4N1fd1qfhdaz_540.png)
## Más que un libro de recetas

¡Vamos a cocinar con la imaginación de uno de los mejores chef del mundo, Ferran Adrià!

 ![](/tumblr_files/tumblr_inline_ohtw0qvIOI1qfhdaz_540.png)

La idea fue crear un proyecto multiplataforma que comenzó en abril con un libro de recetas inspirado en Disney, Pixar, Marvel y Star Wars, y que a partir de ahora se amplía con el lanzamiento de la [APP “Tu cuento en la cocina”](http://www.disney.es/te-cuento-en-la-cocina/app-ferran-adria) con el objetivo de introducir a los más pequeños en el mundo culinario a través de juegos.

 ![](/tumblr_files/tumblr_inline_ohtvy5GeCt1qfhdaz_540.png)

La aplicación ha sido desarrollado por Telefónica e incluye un montón de cosas chulas como personalización de avatar, 24 recetas interactivas, una navegación fantástica por la cocina de Adriá con un movimiento de 360º pudiendo acceder a los contenidos, a los mensajes…, una sección de entrenamiento donde enseñan a los más pequeños a cortar, lavar…

## Convierte tu receta en un cómic
 ![](/tumblr_files/tumblr_inline_ohtvznBxBl1qfhdaz_540.png)

Pero lo realmente emocionante es que podremos convertir cada receta en un cómic personalizado inspirado en los mundos de Disney, Marvel, Star Wars y Pixar!

## Cocina jugando en familia

He tenido la oportunidad de probar la App y comprobar lo sencillo que es aprender a cocinar, lo divertido que le ha parecido a mis hijas y el buen rato que hemos pasado en familia.

Tras elegir una receta, la aplicación nos ha explicado paso a paso cómo se hace, quien puede realizarla (adulto o niño) y además nos ha desvelado algún que otro concepto que teníamos embrollado.

La App podrás encontrarla para Android e iOs y su coste es de 3,99 €.

Este proyecto multiplataforma incluye también un e-book, una web, programas televisivos emitidos en Disney Chanel y el blog de Ferran Adrià.

¡Anímate a cocinar con tus hijos esta Navidad!

Te dejo con un divertido video de la aparición del Pato Donald en la presentación de la App

<iframe width="100%" height="315" src="https://www.youtube.com/embed/7AkAs6WJMwU" frameborder="0" allowfullscreen></iframe>

Más información [aquí.](http://www.disney.es/te-cuento-en-la-cocina/proyecto)

Para [descargar la App aquí](http://www.disney.es/te-cuento-en-la-cocina/app-ferran-adria).