---
date: '2016-03-08T20:29:23+01:00'
image: /tumblr_files/tumblr_inline_o4wczhcXyi1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- bebes
- premios
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- moda
title: Sonajeros guitarra para bebés modernos. Sorteo
tumblr_url: https://madrescabreadas.com/post/140699832689/sonajero-bebe-guitarra
---

## Resultado del sorteo

Muchas gracias a todos por participar. And the winner is…

 ![](/tumblr_files/tumblr_inline_o4wczhcXyi1qfhdaz_540.jpg)

Enhorabuena, Bebé pingüino!! Por favor, déjame tus datos por privado para el envío.

Post originario:

Hace tiempo que estoy cautivada por unos sonajeros de [The Lazy Jellyfish](http://www.thelazyjellyfish.com/), muy especiales, con forma de guitarra eléctrica, y quería compartirlos contigo.

Y qué mejor manera que organizando un sorteo para que la ganadora elija el que más le guste y se lo lleve a casa.

 ![](/tumblr_files/tumblr_inline_o3qiz27v5K1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3qiy48Ztt1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3qiza3G0i1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o3qizfiHrU1qfhdaz_540.jpg)

The Lazy Jellyfish es una empresa joven que nació en Barcelona en 2012, y que actualmente se ha trasladado a U.K., y que se dedica a hacer juguetes y ropa para niños de 0 a 6 años. 

Sus creadoras son amigas y diseñadoras textiles y estilistas y les encanta experimentar con cualquiera de las disciplinas artísticas y colaborar con otros artistas.

Sus prendas son totalmente eco friendly, materiales orgánicos en su mayoría certificados por GOTS y una producción local que facilita el seguimiento y la seguridad de unas condiciones de trabajo justas.

 ![](/tumblr_files/tumblr_inline_o3q99qGAdG1qfhdaz_540.jpg)

Los tejidos orgánicos, estampados divertidos y coloridos y un aire 80’s, 90’s caracterizan The Lazy Jellyfish. Su filosofía se simplifica en [rock, diversión, amigos y amor](http://issuu.com/thelazyjellyfish/docs/issuu), con estas cosas lo tienes todo para ser feliz, y es lo que intentan transmitir con todo lo que hacen.

Han participado en diversos talleres de festivales como [Minimúsica](http://www.eldiaminimusica.com/ca), [Vida Festival](http://www.es.vidafestival.com/), Primavera Kids… y les encanta relacionar niños con música y creación artística.

Lo más característico de esta firma, y lo que me enamoró, son sus [guitarras y sonajeros](https://www.dropbox.com/sh/i080vu731u8qlze/AADhXzpRkeJhFV-KeXotIPj-a?dl=0) blanditos, o con las que muchos niños/as juegan a ser rockstar.

 ![](/tumblr_files/tumblr_inline_o3q9atePOK1qfhdaz_540.jpg)

Ésta es la que he elegido para Leopardito, y le ha entusiasmado porque su diseño moderno, original, y muy vistoso les llama mucho la atención.

 ![](/tumblr_files/tumblr_inline_o3ru1dgYbx1qfhdaz_540.jpg)

Además el tejido de algodón y fieltro es lavable, y agradable al tacto y su relleno es blandito.

Pero The Lazy Jellyfish no sólo tiene sonajeros de lo más original que has visto, tras presentar la colección de ropa en [Playtime Paris](http://www.playtimeparis.com/) confeccionada en algodón y felpa orgánica con estampados digitales inspirados en Egipto y el movimiento Memphis, tengo que destacar sus mochilas, que están hechas en lona orgánica y son super molonas para el cole y la guarde!!

 ![](/tumblr_files/tumblr_inline_o3q9a87r7Z1qfhdaz_540.jpg)
## Para participar en el sorteo

Si quieres llevarte una de estas guitarras sonajero tan guais, sólo tienes que cumplir estos requisitos:

 ![](/tumblr_files/tumblr_inline_o3qit2M9Hr1qfhdaz_540.jpg)
1. Deja en un comentario cuál de estas guitarras elegirías caso de resultar ganadora y tu alias en Instagram.  
2. Seguir a The Lazy Jellyfish en Instagram: [aquí](https://www.instagram.com/thelazyjellyfish/).  
3. Comparte en Instagram la foto del sorteo: [aquí](https://www.instagram.com/p/BCtDhdiFGG6/?taken-by=madrescabreadas).  

No es obligatorio, pero si sigues mi perfil de Instagram te enterarás la primera del resultado.

El ámbito del sorteo es el territorio español.

El plazo para participar finaliza el 18 de marzo, y el resultado se publicará en este post y en Instagram en los días sucesivos.

Me reservo el derecho de eliminar las participaciones fraudulentas bajo mi criterio.

¿Con qué guitarra te quedas?

> [Sorteo molón donde los haya con @thelazyjellyfish . Solo 3 requisitos: 1-Elige tu sonajero guitarra y deja un comentario en el blog madrescabreadas.com diciendo cuál te gusta más: roja, lunares, azul o rosa. 2-Sigue a @thelazyjellyfish 3-Comparte está foto con tu perfil abierto para que podamos comprobarlo. Suerte!! Tienes el enlace en mi bio @madrescabreadas](https://www.instagram.com/p/BCtDhdiFGG6/)