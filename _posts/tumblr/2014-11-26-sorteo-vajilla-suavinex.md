---
date: '2014-11-26T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petodamw6H1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- premios
- colaboraciones
- puericultura
- ad
- familia
- bebes
title: 'VI sorteo de aniversario: Vajilla de aprendizaje y set on the go de Suavinex'
tumblr_url: https://madrescabreadas.com/post/103626208897/sorteo-vajilla-suavinex
---

![image](/tumblr_files/tumblr_inline_petodamw6H1qfhdaz_540.jpg)

## Actualización 8-12-14

Ya sé, ya sé que estábais impacientes. ¿No sabéis que la paciencia es una virtud? ¿Y que además sois tantos participantes que me ha costado un riñón  poquito hacer el sorteo? Pues ale! Aquí tenéis el nombre de las dos ganadoras:

![](/tumblr_files/tumblr_inline_petodakczb1qfhdaz_540.jpg)

Enhorabuena a Elena Salvador, que ha ganado la vajilla de aprendizaje.

Enhorabuena a Sandra Bp, que ha ganado el set on the go

Por favor, escribidme en privado y dadme vuestros datos para los envíos.

Gracias a todos por la gran participación (aunque me deis trabajo no me importa, de verdad. Me encanta que os haya gustado tanto el sorteo), y gracias muy especialmente a Suavinex, y su [Club de Madres Felices](http://elclubdelasmadresfelices.com/), que tan feliz va a hacer a dos ahora mismo.

Post originario:

Suavinex se ha unido a la celebración del [cuarto aniversario del blog](/portada-aniversario) con este chulísimo sorteo, como broche de oro de la serie de sorteos que he ido haciendo de forma escalonada durante este mes.

Me alegra mucho compartir estos momentos con Suavinex porque para mí es mucho más que una marca. Desde que conocí el [Club de las Madres Felices](http://elclubdelasmadresfelices.com/), al poco de aterrizar por la blogosfera maternal cuando era una pipiola de los blogs he encontrado en él acompañamiento, empatía, complicidad, feedback… Verdaderamente a esta marca le gustamos las madres y nos miman como nadie.

Y, aunque os parezca extraño que una madre cabreada como yo pertenezca a un club de madres felices, os puedo asegurar que lo cortés no quita lo valiente, y que para ser feliz, lo mejor es echar los cabreos fuera. Ya sabéis que todas somos en algún momento madres cabreadas.

Y precisamente para tratar de haceros felices sorteamos dos regalos para dos ganadoras:

## Vajilla de aprendizaje

Para mayores de 6 meses

Esta vajilla de aprendizaje rosa o azul a elegir,  compuesta por plato llano, bol, taza, cuchara y mantel antideslizante,

¡Toda una ayuda para aprender a comer solito!

![image](/tumblr_files/tumblr_inline_petoda2zep1qfhdaz_540.jpg)

## Set on the go

Para mayores de 4 meses

Un set compuesto por cuchara, porta cucharas con capacidad para llevar dos cucharas y un sujeta baberos que permite convertir una simple servilleta en un improvisado babero. Ideal para comer fuera de casa.

Lo podéis elegir en rosa o en azul.

![image](/tumblr_files/tumblr_inline_petodbwrVZ1qfhdaz_540.jpg)

## Para participar:

-Dejar un comentario en este post diciendo el color que elegiríais caso de resultar ganadores: rosa o azul, y vuestro nick de Facebook o Twitter.

-Ser fans en Facebook de [El Club de las Madres Felices](https://www.facebook.com/Suavinex?ref=hl)

Y si queréis, aunque no es obligatorio, pero así estaréis al día del resultado del sorteo, podéis suscribiros a este blog. Para ello sólo tenéis que introducir vuestro email en el margen derecho.

El sorteo es válido para territorio español, y el plazo acabaría el 3 de diciembre a las 00:00 h.

¿Qué prefieres, rosa o azul?