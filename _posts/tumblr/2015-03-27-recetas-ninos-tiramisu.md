---
date: '2015-03-27T14:59:10+01:00'
image: /tumblr_files/tumblr_inline_nlvi1vkaKk1qfhdaz_500.jpg
layout: post
tags:
- recetas
- planninos
title: 'Recetas con niños: Tiramisú en ocho pasos.'
tumblr_url: https://madrescabreadas.com/post/114754720609/recetas-ninos-tiramisu
---

Han llegado las vacaciones y hay más tiempo para pasar con los niños. Por eso os dejo la receta del tiramisú que aprendí de mi amiga Lolita, y que hice para el cumpleaños de la abuela, muy fácil de elaborar con ellos y que, además, les encanta… y a mí también. La verdad es que es uno de mis postres favoritos.

Quedó así de bien!

 ![image](/tumblr_files/tumblr_inline_nlvi1vkaKk1qfhdaz_500.jpg)
## Ingredientes:

-1 paquete bizcochos de soletilla (de esos finitos)

-250 g queso mascarpone (o en su defecto queso de untar)

-250 g nata líquida para montar 

-1 cafetera de café o café soluble (descafeinado si van a tomar los niños)

-7 cucharadas de azúcar

-cacao puro en polvo

## Cómo se hace:

1- Se hace el café lo primero para que dé tiempo a que se enfríe. No hace falta ponerle azúcar, y puede hacerse clarito o fuerte al gusto, incluso descafeinado para que puedan comer los niños.

 ![image](/tumblr_files/tumblr_inline_nlvi5pcTfR1qfhdaz_500.jpg)

2- Se hace la crema mezclando primero con la batidora un poco de nata y el azúcar, y después montando toda añadiendo poco a poco el queso mascarpone o queso de untar normal hasta que quede consistente.

 ![image](/tumblr_files/tumblr_inline_nlvi678MEf1qfhdaz_500.jpg)

3- Se bañan los bizcochos en el café (ni mucho ni poco para que no quede  demasiado seco ni demasiado empapado), 

 ![image](/tumblr_files/tumblr_inline_nlvi7knq3J1qfhdaz_500.jpg)

4- y se pone una capa en un recipiente cuadrado, después se espolvorea con un colador el cacao, 

 ![image](/tumblr_files/tumblr_inline_nlviaiB7s81qfhdaz_500.jpg)

5- encima se pone una capa de crema, 

 ![image](/tumblr_files/tumblr_inline_nlvib596av1qfhdaz_500.jpg)

6- otra vez se espolvorea cacao 

 ![image](/tumblr_files/tumblr_inline_nlvibtpMr21qfhdaz_500.jpg)

7- se pone otra capa de bizcocho bañado, otra vez cacao.

 ![image](/tumblr_files/tumblr_inline_nlvidqUhez1qfhdaz_500.jpg)

8- Queda mejor si la última capa de crema es más gruesa, y se cubre con bastante cacao.

 ![image](/tumblr_files/tumblr_inline_nlvieg3o6a1qfhdaz_500.jpg)

Mmmmmm… lo confieso tengo la boca hecha agua ahora mismo al volver a ver las fotos. 

¿A que es fácil? Créeme, os va a encantar. 

Ya me contaréis!