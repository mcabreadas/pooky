---
layout: post
title: Codiciando tiempo
date: 2011-07-26T20:51:00+02:00
image: /images/uploads/depositphotos_157143064_xl.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/8094317629/codiciando-tiempo
---
He caído en la cuenta de que últimamente me he vuelto una codiciosa con el tiempo. Me explico, defiendo a capa y espada mis horas o minutos de soledad como si me fuera la vida en ello. Creo que por primera vez en mi vida disfruto de ella, y eso que soy un ser sociable por naturaleza, como ya os he contado en otras ocasiones, pero he estado tan absorbida desde que nació mi primera hija, allá por 2006, y posteriormente  enlazado con  mi segundo en 2008, que ahora que empiezan a ser un poco más independientes me agarro a mi tiempo con uñas y dientes.



## Me gusta, por ejemplo, quedarme sola en casa y estar en calma, pensar en voz alta, dedicar tiempo a cuestiones de “belleza” tranquilamente, (lo cual es lo contrario de ducharse, vestirse,darse dos brochazos, si acaso, y salir corriendo oyendo de fondo los llantos y/o gritos de los peques).

Busco ese tiempo con ansiedad, es como si se me fuera a escapar, como si me hubiera reencontrado con una vieja amiga a la que hacía un siglo que no veía y por fin pudiéramos sentarnos tranquilamente a tomar un café.  

Ansío mi tiempo, es como si tuviera que recuperar el tiempo que he estado desconectada de la vida “normal” y siento ansiedad por hacer cosas que no he podido hacer mientras mis hijos han sido bebés, o las hacía con prisas y sin disfrutar.

Tenía razón la doctora cuando fui a consultarle hace un par de años que me encontraba débil continuamente y, tras hacerme unos análisis e informarme que estaba como un roble (palabras textuales) me dijo: “cuando logres tener un poco de tiempo para ti empezaras a encontrarte mejor”. Y así fue. Así que no desesperéis si estáis en la primera etapa de la crianza porque todo vuelve, y no de la misma manera, sino mejor.

Sí, me he redescubierto, y lo mejor es que me gusta como soy ahora después de haber sido madre. La maternidad me ha hecho una persona más segura, más madura, más cariñosa, más empática. Pero me pasa una cosa: no soporto las injusticias y los ataques a los débiles, se me ha despertado la justiciera que llevaba dentro.

Otra cosa curiosa: antes prefería callarme ante cualquier cosa que me sentaba mal por evitar la confrontación y, claro, luego me quedaba con el resquemor. Ahora, como por arte magia, he aprendido a decir lo que me incomoda  porque he decidido no quedarme con nada malo dentro. Quiero decir que antes me las tragaba como puños y ahora pienso: “mejor fuera que dentro”, y me quedo en la gloria cuando suelto “un me cabrea”.

Lo habéis probado? Es la mejor terapia!

Foto gracias a [sp.depositphotos](https://sp.depositphotos.com)