---
date: '2016-07-19T18:27:47+02:00'
image: /tumblr_files/tumblr_inline_oakk3uNIDb1qfhdaz_540.jpg
layout: post
tags:
- salud
- crianza
- moda
- familia
title: Las gafas de sol para niños son más que moda
tumblr_url: https://madrescabreadas.com/post/147650021464/gafas-sol-ninos
---

Las gafas de sol para los niños son es sólo una cuestión de moda, sino que pueden prevenir problemas serios de visión en un futuro.

Ésta es la importante conclusión que saqué de nuestro encuentro con el grupo [Sáfilo](http://www.safilonet.safilo.com/webapp/commerce/safilo/jsp/logon.jsp?lng=S&cpy=ES) y la Dra. Barraquer, de la [Fundación Barraquer](http://www.barraquer.com/fundacion/que-hacemos/viajes-humanitarios/), que tuvo lugar a comienzos de verano.

La modelo [Vanesa Lorenzo](http://www.vanesalorenzo.com/) presentó la iniciativa Kids Only de Sàfilo, que aglutina marcas de gafas como [Polaroid](http://polaroideyewear.com/es/es.html), para cubrir las necesidades oftalmológicas de los niños:

 ![](/tumblr_files/tumblr_inline_oakk3uNIDb1qfhdaz_540.jpg)

>  “Tenemos que ser conscientes de la enorme responsabilidad que tenemos como madres para proteger no solo a nuestros hijos, sino también de crear una conciencia social sobre el uso de gafas de sol. ¿Si protegemos nuestra piel, por qué no protegemos nuestros ojos frente al sol?”

Al evento acudió nuestra colaboradora Mónica, de [Paraíso Kids](http://www.paraisokids.es/), para explicarnos de primera mano la labor tan importante que desarrolla esta fundación, y la importancia de proteger la vista de los más pequeños.

Tenemos una responsabilidad como padres y madres.

## La Fundación Barraquer en Mozambique

El pasado mes de mayo arrancó una nueva expedición de la **Fundación Barraquer** y **Sáfilo** a Mozambique para asistir a personas sin recursos con problemas de visión ocasionados por la sobreexposición solar y la malnutrición. Esta marcha realiza intervenciones quirúrgicas para tratar afecciones oftalmológicas como cataratas, una enfermedad ocular que desemboca en la ceguera.

El equipo humano, formado por especialistas en oftalmología y voluntarios, atendió a más de 240 personas.

 ![](/tumblr_files/tumblr_inline_oakl5z6W8K1qfhdaz_540.jpg)

En este video podéis ver un resumen de esta [expedición a Mozambique de la Fundación Barraquer](http://www.serpadres.es/familia/noticias/video/mas-de-200-personas-recuperan-la-vision-gracias-a-una-genial-campana).

Mónica nos resume lo que nos contó la Doctora:

## Proyecto Kids Only

La importancia de proteger la vista de los pequeños de la casa frente al sol es significativa y de ahí que se hayan unido el Grupo Sáfilo y la fundación que lidera la Doctora Elena Barraquer para Iniciar el proyecto Kids Only , haciendo entrega de una donación económica y más de 1.000 gafas de sol y vista para ayudar con la labor a las expediciones que el equipo de la fundación realiza a países en vías de desarrollo.

 ![](/tumblr_files/tumblr_inline_oakkq2PDJP1qfhdaz_540.jpg)

> “Tenemos mucha suerte de haber nacido donde hemos nacido. Existe mucha gente ayudando a otros que lo necesitan y la Fundación Barraquer efectúa una labor grandiosa en los países donde la sanidad no cubre todas las necesidades que los habitantes precisan. Gracias a personas, empresas privadas, etc… que apoyan esta labor se pueden hacer grandes cosas.”

Se puede ayudar de mucha maneras, donando o concienciando.

Si quieres donar puedes hacerlo pinchando [aquí](http://www.barraquer.com/fundacion/colabora). Con muy poco se puede hacer mucho.

La labor de concienciación sobre la importancia del cuidado de la vista de nuestros hijos es muy necesaria en España.

Mientras en otros países es habitual ver a familias en piscinas, playas, ríos… con gafas de sol desde niños, aquí cuesta más que se le dé la importancia debida.

## Prevención de las cataratas

La Dra. Barraquer afirma que:

> “La catarata es la causa número uno de ceguera en el mundo, y una de las causas que contribuye a que la catarata se desarrolle más rápidamente es la luz solar”

 ¿Por qué ponemos crema en la piel de nuestros niños cuando salimos a la calle o a la piscina/playa?

La respuesta es muy sencilla y a muy poca gente se le pasa por la cabeza no hacerlo. Intentamos proteger la piel de nuestros pequeños de la luz solar

¿Y por qué no protegemos los ojos?

La razón es que en España no nos han educado en ese ámbito. Sin embargo, necesariamente hay que hacerlo, los rayos de sol son igual de dañinos para los ojos que para cualquier otro órgano del cuerpo y los oftalmólogos aconsejan usar gafas de sol.

El uso de gafas de sol provoca la reducción de posibilidad de tener cataratas en la edad adulta en un 50%

## Gafas de sol “irrompibles”

Mis hijos han usado varias gafas de sol sin éxito o haber acabado rotas o perdidas, por eso me parece interesante esta línea de Polaroid, hechas pensando en la naturaleza dinámica de los niños.

El grupo Sáfilo nos sugiere su nueva línea de gafas de sol para Kids Polaroid Twist, con tecnología UltraSight, lentes polarizadas, de goma, con colores mates (naranjas, azules, verdes, gris, fucsia y negro)

 ![](/tumblr_files/tumblr_inline_oakkxduitp1qfhdaz_540.jpg)

Fexibles (se doblan y no se rompen) y con posibilidad de que el papá o la mamá tengan unas iguales.

 ![](/tumblr_files/tumblr_inline_oakkyndSeR1qfhdaz_540.jpg)

Una línea que busca de gafas todo terreno, alegres y sobre todo cómodas para que los más peques no las rompan fácilmente.

Gracias, Mónica por tu resumen.

Después de esto, creo que está claro que debemos proteger la vista de nuestros hijos.

¿Los tuyos usan gafas de sol?