---
date: '2014-12-19T20:14:00+01:00'
image: /tumblr_files/tumblr_inline_ngp2pglqBt1qfhdaz.png
layout: post
tags:
- crianza
- trucos
- premios
- recomendaciones
- familia
- juguetes
title: 'Concurso Tu carta a los Reyes Magos: Mesa Parlanchina de Chicco'
tumblr_url: https://madrescabreadas.com/post/105624025869/carta-reyes-magos-chicco
---

ACTUALIZACIÓN 28-12-14

Con el fin de que los regalos lleguen a su destino a tiempo para Reyes  **se cerrará el concurso el 29-12-14 a las 8:00 am.**

Los cinco participantes cuyas cartas hayan obtenido más comentarios de personas diferentes tendrán que mandarme un email a  **mcabreada@gmail.com**  antes de las 12:00 am del día 29 con:

-su nombre y apellidos, dirección y teléfono y email para el envío

Si algún participante no manda el email con su lista antes del 29-12-14 a las 12:00 el premio pasará al concursante cuya carta sea la siguiente en número de comentarios. Todo ello con la finalidad de que el envío de los regalos se haga lo antes posible.

ACTUALIZACIÓN 27-12-14

**SE ADELANTA LA FECHA TOPE PARA EL CONCURSO AL 29 -12**  
Me han propuesto adelantar la fecha de fin del concurso “Tu carta a los Reyes Magos” para que dé tiempo a que lleguen los regalos para esa noche tan especial, y creo que es muy buena idea. Así que se adelanta la fecha tope para subir vuestras cartas al lunes 29 de diciembre a las 00:00 h.

Vamos! Corre, corre!!

_Post originario:_

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

Uno de los regalos que podéis incluír en vuestra cata a los Reyes Magos para participar en el concurso es esta Mesa Parlanchina de Chicco.

 ![image](/tumblr_files/tumblr_inline_ngp2pglqBt1qfhdaz.png)

Esta mesa está incluída en el catálogo de juguetes que Chicco nos presenta para esta Navidad, entre los que también incluye la **bici sin pedales** , ideal para aprender a montar a partir de los tres años, aunque mi Leopardito ya se atreve a intentarlos con 27 meses: Mirad cómo hace sus primeros pinitos. No veáis lo que progresa día a día, y lo bien que le viene para mantener el equilibrio.

<iframe src="//www.youtube.com/embed/-PoRE_kZ63k" frameborder="0"></iframe>

Olvidaos de los ruedines, ya os conté en otro post cómo un famoso ciclista enseñó a su hijo a montar en bici con una sin pedales y sin ruedines.

Y Chicco, que está a la última en todo lo relacionado con el aprendizaje de los niños, ha sacado esta bici en dos colores, rojo y rosa, y a un precio bastante asequible, unos 30€.

 ![image](/tumblr_files/tumblr_ngp2z2xYgK1qgfaqto1_1280.jpg)

Otro de los juguetes que me ha llamado la atención del su catálogo es esta portería de fútbol **Gol League** que detecta cuándo el balón toca la red, y ofrece tres modalidades de juego electrónico: penaltis, gol de oro y entrenamiento libre 

 ![image](/tumblr_files/tumblr_inline_ngsukdjXnn1qfhdaz.jpg)

Para conseguir la Mesa Parlanchina de Chicco:

-Dar “me gusta” a la página de Facebook de Chicco España: [aquí](https://www.facebook.com/chiccocompanyespana?fref=ts)

-participar en el concurso[ “Tu carta a Los Reyes Magos”](/portada-regalos-reyes-magos) escribiendo tu carta pinchando [aquí](/portada-regalos-reyes-magos).

Las condiciones para participar están en el [post explicativo del concurso.](/portada-regalos-reyes-magos)

Tenéis hasta el 5 de enero!