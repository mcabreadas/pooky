---
date: '2014-12-29T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_nh9g8djdVH1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- colaboraciones
- gadgets
- ad
- tecnologia
- educacion
- familia
- juguetes
title: 'Robótica para niños (I): Primera aproximación al Mindstorms de Lego'
tumblr_url: https://madrescabreadas.com/post/106490410521/robot-mindstorms-lego-ni-os
---

Hoy es un día especial en casa porque ha llegado un invitado muy esperado por todos. Y es que el Mindstorms de Lego no es un juego de construcción cualquiera, sino que se trata de una línea de juguetes de robótica para niños fabricada por [LEGO](http://www.lego.com/es-es), que posee elementos básicos de las teorías robóticas, como la unión de piezas y la programación de acciones en forma interactiva.

 ![image](/tumblr_files/tumblr_inline_nh9g8djdVH1qfhdaz.jpg)

En casa siempre hemos tenido claro que queríamos estimular a los niños en el aprendizaje de cosas nuevas y prácticas que no se estudian normalmente en el colegio, aprovechando su motivación personal y la curiosidad innata.

Despertar en ellos mediante el juego la pasión por la ciencia, la tecnología, la programación informática, la robótica, las matemáticas, la geometría, la física, las estrategias de resolución de problemas y el trabajo en equipo nos parece fundamental, por eso pusimos a Princesita (9 años) y a Osito (8 años) a clasificar piezas y a colaborar desde cero en la construcción de “Toti-toti” (más tarde explicaré el por qué de este nombre para el robot), auque he de decir que lo de trabajar en equipo les costó bastante en la primera sesión.

 ![image](/tumblr_files/tumblr_inline_nh9g8rDsVk1qfhdaz.jpg)

También fue todo un reto controlar los impulsos de tocar todo del pequeño de 2 años, empeñado en hacer su propio robot.

 ![image](/tumblr_files/tumblr_inline_nhakwt9eCw1qfhdaz.jpg)

Así, finalmente tuvimos que sacar su Lego Duplo para que participara de la actividad familiar, pero a un nivel más adecuado a su edad.

 ![image](/tumblr_files/tumblr_inline_nh9gwqr0gO1qfhdaz.jpg)

No obstante logramos montar a Toti-Toti y ponerlo en funcionamiento controlándolo remotamente con el Ipad. Cada vez que le dábamos una orden respondía “Oki Doky”, por eso el bebé lo bautizó como “Toti-Toti”.

 ![image](/tumblr_files/tumblr_inline_nham1nbiLe1qfhdaz.jpg)

Lo que más ilusión les hizo fue que obedeciera comandos de voz: “back”, “Stop”…

De momento han programado la primera misión que propone las instrucciónes.

Éste fue el resultado:

<iframe src="//www.youtube.com/embed/mLQju0g8fDc" frameborder="0"></iframe>

Pero esto no acaba aquí. Todavía le quedan muchas misiones a Toti-Toti, ya que tiene cantidad de sensores y se puede programar con mil ideas.

Su próxima misión será [despertar a los peques por las mañanas](/2015/01/07/robot-mindstorm-lego-despertador.html) para ir al cole

¿Lo conseguiremos?

¡Os iré contado nuestras aventuras con nuestro nuevo inquilino!

Gracias, Lego por esta oportunidad.