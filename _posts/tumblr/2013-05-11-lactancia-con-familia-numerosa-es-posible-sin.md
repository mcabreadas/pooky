---
layout: post
title: Lactancia con familia numerosa. ¿Es posible sin morir en el intento?
date: 2013-05-11T16:36:00+02:00
image: /images/uploads/depositphotos_167410762_xl.jpg
author: maria
tags:
  - crianza
  - lactancia
  - bebes
tumblr_url: https://madrescabreadas.com/post/50167539056/lactancia-con-familia-numerosa-es-posible-sin
---
Lo mío con la lactancia natural es una historia de aventuras. Con mis dos primeros hijos practiqué la lactancia mixta para poder descansar por las noches, ya que, como ahora, a la mañana siguiente el trabajo me esperaba. Mi marido se encargaba de la toma de la noche dándole biberón y yo, a dormir a pierna suelta. Pero con el tercero me “convertí” a la crianza natural, y decidí alimentarlo con mi leche exclusivamente durante los 6 primeros meses de vida.

No os quiero ni contar lo que me costaba darle el pecho con los mayores gritando y corriendo por la casa como locos, así que el pobre aprovechaba las noches para ponerse ciego a teta, y por el día hacíamos lo que podíamos.

También superamos obstáculos de reflujo y frenillo, consultamos asesoras de lactancia, acudimos a [reuniones de los grupos de apoyo a la lactancia](/2012/12/06/lactando-murcia-por-un-parto-normal.html), y leí información para aburrirme. Los tres primeros meses fueron estupendos, ya que, acostumbrada a los cólicos de mis otros dos hijos y a su estreñimiento, me parecía casi un milagro que Leopardito no llorara y sólo se despertara para mamar, y una vez terminaba, se volviera a dormir como un angelito sin problemas en su [cuna de colecho](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/).

![image](/tumblr_files/tumblr_inline_mlcb1kuE2w1qz4rgp.jpg)

Pero llegó un momento en que me convertí en un gremlin. Empecé a deteriorarme, sobre todo mentalmente (se me iba la pinza, vamos), cuando empezó a dentar, y se enganchaba cada dos por tres, y no se volvía a dormir automáticamente, sino que había que ayudarle acunándole, cantándole…Creímos automáticamente que la culpa era de la teta, y de mi empeño de mantenérsela de forma exclusiva.

![image](/tumblr_files/tumblr_inline_mlcby0LJzD1qz4rgp.jpg)

Una vez paré el tráfico de mi calle porque, tras varios días sin engancharse bien, por fin lo había hecho y estaba mamando plácidamente, y pensé: “que se pare el mundo, mi hijo está comiendo”, así que dejé al de MRW en la puerta pegando timbrazos y con la furgoneta atascando el tráfico. Los cláxones de los coches en caravana cada vez iban sonando más fuertes conforme iba aumentando la cola, pero mi hijo estaba comiendo con ganas, y no lo hacía desde hacía días… en fin… Ahí tenéis una prueba de mi transtorno mental transitorio por la falta de sueño.

Así estuve hasta los 8 meses, época en la que ya no era persona, ya no era yo misma. Empecé a pensar que no era justo para el resto de mi familia. Ni mi marido ni mis hijos mayores merecían que los dejara de lado y me dedicara por completo al bebé.¿Había cambiado mis prioridades y había puesto la LM en el primer lugar a toda costa, haciendo tambalearse la estabilidad familiar?.

Claro, llegó un momento en que Leopardito se cabreaba cada vez que le acercábamos una tetina,

![image](/tumblr_files/tumblr_inline_mlcbqezDdX1qz4rgp.jpg)

con lo que, cuando realmente necesité ayuda para poder dormir algunas horas de forma ininterrumpida para no enfermar, nadie podía sustituírme. Me sentía atrapada, agobiada, ahogada en mi propia decisión de amamantarlo de forma exclusiva. Algo de lo que estaba totalmente convencida y que había emprendido con tanta ilusión, estaba acabando con mi salud.

![image](/tumblr_files/tumblr_inline_mmml1eGGdB1qz4rgp.jpg)

Empecé a tener fallos en mi trabajo, a resentirse mi matrimonio, y mis hijos mayores a tener problemas en el colegio. Me sentí responsable de todo ello, pero ya no había marcha atrás. Un destete a las bravas no era planteable para mí. Tenía que haber otra manera de solucionarlo sin que sufriera el bebé… y sin que sufriera yo.

Entonces fue cuando mi marido inventó el método natural que os comentaba en [mi anterior post](/2013/04/15/mi-familia-numerosa-y-otros-animales.html): “papá acuna al bebé en brazos por la noche cada vez que pida teta antes de las 6 horas desde la última toma hasta que se duerma”, y así mamá puede disfrutar de un sueño reparador que le permita recuperar fuerzas, y sobre todo paciencia para afrontar el duro día que le espera al amanecer. Fue aplicándolo con mucha paciencia y amor.

La primera noche que conseguí dormir 6 horas seguidas, al día siguiente me sentí de nuevo la misma madre cariñosa de siempre, la que disfruta con sus hijos, la que les prepara el desayuno y el almuerzo para el cole con mimo, la que está deseando que llegue la hora de ir a buscarlos a la salida del cole y que no se le cae el mundo encima cuando los ve salir y los oye gritar. Ahora ya no me taladran sus gritos, sino que me contagian su entusiasmo. Ahora escucho con interés lo que les ha sucedido en clase. Ahora disfruto de la comida con ellos mientras le doy la suya a Leopardito. Ahora espero a mi marido con una sonrisa y no dándole las quejas de lo mal que se han portado y de lo agotada que estoy (aunque hya días de todo). Ahora me hace ilusión llevarlos al parque por las tardes, cosa que llevaba sin hacer desde hacía meses, y disfruto extendiendo una mantita en el césped donde nos tiramos todos mientras el bebé aprende a gatear. Ahora me ilusiona mi trabajo, y tengo fuerza y energías para emprender nuevos proyectos y escribir en mi blog.

![ninos saliendo del cole](/tumblr_files/tumblr_inline_mlcc2eCfVR1qz4rgp.jpg)

Ahora sé que la culpa no era de la teta, sino que ésta fue el consuelo que mi hijo necesitó para aguantar las duras noches padeciendo el reflujo que creíamos exterminado. La prueba está en que al empezar a medicarlo comenzó a dormir mucho mejor … un angelito, vamos… Pero lo que me cabrea es que todo el mundo le echó la culpa a la teta una vez más, incluída la pediatra… e incluso yo, que con mis facultades mentales mermadas por la falta de sueño,[las sombras de la lactancia](https://madrescabreadas.com/2013/04/27/las-sombras-de-la-lactancia-los-niños-se-encelan/) comenzaron a apoderarse de mí.

Me he dado cuenta de que el tiempo siempre da la razón a la lactancia materna. Cuando me surgen dudas pienso que algo que Dios ha dispuesto así para alimentar a nuestras crías, algo tan natural, nunca puede perjudicar. Aunque es cierto que no es un camino de rosas, siempre es buena.

Me pregunto si Leopardito hubiera admitido o tolerado otro alimento distinto a mi leche durante las crisis de reflujo. O si otro tipo de alimentación le hubiera aprovechado tanto, porque a pesar de todo, él hacía bastante peso cada semana, y ya sabéis que está lustroso y rosado como un lechón.

¿Os ha pasado algo parecido? ¿Cómo lo habéis superado? Me interesan la experiencias de otras mujeres y también papás para aprender de ellas.

*Foto de portada gracias a sp.depositphotos*

*Imagen del cole gracias a escuela.cuidadoinfantil*