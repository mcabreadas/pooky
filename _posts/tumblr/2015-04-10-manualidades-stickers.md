---
date: '2015-04-10T08:00:34+02:00'
image: /tumblr_files/tumblr_inline_nmdu3zbdBx1qfhdaz_540.jpg
layout: post
tags:
- trucos
- recomendaciones
title: 'Manualidades para niños: Personaliza tu taza con stickers'
tumblr_url: https://madrescabreadas.com/post/116008398275/manualidades-stickers
---

Os recomiendo una manualidad con niños muy sencilla que les divertirá un montón, al menos mis hijos disfrutaron de lo lindo diseñando sus propias tazas de desayuno.

Necesitas tazas lisas de desayuno normales, que no tengan relieve. Nosotros usamos las básicas de Ikea, y stikers resistentes al agua con el diseño que prefiráis. 

Mis niños eligieron estos Bon Zom de [Petit Fernand](http://www.petit-fernand.es/pegatinas-para-ninos), que tienen todo lo necesario para crear la cara que más te guste. 

 ![image](/tumblr_files/tumblr_inline_nmdu3zbdBx1qfhdaz_540.jpg)

Desde los ojos, nariz boca y orejas, hasta complementos como sombreros, coronas o estrellitas y corazones. 

 ![image](/tumblr_files/tumblr_inline_nmdyiaob1U1qfhdaz_540.jpg)

Además incluye unos graciosos brazos que quedan muy graciosos. 

 ![image](/tumblr_files/tumblr_inline_nmdyj4LYB51qfhdaz_540.jpg)

Son resistentes al agua y, en principio al lavavajillas, pero es mejor esperar 24 horas después de haberlos pegado para meterlos, y aún así puede que se despegue alguna pegatina, por lo que, si quieres que duren bastante, es mejor fregar las tazas a mano.

 ![image](/tumblr_files/tumblr_inline_nmdyjoNPkX1qfhdaz_540.jpg)

Al final los mayores nos animamos también y nos hicimos una taza cada miembro de la familia: Papá, mamá, Princesita (9 años), Osito (7 años) y Leopardito (2 años).

 ![image](/tumblr_files/tumblr_inline_nmdykj4JKb1qfhdaz_540.jpg)

Os propongo un acertijo, ¿a que no sabéis qué taza pertenece a cada uno?

 ![](/tumblr_files/tumblr_inline_nmgecoOveH1qfhdaz_540.jpg)