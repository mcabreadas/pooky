---
date: '2015-01-16T07:59:00+01:00'
image: /tumblr_files/tumblr_inline_ni4sagWU5h1qfhdaz.jpg
layout: post
tags:
- crianza
- libros
- planninos
- familia
title: 'Libro para mamás primerizas: El Síndrome de Mamá Osa'
tumblr_url: https://madrescabreadas.com/post/108239991919/libro-para-mamás-primerizas-el-síndrome-de-mamá
---

[![image](/tumblr_files/tumblr_inline_ni4sagWU5h1qfhdaz.jpg)](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2447045)

A la vista de los peques el libro “El Síndrome de Mamá Osa” en mi mesita de noche (sí, he logrado sacar un ratito al día para leer…que ya se me había olvidado…), los dos mayores mirándome ojipláticos:

-“¿Mamá, estás enferma?

-No, hijos, ¿por qué lo preguntáis?

-¿Qué es un síndrome? ¿Te vas a convertir en osa, como la mamá de [Mérida (película Brave)](http://www.disney.es/brave/index.jsp)?”

-Entre carcajadas y percatándome de que habían reparado en el libro que me llevo entre manos: “No, cariño, es un libro que tiene ese título”

-Expectación al máximo: ¿”y de qué trata”?

Imaginaos lo que debió de pasar por la cabecita de mis fieras. Nada más lejos de la realidad…o bueno… en algo tenían razón. A las mamás no nos sale pelo ni garras ni andamos a cuatro patas, pero sí que nos sale la vena del instinto protector al igual que a las hembras de cualquier especie animal, por ejemplo, una osa, cuando tenemos en brazos por primera vez a nuestro osezno, digo, bebé.

Incluso, yo diría más, desde el momento en que nos enteramos de que estamos embarazadas se nos agudizan los sentidos y ya no tomamos ni una sola decisión sin tener en primer plano ese ser que está en camino y que nos acompañará para siempre.

El Síndrome de la Mamá Osa es un libro en clave de humor, pero escrito con una sensatez palmaria fruto de la experiencia como madre de dos hijos de la periodista Luz Bartivas.

En él nos cuenta la primera vez que tuvo que enfrentarse a los pequeños retos diarios en la crianza de sus hijos. Hace un recorrido por los primeros días de vida, la lactancia, los primeros meses, la infancia, la guardería, el colegio en sus dos etapas (infantil, primaria), el paso a secundaria, ilustrándonos sobre el papel del AMPA y el Consejo Escolar, la adolescencia…

Creo que da una visión general y práctica de lo que se nos viene encima cuando esperamos un hijo, no me malinterpretéis, como dice ella, no pretendo asustar a nadie, pero sí es bueno que conozcamos de primera mano las cosas a las que nos vamos a enfrentar porque nadie nace sabiendo ser padre o madre, y si vamos preparados, mejor.

Lo recomiendo para mamás y papás primerizos porque, además de pasar un buen rato, van a aprender bastante y creo que se les van a quitar telarañas de a mente y a sentirse más seguros cuando llegue el gran día de tener a su bebé en brazos.

## _Si te apetece adquirir el libro, lo tienes en [Casa del libro](http://www.casadellibro.com/afiliados/homeAfiliado?ca=23045&idproducto=2447045) a buen precio._

_Nota: Post NO patrocinado_