---
date: '2015-11-04T23:06:21+01:00'
image: /tumblr_files/tumblr_inline_o39z679Lbg1qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- colaboraciones
- ad
- eventos
- familia
- moda
title: Niños a la moda en Madrid Petit Walking
tumblr_url: https://madrescabreadas.com/post/132560429524/pasarela-moda-madrid
---

El 30 de octubre tuvo lugar la pasarela de moda infantil organizada por la revista Petit Style y Miramami, Madrid Petit Walking, en La Casa de las Alhahas, en la Plaza de San Martín, un lugar emblemático que nos ofreció un bonito marco para disfrutar de una mañana de glamour y diversión a partes iguales.

Ya estuve la semana anterior en[Valencia Petit Walking](/2015/10/29/moda-infantil-pasarela.html), y me encantó la experiencia, así que, contra todo pronóstico, decidí repetir, esta vez en Madrid, y con nuevas marcas (aunque algunas repitieron, eso sí, con nuevos looks).

## El backstage

Cuando llegué a la Casa de las Alhahas, mi vena curiosa se sintió atraída hacia las risas de unos niños que correteaban por el backstage, donde ya estaba casi todo a punto para comenzar el desfile.

 ![backstage moda desfile infantil](/tumblr_files/tumblr_inline_o39z679Lbg1qfhdaz_540.jpg)<map name="Map1" id="Map1"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k3aTVoO1qfhdaz_540.jpg" target="_blank" shape="rect" coords="7,5,181,177">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k3fxNWb1qfhdaz_540.jpg" target="_blank" shape="rect" coords="187,6,359,179">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k3kzVqp1qfhdaz_540.jpg" target="_blank" shape="rect" coords="364,4,538,179">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k49Xwf01qfhdaz_540.jpg" target="_blank" shape="rect" coords="5,182,180,358">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k3nOLZj1qfhdaz_540.jpg" target="_blank" shape="rect" coords="186,184,358,356">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k3swBO31qfhdaz_540.jpg" target="_blank" shape="rect" coords="364,183,538,357">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k44JkC71qfhdaz_540.jpg" target="_blank" shape="rect" coords="7,360,179,535">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k40Qwh41qfhdaz_540.jpg" target="_blank" shape="rect" coords="186,362,357,533">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx8k40Qwh41qfhdaz_540.jpg" target="_blank" shape="rect" coords="362,361,535,535"></map>

Pincha sobre cada foto para verla más grande.

Unas últimas pruebas de sonido y luces, y todo ok.

¡Comenzamos!

## La presentadora

El desfile fue introducido por la actriz  [Esmeralda Moya](http://www.emeralds-girls.es/), que también es mamá, y fue muy dulce con los niños. 

Como anécdota, en el carrusel final una de las niñas más pequeñas comenzó a llorar, y sin pensárselo dos veces tuvo una de esas reacciones que tenemos las madres, aunque no sea nuestro hijo el que llora. Tomó a la niña en brazos y la llevó hasta el final de la pasarela con sus compañeros.

Después de su intervención, la pasarela comenzó a llenarse de color y gracia con los pequeños modelos.

 ![esmeralda moya presentadora](/tumblr_files/tumblr_inline_o39z675WgB1qfhdaz_540.jpg)
## [Señorita Lemoniez](http://www.lemoniez.com/senorita-lemoniez/fall-winter-2015-2016/): distinción parisina

Por un momento la pasarela se llenó del glamour de París, y me imaginé en la Rive Gauche rodeada de artistas bohemios.

La verdad es que fue todo un descubrimiento para mí esta marca. Si te gusta la ropa diferente y muy distinguida, o necesitas vestir a tu hija para una ocasión especial, echa un vistazo a su web, porque no te va a dejar indiferente.

 ![ropa senorita lemoniez](/tumblr_files/tumblr_inline_o39z68Af5X1qfhdaz_540.jpg)<map name="Map2" id="Map2"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9c0wRCtq1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,4,272,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9c11F3HD1qfhdaz_540.jpg" target="_blank" shape="rect" coords="276,4,536,267"></map>

Pincha sobre cada foto para verla más grande

Para ver todos los modelos, te dejo el enlace al [video del carrusel final de la Señorita Lemoniez](https://youtu.be/J02jti7WvR4). 

Y si te gusta alguno en particular, y por ser lectora de mi blog, tienes un **cupón de descuento**  del 25 % para la [tienda online](https://senorita.lemoniez.com/tienda-on-line/) introduciendo el siguiente código: 

**Código descuento: SLWALKING**

## [Zippy](https://es.zippykidstore.com/): versatilidad y tendencia

Si ya me gustó en la pasarela de Valencia, ésta de Madrid me ha demostrado que Zippy tiene una gran variedad de looks, y sus prendas se pueden combinar de mil maneras para que cada niño elija su propio estilo.

Me quedo con el toque étnico del poncho combinado con los vaqueros y las botas de antelina para mi niña.

Y la chaqueta de piel con borrego por dentro para mi niño. 

 ![ropa ninos zippy](/tumblr_files/tumblr_inline_o39z68ImVt1qfhdaz_540.jpg)<map name="Map12" id="Map12"><area alt="" title="" href="/tumblr_files/tumblr_inline_nxadyhRVpR1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,5,179,175">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxadyhX1Jv1qfhdaz_540.jpg" target="_blank" shape="rect" coords="184,3,360,176">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxadyiuOhu1qfhdaz_540.jpg" target="_blank" shape="rect" coords="362,3,538,179"></map>

Pincha sobre cada foto para verla más grande

Puedes ver todos los looks en este [video del carrusel final de Zippy](https://youtu.be/4679ruM86aw).

## [iDO](http://www.ido.it/es/): diversión con estilo  

iDO llega de Italia, y se presenta en España como una marca con valores. Apuestan por la seguridad infantil en los tejidos y materiales, adquiriendo a su vez un compromiso de larga durabilidad de sus prendas, cosa muy a tener en cuenta si sois de los que aprovecháis la ropa para varios hermanos, como es mi caso. 

Ofrece looks elegantes y originales como éstos.

 ![moda ido](/tumblr_files/tumblr_inline_o39z699e9c1qfhdaz_540.jpg)<map name="Map3" id="Map3"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9c9hj0EU1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,3,268,268">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9c9lQ1Xi1qfhdaz_540.jpg" target="_blank" shape="rect" coords="272,7,537,268"></map>

Pincha sobre cada foto para verla más grande.

## [Felicia Much](http://www.feliciamuch.com/): artesanía con personalidad

Esta firma gallega de ropa de varios estilos, tanto informal como de ceremonia conquistó al público por su mezcla de originalidad y sencillez.

Sus prendas hechas a mano denotan una gran personalidad y una mezcla de romanticismo y melancolía que las hacen muy especiales.

Mi look favorito, los pantalones capri a cuadros  combinados con la camisa gris a juego con el turbante.

 ![ropa felicia much](/tumblr_files/tumblr_inline_o39z69PfuR1qfhdaz_540.jpg)<map name="Map4" id="Map4"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9cquacAD1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,5,269,267">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9cqxQzJe1qfhdaz_540.jpg" target="_blank" shape="rect" coords="273,5,539,267"></map>

Pincha sobre cada foto para verla más grande.

Para ver todos los modelos de Felicia Much que pasaron los niños, mira [este video](https://youtu.be/1bKHMzHq6ko).

## [Tartaleta](http://www.tartaleta.com/es_ES/): detalles con dulzura

Si algo define a esta marca es su gusto por los detalles, y su dulzura en la confección. Las puntillas, los lazos, los encajes… hacen de cada prenda una joya.

Me quedo con el vestido con falda capa de la Colección Cake de Jacquard en tono nube, terciopelo, lazada de plumeti bordado, bolillo en tonos arena e hilo de chenilla aguamarina…. Todo dulzura!

 ![desfile tartaleta](/tumblr_files/tumblr_inline_o39z6aRtnK1qfhdaz_540.jpg)<map name="Map13" id="Map13"><area alt="" title="" href="/tumblr_files/tumblr_inline_nxadz8OUUK1qfhdaz_540.jpg" target="_blank" shape="rect" coords="5,4,271,268">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxadz8dFO81qfhdaz_540.jpg" target="_blank" shape="rect" coords="273,5,538,268"></map>

Pincha sobre cada foto para verla más grande.

## Bóboli: sport sofisticado

Bóboli presenta su colección “Make a Wish”, donde el blanco y negro se presenta como protagonista dando un aire sofisticado al look, pero sin dejar de ser sport. Los destellos brillantes salpican las prendas, que se superponen unas a otras jugando con los cuadros y las rayas.

Me encanta el conjunto de la niña de la derecha, con camiseta de rayas y falda bailarina negra a juego con la bufanda de estrellas en blanco y negro. ¡Me lo pido para mi Princesita!

 ![ropa boboli](/tumblr_files/tumblr_inline_o39z6aw1fw1qfhdaz_540.jpg)
## [Bébécar](http://www.bebecar.com/bebecar/es/): seguridad y moda

Innovación, elegancia, tendencia sin olvidar la seguridad y comodidad del bebé, es lo que nos ofrece una vez más Bébécar en su colección Privé 2016. 

Su [tejido “Magic” antimanchas que os explicaba en este post](/2014/12/06/claves-eleccion-cochecito-bebe.html), nos permite, optar por colores claros, como éste amarillo pastel sin miedo a la limpieza.

Podemos combinar el color que más nos guste con un chasis clásico de ruedas grandes, en blanco o negro, según nuestro gusto en un resultado impecable y sin remaches. O bien optar por el chasis más moderno y práctico.

La silla de paseo “Spot +” [que ya testeamos en el blog](/2015/09/20/test-spot-bebecar.html) en su versión anterior, con acabado cromado y manillar corrido, pero plegable en bastón, luce así de elegante en un tono azul clarito.

El color marsala pasa de las grandes pasarelas de moda a la puericultura en forma de cochecito para que las mamás y los bebés también vayamos a la última.

 ![desfile bebecar](/tumblr_files/tumblr_inline_o39z6azlCY1qfhdaz_540.jpg)<map name="Map14" id="Map14"><area alt="" title="" href="/tumblr_files/tumblr_inline_nxae019GiQ1qfhdaz_540.jpg" target="_blank" shape="rect" coords="3,3,177,176">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxae02sTQa1qfhdaz_540.jpg" target="_blank" shape="rect" coords="182,3,359,176">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxae03YwPc1qfhdaz_540.jpg" target="_blank" shape="rect" coords="361,5,537,177"></map>

Pincha sobre cada foto para verla más grande.

## [Mamá mi Sol](http://www.mamamisol.com/): toda la familia a juego

Una de mis favoritas de esta pasarela es la firma Mamá mi Sol porque sirve para vestir a toda la familia a juego tanto en ropa de baño como en pijamas.

Padres e hijos coordinados con diseños personalizados hechos a mano que podemos elegir a nuestro gusto serán la sensación allá donde vayan.

Además, detrás de esta marca hay tres mujeres emprendedoras, una madre, y dos hijas, que apostaron por un sueño para poder compatibilizar su trabajo con el cuidado de su familia.

 ![pijamas personalizados](/tumblr_files/tumblr_inline_o39z6bkg501qfhdaz_540.jpg)<map name="Map5" id="Map5"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9cwgLylD1qfhdaz_540.jpg" target="_blank" shape="rect" coords="5,4,268,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9cwkEsub1qfhdaz_540.jpg" target="_blank" shape="rect" coords="273,3,534,265"></map>

Pincha sobre cada foto para verla más grande.

[Aquí tienes todos los modelos de pijamas de Mamá mi Sol](https://youtu.be/iKmsQVVb00Q) que desfilaron.

## [Trasluz](http://www.trasluz.net/): elegancia divertida

Yo definiría esta marca como de ropa para niños que ya no lo son tanto, y que no quieren llevar un look demasiado infantil, pero que tampoco irían bien con ropa de muy mayor.

Trasluz ha encontrado ese punto medio que adoramos las madres, y que será fácil que ellos también lo hagan porque verdaderamente tienen auténticas chuladas, muy originales y súper estilosas.

Los pantalones de cuadros de colorines me parecen muy divertidos, y el abrigo de caperucita, que parece que ha cazado al lobo es espectacular.

 ![desfile trasluz](/tumblr_files/tumblr_inline_o39z6bJwLw1qfhdaz_540.jpg)<map name="Map6" id="Map6"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9d83mt151qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,5,268,265">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9d87zauS1qfhdaz_540.jpg" target="_blank" shape="rect" coords="274,2,537,266"></map>

Pincha sobre cada foto para verla más grande.

Para ver [todos los modelos de trasluz pincha en el siguiente video](https://youtu.be/465IqHCZ_kE).

## [Cóndor](http://shop.condor.es/): comodidad y originalidad

Su colección Chocolat me parece espectacularmente original y cómoda para los niños. Infinidad de vestidos para niñas, que puedes combinar con leotardos y chaquetas.

Me parece ropa muy ponible, pero con un punto de tendencia muy interesante.

 ![desfile condor](/tumblr_files/tumblr_inline_o39z6cwwbh1qfhdaz_540.jpg)<map name="Map7" id="Map7"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9e13ogY51qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,4,268,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9e17kwCA1qfhdaz_540.jpg" target="_target" shape="rect" coords="276,5,537,268"></map>

Pincha sobre cada foto para verla más grande.

Para ver [todos los modelos de Cóndor mira este video](https://youtu.be/nYO3P9WbmBY).

## [Curro de Loucas](http://www.currodeloucas.com/): ríos de elegancia

Presentó sus nuevas colecciones, todas con nombres de ríos.

Me quedo con estos dos modelos de la colección Danubio, uno para niño con la clásica camisa y jersey cuello caja azul marino y tejanos, ideal para diario, y este vestido camisero azulón con lazos en el cuerpo y cuello con volantes que me parece muy favorecedor.

![curro de loucas](/tumblr_files/tumblr_inline_o39z6cFokh1qfhdaz_540.jpg)<map name="Map8" id="Map8"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9eeb86GM1qfhdaz_540.jpg" target="_blank" shape="rect" coords="5,5,269,269">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9eefBWOP1qfhdaz_540.jpg" target="_blank" shape="rect" coords="275,6,535,267"></map>

Pincha sobre cada foto para verla más grande.

En este video puedes ver todos los [modelos que desfilaron de Curro de Loucas](https://youtu.be/ZzSIIDX0Uh0).

## [What´s Up! Kids](http://www.wspkids.com/): ropa loca y divertida

Esta marca derrocha colorido y vitalidad. 

De repente la pasarela se vio invadida por los indios, y nos divertimos mucho viendo desfilar a unos niños que se notaba que estaban disfrutando.

Me ha encantado descubrir What´s Up Kids porque creo que hace ropa de esa que elegirían tus hijos si los dejaras solos vestirse a su gusto, pero al mismo tiempo nos gusta también a las madres.

Me quedo con el conjunto de falda de vuelo e turquesa y rosa combinada con la camiseta rosa chicle, y el vestido de florecitas granates de la colección College par mi niña, y los tejanos amarillo huevo con camiseta verde y chaqueta azul para mi niño.

 ![wspkids moda infantil](/tumblr_files/tumblr_inline_o39z6dUUJ11qfhdaz_540.jpg)<map name="Map15" id="Map15"><area alt="" title="" href="/tumblr_files/tumblr_inline_nxae0swBuo1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,5,182,176">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxae0sLBCK1qfhdaz_540.jpg" target="_blank" shape="rect" coords="183,3,358,178">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxae0tqM0L1qfhdaz_540.jpg" target="_blank" shape="rect" coords="363,6,536,178"></map>

Pincha sobre cada foto para verla más grande.

Podéis ver [todos los looks de What´s Up! Kids en este video](https://youtu.be/ZluXJwm7F-w).

Si te apetece visitar la [web de What´s up! Kids](http://www.wspkids.com), y quieres comprar algún conjunto, puedes hacerlo utilizando este código descuento hasta el 22 de noviembre por ser lector de mi blog:

**Código descuento 30% What´s up Kids: WSPWALKING**

## [Lasuanzes](http://lasuanzes.com/): comuniones diferentes

Aunque dedicaré un post especial sobre Comuniones con todo lo que estoy viendo en los desfiles, os adelanto un poquito.

Si buscas trajes de comunión o de arras que rompan con lo tradicional, pero muy elegantes, esta es tu firma.

Plumeti y algodones finos dan un aspecto vaporoso a los vestidos. Y los pies nos sorprenden ¡con zapatillas de ballet!.

 ![ropa ninos lesuanzes](/tumblr_files/tumblr_inline_o39z6dFifW1qfhdaz_540.jpg)

Merece la pena ver el [video con el carrusel final de Lesuanzes](https://youtu.be/xQYeZiMSihM).

## Kauli: la moda de siempre

Una vuelta al pasado, a cómo vestían a nuestros padres cuando eran pequeños, a los pantalones extra cortos para niños, y gorritos para niñas, a los lazos y los pompones.

Lo clásico siempre está de moda, y Kauli lo domina a la perfección.

Es una ropa ideal para vestir a todos los hermanos de la familia coordinados, ya que en cada colección hay tallas adaptadas a todas las edades. 

¡Vamos, que se me viene a la mente la familia Trapp, de la película Sonrisas y Lágrimas!

 ![desfile kauli](/tumblr_files/tumblr_inline_o39z6eQcZA1qfhdaz_540.jpg)<map name="Map9" id="Map9"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9f5xFjXG1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,6,180,176">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9f5teK4l1qfhdaz_540.jpg" target="_blank" shape="rect" coords="184,6,357,177">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9f626E871qfhdaz_540.jpg" target="_blank" shape="rect" coords="363,5,534,177"></map>

Pincha sobre cada foto para verla más grande.

## Lluvia de corazones rojos

Y para terminar el desfile, un bonito fin de fiesta con una lluvia de corazones rojos al son de la música de Cris Méndez, que cantó en directo, y que hicieron magia sobre la pasarela blanca logrando que grandes y pequeños nos tiráramos a jugar al suelo con ellos. 

 ![desfile petit walking](/tumblr_files/tumblr_inline_o39z6foxBf1qfhdaz_540.jpg)<map name="Map10" id="Map10"><area alt="" title="" href="/tumblr_files/tumblr_inline_nx9fz2FFe11qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,2,177,177">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9fz7D9du1qfhdaz_540.jpg" target="_blank" shape="rect" coords="182,5,358,175">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nx9fzeOvEU1qfhdaz_540.jpg" target="_blank" shape="rect" coords="364,3,536,177"></map>

Pincha sobre cada foto para verla más grande.

## Showroom  

Después tuvimos la oportunidad de charlar con las marcas y de ver las prendas de cerca tocando los tejidos y comprobando su excelente calidad en un showroom de lo más entretenido.

 ![showroom petit walking](/tumblr_files/tumblr_inline_o39z6fG1LS1qfhdaz_540.jpg)<map name="Map11" id="Map11"><area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa19D6Rq1qfhdaz_540.jpg" target="_blank" shape="rect" coords="2,6,176,132">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa1fXwD21qfhdaz_540.jpg" target="_blank" shape="rect" coords="183,6,355,133">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa1l379K1qfhdaz_540.jpg" target="_blank" shape="rect" coords="362,7,534,135">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa1pN8GU1qfhdaz_540.jpg" target="_blank" shape="rect" coords="6,140,176,263">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa1u1nwI1qfhdaz_540.jpg" target="_blank" shape="rect" coords="185,139,357,266">
<area alt="" title="" href="/tumblr_files/tumblr_inline_nxaa1yPIdh1qfhdaz_540.jpg" target="_blank" shape="rect" coords="363,138,534,265"></map>

Pincha sobre cada foto para verla más grande.

## Los momentos más divertidos del desfile

Quiero dar la enhorabuena a la revista Petit Style y a Miramami por la estupenda organización porque soy consciente de que sacar a escena a 14 marcas requiere mucho trabajo y esfuerzo, sobre todo para lograr que todo saliera a la perfección, además de regalarnos más de una carcajada con momentos como el selfie de los niños de Zippy en plena pasarela. Selfie que me consta que fue real.

 ![desfile zippy selfie](/tumblr_files/tumblr_inline_o39z6gSGtJ1qfhdaz_500.gif)

O este baile tan coqueto de la chiquitina vestida de Tartaleta, que provocó los suspiros de todas las madres allí presente, que éramos muchas, os lo aseguro porque, además, en la revista Petit Style, todos los colaboradores son madres y padres. 

Así se explica el ambiente familiar y distendido que se respira, y con el que los pequeños modelos se sienten tan cómodos.

 ![desfile tartaleta](/tumblr_files/tumblr_inline_o39z6g2z5T1qfhdaz_500.gif)

¿A que es para comérsela?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).