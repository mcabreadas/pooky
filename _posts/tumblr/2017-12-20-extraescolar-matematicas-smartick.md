---
date: '2017-12-20T12:56:34+01:00'
image: /tumblr_files/tumblr_inline_p19csoJr381qfhdaz_540.png
layout: post
tags:
- crianza
- testing
- educacion
title: Probamos el método on line de matemáticas Smartick
tumblr_url: https://madrescabreadas.com/post/168748746924/extraescolar-matematicas-smartick
---

Si me sigues en las redes sociales habrás visto que hemos estado probando durante tres meses el [método on line para aprender matemáticas Smartick](https://www.smartick.es/?f=1). La colaboración ha consistido en una prueba con mis tres hijos de 4, 9 y 11 años, de tres meses de duración para que pudiera dar mi opinión.

Aprovechamos las vacaciones de verano para hacer la prueba, con el reto que supone organizar una rutina en esos días de desbarajuste y anarquía vital.

## Flexibilidad

Al principio costó que cogieran el hábito, sobre todo los mayores, que se ponen más rebeldes para estas cosas, pero ayudó el que las sesiones fueran sólo de 15 minutos diarios, que pudieran escoger ellos la hora, y que requirieran ordenador o tablet, ya que a  mis hijos les encantan las pantallas, como a los tuyos seguramente, y como se las tengo restringidas, era una excusa perfecta para estar un ratito “jugando”.

 ![](/tumblr_files/tumblr_inline_p19csoJr381qfhdaz_540.png)
## Como un juego

Se pueden registrar a partir de los 4 años, aunque no sepan leer, y realmente está planteado como algo lúdico, ofreciendo un sistema de recompensas por los logros alcanzados en forma de “ticks” canjeables por mejoras para el avatar que diseña cada niño o su mascota, como complementos, disfraces, decoración para su casa…

 ![](/tumblr_files/tumblr_inline_p19chlEMsE1qfhdaz_540.png)

## Se premia el esfuerzo

Además, tras cada sesión, tienen ocasión de disfrutar con distintos juegos que desarrollan las habilidades que el método trabaja, como la lógica, el cálculo mental… y pueden estar en contacto con otros usuarios de Smartick (de forma totalmente anónima) y formar clubes. En nuestro caso, mis hijos formaron uno de tres miembros donde estaban ellos solos, y cada vez la estrella de la semana era quien había obtenido mejor rendimiento (la competitividad sana puede llegar a ser una gran motivación para superarse).

Otra cosa que les ha estimulado mucho son los diplomas que se van emitiendo conforme van superando áreas. Nosotros los imprimimos en color y los tenemos puestos en la pared.

 ![](/tumblr_files/tumblr_inline_p19ckydVe51qfhdaz_540.png)
## Nuestra experiencia

El método empieza con un tanteo al niño para ver el nivel que tiene y ofrecerle así ejercicios con los que se encuentre cómodo y seguro por dominarlos.

En el caso de mi mediano, no lograron acertarle demasiado, y se desesperó un poco; también hay que tener en cuenta que su carácter es bastante impaciente.

Si embargo, mi pequeño y mi mayor enseguida  se sintieron cómodos, y estuvieron bastante motivados durante toda la prueba, aunque había días de desgana y momentos de altibajo porque no podemos pretender que los niños estén siempre al 100%, sobre todo en verano.

## De 4 a 14 años

En el caso de mi pequeño de 4 años estoy especialmente contenta porque durante el curso se había sentido bastante inseguro en el colegio a la hora de realizar los trabajos, y creo que su autoestima estaba en horas bajas. El método Smartick le hizo coger seguridad en sí mismo al ver que iba progresando, y que lo iban premiando. Y eso que al principio hasta le costaba manejar bien el ratón: arrastrar los elementos, hacer click en el lugar correcto…

 ![](/tumblr_files/tumblr_inline_p19cmnbCAy1qfhdaz_540.png)

Pero a lo largo de las sesiones mejoró su psicomotricidad fina y acabó manejando el ratón con soltura.

La verdad es que es al que menos le costaba sentarse cada día frente al odenador a hacer su sesión. Incluso la hizo el día de su cmpleaños y le hicieron un regalo!

La experiencia para él fue realmente positiva ya que, aunque no sabía leer, las locuciones de voz le iban explicando qué debía hacer en cada ejercicio.

Para la niña de 11 años fue genial para repasar lo aprendido durante el curso y aprender algunas cosas nuevas. Ella siempre había dicho que era de letras y que las mates se le daban fatal, pues bien, doy por buena la prueba de Smartick en su caso porque ya no lo tiene tan claro y ha ganado seguridad en sí misma con los números.

 ![](/tumblr_files/tumblr_inline_p19cj3iuhB1qfhdaz_540.png)
## El papel de los padres

En general me parece un sistema ameno para los niños y cómodo para los padres porque es on line, y en todo momento estamos informados de los progresos a través de un email informativo que indica cada dia si se han conectado, las áreas que han trabajado y su rendimiento.

También tenemos acceso como tutores al trabajo de nuestros hijos para poder ayudarles en las posibles dificultades que tengan.

 ![](/tumblr_files/tumblr_inline_p19cz8ehaj1qfhdaz_540.png)

Además,los niños pueden comunicarse con personal de Smartick para quejarse si algún ejercicio no les parece bien, o si se están repitiendo demasiado.. y ellos contestan y tienen en cuenta sus sugerencias.

## Mi opinión

El método Smartick no creo que sustituya a una clases particulares personalizadas para dificultades específicas que tengan los niños en el estudio de las matemáticas, no nos confundamos. 

Sin embargo, está bien como complemento del colegio, para estimular a los niños y motivarlos, para que encuentren el gusto por los números, para que cojan seguridad en sí mismos a la hora de enfrentarse a un problema o un ejercicio en clase, para trabajar la constancia, y en definitiva, para entender las matemáticas de otra forma y descubrir que pueden ser divertidas y prácticas, cosa que a veces no se logra en las aulas.

En este enlace te dejo más [información sobre el método Smartick](https://www.smartick.es/?f=1), así como precios, (hay descuentos por hermanos y por tiempo de contratación) por si te interesa para tus hijos, además, puedes probarlo durante 15 días gratis sin compromiso.  

Ya me contarás!