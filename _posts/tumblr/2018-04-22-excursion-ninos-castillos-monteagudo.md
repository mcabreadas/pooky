---
date: '2018-04-22T20:51:01+02:00'
image: /tumblr_files/tumblr_inline_p7ln1tdnIv1qfhdaz_1280.jpg
layout: post
tags:
- familia
- planninos
- local
title: Familias numerosas a la conquista de los castillos de Monteagudo
tumblr_url: https://madrescabreadas.com/post/173199088449/excursion-ninos-castillos-monteagudo
---

Hoy nos hemos ido de aventuras a la ruta de los castillos de Monteagudo (Murcia) con [Fanumur](http%3A%2F%2Fwww.fanumur.org%2F&t=OTQ3ZTVhNzQ3ZDBkMWY1ZTU0NzZiYTc3YTUyY2RiN2ViYzJkNDcwMSxhZDliY2NmYTYzMDk2N2I0YzEyNWQzZDRmOTljNjVhOGZkMDQyOGE2),  [ASAFAN](http%3A%2F%2Fasafan.org%2Fes%2F&t=MmEzN2MyMTdiMjk0OGY3NWI2ZTgzNGFkZTYyNTI3Y2Y3ZjNkOTZhOCwxZjYxZjMxNjZiOGJlZDdlMGY3MDlkNDg4MDZmZDIyMmVkN2ZjYzZk) y [EcoAmbiental](http%3A%2F%2Fwww.ecoambientalmurcia.com%2F&t=ODkyMWNmNzdmNWI1ZmRlNTRhZDZjYjNjZTViYjA4ZjZiNTU5Mjk0OSwwOWI0MjczYzMzZTYyYjY3YTk3YTNlYzkzNmNhMGVlMjc2NDFmYTlh).

 ![](/tumblr_files/tumblr_inline_p7ln1tdnIv1qfhdaz_1280.jpg)

Nos encantan este tipo de excursiones donde los niños dejan volar su imaginación y se imaginanan otra época de nuestra historia. ya os conté la amravillosa experiencia en [Medina Siyasa y el cañón de Almádenes, en Cieza](http%3A%2F%2Fmadrescabreadas.com%2Fpost%2F141727573209%2Fsiyasa-almadenes-excursion&t=NjU3OGNkY2ZjZDc4YWU2MDFjYWE5OGJkYjMyYzJkNDBkMGQ1ZjgxYSxkNzRhODE4M2IzNjk4MjY2ODViODNmNWVjZjQyMzI1OTNhNWQ2MjU5) (Murcia).

Hoy nos hemos acercado a Monteagudo, muy cerquita de la capital, y nos ha sorprendido su riqueza histórica y cultural. Muchas veces no somos consciente de lo que tenemos justo al lado.

 ![](/tumblr_files/tumblr_inline_p7lod3uQuq1qfhdaz_400.gif)

Nos hemos trasladado a la Murcia de mediados del S. XII, capital de Al-Andalus, y hemos vencido a [Mardanis, el Rey Lobo](https%3A%2F%2Fes.wikipedia.org%2Fwiki%2FMuh%25C3%25A1mmad_ibn_Mardan%25C3%25ADs&t=NjQyYmEyN2MxMDg2Y2ZkNDcxMTM4MzBhYzI1NTNhMTY2ZDhjNTE0MCwzNWU4M2VmOWUyZThlM2NmZWI1NWY1MjFlMTY2MmZlOTYwZGVkNWJm).

 ![](/tumblr_files/tumblr_inline_p7ln1ulu671qfhdaz_1280.jpg)

La ciudad de Murcia se consolidó como una gran ciudad y capital de Al- Andalus a mediados del Siglo XII gracias a la figura de Mardanis, más conocido como el Rey Lobo, un monarca musulmán que se enfrentó por igual a los reinos cristianos y a los extremistas musulmanes almohades e hizo de Murcia la capital más importante de levante durante su mandato.

 ![](/tumblr_files/tumblr_inline_p7ln1urDfy1qfhdaz_1280.jpg)

Le tenemos que agradecer la red de riegos de la huerta de Murcia, puso en marcha un importantisimo comercio internacional de artesanía, seda, papel… hasta su moneda era referente en toda Europa.

 ![](/tumblr_files/tumblr_inline_p7ln1uxzLW1qfhdaz_1280.jpg)

En el [Centro de Visitantes de Monteagudo](http%3A%2F%2Fwww.monteagudo.info%2Fweb%2Finstalaciones-y-servicios%2Fcentro-visitantes-monteagudo%2F&t=OGM2NmE2NjUzY2IwYWJkZTQwZGNhMDc1YmU4Y2VjNDI4ZmU4YzNmYSwzOWQ0ZDc5YzNkMjJhMGIyMGUzZWE5ZjdkMGIzOTVhNGIwODczOWVk) hemos disfrutado con los restos de casas con mobiliario incluido de la [cultura argárica](https%3A%2F%2Fes.wikipedia.org%2Fwiki%2FCultura_arg%25C3%25A1rica&t=MGYzZTJkZWU5MzdjMmMyYWM5Njk5NjY5Y2FmNDQ5ZTExODczMjdiOSw2Y2JjNTcyZDIxZTg2MzQ5NTZiZmJmOTVkMjdhZDc1NTYzODI1MWUx), así como herramientas de la edad de bronce.

 ![](/tumblr_files/tumblr_inline_p7ln1v9wUB1qfhdaz_1280.jpg) ![](/tumblr_files/tumblr_inline_p7ln1vtJRX1qfhdaz_1280.jpg) ![](/tumblr_files/tumblr_inline_p7ln1vrvOc1qfhdaz_1280.jpg)

Recomiendo esta ruta para niños a partir de 8 años, aunque mi hijo de 5 la ha hecho, pero reconozco que ha sido dura para él (aunque es un polvorilla)

Gracias a Fanumur por invitarnos a pasar un día fantástico en la mejor compañía, y a Asafan. Lo hemos pasado genial. Hasta la próxima!

 ![](/tumblr_files/tumblr_inline_p7ln1wLpK71qfhdaz_1280.jpg)