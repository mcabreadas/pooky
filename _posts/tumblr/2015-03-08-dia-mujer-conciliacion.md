---
layout: post
title: Día de la mujer trabajadora que no logra conciliar y se cabrea
date: 2015-03-08T10:40:57+01:00
image: /tumblr_files/tumblr_inline_pgauzmusun1qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/113053923704/dia-mujer-conciliacion
---
Hoy es el Día Internacional de la Mujer Trabajadora, y siento rabia y frustración por lo que se supone que la mujer tiene que conseguir en la vida, porque yo no lo logro.\
Si decide ser madre y trabajar le espera una vida de continuo conflicto interno y lucha para intentar hacer las dos cosas bien, si no, la culpa interna y la disyuntiva diaria acabarán con su equilibrio emocional.\
Llevo tiempo pensando que en las Universidades debería de advertirse esto. Ojalá me lo hubieran dicho a mí.\
Me siento engañada y enfadada, y me pregunto:

* **¿De qué me sirvió tanto estudio, tanto esfuerzo en mi carrera profesional si ahora no puedo compatibilizarla con la crianza de mis hijos?*** 

Y digo crianza en primera persona, porque [también intenté que me los criaran](/2012/12/09/reportajemujerhoy.html) para no perder el carro, pero no pude hacerlo, no tuve el coraje suficiente.

 ![image](/tumblr_files/tumblr_inline_pgauzmusun1qfhdaz_540.jpg)

Después [aposté por la conciliación laboral](/2013/11/13/diamujerpopulartv.html). 

 ![image](/tumblr_files/tumblr_inline_pgauznY69k1qfhdaz_540.jpg)

[Tuve un sueño](/2013/10/25/entrevista-con-radio-euskadi.html) y creí que se podía lograr, 

(espero que mis compañeros de lucha, [José Miguel](http://www.optimainfinito.com/), [Pepe](https://twitter.com/search?q=pepe%20cuesta&src=typd), [Rocío](http://hrlab.es/),  Mónica, [Jaime](http://jaimepereira.es/), [Roberto](http://conciliatorvsdarkworker.blogspot.com.es/), [Isabel](https://twitter.com/masfamilia), Catalina y Patricia me den permiso para usar estas fotos),

 ![image](/tumblr_files/tumblr_inline_pgauznVvEX1qfhdaz_540.jpg)

pero no nos escucharon, me equivoqué de país, y me caí de bruces al comprobar que no hay voluntad política de cambiar las cosas, y así como están, no se puede… al menos yo no puedo.

Ahora [opto por mi familia](https://madrescabreadas.com/2015/01/19/quedamos-tú-y-yo/), bueno, no nos engañemos, ya opté hace tiempo en el camino hasta llegar hasta aquí, pero ahora soy consciente del engaño, y me dan ganas de coger un soplete y empezar a quemarlo todo (quedo eximida de toda responsabilidad por mis palabras).\
Pero lo que realmente me cabrea es que nadie me advirtiera a tiempo, que nadie me dijera algo tan básico como:

* **“Chica estudiante, si proyectas tener hijos y criarlos tú misma, no te esfuerces demasiado en tus estudios porque en el mejor de los casos en que consigas mantener tu trabajo al anunciar tu embarazo, seguramente no logres hacer una gran carrera profesional. Y si intentas conciliar ambas cosas, puede que mueras en el intento”*** 

Que lo pongan en la entrada, por favor, en un cartel a modo de advertencia, como en las cajas de cigarros, para que no caiga en la trampa ninguna inocentes criatura más.\
Hace un tiempo pensaba que las cosas podían cambiar, pero si esto llega, desde luego, a mí se me pasó el tren, quizá mi hija, o mis nietas lo vean, pero para mí es demasiado tarde…y me cabrea.

Feliz día de la mujer trabajadora que no logra conciliar…

 ![image](/tumblr_files/tumblr_inline_pgauznavi41qfhdaz_540.jpg)