---
date: '2015-11-19T18:22:01+01:00'
image: /tumblr_files/tumblr_inline_nygxcb9Xa41qfhdaz_540.jpg
layout: post
tags:
- premios
- planninos
- familia
title: 'Teatro para niños: La Biblioteca de los Ratones'
tumblr_url: https://madrescabreadas.com/post/133537367714/teatro-para-niños-la-biblioteca-de-los-ratones
---

ACTUALIZACIÓN 27-11-15

Ya tenemos ganadora de la entrada familiar para ir al teatro este fin de semana. La afortunada es SBL. Enhorabuena, y a disfrutar! Ya nos contarás

 ![](/tumblr_files/tumblr_inline_nygxcb9Xa41qfhdaz_540.jpg)

Post originario:

Esta semana el el blog no paramos de hablar de ratones, y es que estos personajillos siempre han dado mucho juego en la literatura infantil, de la que soy fan absoluta, ya lo sabéis, y si se representa en teatro, mucho más.

Tuvimos la suerte de que nos invitaran de [La Caseta Teatro](http://www.lacasetadt.es/) a una obra muy especial protagonizada por estos minúsculos animalitos, a la que acudió una de nuestra colaboradora, Rosa, de [Paraíso Kids](http://www.paraisokids.es/), con su hija de tres años. Nos cuenta su experiencia:

> El domingo tuvimos la oportunidad de disfrutar en la [Sala BULULU](http://teatro.bululu2120.com/) 2120 de un Teatro para niños.
> 
> Fui con mi pequeña que tiene tres años y que es un poquito inquieta, al principio dudaba de si la obra le gustaría o no.
> 
> Cuando entramos en la sala nos encontramos a la abuelita durmiendo, es de destacar la caracterización de la actriz, los niños abrían los ojos todo sorprendidos y les daba miedo hasta pestañear por si se perdían algo.

 ![teatro ninos](/tumblr_files/tumblr_inline_nyetemwD6D1qfhdaz_540.jpg)

> Cuando nos sentamos en las colchonetas preguntaron si algún niño quería acudir al baño, un gran detalle, para evitar que los pequeños se perdieran el cuento. 
> 
> Una vez todos organizados comienza el espectáculo. Desde que empezó el cuento narrado por la abuelita hasta el final noté como los niños se habían integrado en el espectáculo porque ellos formaban parte de él, e interactuaban con los protagonistas, en este caso, “ los ratones”. 

 ![teatro ninos](/tumblr_files/tumblr_inline_nyetenMPus1qfhdaz_540.jpg)

> Y es que nos narraron el cuento de La Ratita Presumida de una forma especial y cuando nos quisimos dar cuenta ya había concluido la función.
> 
> Entonces mi pequeña, que sólo se habia movido para interactuar en la historia me dijo :“ Mami yo me quedo al siguiente”. Cuando escuché esto ya no me cabía ninguna duda, los niños lo habían vivido como un personaje más de la historia. Para que os hagaís una idea os cuento esto porque ella no es capaz de estar diez minutos quieta.
> 
> Desde el domingo han transcurrido unos días, y cuando voy a buscarla a la guardería me pregunta si podemos ir a la Biblioteca de los Ratones. Con esto os digo todo.

 ![nina biblioteca](/tumblr_files/tumblr_inline_nyetenBS5O1qfhdaz_540.jpg)

Mi pequeña y yo repetiremos, y sin ninguna duda os animo a que os acerquéis un fin de semana y comprobéis cómo la representación con dos únicos actores y unos peluches está realizada con tanta delicadeza que llega al corazón de los más pequeños.

Gracias a Madres Cabreadas y a la Sala BULULÚ2120 por hacer que mi hija viva esta experiencia.

Muchas gracias a Rosa por compartirlo con nosotros, y a [La Caseta Teatro](http://www.lacasetadt.es/) por invitarnos a este espectáculo tan bonito.

## Sorteo de una entrada familiar  

Si tú también quieres acudir con tus hijos a ver la biblioteca de los Ratones el fin de semana del 28 y 29 de Noviembre en la Sala Bululú de Madrid, participa en el sorteo de una entrada para toda la familia.

Los requisitos son dos:

- Deja un comentario en este post contándonos si alguna vez has llevado a tus peques al teatro.  
- Seguir en [Facebook a Quica&Kids](https://www.facebook.com/Quicakids/?fref=ts).

El último día para participar es el 26 de noviembre.

El nombre del ganador se publicará en este blog el día 27 de noviembre.

Me reservo el derecho de anular cualquier participación que no considere apropiada bajo mi criterio.

## Calendario de actuaciones

- Los fines de semana de Noviembre en La sala Bulubú, C/ Canarias, 16, Madrid.  
- El 19 de diciembre a las 12.00 h, en la ludoteca Mi pequeño Monito,  Arroyomolinos  
- Domingos de enero: 10, 17, 24, 31, a las 18.00. Sala Montacargars. En Inglés  

Para [comprar entradas para la Biblioteca de los Ratones pincha aquí](http://www.atrapalo.com/entradas/la-biblioteca-de-los-ratones_e301079/).

 ![programa teatro ninos](/tumblr_files/tumblr_inline_nyetenfgX21qfhdaz_540.jpg)

Vamos! Participa! ¿Has llevado alguna vez a tus hijos al teatro?