---
layout: post
title: Las dos caras de la Navidad
date: 2013-12-22T18:04:00+01:00
image: /tumblr_files/tumblr_inline_mxn7x0nECP1qfhdaz.jpg
author: maria
tags:
  - mecabrea
  - navidad
tumblr_url: https://madrescabreadas.com/post/70798069056/las-dos-caras-de-la-navidad
---

Navidad.Tiempo de alegría, de ilusión, de reencuentros esperados, de risas de niños con ojos iluminados por la emoción, de luces en las calles, de adornos en las casas, de sonrisas inesperadas y abrazos…

Pero también es época de nostalgia, de recuerdos de tiempos pasados, de personas que estuvieron, de reuniones con huecos en las sillas, de vivir el ambiente de las calles como una película de otros a cámara lenta, de sentir… de sentir mucho…

Las madres tenemos suerte porque nos llevamos la mejor parte de las Fiestas Navideñas al rememorar lo vivido en nuestra infancia en cada momento mágico que nos regalan nuestros hijos. Pero si miramos hacia otro lado nos sorprenderán miradas perdidas buscando un refugio en la alegría de los más pequeños.

![image](/tumblr_files/tumblr_inline_mxn7x0nECP1qfhdaz.jpg)

Y es que esta época que se avecina es muy dura para muchas personas, incluso para algunos matrimonios, que ven cómo sus diferencias aumentan y, lejos de reinar el amor y la paz, las peleas y desencuentros son los protagonistas en estos días.

Por no hablar de las personas que, debido a las circunstancias económicas actuales, se encuentran en situación de necesidad, personas que de la noche a la mañana se han quedado sin trabajo, o les ha dado un revés la vida, y se encuentran en un estado de verdadera pobreza. Personas que van a los comedores sociales con la cabeza agachada a pedir un plato de comida, o que no pueden calentar su hogar porque no les llega para pagar la factura de la luz… Ejemplos hay mil, y en estos tiempos más. 

Por eso veo la Navidad como una moneda con dos caras, y creo que los que estamos en la cara buena podemos ayudar a quienes se encuentran en la cara más amarga. A veces un simple detalle alegra los ojos de quien lo está pasando mal.

Qué se te ocurre?