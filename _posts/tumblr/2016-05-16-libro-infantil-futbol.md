---
date: '2016-05-16T10:00:11+02:00'
image: /tumblr_files/tumblr_inline_o75zl51RBi1qfhdaz_540.jpg
layout: post
tags:
- trucos
- libros
- planninos
- recomendaciones
- familia
title: Libro Anti escuela de fútbol. Al míster se le va la olla
tumblr_url: https://madrescabreadas.com/post/144443368318/libro-infantil-futbol
---

Los pardillos son el equipo de fútbol protagonista de esta historia.

Sí, el nombre es bastante curioso, pero para conocer su origen tendréis que esperar que leamos otro de los libros de esta serie tan divertida, que encantará a los niños amantes del fútbol.

Se trata de la serie “Antiescuela de fútbol”, de Juan Carlos Crespo, editorial Alfaguara, que alberga títulos como el que acabamos de leer en casa, “Al Mister se le va la olla”, y otros como “Los siete pardillos, “Misión portero imposible” y “Meaditos de miedo”.

Con estos títulos, imaginaos lo disparatado de las historias…

[“Al Mister se le va la olla”](http://www.megustaleer.com/libro/al-mister-se-le-va-la-olla-antiescuela-de-futbol-3/ES0143210) es un libro ideal para motivar a los niños que les cuesta leer, ofreciéndoles un tema muy atractivo para ellos, y a una edad muy apropiada para que se aficionen tanto al fútbol como a la lectura.

 ![](/tumblr_files/tumblr_inline_o75zl51RBi1qfhdaz_540.jpg)

Con referencias a jugadores y entrenadores actuales y muy conocidos por todos como Di Stefano o el recientemente fallecido, Cruyff, y tomando como eje de la historia un campamento donde se dan cita varios equipos de fútbol de chicos y chicas, que juegan un torneo, el libro tiene todos los ingredientes para gustar a niños a partir de 9 años, aunque mi hijo, con 8 años, lo ha disfrutado mucho.

Además, la historia toca temas como los primeros enamoramientos inocentes, la amistad, y preocupaciones de los chavales de esta edad.

Las ilustraciones están muy conseguidas, y captan la atención de los pequeños lectores, que sienten la necesidad de leer más para ver qué pasa.

 ![](/tumblr_files/tumblr_inline_o75zwc88aV1qfhdaz_540.jpg)
El humor no falta, esta vez en la persona del entrenador, que sufre una serie de catastróficas desdichas desde que pone un pie en el campamento, que le harán protagonizar numerosas escenas un tanto surrealistas que divertirán mucho a los miembros de “Los Pardillos”.

Y la edición es de tapa dura, ideal para regalar en cumpleaños o, incluso se me ocurre, para una comunión, un lote con la serie completa “Antiescuela de Fútbol”.

Te dejo un pequeño video para que lo veas:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/zFaHUbG3Egs" frameborder="0" allowfullscreen></iframe>

¡Mi hijo y yo os lo recomendamos sin duda!

Si quieres ver más recomendaciones de libros, puedes pinchar [aquí](/search/libros).