---
layout: post
title: Prevención del abuso sexual nfantil. Juego del semáforo.
date: 2013-11-19T19:30:00+01:00
image: /images/uploads/traffic-light.jpg
author: .
tags:
  - educacion
  - 3-a-6
tumblr_url: https://madrescabreadas.com/post/67481005053/prevención-del-abuso-sexual-nfantil-juego-del
---
Hoy es el día contra el abuso infantil. Tema que me cabrea especialmente y me remueve lo que se nos remueve a las madres cuando se trata de nuestros hijos. Para qué os voy a explicar más.

Comentando con algunas amigas la manera de prevenir a los niños sin quitarles la inocencia, se me ocurrió explicar en este post cómo lo he hecho yo con mi niña y niño mayores por si os sirve de ayuda.

Lo planteo como un juego. Les hago dibujar, a mi Princesita, una niña, y a mi Osito, un niño. Lo hago por separado, dedicándoles un buen rato a cada uno en exclusiva.

A continuación dibujan un semáforo junto a la figura, y lo colorean con los colores propios del mismo: rojo, amarillo y verde.

Les cuento que mamá les va a explicar algo muy importante, y que vamos a pintar las partes del cuerpo del dibujo según sea normal o no que alguien nos toque ahí.

Pintaremos de verde las partes que no nos importa que nos toque alguien: el pelo, los mofletes, la frente, la nariz, las orejas, las manos, brazos, hombros, pies…

De rojo, las partes donde NO deben tocarles, como genitales, culete, pecho, labios…

De amarillo, zonas donde deben empezar a sospechar que algo raro pasa, como muslos, cerca del pecho…

Y, por supuesto, contar a mamá siempre que alguien les haga sentir incómodos, aunque el sujeto en cuestión les advierta que no lo hagan, y aunque se trate de alguien de la familia o amigo o profesor, entrenador, catequista…quien sea. 

Les he dejado claro que, aunque ellos crean que tienen la culpa, o así se lo hagan ver, que NUNCA es así, y que mamá JAMÁS va a reñirles por esta causa.

Ninguno ha comprendido por qué alguien iba a querer hacer eso, ni yo he ahondado en el tema, pero creo que han captado el mensaje.