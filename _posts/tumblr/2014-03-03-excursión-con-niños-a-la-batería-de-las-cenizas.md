---
layout: post
title: Excursión con niños a la Batería de las Cenizas. Cañones y mar
date: 2014-03-03T21:00:00+01:00
image: /tumblr_files/tumblr_n1gqyokvTP1qgfaqto1_1280.jpg
author: .
tags:
  - 6-a-12
  - planninos
  - local
tumblr_url: https://madrescabreadas.com/post/78465905683/excursión-con-niños-a-la-batería-de-las-cenizas
---
¡Qué excursión más chula hicimos con las fieras! Fue uno de esos días que quedarán para el recuerdo, sobre todo de los niños, que vivieron la experiencia de subirse encima de un cañón de verdad y otear el horizonte desde una torre de vigilancia y ver una mina. Y todo ello rodeados de montaña y mar con unos paisajes verdaderamente espectaculares.

## Historiade la Batería de las Cenizas, Cartagena

Este enclave militar tuvo su origen en la necesidad de defender la ciudad de Cartagena, puerto y astillero militar, de ataques por mar, construyéndose entre los siglos XVIII y principios del XX una serie de baterías de costa. Algunas de ellas conservan cañones del siglo XVIII y otras enormes cañones de principios del siglo XX. Hay en total dieciocho baterías de costa, la mayor parte artilladas y el resto destinadas a proyectores de luz para la defensa antiaérea.

Todo un escenario, que unido a la imaginación desbordante de los niños, da para un día de incansables juegos.



##  Cómo llegar a la Batería de las Cenizas en Cartagena

Un primer tramo ser en coche (no olvides poner a punto tus [sistemas de retencin para automovil](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/) si vas con los niños), pero el resto deber ser a pie. El acceso se realiza desde la vía rápida de la Manga. En Los Belones encontramos una salida hacia Atamaría y Portmán que debemos seguir. A algo menos de 5 kilómetros llegaremos al cruce con la carretera del Llano del Beal. Desde este punto iniciaremos nuestra marcha por el carril bici hasta alcanzar a unos 200 metros más arriba y a la izquierda, la barrera que da acceso al Monte de las Cenizas

##  Ruta a pie a la Batería de las Cenizas de Cartagena

La ruta a pie es de dificultad media, y la recomiendo para niños mayores de 6 años, y bebés en mochila (ergonómica, no colgona), nada de carritos, aunque vi varias familias que los usaban, pero el camino tenía demasiadas piedras para que fuera práctico, en mi opinión.

Se trata de subir el “Monte de las Cenizas”, cerca de Portmán, dentro del Parque Natural de Calblanque, en Cartagena, para llegar a la “Batería de las Cenizas”.

La primera parte del itinerario nos ofrecerá unas magníficas panorámicas hacia el este del Mar Menor y La Manga. Después el camino pasará a través de un collado a la vertiente oriental del Monte de las Cenizas, dejándonos unas impresionantes vistas de la Bahía de Portmán. 

Por aquí a diario pasean, hacen bicicleta y corren muchas personas. Si llevas mascota es un paseo bueno para ella. Hasta arriba se tarda alrededor de 1 hora si vas tranquilamente con niños disfrutando del paisaje. No olvides la cámara de fotos.

Este Portus Magnum romano se convirtió en la antigüedad en un estratégico enclave de gran importancia en el mediterráneo. Desde aquí, los barcos romanos eran cargados de mineral de plata, plomo y cobre, además de ánforas con salazones, vino y aceite, con rumbo a otras zonas del Gran Imperio. La presencia en las proximidades de una villa y una calzada romana atestiguan la importancia que tuvo Portmán y toda esta franja litoral en épocas pasadas. 

![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto3_1280.jpg)

 Tras una pequeña pendiente llegaremos a la batería militar del Monte de las Cenizas, pasando bajo una monumental portada de hormigón inspirada en el  [Templo de los Guerreros Blancos de las ruinas de Chichen — ltzá,](https://www.chichenitza.com/es/templo-de-los-guerreros) de estilo maya-tolteca. Parece como si fuéramos a entrar en un fuerte militar. A los niños les llama mucho la atención.Todo el conjunto de la batería representa un magnífico ejemplo de arquitectura militar que se encuentra protegido por la Ley de Patrimonio Histórico Español. Construida en el año 1931 sobre otra edificación anterior del siglo XVIII.

Siguiendo recto por el camino de la izquierda culminaremos nuestra marcha en un gran mirador circular donde se asienta uno de los dos imponentes cañones Vickers modelo 1923 que posee la batería. La magnífica panorámica que disfrutaremos desde aquí, culmina este bello sendero. Hacia el este apreciaremos el Mar Menor y todo el cordón montañoso litoral del área de Calblanque, y hacia el oeste, los imponentes acantilados de la sierra de la Fausilla y Cabo Tiñoso.

Si hace buen día y claro hasta puede verse África. 

![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto4_1280.jpg) ![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto5_1280.jpg) 

## Consejos prácticos para la excursión a la Batería de las Cenizas en Cartagena

Es imprescindible llevar lo necesario para comer y beber, sobre todo si vais en época calurosa, ya sabéis lo importante que es hidratarse, y también llevar crema solar y gorras. Nosotros hemos ido en invierno, y hacía aire arriba, así que si vas en esta época, una buena braga de cuello y forro polar son importantes.

¿Os apuntáis a este plan en familia?

Fuentes:

* <http://www.slideshare.net/apalamismo/bateria-de-cenizas-bahia-de-portman-la-union> :
* <http://www.minube.com/rincon/monte-de-las-cenizas-a130563>
* <http://www.regmurcia.com/servlet/s.Sl?sit=c,365,m,108&r=ReP-28290-DETALLE_REPORTAJES>

![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto6_1280.jpg) ![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto7_1280.jpg) ![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto8_1280.jpg) ![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto9_1280.jpg) ![](/tumblr_files/tumblr_n1gqyokvTP1qgfaqto10_1280.jpg)