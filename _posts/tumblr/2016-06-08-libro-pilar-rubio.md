---
date: '2016-06-08T10:00:21+02:00'
image: /tumblr_files/tumblr_inline_o8ikicE3KY1qfhdaz_540.jpg
layout: post
tags:
- crianza
- maternidad
- postparto
- libros
- planninos
- colaboraciones
- ad
- eventos
- embarazo
- familia
- salud
- mujer
title: El libro milagro de Pilar Rubio
tumblr_url: https://madrescabreadas.com/post/145600634857/libro-pilar-rubio
---

Casi todas tenemos una idea preestablecida de Pilar Rubio, que nos puede incluso llevar a poner en duda la valía de su nuevo libro [“Embarazada, ¿y ahora qué?”](https://amzn.to/3tP0BqR), más que nada porque vemos como un milagro que una mujer esté [así de estupenda embarazada](/2013/11/24/pilar-rubio-embarazada-ya-no-es-sexy.html), e incluso poco después de dar a luz.
    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=841644918X&linkId=056bd0395c919c7847a809ed1071ce5c&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

    
 ![](/tumblr_files/tumblr_inline_o8ikicE3KY1qfhdaz_540.jpg)

Cierto es que no es una madre común con la que nos sintamos identificadas la mayoría porque su físico, su estatus y su forma de vida no se corresponde con el común de las madres, que tenemos que apechugar con la crianza a tope y, en muchas ocasiones, sin ayuda.

No somos famosas, ni nuestros maridos ricos, pero a mí no me gusta prejuzgar antes de conocer, y la curiosidad me llevó a pedirle a mi colaboradora Mónica Marcos, del blog [Paraíso Kids](http://www.paraisokids.es/blog), que comprobara de primera mano si todas las críticas que está recibiendo Pilar Rubio por escribir este libro tienen fundamento.

Creo que ella, como todas las madres, tiene derecho a compartir su experiencia, aunque algunas de las cosas que hemos visto o escuchado de su persona no nos gusten o no nos cuadren con nuestra forma de criar a nuestros hijos.

## Qué me llevó hasta este libro

La **curiosidad** , y la **certeza de que los milagros no existen** es la que me ha llevado a interesarme por su libro y compartirlo con vosotras, y la conclusión que he sacado es que, milagros aparte, creo que Pilar no pretende dar lecciones sino que, dese la humildad, recurre a profesionales que le han aclarado las dudas que, como madre, le han ido surgiendo a lo largo de sus dos embarazos y puerperios y, bien documentadas, las comparte con las mujeres que quieran leerlo.

Te digo una cosa, sólo por la labor informativa que está haciendo sobre los hipopresivos en el postparto, recomendándolos en lugar de los abdominales, le doy una oportunidad para leer su libro; El suelo pélvico de muchas mujeres se lo agradecerá en el futuro.

## La presentación del libro

Te dejo con Mónica Marcos, quien estuvo en la presentación del libro, a quien regaló un ejemplar y, una vez leído, opina esto:

_“Estuve en la presentación del libro de Pilar Rubio y Caroline Correia [“Embarazada ¿Y ahora qué?](http://www.megustaleer.com/libro/embarazada-y-ahora-que/ES0124032) en el Hotel Petit Palace de Madrid, gracias a María del blog Madres Cabreadas y a la [editorial Penguin Random House con el sello Grijalbo](http://www.megustaleer.com/editoriales/grijalbo/GJ/)_

_La verdad que yo iba con una idea establecida de Pilar (por lo que oigo y veo en la Tv) y me llevé una grata sorpresa.No sé si estaba metida en un papel o es así, es la primera vez que la veo y entablo conversación con ella, pero me pareció una chica muy cercana, simpática y agradable.No puso pegas a ninguna pregunta, se la notaba tranquila, segura de sí mima. Se apreciaba que ha trabajado mucho en el libro y me parece muy honesto por su parte nombrar a todos los profesionales que han resuelto sus dudas._

 ![](/tumblr_files/tumblr_inline_o8ikicjekM1qfhdaz_540.jpg)

_El evento se desarrolló con total normalidad. Tras la suntuosa aparición de la recién autora, Pilar Rubio, se produjo la popular aglomeración de medios de comunicación (he de decir que algo aburrido para mí y agotador para ella, ya que tardaron más de media hora en hacerles mil y una foto y mil y una pregunta, alguna fuera de lugar)._

_Por fin consiguieron llegar (ella y Caroline) al espacio preparado para desarrollar el acto y la cosa empezaba con buena pinta (encantadora, seductora, amable y muy contenta). _

## Opinión sobre el libro

_Después de terminar de leerme el libro puedo afianzar que es un **manual valioso para todas aquellas futuras mamás, sencillo, muy claro y práctico**._

 ![](/tumblr_files/tumblr_inline_o8ikicWhXj1qfhdaz_540.jpg)

_En los centros comerciales, en las librerías, en las bibliotecas… podemos encontrarnos multitud de libros que hablan de la maternidad, lactancia, alimentación infantil, crianza, parto…_

_Lo que Pilar Rubio ha querido elaborar es un libro personal, que fuera útil; Una guía práctica donde describir sus dudas, las respuestas que tuvo de éstas a través de profesionales y un sinfín de temas que cualquier madre como ella alguna que otra vez se pregunta cuando sabe que va a ser mamá._

>  “Este libro no es un únicamente un libro acerca del embarazo, sino sobre todo un libro de mujeres que también son madres”

_Pilar tuvo muy claro que quería seguir siendo ella misma; Que tenía que cuidar a su bebé pero sin dejarse de mimarse ella._ 

> “Hablan aquellos profesionales que saben de las preocupaciones que tuve como madre durante mis dos embarazos”

_Se puso manos a la obra a buscar información y tras mucha investigación no encontraba nada alentador. El destino hizo que encontrase a [Soraya Arranz](http://www.be-water.es/), personal coach y experta en inteligencia emocional, propietaria de [Be Water](http://www.be-water.es/), un centro integral para futuras mamás en el que dan importancia al ámbito emocional y físico, al suelo pélvico, a los masajes perineales, a la preparación al parto… y allí estaba Caroline Correia, fisioterapeuta especializada en obstetricia y uroginegología, que dirigió físicamente sus dos embarazos y pospartos._ 

> “En mi primer embarazo me propusieron escribir un libro contando mi experiencia, cómo iba mí gestación y yo no me encontré preparada para ello, estaba aprendiendo de todo lo que me rodeaba, era un conejillo de indias”

_Cuando llegó a término el primer embarazo de Pilar y tan solo un año y medio después comenzar el segundo, se dio cuenta que todo el trabajo que estaba realizando estaba siendo útil, que el método que estaba llevando a cabo era muy efectivo y además **todos los ejercicios podían realizarlos las mujeres en casa sin necesidad de gastar mucho dinero en ello**._

_Una de las cosas que más **le alarmaba era la mala información que tenían muchas mujeres**._

> “Conozco a personas que tras finalizar sus embarazos se ponen como locas a realizar abdominales tradicionales. No hay nada peor que puedas hacer porque lo único que puedes conseguir es que se abra más la diástasis abdominal”

 _Dentro de cada apartado del libro podemos encontrar un relato científico, sus dudas al respecto sobre el tema, las respuestas de varios expertos, falsos mitos…_

_El libro consta de doce capítulos (a cual más atrayente):_

- _El embarazo_  
- _estado emocional_  
- _las pruebas prenatales_  
- _dieta sana y equilibrada_  
- _hábitos saludables_  
- _belleza y moda_  
- _el primer trimestre_  
- _el segundo trimestre_  
- _el tercer trimestre_  
- _el parto_  
- _el postparto_  
- _la conclusión_  

_No quiero desvelar más detalles del libro para animaros a leerlo.“_

 ![](/tumblr_files/tumblr_inline_o8ikicDsJi1qfhdaz_540.jpg)

Tengo que dar las gracias a Mónica por su análisis y por compartir su opinión con nosotras porque, de verdad, si es un libro que puede ayudar a las madres, aunque venga de una con la que no compartamos modelo de crianza, y la información que comparte es veraz y contrastada, y teniendo claro que los milagros no existen, y que no tenemos que obsesionarnos con ser Pilar Rubio, porque nunca lo seremos, ni falta que hace, porque cada una tenemos nuestra propia belleza, no me parece mal darle una oportunidad y quedarnos con los consejos que nos puedan ayudar a llevar mejor una de las etapas más bonitas en la vida de una mujer.