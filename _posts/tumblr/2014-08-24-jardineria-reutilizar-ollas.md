---
date: '2014-08-24T19:49:00+02:00'
image: /tumblr_files/tumblr_inline_natmd1Oo1Q1qfhdaz.jpg
layout: post
tags:
- salitaabuela
- trucos
- planninos
- diy
title: 'Jardinería de la abuela: Reutiliza tus ollas viejas como macetas '
tumblr_url: https://madrescabreadas.com/post/95655080839/jardineria-reutilizar-ollas
---

Este verano la abuela nos ha sorprendido reutilizando tres generaciones de ollas que ya no servían para cocinar, y como no le gusta tirar nada, ha inventado un nuevo concepto de jardinería plantando cactus en los cacharros de cocina antiguos.

La abuela lo ha llamado “operación rescate”. ella misma os lo cuenta:

“_La operación rescate consiste en rebuscar en trasteros, sótanos, trastiendas… etc, objetos pertenecientes a antepasados o que habíamos olvidado que teníamos._

_Por ejemplo, yo encontré tres generaciones de ollas y sartenes; de mi querida madre, mías y de mi hija mayor que, además de ser un capricho sentimental, quedan chulísimas._

## Cómo se hace

## Primero

_Ponemos en el fondo de la olla chinarro y piedrecitas menudas para el drenaje._

## Segundo

_Echamos turba mezclada con tierra de campo._

## Tercero

_Clavamos el cactus_

## Cuarto

_Echamos de nuevo tierra_

## Quinto

_La prensamos bien con las manos._

## Sexto

_La regamos y esperamos a que agarre. _

_Suerte!”_

Os nuestro cómo quedaron los cacharros.

En las ollas de mi abuela, mi madre plantó cactus. No me digáis que no es una monería:

![image](/tumblr_files/tumblr_inline_natmd1Oo1Q1qfhdaz.jpg)

En sus ollas, la abuela plantó helecho, además de decorarlas con unos motivos grafiteros dignos de cualquier banda urbana jajajaja…

![image](/tumblr_files/tumblr_inline_natmezuCMf1qfhdaz.jpg)

Y en mis ollas, la abuela plantó ciprés, olivo, don Pedro y lirio, que todavía no ha agarrado, por eso no se muestra erguido:

![image](/tumblr_files/tumblr_inline_natmgdyeGC1qfhdaz.jpg)

Pero sin duda, lo más original que ha hecho es plantar semillas de césped en dos sartenes mías que deseché porque perdieron su aniadherencia. Flipad!

![](/tumblr_files/tumblr_nats9aQjfg1qgfaqto1_500.jpg)

Que os parece? Os animáis? Seguro que se os ocurren miles de ideas.

La abuela espera vuestras fotos!