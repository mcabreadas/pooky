---
layout: post
title: "Primera comunión 2016: mis propuestas"
date: 2016-04-08T18:37:47+02:00
image: /tumblr_files/tumblr_inline_o5b7b9qtfG1qfhdaz_540.jpg
author: .
tags:
  - 6-a-12
  - comunion
  - moda
tumblr_url: https://madrescabreadas.com/post/142465741534/comunion-vestido-recuerdos
---
El año pasado hizo la [Primera Comunión Princesita](/2015/05/19/c%C3%B3mo-celebrar-la-comuni%C3%B3n-sin-arruinarse.html), así que me inicié en el maravilloso mundo de los vestidos, las diademas, los tocados, los recordatorios, los regalos originales para los invitados, los detalles para los niños, los trajes de ceremonia para familiares…

Este año no he querido perder comba porque el que viene le toca el turno a mi hijo mediano, y no quiero que me pille en frío. Así que he seguido al día en tendencias asistiendo a eventos y desfiles donde las mejores marcas presentan sus últimas novedades.

Te voy a dar unas recomendaciones de lo que más me ha gustado porque la oferta es amplísima.

## Vestidos de Primera Comunión

Empecemos por lo que más nos preocupa a las madres, y lo que con más antelación miramos; el vestido o traje de primera comunión.

Mi primera sugerencia viene de la mano de una firma que conocí en [FIMI 2016](/2016/01/27/fimi-moda-infantil.html), y me dejó totalmente enamorada por la calidad y naturalidad de sus tejidos, sus acabados, algunos a mano, y la originalidad de sus diseños.

Se trata de Margarita Freire, y os puedo garantizar que los estuve mirando muy de cerca, incluso tocando, y me encantaron. 

Además, el precio es muy asequible.

 ![](/tumblr_files/tumblr_inline_o5b7b9qtfG1qfhdaz_540.jpg)

Otro estilo, más de princesa romántica de cuento, es el de [Hortensia Maeso](http://hortensiamaeso.com/), que tuve la ocasión de ver en [Valencia Petit Walking](/2015/10/29/moda-infantil-pasarela.html).

 ![](/tumblr_files/tumblr_inline_o5b7etAOwc1qfhdaz_540.jpg)

Sencillez, originalidad y sobriedad elegante es la propuesta de Lesuanzes que conocí en Madrid Petit Walking, y que sugiere para los pies unas sencillas bailarinas.

 ![](/tumblr_files/tumblr_inline_o5b7csXO9u1qfhdaz_540.jpg)



Rubio Kids impresiona por sus vestidos de comunión vaporosos y con capas superpuestas que nos transportan a otra época. Son una verdadera maravilla.

 ![](/tumblr_files/tumblr_inline_o5b7gn85Ed1qfhdaz_540.jpg)

## Zapatos de Primera Comunión

Para los pies me quedo con bailarinas sencillas o con alpargatas. Me han robado el corazón.

Las tienes en Petritas y en Alpargatitas.

 ![](/tumblr_files/tumblr_inline_o5b7celLGj1qfhdaz_540.jpg)

## Trajes de Primera Comunión de niño

No creas que me olvido de los niños quienes,para mi sorpresa en las colecciones de este año tienen más protagonismo que otras veces. Lo que me viene genial porque ya estoy fichando ideas para mi hijo.

[Hortensia Maeso](http://hortensiamaeso.com/) juega con el marinero, pero dándole un toque moderno.

 ![](/tumblr_files/tumblr_inline_o5b7c0Lz4Z1qfhdaz_540.jpg)

Y Rubio Kids apuesta por tejidos naturales y colores suaves dando también una vuelta de tuerca al look marinero.

 ![](/tumblr_files/tumblr_inline_o5b7c7FIGD1qfhdaz_540.jpg)

## Coronas de Comunión

Para el pelo de las niñas las coronas hacen furor, pero me gustan las finitas, que no sean demasiado aparatosas, quizá sea porque me Princesita tiene la cabeza pequeña.

Las de flores preservadas engarzadas con alambre quedan muy finas y elegantes. Y, más que como corona, me gusta más tipo diadema.

Os recomiendo el taller de Juana Nicolás, experta sombrerera que trabaja la técnica del tocado de la forma más tradicional y profesional.

 ![](/tumblr_files/tumblr_inline_o5b7h25vDR1qfhdaz_540.jpg)

## Trajes y vestidos de ceremonia

Ya sabéis que soy fan de Oh Soleil porque tiene ropa específica para teens, y nosotros estamos ahora en esa edad complicada donde ellas quieren elegir y marcar su propio estilo.

He probado su ropa en mi hija, y os garantizo que además de encantarnos, su calidad es excepcional.

 ![](/tumblr_files/tumblr_inline_o5b8laPBp61qfhdaz_540.jpg)

[Elisa Menuts](http://elisamenuts.com/) tiene ropa muy original para niños y niñas, y muy ponible. Creo que con esta marca acertarás seguro.

 ![](/tumblr_files/tumblr_inline_o5b8bonoqH1qfhdaz_540.png)

Y si no quieres gastarte un dineral, en Zippy tienes precios muy asequibles, y unos conjuntos ideales en su edición “ocasiones especiales” que ya utilicé para nuestra última boda, y que os conté aquí.

 ![](/tumblr_files/tumblr_inline_o5b8kswqjF1qfhdaz_540.png)

## Recordatorios personalizados y dulces de comunión

Es cierto que es algo prescindible, ¡pero me encantan estos detalles! No creo que me pueda resistir a ellos, y más si los hace [Alejandra Fraile, de Mamá Multitarea](http://www.fraileayd.com/) con ese gusto tan original.

 ![](/tumblr_files/tumblr_inline_o5bkukNWW81qfhdaz_540.jpg)

Ella hizo los recordatorios de mi niña el año pasado, y me dibujó en acuarela la Iglesia donde hizo la Primera Comunión, además de personalizar la muñeca con su vestido. 

 ![](/tumblr_files/tumblr_inline_o5bkyw2Vd71qfhdaz_540.jpg)

## Detalles para los invitados

La tecnología también invade el mundo de las comuniones, y me parece genial porque , quién no se ha quedado sin batería alguna vez tras el banquete y se ha quedado a medio de subir una foto a Instagram?

 Si quieres seguir viendo cositas o pedir consejo de mamás experimentadas en comuniones, o compartir con otras que la están organizado, te invito a mi [grupo de Facebook “Organizando la Primera Comunión”](https://www.facebook.com/groups/1377576085877092/), donde ponemos en común nuestra experiencia.

¿Te apuntas?

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).