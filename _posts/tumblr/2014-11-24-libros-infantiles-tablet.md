---
date: '2014-11-24T17:06:00+01:00'
image: /tumblr_files/tumblr_inline_petodaH8qP1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- recomendaciones
- educacion
- familia
title: Nuevos libros para pre lectura en realidad aumentada.
tumblr_url: https://madrescabreadas.com/post/103466490594/libros-infantiles-tablet
---

Ya sabéis que estamos[introduciendo a Leopardito en la pre-lectura](/2014/11/21/libros-cuentos-bebes.html) y le está entusiasmando. Le gusta pasar las hojas de los libros imitando a sus hermanos, y fijarse en los dibujos, incluso señala con su pequeño índice las palabras y simula que lee.

Acabo de descubrir estos libros de realidad aumentada para niños de 3 a 6 años, que se pueden ver a través de una tablet mediante una app gratuita para Android o para Apple.

Mirad qué chulada:

<iframe src="//www.youtube.com/embed/IWbwfHZ4rgs?rel=0" width="100%" height="315" frameborder="0"></iframe>

Mi pequeño está muy acostumbrado a la tablet, y casi la maneja mejor que yo. Los nativos tecnológicos usan con una naturalidad pasmosa las nuevas tecnologías, por eso me encanta esta fusión entre el mundo tradicional de los libros y los dispositivos actuales.

La realidad aumentada incorpora a los libros elementos que llevan la historia a otros medios conocidos por el pre-lector que le permiten entenderla y disfrutarla desde un ángulo diferente: el libro y la tablet, todo en uno.

Con estos libros, la editorial Parramon Paidotribo ha querido romper con los libros tradicionales ofrecer algo diferente y transgresor que salta las barreras del papel aumentando las posibilidades de crecimiento de las historias a partir de las páginas del cuento.

 ![image](/tumblr_files/tumblr_inline_petodaH8qP1qfhdaz_540.jpg)

Este libro se titula [Zapatos, Zapatitos Y Zapatones](http://www.amazon.es/gp/product/8434240424/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8434240424&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=8434240424), y es el primero de una colección, y si lo enfocamos con la tablet, ésta dará continuidad a la historia: sonido, música, voces, elementos en 3D, profundidad. Todos los complementos añadidos permiten entender la historia a los niños que se están iniciando en la lectura, aunque no sepan leer aún.

 ![image](/tumblr_files/tumblr_inline_petodaflV61qfhdaz_540.jpg)