---
date: '2016-11-10T13:41:29+01:00'
layout: post
tags:
- libros
- planninos
- familia
title: 'Libros sobre diversidad: Calvin, ten cuidado'
tumblr_url: https://madrescabreadas.com/post/152992943939/libro-infantil-calvin
---

Me he animado con las “videoreseñas porque he visto que os gustan bastante”.

Esta vez os recomiendo un libro que trata sobre la diversidad, y cómo las diferencias pueden convertirse en algo positivo. “[Calvin, ten cuidado](http://www.boolino.es/es/libros-cuentos/calvin-ten-cuidado/)”

Este libro de la editorial Takatuka que me ha mandado Boolino nos ha encantado.

Os dejo mi opinión en el siguiente video:

 <iframe width="100%" height="315" src="https://www.youtube.com/embed/DU9uGzFfFJs" frameborder="0" allowfullscreen></iframe>