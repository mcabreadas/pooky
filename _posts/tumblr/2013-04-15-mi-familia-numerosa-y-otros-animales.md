---
layout: post
title: Mi familia (numerosa) y otros animales.
date: 2013-04-15T11:46:00+02:00
image: /images/uploads/depositphotos_185783294_xl.jpg
author: maria
tags:
  - crianza
tumblr_url: https://madrescabreadas.com/post/48031512613/mi-familia-numerosa-y-otros-animales
---
“Mi familia numerosa y otros animales”, o cómo sobrevive una mujer de hoy en día con un marido y tres hijos, uno de ellos de teta, es mi nueva sección del blog, mi espacio vital de desahogo, donde contaré mis experiencias como mujer de 35, madre, esposa, [profesional](/post/6137543065/reflexiones-de-una-mama-abogada "Reflexiones de una mamá abogada")… y seguramente algo más. El inspirador de este apartado, es el causante directo de nuestra transformación en familia numerosa.

Llamado Leopardito por su hermano mediano, incluso antes de nacer, y luchador donde los haya, hizo su primera gran hazaña precisamente en ese momento, en su nacimiento. Sobrevivió a una amenaza de aborto durante el primer trimestre de gestación, y cuando el embarazo llegó a término, se bebió casi todo el líquido amniótico liándola parda en el parto, siendo necesaria una cesárea de urgencia para traerlo a este mundo. Y es que en agosto, y más en esta ciudad donde vivimos, el calor es inhumano. No me extraña que el pobre se bebiera lo que pillara. Bromas aparte, gracias a Dios, todo salió perfecto.

Pero ya os iré contando más cosas sobre Leopardito. Ahora os quiero presentar a mi hijo mediano, de 5 años, también conocido como Osito, y a mi niña mayor, mi Princesita de 7 años. Ambos adoran a su hermanito, pero han causado una revolución en casa consecuencia de su llegada a la familia, vamos, que el caos y la destrucción se apoderaron de nuestro hogar y no sabemos cuándo saldrán.

 ![image](/tumblr_files/tumblr_inline_mlagyjqDF31qz4rgp.jpg)

Y eso, para una persona perfeccionista como yo (y un poco cuadriculada) es un gran reto a superar, sobre todo si decides hacer de la crianza natural tu “Biblia” , y te pasas más de 8 meses dando teta a destajo, y sin dormir más de tres horas seguidas ninguna noche… Imagínate la locura de casa y de niños, y el pobre macho de la manada diciendo que dónde está su mujer, que se la han robado…. y maldiciendo entre dientes en la madrugada y con los ojos pegados a mi querido [Carlos González](http://es.wikipedia.org/wiki/Carlos_Gonz%C3%A1lez_(pediatra) "Carlos González pediatra Wikipedia") y a toda su estirpe…

Y es que todos los extremos son malos, y una tiene que conocerse a sí misma, y sobre todo, su realidad de [madre de 3, y trabajadora](/post/5767664057/yo-tambien-tuve-que-elegir "Yo también tuve que elegir"), y no tirarse a la piscina sin flotador y sin pensar en que no sólo la necesita su bebé, sino que tiene 2 hijos más, un marido y un trabajo. Y que vaya por delante que adoro la crianza natural (tanto que casi me cargo a mi familia), pero todo en su justa medida. Y una, que es de extremos, o todo o nada, pues ya sabéis, a “tope con la cope” con la [teta](/post/37704854022/quien-ha-dicho-que-la-lactancia-es-incompatible-con-la "Quién ha dicho que la lactancia es incompatible con la moda?"), el [colecho](/post/41598512975/convierte-una-cuna-de-ikea-en-cuna-de-colecho "Convierta una cuna de Ikea en cuna de colecho") y el porteo… (hasta que una contractura me dejó K.O. y tuve que darme fisioterapia durante un mes… y sin tiempo ni para ducharme…) en fin… pero yo no desisto, eh? Qué si el [“Hipseat”](https://amzn.to/2LxDfiK) no me va bien, pues cojo el mei tai o la manduca o un fular tejido, o lo que haga falta, pero que si quiero portear, porteo…

 ![image](/tumblr_files/tumblr_inline_mlahkzLo7K1qz4rgp.jpg)

Menos mal que mi marido, que me adora, lo cual ha quedado sobradamente demostrado, pensó por mí, y “racionalizó” la crianza natural adaptándola a nuestra realidad antes de que acabara con el resto de la manada por proteger, dormir y alimentar a Leopardito, cual si viviéramos en una tribu en la sabana; decidió que era absolutamente necesario para nuestra supervivencia que yo durmiera cada noche al menos 6 horas ininterrumpidas, y así, inventó el método:

***“papá acuna al bebé cada vez que pida teta antes de las 6 horas desde la última toma hasta que se duerma”,***

y así mamá puede disfrutar de un sueño reparador que le permita tener fuerzas, y sobre todo paciencia para afrontar el duro día que le espera al amanecer, y la tarde sola ante el peligro con las tres fieras (porque la Princesita automáticamente se convierte en una de ellas por mimetismo cuando están juntos).

Desde entonces parece que ya voy viendo la luz al final del túnel, y gracias a ello he retomado este blog, que lo tenía abandonado, pero tengo la firme intención de iros contando mis aventuras y mis cabreos desde mi nueva óptica de mamá de familia numerosa.

Seguís ahí? Dadme una señal, por favor, dejadme un comentario :))

Gracias por leerme de nuevo.

*Foto de portada gracias a [sp.depositphotos](https://sp.depositphotos.com/)*