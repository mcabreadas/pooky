---
layout: post
title: Entrevista como usuaria destacada de Tumblr
date: 2014-05-08T08:02:30+02:00
image: /tumblr_files/tumblr_n55yij5teQ1r1tk1zo1_1280.jpg
author: maria
tags:
  - revistadeprensa
tumblr_url: https://madrescabreadas.com/post/85100080499/usuaria-destacada-mar-a-s-nchez
---
![](/tumblr_files/tumblr_n55yij5teQ1r1tk1zo1_1280.jpg)

[equipo](http://equipo.tumblr.com/post/84941328908/usuaria-destacada-mar-a-s-nchez):

> **Usuaria destacada: María Sánchez**
>
> **Blog:** [Madres cabreadas](https://madrescabreadas.com)
>
> **Ciudad:** Murcia, España
>
> **Primera publicación:** Enero de 2011
>
> María estudió Derecho y ejerce como abogada en su propio despacho desde hace 14 años. Es esposa de un marido maravilloso y madre de tres fieras que la hacen feliz y llenan su vida por completo. Inconformista y protestona, nunca quiere quedarse con la primera respuesta. Le indignan las injusticias y se empeña en intentar cambiar las cosas que no le gustan. Le apasiona la etapa que le está tocando vivir ahora: la maternidad. Es sensible y le encanta llegar a las demás madres y padres transmitiendo sus vivencias a través de su blog, *Madres cabreadas*.
>
> **¿Qué te hizo animarte a crear el blog?**
>
> En realidad, todo empezó con una cuenta de Twitter hace algo más de tres años, con el usuario @madrescabreadas, un nombre muy apropiado para el caso y que es totalmente responsabilidad de mi marido. Fue él quien me propuso que lanzara mis quejas al mundo en vez de rumiarlas en casa, pero, como algunas no me cabían en los 140 caracteres de un *tweet*, decidí expandir mi canal de comunicación y me abrí un blog con el mismo nombre para dar rienda suelta a mis cabreos.
>
> [Read More](http://equipo.tumblr.com/post/84941328908/usuaria-destacada-mar-a-s-nchez)

## Entrevista como usuaria destacada de Tumblr

Muchas gracias al [equipo](http://tmblr.co/mKiBYEnmyXDZwvI54OqkLNA) Tumblr por esta entrevista, por el interés mostrado en mi blog y por todo su apoyo. Me ha encantado abrir mi puerta para que todos me conozcáis un poquito mejor.