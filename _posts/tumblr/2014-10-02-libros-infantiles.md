---
date: '2014-10-02T09:01:00+02:00'
image: /tumblr_files/tumblr_inline_ncmba2Nj811qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- gadgets
- colaboraciones
- ad
- tecnologia
- recomendaciones
- educacion
- familia
title: E-book infantil de poesías escritas por y para niños
tumblr_url: https://madrescabreadas.com/post/98957581231/libros-infantiles
---

Ya está a la venta en e-book Grandes Poesías de Pequeños Poetas, del que tanto os he hablado, y del que muchos de vosotros tenéis la versión en papel.

Este proyecto partió del [sueño de una maestra](/post/4062118966/el-sueno-de-una-maestra) vocacional, María Felisa Martínez Talavera, con más de 35 años dedicados a la enseñanza, quien decidió recopilar los poemas escritos por sus alumnos de 8 y 9 años, y algunos creados por ella misma como ejemplos de clase para ellos, en un libro editado con sus propios medios y sin ánimo de lucro como homenaje a todos los niños.

![image](/tumblr_files/tumblr_inline_ncmba2Nj811qfhdaz.jpg)

![image](/tumblr_files/tumblr_inline_ncmbaiAshc1qfhdaz.jpg)

Su objetivo fundamental es ayudar a despertar en los niños y niñas el gusto por la poesía sencilla y espontánea.

Según la autora, la primera reacción de los niños ante la poesía es el pudor a expresar sus sentimientos más íntimos, de manera que cuando lo consiguen, además de aprender a comunicarlos, supone una gran victoria personal. A medida que iban trabajando las poesías contenidas en este libro en clase, la relación de los alumnos entre ellos y con la maestra fue enriqueciéndose y haciéndose más espontánea y cercana.

Este libro es el broche de oro a la carrera profesional de María Felisa, y todavía se le ilumina la cara cuando habla de ello.

Tras agotarse la versión impresa se ha lanzado el e-book con un precio simbólico para ver si en un futuro se consiguen los fondos para hacer la asegunda edición en papel, ya que muchas famillias se han quedado con las ganas de un ejemplar.

Podéis comprarlo en La Casa del Libro pinchando [aquí](http://www.casadellibro.com/ebook-grandes-poesias-de-pequenos-poetas-ebook/9788461344314/2385231).

[![image](/tumblr_files/tumblr_inline_ncmb0hcQUW1qfhdaz.jpg)](http://www.casadellibro.com/ebook-grandes-poesias-de-pequenos-poetas-ebook/9788461344314/2385231)

Os dejo un aperitivo para que veáis lo divertidos que son los poemas de los niños, y lo que pueden despertar en vuestros hijos y en vosotros mismos:

“Mi madre me quiere como un tesoro,

pero no porque estoy hecha de oro.

Mi madre trabaja sin parar,

pero no le gusta nadar.

A mi madre le duelen los codos

y la cuidamos entre todos.

Mi madre es guapa

 y se pone una capa.

A mi madre le gusta leer

y adquiere saber.

A mi madre le encantan los gatos

y odia a los patos.

A mi madre no le gusta estar de fiesta,

sino dormir la siesta.”

Rosana Iniesta

“Mi abuela es la más bella

entre todas ellas

porque cuando su boca ríe,

sus ojos sonríen.

Mi abuelo es tan formal

como un oficial.

A mi abuelo le gusta cantar

como a los pájaros trinar.

Mis abuelos son los mejores,

porque cocinan en los fogones y

me ayudan con los ordenadores.

Mi abuela es muy buena

y también perfecta.

Cuando le pido algún caprichillo

ella siempre me lo compra,

sea un helado o algún dulce,

aunque sea algo de luces.

Mi abuela es la mejor

y siempre compra turrón.

Dentro de su corazón hay

amor, simpatía y mucha alegría .”

Javier Benito López

¿A que se te ha dibujado una sonrisa?

Pues anímate y descárgatelo sólo por 2,50 €

[![image](/tumblr_files/tumblr_inline_ncmbwnpD1o1qfhdaz.jpg)](http://www.casadellibro.com/lectores/misLibrosPublicados?idusuario=4378227&lang=es)

_NOTA: Este post NO es patrocinado_.