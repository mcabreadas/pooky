---
date: '2015-05-31T17:24:23+02:00'
image: /tumblr_files/tumblr_inline_np5j33xly31qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- menopausia
- recomendaciones
- salud
- mujer
title: Menopausia precoz. Experiencia y consejos.
tumblr_url: https://madrescabreadas.com/post/120356055789/menopausia-consejos
---

La menopausia es algo que me preocupa, y me parece importante estar preparada para cuando llegue teniendo información, incluso conociendo experiencias de otras mujeres.

La edad en la que comenzará esta nueva etapa en nuestras vidas puede variar mucho de una persona a otra. Aunque existen las menopausias precoces, como es el caso que os voy a contar, lo normal es que la mayoría de las mujeres la experimentan alrededor de los 50 años, [el rango de edad normal es de 45 a 55](http://www.nlm.nih.gov/medlineplus/spanish/ency/article/004016.htm).

 ![image](/tumblr_files/tumblr_inline_np5j33xly31qfhdaz_540.png)
## Los síntomas comunes de la menopausia

- Periodos menstruales menos frecuentes y que finalmente cesan.  
- Latidos cardíacos fuertes o acelerados.  
- Sofocos, usualmente peores durante el primero y segundo año.  
- Sudores fríos.  
- Enrojecimiento de la piel.  
- Problemas para dormir (insomnio).  

## Posibles soluciones

Existe una terapia hormonal sustitutiva para paliar estos síntomas, pero algunos estudios grandes han cuestionado sus riesgos y beneficios para la salud, entre ellos, el riesgo de cáncer de mama, ataques cardíacos, accidentes cerebrovasculares y coágulos de sangre.

Hay otros medicamentos que pueden ayudar con los cambios del estado de ánimo, los sofocos y otros síntomas. Estos medicamentos son, entre otros:

Antidepresivos tales como paroxetina (Paxil), venlafaxina (Effexor), bupropión (Wellbutrin) y fluoxetina (Prozac).

Un medicamento antihipertensivo llamado clonidina.

La gabapentina, un medicamento anticonvulsivo que también ayuda a reducir los sofocos.

([http://www.nlm.nih.gov](http://www.nlm.nih.gov/medlineplus/spanish/ency/article/000894.htm))

## Experiencia cercana

Creo que es uno de los cambios más importante que tendremos que afrontar en nuestra vida, por eso he preguntado a una mujer cercana a mí.

Ella tuvo una menopausia precoz, y se sometió a la terapia hormonal, que consistió en ponerse unos parches durante 4 años, en los que le desaparecieron los síntomas. Dice que “rejuveneció”.

Pero al suspender el tratamiento pasó una especie de síndrome de abstinencia, y le empeoraron los síntomas. Sentía dolor de huesos, cansancio, ánimo bajo, tristeza, hipersensibilidad, iritabilidad, la piel deteriorada, caída del cabello, aumento de peso…

## Alternativa natural

Cuando le pregunto si volvería a someterse al tratamiento de hormonas me contesta que no, pero que antes no había tantas alternativas como ahora, que conoce mujeres que han optado por la vía natural, y que merece la pena intentarlo porque se mejora bastante, aunque no existe la panacea en cuanto  a la menopausia se refiere, que la soja, el aceite de onagra y otros compuestos pueden aliviarnos, incluso me recomienda empezar a tomarlos desde ya.

He investigado, y he encontrado estas cápsulas de [Donna Plus](http://www.ordesa.es/donnaplus/productos/donnaplus--menopausia), que llevan isoflavonas de soja, aceite de onagra y extractos de salvia, melisa, pepitas de uva y esencia de romero. Tienen buena pinta, y parece ser que funcionan bastante bien.

 ![image](/tumblr_files/tumblr_inline_np5i9k3wCT1qfhdaz_540.png)

## Más vale prevenir

También podemos reducir el riesgo de problemas a largo plazo, como osteoporosis y cardiopatía, tomando las siguientes medidas: 

- Controlando la presión arterial, el colesterol y otros factores de riesgo de cardiopatía.  
- NO fumando, ya que el consumo de tabaco puede causar menopausia temprana.  
- Llevar una alimentación baja en grasa.  
- Hacer ejercicio regularmente. Los ejercicios de resistencia ayudan a fortalecer los huesos y mejorar el equilibrio.  
- Tomando calcio y vitamina D.

Es hora de empezar a tomar medidas. 

Creo que estamos a tiempo. 

¿No crees?