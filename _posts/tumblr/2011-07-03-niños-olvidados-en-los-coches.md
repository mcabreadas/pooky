---
layout: post
title: Niños olvidados en los coches
date: 2011-07-03T17:43:00+02:00
image: /images/uploads/depositphotos_202388264_xl.jpg
author: maria
tags:
  - mecabrea
tumblr_url: https://madrescabreadas.com/post/7193150913/niños-olvidados-en-los-coches
---
Ayer me despertaba con esta trágica noticia que difundió [@dandocoloralosd](http://twitter.com/#!/dandocoloralosd) en twitter: En lo que va de verano ya han muerto 16 niños en EEUU por golpe de calor al quedar olvidados en el coche. En 2010 la cifra alcanzó los 49 niños.

Esto es aterrador, horrible y casi increíble… pero lamentablemente cierto. 

 Todo esto va más allá de un simple descuido o despiste, y lo está diciendo la persona más despistada que conozco, o sea, yo. Va más allá de una dramática anécdota aislada, es algo mucho más grave e importante, ya que vamos camino de batir el triste record del verano pasado. Para evitarlo no creo que baste con las recomendaciones que nos hace el artículo: dejar un peluche en el asiento del copiloto o el maletín de trabajo en el trasero para tener presente que nuestr@ hij@ va con nosotros en el coche (qué triste).

Se trata de un problema más de fondo que hay que atacar de raíz, ante el que nos tenemos que parar en seco y pensar: “QUÉ NOS ESTÁ PASANDO!?” ALGO FALLA EN NUESTRAS VIDAS. STOP. PELIGRO! ¿A dónde nos lleva esta vida de prisas, de no llegar, de presión laboral, de horarios que cumplir? ¿Cuáles son nuestras prioridades y cuáles las que nos está imponiendo la sociedad hiperacelerada? Porque os aseguro que no cabe en la naturaleza humana la posibilidad de que un padre o madre se deje olvidado a su hij@. Eso no le pasa a ningún animal en estado natural, mucho menos debería pasarle al ser humano.

Se trata de algo antinatural porque la hiperaceleración de nuestras vidas nos lleva a la deshumanización y a convertirnos en relojes humanos, máquinas de hacer cosas, de conseguir cosas, lo que termina por cambiar el orden de nuestras prioridades naturales porque vamos aturdidos, embotados, estresados, acelerados, abducidos por una sociedad que nos impone un ritmo que saca lo peor de nosotros mismos. Me niego a culpabilizar a esos padres y madres que han “olvidado” a su hij@ en el coche. Es más, no creo que se trate de un olvido, sino más bien ,la punta del iceberg de una situación personal insostenible que les ha llevado a cometer el peor error de su vida, y que lamentarán el resto de su existencia.

No lo puedo evitar, empatizo con esos padres porque yo he pasado por situaciones de estrés desbordante que no sabía manejar porque nadie te enseña a hacerlo, te metes en una rueda que gira y gira, y tú estás dentro, y no sabes cómo escapar. Yo, di un salto, dije basta! Y prioricé, no dejé que la sociedad lo hiciera por mí. Pensé: ¿qué es lo más importante? La respuesta estaba clara, mis hijos. Y relegué todo lo demás a un segundo plano; ¿Qué supone? ¿Disminuír el ritmo de trabajo para poder atender a mis hijos? ¿Levar menos casos? ¿Trabajar desde casa? ¿Ganar menos dinero? ¿Tener que ocuparme yo de las tareas domésticas? Tuve que hacerlo. 

Me cabrea que mueran niños en los asientos traseros de los coches, y no es algo lejano que sólo pasa en EEUU. Nos puede pasar a cualquiera. La solución no es poner una alarma que nos avise de que está nuestro hij@ en el coche, sino, darle el lugar que se merece en nuestras vidas. Ya me diréis si si tengo razón.

Foto gracias a [sp.depositphotos](https://sp.depositphotos.com)