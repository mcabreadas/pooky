---
date: '2017-09-17T19:37:27+02:00'
image: /tumblr_files/tumblr_inline_owfntnjXpn1qfhdaz_540.jpg
layout: post
tags:
- crianza
- educacion
title: Elegir activiadades extraescolares según la edad
tumblr_url: https://madrescabreadas.com/post/165442562184/elegir-activiadades-extraescolares-según-la-edad
---

Música, teatro, inglés, idiomas, deportes, ajedrez, aeromodelismo, robótica son ya actividades extraescolares muy frecuentes entre nuestros niños, pero últimamente está surgiendo una nueva generación de actividades tan original como sorprendente, por ejemplo cultivar un huerto urbano, yoga para niños hacer grafitis o circo!

Septiembre es el mes más estresante del año para casi todas las familias porque toca encajar los horarios de todos, elegir las actividades extra escolares, que no se solapen y coordinarse cada día para llevar y recoger a los niños.

A veces no tenemos más remedio que apuntarlos incluso a punto de cruz porque tenemos que hacer coincidir nuestro horario con el de ellos ya que la conciliación laboral y familiar brilla por su ausencia en este país, y quien lo paga al final son nuestros hijos con jornadas interminables (y sin cobrar horas extra…)  
Además, cuando crecen y rondan la adolescencia son ellos los que quieren decidir qué actividades hacer, y si son varios hermanos, te digo yo que ni la Asamblea de la ONU, oye… ponerse de acuerdo supone todo un reto.

Pero no todas las actividades son adecuadas para las mismas edades, sino que hay algunas más recomendables para ciertas etapas del desarrollo.

## Entre los 6 y 12 años: deportes de equipo

El fútbol o el [baloncesto](http://blog.baloncesto.decathlon.es/por-que-el-baloncesto-es-beneficioso-para-los-ninos/) son un clásico, y la gran mayoría de los niños los escogerán.

Los deportes de equipo son ideales para esta franja de edad y desarrollan una serie de valores personales y sociales como el compromiso (en ocasiones tendrá que renunciar a sus apetencias personales y sacrificarse por el grupo, desarrollando así una disciplina), la perseverancia, las responsabilidades individuales dentro de un grupo, el trabajo en equipo, el respeto a las normas, el respeto a los demás, y aprender a competir de forma saludable y eficaz.

 ![](/tumblr_files/tumblr_inline_owfntnjXpn1qfhdaz_540.jpg)

Además, los beneficios psicológicos que aporta el baloncesto son extraordinarios, ya que les ayuda a desarrollar recursos que les serán muy útiles para su vida, tales como el desarrollo cognitivo, la percepción de control,la autoconfianza, la gerstión del éxito y el fracaso, el autoconcepto y la autoestima.

## 6-7 años: mini tenis

La etapa del desarrollo infantil óptima para practicar este deporte, que fomenta la psicomotricidad alcanza hasta los 6-7 años de edad de los niños, lo cual coincide con la etapa de pre tenis, mini tenis o de bola roja. En esta etapa la pista mide 11 metros de largo y se juega con pelotas de espuma o bolas de fieltro de punto rojo.

 ![](/tumblr_files/tumblr_inline_owfns1yccO1qfhdaz_540.jpg)

Las habilidades motrices básicas que se trabajan son los lanzamientos, recepciones, giros, saltos y desplazamientos; las habilidades motrices genéricas son el bote, la finta (engaño), pase, golpeo, conducción, interceptación; y las habilidades motrices específicas que son en este caso las propias del tenis: derecha, revés, saque, voleas, remate, y sus variantes.

Todo ello se desarrolla poco a poco a base de juegos que permitan a los niños divertirse mientras aprenden, y que además les supongan un reto, lo cual es fundamental para que mantengan la atención y la motivación por la tarea que están realizando. Así mismo, la interacción entre ellos, con varios cambios de compañero dentro de una misma actividad les facilitará experimentar con otros alumnos con niveles de habilidad diferente, lo cual facilitará su aprendizaje al practicar con niños más hábiles y permitirá que actúen como ayudantes de otros con un nivel de habilidad igual o inferior.

Te dejo más [información sobre el mini tenis concreta aquí](http://blog.tenis.decathlon.es/desarrollar-la-psicomotricidad-en-los-ninos/).

## Karate o judo: 6 años

A partir de los 6 años los niños pueden empezar a practicar actividades como el kárate o judo. Estos deportes ayudan al desarrollo mental y enseñan valores como la integridad, la honestidad y la autodisciplina.

## Atletismo: 5 años 

La moda del running llega también a la infancia, pero tenemos que adaptar las sesiones en función de sus edades, a la vez que tendremos en cuenta que debe ser una práctica divertida, que sea como un juego.

 ![](/tumblr_files/tumblr_inline_owfnsqXk7F1qfhdaz_540.jpg)

Lo ideal es que la práctica se realice en lugares al aire libre a ser posible en contacto directo con la naturaleza y  en grupo con más niños.

Se recomienda la practica a partir de los 5 años ya  que en edades inferiores el grado de madurez física y postural aun no esta desarrollado y el grado de lesiones es muy alto.

De los 12 años en adelante el niño ya ha conseguido un desarrollo notable pero aun así tenemos que ser conscientes de que aun no podrán enfrentarse a grandes distancias. Sino que lo ideal será ir incrementado el kilometraje de forma gradual.

Más información sobre la práctica de [atletismo según la edad de los niños, aquí](http://blog.running.decathlon.es/haz-de-tu-hijo-un-gran-corredor/).

## Música: 6 años

Los 6 años es un buen momento para empezar a tocar un instrumento. Despierta la inteligencia musical y el sentido del ritmo, divierte, relaja, estimula y proporciona placer. Desarrolla la coordinación y la concentración.

Este aprendizaje puede ser tanto en un centro privado como en uno oficial (en este caso, a partir de los 8 años).

 En este [post de Música Planet tienes más información sobre las enseñanzas oficiales de música](http://proyectohelade.com/conservatorio-de-musica/).

Mis hijos mayores empezaron el curso pasado a estudiar música, y este año repiten!

## Teatro: 8 años

Lo mío con el teatro es puro amor. 

Puedo agradecerle mi paso de la timidez más enfermiza de niña a mi capacidad para la oratoria en público, control de nervios, respiración y expresión de emociones.

Otros beneficios son el desarrollo de la memoria la mejora en la lectura.

## Ajedrez: 8 años

Con 8 años se puede empezar a jugar al ajedrez. Esta extraescolar ayuda a desarrollar la memoria, la concentración y la imaginación. Enseña a tomar decisiones, planear estratefias, asumir responsabilidades por sus actos, superar errores y disfrutar de los aciertos. Además, estimula la seguridad en uno mismo.

Muy recomendable!

## Nuestra elección

Nosotros este año hemos optado por los deportes (además de la música, que ya venían estudiando desde el curso pasado) porque pensamos que es muy importante la actividad física para su desarrollo, su salud, autoestima, seguridad en sí mismos y para aprender a trabajar en equipo.

La mayor se estrena con el atletismo, el mediano con el baloncesto, y el pequeño con el mini tenis.

## La importancia de una buen equipación

Es importante equiparlos correctamente , sobre todo a nivel de calzado, ya que cada deporte exige unos movimientos e impulsos diferentes, por ejemplo, las zapatillas de basket son importantísimas para que amortigüen los saltos y sujeten el tobillo para prevenir posibles torceduras.

![](/tumblr_files/tumblr_inline_p7n3eePjea1qfhdaz_540.gif)  
Las de atletismo llevan unos tacos de metal para correr en pista que son fundamentales para un correcto desarrollo de la pisada.

 ![](/tumblr_files/tumblr_inline_p7n3ef4pC01qfhdaz_540.gif)

Las de tenis son específicas para pista, y la ropa es ligera, transpirable, y el peque está para comérselo con ella (este gif lo hice en un rocódromo).

 ![](/tumblr_files/tumblr_inline_p7n3egE0bq1qfhdaz_540.gif)

Nosotros los hemos equipado a los 3 en [Decathlon](https://www.decathlon.es/) porque:

- Tienen todos los deportes tallas para niños   
- Tienen variedad de modelos y precios  
- Cuentan con marca de fabricación propia de calidad, lo que abarata el precio sin mermar la funcionalidad del producto.  
- Multitud de colores y modelos para elegir.  
- Vendedores especializados que te aconsejan y orientan (yo andaba bastante perdida, la verdad..)  
- Además, si te haces la tarjeta de fidelización, puedes ir acumulando puntos, y te van mandando chequea descuento que vienen genial!  

Así que este curso, ponlos en movimiento y que no pare el deporte!

##  #ElDeporteNuncaPara