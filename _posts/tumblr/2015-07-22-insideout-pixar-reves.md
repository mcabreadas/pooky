---
date: '2015-07-22T20:15:06+02:00'
image: /tumblr_files/tumblr_inline_nrwfzbZcyr1qfhdaz_540.jpg
layout: post
tags:
- crianza
- cine
- planninos
- educacion
- familia
title: Del Revés (Inside Out). La opinión de los niños.
tumblr_url: https://madrescabreadas.com/post/124764852474/insideout-pixar-reves
---

Me sorprende la cantidad de análisis de sicólogos, neurólogos que ya hay sobre la película Del Revés de Disney Pixar. Todos coinciden en que es una buena representación del funcionamiento de nuestro cerebro y la valoran positivamente desde el punto de vista científico.  
Pero todos también se preguntan si gustará a los niños, y si la comprenderán. Al fin y al cabo la peli es para ellos.

Y esto, quien mejor puede responderlo son ellos mismos, ¿no crees?

## Sinopsis

Se trata de Riley, una niña de 11 años a la que le cambia la vida de repente y todo su mundo se tambalea. Se rompe su estabilidad, sus emociones se hacen un lío y comienza a sentir cosas que nunca había experimentado.

Sus recuerdos esenciales de la infancia y los pilares básicos de su personalidad (en la peli, islas), sobre todo su familia, la ayudan a superar las dificultades.  
Todo ello visto desde la óptica del funcionamiento de las emociones en su cerebro, que se representa como un panel de mandos donde en cada momento lleva las riendas una de las cinco emociones que se representan con un personaje: ira, asco, miedo, alegría y tristeza.

![image](/tumblr_files/tumblr_inline_nrwfzbZcyr1qfhdaz_540.jpg)

Fueron mis hijos quienes pidieron ir a ver la peli, con tanta insistencia que, aún estando en nuestro lugar de vacaciones, lejos del cine, recorrimos 45 km para complacerlos, porque pensábamos que iba a merecer la pena.

## Durante la peli

Los tres (9, 7 y 3 años), repito, los tres, no despegaron los ojos de la pantalla.

Esto, sobre todo para el pequeño fue todo un logro, ya que es la primera vez que no se dedicó a subir y bajar los escalones de la sala.

Primer objetivo cumplido:

Es un film que capta la atención de los peques, y la mantiene. Esto ya es todo un éxito!

Que la comprendan en mayor o menor medida, dependerá de su edad, madurez, y de las explicaciones que les den los adultos que los acompañen.

##   

## Opinión de los niños

-Os ha gustado?

Respuesta unánime  
-Siiií  
Aprovecho para indagar en sus cerebros (una oportunidad de oro para las madres.)  
Al mediano:   
-¿qué personaje de los que manejaba el panel de mandos crees que toma el control más a menudo en tu cerebro?  
El niño lo piensa, y me responde que, como en la peli pasa a veces, no es uno sólo, sino que se juntan dos a la vez la mayoría de veces.   
Mi hijo tiene 7 años.   
¿Creéis que comprendió la película?  
Fue capaz de entender mi pregunta, para empezar.  
Y para continuar, la respuesta que me dio me hizo pensar que, quizá la había entendido mejor que yo.  
Pero no sólo eso, sino que le sirvió para hacer la primera introspección de sí mismo, y para conocerse mejor.  
Ahora, cuando se empieza a enfadar le digo: uy! El hombrecillo rojo toma el mando.   
Creedme si os digo que es capaz de controlarlo.  
Quizá eso fue lo que eché en falta en la peli, que de alguna manera se hiciera ver que podríamos ser capaces de controlar qué emoción debe prevalecer en cada momento.

##   

## La protagonista

Podría haber sido mi hija mayor perfectamente. En serio, ella misma dijo que se parecían (incluso físicamente).

Creo que fue muy positivo para ella comprenderse a sí misma un poquito más en una edad en la que está empezando a cambiar, y su mundo infantil comienza a cobrar otro significado.

Charlamos y buscamos las “islas de su personalidad” y, como en la película, le dimos la mayor relevancia a la familia, porque en esa etapa de la preadolescencia, en la que se encuentran perdidos, es a lo que los padres debemos lograr que se agarren para encontrar su camino.

![image](/tumblr_files/tumblr_inline_nrwg8vxMQs1qfhdaz_540.jpg)

Esto la peli lo refleja muy bien.  
El concepto de isla de la personalidad me ha servido mucho para comprender a mi hija mejor y para ayudarla a forjar esas islas que la sostendrán en los malos momentos.

![image](/tumblr_files/tumblr_inline_nrwg8dVjch1qfhdaz_540.jpg)

##   

## Los recuerdos esenciales

En la película se representan mediante esferas brillantes, y se les da un valor muy especial.  
Creo que los padres somos fabricantes en gran medida de esos recuerdos de la infancia que acompañarán a nuestros hijos durante toda su vida, y que por ello tenemos un papel crucial para procurar que sean lo mejor posible.

De hecho creo que llevando a los niños a ver la peli, recorrer tantos kilómetros, disfrutarla y comentarla todos juntos ¡construimos uno de ellos!

##   

## Alegría y tristeza

Muy importante la lección que aprendimos de que ambas son igual de importantes, y a veces hay que dejar que las lágrimas fluyan y que se expresen los sentimientos para comenzar a poder alegrarnos de nuevo.

##   

## Los padres

Se podría hacer otra película sobre cómo juegan las emociones en el cerebro de los padres, pero creo que sería un auténtico caos.

Pero me conformo con la brillante escena en la que intentan que la Riley se abra con ellos y les cuente la experiencia de su primer día de cole, y el fatal resultado que obtienen porque, además de divertidísima, creo que representa a la mayoría de las familias, al menos eso pareció en la sala a juzgar por la carcajada generalizada.

##   

## El photocall

El cartel de la película es un gran photocall que nos da la oportunidad de expresar con la cara las cinco emociones básicas.  
Gran acierto!  
“Pon cara a tus emociones”  
Yo enseguida puse cara de cabreada, para hacer honor al nombre del blog, pero esta vez no me salió muy bien. Así que me vine arriba y probé con otras.  
¿A que no aciertas las emociones que representé?

![image](/tumblr_files/tumblr_inline_ns000o6yVn1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_ns0016cKh41qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_nrwgeoBqc11qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_nrwgfc2xqS1qfhdaz_540.jpg)

Y esto es lo que nos ha regalado Disney Pixar esta vez. Así que en nombre de todas las madres del mundo le doy las gracias por la herramienta que nos ha facilitado para empezar a educar a nuestros hijos en emociones (y a nosotros mismos).  
¿Te animas a acertar las emociones de las fotos?