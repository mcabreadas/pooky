---
date: '2015-09-02T18:00:38+02:00'
image: /tumblr_files/tumblr_inline_o39zg3NKrl1qfhdaz_500.gif
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- familia
- ad
- recomendaciones
- testing
- educacion
- juguetes
title: Emotiblocks de Miniland. Testing
tumblr_url: https://madrescabreadas.com/post/128192187953/emotiblocks-miniland
---

¡Ay! Tardes calurosas de verano que tan largas os hacéis a veces…

¿Qué haríamos las madres si no tuviéramos una as en la manga para controlar a las pequeñas fieras?

Este verano hemos probado los [Emotiblocks, un juguete nuevo de Miniland](http://www.amazon.es/gp/product/B017CAR7YC/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B017CAR7YC&linkCode=as2&tag=madrescabread-21) ![](/tumblr_files/tumblr_inline_o39zg3NKrl1qfhdaz_500.gif) que, sorprendentemente ha enganchado también a los mayores.

En teoría es ideal para niños de 2 a 6 años, por eso cuando me lo ofrecieron testear en Miniland pensé, sobre todo en el pequeño de 3 años, pero resulta que a los mayores (7 y 9 años) también les ha gustado. 

 ![image](/tumblr_files/tumblr_inline_o39zg3kRBW1qfhdaz_540.jpg)

No es un juego de construcciones al uso, ya que las piezas tienen caras que reflejan una emoción cada una, de manera que los niños pueden identificarlas, y decidir si el muñeco que están construyendo va a estar triste, alegre…

 ![image](/tumblr_files/tumblr_inline_o39zg4sPTQ1qfhdaz_540.jpg)

Nos ha venido genial este juego porque después de ver la película de Disney Pixar Inside Out (Del revés), como os contaba en este post, estábamos muy concienciados con el tema, lo que ha hecho que este haya sido un verano muy “emocionante”.

Las piezas son muy manejables para niños de 2 años, y suaves al encajarlas y desencajarlas, lo que hace que les encante combinarlas de muchas formas diferentes para hacer su propio personaje.

Hay hasta 100 combinaciones posibles.

 ![image](/tumblr_files/tumblr_inline_o39zg5llgi1qfhdaz_540.jpg)

El juego lleva unas tarjetas con fotografías de niños mostrando las diferentes emociones en sus caras para ayudar a los más pequeños a comprenderlas.

 ![](/tumblr_files/tumblr_inline_o39zg5XI0H1qfhdaz_540.jpg)

Pero lo más divertido ha sido ver al chiquitín intentando imitar las caras de los muñecos haciendo muecas que hacían que nos partiéramos de risa todos. Y cuanto más nos reíamos, más caras ponía.

No me extraña que los Emotiblocks hayan sido galardonados por la Asociación Española de Fabricantes de Juguetes con el premio al mejor juguete 2015

 ![image](/tumblr_files/tumblr_inline_o39zg6fcSU1qfhdaz_540.jpg)

## Aplicación para móvil para organizarte con tu bebé

Además, Miniland ha sacado un aplicación gratuita para móvil que te faciliata las cosas.

Te permite organizar datos médicos, conectarte para vigilar al bebé si no estás en casa, te avisa de las vacunas o citas y además puedes tener organizados los mejores momentos familiares.

Te la puedes [descargar aquí para Android](https://play.google.com/store/apps/details?id=com.sozpic.miniland&hl=es) 

Y [aquí para IOS](https://itunes.apple.com/es/app/emybaby/id610312898?mt=8)

¿Te animas a probarla?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.