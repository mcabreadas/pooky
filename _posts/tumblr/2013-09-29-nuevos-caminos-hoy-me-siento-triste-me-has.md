---
layout: post
title: Destete. Nuevos caminos
date: 2013-09-29T18:23:00+02:00
image: /tumblr_files/tumblr_mtwav8DmN81qgfaqto1_1280.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - lactancia
  - cosasmias
  - bebe
tumblr_url: https://madrescabreadas.com/post/62627487399/nuevos-caminos-hoy-me-siento-triste-me-has
---
Hoy me siento triste. Me has pedido casi en un susurro resignado porque ya casi has asumido que se acabó. 

Han sido casi 14 meses de encontrarnos cada rato para compartir alimento, caricias, miradas de almendra, calor… amor.

\
 Hoy te he dormido por primera vez sin pecho. Lo has buscado, como siempre, sin entender qué ha cambiado para ya no tenerlo, pero aceptándolo casi sin protestar. Te he abrazado fuerte, he pegado tu dulce carita a la mía y te he dicho que te quiero, y que siempre te voy a cuidar. 

Entre los dos hemos buscado una nueva manera de acurrucarnos y, entre arrumacos y balanceos te has relajado. 

Enseguida has querido tumbarte en tu cuna, y con la voz entrecortada te he cantado lo mismo de siempre, pero diferente: 

“A rorró mi niño, a rorró mi sol, a rorró pedazo de mi corazón…” a la vez que te movía suavemente la cuna. Al intentar parar varias veces has protestado porque necesitabas sentirme más rato…\
 No te preocupes, no me voy.