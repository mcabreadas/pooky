---
date: '2015-12-20T19:04:12+01:00'
image: /tumblr_files/tumblr_inline_o39yvllljx1qfhdaz_540.jpg
layout: post
tags:
- navidad
- trucos
- reyesmagos
- regalos
- premios
title: Cuentos personalizados. Un regalo inolvidable. Sorteo.
tumblr_url: https://madrescabreadas.com/post/135582059689/cuentos-personalizados-un-regalo-inolvidable
---

## Ganadora

Muchas gracias por participar en este sorteo tan especial. Me encanta colaborar con negocios artesanos que ponen en cada producto todo su cariño.

La ganadora es:

 ![](/tumblr_files/tumblr_inline_o39yvllljx1qfhdaz_540.jpg)

El Universo de Lai. Enhorabuena!!

## Ampliado el plazo hasta el 4 de enero

Si quieres hacer un regalo que de verdad sea especial y poniendo tu corazón en él, te sugiero los cuentos personalizados.

Yo no sabía demasiado sobre esto hasta que descubrí la web de Gema de la Fuente, [Rabitos de Pasa](http://www.rabitosdepasa.com/), que hace verdaderas maravillas con las fotos y las historias que éstas cuentan.

 ![](/tumblr_files/tumblr_inline_o39yvlsisj1qfhdaz_540.jpg)

Por ejemplo, me parece un detalle precioso para un cumpleaños en el que se cumpla una cifra importante, como los 10 años, los 18, los 40… (ya me queda poco…ejem…), o para una jubilación…

 ![](/tumblr_files/tumblr_inline_o39yvlSG0p1qfhdaz_540.jpg)

Otra idea chula que se me ocurre es recopilar los primeros meses de vida de un bebé, hacer fotos desde el embarazo hasta los dos o tres años, por ejemplo, que es cuando los niños más cambian y más aprenden, y protagonizan anécdotas super divertidas que podemos contar en un cuento que leerá cuando sea mayor…

 ![](/tumblr_files/tumblr_inline_o39yvmHlQj1qfhdaz_540.jpg)

¿Te imaginas cuando tu hijo descubra que es protagonista de un cuento que cuenta su propia historia?

La presentación es exquisita, y Gema cuida cada detalle, incluso de envoltorio.

 ![](/tumblr_files/tumblr_inline_o39yvm55bC1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yvmeOi21qfhdaz_540.jpg)

Además, durante esta Navidad, hasta el 7 de enero de 2015, tienes los gastos de envío gratis en los cuentos personalizados.

También se pueden personalizar láminas para enmarcar y poner en el cuarto de los niños o donde quieras, porque son ideales.

Os quiero enseñar éstas de Navidad que creó Gema especialmente con motivo de este sorteo para mis lectores.

La ganadora podrá elegir la que más le guste. 

No sé a ti, pero a mí me han enamorado todas:

## Lámina muñeco de nieve
 ![](/tumblr_files/tumblr_inline_o39yvnPvvm1qfhdaz_540.jpg)

## Lámina árbol de Navidad
 ![](/tumblr_files/tumblr_inline_o39yvoIBOp1qfhdaz_540.jpg)

## Lámina feliz Navidad
 ![](/tumblr_files/tumblr_inline_o39yvqzYOS1qfhdaz_540.jpg)

## Lámina blanca Navidad
 ![](/tumblr_files/tumblr_inline_o39yvq0bZS1qfhdaz_540.jpg)
##   

## Sorteo de una lámina de Navidad

Para participar en el sorteo de una de las anteriores láminas de [Rabitos de Pasa](http://www.rabitosdepasa.com/):

-Deja un comentario en este post indicando tu nick de Facebook o Twitter y el modelo de lámina que te gustaría elegir si resultases ganador/a.

-Comparte el sorteo en Facebook o Twitter con la foto de la lámina que más te guste de las que te he enseñado.

Puedes participar hasta el 29 de diciembre. 

El resultado se publicará en este mismo post en los días sucesivos.

El ámbito del sorteo es internacional.

Me reservo la facultad de eliminar cualquier participación fraudulenta bajo mi criterio.

Ya sé que es difícil decidirse, ¿pero cuál te gusta más?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.