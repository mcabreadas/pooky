---
date: '2015-07-12T19:00:15+02:00'
image: /tumblr_files/tumblr_inline_nr2imiPZhn1qfhdaz_540.jpg
layout: post
tags:
- cine
- trucos
- crianza
- planninos
- recomendaciones
- familia
- juguetes
title: LEGO con Jurassic World
tumblr_url: https://madrescabreadas.com/post/123900593905/lego-jurassic
---

<iframe src="https://www.youtube.com/embed/T_2jF-_LnNg" frameborder="0"></iframe>

## ¿Habéis visto la nueva película Jurassic World?

Nosotros la vimos el día de su estreno, el 12 de junio, y he de decir que a mi hijo mediano le encantó. Yo la veo un poco fuerte para niños pequeños, pero a partir de 9 años creo que les puede gustar bastante.

## En palabras de mi pequeño os la cuento:

“El nuevo dinosaurio Indominius Rex tenía parte del velociraptor y de los demás dinosaurios, y les comunicó que destruyeran a los humanos que iban con pistolas. Ahora el era su nuevo lider, porque antes era un humano.

 ![image](/tumblr_files/tumblr_inline_nr2imiPZhn1qfhdaz_540.jpg)

Una chica liberó al Tyrannosaurus Rex para que matara al modificado genéticamente, luego lucharon, y de repente cuando el Tyrannosaurus le había empujado rompió la valla donde había un dinosaurio marino gigante, y cuando olió la carne , se lanzó, lo metió al agua y se comió al dinosausio malo.”

Yo creo que está bastante claro, no?   

Lo que más le gustó de la película fue cuando los niños iban en la bola azul

Su dinosaurio favorito, el Velociraptor porque es el más rápido.

## Nuevos sets LEGO Jurassic World

Si os flipan los dinosaurios, como a mi hijo, os recomiendo los nuevos sets de LEGO, que estamos deseando probar:

- **Emboscada al Dilofosaurio:** ¡Peligro! Gray se ha perdido en el parque y un dilofosaurio persigue su girosfera. Únete al soldado de camuflaje y acude al rescate en el clásico 4X4 de Jurassic World. Usa los prismáticos para localizar al feroz dinosaurio y pide refuerzos con el walkie talkie. Esquiva sus poderosas mandíbulas y usa el gancho para atraparlo por las patas. ¡Captura al dinosaurio antres de que logre subir a la parte trasera del 4x4 y activar la función de explosión! Incluye dos minifiguras con un arma y diferentes accesosios  

 ![image](/tumblr_files/tumblr_inline_nr2ihc31z71qfhdaz_540.jpg)
- **La Huida del Raptor:** ¡Un problema más en Jurassic World! Uno de los raptores ha escapado de su recinto y está libre. Sube al veloz todoterreno y sal en su busca. Dispara el cañón y usa el bastón eléctrico del soldado de camuflaje para reducir al feroz depredador. Mientras, el otro raptor ha activado la función de trampa para atraer al cuidador de dinosaurios al interior del recinto. Corre a salvarlo antes de que el raptor lo atrape con sus poderosas mandíbulas. Incluye dos minifiguras con diferentes armas: un cuidador y un soldado de camuflaje   
 ![image](/tumblr_files/tumblr_inline_nr2ikyXuTV1qfhdaz_540.jpg)

## El videojuego

LEGO, en colaboración con Warner Bros, ha diseñado un videojuego del film que estará disponible en las tiendas al mismo tiempo que el estreno de la película. En él podremos jugar con todos los dinosaurios de la película, y ayudar a los protagonistas en esta fantástica aventura.

¿Lo has probado? 

_NOTA: Post NO patrocinado_