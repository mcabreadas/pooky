---
date: '2016-11-16T16:53:17+01:00'
image: /tumblr_files/tumblr_inline_oglg6suHsA1qfhdaz_540.png
layout: post
tags:
- familia
- planninos
- local
title: Un paseo por las nubes de la Hacienda del Carche
tumblr_url: https://madrescabreadas.com/post/153261654704/actividades-familia-vino
---

Este fin de semana vivimos uno de los días en familia más bonitos que hemos pasado. Fuimos invitados por las [Bodegas Hacienda del Carche](https://www.haciendadelcarche.com/es/inicio/), en Jumilla, Murcia, a una visita guiada por sus instalaciones, una prueba de aromas, mientras las fieras, no sabéis como lo pasaron en los talleres y juegos que les organizaron, y una comida campera con platos típicos de la tierra regada con sus vinos TAUS (Tierra, Agua, Uva y Sol), como no podía ser de otro modo.

 ![](/tumblr_files/tumblr_inline_oglg6suHsA1qfhdaz_540.png)

## Un día en familia
 ![](/tumblr_files/tumblr_inline_oglgdf4pOk1qfhdaz_540.png)

Pero si una palabra describe nuestra jornada, esa es “cariño”. Esta gente de la Hacienda del Carche lo pone en todo lo que hace. 

Si los escuchárais hablar de las cepas y las uvas, parece que se refieren a sus propios hijos, y describen el proceso de elaboración del vino con una pasión, que explica lo bueno que les sale.

Además, haciendo honor al nombre de la actividad a la que asistimos: “Un día en familia”, Carmen y Miriam nos hicieron sentir de esa forma… como si estuviéramos en familia.

Una de las cosas más divertidas que hicimos fue intentar adivinar los aromas de los vinos, y estoy muy orgullosa de haber acertado aunque sólo fuera uno, porque es realmente difícil, aunque nos dijeron que educando el olfato se pueden conseguir grandes resultados.

 ![](/tumblr_files/tumblr_inline_oglghcS0V91qfhdaz_540.png)

Pero lo realmente bonito del día, lo que realmente me inspiró, fue el paisaje rojizo de los viñedos en otoño, y poder pasear entre ellos mientras mis hijos correteaban libremente y jugaban a hacer barro con el resto de niños.

 ![](/tumblr_files/tumblr_inline_oglgc3lKDQ1qfhdaz_540.png)

Mi marido se reía, menos mal que ya conoce mi lado ñoño romántico, cuando empecé a sentirme como Aitana Sánchez-Gijón en la película [Un Paseo por las Nubes](http://www.sensacine.com/peliculas/pelicula-13668/).

Vale, puede no gustarte la película, pero no creo que haya nadie a quien sus paisajes le dejen indiferente.

Pues allí estaba yo, que sólo me faltó agitar los brazos con las alas en camisón de raso blanco, con mi Keanu Reeves particular detrás. (No tengo mucho remedio, lo sé)

![](/tumblr_files/tumblr_oglh4kOr6S1qgfaqto2_r1_400.gif)

## Curiosidades sobre el proceso del vino

Mi familia es de tierra de vinos, por eso yo ya conocía algo sobre el proceso de convertir la uva en vino.

Cuando iba al pueblo de pequeña a pasar los veranos con mis abuelos, veía pasar los tractores con la uva recién recogida y a las cuadrillas de jóvenes que volvían de vendimiar al caer la tarde.

 ![](/tumblr_files/tumblr_inline_oglgegBCTX1qfhdaz_540.png)

A mi abuelo le gustaba salir al campo a ver sus viñas cada mañana muy temprano, era una de sus pasiones. Así que me acordé mucho de él durante la visita.

Una vez me llevó a la cooperativa donde se hacía el vino, y recuerdo cómo me impresionaron esos depósitos enormes. Apenas podía imaginarme que pudiera hacerse tanto vino como para llenarlos. Por eso me pareció muy interesante lo que nos contó Miriam en la Hacienda del Carche.

Os voy a destacar algunas curiosidades que aprendí sobre el vino y sobre las Bodegas Hacienda del Carche:

 ![](/tumblr_files/tumblr_inline_oglgm9CBV21qfhdaz_540.png)
- El vino necesita una temperatura constante de 20ºC, por eso se ha adapatado la bodega a los cambios de temperatura, que en verano llega a los 40ºC y en invierno a - 5ºC, y su fachada se construyó de piedra de Villamayor, Salamanca, que hace efecto cueva para mantener la temperatura constante.  
- Existen dos tipos de cultivo de la vid, en espalderas (como en la foto), y en vaso (cepas bajas que sólo se pueden vendimiar a mano).  
 ![](/tumblr_files/tumblr_inline_oglgjmgE3E1qfhdaz_540.png)

- La Hacienda del Carche tiene una producción limitada de 300.000 botellas por año porque siempre que se puede la vendimia se hace de forma natural (a mano), solo si hay peligro de perder la uva se meten las máquinas para hacerlo rápidamente y poder salvarla.  
- La uva morada, por dentro es blanca, y para que coja el color que tiene el vino tinto se mezcla con la piel.  
- Si en el prensado se rompe la pepita de la uva amargará el vino, por eso se hace de forma suave.  
- En la actualidad no se encuentran posos al acabar las botellas de vino (en el culo) porque se eliminan filtrándolo debido a que el gran mercado de china no entiende mucho de vinos, y pensaban que los posos eran signo de vino en mal estado. A pesar de esto, en las Bodegas Hacienda del Carche son “antifiltrados” porque prefieren respetar los valores primarios de la uva, por eso los centrifugan, lo que hace que salgan al mercado dos meses después que el resto de bodegas.  
- Tienen un vino sin filtrar, más respetuoso con la manera tradicional y natural de elaborarlo, que se comercializa bajo el nombre de “Infiltrado” en una botella especial decantadora, que recoge los posos y evita en gran parte que lleguen a la copa al servirlo.  
- La Hacienda del Carche realiza colaboraciones con la Universidad de Murcia en las que los estudiantes hacen proyectos, y algunos han llegado a comercializarse, como es el caso del vino “Escarche”, elaborado con uvas congeladas artificialmente con nieve carbónica, dando lugar a un vino dulce muy fresco y fácil de beber ideal para aperitivos (doy fe).  
- Sólo usan corchos naturales para tapar las botellas  
- El vino reserva ya no se comercializa porque no se demanda, sólo los vinos jóvenes y crianzas.  
- Los vinos rosados y blancos son los preferidos por las mujeres, y los tintos, por los hombres en general.  
 ![](/tumblr_files/tumblr_inline_oglg9lIc1h1qfhdaz_540.png)

¿A que muchas cosas no las sabíais?

## Casa rural del labrador
 ![](/tumblr_files/tumblr_inline_oglg1lPU1m1qfhdaz_540.png)

También conocimos la Casa Rural de El labrador, a pocos kilómetros de la Hacienda del Carche, un sitio ideal para pasar unos días en familia o con amigos, ya que tiene 8 plazas, además de un comedor enorme para hacer grandes comidas, y un salón también muy grande que invita a pasar agradables tardes de invierno junto a la chimenea.

 ![](/tumblr_files/tumblr_inline_oglg5iv01x1qfhdaz_540.png)

Está muy bien acondicionada con calefacción, y todas las comodidades, pero respetando la estética retro que tuviera antaño cuando era verdaderamente la casa de los labradores de las tierras. 

 ![](/tumblr_files/tumblr_inline_oglg2on6z41qfhdaz_540.png)

Si queréis conocer esta zona de la región de Murcia, os la recomiendo.

 ![](/tumblr_files/tumblr_inline_oglg3symDR1qfhdaz_540.png)

Como también os recomiendo participar en las visitas para familias que regularmente organizan en Hacienda del Carche, sin duda un plan genial para pasar un inolvidable día en familia.

 ![](/tumblr_files/tumblr_inline_oglh58FE5B1qfhdaz_540.png)

_(Con mis compañeras Lou, del blog [Pintando una mamá](http://www.pintandounamama.es/), y Ana, del blog [¿Mi mundo? Los míos](http://mimundolosmios.es/), con las que fue un lujo compartir un día tan especial)._