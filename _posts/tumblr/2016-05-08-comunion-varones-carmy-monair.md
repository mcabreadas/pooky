---
date: '2016-05-08T17:20:49+02:00'
image: /tumblr_files/tumblr_o6v6n5yPVz1qgfaqto2_400.gif
layout: post
tags:
- familia
- eventos
- moda
- comunion
title: comunion varones carmy monair
tumblr_url: https://madrescabreadas.com/post/144045318329/comunion-varones-carmy-monair
---

![](/tumblr_files/tumblr_o6v6n5yPVz1qgfaqto2_400.gif)  
 ![](/tumblr_files/tumblr_o6v6n5yPVz1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_o6v6n5yPVz1qgfaqto3_400.gif)  
  

## Vestidos y trajes de Comunión: Mon Air, Varones y Carmy

## Día Mágico de FIMI

Este año las Comuniones nos quieren trasladar a otra época. Los vestidos románticos o decimonónicos, y los aires vaporosos son los protagonistas para las niñas.Para los niños, encontramos los marineros renovados, donde el lino es la clave, y en los que los niños se sentirán cómodos.Cada año, las principales marcas de ropa de Comunión y ceremonia presentan sus novedades en un evento organizado por FIMI, que se llama “Paseo Mágico”, al que tuve la suerte de asistir para mostrarte las novedades para la temporada 2017.En los gifs de arriba te muestro trocitos del desfile, que fue espectacular. Por orden, encontramos las siguientes firmas:

[Mon Air](http://www.monair.es/)

Sin poder abandonar la búsqueda de lo insólito nos adentramos en un mundo donde los sentidos cobran vida. Obras maestras fruto de la tradición nos inspiran en esta colección donde los conceptos se encuentran y se cruzan.

La vuelta al pasado, al olor que trae recuerdos infinitos, asentados en valores de compromiso, confianza, convicción y tradición nos elevan para sobrepasar el mundo real y transportarnos al espiritual.

La inocencia, el tiempo perdido y no explorado, lo visten con tules extremos más allá del límite para preparar un camino. Miradas profundas encontramos a nuestro paso.

[Varones](http://www.onevaron.com/es/colecciones/25/comunion-17)

Varones presenta una colección renovada y fresca para vestir a los niños en el día más importante. Su línea más tradicional de marinero y almirante se renueva en colores y tejidos, dando como resultado unos modelos novedosos y actuales, perfectos para un evento tan importante.

Para el marinero utilizan diferentes linos y esterillas, con diferentes texturas.

Los almirantes son una explosión de colores y detalles, como vivos pespuntes que dan toque de originalidad.

Carmy de Luxe, [Litle Paula](http://www.carmy.es/web/little-paula-2017/), [Paola Dominguín](http://www.carmy.es/web/paola-dominguin-2017/), [Aire Barcelona](https://www.airebarcelona.com/coleccion/comunion/coleccion-comunion-2017/comunion-y-arras-2017/), [Rosa Clará](https://www.rosaclara.es/coleccion/comunion/coleccion-comunion-2017/comunion-y-arras-2017/)

 ![](/tumblr_files/tumblr_inline_ol3uoiFXNm1qfhdaz_540.jpg)

## Galería completa de fotos

Para ver la galería completa de fotos de la pasarela de moda de Comunión y ceremonia para 2017 pincha [aquí](https://www.flickr.com/photos/142190598@N06/)

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).