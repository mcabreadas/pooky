---
date: '2015-08-23T18:38:39+02:00'
image: /tumblr_files/tumblr_inline_o39zhqsqUl1qfhdaz_540.png
layout: post
tags:
- premios
title: Vuelta al cole. Sorteo 20 euros en material escolar
tumblr_url: https://madrescabreadas.com/post/127402530479/sorteo-material-escolar
---

SORTEO CERRADO

Ya tenemos ganadora del sorteo, y en cuanto nos de su email por privado, procederemos a hacerle llegar el vale de 20€ para que lo gaste en material escolar para la vuelta al cole. ¡Le va a venir de rechupete!

Gracias a todos por la participación

**La ganadora es Isabel Piñel.**

¡Enhorabuena!

 ![](/tumblr_files/tumblr_inline_o39zhqsqUl1qfhdaz_540.png)

_Post original:_

Os veo preocupadas, incluso angustiadas con la vuelta al cole y la cantidad de gastos que tenemos que soportar las familias, y si somos numerosas, más aún.

[En este post os contaba qué hemos hecho nosotros y os daba algunos consejos para ahorrar.](/2015/08/23/vuelta-cole-ahorro.html)

Por eso no he querido desaprovechar la oportunidad que me brindan en [Materialescolar.es](http://www.materialescolar.es/) de aliviaros un poquito el bolsillo con el sorteo de un **vale de 20 € más IVA** para gastar en su tienda on-line de material escolar.

No sé si conocéis esta web, pero tienen de todo, y muy organizadito.

Además tienen precio mínimo garantizado, el envío es gratuíto a la península, tenéis 30 días para devoluciones, y te lo llevan a casa, como máximo en 3 días laborables.

Pero lo que más me ha gustado es que tienen descuentos para familias numerosas! Cualquier ayudita nos hace un mundo, verdad?

También tienen juegos educativos muy interesantes a buen precio. [Los podéis ver pinchando aquí.](http://www.materialescolar.es/papeleriaonline/juegos-diset-lectron)

**Requisitos para participar en el sorteo**

1 Hazte fan de [MaterialEscolar en Facebook](https://www.facebook.com/MaterialEscolaronline).

2 Comparte el concurso en Facebook de forma pública.

3 Deja un comentario en este post diciendo que participas y tu nick de Facebook.

4 Tener dirección de envío en la península.

Y si quieres, aunque no es obligatorio, pero sí divertido, puedes suscribirte a mi blog en la columna lateral derecha. 

El plazo acaba el 1/9/15, y el resultado se publicará en este blog en los días sucesivos.

Me reservo el derecho a cancelar las participaciones que me parezcan inapropiadas según mi criterio, y sin explicación alguna.

¿Te apuntas?

_NOTA: [Materialescolar.es](http://www.materialescolar.es/) me ha cedido un vale de 20 € más IVA para sortear en el blog, y otro de valor equivalente para uso personal con el fin de que pruebe sus servicios_.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.