---
layout: post
title: Al rescate de supermamá
date: 2011-04-28T15:33:00+02:00
image: /tumblr_files/tumblr_inline_pgav19OicO1qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/5011667060/al-rescate-de-supermamá
---
Clara es mamá de dos niños y una niña de dos, cinco y siete años de edad. Su marido trabaja fuera de la ciudad, a una distancia respetable, y se ve obligado a levantarse muy temprano y salir de casa cada día para no volver hasta las diez de la noche, por no hablar de cuando le toca viajar y pasar varios días fuera.

![image](/tumblr_files/tumblr_inline_pgav19OicO1qfhdaz_540.jpg)



Clara es una mujer menuda, alegre y vivaracha, nunca le he visto una mala cara, a pesar de sus ojeras, ya crónicas. Es su expresión la que la hace más admirable aún. La primera vez que reparé en ella fue en una reunión del colegio, y fue porque estaba amamantando a su bebé, que hoy tiene dos años, y en esa misma pose alzó el brazo que le quedaba libre e hizo una pregunta con toda la naturalidad del mundo. Me impresionó su confianza en sí misma.

Al año siguiente, también en la reunión del cole, le dio el potito a su bebé y después éste empezó a gatear por el aula. Yo lo cogí y lo tuve casi toda la reunión en brazos. Ella me lo agradeció enormemente.

Nos hemos ido viendo en convivencias, cumpleaños… no falta a ningún evento, y cuenta su caso como algo normal y sin quejarse. Al contrario, está contentísima porque ha conseguido sacar un par de horas a la semana para ir a la piscina a las diez de la noche, a pesar de que se levanta cada día a las cinco o seis de la mañana para hacer las tareas domésticas antes de irse a trabajar. También está contenta porque ahora se puede permitir pagar a una persona para que vaya una mañana a su casa a hacer “lo más gordo”,pero no siempre ha sido así.

![image](/tumblr_files/tumblr_inline_pgav19Xt7l1qfhdaz_540.png)



Me confesó que habían tenido problemas económicos y que no se podía permitir una reducción de jornada, con la consiguiente disminución de sueldo. Eso sí, de lo único que se lamenta, aunque bromea con ello, es de que lleva nueve años seguidos pagando guardería, y que cuando ha ido a hacer la matrícula para el curso siguiente no le han rebajado ni siquiera el pico, “cinco euros”. 

Y es que la unidad familiar de Clara no tiene derecho a ayudas económicas para guardería, ni acceso a Guarderías Públicas o Centros de Conciliación porque supera los ingresos mínimos de los demás optantes, y sin embargo, dichos ingresos no son suficientes para pagar un centro privado sin ver gravemente mermada su capacidad económica impidiéndole pagar una ayuda para el cuidado de sus hijos y las tareas del hogar. 

Estamos hablado de que en 9 años, la familia de Clara lleva gastados casi 40.000 €, a razón de unos 400 € mensuales (repaso el cálculo dos veces porque creía que me había equivocado).

Si a eso le sumamos que su marido pasa la mayoría del tiempo fuera de casa y que no tiene familiares cerca que la puedan ayudar, obtenemos el resultado de una situación injusta, casi insostenible, pero muy habitual en nuestra sociedad que, si bien da lugar al surgimiento de súper heroínas anónimas, pienso que no hay necesidad de llevar a las mujeres a estos extremos, como si criar a tres hijos fuera una proeza sobrenatural privilegio sólo de unas pocas pocas familias campeonas. 

Ya me diréis si tengo razón.

![image](/tumblr_files/tumblr_inline_pgav1aDIIr1qfhdaz_540.gif)