---
date: '2016-10-20T12:40:10+02:00'
image: /tumblr_files/tumblr_inline_ofcbxwzeq91qfhdaz_540.jpg
layout: post
tags:
- adolescencia
- moda
- familia
title: 'Moda juvenil: los estilos que se llevan'
tumblr_url: https://madrescabreadas.com/post/152064501939/moda-juvenil-los-estilos-que-se-llevan
---

Atrás quedaron los tiempos en que vestíamos a nuestros hijos como nos gustaba a nosotras sin pedirles ni siquiera su opinión, porque la moda era algo que no les importaba lo más mínimo.

Ahora los niños cada vez más son más críticos con todo, también con la ropa que les ponemos, sobre todo conforme se van aproximando a la temida adolescencia que, no te engañes, llega antes de que lo imaginas.

Por eso, y basado en mi experiencia con mis dos mayores, quiero hablarte hoy de algunos estilos de ropa juvenil que están de moda para que, cuando te vengan pidiendo [ropa hip hop](http://www.plns.es/), un vestido Naif, pantalones boho, o una chaqueta rockera, no te pille in fraganti.

Soy partidaria de ayudarles a que encuentren su propio estilo orientándolos y guiándoles un poco si se dejan, pero para eso tenemos que ponernos al día nosotras.

## Estilo Hip Hop

El [estilo basado en la música Hip Hop](https://es.wikipedia.org/wiki/Hip_hop) me encanta para los teens, tanto para chico como para chica por su comodidad a base de pantalones anchos con tiros muy bajos, zapatillas deportivas, remeras y camperas también grandes…

 ![chica hip hop](/tumblr_files/tumblr_inline_ofcbxwzeq91qfhdaz_540.jpg)

Como esta forma musical aparece entre los jóvenes de las ciudades de Estados Unidos, en donde se acostumbra mucho a jugar al baloncesto, muchas de las prendas están directamente relacionadas con la ropa deportiva de este tipo y con distintos equipos. Así, abundan las sudaderas, ideales para esta época de frío, y tan ponibles.

 ![chico hip hop](/tumblr_files/tumblr_inline_ofcc8fLDlG1qfhdaz_540.jpg)
## Estilo Naif

Muy diferente es el estio Naif, que en francés significa “ingenuo”. Su ropa transmite dulzura, ingenuidad, espontaneidad y un cierto aire romántico.

 ![vestido invierno ormiga](/tumblr_files/tumblr_inline_ofcbe9oMGf1qfhdaz_540.png)

Abundan los vestidos y faldas de vuelo, con estampados coloridos de rayas, lunares o flores, encajes y transparencias, camisas con cuellos bebé y mil y un accesorios para el pelo como diademas, lazos, boinas…

Ideal para niñas románticas y soñadoras.

## Estilo Boho-Chic

Otro de mis favoritos es el estilo Boho-Chic, una mezcla entre bohemio y hippie, por la comodidad de sus prendas amplias, pantalones bombachos y estampados alegres, camisas over size, superposición de prendas, que da pie a que decidan su propia forma de llevarlo. 

 ![ropa bo teen](/tumblr_files/tumblr_inline_ofcbvaAD8O1qfhdaz_540.jpg)

Me parece ideal para esta edad en la que sienten cierta rebeldía, y no quieren nada que pueda parecer infantil, pero sin embargo, en este estilo encontrarás un aire fresco, que me parece perfecto nuestros teens.

## Estilo Preppy

Súper de moda en este otoño invierno, el estilo basado en la clásica forma de vestir de los universitarios en EEUU, más bien pijo, o que aparenta pertenencia la élite.

 ![ropa colegial porches](/tumblr_files/tumblr_inline_ofcbgzCxvb1qfhdaz_540.png)

Se trata de ropa clásica, de colores pastel, blanco o primarios, donde abundan los jerseys de algodón, escotes de pico, chalecos, chaquetas, pantalones tipo chino, polos, blusas y camisas abotonadas, faldas plisadas…

 ![](/tumblr_files/tumblr_inline_ofccckgkuY1qfhdaz_540.png)

## Estilo Glam-Rock

Ideal para jóvenes que huyen de lo clásico, también me encanta para esta edad en la que se sienten rebeldes.

 ![](/tumblr_files/tumblr_inline_ofcbieG5oe1qfhdaz_540.png)

Con el negro como color estrella, combinado con toques metálicos, encontramos chaquetas de cuero, o imitación, pantalones pitillo, leggins, botas e infinidad de camisetas que pueden combinar a su gusto con accesorios como pulseras, anillos grandes, cruces, calaveras, tachuelas y pinchos, lentejuelas, brillos…

Espero que este resumen te haya servido para aclararte un poco sobre algunos de los estilos que están más de moda esta temporada, porque el universo de la moda es infinito, y es fácil perderse entre la cantidad de propuestas que hay para niños y jóvenes, tanto, que no tienen nada que envidiar a la moda para adultos.

Yo, a mis hijos todavía nos los dejo que decidan del todo por ellos mismos, así que procuro estar al día y adelantarme a proponerles ideas dentro de lo que a mí me parece más adecuado, pero que creo que les va a encajar con su forma de ser y con sus gustos, sin imponerles nada, pero tampoco dejándoles que decidan ellos solos cómo vestirse al cien por cien.

¿Tú cómo lo haces con tus hijos?