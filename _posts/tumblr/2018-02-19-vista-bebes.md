---
date: '2018-02-19T07:37:14+01:00'
image: /tumblr_files/tumblr_inline_p6m7ttvTeC1qfhdaz_540.jpg
layout: post
tags:
- salud
- crianza
- bebe
title: Mira por los ojos de tu bebé
tumblr_url: https://madrescabreadas.com/post/171043868399/vista-bebes
---

No te pasa que a veces desearías meterte en la cabeza de tus hijos para saber lo que piensan, o cómo perciben las cosas? Seguro que si lo consiguiéramos nos ahorraríamos muchos quebraderos de cabeza! Pues yo te voy a ayudar un poco con esto haciéndote ver tal y como ve un bebé según su edad: desde recién nacido hasta el año. ¿Lo quieres probar?

[![Vista Bebé](/tumblr_files/tumblr_inline_p6m7ttvTeC1qfhdaz_540.jpg)](https://www.visiondirect.es/aplicacion-vista-del-bebe/)
## Recién nacidos son “miopes”

Durante sus primeras semanas de vida, los bebés no pueden ver de lejos. Todo aquello que esté [a más de 30 centímetros de distancia lo ven borroso](https://www.visiondirect.es/aplicacion-vista-del-bebe), justo la distancia que suele haber entre entre los ojos del bebé y los de la madre cuando damos pecho.

Yo, en cierto modo ya me imaginaba cómo veían mis hijos de bebés porque soy miope, así que me puedo hacer una idea de cómo ven los primeros meses de vida. Y digo en cierto modo porque yo no tengo más que colocarme las gafas o las [lentillas](https://www.visiondirect.es/acuvue-oasys), para solucionar el problema, pero los bebés tendrán que esperar hasta los 12 meses para poder ver bien del todo. Por eso nacen con otros sentidos más desarrollados, como el olfato o el tacto, para poder reconocer a la madre. ¿No te parece maravilloso?

Menos mal porque los recién nacidos presentan solo un 5% de la capacidad de visión de los adultos y, aunque son capaces de ver colores, no los identifican bien, sólo perciben el contraste, como por ejemplo, el entre el blanco y el negro.

Pero a medida que pasan los meses, comienzan a enfocar más la vista y van ampliando la gama de colores que son capaces de diferenciar. 

## Con 1 o 2 meses, colores primarios

Con 1 ó 2 meses, ya deben ser capaces de distinguir colores primarios como el rojo o el amarillo, pero no será hasta los 5 ó 6 meses, cuando empiecen a diferenciar las distintas gamas de un determinado color.    

## Antes de los 6 meses pueden bizquear

Recuerdo el susto que me llevé con mi pequeño cuando me percaté de que bizqueaba. Incluso lo llevé al oftalmólogo, pero me tranquilizó al explicarme que los bebés no pueden enfocar a un mismo punto con los dos ojos a la vez hasta los 4-5 meses, y que si a los 6 meses seguía observando ese síntoma u otros como  **mover los ojos de un lado a otro cuando está cansado, inclinar la cabeza para poder ver objetos o acercárselos a los ojos**  que lo volviera a llevar.

 ![](/tumblr_files/tumblr_inline_p45krsAmZE1qfhdaz_540.png)

_foto gracias a visiondirect.es_

## A los 8 meses ya reconocen nuestro rostro

Con 7-8 meses la visión del bebé ya es lo suficientemente nítida como para reconocer a las personas. Además, notarás que tu bebé empezará a mostrar interés por objetos, aunque sean pequeños, así como por imágenes o dibujos. En este punto también comienzan a mover los ojos con más agilidad, sin tener que mover la cabeza, y ya son más conscientes de las diferentes partes de su cuerpo. No es de extrañar que observen sus pies y manos con detenimiento. Para estimular su visión, asegúrate de poner a su alcance juguetes con formas y colores variados. También, puedes empezar a leerle libros con dibujos.

## Todos los bebés tienen los ojos azules

La melanina es el pigmento que determina el color de tus ojos, piel y pelo. Cuando nacemos los niveles de melanina suelen ser más bajos. Por ello, muchos bebés al nacer presentan un color claro de ojos. Normalmente, el mayor cambio lanina suelen ser más bajos. Por ello, muchos bebés al nacer presentan un color claro de ojos. Normalmente, el mayor cambio de color se produce entre los 6 y 9 meses, tendiendo los ojos a oscurecerse. 

Este cambio suele ser tan gradual que, algunas veces, apenas se notan diferencias. Entre los 10 y 12 meses de edad la mayoría de los niños ya cuentan con su color definitivo de ojos. Pero, algunos expertos indican que el color de ojos puede seguir variando, incluso, hasta los 6 años. Esto, sin embargo, no es muy frecuente. 

Conviene consultar a tu pediatra si ves que tu bebé tiene un ojo de cada color como, por ejemplo, azul y marrón. Esto puede estar relacionado con condiciones genéticas poco comunes como el [Síndrome de Waardenburg](https://es.wikipedia.org/wiki/S%C3%ADndrome_de_Waardenburg).

## Hasta los 12 meses no ven bien

Aunque nuestra visión no deja de evolucionar, la vista del bebé comienza a ser tan nítida como la de un adulto con un año, pudiendo identificar objetos y personas a una distancia considerable. Además de ser capaz de mantener los ojos fijados en un objeto durante más tiempo, también reconocerá a sus personajes o dibujos favoritos. 

En esta etapa, por tanto, es especialmente importante vigilar indicios de un posible problema ocular como lagrimeo, asimetrías o movimiento nervioso de los ojos. 

Yo me di cuenta de que mi hijo pequeño no veía bien a los 4 años porque guiñaba los ojos con mucha frecuencia, como si le picaran… Lo llevé al oftalmólogo pensando que tendría una infección, y salió con gafas, pero tan feliz porque por primera vez en su vida era capaz de ver bien!

 ![](/tumblr_files/tumblr_inline_p45kj3PNw61qfhdaz_540.png)

Así que no descuides los ojos de tus hijos, y revísalos a partir de los 6 meses, al menos una vez al año.