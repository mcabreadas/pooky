---
date: '2016-03-17T09:34:17+01:00'
image: /tumblr_files/tumblr_inline_o46d50bOQF1qfhdaz_540.png
layout: post
tags:
- trucos
- colaboraciones
- ad
- recomendaciones
- familia
- moda
title: Vistelos de primavera en What´s Up! Kids. Cupón descuento
tumblr_url: https://madrescabreadas.com/post/141192589159/moda-infantil-primavera-wspkids
---

Desde que mis niños han crecido tanto que han superado la talla 10 me he dedicado a buscar marcas de ropa infantil que tengan línea de teenagers o, lo que es lo mismo, que hagan ropa especial para esa edad difícil en la que ya no quieren vestirse con un estilo demasiado pero tampoco podemos vestirlos de mayores.

La marca [What´s Up! Kids](http://www.shopwspkids.com/) ha sacado esta temporada primavera/verano 2016 una nueva línea de ropa Junior muy fresca y moderna. A los teens les encanta ir rompedores y llevar su propio estilo, así que veo este tipo de ropa ideal para ellos.

 ![](/tumblr_files/tumblr_inline_o46d50bOQF1qfhdaz_540.png)

Mi Princesita ha elegido un conjunto de algodón de pantalones tipo chándal súper frescos con detalles en flúor, camiseta rosa (como no) con lentejuelas con un diseño muy molón, y un fular a juego con los pantalones que queda muy chulo.

 ![](/tumblr_files/tumblr_inline_o46d51AUHn1qfhdaz_540.jpg)

Ya veis que es topa muy cómoda, ideal para el trote diario y para que disfruten cada momento.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/oPWCYFk1dIY" frameborder="0" allowfullscreen></iframe>

El niño eligió este conjunto, también de algodón, surfero de un color verde que me parece muy original y favorecedor. Así que, aunque sea en la ciudad, imaginó que surfeaba en el parque sobre olas de asfalto (me encanta la imaginación de los niños) 

![](/tumblr_files/tumblr_inline_o46gadGVF41qfhdaz_540.jpg)

## Temas de la nueva colección

Este año la colección primavera/verano tiene temas como 

- Tropical garden,   
- Bumbulama,   
- My City,   
- Varsity girls,   
- Mon amour   
- Blue bird  

  para niñas

 ![](/tumblr_files/tumblr_inline_o46d52awhI1qfhdaz_540.jpg)
- Vintage surf,   
- BLue Ocean,   
- Garenero Island,   
- Filling station  
- y Super run   

para niños

 ![](/tumblr_files/tumblr_inline_o46d52CX3P1qfhdaz_540.jpg)

Te dejo esta selección de las prendas que más me han gustado de la nueva colección, y un código por ser lectora de mi blog por el que podrás beneficiarte de un 30% de descuento en cualquier compra en la tienda on-line haasta el 22 de marzo:

## Código descuento 30%: CABREADAS

 [New born 1-24 meses](http://www.shopwspkids.com/c181508-new-born-1-24-meses.html)

 ![](/tumblr_files/tumblr_inline_o46d52ZMdh1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o46d523bau1qfhdaz_540.jpg)

[Baby: 3-6/24-36 meses](http://www.shopwspkids.com/c181509-baby-nina-3-6-24-36-meses.html)

 ![](/tumblr_files/tumblr_inline_o46d54Ky2d1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o46d5430Bc1qfhdaz_540.jpg)

[Niño/a 2-10 años](http://www.shopwspkids.com/c181521-nina-2-10-anos-2016.html)

 ![](/tumblr_files/tumblr_inline_o46d543ruE1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o46d55mVHV1qfhdaz_540.jpg)

[Junior 12-14 años](http://www.shopwspkids.com/c181535-junior-12-14-anos.html)

 ![](/tumblr_files/tumblr_inline_o46d55HhbY1qfhdaz_540.jpg)
## Outlet de invierno al 60%

Además, tienen un outlet de invierno donde están todas las prendas al 60% de descuento, que viene genial porque todavía hace fresquito, y los niños cambian de talla con tanta facilidad que a veces se nos queda corta la ropa que hemos comprado al principio de temporada.

¿Cuál te gusta más?

Sigue What´s Up! Kids en [Facebook](https://www.facebook.com/WspKids-416104098556278/), [Twitter](https://twitter.com/wspkids), o [Instagram](https://www.instagram.com/wspkids/) para estar al día de sus novedades.

Si te ha gustado, compártelo. No te cuesta nada, y me harás feliz :)