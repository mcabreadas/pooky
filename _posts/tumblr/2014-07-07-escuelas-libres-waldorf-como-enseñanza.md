---
date: '2014-07-07T10:00:00+02:00'
image: /tumblr_files/tumblr_inline_n650r7rJCN1qfhdaz.jpg
layout: post
tags:
- crianza
- educacion
title: Escuelas libres. Waldorf como enseñanza alternativa. Experiencias y opiniones
tumblr_url: https://madrescabreadas.com/post/91030296153/escuelas-libres-waldorf-como-enseñanza
---

Una amiga me invitó a una charla para conocer de primera mano el método Waldorf. Casi me pierdo para llegar a la escuela, pero mereció la pena el paraje, la tranquilidad, el olor a azahar que se respiraba y sobre todo escuchar hablar con tanta pasión sobre el cuidado de niños en uno de los colegios privados más diferenciales que conozco.

## ¿QUÉ NOS CONTARON EN LA CHARLA SOBRE LAS ESCUELAS WALDORF?

## ¿Qué es la pedagogía Waldorf?

La pedagogía Waldorf fue creada por [Rudolf Steiner](https://amzn.to/2FVk8fC) , poco después de la Primera Guerra Mundial (1919), cuando recibió el encargo de organizar y dirigir una escuela libre en Stuttgart para los hijos de todos los empleados de una fábrica de cigarrillos Waldorf. Pretende una educación del niño y del joven hacia la libertad, dentro de una continua renovación de la sociedad. La idea de utilizar el arte de educar como fundamento de una verdadera renovación social, está presente siempre.

En la primera etapa del niño, de 0 a 6 años, el movimiento y el juego libre son los protagonistas. Nos olvidamos de los pupitres y los libros. En esta fase, quieren moverse y aprender mediante el juego. Se trata de un juego relajado, por lo que practicarán unos ejercicios de respiración antes de iniciarlo, según nos contaron los profesores. También es importante el momento en que se saca al alumno el juego,ya que se hace con una campanita y nunca bruscamente porque al fin y al cabo está en un mundo que ha creado.

## ¿Cómo son las escuelas Waldorf?

El ambiente de cada escuela está perfectamente estudiado para crear un entorno que sea como una prolongación del hogar, de la familia. Los colores cálidos son los protagonistas, los grandes espacios en la naturaleza, los árboles para trepar, y los [juguetes waldorf](http://) fabricados a mano por los propios profesores, y libres de funciones para que el niño desarrolle su creatividad sin condicionamiento alguno son clave para este modo de educar.

 ![image](/tumblr_files/tumblr_inline_n650r7rJCN1qfhdaz.jpg)
## ¿Qué actividades se hacen?

Las actividades artísticas son el centro de cada jornada, como por ejemplo modelar cera de abeja. 

 ![image](/tumblr_files/tumblr_inline_n650qdURuV1qfhdaz.png)

La alimentación es a base de [productos ecológicos](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_il_tl?ie=UTF8&tag=madrescabread-21&keywords=comida%20ecologica&index=aps&camp=3638&creative=24630&linkCode=xm2&linkId=1b5d8384f58959901403fac6bb38a008) , y cada día se trabaja un cereal transmitiéndoles el sentimiento de gratitud hacia la tierra que nos da los alimentos.

 ![image](/tumblr_files/tumblr_inline_n650s0sCDj1qfhdaz.jpg)

Se trabaja la “autoeducación” del maestro para ser capaz de descubrir y conseguir que aflore lo que de especial trae cada niño, y respetará el ritmo evolutivo individual de cada uno. El compromiso con sus alumnos es el eje de funcionamiento de los maestros Waldorf, que será el mismo en cada etapa educativa, me transmitieron un gran entusiasmo por su labor. Creen en lo que hacen y eso se nota porque se les ilumina la cara cuando lo cuentan.

## EL PUNTO DE VISTA LEGAL DE LAS ESCUELAS WALDORF

Las escuelas Waldorf las ponen en marcha padres y profesores a través de una asociación, y posteriormente solicitan la homologación  a la Administración. 

En España los gobiernos autonómicos de Madrid, Cataluña, Castilla y León, Galicia y País Vasco han autorizado como centros educativos privados 8 colegios y decenas de escuelas infantiles Waldorf, una homologación que no todos los colegios han conseguido, ya que los trámites resultan a veces complicados.

El resultado es que los alumnos de estas escuelas no homologadas constan legalmente como no escolarizados, incluso la en Comunitat Valenciana, la conselleria los equipara a los «homeschoolers», niños cuyos padres prefieren educarlos en casa según sus propios principios, algo que no es legal en España.

## LOS PADRES OPINAN SOBRE LAS ESCUELAS WALDORF

-La psicóloga y amiga **[Diana Sánchez](http://www.dianasanchezsanchez.com/)** tiene dos hijos en la escuela [El Lirio Azul](http://www.lirioazulwaldorf.com/) de Madrid, y su experiencia está siendo muy positiva porque sus hijos están recibiendo un “ **aprendizaje vivencial”** , donde crecen y se abren si recibir presión. 

 ![image](/tumblr_files/tumblr_inline_n7zc250NoB1qfhdaz.jpg)

Dice que sus “hijos han cambiado, están como más libres, son más ellos y esto es así porque les han validado sus emociones y como son. No hay intentos de cambio ni de manipulación…”

Como

**contras**

señala que no tienen la asignatura de deporte como tal, al menos en la etapa de 0 a 6 años, aunque los niños están continuamente jugando al aire libre y subiéndose a los árboles, y que no es bilingüe, ya que el inglés no se empieza a aprender hasta los 6 años. A pesar de ello,

**Diana recomienda las escuelas Waldorf**

, pero sólo si los padres están informados de que se trata de una enseñanza muy espiritual con una base detrás, que es bueno conocer para no sorprenderse.

**-**

El terapeuta **Javier Domingo** lleva a sus hijos a la escuela Aravaca de Madrid, y cuando crezcan, tiene clarísimo que los va llevar a la de los más mayores,[Micael](http://www.escuelamicael.com/) porque no puede concebir otro tipo de educación para sus ellos. Precisamente, en esta misma escuela es donde asiste el hijo de Penélope Cruz y Javier Bardem, según [algunos medios](http://www.teinteresa.es/gente/Bardem-Penelope-unen-metodo-Waldorf_0_938306789.html)

.

[![image](/tumblr_files/tumblr_inline_n7w785G0gk1qfhdaz.jpg)](http://www.vanitatis.elconfidencial.com/noticias/2014-01-10/waldorf-el-controvertido-metodo-de-estudios-del-hijo-de-penelope-cruz-y-javier-bardem_74428/)

Javier Domingo confiesa que al principio tenía los mismos **prejuicios** contra este tipo de escuelas que las personas que no las conocen a fondo. Pero una vez conoció la de sus hijos de primera mano, comprobó que las etiquetas de hippies, de izquierdas, o alternativos son sólo eso, etiquetas, porque se encontró con gente de todo tipo, nivel social, nacionalidad… Y esto es lo que más le gusta, sobre todo la multinacionalidad.

Cuando vio la escuela [Aravaca](http://www.waldorfaravaca.es/) por primera vez le entraron ganas de llorar porque se encontró con algo que podría ser **el sueño de cualquier niño.** Las cabañas dentro de las aulas le parecieron algo mágico.  

Para este tipo de enseñanza **las disciplinas académicas son algo secundario** , los maestros son especialistas en acompañar, ya que piensan que si se valora al niño únicamente por su nivel de conocimiento puede vulnerar su autoestima. 

Javier destaca la importancia que se da en estos centros a la búsqueda del equilibrio cuerpo, alma y mente, es decir, la armonía entre lo que haces, lo que sientes y lo que piensas.

Al preguntarle por el tema económico comenta que cuesta unos 340 €/mes más comedor, unos 175 €. Y para ayudar a quienes no pueden costear estos gastos hacen un mercadillo donde colaboran todos los padres.

Sin embargo ve algunos **contras** , como por ejemplo que haya imágenes de la Virgen María y de Jesús en las aulas, y que sean tan selectivos a la hora de aceptar alumnos, ya que “evalúan” a los padres en una entrevista antes de admitirlos.

Javier **recomienda las escuelas Waldorf** , pero dice que no son para todo el mundo porque exige padres muy conscientes. 

## LA EXPERTA OPINA SOBRE LA PEDAGOGÍA WALDORF

[Azucena Caballero](http://www.pedagogiablanca.com/nosotras/) es una de las fundadoras de la [Pedagogía Blanca](http://) . Experta en aprendizaje creativo, adolescencia, educación disruptiva, métodos dinámicos y edupunk. Es licenciada en Historia, profesora, conferenciante, madre homeschooler, empresaria, y muchas más cosas.

 ![image](/tumblr_files/tumblr_inline_n7zc32KZjR1qfhdaz.jpg)

Sigo su trabajo a través de las redes sociales desde hace años y me parece una gran profesional. Y, a pesar ser una mujer muy ocupada, tuvo la amabilidad de compartir su opinión sobre la pedagogía Waldorf, lo que le agradezco enormemente. 

Azucena piensa que las escuelas Waldorf son sitios donde prima la belleza. Le parecen espacios muy bonitos para la etapa de educación infantil, pero tiene algunas dudas, sobre todo para la educación primaria por varias razones: 

-Son **demasiado directivas** para su gusto. Por ejemplo, todos los niños copian de la pizarra en sus macro libretas blancas, han de dibujar y pintar, hacer cenefitas, alrededor, les interese o no. Señala que no a todos los niños les tiene por qué gustar participar en coreografías y movimientos eurrítmicos… 

-Únicamente se usan [juguetes fabricados con materiales naturales](http://). Le parece fabuloso que se le dé tanta importancia en la etapa de juego y manipulativa en la educación infantil, pero no le gustan los extremos y piensa que un juguete de plástico o de fibras sintéticas, o una cera que no sea de origen natural, no va a dañar a ningún niño. 

-En cuanto a la **edad con la que empiezan a leer** , a pesar de ser una escuela que presume de caracterizarse por ser respetuosa con los ritmos de los niños, no siempre tienen muy en cuenta las diferencias personales. Hay niños que quieren aprender a leer con 4 años y no tener que esperar a los 7, porque sea lo que indica el sistema creado por Rudolph Steinner (y, en algunas escuelas Waldorf intentarán que no lea).

- **A**** leja a los niños de la tecnología: **no tienen en cuenta que los niños que acuden ahora a esos centros han nacido y viven en el siglo XXI, y forma parte de su realidad cotidiana. La tecnología sólo es una herramienta más, que bien usada da buen fruto.

Azucena concluye que es un tipo de escuela que **ha de renovarse,**  ya que no podemos olvidar que la primera escuela Waldorf se fundó hace ya 95 años. 

Dice que no la escogería para sus hijos porque es una escuela que adoctrina con una ideología concreta, la [**antroposofía cristiana**](http://www.casasteiner.com.ar/antroposofia.htm), en la que se transmite la creencia en seres espirituales que nos acompañan, reencarnación, fundamentos de Cristo como unión para todos, celebración del adviento, etc, y eso está bien para quienes comparten esas creencias, pero no todos los demás.

Esto último que comenta Azucena me ha llamado mucho la atención porque lo desconocía,  y al investigar sobre ello en la red he comprobado las feroces [críticas](http://www.educasectas.org/antroposofia-2/) que reciben las escuelas Waldorf  por este motivo en Estados Unidos, donde incluso hay una organización llamada [PLANS](http://www.waldorfcritics.org/concerns.html) para que se dejen de financiar con dinero público en este país. También he encontrado duras críticas en [este blog](http://webcache.googleusercontent.com/search?q=cache:pa-NFM_Imz8J:charlatanes.blogspot.com/2013/10/el-ataque-la-educacion-y-las-escuelas.html+&cd=1&hl=en&ct=clnk&gl=es&client=safari) español.

Mi opinión sobre este tipo de enseñanza alternativa al modelo tradicional, que ya sabemos que tiene grandes carencias, es que no me parece mala opción para suplir a las escuelas infantiles, pero me han sorprendido las reacciones que genera y la polémica que despierta, lo que personalmente me inquieta.

Lo que está claro es que la pedagogía Waldorf a nadie deja indiferente. 

¿Tú qué opinas?

Fuentes y fotos gracias a 

[http://colegioswaldorf.org/](http://colegioswaldorf.org/)

[http://asociacionwaldorfmurcia.wordpress.com/](http://asociacionwaldorfmurcia.wordpress.com/)

[http://www.levante-emv.com/comunitat-valenciana/2013/12/30/escuela-alternativa-clandestinidad/1064102.html](http://www.levante-emv.com/comunitat-valenciana/2013/12/30/escuela-alternativa-clandestinidad/1064102.html)

[http://www.vanitatis.elconfidencial.com](http://www.vanitatis.elconfidencial.com)