---
date: '2017-05-10T16:49:39+02:00'
image: /tumblr_files/tumblr_inline_opnc4lQZla1qfhdaz_540.png
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: 7 desayunos equilibrados para los niños
tumblr_url: https://madrescabreadas.com/post/160515841844/7-desayunos-equilibrados-para-los-niños
---

Las prisas de la mañana, el caos de preparar a los niños, el estrés o el cansancio muchas veces hacen que no prestemos la atención que merece a la comida más importante del día; el desayuno. Y no, no es un tópico. Los nutricionistas así lo dicen, y tienen toda la razón. Un desayuno equilibrado es fundamental para que nuestros hijos rindan de una forma óptima en el colegio.

 A pesar de ello, sólo un 2,8% de los niños desayuna correctamente, según el [estudio ALADINO de 2015](http://www.aecosan.msssi.gob.es/AECOSAN/docs/documentos/nutricion/observatorio/Estudio_ALADINO_2015.pdf), el 15,5% de los niños encuestados de entre 6 y 9 años sólo desayuna leche, zumo o agua. 

El Estudio de Vigilancia del Crecimiento, Alimentación, Actividad Física, Desarrollo Infantil y Obesidad en España, asegura que sólo un 8’4% de los niños incluyen en su desayuno zumo o fruta natural, frente al 39,1% , que desayuna galletas. Este estudio reveló que la prevalencia de sobrepeso era del 23,2% y la de obesidad del 18,1%, utilizando los estándares de crecimiento de la OMS.

Estas escandalosa cifra me ha hecho saltar la alarma, y he rediseñado nuestros desayunos familiares para hacerlos variados y ricos en los nutrientes necesarios para darlo todo tanto en el trabajo como en el cole.

El desayuno debería incluir al menos un lácteo, un cereal y una pieza de fruta, pero nada más lejos de la realidad. Parece ser que los alimentos más presentes en las comidas de los menores son los cereales azucarados, los zumos no naturales, la bollería industrial y por consiguiente, el azúcar.

Te doy una idea de desayuno para cada día de la semana de los que yo me he propuesto hacer a partir de ahora, porque los niños también se cansan de comer siempre lo mismo:

### Lunes

Un cuenco de yogur natural con miel, cereales corn flakes sin azúcar y trocitos de manzana y plátano. 

###  Martes

- Vaso de leche   

- Un par de [Galletas caseras con chips de chocolate](/2016/12/18/recetas-cookies-chocolate.html) o, si prefieres, [galletas plátano y avena](/2016/10/30/galletas-avena-platano.html) o un [donut casero](/2014/05/25/donuts-caseros-madres-cabreadas.html)  

- Zumo de naranja natural  
 ![](/tumblr_files/tumblr_inline_opnc4lQZla1qfhdaz_540.png)
### Miércoles

- Un vaso de [leche con cacao natural y miel](/2016/02/28/hacer-chocolate-cacao.html)  

- 4 galletas maria  

- Kiwi troceado   
 ![](/tumblr_files/tumblr_inline_opnceu8Svi1qfhdaz_540.png)
###  Jueves

- Un vaso de leche  

- Tostada de pan con AOVE  

- Un trocito de [sandía](/2014/07/24/receta-tarta-de-sand%C3%ADa-las-tartas-no-siempre.html) (si te sientes inspirada, puedes presentarla de esta forma tan original  
 ![](/tumblr_files/tumblr_inline_opncntmjyf1qfhdaz_540.png)
###  Viernes

- Yogur con trocitos de mango  
- Tostada de pan de multicereales con tomate rallado aceite y sal   

###  Sábado

- M[ilk shake de plátano y cacao natural](/2016/08/10/merienda-milkshake-platano.html)  
- Huevos revueltos  
 ![](/tumblr_files/tumblr_inline_opncbjVI8D1qfhdaz_540.png)
### Domingo

- Crepe de jamón york y queso  
- Zumo de arándanos  

![](/tumblr_files/tumblr_inline_pfx8uxMbSe1qfhdaz_540.gif)

Ya no hay excusa para seguir estando en el 2,8% de los niños que desayunan mal!