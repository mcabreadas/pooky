---
layout: post
title: Violencia contra la mujer. Una visión profesional
date: 2013-11-19T16:54:00+01:00
image: /tumblr_files/tumblr_inline_mwka38cOSG1qfhdaz.gif
author: maria
tags:
  - mecabrea
  - derechos
tumblr_url: https://madrescabreadas.com/post/67470694345/violencia-contra-la-mujer-una-visión-profesional
---
 Mi experiencia en el turno de oficio de violencia de género fue breve pero intensa. En mis primeros años de ejercicio de la abogacía hice el curso de especialización requerido por el Colegio de Abogados de mi ciudad para entrar a atender estos casos y me lancé a ayudar a las mujeres que lo necesitaban.

Por aquella época cambió el modelo de atención a estas víctimas, y pasaron de ser tratadas como el resto de denunciantes, a serles de aplicación un protocolo específico en atención a su especial vulnerabilidad. 

Se formó personal específicamente para ello, para que la mujer que se decidía a dar el paso de denunciar a su agresor no se sintiera sola o desamparada en ninguno de los pasos a seguir.

Las instituciones se unieron con un fin común: que la mujer maltratada se decidiera a poner fin a su calvario y que , una vez dado el paso, no se echara atrás por deficiencias en el sistema, o la falta de tacto de alguna de las personas con las que se topara en el camino.

Desde la persona que descuelga el teléfono cuando la mujer decide llamar al 016 para pedir ayuda, hasta el Juez que finalmente dicta la Sentencia, pasando por el agente de Policía o Guardia Civil que la recibe en Comisaría o Cuartel, y quien le toma la denuncia, el abogado que la asiste y le explica sus derechos, lo que va a pasar con sus hijos, su casa, su familia… su vida, el psicólogo que la apoya, los trabajadores sociales que la orientan y le muestran las ayudas a las que puede optar… Toda una red a su servicio para que confía, para que deje de tener miedo, para que deje de aguantar porque no tiene más remedio.

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/ATvxkXXoS14?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 

## Todavía recuerdo a María y a Lucía

La primera, de unos cuarenta años, nerviosa y fumadora empedernida. Acudió a la Guardia Civil y me llamaron por la noche. Acudí de inmediato al Cuartel. La saludé amablemente y le dije que la ayudaría en todo lo que pudiera. Las dos estábamos nerviosas, pero en sus ojos, además,  había miedo e incertidumbre. 

Contó su historia, ante la actitud exquisitamente respetuosa de la agente, que tomaba nota cuidadosamente de los detalles. Fue fundamental el ambiente de protección que creamos para que María se abriera y volcara todo lo que llevaba dentro. Nada de: “céntrese en lo importante” o “vaya concretando, señora”.

La denuncia estaba firmada. El primer paso hacia la liberación estaba dado. También se le asignó una psicóloga, y se le informó de todo detalladamente para que no tuviera ninguna duda. 

Por fin llegó el día del juicio. Jueza, fiscal y personal del Juzgado se volcaron con ella. Íbamos todos a una, y conseguimos la tan ansiada orden de alejamiento. Para ella fue como un logro personal tras muchos años de aguantar la proximidad. Estaba pletórica.

Con Lucía fue diferente. Una mujer que no llegaba a la veintena, estudiante, con un bebé de meses y un marido mucho mayor que ella, empresario de éxito y de carácter dominante. Ya comenzó a agredirla en su noche de bodas y desde ese momento no paró. 

La acompañé a la Policía a poner la denuncia, hablé con sus padres, quienes se volcaron con ella y la protegieron, le preparé la demanda de separación, incluso empecé a negociar con el abogado de su marido las condiciones… Pero ocurrió lo que tantas veces. Ella se echó atrás. Retiró la denuncia, no presentamos la demanda de separación, lo perdonó, y volvió con su agresor.

En estos casos la tristeza te embarga y la impotencia te enciende. ¿Qué más podríamos haber hecho? Al menos vio que no estaba sola, quizá otro día se decidió y volvió a llamar al 016 recordando la acogida que recibió, quizá volvió a confiar en el bando bueno. No lo sé, sólo sé que a veces recuerdo el calvario que sufrió, y pienso en su bebé, que ya será niño, y se me encoge el alma.

Años después dejé el turno de oficio de violencia de género. Ellas fueron las primeras, las que me marcaron. Las de mi primera guardia. Y a ellas, a María y a Lucía les dedico esta entrada. 

Ojalá ambas voléis libres!