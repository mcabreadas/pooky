---
date: '2016-08-18T09:17:03+02:00'
image: /tumblr_files/tumblr_oc0ii0TNwC1qgfaqto1_1280.jpg
layout: post
tags:
- recetas
- familia
title: receta pink lemonade
tumblr_url: https://madrescabreadas.com/post/149117905914/receta-pink-lemonade
---

![](/tumblr_files/tumblr_oc0ii0TNwC1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_oc0ii0TNwC1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_oc0ii0TNwC1qgfaqto3_1280.jpg)  
  

## Pink lemonade. Un refresco diferente

Una de las bebidas más cuquis que puedes preparar en una fiesta, ya sea infantil o no, y con la que quedarás genial, es pink lemonade o, lo que es lo mismo, limonada rosa.

Es ideal para cuando no quieres poner nada de alcohol, pero quieres ofrecer algo más especial que unos simples refrescos.

Nosotros la preparamos para una fiesta con chicos y chicas de unos 16 años, que hicimos en casa, y les encantó.

 

¡Todos repitieron!

Es tan fácil que te podrán ayudar tus hijos.

## Ingredientes

100 ml zumo de limón natural

100 g azúcar

60 ml zumo de arándanos

500 ml agua

## Cómo se hace

Disuelve el azúcar en el agua templada

Añade el zumo de limón y el de arándanos y remueve.

Retira la espumilla blanca que se forma arriba

Añade unas rodajas de limón

Enfría en la nevera

## Unos consejitos

Si la acompañas con unas palomitas saladas, además de quedar genial en la mesa su color blanco con el rosa de la limonada, el contraste de sabores resultará casi un vicio.

Sírvela muy fría, te recomiendo meterla al congelador media hora antes de servirla.

Utiliza jarra y vasos transparentes para que se aprecie el color rosa, ya que es la gracia de esta limonada y, si tienes pajitas decorativas, éste es el momento perfecto para usarlas.

¡A disfrutar!