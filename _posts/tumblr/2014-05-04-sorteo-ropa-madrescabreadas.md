---
date: '2014-05-04T19:00:00+02:00'
image: /tumblr_files/tumblr_inline_pb455bnxy41qfhdaz_540.jpg
layout: post
tags:
- trucos
- premios
- colaboraciones
- ad
- recomendaciones
- moda
title: Sorteo Siete Pecas. Ropa con un punto diferente.
tumblr_url: https://madrescabreadas.com/post/84732597415/sorteo-ropa-madrescabreadas
---

“Mi muñeca

se llama Rebeca

y tiene 7 pecas

sobre la nariz…”

¿Quién no recuerda esta canción de nuestra infancia?

Ya… Ya sé que la letra no es exactamente así, que la original decía “cuatro pecas”, y no siete. Pero es que cuando oí el nombre de esta tienda on line de ropa para niños y niñas por primera vez inmediatamente empecé a tararearla, y me pudo la curiosidad. Me preguntaba por qué ese nombre, “[Siete Pecas](http://www.sietepecas.com/)”.

Menos mal que Patricia, su creadora, una extremeña afincada en Madrid, sació mi curiosidad explicándome que es debido a que ella, de pequeña dibujaba siempre siete pecas en la cara de sus muñecas. Me parece muy tierno que ese recuerdo siga vivo en ella y le haya inspirado el nombre de su nuevo proyecto.

Se trata de una tienda exclusivamente on line de ropa seleccionada cuidadosamente para ser diferente para niños de entre 0 y 4 años Precisamente esa edad en que no sabes si tirarte a por las puntillas, los lazos, el rosa si es niña, y el azul para niños, o arriesgarse a algo más moderno, porque tampoco te apetece vestirlos de raperos, surferos o militares tan pronto. Y muchas veces, las tiendas del barrio no te dan más opciones (al menos en mi caso).

Tienen cosas originales de marcas como [Indikidual](http://www.sietepecas.com/indikidual), [Mói](http://www.sietepecas.com/moi), [Picnik](http://www.sietepecas.com/picnik), [Piu et Nau](http://www.sietepecas.com/piu-et-nau), [Hatley](http://www.sietepecas.com/hatley), [Chilicu](http://www.sietepecas.com/chilicu) y [Victoria](http://www.sietepecas.com/victoria)… Muy seleccionadas y con un punto diferente pero elegante. Yo, la verdad, es que soy algo clásica para la ropa de mis fieras, no rancia, pero reconozco que soy de las de rosa y azul (y me chifla vestirlos iguales!„, me los como!!!, jajajaja…) Pero el tipo de ropa de Siete Pecas me ha ganado.

Además, me han dejado elegir los modelos para el sorteo y he disfrutado como loca. Me encantan, y no descarto comprar alguno para mis hijos, sobre todo porque nos han regalado para los lectores del blog este **código promociona** l del 10% de descuento para la primera compra **canjeable hasta el 10 de mayo**.

[![image](/tumblr_files/tumblr_inline_pb455bnxy41qfhdaz_540.jpg)](http://www.sietepecas.com/)

**CÓDIGO DESCUENTO: 7BLOGS14**

Patricia dice que busca para su tienda ropa con la que los más pequeños “se sientan cómodos para trastear y descubrir el mundo y que, además, cuando pasen los años y vean sus fotos de antaño piensen: “Cómo molaba”, y no… “menuda pinta me ponían mis padres”. 

Además, las prendas  que selecciona están elaboradas con tejidos de calidad y siguiendo una producción sostenible. No todo va a ser llevar al niño como un pincel…

**CÓMO PARTICIPAR EN EL SORTEO** :

**1** - Tienes que dejar un comentario en este post indicando **qué prenda elegirías caso de ser ganador,** entre estas cuatro que os proponemos:

**Para tallas de 0 a 2 años** :

[![image](/tumblr_files/tumblr_inline_pb455buuWf1qfhdaz_540.jpg)](http://www.sietepecas.com/bodies-para-bebes/body-nina-picnik-barcos)

 

  

[![image](/tumblr_files/tumblr_inline_pb455ckEt81qfhdaz_540.jpg)](http://www.sietepecas.com/bodies-para-bebes/body-picnik-globos)

**Para tallas de 2 a 4 años:**

[![image](/tumblr_files/tumblr_inline_pb455cGGvI1qfhdaz_540.jpg)](http://www.sietepecas.com/petos-para-peques/peto-tirantes-hatley)

[![image](/tumblr_files/tumblr_inline_pb455c3W9a1qfhdaz_540.jpg)](http://www.sietepecas.com/camisetas/camiseta-picnik-bicicleta-4368)

 

  **2** - No es obligatorio, pero si queréis podéis seguir en [Facebook](https://www.facebook.com/sietepecas) o [Twitter](https://twitter.com/Siete_Pecas) a Siete Pecas.

Bueno… y a mí también me podéis seguir si os apetece, seguro que nos divertimos y cabreamos a la par.

**3** - Podéis participar **hasta el 14 de mayo hasta las 00:00 h.** Durante los días siguientes publicaré en este blog el nombre del ganador, que me tendrá que mandar por privado sus datos para el envío, que se hará directamente desde Siete Pecas.

**4** - El ámbito del sorteo es nacional.

¿Os mola o no?

Dime!, ¿con cuál te quedarías?

_NOTA: Este post NO es patrocinado_