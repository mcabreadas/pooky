---
date: '2016-06-01T10:01:45+02:00'
image: /tumblr_files/tumblr_inline_o83tb25Ui61qfhdaz_540.png
layout: post
tags:
- trucos
- libros
- planninos
- recomendaciones
- familia
title: El libro de las muñecas Gorjuss
tumblr_url: https://madrescabreadas.com/post/145247682254/libro-munecas-gorjuss
---

¿Recuerdas de niña Cerrar los ojos e imaginar que eras otra cosa?

¿Y ahora? 

¿Qué te gustaría ser? 

¿Un barco, una princesa, un zorro…? 

Hazlo por un momento. 

Para un rato y piensa en qué te gustaría convertirte y por qué… 

A mí me gustaría ser una amapola en medio de un campo de espigas de trigo, que se mece a merced del viento, y que luce bella y diferente entre lo igual.

Pregunta a tu hija… 

Juguemos por un ratito con nuestra imaginación… 

 ![](/tumblr_files/tumblr_inline_o83tb25Ui61qfhdaz_540.png)

[“Si yo fuera…”](http://www.megustaleer.com/libro/si-yo-fuera-gorjuss/ES135346#LQ) es un libro de sueños, que tiene como protagonistas a las [muñecas Gorjuss, de Suzanne Woollcott](http://www.suzannewoolcott.co.uk/), y está editado por Beascoa en tapa dura, y grande, ideal para que nos dejemos llevar e imaginemos un ratito antes de dormir. 

Te leo un trocito en este video , pero tienes que cerrar los ojos.

 No hagas trampa!! 

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Ojfk_lTxhSM?rel=0" frameborder="0" allowfullscreen></iframe>

Cada muñeca Gorjuss nos cuenta una historia y nos muestra un sentimiento. Su creadora, Suzanne Woollcott Insiste en que son retratos que muestran una emoción, por eso las deja mudas, sin boca, para hacernos percibir los sentimientos que de ellas emanan a través de la expresividad de sus ojos, o del movimiento de su pelo. 

Tampoco tienen fondo, y los objetos que coloca en cada dibujo están minuciosamente elegidos, sin dejar nada al azar, para transmitirnos la fuerza de los sentimientos de la niñez.

Suzanne Woollcott dice que cada muñeca cuenta lo que podría sentir cualquier niña, y que ella misma sitio en su infancia.

Tras esta artista hay una historia de alguien que no puede andar, y que desde su silla de ruedas ha llegado muy lejos con su arte. La prestigiosa firma Santoro London tiene la exclusiva de distribución de las Gorjuss, y las plasma en ropa, bolsos, fundas de móvil, y todo tipo de accesorios.

No me negaréis que transmiten mucha fuerza… yo creo que la misma que tiene su creadora para luchar.

Y ahora dime  ¿tú qué serías?