---
date: '2013-12-15T21:32:00+01:00'
layout: post
tags:
- crianza
- trucos
- maternidad
- postparto
- planninos
- null
- recomendaciones
- salud
- mujer
- bebes
title: 'GIMNASIA HIPOPRESIVA PARA EL POSTPARTO: Sorteo de Navidad: Ejercicio y risas
  con mamá'
tumblr_url: https://madrescabreadas.com/post/70114182222/gimnasia-hipopresiva-para-el-postparto-sorteo-de
---

**Participa en nuestro Sorteo navideño de “Ejercicio y Risas con mamá” y gana un Bono de  4 clases gratuitas de actividades para mamás y bebés.**

**Puedes usarlas para ti, para compartir o para regalar.**

Esta Navidad quiero haceros un regalo porque vosotras lo valéis. Quiero que os miméis y dediquéis un tiempo a cuidaros, que el no parar del día a día hace que muchas veces nos olvidemos de nosotras mismas.

Os presento a [Alicia de la Fuente](https://plus.google.com/112146138578858025661), fisioterapeuta especializada en obstetricia, postparto y cólico del lactante. Es una mamá emprendedora que, como muchas, ante las dificultades que encontró en el mundo laboral cuando se quedó embarazada, decidió reinventarse a sí misma y poner toda su ilusión en un proyecto dedicado a la salud de la mujer en una de las etapas más intensas y más bonitas de su vida, el postparto.

Así surge “[Ejercicio y risas con mamá](http://clasesconbebesygimnasiahipopresiva.blogspot.com)”, un blog especializado en gimnasia abdominal hipopresiva en el postparto y talleres de masaje infantil, juegos y gimnasia para bebés, en el que se ofrecen diferentes actividades para desarrollar con tu bebé:

- **Gimnasia Abdominal Hipopresiva postparto** , para recuperar la figura y tonificar tu abdomen y suelo pélvico, mientras disfrutas de la compañía de tu bebé.

Además, [tiene claros beneficios para la vida sexual](http://clasesconbebesygimnasiahipopresiva.blogspot.com.es/2013/12/mejor-suelo-pelvico-mejor-sexo.html)

- **Taller intensivo de masaje Infantil** , donde aprenderás a aliviar, y relajar a tu bebé a través de masajes, canciones y juegos de contacto.

- **Talleres de Gimnasia y Juego  para bebés de 0-3 años** , donde aprenderás los ejercicios y juegos que más favorecen a tu hijo según la etapa en la que se encuentre. Existen diferentes grupos según la edad del bebé;  0-6 meses, 6-12 meses, 1-3 años.

**Condiciones del sorteo**

\*El premio consiste en un bono de 4 sesiones de las actividades que tú elijas. Puedes combinar diferentes actividades en el mismo bono.

\*El Bono lo puedes gastar tú, compartirlo con alguna amiga o regalarlo a quien te apetezca.

\*En el caso de los talleres infantiles se contabilizará una clase por cada niño.

\*El Bono tiene una caducidad de dos meses.

\*El Bono es válido para las clases colectivas, no incluye tratamientos personalizados, ni sesiones individuales.

\*Cuando elijas la actividad a la que deseas acudir, deberás llamar o escribir un correo para reservar la plaza. Las sesiones tendrán lugar en Las Rosas, Madrid. Más info [aquí](http://clasesconbebesygimnasiahipopresiva.blogspot.com.es/p/gimnasia-abdominal-hipopresiva.html). 

El plazo para participar finaliza el 2 de enero de 2014 a las 23:00 h, y el sorteo se llevará a cabo a través de [SORTEA2](https://www.sortea2.com/)

La ganadora se publicará en este blog al día siguiente de finalizar el plazo, y tendrá 24 horas para facilitarnos sus datos.

**Cómo participar**

1-Deja un comentario en este post indicando qué actividades elegirías si resultaras ganadora del bono de 4 sesiones, y dinos tu nick de Facebook.

2-Comparte el sorteo en tu muro de Facebook. No olvides elegir la opción de “público” [una publicación](https://www.facebook.com/photo.php?fbid=689556527745473&set=a.359491987418597.90857.286180668083063&type=1) de Blog Madres cabreadas

Madrileñas! No creéis que ha llegado el momento de daros un homenaje?

Y si no vives en Madrid, también puedes participar y regalar el bono a quien tú quieras.