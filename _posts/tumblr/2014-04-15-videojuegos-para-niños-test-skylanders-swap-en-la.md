---
layout: post
title: "Videojuegos para niños: test Skylanders Swap en la Wii"
date: 2014-04-15T08:00:28+02:00
image: /tumblr_files/tumblr_n3z82syI4E1qgfaqto3_500.jpg
author: .
tags:
  - juguetes
  - 3-a-6
  - 6-a
  - "12"
tumblr_url: https://madrescabreadas.com/post/82768003425/videojuegos-para-niños-test-skylanders-swap-en-la
---
Estos días hemos estado probando en casa el nuevo videojuego que ha sacado Skylanders en la Wii. Nos gusta que los peques jueguen juntos y de una forma activa y huímos de juegos violentos y que den como única opción un jugador.

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/Fub8ypX41KI?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Opinión de mi hijo

A mis niños les encantó, sobre todo a Osito, de 6 años. Cito textualmente: "Genial, es chulísimo!“

Lo que más le gusta es la posibilidad de combinar dos Skylanders diferentes y conseguir el doble de poderes. 

Os puedo decir que le entusiasma jugar, le divierte, le engancha y no sólo los primeros días seducido por la novedad, sino que lleva así un par de semanas.

## Mi visión como madre

Es cierto que los videojuegos les pueden crear una cierta adicción, pero creo que es una buena oportunidad para que los padres eduquemos esta conducta, los enseñemos a auto controlarse, a ponerse unos límites, unos horarios de juego, y que ellos mismos apaguen cuando llega el momento. Tienen que aprender que todo hay que hacerlo en su justa medida.

El juego nos ha gustado, es divertido y es tranquilo en sus ritmos.

No es violento en general, aunque sí se matan “bichos” y hay unos malos y unos buenos muy definidos.

Está muy orientado a la edad de entre 5 y 10 años.

Mezcla puzles y acción. Los puzles están muy bien pensados para ser sencillos pero que cuesten un poco.

Pueden jugar dos, la pega es que hay que adquirir las figuras para poder jugar, y los niños pronto piden más para poder conseguir nuevos personajes conjugando las cabezas de unos con los cuerpos de otros. Así adquieren el doble de poderes.

Lo bueno es que se puede jugar con las figuras de forma independiente del videojuego.

Nuestra valoración en general es positiva y lo recomiendo. Además, a los niños les chifla.



![](/tumblr_files/tumblr_n3z82syI4E1qgfaqto6_r1_500.jpg)

Y vosotros? Habéis probado el videojuego Skylanders SWAP? Qué os ha parecido?