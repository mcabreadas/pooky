---
date: '2011-10-04T11:33:00+02:00'
layout: post
tags:
- colaboraciones
- participoen
- ad
- conciliacionrealya
- conciliacion
title: 'Qué es #ConciliacionRealYa ?'
tumblr_url: https://madrescabreadas.com/post/11016454860/qué-es-conciliacionrealya
---

En estos días de comienzo del otoño, está surgiendo en nuestro país un movimiento en torno a la conciliación familiar y laboral.

La crisis económica provoca miedo y ansiedad en las personas, pocas, que aún conservan su puesto de trabajo. La necesidad hace que tanto hombres como mujeres, tengan o no tengan hijos a su cargo, se vean en la obligación de trabajar sí o sí. Los hijos han quedado en un segundo plano sufriendo consecuencias como estrés, angustia y una obligada maduración precoz quieran o no. Abuelos exhaustos o guarderías a tiempo completo son las alternativas a un padre o una madre.

Cansados y hastiados de escuchar bonitas palabras en boca de políticos, empresarios y sindicatos,  algunas de las voces más seguidas y destacadas en la blogosfera maternal y paternal han iniciado en las redes sociales, el grupo Conciliación Real Ya, una plataforma para impulsar un cambio de paradigma en la crianza, una educación presente, consciente y respetuosa con nuestros hijos.

Queremos cambiar mentalidades respecto a la Conciliación familiar y laboral.

Conciliación Real Ya, nace de las inquietudes de un grupo de mamás y papás blogueros, que no se conforman con las medidas de conciliación de la vida laboral y familiar que hay en la actualidad y el modo de aplicarlas por algunas empresas. También se han unido a nosotros personas que no son padres o que lo quieren ser algún día y no ven un horizonte demasiado favorecedor.

Queremos criar a nuestros hijos, no que nos los críen, y queremos tener las mismas oportunidades laborales que los demás, demostrando que ambas cosas pueden ser total y eficazmente compatibles. Queremos cambiar mentalidades de empresarios, trabajadores y de la sociedad en general en pro de que la conciliación sea una realidad para hombres y mujeres. ¿Nos ayudas?

Invito a difundir, participar y animar este movimiento, como paso imprescindible para aspirar a una sociedad más solidaria  y responsable, que solo vendrá dada si empezamos a criar a nuestros hijos desde un lugar de mayor presencia, consciencia y responsabilidad.

Contamos contigo