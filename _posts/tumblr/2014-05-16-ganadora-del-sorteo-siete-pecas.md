---
date: '2014-05-16T08:07:40+02:00'
image: /tumblr_files/tumblr_inline_pb455btFkz1qfhdaz_540.jpg
layout: post
tags:
- trucos
- premios
- colaboraciones
- ad
- recomendaciones
- moda
title: Ganadora del sorteo Siete Pecas
tumblr_url: https://madrescabreadas.com/post/85893691074/ganadora-del-sorteo-siete-pecas
---

Es la primera vez que tengo la oportunidad de sortear ropa en el blog y me ha sorprendido lo que os ha gustado, y la gran afluencia de participantes. ¿Cómo no os lo había ofrecido antes?.

No será la última vez que traiga cosas tan chulas como las de [Siete Pecas](/2014/05/04/sorteo-ropa-madrescabreadas.html) por aquí.

Muchas gracias por participar en el sorteo de estas prendas que elegí personalmente, y que me las llevaría todas a casa, sin duda.

Mi favorita es el body de globos para mi Leopardito. Seguro que estaría para comérselo.

 ![](/tumblr_files/tumblr_inline_pb455btFkz1qfhdaz_540.jpg)

Enhorabuena a la ganadora Patri grande, que eligió la ranita de barquitos rosas, sin duda, ideal para este verano lucir palmito en la playa.

Ponte en contacto conmigo en privado para darme tus datos de envío, por favor.

Queremos foto!!!!

 ![](/tumblr_files/tumblr_inline_pb455bwPLm1qfhdaz_540.jpg)

Agradecimientos a Patricia, de Siete Pecas, por la generosidad de organizar este sorteo y por darnos a conocer su recién inaugurada tienda on-line.

No os despistéis, que ya le he cogido el gusto a esto de la ropita, y no tardaré en traeros otras cosas también muy molonas.

 ![](/tumblr_files/tumblr_inline_pb455cZO1v1qfhdaz_540.png)

_NOTA: Este post NO es patrocinado_.