---
date: '2016-05-23T10:00:28+02:00'
image: /tumblr_files/tumblr_inline_o7f8a4ZbzG1qfhdaz_540.png
layout: post
tags:
- educacion

title: Mi hijo no me da la mano por la calle
tumblr_url: https://madrescabreadas.com/post/144797736281/seguridad-vial-ninos
---

Hay una edad crítica en la que andar con niños por la calle puede suponer una auténtica odisea. Y si te juntas con varios niños con esos años, seguramente prefieras quedarte en casa, a no ser que haya una necesidad imperiosa de salir. 

## La edad crítica

Entre los 3 y 4 años (meses arriba, meses abajo) suele estar la edad terrible para pasear (por decirlo de algún modo) con tus hijos. A esa edad, normalmente les encanta andar, y ya no quieren ver la sillita de paseo ni en pintura. 

 ![](/tumblr_files/tumblr_inline_o7f8a4ZbzG1qfhdaz_540.png)

Podemos pasar fácilmente, y en la misma tarde, de la euforia de querer ir corriendo a todos lados, al bajón repentino y la consiguiente sentada en el suelo porque está demasiado cansado. Y no se sabe lo que es peor, porque si corre, malo, porque se puede meter a la carretera. Y si no anda, nos tocará llevarlo en brazos hasta casa (y ya pesan lo suyo…).

Y si al pequeño le gusta ir de la mano, estarás de suerte, pero como sea como el mío, que no aguanta ni medio segundo, el recorrido desde el cole  a casa puede convertirse en una pesadilla. 

 ![](/tumblr_files/tumblr_inline_o7f8eb9pis1qfhdaz_540.jpg)

Yo me estreso bastante cuando Leopardito se escapa corriendo descontrolado y tengo que hacer la maratón para pillarlo. Vivo en continuo sobresalto cuando veo que se acerca a un cruce y no sé si va a parar o no. O cuando hay mucha gente y lo pierdo de vista un segundo. 

## Qué trucos utilizo yo 

- Obligarle a que te de la mano no me funciona porque se hace daño intentando soltarse a lo bruto.   
- Le hago el **juego de las metas**. Como es un polvorilla y le encanta correr “a toda velocidad”, como él dice (y es lógico, porque salen del cole deseando moverse), lo dejo correr, pero hasta ítems que le voy marcando por el camino, de manera que cuando llega al primero, se para y me espera a que yo llegue. Entonces le marco el segundo.   
- **Las normas del juego** son sencillas. En los lugares de peligro hay que dar la mano a mamá, y en los espacios peatonales o de no peligro, puede correr hasta un punto predeterminado por mí.   
- Busco siempre **caminos que pasen por plazas, calles peatonales** , o anchas, o poco transitadas, aunque no sea el más directo a nuestro destino.  
- Procuro que siempre haga **sus necesidades antes de emprender la marcha** para que esté tranquilo (no es una tontería, muchas veces no atienden a razones porque se hacen pipí y están nerviosos y no identifican la causa fácilmente)  
- Si llevo carro de la **compra, o sillita, le dejo que los empuje**. De esta manera está controlado y además le encanta!!   
 ![](/tumblr_files/tumblr_inline_o7f8gbcY5z1qfhdaz_540.jpg)

- También es buena idea **cantar** una canción entre todos, o **contar una historia** encadenada, de forma que cada uno va contando una parte.   
- Sacarle un **tema de conversación que le interese** suele funcionar porque querrá escuchar o participar, y permanecerá a tu lado.  
- **Explicar en cada cruce la importancia de pararse** , y el peligro de los coches, sin atemorizarlo.  
- **Explicar los colores del semáforo** y dejar que sea él quien dé la salida cuando se pone verde para los peatones.  
- **Felicitarlo** cada vez que se pare y nos dé la mano para cruzar.  
- Dejar claro que **las personas que cruzan en rojo lo están haciendo mal** o se han equivocado.  
- Jamás cruzar en rojo nosotros
## Cuéntame los tuyos

Creo que alguno de estos trucos te ayudará a que los paseos en familia sean menos estresantes, ¡pero seguro que tú tienes los tuyos propios! 

¿Me los cuentas?

Más [información sobre seguridad vial aquí](https://madrescabreadas.com/2022/02/13/como-ensenar-seguridad-vial-para-ninos/).