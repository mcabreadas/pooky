---
layout: post
title: Aquellos cumpleaños felices
date: 2011-06-20T12:47:00+02:00
image: /images/posts/cumpleanos/p-cumpleanos.jpg
author: maria
tags:
  - mecabrea
  - planninos
tumblr_url: https://madrescabreadas.com/post/6719628595/aquellos-cumpleaños-felices
---
Como muchos sabréis, esta semana he tenido varios eventos infantiles obligatorios (EIO), también llamados cumpleańos de amigos de mis hijos o de hijos de compańeros de trabajo y/o amigos nuestros… En fin… esto no es lo que era, perdonadme si vosotros ya os habíais percatado, pero yo me estoy estrenando en estos menesteres y, la verdad, estoy desconcertada.

Yo soy de la generación de los cumpleańos en la terraza/patio de casa, con las cinco o seis mejores amigas, bocadillos de salchichón, sandwiches de nocilla, patatas fritas, que las más gamberras mojaban en la cocacola, y tarta de galletas de postre. Y una vez comidas dichas viandas: a la calle a jugar!

Entonces se nos unían los demás nińos del barrio y lo pasábamos de miedo jugando al plano, al mate, los espías, al stop… Anda que nos íbamos a aburrir… Y no es por nada, pero las nińas de mi clase se daban tortas por venir a mis cumples,y eso que no había piscina de bolas, ni hinchables, ni jaulas de monos (sí, de esas que tienen una red para que los nińos no se salgan), y mucho menos payasos o animadores pegando gritos para entretenernos, porque nosotros sabíamos divertirnos solitos con nuestra imaginación.

Recuerdo mis cumpleańos como los días más felices de mi vida. Mi madre me hacía mi comida favorita, a saber, patatas fritas y huevo, y venía mi abuela de fuera para pasar el día con nosotros. Yo era la protagonista, y todos me demostraban su amor de manera más patente ese día. 

También recibía regalos de familiares y amigos, pero nada que ver con lo que se regala ahora, que parece que se trata de una competición a ver quién se gasta más. Los cumpleańeros se aturden con tanto paquete, y no saben a qué prestar atención ante la mirada atónita e inocente de los pequeńos asistentes al evento,que quedan absolutamente alucinados ante tal lluvia de presentes y piensan: “¿Cuándo me tocará a mí?” Mientras, a los padres de la criatura homenajeada se les congela la sonrisa en la cara y un solo pensamiento ocupa su mente… “¿DÓNDE VAMOS A METER TODO ESTO?” Y es que una vez vi regalar una bici!

Pero bueno, cuando llega el momento de dar los regalos en estas macrofiestas  es buena seńal porque eso quiere decir que está terminando y pronto harán entrega a los asistentes de una bolsa atestada de chuches, por si alguno no había ingerido todavía la suficiente cantidad de azúcar, para que se vaya contento a la cama…

En fin, no es que no me gusten los cumples, no me malinterpretéis, soy un ser sociable por naturaleza, pero es que son tan diferentes a cómo yo los viví en mi infancia. Ahora son impersonales, en locales cerrados y artificiales, con un protocolo de actividades definido,con sus correspondientes variantes y extras en función del precio/niño. A saber:

\-hinchables/bolas/jaulamonos

\-difraces.

* merienda
* tarta
* pińata
* hinchables/bolas/jaulamonos
* regalos
* hinchables/bolas/jaulamonos

\-bolsa de chuches

Mientras, los padres allí mirándolos, mirándonos unos a otros… mirándolos otra vez..:”cómo disfrutan, eh?”- “sí… se lo pasan pipa…” Y es que, en general no se suele conocer a los demás padres o, si los conoces, es sólo de vista o de intercambiar saludos en el patio del cole… Es una situación ciertamente peculiar, y eso que, como he dicho antes, soy sociable, pero para los padres que no lo son, esto supone un verdadero trauma, y lo digo porque lo he visto, ¿eh? He visto sus cansadas miradas al infinito, sus insistentes vistazos al reloj, el sudor en su frente… Mi admiración hacia ellos ¡porque son unos campeones!Yo, por mi parte, me adapto al medio y trato de pasarlo lo mejor posible (sobre todo si hay mojitos), y más sabiendo que lo los peques disfrutan como locos.

Lo peor es que quizá no me quede más remedio que organizarlo así para mis hijos, aunque me encantaría que tuvieran, aunque fuera, un cumple como los de mi infancia. Pero la duda que se me plantea es si serían capaces de disfrutar igual, porque los tiempos han cambiado, y los niños también y, sobre todo, es que lo que ven continuamente son las macrofiestas de las que os he hablado… y eso es lo que me cabrea de este asunto. Ya me diréis si tengo razón.

Y tú, cómo celebras los cumpleaños de tus hijos?

*Photo by Senjuti Kundu on Unsplash*