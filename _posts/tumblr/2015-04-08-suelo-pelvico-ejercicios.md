---
date: '2015-04-08T08:00:20+02:00'
image: /tumblr_files/tumblr_inline_nmg3t4SefZ1qfhdaz_540.jpg
layout: post
tags:
- crianza
- postparto
- embarazo
- familia
- salud
- bebes
title: Suelo pélvico. Ejercicios y cuidados antes, durante y después del embarazo
tumblr_url: https://madrescabreadas.com/post/115831531289/suelo-pelvico-ejercicios
---

Os presento a Victoria Zamora, mamá de un bebé de 2 años, y fisioterapeuta, que acaba de emprender un proyecto sobre el cuidado de la salud para las mujeres, sobre todo para las que hemos sido madres recientemente, relacionado con el cuidado del suelo pélvico. En su web [Saludpelvica.com](http://saludpelvica.com/) encontraréis todo tipo de consejos, ejercicios y recomendaciones para prevenir lesiones, detectar problemas y posibles soluciones. 

Cuando me presentó su proyecto quise compartirlo con vosotras, porque más vale prevenir, y por eso le propuse hacerle una entrevista, a lo que accedió gustosa, y desde aquí, mi agradecimiento.

##   

 ![image](/tumblr_files/tumblr_inline_nmg3t4SefZ1qfhdaz_540.jpg)
## “¿Exactamente que es el suelo pélvico?  

El suelo pélvico es un grupo de músculos que, como su nombre indica se encuentran en la zona baja de la pelvis.

La gente a menudo piensa que la pelvis por abajo es sólida, ósea, pero es una cavidad hueca que tiene esa musculatura flexible, la cual permite soportar los órganos que hay dentro (digestivo, urinario y reproductivo, incluyendo un feto) sin que se pierda la posibilidad de abertura o salida (en la micción, defecación, o incluso para que salga un bebé en el parto).

Si se lesiona puede generar problemas: descenso de los órganos que hay dentro (prolapsos), escapes de orina, falta de sensación en las relacionessexuales,…

## ¿Qué podemos hacer para evitar que se lesione el suelo pélvico?

Esta zona se puede lesionar debido a determinadas actividades. El embarazo es una de ellas pero hay más: la pérdida de fuerza muscular en la menopausia, realizar ejercicios que causan impacto repetido en la zona, como es hacer running, volley, boxeo,…

Por eso es importante entrenarlo, especialmente en esos casos, para evitar futuras lesiones.

 ![image](/tumblr_files/tumblr_inline_nmfoov8oRh1qfhdaz_540.jpg)
## ¿Cómo puedo evitar que se lesione en el embarazo?

El embarazo y el parto son unas etapas importantes para nosotras en nuestra vida. Yo lo recordaré como un momento único y especial. A pesar de lo mágico que es, también es un cambio intenso en nuestro cuerpo, el cual puede ser traumático si incluso hay una cesárea.

El embarazo es una etapa clave para el suelo pélvico: tanto el peso del bebe sobre él (más si es múltiple), como la postura que se adopta durante ese momento o la pérdida de la faja abdominal hacen que esa zona esté sometida a un gran sobre esfuerzo.

Además, tras el parto vaginal suele resentirse esta musculatura aún más, sobre todo se pueden producir roturas musculares: desgarros y episiotomías son muy frecuentes y normalmente luego no se tratan (solamente se curan los puntos).

Para evitar los riesgos lo ideal sería entrenar el suelo pélvico **incluso antes de estar embarazada** , cuando ya sabes que quieres ser madre. En ese momento empezar a hacer ejercicios de Kegel o perineales puede ayudarte.

**Ya en el embarazo** , interesa trabajar esa zona, pero no sólo fortaleciéndola, sino también generando un músculo capaz de relajarse, para que pueda estirarse correctamente en el parto y prevenir desgarros.

Seguramente tu matrona te explique cómo realizar un **masaje perineal** , también hay **estiramientos específicos** (con o sin ayudas, como puede ser con Epi-no, un ejercitador para elastificar esta musculatura). Además, serán útiles los ejercicios para la espalda y abdomen, equilibrio, movilidad abdominal…la pelvis hay que fortalecerla en conjunto!

**Una vez que has dado a luz,** se trata de recuperar la musculatura dañada: además de ejercicios perineales, hay que fortalecer abdomen, los hipopresivos pueden ayudarte y te recomiendo acudir a un centro especializado en postparto para que te ayudan a trabajar la musculatura sin lesionarla (existen abdominales que te pueden ayudar pero otros pueden perjudicar aún más tu suelo pélvico).

## ¿Cómo sé si tengo algún problema? 

 Hay varias formas de darte cuenta de que algo falla: 

·Si ante risas, estornudos, subir escaleras,…notas que podría escaparse pis

·Si tienes dolor en relaciones sexuales (por la episiotomía mal recuperada,…)

·Si en las relaciones sexuales ya no sientes como antes

·Si notas que “algo choca” con tu vagina” (por el descenso de los órganos).

·Tienes dificultad para expulsar pis o heces.

## ¿Qué puedo hacer? ¿Cuándo debería empezar a entrenarlo?

Como he comentado anteriormente, pasado el puerpuerio, considerado un periodo de cuarentena, puedes iniciar de forma progresiva un programa de ejercicios. Este puede incluir:

- **Hipopresivos.** Seguramente has oído hablar de ellos, son ideales para el postparto porque te ayudan a recuperar tu faja abdominal y activan de forma indirecta tu suelo pélvico.

He de avisarte que su ejecución no es sencilla de aprender, ya que deben hacerse con una postura determinada (columna elongada, brazos y piernas también y con una determinada tensión).

Cuando estás situada en la posición correcta, con las articulaciones de codos, rodillas,…también en el ángulo correcto, entonces haces la contracción en apnea tras una espiración (retienes aire tras espirar).

Por eso siempre es aconsejable que te guíe para comenzar a hacerlo de la forma apropiada un profesional formado, ya que puede ser que no lo hagas correctamente, además de que hacer ejercicios en apnea también puede tener sus inconvenientes, como es el aumento de tensión arterial.

- **Pilates y otros ejercicios de zona pélvica.** Realizar sesiones de Pilates (pasados al menos 6-9 meses tras dar a luz) también puede beneficiarte, ya que tonificas el cuerpo en general, pero también la zona de “core”: la zona pélvica lesionada (abdomen, lumbar y suelo pélvico).

 No obstante, vuelvo a comentarte lo de antes: acude a un centro formado, ya que ellos saben que ejercicios son los que te ayudan, hay otros abdominales que te pueden lesionar el suelo pélvico y el abdomen. Además, siempre serán más convenientes los grupos reducidos que forman.

** **

-  **Por tu cuenta**. En casa puedes comenzar a recuperarte realizando actividades como: salir a andar (con o sin tu bebe si no tienes con quien dejarlo), hacer bicicleta o elíptica, realizar ejercicios perineales (como los de [Kegel](http://saludpelvica.com/kegel-guia-basica/) ).

** **

Son muy sencillos de realizar, sólo necesitas una colchoneta para empezar. Si quieres potenciar la zona del suelo pélvico, porque notas esas pequeñas fugas, puedes incluir en tu rutina de ejercicio accesorios como son los conos vaginales (pesos para aumentar de forma progresiva la fuerza muscular) o utensilios de biofeedback (con algunos aparatos puedes ir visualizando como contraes correctamente la musculatura y sabes cuando progresas adecuadamente).

 ![image](/tumblr_files/tumblr_inline_nme65vswWw1qfhdaz_540.jpg)¿Son útiles las bolas chinas?

** ** En realidad hay muchas alternativas para el cuidado del suelo pélvico que las mujeres desconocen, bien porque no se habla de ello, o por ser tabú. Entre ellas, puedes usar desde bolas chinas, los conos, electroestimulación, pesarios, biofeedback,..

Incluso si tienes fugas siempre puedes evitar las compresas usando unos dispositivos que se aplican como los tampones: son más cómodos, los puedes llevar incluso en bikini porque no se notan, evitas olores y problemas de higiene y a largo plazo son más económicos porque algunos son reutilizables.”

Hasta aquí la entrevista a Victoria, os recomiendo que visitéis su web porque encontraréis información súper útil sobre el cuidado de la salud pélvica: [www.saludpelvica.com](http://www.saludpelvica.com)con consejos y ejercicios para entrenarte. A mí me ha sido de especial interés los consejos sobre cómo paliar los dolores menstruales, ya que padezco de ellos desde siempre, y muy fuertes.

Pero si crees que tienes alguna potología, siempre es recomendable acudir a un profesional para que te examine.

Muchas gracias a Victoria Zamora por la entrevista.

¿Os ha quedado alguna duda? Dejadme vuestro comentario.