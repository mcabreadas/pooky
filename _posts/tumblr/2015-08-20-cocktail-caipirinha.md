---
date: '2015-08-20T18:00:26+02:00'
image: /tumblr_files/tumblr_inline_o39zpzHPIa1qfhdaz_540.jpg
layout: post
tags:
- recetas
- familia
title: 'Receta del cocktail del verano: la caipirinha'
tumblr_url: https://madrescabreadas.com/post/127163662763/cocktail-caipirinha
---

Este verano de tanto calor date un homenaje con este cocktail sencillo, pero muy rico y refrescante. Transpórtate a Brasil y hazte una caipirinha. Su sabor te va a sorprender en el primer trago, pero luego no podrás olvidarlo.

## Ingredientes
 ![image](/tumblr_files/tumblr_inline_o39zpzHPIa1qfhdaz_540.jpg)

-Limas

-Azúcar

-Hielo picado (si no tienes, usa un martillo, pero es importante que se use picadito)

 -1 botella de  Cachaça. Se trata de la bebida alcohólica más popular de Brasil,  y se obtiene de la destilación del jugo fermentado de la caña de azúcar, con una concentración de [alcohol](https://es.wikipedia.org/wiki/Alcohol "Alcohol") de entre el 38% y el 51%. Pueden también ser añadidos hasta 6 gramos de azúcar por litro.

 ![image](/tumblr_files/tumblr_inline_o39zq0VSYJ1qfhdaz_540.jpg)

Podéis encontrarla en grandes superficies, en tiendas pequeñas es más difícil que la tengan.

## Modo de hacerlo

Usa media lima por vaso.

Lo mejor es cortar la media lima es seis trozos, para que salga más fácil el jugo.

Echas los seis trozos en el vaso. 

Pones una cuchara sopera de azúcar y machacas los trozos de lima con el azúcar con el palo de un mortero hasta que salga todo el jugo, con ganas.

 ![image](/tumblr_files/tumblr_inline_o39zq03URa1qfhdaz_540.jpg)

Una vez mezclado así, llenas el vaso hasta arriba de hielo picado.

Le añades la cachaça y remueves con un palito de estos de agitar bebidas.

 ![image](/tumblr_files/tumblr_inline_o39zq15Vzk1qfhdaz_540.jpg)

Y a disfrutar!

## Consejos

El vaso es mejor que no sea muy grande, más bien achatado. Como un vaso de agua, pero no de los altos.

No piques el hielo en Thermomix ni batidora, mejor cómpralo ya picado, o mucho, mejor, pícalo tú a martillazo limpio, porque los trozos quedarán más irregulares y mola más.

El primer trago parece muy fuerte, pero dale una oportunidad y deja que se rebaje un poco con el hielo y ya verás como su sabor te conquista.

NOTA: El segundo vaso está mejor que el primero.

¿Te animas a probarlo?

Mi agradecimiento a Alicia por descubrírmelo

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.