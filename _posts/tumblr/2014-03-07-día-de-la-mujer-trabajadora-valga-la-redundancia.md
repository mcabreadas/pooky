---
date: '2014-03-07T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_pd3ecaZQy11qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- conciliacion
- mujer
title: Día de la mujer trabajadora, valga la redundancia.
tumblr_url: https://madrescabreadas.com/post/78829261750/día-de-la-mujer-trabajadora-valga-la-redundancia
---

El día 8 de marzo es el día de la mujer trabajadora.  
 Para mí es un título redundante porque normalmente una cosa va unida a la otra. Y si encima la mujer es madre, ya ni te cuento.  
 O si no, mirad el dibujo que hizo Princesita cuando le expliqué en qué consiste ese día:

![image](/tumblr_files/tumblr_inline_pd3ecaZQy11qfhdaz_540.jpg)

Sorprendidos?   
 Directamente dibujó una mamá. Se ve que pensó: “quienes son las que más trabajan del mundo mundial?”  
 Pues eso, las madres.  
 Pero una mujer, ya trabaje fuera de casa o sólo en las tareas del hogar o en las dos cosas, ya sea madre o hija o esposa o todo a la vez, lo que está claro es que nos pasamos el día trabajando.   
 Pero no sólo los días laborables, sino también los festivos y vacaciones (en mi caso, mucho más) y no sólo de día, o dentro de una jornada de 8 horas, sino, como dice mi padre, entre la noche y el día no hay pared.   
 Así es, por eso con decir el día de la mujer, se sobreentiende lo de trabajadora, sobre todo porque tenemos que trabajar más que los hombres para [cobrar lo mismo](http://brizas.wordpress.com/2014/02/18/brecha-salarial-de-genero/ "Brecha salarial"), o para demostrar que valemos “a pesar” de estar en edad fértil o con churumbeles.  
 Desgraciadamente todavía no tenemos igualdad de oportunidades laborales, y las pasamos canutas para conciliar nuestro trabajo con el cuidado de la familia.

![](/tumblr_files/tumblr_inline_pd3eca67wL1qfhdaz_540.jpg)

En mi caso, soy profesional liberal y no tengo baja de maternidad, ni permiso de lactancia, ni días de permiso por hijo enfermo…

Así que abuso de los abuelos, y hago diagramas de flujo a diario para encajar los horarios de todos y además, tener capacidad de reacción ante imprevistos, enfermedades varias, esguinces y todo lo que se nos pueda ocurrir, todo ello sin dormir más de 3 ó 4 horas seguidas al día.

Pero aquí estamos, en la lucha diaria, y con las fuerzas que sacamos del amor hacia los nuestros.