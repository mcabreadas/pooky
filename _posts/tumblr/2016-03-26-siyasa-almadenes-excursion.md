---
date: '2016-03-26T18:23:23+01:00'
image: /tumblr_files/tumblr_o4npnaz6Eo1qgfaqto1_1280.jpg
layout: post
tags:
- vacaciones
- planninos
- familia
title: Excursión Medina Siyasa y Cañón de Almadenes, Cieza
tumblr_url: https://madrescabreadas.com/post/141727573209/siyasa-almadenes-excursion
---

## Medina Siyasa

Visita cultural con los niños para conocer Medina Siyasa, en Cieza (Murcia), un conjunto arqueológico de 19 casas de la epoca árabe SS XI, XII y XII, en la falda de una montaña estratégicamente situada.

Una de las casas, de 180 m2 ha sido reconstruída; en el centro un patio, lugar donde dan todas las habitaciones: cocina, salón de invierno, salón de verano y dormitorios. Hay una especie de vestíbulo y un mirador con unas vistas increíbles.

![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto1_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto2_1280.jpg) 
 
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto3_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto4_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto5_1280.jpg)  

[Te dejo unos videos donde podrás verlo en todo su esplendor](http://bit.ly/1Uv25T4)

## Cañón de Almadenes

Estas fotos corresponden a la cueva de la Serreta, en el cañón de Almadenes, también en Cieza.

![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto6_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto7_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto8_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto9_1280.jpg)  
![](/tumblr_files/tumblr_o4npnaz6Eo1qgfaqto10_1280.jpg)  
  

Espectaculares las pinturas rupestres en excelente estado de conservación.

La bajada a la cueva es algo complicada, pero está adaptada para visitas en grupos reducidos, incluso con niños y bebés en portabebés, previa cita.

El mirador del cañón, con el río Segura abajo es una de las vistas más impresionantes de las que podrás disfrutar.

La visita de ambos parajes está organizada por el Ayuntamiento de Cieza y es guiada. Tiene un coste de 8 Euros por adulto. Los niños no pagan.

Puedes pedir cita mandando un email a oficina.turismo@cieza.es

Los niños aprenderán más historia que en una semana de clase.

¡Totalmente recomendable!

Si te ha gustado, comparte par ayudar a otras familias, y suscríbete para no perderte las novedades