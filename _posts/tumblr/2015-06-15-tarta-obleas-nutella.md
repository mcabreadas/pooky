---
date: '2015-06-15T20:12:38+02:00'
image: /tumblr_files/tumblr_inline_npzz9cbx051qfhdaz_500.jpg
layout: post
tags:
- recetas
- decoracion
- trucos
- planninos
title: 'Receta: Tarta de Ferrero Rocher en 5 minutos'
tumblr_url: https://madrescabreadas.com/post/121604418614/tarta-obleas-nutella
---

Este fin de semana he aprendido una de las cosas más valiosas de toda mi vida.

Mira que me gustan los bombones Ferrero Rocher, y a ti? 

Pues imagínate una tarta entera, no sólo con su sabor, sino también con una textura similar: crujiente y ligera… (traga, traga, que se te está haciendo la boca agua, como a mí…).

Pues me han enseñado a hacerla! Y más sencilla y rápida no puede ser. Gracias a mi cuñada voy a compartir la receta con la que quedaréis como reinas. 

Éxito asegurado!

## Ingredientes

 ![image](/tumblr_files/tumblr_inline_npzz9cbx051qfhdaz_500.jpg)

-10 obleas grandes

-Un bote de Nutella

-lacasitos, fideos de colores… o lo que se os ocurra para decorar

## Cómo se hace

-Se calienta la Nutella en el microondas para que quede un poco líquida de manera que se extienda con facilidad (no os paséis, que se quema). Aseguraos que no queda ningún trocito de la tapa dorada porque si no saltarán chispas!!

-Sobre una superficie plana se coloca una oblea (un plato normal, no, porque no cabe y la oblea se rompe a la mínima), y se extiende una capa de Nutella.

 ![image](/tumblr_files/tumblr_inline_nq7a25qArk1qfhdaz_540.jpg)

-Sobre ella se coloca otra oblea, y se extiende otra capa de Nutella, y así hasta 10 obleas.

 ![image](/tumblr_files/tumblr_inline_nq7a30W3CC1qfhdaz_540.jpg)

-Sobre la última se extiende una capa, esta vez más gruesa, y se decora al gusto con lo que se os ocurra.

-Servir fría. La podéis meter 30 minutos al frigorífico, y triunfaréis seguro!

Así quedó la nuestra:

 ![image](/tumblr_files/tumblr_inline_npzz67EprS1qfhdaz_540.jpg)

Y aquí tenéis otra idea de decoración con gominolas

 ![image](/tumblr_files/tumblr_inline_nq7a44Kk8y1qfhdaz_540.jpg)

Ah! Y la pusimos junto con otra de [tiramisú, cuya receta en 8 pasos os contaba en este post](/2015/03/27/recetas-ninos-tiramisu.html), en el centro de una mesa dulce, cuyo color predominante fue el favorito de la cumpleañera.

 ![image](/tumblr_files/tumblr_inline_npzzbkct2V1qfhdaz_540.jpg)

¿Adivinas cuál?

 ![image](/tumblr_files/tumblr_inline_npzymvhuUb1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_npzyrmsnYd1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_npzyx4h96k1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_npzzrliaSd1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_npzzuiNyBn1qfhdaz_540.jpg)