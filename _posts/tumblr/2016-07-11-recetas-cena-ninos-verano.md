---
date: '2016-07-11T21:27:39+02:00'
image: /tumblr_files/tumblr_inline_oa5z6vcqRw1qfhdaz_540.png
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: Cinco cenas rápidas para el verano
tumblr_url: https://madrescabreadas.com/post/147250626664/recetas-cena-ninos-verano
---

En vista del éxito del post sobre [recetas de comidas fresquitas, rápidas y que ensucian poco para el verano](/2016/06/29/recetas-verano-ninos.html), os traigo éste donde te doy ideas para cenas rápidas y nutritivas para niños, ideales para hacer estas vacaciones.

##   

## 1-Sandwich Club
 ![](/tumblr_files/tumblr_inline_oa5z6vcqRw1qfhdaz_540.png)

**Ingredientes**

- Pan de molde  
- Pechuga de pollo  
- Bacon  
- Queso en lonchas  
- Huevos  
- Lechuga  
- Tomate  
- Mayonesa  

**Cómo se hace**

- Tuesta 2 rebanadas de pan de molde  
- Úntalas con un poquito de mayonesa  
- Pon un filete de pechuga de pollo a la plancha  
- Una loncha de bacon a la plancha (o al microondas)  
- Pon el resto de los ingredientes  
- Encima de todos, y antes de cerrar el sandwich, pon el huevo a la plancha  
- Con un vaso pequeño haz un agujero en la rebanada que falta por poner, y que coincidirá con la yema del huevo cuando lo tapes  

##   

## 2-Tortillas de trigo rellenas
 ![](/tumblr_files/tumblr_inline_oa5zmgSbSB1qfhdaz_540.png)

**Ingredientes**

- Tortillas de trigo  
- Lechuga  
- Bacon  
- Queso rallado  
- Champiñones  

**Cómo se hace**

Pon sobre la tortilla el bacon y los champiñones a la plancha, el queso rallado mientras está caliente para que se funda

Enróllala

Puedes completar el plato con guacamole y nachos o crackers

 ![](/tumblr_files/tumblr_inline_oa5zq0u2Qb1qfhdaz_540.png)
##   

## 3-Huevos al plato al microondas
 ![](/tumblr_files/tumblr_inline_oa5zrfJKPT1qfhdaz_540.png)

**Ingredientes**

- 2 huevos por persona  
- Chorizo  
- Cebolla tierna o puerro  
- Tomate frito  
- Queso rallado para gratinar  
- Perejil  
- Sal  
-  Aceite de oliva  

**Cómo se hace**

- Hazte con unas cazuelas de barro para que quede más resultón, pero si no tienes puedes usar un plato  
- Pon en cada una la cebolla o puerro con el chorizo a taquitos y un pelín de aceite de oliva  
- Mételo al microondas 1 minuto  
- Añade el tomate frito con un poco de sal y ponlo otro minuto al microondas Separa la clara de la yema de cada huevo  
- Pon las claras 1 minuto y medio  
- Añade las yemas perejil y sal, y dales 1 minuto más, o un poco más si ves que te gustan más hechas  
- Pon queso rallado y gratina  

¡A mojar pan!

##   

## 4-Pan pizzas
 ![](/tumblr_files/tumblr_inline_oa6003FbL41qfhdaz_540.png)

**Ingredientes**

- 1 barra de pan  
- Tomate frito  
- Salchichas  
- Jamon jork, bacon, champiñones… o lo que tengas por casa que quieras ponerle  
- Queso rallado para gratinar  
- Orégano  

**Cómo se hace**

- Corta rebanadas de pan  

- Cúbrelas con tomate frito  

- Coloca los ingredientes de los que quieras hacer los pan pizzas  

- Cúbrelos con queso rallado  

- Mete al horno precalentado a 200 grados  

- Cocina durante 15 minutos aproximadamente  

- Puedes acompañar con palitos de zanahoria para mojar en crema de queso (quesitos con un poquito de leche derretidos en microondas)  

 ![](/tumblr_files/tumblr_inline_oa60620xhY1qfhdaz_540.png)

## 5-Emperador y patatas fritas con salsa boloñesa
 ![](/tumblr_files/tumblr_inline_oa607tc5UR1qfhdaz_540.png)

**Ingredientes**

- Patatas fritas   
- Emperador a rodajas (yo lo uso congelado)  
- Carne picada  
- Tomate frito  

**Cómo se hace**

- Pasa por la plancha vuelta y vuelta las rodajas de emperador y sazona con sal, ajo y perejil  

- Fríe las patatas en la freidora o hazlas en el horno para que sean más saludables  

- Haz la [salsa boloñesa](http://comida.uncomo.com/receta/como-hacer-salsa-bolonesa-7164.html) (yo, para ir más rápida, aprovecho la que me ha sobrado de la pasta, o directamente hago más a propósito)  

Sírvelo como el la foto, les encantará mojar las patatas en la salsa.