---
date: '2016-10-26T22:27:10+02:00'
image: /tumblr_files/tumblr_inline_ofo8pegZrl1qfhdaz_540.png
layout: post
tags:
- salud
- crianza
- bebe
title: 'Sordera en niños y bebés: señales para detectarla'
tumblr_url: https://madrescabreadas.com/post/152348975064/detectar-sordera-bebes
---

La detección precoz de la sordera ayuda a paliar los efectos que ésta puede ocasionar en los niños, por eso en la mayoría de los hospitales se realiza a los recién nacidos una prueba de cribado neonatal o precoz de la hipoacusia congénita.

Se debe realizar mientras el bebé está tranquilo o durmiendo. Lo habitual es hacer la prueba entre las 12 y las 48 horas de vida, antes de salir de la maternidad. En cualquier caso, debe ser realizada en el primer mes de vida.

El problema surge cuando la pérdida de audición es de naturaleza progresiva y comienza a manifestarse en etapas posteriores del crecimiento del niño, y aunque en este campo se ha avanzado mucho con soluciones como el [implante coclear](http://www.gaes.es/que-necesitas/implantes-auditivos/implante-coclear), es muy importante que observemos con atención a nuestros hijos en las distintas etapas de su desarrollo para detectar posibles deficiencias auditivas, según nos recomienda la [Asociación Clave](http://www.oiresclave.org/).

 ![](/tumblr_files/tumblr_inline_ofo8pegZrl1qfhdaz_540.png)
## Signos de posible deficiencia auditiva

Si un bebé recién nacido no muestra sobresalto ni se despierta ante cualquier ruido del ambiente.

Si entre los 8 y 12 meses no vuelve la cabeza ante un sonido familiar o no hace los típicos balbuceos de los bebés.

Si a los 2 años no entiende órdenes sencillas sin que le hagamos gestos.

Si a los 3 años no localiza la fuente del sonido y no es capaz de repetir frases.

Si a los 4 años no sabe contar espontáneamente lo que pasa.

Si a los 5 años no puede mantener una conversación sencilla.

Si pregunta “¿Qué?” con demasiada frecuencia.

Si tiene falta de concentración y pérdida de atención frecuente.

Si pone la televisión muy alta o hace mucho ruido durante los juegos.

Si orienta la cabeza hacia la fuente del sonido de forma exagerada.

Si mira a la cara fijamente cuando le hablan.

Si no escucha bien cuando le hablamos desde otra habitación.

## Más vale prevenir
 ![](/tumblr_files/tumblr_inline_ofo8n5mP041qfhdaz_540.png)

Vivimos en un mundo de excesivo ruido, ¿no te has fijado? Desde que nos levantamos con la alarma del despertador sometemos a nuestros oídos a un sin fin de molestos ruidos como el claxon de los coches, la maquinaria de las obras que encontramos camino al trabajo, el sonido de impresoras, máquinas de café, nuestros propios gritos.

Si te das cuenta, nuestros hijos cada vez tienden a poner la tele más fuerte, y eso les obliga a que también aumenten el volumen de su voz.

Creo que estamos metidos en una espiral de ruido de la que nos es difícil salir, y esto, y esto puede afectar a nuestra salud auditiva.

Recuerda que la prevención es nuestra mejor baza, por eso te dejo unos [consejos muy sencillos para cuidar y limpiar los oídos](http://www.viviendoelsonido.com/prevencion).

En los últimos días circula por la red este video que recoge el maravilloso momento en que Xander, un bebé prematuro, que nació sordo, escucha por primera vez la voz de su madre:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/PWw6IFuSD98" frameborder="0" allowfullscreen></iframe>