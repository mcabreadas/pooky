---
layout: post
title: Alimentación complementaria sin papillas para bebés. Baby Led Weaning "
  Todo Terreno"
date: 2013-11-26T13:38:00+01:00
image: /images/uploads/depositphotos_4770656_xl.jpg
author: maria
tags:
  - crianza
  - lactancia
  - nutricion
  - bebes
tumblr_url: https://madrescabreadas.com/post/68157636006/alimentación-complementaria-sin-papillas-para
---
Mi experiencia con el Baby-Led Weaning o alimentación sin papillas, es peculiar, ya que mi Leopardito, lo mismo se come un potito de farmacia que un bocadillo de chorizo. Así es.

Y es que en una familia numerosa hay que hacerse a todo. Y habrá días en que tengamos más tiempo para que el bebé se regodeé paladeando y experimentando sabores y texturas, y otros en los que lo recogeré de la guarde a las 13:00, le daré de comer, y a las 14:00h ya estaremos ambos en el colegio esperando a sus hermanos mayores, incluso le tendré que dar el postre por el camino, por poner un ejemplo.\
 Yo lo llamo “Baby Led Weaning Todoterreno”. Supongo que para los puristas de esta manera en la materia será una aberración, pero yo veo una ventaja que el niño sea capaz de tomar también un potito de emergencia en aras de la supervivencia familiar.

## \
Nuestra historia con el baby lead weaning

\
 Tras casi 6 meses de Lactancia materna exclusiva, un día me estaba comiendo un plátano, y Leopardito se tiró hacia él cual fiera corrupia. Yo, ante tal signo de salvajismo, decidí dejar a la naturaleza seguir su curso, no sin sufrir una taquicardia por el temido riesgo al atragantamiento. A partir de ese momento le empecé a dar después de las tomas de pecho la comida que había en casa.

\
 La verdad es que los desayunos estaban bien, porque le encantaban las galletas maria, en el almuerzo le daba pan con tomate y aceite, o un trozo de naranja y se relamía. La hora de la comida era más complicada por la falta de tiempo y mi falta de logística a la hora de planear platos compatibles para todos, y cuando lo lograba, el resto de la familia se me amotinaba porque no les gustaban los platos tan “sanos” o tan “de bebé”.

\
 Por otra parte, como Leopardito comía poca cantidad, enseguida tenía hambre de nuevo, lo que lo llevaba a no dormir la siesta y a pedir más comida enseguida. El caos estaba servido. Y en un momento del día en que los mayores también necesitaban su cuota de atención.

\
 Pero yo lo intenté, ¿eh? Qué conste. Hasta participé en un taller con mi amiga [Pilar Martínez](http://www.maternidadcontinuum.com/).

\
Sin embargo, las meriendas se prestaban más al experimento, ya que eran más relajadas, al aire libre…

\
 Pero al llegar la cena, sobre todo entre semana, tampoco logré llevarlo a cabo.\
 Tras varios días de frustración por mi parte, decidí hacer un sistema mixto, adaptarlo a las circunstancias particulares de nuestra e familia , y seguir intentándolo.

\
 Como lo que mejor se comía y lo más factible para nosotros era la fruta, apliqué el método sobre todo en el almuerzo y la merienda, y aproveché el veranito y el césped de la piscina de los abuelos para dejarlo como Dios lo trajo al mundo y que se enguarrara agusto.

Así, echábamos las tardes comiendo sandia, melón, ciruelas, plátano, rosquillas, galletas, pan… Mientras los dos mayores se bañaban “tranquilamente”.\
 Los churretes del jugo de la fruta le caían por su barriguita y por sus brazos rechonchetes y se le iluminaba la cara cuando conseguía agarrar con sus deditos el tan ansiado trozo de fruta.

\
 He de decir que fue pasmosa la velocidad con la que aprendió a hacer la pinza. Reconozco que el primer día me desesperé me dieron ganas de darle yo misma de comer porque vi el esfuerzo que hacía para agarrarla y no había manera, pero descubrí que si lo dejas y esperas dos días más, el progreso es tremendo… es como magia, aprenden rapidísimo!

\
 Y así pasamos las tardes de verano. El resto de comidas las chafaba y se las daba con una cuchara, y la verdad es que todo le encataba!

\
 Un día, en la fiesta del colegio de sus hermanos les dieron unos bocadillos. Princesita, que tardaba en comérselo demasiado, iba andando junto al carro de Leopardito. Cuál fue mi sorpresa cuando éste le arrebató el bocata de un plumazo, y se lo empezó a comer ante la mirada atónita de los presentes. Aún recuerdo las carcajadas de su hermanos.

\
 Y así fue como me enteré de que ya estaba listo para comer bocadillos de chorizo. Además, le chiflan!

\
 La verdad es que creo que uno de los objetivos más importantes del Baby Led Weaning lo hemos logrado, porque controla los trozos que ingiere y su tamaño, y no tiene problema en gestionarlos con destreza al tragarlos.

\
 Ahora ya tiene 15 meses y dos muelas, y no veáis cómo les saca partido. Hasta ahora por ejemplo, no había querido manzana porque se la dábamos rallada, pero ahora que es capaz de masticarla, la toma a trozos, y le chifla. Se la come genial, además, le alivia las encías cuando se pone molesto por la dentición.

Y tú? Les has dado papillas a tus hijos o te has lanzado al Baby Led Weaning?