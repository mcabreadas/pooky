---
date: '2018-07-31T12:36:40+02:00'
image: /tumblr_files/tumblr_inline_pcdpwd7lgx1qfhdaz_540.jpg
layout: post
tags:
- crianza
- planninos
- colaboraciones
- puericultura
- ad
- familia
- bebes
title: Tips para viajar en avión con niños
tumblr_url: https://madrescabreadas.com/post/176477488339/viajar-avion-ninos
---

Imaginaos un viaje transatlántico en avión con dos bebés, uno de 9 meses, y otro de 18. Puede ser un caos o, por el contrario puede no serlo serlo menos si vamos preparados.

## Reserva asiento con cuna

Lo primero que os aconsejo es que reservéis el vuelo directamente con la compañía y os aseguréis de que os asignan asientos específicos para la edad de vuestro bebé. Por ejemplos muy útil reservar asiento con cuna, ya que , sobre todo enviases largos, os permitirá ir más cómodos a todos.

## Infórmate si puedes subir a cabina una silla para coche

Os recomiendo leeros los términos y condiciones de la aerolínea que vayáis a contratar antes de reservar.

Es importante saber qué productos infantiles se pueden subir o no a cabina. El poder viajar, por ejemplo, con una **silla de coche** instalada en los asientos del avión muy interesante porque os servirá también para usarla en destino para moveros en automóvil con toda la seguridad si alquiláis un coche, por ejemplo, puesto que las compañías de alquiler de automóviles cobran alrededor de 50 euros extra por día por un asiento infantil para automóvil.

Si la silla de coche está homologada y marcada con el sello TÜV-Rheinlandq (Para Uso en Aviones) se debería poder subir al avión sin ningún problema. Todos los modelos de sillas de coche homologados para avión están registrados y listados en la web [www.tuv.com](http://www.tuv.com). Aun así, existen muchas aerolíneas que también permiten la utilización de otros modelos de sillas que no figuran en ese listado.

 ![](/tumblr_files/tumblr_inline_pcdpwd7lgx1qfhdaz_540.jpg)

_El modelo CYBEX ATON M (Grupo 0+, 0-13 kg, desde el nacimientohasta los 18 meses aprox.) es una silla de coche de la gama gold dela marca que cuenta con el sello TÜV-Rheinland “Para uso enaviones”_

No obstante, el uso de la silla de coche en el avión depende de cada aerolínea, por lo que siempre recomendamos verificarlo primero. Por ejemplo, en los Estados Unidos únicamente se permiten las sillas de coche para bebés en vuelos nacionales.

Los padres que deseen utilizar su silla de coche en el avión siempre deberán reservar un billete adicional, independientemente de la edad del niño (es decir, aunque el niño sea menor de 2 años y en principio pueda viajar gratis en el regazo de los padres).

Hay que tener en cuenta que “las sillas para automóviles se pueden colocar en cualquier asiento de la ventana, exceptuando los de las filas de salida de emergencia, así como en la primera y última fila. Si viaja con dos niños, puede reservar la ventana y el asiento del medio para instalar las sillas"

## Infórmate si puedes subir a cabina una silla de paseo compacta

El hecho de viajar con una **silla de paseo compacta** con las dimensiones aprobadas por la mayoría de las compañías como equipaje de mano puede resultar también de gran utilidad, incluso para los traslados por el aeropuerto, evitando así las posibles esperas para poder recuperar la silla al aterrizar.

 ![](/tumblr_files/tumblr_inline_pcfnoz1DH91qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_pcfnoz3zlP1qfhdaz_540.jpg)

Ésta es la CYBEX Eezy S Twist, y su mecanismo de plegado posibilita convertir la silla en un paquete compacto de 25x45x53 cm en pocos segundos y con una sola mano

## Usa portabebés

Otra opción es el porteo, ya sea con [fular, mochila o portabebés](/2017/11/20/portabebes-personalizados-teoyleo.html), mucho más práctico si estáis acostumbrados a ello, y no tendréis problema para subirlo sin facturar a la cabina.

 ![](/tumblr_files/tumblr_inline_pcfnxrzAUf1qfhdaz_540.png)

## Exige tu derecho a embarcar con prioridad

La mayoría de las compañías permiten que los padres que viajan con niños pequeños puedan embarcar primero en el avión, sin necesidad de tener que estar esperando en la cola.

No obstante, antes de emprender un viaje es importante confirmar con la compañía con la que se va a volar todos estos detalles.

Espero que te animes a viajar con niños en avión y te haya sido de utilidad esta información. Si es así, compártela ;)

_Fuente:[Cybex](http://cybex-online.com/es)_