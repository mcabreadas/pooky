---
date: '2015-01-12T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_pc7s7fb1I11qfhdaz_540.jpg
layout: post
tags:
- derechos
- hijos
- trucos
- familia
title: Deducciones para familias numerosas. Solicita el abono anticipado.
tumblr_url: https://madrescabreadas.com/post/107874391975/pago-anticipado-deducciones-familias-numerosas
---

![image](/tumblr_files/tumblr_inline_pc7s7fb1I11qfhdaz_540.jpg)

Familias numerosas, ya podemos solicitar a través de internet o por teléfono el abono anticipado mes a mes de las nuevas deducciones por familia numerosa y personas con discapacidad a cargo. La solicitud podrá ser presencialmente a través del modelo 143 a partir del 3 de febrero.

Muchos sois quienes me habéis estado preguntando todo este tiempo dudas sobre los requisitos, los beneficiarios…

En este post voy a intentar dar una visión general del tema y aclarar alguna de las cuestiones más frecuentes que me habéis planteado.

La solicitud se presentará mediante el nuevo modelo 143 de manera individual o colectiva, y su cuantía será de 100€ mensuales para familias numerosas de categoría general, y 200€ para las de categoría especial.

## Cantidad tope

La cantidad tope anual será de 1.200 € por familia numerosa categoría general, y 2.400 € por cada familia numerosa categoría especial, y se calcularán de forma proporcional al número de meses en que se cumplan los requisitos.

Pero, ojo, la cantidad anual puede resultar menor. El límite para la deducción será las cotizaciones y cuotas to­tales a la Seguridad Social y/o mutualidades devengadas en cada período impositivo. Es decir, si cotizamos menos del tope anual, obtendremos menos deducción.

## Requisitos

1- Realizar una actividad por la que se cotice a la Seguridad Social o mutualidad alternativa.

2- Ser ascendiente o hermano huérfano de padre y madre formando parte de una familia numerosa.

## ¿Qué familias se consideran numerosas?

Uno o dos ascendientes con tres o más hijos, comunes o no.

Uno o dos ascendientes con dos hijos, comunes o no, siempre que al menos uno de ellos sea discapacitado (33%) o esté incapacitado para trabajar por el INSS.

El padre o la madre separados o divorciados con tres o más hijos, comunes o no, siempre que estos se encuentren bajo su dependencia económica, aunque estén en distintas unidades familiares y no vivan en el domicilio conyugal.

Dos ascendientes si ambos son discapacitados o al menos uno de ellos, tiene un grado de discapacidad igual o superior al 65%, o está incapacitado para trabajar, con dos hijos, comunes o no.

Dos o más hermanos huérfanos de padre y madre sometidos a tutela, acogimiento o guarda, que conviven con el tutor, acogedor o guardador, pero no se hallan a sus expensas.

Tres o más hermanos huérfanos de padre y madre mayores de 18 años o dos, si uno de ellos es discapacitado, que conviven y tienen una dependencia económica entre ellos.

El padre o la madre con dos hijos, cuando haya fallecido el otro progenitor.

## Procedimiento

Una vez cumplimentado, el modelo de solicitud se podrá presentar:

![image](/tumblr_files/tumblr_inline_pc7s7gFNCG1qfhdaz_540.jpg)

1- Por internet a partir del 7 de enero en la web de la Agencia Tributaria ([www.agenciatributaria.es](http://www.agenciatributaria.es)) mediante ‘Cl@ve’, ‘PIN 24 horas’, DNI electrónico o certificado digital.

2- A partir de 7 de enero también se podrá solicitar por teléfono, en el número **901 200 345, ** aunque según he podido comprobar, la línea está bastante saturada y no se está ofreciendo una atención eficiente por esta vía. Esperemos que se solucione pronto.

3- A partir del 3 de febrero, la solicitud se podrá presentar también en cualquier oficina de la Agencia Tributaria, previa cumplimentación en la web de la Agencia, impresión y firma por parte de todos los solicitantes.

## Solicitud colectiva o individual

Cuando existan varios beneficiarios de la deducción por familia numerosa, por ejemplo, el padre y la madre, el abono anticipado se solicitará de manera colectiva o individual.

Solicitud colectiva: deberán firmar la solicitud todos los solicitantes que pudieran tener derecho a la deducción. Quien figure como primer solicitante deberá cumplir los requisitos anteriormente expuestos en el momento en que se presente la solicitud, y será quien reciba la transferencia bancaria mensual.

Solicitud individual: el importe de la deducción se dividirá entre los distintos solicitantes por partes iguales.

## ¿Quiénes pueden solicitar el abono anticipado?

-ascendientes (padre y madre) dela familia numerosa, o

-hermanos huérfanos de padre y madre que formen parte de una familia numerosa.

## Documentación a presentar

- Título de familia numerosa en vigor.

- Número de Identificación Fiscal (NIF) de todos los solicitantes.

- Alta en la Seguridad Social o mutualidad y plazos mínimos de cotización (en las solicitudes colectivas basta con que el requisito lo cumpla quien figure como primer solicitante). 

En principio se trata de un procedimiento sencillo, y se pretende que los anticipos se empiecen a cobrar lo antes posible por los beneficiarios.

Yo ya lo he solicitado, ¿y tú?