---
date: '2015-11-26T10:00:23+01:00'
image: https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=8416363498
layout: post
tags:
- crianza
- trucos
- regalos
- libros
- planninos
- embarazo
- familia
- bebes
title: El libro ideal para regalar a una embarazada estas Navidades
tumblr_url: https://madrescabreadas.com/post/133985181942/rregalo-libro-embarazada
---

Si tienes que hacer un regalo de Navidad o de amigo invisible de empresa, o simplemente porque te apetece, a una amiga o familiar que esté embarazada. os voy a dar una idea genial, y que seguro le va a encantar a la futura mamá.

Se trata del libro [Pequeñas Ideas Antes De Ser Mama. O Como Sobrevivir A La Maternidad](http://www.amazon.es/gp/product/8416363498/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8416363498&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=8416363498)

 ![embarazada leyendo](/tumblr_files/tumblr_inline_o39z0qwLk41qfhdaz_540.jpg)

¿Que cómo estoy tan segura de que vas a triunfar? 

Pues porque yo misma se lo regalé a mi cuñada, que espera una niña para enero, y esto fue lo que escribió sobre el libro:

Hace poco llegó a mis manos el libro “Pequeñas ideas antes de ser mamá… O cómo sobrevivir a la maternidad”. Fue un regalo de mi cuñada, autora de este blog, a la vista de que pronto voy a ser mami. 

Me hizo mucha ilusión, y es que como podréis imaginaros, a poquitas semanas de tener a mi pequeña en brazos, cualquier cosa que me acerque a ese momento es más que bien recibido. Y más aún si tenemos en cuenta que soy una novata en esto: la que está en camino es mi primera hija. 

 ![libro embarazada](/tumblr_files/tumblr_inline_o39z0rom1g1qfhdaz_540.jpg)

## Un libro diferente  

> Por eso, en un momento en que te asaltan tantas dudas, en que no sabes muy bien lo que te viene encima y lo mucho que va a cambiar tu vida, un libro como éste del que os estoy hablando es muy oportuno. Lo primero que me llamó la atención es que no es un libro al uso. Quiero decir que no te cuenta una historia, no tiene capítulos. Haciendo honor a su título, se dedica a darte pequeñas ideas. En cada una de sus páginas, encuentras una idea, algunas muy originales, otras esperables y conocidas, pero que siempre te arrancan una sonrisa. 

 ![mujer embarazada-leyendo](/tumblr_files/tumblr_inline_o39z0rluup1qfhdaz_540.jpg)

> Frases como 

## “Sube al desván tus tacones. Durante una temporada preferirás las bailarinas" 

> seguidas de un espacio en la página en el que el libro te invita a hacer un inventario de tus zapatos de tacón “para no olvidarlos” no dejan de mostrarte una realidad desde un punto de vista gracioso y positivo, que te llevan a vivirlo con humor y cariño, riéndote de lo que te pasa. 

## Es un libro para leer durante el embarazo. 

> Te va contando pequeños (y no tan pequeños) cambios que vas a ir experimentando, tanto durante la “dulce espera” como una vez con tu bebé en brazos. Y te va recordando como cosas que haces ahora vas a dejar de hacerlas, y como vas a empezar a hacer otras, dándote pequeños trucos para que disfrutes de lo que tienes ahora, seas consciente de lo que estás viviendo, y de lo que vas a experimentar en los meses que vienen. Son ideas, sugerencias, que te va haciendo el libro para que reflexiones,para sacarte la sonrisa y para que, siempre desde el humor, vayas reconociendo esos pequeños cambios y los vayas aceptando como algo propio de lo que estás viviendo. 

## En muchas cosas te sientes ya reflejada 

> A mí me ha pasado, y estoy convencida de que a todas las mujeres embarazadas les pasa también, mientras que en otras sabes que aún no ha llegado el momento pero que te van a pasar. Incluso a veces te sorprende, y te dice algo que no esperabas. Pero siempre lo vas leyendo y sonríes para ti, con un sentimiento entre ternura, risa, optimismo e ilusión. Porque sabes que todo lo que lees es parte de lo más grande que te va a pasar en la vida: ser mamá. 
> 
> Es un libro simpático, sencillo, y práctico, que recomiendo para regalar a quien, como yo, tiene un bebé en camino. Te ayuda a reírte de esos pequeños inconvenientes que tiene el embarazo, te recuerda que tienes que disfrutar el maravilloso momento que estás viviendo aunque lo mejor está por llegar. Y por supuesto te recuerda que no eres la única en esto, que lo que te pasa es normal y que lo que empieza es la mayor aventura de tu vida.

Ya veis que tenía razón, y que vais a acertar seguro si lo regaláis estas fiestas.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.