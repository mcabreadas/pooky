---
date: '2014-04-13T19:00:00+02:00'
image: /tumblr_files/tumblr_inline_pcrtqlKiZN1qfhdaz_540.png
layout: post
tags:
- mecabrea
title: 'Keep calm and tweet #MeCabrea'
tumblr_url: https://madrescabreadas.com/post/82594846509/keepcalm-madrescabreadas
---

No hay mejor terapia tuitera para las mamás y papás de hoy en día que se encuentran con mil dificultades para llegar a todo y que les exigen y se exigen demasiado en la lucha por sus hijos, que desahogarse en el momento en que se cabrean y les surge la necesidad de contarlo antes de explotar soltando un [#MeCabrea](https://twitter.com/search?q=%23MeCabrea&src=hash) en Twitter al mundo para sentirse liberados.

Créeme, funciona. Compruébalo.

Porque todas somos “madres cabreadas” en algún momento (o padres) te regalo esta insignia para que la coloques en tu blog o web, si tienes, o en tu muro de Facebook, en Twitter, Google+ o donde quieras.

Puedes escoger entre estos tres tamaños diferentes.

Colócatela, es tuya! Seguro que te queda genial.

Insignia 250px:

 ![image](/tumblr_files/tumblr_inline_pcrtqlKiZN1qfhdaz_540.png)

Código:

\<a href=/?utm\_source=insignia%20tweet%20mecabrea&utm\_medium=banner&utm\_campaign=keep%20calm%20and%20tweet%20%23mecabrea\>\<img src=http://31.media.tumblr.com/5053e9d3d988b432614f0897fa12edaa/tumblr\_inline\_n3wyt4DVBc1qfhdaz.png\>\</a\>

Insignia 200px:

 ![image](/tumblr_files/tumblr_inline_pcrtqlaZbx1qfhdaz_540.png)

Código:

\<a href=/?utm\_source=insignia%20tweet%20mecabrea&utm\_medium=banner&utm\_campaign=keep%20calm%20and%20tweet%20%23mecabrea\>\<img src=http://31.media.tumblr.com/ba3a89cc241f7aea4b069be1d7d2bad7/tumblr\_inline\_n3x0s4GhfE1qfhdaz.png\>\</a\>

Insignia 150px:

 ![image](/tumblr_files/tumblr_inline_pcrtqmDL5R1qfhdaz_540.png)

Código:

\<a href=/?utm\_source=insignia%20tweet%20mecabrea&utm\_medium=banner&utm\_campaign=keep%20calm%20and%20tweet%20%23mecabrea\>\<img src=http://31.media.tumblr.com/3c5891251a16396b963c2fd67b88745d/tumblr\_inline\_n3x106jXj71qfhdaz.png\>\</a\>

Te gusta?