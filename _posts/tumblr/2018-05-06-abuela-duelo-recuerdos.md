---
date: '2018-05-06T09:17:21+02:00'
image: /tumblr_files/tumblr_inline_p88xg5FHlP1qfhdaz_500.png
layout: post
tags:
- mecabrea
- mujer
- cosasmias
title: Mujeres de otra pasta
tumblr_url: https://madrescabreadas.com/post/173630645974/abuela-duelo-recuerdos
---

Hoy hace los años que murió mi abuela. No sé exactamente cuántos porque duele demasiado, y soy incapaz de llevar la cuenta. Simplemente demasiados. Sobre todo porque cada día está presente en mí, y la recuerdo por algo, y no precisamente por cosas tristes sino, más bien suelo esbozar una sonrisa porque eso me hace sentirla cerca.

Son pequeños detalles cotidianos los que me recuerdan a ella, una mujer de las que ya no hay. Como la mayoría de las mujeres de aquella época en la que o eras fuerte o eras fuerte… no te quedaba otra.

 ![](/tumblr_files/tumblr_inline_p88xg5FHlP1qfhdaz_500.png)

Historias de abuelas hay muchas, y muchas sorprendentes, incluso épicas por las barreras que tuvieron que salvar y la sociedad donde les tocó lidiar para sobrevivir siendo mujer.

Son mujeres de otra pasta, olvidadas de sí mismas, la mayoría sin un proyecto personal, ni mucho menos profesional que llevar adelante. Ni se lo planteaban siquiera porque no les tocaba. Se casaban y criaban. Cuidaban y limpiaban. Cocinaban y lavaban en el río o la fuente. Por la noche remendaban a la luz del candil. Y las más afortunadas tenían un ratito para leer a [Corín Tellado](https://es.wikipedia.org/wiki/Cor%C3%ADn_Tellado) y soñar despiertas un poquito.

 ![](/tumblr_files/tumblr_inline_oqgnmuGL3Z1qfhdaz_500.png)

Quizá por estos sueños mi abuela no se casó hasta los 24 años (una edad ya tardía para esa época) y decidió salir del pueblo. Uno de tantos donde la única forma de ganarse la vida era trabajando en el campo y en el que no había muchas fórmulas para escapar de su destino.

Y en este punto entra en escena mi abuelo. Un muchacho que rompe con lo que le tocaba y se mete al ejército. Se casa con mi abuela en un permiso, y vuelve a marcharse al día siguiente de la boda dejando ya la semilla de la vida en una muchacha más asustada que otra cosa.

A partir de ahora [nunca más estaría sola](https://madrescabreadas.com/2013/06/22/la-soledad-de-la-crianza/). Ella le tenía especial miedo a la soledad, y me repitió esas mismas palabras cuando se enteró de que yo esperaba mi primera hija:

“María, ya nunca más vas a estar sola”

Ahora le tocaba cuidar de su propia hija, pero anteriormente toda su vida había estado cuidando de gente: su madre enferma, después sus sobrinos, su padre, tíos mayores…

Ella no lo eligió, ni seguramente era su vocación, ni sé si alguien se lo agradeció lo suficiente alguna vez… 

Después nació mi madre, un bebé de casi 4 Kg, tras más de 24 horas de parto (un parto en casa, pero con colchón de lana de los que se hundían), y la amamantó hasta los 4 ó 5 años, lo que la salvó de una infección del estómago, ya que la leche materna era lo único que medio toleraba. Me contó que se la extraía manualmente y se la daba a beber en vaso.

 ![](/tumblr_files/tumblr_inline_p7woo4feGu1qfhdaz_500.jpg)

Pero la generosidad de mi abuela iba más allá, ya que se enteró de que un bebé no podía alimentarse porque su madre no podía darle pecho, y lo amamantó también. ¡Así que mi madre tiene un hermano de leche!

Cuando nacimos mi hermana y yo también nos cuidó haciendo posible que mi madre trabajara sin la angustia que pasaban sus compañeras al no tener la facilidad de dejar a sus hijos con alguien de la familia, y sin horarios (porque el trabajos de mi madre no tenía horarios).

 ![](/tumblr_files/tumblr_inline_o75ivpnLQr1qfhdaz_500.png)

Pero no sólo nos cuidaba, sino que sacaba adelante las dos casas: la suya y la nuestra. Y no es que fuera una mujer especialmente fuerte físicamente, pero tenía una fuerza interior digna de admirar. Y amor… mucho amor.

Más adelante nos fuimos a vivir todos juntos en dos casas comunicadas, y esto le facilitó la vida bastante, pero seguía teniendo que cuidar a dos familias.

No le recuerdo aficiones, salvo ir al mercado, hacer punto y [volver cada verano al pueblo](https://madrescabreadas.com/2013/08/10/ra%C3%ADces-noche-fresca-noche-de-campo-noche-de/). Ah! Y ver escaparates. Nada podía hacerle más ilusión que pasear tranquilamente por la ciudad y mirando los escaparates delas tiendas de ropa. Siempre me informaba de lo que se llevaba esa temporada, y le encantaba comprarse cosas, aunque jamás derrochó.

 ![](/tumblr_files/tumblr_mrb6xqZkNg1qgfaqto1_500.jpg)

Mi abuela era una mujer muy inteligente, que fue al colegio, pero no todo lo que le hubiera gustado, y que se quedaba absorta en la lectura, hasta el punto de que tuvo que dejar de leer porque olvidaba hacer las tareas domésticas. Por eso siempre he pensado que si hubiera tenido la oportunidad, seguramente hubiera seguido estudiando y se hubiera desarrollado profesionalmente. Pero sin embargo ni se o planteó, ni lo echó de menos porque simplemente no le tocaba, no era su turno. Imagino lo que debió suponer para ella asistir a mi graduación al recordar la cara de satisfacción y orgullo con la que me miraba y esa sonrisa que no regalaba a cualquiera.

A ella no le tocó, en cambio ella le allanó el camino a mi madre y fue pieza clave para que estudiara y posteriormente trabajara mientras sus hijas quedaban a buen recaudo porque era mi abuela la que nos cuidaba cuando caíamos enfermas, quien siempre estaba en casa con la luz de su salita encendida esperándonos. Era nuestra referencia, nuestro faro, siempre fue pieza clave en la familia viviendo por los demás.

Era una mujer de otra pasta, de las tantas abuelas que prepararon el camino y saltaron barreras para que las mujeres hoy pudiéramos elegir. Son nuestras raíces, y me enorgullece saber que soy parte de ella, de su fuerza, y le agradezco todo lo que hizo por nosotros, aunque no sé si llegué a decírselo.