---
date: '2017-05-02T11:14:33+02:00'
image: /tumblr_files/tumblr_inline_op49xmzS281qfhdaz_540.jpg
layout: post
tags:
- crianza
- adolescencia
- tecnologia
- educacion
- familia
title: La tecnología también es cosa de niñas
tumblr_url: https://madrescabreadas.com/post/160223326709/tecnologia-ninas
---

Curioso el nuevo estudio de Microsoft sobre el interés de las niñas por la tecnología; no tenía ni idea de que hay una brecha de edad en la que se sienten atraídas por este campo en igual medida que los niños, pero que sólo les dura unos años, periodo en el que, si no las [motivamos lo suficiente en el uso de la tecnología](/2015/01/07/robot-mindstorm-lego-despertador.html) pueden perder el interés, o no sentirse demasiado buenas como para apostar por ello como opción de futuro.

Coincidiendo con la celebración el 27 de abril en todo el mundo del [Girls in ICT Day](http://girlsinict.org/es) (Día de las Niñas y la Tecnología) organizado por la Unión Internacional de Comunicaciones, [Asus](https://www.asus.com/es/), [Lenovo](http://www.lenovo.com/es/es/), [Fundación Junior Achievement](http://fundacionjaes.org/), [Conmasfuturo.com](http://www.conmasfuturo.es/), [Sigesa](https://www.sigesa.com/) y [Big Van Científicos sobre Ruedas](http://www.bigvanscience.com/index.html) se han unido a [Microsoft Ibérica](https://www.microsoft.com/es-es) en la iniciativa _Construyendo el Futuro_ _/ MakeWhatsNext_ con el objetivo de fomentar el interés de las niñas y las adolescentes por los estudios científico-técnicos (también conocidos como STEM por sus siglas en inglés – _Science, Technology, Engineering & Mathematics_)

 ![](/tumblr_files/tumblr_inline_op49xmzS281qfhdaz_540.jpg)
## Una ventana de oportunidad de apenas cuatro años

Un reciente [estudio](https://1drv.ms/b/s!AiMCU9ccVxk5gfwP-YQLiaYhKLXc0Q) realizado por Microsoft entre adolescentes europeas muestra que, a los once años, las niñas tienen un interés por la Ciencia y la Tecnología equiparable al de los niños, pero que este interés decae significativamente a partir de los 15 años. Existe, por tanto, una ventana de apenas cuatro años en la que la escuela, las familias y las entidades que trabajan en este ámbito deberían centrar sus esfuerzos para evitar que las adolescentes pierdan su interés inicial por los estudios STEM.

En palabras de Pilar López, presidenta de Microsoft Ibérica:

> “Nuestro [estudio](https://1drv.ms/b/s!AiMCU9ccVxk5gfwP-YQLiaYhKLXc0Q) revela que no podemos esperar hasta las últimas etapas de su formación para despertar el interés de las niñas por la Ciencia y la Tecnología; es necesario actuar también en Educación Primaria y Secundaria. Estamos trabajando con otras empresas de tecnología, con asociaciones y entidades no lucrativas, redes de mujeres profesionales y, por supuesto, con los profesores y las instituciones educativas, para demostrar a las niñas que una carrera en el ámbito del STEM puede ser creativa, interesante e ilusionante” 

 ![](/tumblr_files/tumblr_inline_nhb3a0Ztv41qfhdaz.jpg)
## Las madres somos un ejemplo para ellas

El [estudio](https://1drv.ms/b/s!AiMCU9ccVxk5gfwP-YQLiaYhKLXc0Q) destaca que, pese a los esfuerzos realizados en este sentido, sigue siendo necesario destacar y dar visibilidad a mujeres científicas de éxito y trabajar en el aula y en el hogar en presentar _role models_ en este ámbito. Los profesores (y muy especialmente las profesoras), las familias (particularmente las madres), los hermanos y hermanas y los compañeros y compañeras de clase juegan un papel fundamental a la hora de [fomentar el interés de las niñas por la Ciencia y la Tecnología](/2015/09/26/noche-investigadores.html):

- La falta de _role models_ femeninos en Ciencia y Tecnología impacta negativamente en la confianza de las niñas interesadas en este ámbito. Tres de cada cuatro niñas piensan en un hombre cuando se les pide que visualicen a una persona que trabaje en el campo de la Ciencia y la Tecnología. 
- La autoestima de las niñas se ve muy reforzada cuando profesores y compañeros de clase reconocen sus habilidades en Ciencia y Tecnología de manera abierta. Este reconocimiento les hace sentirse aceptadas y valoradas.
- El papel de los profesores es esencial. Cuando los profesores destacan el papel de las mujeres en el campo de la Ciencia y la Tecnología, las niñas se sienten reforzadas en su interés por el STEM.
- Las niñas quieren tener contacto de primera mano con el entorno científico-técnico y con mujeres que trabajan en este ámbito. En este sentido, las visitas a laboratorios de investigación o empresas del ámbito de la tecnología se revelan como herramientas fundamentales para despertar su interés.
 ![](/tumblr_files/tumblr_inline_nhb3azB9kP1qfhdaz.jpg)
## ¿Existe una brecha de género?  

Las niñas encuestadas son optimistas y están convencidas de que pueden alcanzar sus objetivos en el terreno de los estudios STEM, pero reconocen que existe todavía una importante brecha de género en el mundo de la Ciencia y la Tecnología:

- Las adolescentes interesadas por la Ciencia y la Tecnología están seguras de sus capacidades, pero tienden a evitar ser el foco de atención cuando están en clase y mantienen un perfil bajo en el aula cuando se tratan temas científico-técnicos.
- Las niñas rechazan la afirmación de que los niños tienen una mayor capacidad natural o mejores habilidades en Ciencia y Tecnología. De hecho, sólo el 15% de las niñas encuestadas están de acuerdo con esta afirmación.
- Incluso a edades tempranas, las niñas son conscientes de la brecha de género existente en la industria de la Ciencia y la Tecnología, lo que supone una preocupación para ellas y las desanima a la hora de plantearse su carrera profesional.
- Aunque reconocen que sus madres y abuelas han tenido que enfrentarse a la existencia de estereotipos de género, las niñas encuestadas están convencidas de que su generación será la primera en la que las mujeres conseguirán un trato igualitario en todas las áreas.

## ¿Cómo suscitarles interés?

Lo que más atrae a las adolescentes consultadas para la elaboración del informe presentado por Microsoft es la posibilidad de realizar experimentos, participar en talleres y, en definitiva, enfrentarse a retos intelectuales que requieran trabajo en equipo:

- Las niñas prefieren las actividades que despiertan su creatividad y les permiten desarrollar los aspectos prácticos de la Ciencia y la Tecnología.
- Comprender cómo funciona el mundo es el principal motivador citado por las jóvenes interesadas por las disciplinas STEM.
- Cómo la Ciencia y la Tecnología cambiará la forma en la que vivimos en el futuro se perfila como una de las áreas que más interesa a las adolescentes, pero existe un 45% de encuestadas que asegura no entender el impacto de las disciplinas STEM en nuestras vidas.
- Las adolescentes no interesadas en los estudios STEM aseguran que su rechazo proviene del hecho de que estas disciplinas ofrecen menos oportunidades de interacción social y de que no estimulan su imaginación.

## Más oportunidades laborales para las mujeres

En Europa, sólo uno de cada cinco graduados en Ciencia y Tecnología es una mujer. El informe PISA que elabora la OCDE revela que los chicos son mucho más proclives a imaginarse a sí mismos como tecnólogos, ingenieros o científicos cuando se les pregunta por su futuro profesional. Se trata de un importante problema para la competitividad de la industria del Continente; la Comisión Europea ha alertado de que en 2020 existirán 900.000 empleos sin cubrir en el campo de la tecnología. Si no impulsamos el interés de las mujeres por los estudios STEM, nunca conseguiremos reducir esta brecha. Según [datos de la Comisión Europea](https://ec.europa.eu/digital-single-market/en/women-ict), la paridad de hombres y mujeres en la industria digital permitiría elevar el PIB de la Unión Europea en unos 9.000 millones de euros anuales.

Así es que, ya sabes, las madres tenemos que espabilarnos y aprovechar la edad de entre los 11 a los 15 años para [fomentar el interés de nuestras hijas por la ciencia y la tecnología](/2016/10/20/munecas-project-mc2.html) llevándolas a [talleres y actividades de robótica](/2015/01/07/robot-mindstorm-lego-despertador.html), por ejemplo, ferias de investigadores, como [la Noche de los Investigadores](/2015/09/26/noche-investigadores.html) de la que te hablé, y no sólo eso, sino ponerte tú misma a hacer experimentos porque ella querrá imitarte.

¡Hazle ver que es buena y que podría llegar a ser lo que ella quisiera!