---
date: '2017-02-05T19:59:23+01:00'
image: /tumblr_files/tumblr_oka4icqRGW1qgfaqto1_400.gif
layout: post
tags:
- cumpleanos
- trucos
- planninos
- familia
title: 'La vida pirata, la vida mejor: Fiesta infantil pirata'
tumblr_url: https://madrescabreadas.com/post/156853583659/fiesta-infantil-pirata
---

![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto2_400.gif)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto3_400.gif)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto4_400.gif)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto5_400.gif)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto7_r1_1280.jpg)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto8_r1_1280.jpg)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto10_r1_1280.jpg)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto9_r2_1280.jpg)  
 ![](/tumblr_files/tumblr_oka4icqRGW1qgfaqto6_r2_1280.jpg)  
  

## La vida pirata: Cumpleaños infantil pirata

Este año ha tocado celebrar el cumpleaños de mis hijos con una fiesta pirata, y nos lo hemos currado mucho, todo hay que decirlo.

No ha sido muy difícil porque la red está llena de ideas súper chulas, algunas muy sencillas, para lograr una decoración digna de película.

En esta ocasión tuve la ayuda de mi cuñado, quien puso el broche final con unos mapas del tesoro envejecidos con café, que transportaron a los niños a un mundo de aventuras.

También les organizó una búsqueda del tesoro por el parque, y les puso unos pañuelos en la cabeza y parches en el ojo, que terminaron de meterlos en su papel.

En los gifs, muestro cómo lograr darle a cualquier papel un aspecto antiguo y misterioso.

## Cómo envejecer el papel para la fiesta infantil pirata

Os lo explico, es muy sencillo:

1. Arrugamos el folio  
2. Lo volvemos a extender  
3. Preparamos un cuenco con café soluble  
4. Lo extendemos sobre el folio con ayuda de un algodón sin incidir muchas veces sobre una misma zona, para no levantar el papel.  
5. Dejamos secar  
6. Lo enrollamos  
7. Quemamos los bordes  
8. Lo atamos con cordón  

Ya tenemos listos nuestros mapas del tesoro!

## La “Taberna Pirata” para el cumpleaños pirata
También hicimos un **cartel gigante** que rezaba: “Taberna Pirata”, y lo pusimos en la zona donde los niños iban a tomar el almuerzo.

Lo mejor para ello es usar papel de embalar, que venden por rollos, y luego te puede servir para envolver regalos y decorarlos a tu gusto con algún cordón o tarjeta…

Para la mesa elegí un mantel neutro en gris clarito, y jugué con el rojo y el negro en platos, vasos, pajitas y servilletas.

El toque especial lo dieron los perritos piratas con unas velas de barco clavadas en cada uno.

Fíjate qué resultón! Y más sencillo, imposible:

En una brocheta clavas dos trozos de folio, el de arriba más pequeño que el de abajo, y los dejas huecos a modo de velas bufados por el viento. Después los clavas en cada perrito, y ya está!

## La mesa dulce para la fiesta infantil pirata

Como mantel usé un camino de mesa de tela de arpillera con motivos en purpurina dorada porque la mesa donde la quería hacer es una mesita auxiliar estrecha.

Para la tarta, me decidí por una tarta que simulaba un cofre del tesoro.

Solo necesitas un bizcocho rectangular de los que sueles hacer, monedas de chocolate, crema de cacao para cubrirlo y conguitos blancos.

Es muy sencillo de montar: partes el bizcocho por la mitad transversalmente, le pones monedas por los bordes, como si formaran parte de un tesoro que rebosa del cofre, y lo cubres con una fina capa de crema de cacao. Después, le pones los conguitos blancos como en la foto de arriba, que quedarán pegados, y listo.

El secreto está en hacer un delicioso bizcocho de los que mejor te salgan, y si es de cacao, mejor.

Los cupcakes los hice sencillos de cacao porque son los favoritos de mis hijos, sin crema por encima ni nada, y los puse e unas cápsulas rojas de lunares blancos.

Les clavé unos palillos con [calaveras piratas que puedes descargar gratis aquí](http://www.pequeocio.com/wp-content/uploads/2010/06/fiesta-pirata-pack.pdf). Los puse en un expositor es de Ikea.

La bebida de la mesa dulce consistió en batido de vainilla y de chocolate, que simulaban el tequila y el ron de los piratas, y los puse en unas botellas de vidrio a las que enrollé cordón, y quedaron así de auténticas.

Para los timones encontré unos salvamanteles que se me antojaron timones, así que los pegué con cola blanca a unas cajas de fresas que me dio mi amigo el frutero.

También puse una maceta de lata con Mikado clavados en un montón monedas de chocolate y conguitos.

En un cofre pequeño de madera metí más monedas de chocolate y diamantes que encontré en un bazar, en la sección de jardinería porque sirven para poner sobre la tierra de las macetas.

Para los regalitos de los invitados compré tela de arpillera, y la éécorté para hacer mini sacos para que cada niño se llevará su parte del botín.

Los saquitos los grapé, y luego les di la vuelta, pero si sabéis coser, es mejor opción, porque las grapas se escapan. Luego los rellené de monedas de chocolate, y se los llevaron tan contentos a su casa.

La verdad es que los niños ya venían motivados porque una fiesta pirata siempre les hace mucha ilusión, ya sean niños o niñas, pero la ambientación y el cariño que pusimos todos los que colaboramos fueron cruciales para que fuera todo un éxito, y fuera un cumpleaños para recordar.

Gracias a quienes me ayudaron!

Te dejo mi [tablero de Pinterest Fiestas Pirata](https://es.pinterest.com/madrescabreadas/fiesta-pirata/tablero) para que veas más ideas si quieres.