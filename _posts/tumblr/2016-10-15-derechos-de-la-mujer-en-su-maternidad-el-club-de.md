---
date: '2016-10-15T07:57:24+02:00'
image: /tumblr_files/tumblr_of2r1zLpqz1qgfaqto1_540.png
layout: post
tags:
- mecabrea
- crianza
- revistadeprensa
- maternidad
- colaboraciones
- ad
- derechos
- conciliacion
- mujer
title: Derechos de la mujer en su maternidad | El club de las madres felices
tumblr_url: https://madrescabreadas.com/post/151825784249/derechos-de-la-mujer-en-su-maternidad-el-club-de
---

[Derechos de la mujer en su maternidad | El club de las madres felices](http://elclubdelasmadresfelices.com/derechos-mujer-maternidad/)  
 ![](/tumblr_files/tumblr_of2r1zLpqz1qgfaqto1_540.png)  
De nuevo colaboro con mis amigas de El Club de las Madres Felices de Suavinex con un post tan útil como necesario para todas aquellas que somos mamás y que queremos conocer nuestros derechos en esta etapa de la vida, ya que la ley nos protege de una forma especial en algunos aspectos.  
Gracias de nuevo por contar conmigo y mi esperiencia como letrada para compartirla con el resto de mamás.  
[Aquí](http://elclubdelasmadresfelices.com/derechos-mujer-maternidad/) tienes el post completo.