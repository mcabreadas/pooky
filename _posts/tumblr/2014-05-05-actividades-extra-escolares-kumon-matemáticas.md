---
layout: post
title: "Actividades extra escolares: Kumon, matemáticas milagro"
date: 2014-05-05T08:00:13+02:00
image: /tumblr_files/tumblr_inline_p6m7w3WlOx1qfhdaz_540.jpg
author: .
tags:
  - educacion
  - 3-a-6
  - 6-a-16
tumblr_url: https://madrescabreadas.com/post/84802888823/actividades-extra-escolares-kumon-matemáticas
---
¿Habéis oído hablar del método de aprendizaje [Kumon](http://www.kumon.es/)? Seguro que habéis oído opiniones y experiencias de algunos padres. Yo no sabía nada de él hasta hace un año más o menos, que de repente empecé a saber cada vez de más niños que iban a una clases especiales para aprender cálculo.

Pero no despertó del todo mi curiosidad, hasta el punto de ponerme a investigar en internet y directamente con padres y niños que lo están practicando hasta que fuimos de viaje con unos amigos y sus hijos no dejaron ni un solo día de hacer sus cuadrenillos de ejercicios Kumon.

Con cada niño se parte de los conocimientos que ya domina a la perfección, una base segura donde se siente cómodo, y a partir de ahí se le va introduciendo muy poco a poco conceptos nuevos que él solo irá descubriendo y aprendiendo por sí mismo.

Esto le fomentará la autoestima y le proporcionará seguridad, también porque los fallos que cometa tendrán que ser resueltos por el propio alumno tras señalarlos los padres.

El papel de los padres se ciñe a ayudarles a coger el hábito de sentarse a hacer los ejercicios todos los días del año, ya se fiesta o vacaciones, y en señalar los errores para que el niño tenga la oportunidad de rectificar por él mismo. De este modo se pretende evitar frustraciones ya que los ejercicios siempre quedaran perfectos.

La constancia es fundamental, ya que es indispensable dedicar un rato al día, cada día sin excepción, más dos clases a la semana en un centro Kumon.\
El método se materializa en unos cuadernillos  con varias hojas a realizar uno al día. En algunos es necesario escribir la hora de inicio del cuaderno y la hora de finalización.

 ![image](/tumblr_files/tumblr_inline_p6m7w3WlOx1qfhdaz_540.jpg)

Mi motivación real para profundizar en este sistema de aprendizaje japonés es descubrir si tal rigidez es beneficiosa para los niños, o si merece la pena llevarlo a rajatabla por las ventajas que aporta a los niños.

Para ello he hablado también con **José Ángel Murcia, del blog Tocamates** del que soy fan desde que lo creó, y os lo he recomendado en varias ocasiones en las redes sociales. Es profesor de secundaria y experto en matemáticas creativas especiales para motivar en los niños el gusto por aprenderlas y descubrir su lado divertido. Ganó el premio Bitácoras al mejor blog de educación en 2013, con eso os lo digo todo.

 ![image](/tumblr_files/tumblr_inline_p6m7w4oFI91qfhdaz_540.jpg)

## **La opinión del experto Jose Angel Murcia sobre Kumon**

“El método Kumon es un sistema de práctica y aprendizaje que se centra en dos áreas que preocupan -¿con razón?- a gran parte de padres y educadores: las matemáticas y la lectura. Me centraré en las primeras por ser de lo que tengo más conocimiento, aunque también diré algo de la lectura.

Los centros Kumon -generalmente franquicias a pie de calle- disponen de un espacio común dotado con 30 o 40 mesas escolares, a las que los estudiantes acceden libremente (sin cita previa) en una o dos sesiones semanales de menos de 30 minutos, cogen la ficha por la que van y se ponen a ello. Un monitor guarda el orden y valora periódicamente el trabajo de los niños. Cuando lo consideran les pasa un breve control de nivel para comprobar si pueden pasar al siguiente escalón.

El trabajo fundamental es sobre operaciones aritméticas, muy repetitivas. También hay fichas en papel para grafomotricidad, y conteo y otras. Una ficha puede consistir en 20 operaciones en dos columnas, muy semejantes 2+4, 3+4, 5+4, 4+4… (en un nivel básico) que el alumno responde en el mínimo de tiempo (buscando el máximo de aciertos). En casa tienen que realizar fichas análogas todos los días, en periodos muy cortos (la brevedad del trabajo diario parece ser clave a la hora de promocionar estos métodos: Smartick, Aloha, Khan academy… también prometen mejoras en el rendimiento de tus hijos a cambio de quince minutos diarios de trabajo y precios variables siempre superiores a los 30€ mensuales).

Como profesor de matemáticas valoro que se potencie el cálculo mental, pero no puedo aceptar que se haga equivalente a “matemáticas”. Es muy positivo que los alumnos practiquen y aprendan (incluso de memoria) las operaciones aritméticas, esto les va a dar confianza, pero las matemáticas son muchísimo más. Son geometría, resolución de problemas, lógica, interpretar y realizar gráficas y conjuntos de datos, identificar patrones… pero sobre todo deben fomentar hacer descubrimientos, crear estructuras (mentales y reales), una visión integradora con otras ciencias y artes. Son belleza y creatividad.

No digo que la escuela cubra todos esos objetivos -en general no lo hace- digo que Kumon ni los intuye. Tampoco el trabajo sobre la grafía (forma) de los números tiene la importancia que se le suele dar -ni en la escuela ni en Kumon-. En la forma del 2 no hay nada que pertenezca al dos, el dos es la cualidad de ser pareja, si se añade uno ya son tres, si se va el otro, me quedo solo. Nada de eso se aprende coloreando doses, ni punzándolos, ni cubriéndolos de gomets.

 ![image](/tumblr_files/tumblr_inline_p6m7w4REh51qfhdaz_540.jpg)

No entiendo la urgencia para que los niños lean cuanto antes, resten cuanto antes o resuelvan raíces cuanto antes, creo que es mucho más importante que desarrollen gusto por las historias y los retos, que gusten de plantearse y resolver situaciones problemáticas cotidianas. Por ejemplo, ¿Cuánto suman los números de esa matrícula? ¿Hay alguna manera de conseguir que salga 0? ¿Cuánto será la cuenta del bar? ¿Y lo que llevamos en el carro? ¿Qué sombra dejará esta forma? ¿Qué huella dejará?…

Hace poco aprendí un juego fantástico para niños que están empezando a dividir y mayores. Se llama “El resto cuenta”. Se necesitan tres dados, los lanzas (supongamos 5, 2, y 4) eliges el orden para que la división 52:4, 25:4, 42:5, 24:5… dé el resto mayor posible. Gana el jugador que alcance 25 en su cuenta de restos. Es un juego que implica hacer 20 o 30 divisiones como mínimo en una partida y para el que los jugadores demandan papel y lápiz para hacerlas sin percatarse de nuestra intención (¡que las hagan! Y que descubran sus propiedades). Es la posición más lejana que encuentro a una ficha con 20 divisiones para hacer una detrás de la otra.

 ![image](/tumblr_files/tumblr_inline_p6m7w4So9e1qfhdaz_540.jpg)

No conozco a ningún niño que haya estado mucho tiempo en Kumon, lo normal es que vayan unos meses o un par de cursos y acabe dejándolo cuando surja alguna necesidad más acuciante. Creo que el trabajo sistemático es necesario en matemáticas y que el cálculo mental se apoya necesariamente en realizar muchas operaciones y si no el niños no memoriza al final las tablas no podrá desenvolverse con soltura en operaciones “de cabeza”.

Mi opinión personal sobre la “dieta Kumon” -que hago extensible a la mayor parte de métodos sistemáticos- es que les ocurre como a las dietas “milagro”, si dejas de comer entre horas y haces deporte, pierdes peso, aunque comas solo cosas que sean de color naranja, solo proteina o que no den sombra, pierdes peso, si te pones en serio (y tienes kilos de más) puedes perder mucho peso. Si pones a tu hijo todos los días a hacer matemáticas gana destrezas, ya sea con un ábaco, con una ficha, o con una tablet, pero ¿qué ocurre con el efecto rebote?”

Le agradezco a José Ángel el esfuerzo que ha hecho para darnos su opinión de una forma tan fundamentada y el tiempo que nos ha regalado

¿Tenéis experiencia en el método Kumon? ¿Cómo les ha ido a vuestros hijos?

Fuentes: 

[kumon](http://www.kumon.es)

[noticiariomatematico](http://noticiariomatematico.blogspot.com.es/)

Fotos:

noticiariomatematico

Guiainfantil.com

Tocamates