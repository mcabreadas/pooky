---
date: '2016-04-21T10:00:28+02:00'
image: /tumblr_files/tumblr_inline_o5o76eGPSz1qfhdaz_540.jpg
layout: post
tags:
- trucos
- regalos
- colaboraciones
- ad
- recomendaciones
- familia
- moda
title: Regala moda para el día de la madre con C&A
tumblr_url: https://madrescabreadas.com/post/143155815255/regalo-madre-cya
---

¿Ya tienes el regalo para el día de la madre? ¿Ya has elegido el tuyo?

Este año me he adelantado y me he aventurado a hacer una wish list por si alguien me quiere regalar algo y no se le ocurre con qué acertar, que por mi parte no quede, que yo lo pongo fácil.

No es que quiera coartar la imaginación de nadie, dios me libre, pero he pensado que quizá unas sugerencias facilitarían la labor del regalador o regaladores y, de paso, actualizo mi armario, que está más obsoleto que Los Pecos.

Tampoco quiero que se gasten un dineral, pero me hace ilusión estrenar algún conjunto primaveral ese día.

Lo que más me ha gustado siempre son los estilos étnicos y hippie, y esta temporada se llevan un montón. ¡Así que estoy de suerte!

Me he metido en la [Shop online de C&A](http://www.c-and-a.com/es/es/shop/mujer), y me he compuesto unos looks que me encantan, y que comparto con vosotras por si no tenéis tiempo de andar buscando cosas, y os apetece echarles un vistazo (se aceptan más sugerencias).

## Look étnico

Hija, a mí todo lo que sena flecos, estampados étnicos, y todo lo relacionado con los indios me vuelve loca desde mi más tierna juventud. Así que se me fueron los ojos directamente a este vestido, y a los botines, que me chiflan.

 ![](/tumblr_files/tumblr_inline_o5o76eGPSz1qfhdaz_540.jpg)

## Look hippie

Las faldas largas son mi debilidad, y ahora han vuelto a ponerse de moda, pero he pasado muchos años que me costaba horrores encontrarlas y, la verdad, las echaba de menos.

Me gustan con sandalia plana, no con tacón, y bien combinadas con un top a juego.

 ![](/tumblr_files/tumblr_inline_o5o76dL7P61qfhdaz_540.jpg)

Para este look me encantan estos complementos, que dan un toque hippie muy chic.

 ![](/tumblr_files/tumblr_inline_o5ilhnSGMw1qfhdaz_540.jpg)
## Look picnic

Imagino una tarde primaveral en el campo, extender un mantel de cuadros sobre la hierba, colocar una cesta con la merienda, y mi madre y yo solas compartiendo confidencias sin interrupciones ni llamadas, ni niños gritando y peleando.

Así que, como soñar es gratis, he elegido este vestido tan florido para la ocasión si la hubiere.

La visera es fundamental para el sol, y las cuñas son súper cómodas.

 ![](/tumblr_files/tumblr_inline_o5o76aYKoi1qfhdaz_540.jpg)

## Look clásico

Y, como tengo algún evento que otro próximamente, he aprovechado para sugerir en mi wish list del día de la madre este vestido blanco, color que me parece ideal para primavera verano, con el que es fácil acertar, a no ser que vayas a una boda, claro está, con zapatos y bolso a juego en nude.

Creo que queda súper elegante.

 ![](/tumblr_files/tumblr_inline_o5o768186v1qfhdaz_540.jpg)

## Para dormir y hacer deporte

Y, una que es muy práctica, también ha incluído estos pantalones de pijama de algodón fresquitos que parecen hechos a retales, com empuñadura en el tobillo para que no se me suban cuando duermo (una que es maniática), y ropa para hacer deporte, que hace que no la renuevo desde que iba a la facultad porque es lo típico que vas dejando porque siempre apetece comprar algo más de vestir, y llega un día en que te das cuenta de que vas demasiado “vintage” a hacer yoga.

 ![](/tumblr_files/tumblr_inline_o5o768eA6E1qfhdaz_540.jpg)

Todo esto lo podéis encontrar en la [tienda online de C&A](http://www.c-and-a.com/es/es/shop/mujer), y a precios más que asequibles por si quieres sugerir a alguien a modo de indirecta directa lo que te gustaría que te regalasen, o por si quieres hacer un regalo a tu mamá en el día de la madre.

¿Te mola mi lista de deseos? (soñar es gratis, y desear, también)

_Si te ha gustado, compártelo, no te cuesta nada :)_