---
date: '2017-04-10T20:47:42+02:00'
image: /tumblr_files/tumblr_inline_ontxwoPM1J1qfhdaz_540.jpg
layout: post
tags:
- puericultura
- crianza
- colaboraciones
- ad
title: 'Prueba extrema Bébécar IV: suelos empedrados'
tumblr_url: https://madrescabreadas.com/post/159422434464/ipop-bebecar-amortiguadores
---

Llega la Semana Santa, y con ella las [procesiones](/2016/03/25/procesion-salzillo-murcia.html) por los cascos antiguos de las ciudades con sus temibles adoquines.

Quizá pienses que me equivoco con el adjetivo que utilizo, y tienes razón, no son temibles, son peor que temibles.

 ![](/tumblr_files/tumblr_inline_ontxwoPM1J1qfhdaz_540.jpg)

Si no entiendes de qué hablo, entonces es que no has pasado nunca con tu cochecito de bebé por una de estas calles empedradas que te digo porque si lo hubieras hecho lo recordarías seguro: los primeros metros piensas que no es para tanto, y que puedes con eso y con más, pero cuando los brazos empiezan a dormírsete por la vibración del carro, las ruedas se atascan, el bebé empieza a pegar botes en cuanto intentas ir más rápido, y el carro parece que se va a _descuajeringar_ en cualquier momento ante tales meneos, empiezas a arrepentirte de haber salido de casa.

De este temor fundado que experimentamos todos los padres cuando acometemos un camino de estas características proviene esta IV prueba extrema,que se encuadra dentro de la serie de pruebas extremas a la que hemos estado sometiendo a nuestro cochecito [Ip Op Bébécar](/2016/03/09/coleccion-primavera-bebecar.html) durante todo este tiempo de testing (y aún sigue vivo). A saber:

- [Prueba extrema Bébécar I: Aglomeraciones](/2016/04/28/coche-bebes-bebecar.html)  

- [Prueba extrema Bébécar II: Playa, calor y arena](/2016/08/26/cochecito-bebecar-playa.html)  

- [Pruebe extrema Bébécar III: Cuestas empinadas](/2016/09/18/carrito-bebe-resistente-bebecar.html)  

## Nos vamos de procesiones

No sé si será tu caso, pero en muchas ciudades los cascos antiguos, sobre todo si son históricos (que son preciosos, que no digo yo que no) están empedrados o con suelo de adoquines que, si bien dan un aspecto antiguo y de época a las ciudades, son bastante incómodos porque te los clavas en los pies, se te tuercen los tacones, y hacen imposible para con cualquier tipo de carro.

 ![](/tumblr_files/tumblr_inline_ontzqhTxxr1qfhdaz_540.png)

Pero como a nosotros nos gustan los retos, you know, nos hemos propuesto meter nuestro [Bébécar](http://www.bebecar.com/bebecar/es/) por todo el centro, y ver si supera la prueba.

## Amortiguación en las ruedas

Comenzamos nuestra andadura y de repente nos acordamos del típico detalle que cuando te lo explican en la tienda de cochecitos no le das demasiada importancia, pero que en ese momento comprendes perfectamente a qué se refería el vendedor cuando hablaba de la amortiguación en las ruedas, y das gracias a todos los Santos de la procesión por haberle hecho caso.

 ![](/tumblr_files/tumblr_inline_o39zd5rSRd1qfhdaz_500.gif)

Con otro tipo de carro hubiéramos sufrido bastante, pero la verdad es que este sistema hace que la vibración se note mucho menos, y evita los movimientos bruscos y atascos de ruedas.

La suspensión trasera regulable presionando o no los bloques que ves junto a las ruedas según el peso del bebé, también es un detalle que contribuyó al éxito de nuestra misión.

 ![](/tumblr_files/tumblr_inline_ontxlydgAA1qfhdaz_540.png)
## Un truco

También descubrimos que si bloqueamos las ruedas delanteras para que no puedan girar evitamos que se queden atascadas entre piedra y piedra, lo que nos convirtió en auténticas máquinas de avanzar sobre adoquines.

## Conclusión

Bébécar vuelve a superar la prueba, esta vez en uno de los terrenos más hostiles en os que podemos meter un carrito de bebé gracias a su sistema de amortiguación en las ruedas, uno de los detalles que lo diferencian de otros cochecitos, y por lo que vale la pena recomendarlo.