---
date: '2018-02-22T09:00:05+01:00'
image: /tumblr_files/tumblr_inline_p4cypmd6KT1qfhdaz_540.png
layout: post
tags:
- moda
- familia
title: Los colores de la primavera 2018 en la moda infantil
tumblr_url: https://madrescabreadas.com/post/171157044097/colores-primavera-2018
---

Sé que todavía hace frío, pero me he propuesto firmemente que no me pille el toro como otros años a la hora de hacer el cambio de armario para esta primavera.

No sé en tu ciudad, pero en la mía el calor entra de golpe, y es entonces cuando se nos ve a las madres correr a las tiendas a comprar urgentemente ropa de verano porque, obviamente, de la del año pasado no les sirve nada o casi nada.

Mi consejo es que saquemos la ropa de la primavera pasada para comprobar si hay algo que se puede aprovechar o dar en herencia a algún hermano, y después hacer una lista con las prendas que van a necesitar. Así evitaremos comprar a lo loco cuando llegue el momento porque tendremos nuestra lista en la mano.

De todas maneras, yo no guardo las medias y chaquetas hasta  bien entrado el calor porque siempre las puedes necesitar para algún vestido en un día fresco.

Me encanta indagar sobre los colores que se van a llevar en primavera. Ya publiqué un post sobre las tendencias en moda infantil para la primavera 2017, y tuvo una gran acogida, así que me lanzo de nuevo basándome en las propuestas que vimos en la Feria Internacional de Moda Infantil y en las pasarelas de adultos.

## El amarillo es la estrella

Ha inundado pasarelas de adultos, pero también es un color ideal para nuestros peques porque es súper alegre, además, en ellos se presta más para hacer combinaciones atrevidas con otros tonos vivos. ¿Te atreverías con unos [leotardos verdes para niña](https://www.condor.es/tienda/es/leotardos) con este vestido?

 ![](/tumblr_files/tumblr_inline_p4cypmd6KT1qfhdaz_540.png)

Si no quieres arriesgarte puedes combinarlo con negro, azul, malva (también de súper tendencia esta primavera), gris…

## El rosa y azul pastel

Los tonos suaves han sido los protagonistas de las pasarelas infantiles, me gustan con una prenda encima que le dé vida al look, como por ejemplo un [bolero para niña de colores](https://www.condor.es/tienda/es/chaquetas) vivos u otra prenda que aporte personalidad.

 ![](/tumblr_files/tumblr_inline_p4cyqosmQi1qfhdaz_540.png)
## El rojo

Creo que no hay verano sin rojo. Además, combinado con blanco o con azul marino nos da un toque marinero.

O si lo prefieres puedes arriesgarte con un amarillo, un tierra o beige…

También queda ideal en total look.

 ![](/tumblr_files/tumblr_inline_p4cyurnAAf1qfhdaz_540.png)
## El ultraviolet

En adultos lo vamos a ver en mil prendas, pero en niños yo apostaría por llevarlo en complementos, que sin duda darán gran personalidad y sofisticación al look.

## El fucsia repite

Es un color muy veraniego y muy vivo, por lo que este año vuelve a llevarse un montón.

 ![](/tumblr_files/tumblr_inline_p4cyt1ETmw1qfhdaz_540.png)
## Verde esmeralda

Me encanta este tono, y con la piel morenita de nuestros niños les va a quedar genial!

 ![](/tumblr_files/tumblr_inline_p4cytyE5hU1qfhdaz_540.png)

Espero haberte orientado para completar el armario de primavera verano de tus peques. Y, recuerda: No te duermas en los laureles!

_Fotos: Marcos Soria_