---
date: '2015-12-01T10:00:14+01:00'
image: /tumblr_files/tumblr_inline_o39yzrUHPG1qfhdaz_540.jpg
layout: post
tags:
- navidad
- trucos
- decoracion
- planninos
- familia
- diy
title: Manualidades con niños. Dos arbolitos de Navidad diferentes.
tumblr_url: https://madrescabreadas.com/post/134323160095/manualidades-ninos-navidad-westwing
---

Las vacaciones de Navidad se aproximan y los niños van a pasar mucho tiempo en casa. Así que te voy a dar dos ideas facilísimas para hacer manualidades en familia que, además, van a servir de decoración navideña para el salón.

Se trata de dos arbolitos muy originales que podrán hacer tus pequeños con un poco de ayuda y mucha imaginación. 

## Arbolito de Navidad goloso

Esta idea nos la sugiere Veva, del blog [My White Idea Diy](http://www.mywhiteideadiy.com.es)

 ![](/tumblr_files/tumblr_inline_o39yzrUHPG1qfhdaz_540.jpg)

**Necesitas** :

- Una piña que esté abierta y que sea grandecita.  
- Pintura en spray del color que os guste, no tiene por qué ser verde. Por ejemplo, en dorado quedaría también muy chulo.  
- Un bote de cristal con su tapa. Os puede servir cualquiera que vayáis a desechar de conservas.  
- Esmaltes de colores. Podéis aprovechar esos que os comprásteis en verano de colores vivos, que ahora no pegan.  
- Un trozo de cordel.  
- Una pistola de silicona o un bote de pegamento de silicona.  
- Las chuches favoritas de vuestros hijos.  

 ![](/tumblr_files/tumblr_inline_o39yzr4WbR1qfhdaz_540.jpg)

**Cómo se hace** :

Pinta la piña con el spray del color que prefieras. No olvides proteger la mesa y usar guantes.  
Pon bastante silicona en el centro de la tapa por la parte de arriba.  
Coloca la piña encima y espera unos minutos hasta que la silicona se seque y la piña se sostenga por si sola.  
Pon puntitos de colores con los esmaltes de uñas simulando las bolas del árbol.  
Si tienes esmalte de uñas blanco, haz pequeños puntitos para simular la nieve.  
Coloca el trozo de cordel alrededor de la base de la piña de modo que tapes la silicona. Puedes hacer un lazo.

¿A que es fácil?

## Arbolito de Navidad con papel crepé

Esta idea es de Ire, del blog [El taller de Ire](http://www.eltallerdeire.com).

 ![](/tumblr_files/tumblr_inline_o39yzsiiZy1qfhdaz_540.jpg)

**Necesitas** :

- Un macetero pequeño.   
- Plastilina para la base y para la estrella.  
- Una brocheta  
- Papel crepé (el papel pinocho de toda la vida, vamos) verde oscuro y verde más claro. Si quieres hacerlo de otros colores, no te cortes y sé creativa.  
- Pompones de colores  
- Cola blanca
 ![](/tumblr_files/tumblr_inline_o39yzsMVdT1qfhdaz_540.jpg)

**Cómo se hace**

- Haz una bola de plastilina para y rellena el macetero que hayas elegido.  
- Pincha la brocheta brocheta en la plastilina con el pico hacia arriba. (Aquí vigila para que no se lo claven)  
- Haz bolas con el papel crepé de tamaño según lo grande que quieras hacer el árbol.  
- Pincha algunas bolas de papel en la brocheta hasta dejar fuera sólo la punta , y después ve pegando el resto de las bolas alrededor de las centrales con ayuda de la cola blanca. Pega más por la parte de abajo, y menos por la de arriba progresivamente para darle la forma triangular del árbol de Navidad.  
- Pega los pompones de colores repartiéndolos por todo el árbol a tu gusto con la cola.  
- Corta un trocito de plastilina amarilla en forma de estrella, y pínchala en la punta de la brocheta que ha quedado libre.  

Éste es un poco más laborioso que el anterior, pero no es nada complicado, además, podrán participar los niños más chicos porque les encanta hacer bolas de papel, y es algo muy fácil para ellos.

Tienes más ideas sobre decoración navideña, diferentes formas de envolver regalos, de poner la mesa de Nochebuena, y de adornar el árbol de [Navidad en Westwing](https://www.westwing.es/una-navidad-creativa/).

 ![](/tumblr_files/tumblr_inline_o39yztIDQ91qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yzts1FR1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yztmybs1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yzuDkHo1qfhdaz_540.jpg)

¿No te han entrado unas ganas locas de que llegue la Navidad?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.