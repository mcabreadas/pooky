---
date: '2013-10-14T23:08:00+02:00'
image: /tumblr_files/tumblr_inline_muofrf9HJF1qfhdaz.jpg
layout: post
tags:
- recetas
- trucos
- familia
title: Remedio casero para el resfriado. El Ponche del Abuelito Tomás.
tumblr_url: https://madrescabreadas.com/post/64051476482/remedio-casero-para-el-resfriado-el-ponche-del
---

Estrenamos sección dedicada a la abuela de mis fieras y a sus experiencias con ellos, con la vida, trucos, remedios caseros… Empezamos con uno que alivia el resfriado:

“Cuando mi padre estaba con nosotras, cada vez que me acatarraba, había algo que me encantaba: su ponche. Me lo llevaba a la cama y me decía: “bébetelo despacio, que quema, y luego, aunque sudes, no te destapes, ya verás como mańana estás mejor y puedes ir al cole .

 ![image](/tumblr_files/tumblr_inline_muofrf9HJF1qfhdaz.jpg)

Voy a compartir con vosotros su receta.

**Ponche del abuelito Tomás**

En un cazo, se caramelizan tres cucharadas de azúcar

Ańadimos un chorro de brandi y se flambea

Se vierte en el cazo medio vaso de leche.

Se remueve hasta que el azúcar quede disuelta.

Lo echamos en una taza y a disfrutarlo.

Que os mejoréis!”

Conoces algún otro remedio para el resfriado?

<small>Foto gracias a static.nanopress.es</small>