---
date: '2016-04-28T10:40:15+02:00'
image: /tumblr_files/tumblr_inline_o6aj4f6Axp1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- bebe
- puericultura
- ad
- testing
- recomendaciones
- familia
- bebes
title: 'Coches para bebés Bébécar. Prueba extrema I: Aglomeraciones'
tumblr_url: https://madrescabreadas.com/post/143523970149/coche-bebes-bebecar
---

Ya llevamos cuatro meses de prueba del cochecito de bebé Ip Op XL Classic de [Bébécar](http://www.bebecar.com/bebecar/es/). Podéis seguir nuestra experiencia [aquí](/2015/12/28/cochecito-bebe-bebecar.html), [aquí](/2016/02/15/montajecarritobebecar.html) y [aquí](/2016/03/09/coleccion-primavera-bebecar.html). 

El carrito va fenomenal, como ya te comenté, es manejable, confortable, seguro y demás. Pero yo no me conformo con que cumpla los estándares de calidad normales, no. Yo quiero meterle caña, y ver qué tal responde. Para ello me he inventado una serie de retos basados en situaciones extremas que una madre tiene que vivir con su carro de bebé a diario.

El primero que quiero compartir contigo es cómo se maneja en aglomeraciones de gente. Me dirás que es una locura que unos padres se metan en un mogollón de personas con un carricoche, y te daré la razón. Pero hay veces que es inevitable, o que sin darte cuenta te encuentras en esa situación en hora punta en un centro comercial, por ejemplo.

Esto es lo que nos pasa a nosotros cada año cuando llega el día grande de nuestra región, el Bando de la Huerta. Quien conoce Murcia lo puede asegurar. Si tienes bebé no te libras de los atascos de gente por las calles del centro de la ciudad con la silla o carrito.

Por eso, he elegido este día para realizar nuestra primera prueba extrema con mi sobrina Chiquita, quien lo pasó en grande, todo hay que decirlo.

## ¿Qué tememos las madres cuando nos metemos en una aglomeración con el carrito?

-Que le den un golpe

-Que lo muevan o zarandeen bruscamente

-Que se caiga alguien encima

-Que se atranque y cueste salir con facilidad del sitio

-Que cueste sortear obstáculos

Veamos cómo reaccionó nuestro cochecito Bébécar a la prueba:

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Yavt9YTwgP0" frameborder="0" allowfullscreen></iframe>

Gracias a su **robustez** , al estar fabricado en Europa con materiales de primera calidad, os puedo asegurar que la impresión que tuvimos toda la familia fue que Chiquita iba “blindada”. Le pusimos la capota, y quedó casi como un armadillo. 

 ![](/tumblr_files/tumblr_inline_o6aj4f6Axp1qfhdaz_540.jpg)

La sensación de **protección** que te da esta marca, yo no la he tenido con ninguna otra que haya probado.

Por otra parte, la **estabilidad**  y **solidez** del carro nos garantizó que, a pesar de los choques con la gente, Chiquita no iba a sufrir movimientos bruscos que la perturbaran en su descanso. 

Su **manejabilidad** es excelente, así que sortear obstáculos fue sencillo, y no perdimos en ningún momento el control del carro, pudiendo maniobrar con él sin problemas y hacer giros en poco espacio gracias a su sistema de **amortiguación** , sus **ruedas grandes y anchas** , y su **buen agarre**  **en el manillar** que, además, es **regulable** en cuatro posiciones.

Hay que destacar la facilidad con la que se logra levantar la parte de delante, dejándolo sobre las ruedas traseras (hacer el caballete, o la cabra, en mi tierra), evitando que tengamos que ejercer mucha fuerza con los brazos, lo cual es de agradecer. 

 ![](/tumblr_files/tumblr_inline_o6aix6bAvn1qfhdaz_540.jpg)

Su **acolchado**  **interior** es muy mullido para mayor protección del bebé, caso de algún movimiento brusco inesperado.

 ![](/tumblr_files/tumblr_inline_o6aj19b8bQ1qfhdaz_540.jpg)
## Conclusión

El cochecito Bébécar supera con nota la primera prueba extrema gracias a su:

- Seguridad  

- Robustez  

- Estabilidad  

- Manejabilidad  

- Amortiguación  

- Ruedas grandes  

- Buen agarre y posición del manillar  

- Súper acolchado interior  

Espero que os haya gustado esta prueba, y no os perdáis las siguientes, que serán más extremas todavía.

 

Ah, y si se te ocurre alguna idea que te apetezca que probemos, déjamelo en los comentarios, por favor.