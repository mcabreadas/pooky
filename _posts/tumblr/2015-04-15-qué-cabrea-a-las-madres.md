---
date: '2015-04-15T10:00:33+02:00'
image: /tumblr_files/tumblr_inline_nmt83wUWmn1qfhdaz_540.png
layout: post
tags:
- mecabrea
title: Qué cabrea a las madres
tumblr_url: https://madrescabreadas.com/post/116453788791/qué-cabrea-a-las-madres
---

Si de algo sé es de las cosas que suelen cabrear más a las madres porque a diario compartimos cabreos en el [Club de las Madres Cabreadas de Facebook](https://www.facebook.com/groups/clubmadrescabreadas/), y en Twitter con el hastag [#MeCabrea](https://twitter.com/search?src=typd&q=%23mecabrea) desde hace ya varios años.

Es curioso como existen cabreos típicos que se repiten una y otra vez, y que al leerlos muchas respondemos: “podría haberlo escrito yo”. A veces pienso que es increíble que otra madre desde la otra punta del mundo, y que ni siquiera conoces, esté cabreada justo por lo mismo que tú. Pero lo realmente increíble es que cuando se enfurece, en vez de ponerse a gritar, o pagarlo con quien tiene más cerca, abre el Facebook o el [Twitter y escribe un #MeCabrea para desahogarse. ](/2014/04/13/keepcalm-madrescabreadas.html)

 ![image](/tumblr_files/tumblr_inline_nmt83wUWmn1qfhdaz_540.png)

Pero lo que de verdad de verdad es impresionante es que normalmente, a los pocos minutos, tenga ya varias respuestas de empatía de otras madres. 

Los cabreos más frecuentes que sufren las madres suelen ser provocados por falta de colaboración o implicación de la pareja en tareas domésticas o con los niños, roces con la familia política, críticas a su manera de criar por parte de su entorno, opiniones y consejos no pedidos, conflictos en el colegio, exceso de deberes…

La falta de empatía que encuentran algunas madres en su trabajo, el machismo que aveces sufren, y la falta de medidas reales de conciliación laboral y familiar suele ser otra fuente frecuente de cabreos, sobre todo cuando les toca dejar a sus hijos enfermitos en casa, o no pueden asistir a tutorías, funciones de Navidad o de fin de curso. 

La falta de dinero, la crisis, la falta de empleo, la discriminación en las entrevistas de trabajo por ser madre se están llevando la palma últimamente. Puedo afirmar que si la crisis está afectando a todos, a las mujeres madres, mucho más.

Las cosas triviales, como estar enferma del estómago y que hagan bizcocho de chocolate en casa ,las ojeras crónicas, un corte de pelo mal hecho en la pelu, o encontrarse el sofá “decorado” con rotulador por el pequeño de la casa suelen acabar en risas y en algún que otro desvarío.

El tráfico, los vecinos ruidosos, las compañías telefónicas y las llamadas intempestivas de algunos comerciales qu terminan despertando a los niños también protagonizan bastantes cabreos, y de los gordos!

 ![image](/tumblr_files/tumblr_inline_nmt88qgESw1qfhdaz_540.jpg)

Pero también hay algunas confesiones muy crudas de situaciones personales durísimas que nos hacen llorar y volcarnos con la madre angustiada, a la que normalmente logramos levantar un poquito el ánimo.

Las carencias de la sanidad pública por las que a veces se ven afectados nuestros hijos, los partos no respetados, la falta de apoyo a la lactancia materna en algunos hospitales y por algunos pediatras y enfermeras de atención primaria protagonizan más #mecabrea de los que serían deseables.

En general los #mecabrea con trasfondo de culpa por no llegar a todo y por sentirnos juzgadas por nuestro entorno llenan el muro del [Club de las Madres Cabreadas](https://www.facebook.com/groups/clubmadrescabreadas/)a diario.

Y todavía la gente me sigue preguntando el por qué del nombre de mi blog, y de mi avatar…

 ![image](/tumblr_files/tumblr_inline_nmt6khdZ9l1qfhdaz_540.jpg)

¿Y a ti, qué te cabrea?