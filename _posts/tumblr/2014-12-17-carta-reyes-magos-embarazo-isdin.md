---
date: '2014-12-17T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_p98q77YN6u1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- postparto
- premios
- recomendaciones
- embarazo
title: 'Concurso Tu carta a los Reyes Magos: Kit embarazo y postparto Isdin'
tumblr_url: https://madrescabreadas.com/post/105422529023/carta-reyes-magos-embarazo-isdin
---

ACTUALIZACIÓN 28-12-14

Con el fin de que los regalos lleguen a su destino a tiempo para Reyes  **se cerrará el concurso el 29-12-14 a las 8:00 am.**

Los cinco participantes cuyas cartas hayan obtenido más comentarios de personas diferentes tendrán que mandarme un email a  **mcabreada@gmail.com**  antes de las 12:00 am del día 29 con:

-su nombre y apellidos, dirección y teléfono y email para el envío

Si algún participante no manda el email con su lista antes del 29-12-14 a las 12:00 el premio pasará al concursante cuya carta sea la siguiente en número de comentarios. Todo ello con la finalidad de que el envío de los regalos se haga lo antes posible.

ACTUALIZACIÓN 27-12-14

**SE ADELANTA LA FECHA TOPE PARA EL CONCURSO AL 29 -12**  
Me han propuesto adelantar la fecha de fin del concurso “Tu carta a los Reyes Magos” para que dé tiempo a que lleguen los regalos para esa noche tan especial, y creo que es muy buena idea. Así que se adelanta la fecha tope para subir vuestras cartas al lunes 29 de diciembre a las 00:00 h.

Vamos! Corre, corre!!

_Post originario:_

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a haceros felices estas Fiestas.

Uno de los regalos que podéis incluír en vuestra cata a los Reyes Magos para participar en el concurso es este kit de embarazo y postparto que nos cede Isdín, que no ha querido perder la oportunidad de mimar a las lectoras de mi blog que están embarazadas.

Por eso también, Isdin os ofrece un [**menú compensado**](http://www.disfrutatuembarazo.com/navidad), elaborado por la  **nutricionistas Lucía Bultó** , que os ayudará que podáis disfrutar de las fiestas sin que vuestro peso se vea afectado.

La  **alimentación**  de la  **embarazada**  es, en términos generales, muy parecida a la de la mujer no gestante. Sí es cierto que hay una serie de parámetros que debes cuidar con especial mimo, como la ingesta de productos lácteos -para mantener la salud de tus huesos y la formación del nuevo ser-, la hidratación y la toma de fibra, vitaminas y minerales.

El  **em**** barazo ** es una época muy motivadora para la mujer. Es cuando más se cuida la ** alimentación**, la piel y cuando con más facilidad se evitan el tabaco, el alcohol y los excesos en general. Pero las fiestas navideñas, con días llenos de extras, te van a plantear más de un dilema porque, aunque estés muy concienciada, vas a tener que enfrentarte a constantes tentaciones.

En este enlace os dejo un análisis de diferentes menús y platos festivos para que veais de qué manera podéis manejarlos durante la  **Navidad**  para que la  **alimentación**  y, a través de ella vuestro peso y salud, no se resientan.

Además Lucía Bultó os propone un ejemplo de menú para Nochebuena, Navidad, Nochevieja, Año Nuevo y Reyes.

Qué incluye el regalo de Isdin?

Velastisa Antiestrías potenciador de la elasticidad

Velastisa Antiestrías reafirmante posparto

Velastisa Antiestrías cuidado del pezón

Velastisa Íntim Gel Higiene íntima

Velastisa Intim VV (alivio de la sequedad vulvar)

Velastisa Lubricante Íntim Hidrogel

 ![image](/tumblr_files/tumblr_inline_p98q77YN6u1qfhdaz_540.jpg)

Para ganar este regalo tienes que participar en el concurso[ “Tu carta a Los Reyes Magos”](/portada-regalos-reyes-magos) escribiendo tu carta pinchando [aquí](/portada-regalos-reyes-magos).

Las condiciones para participar están en el [post explicativo del concurso.](/portada-regalos-reyes-magos)

Tenéis hasta el 5 de enero!