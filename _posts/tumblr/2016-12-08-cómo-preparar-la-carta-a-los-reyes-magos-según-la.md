---
date: '2016-12-08T19:20:56+01:00'
image: /tumblr_files/tumblr_inline_ohvpmsV5ZU1qfhdaz_540.png
layout: post
tags:
- navidad
- regalos
- juguetes
title: Cómo preparar la carta a los Reyes Magos según la edad
tumblr_url: https://madrescabreadas.com/post/154212389239/cómo-preparar-la-carta-a-los-reyes-magos-según-la
---

**De 0 a 3 meses**

Como es muy complejo en la sociedad en la que vivimos hoy en día decir a los familiares y amigos que no regalen nada al peque en los [Reyes Magos o Papá Noel](https://madrescabreadas.com/2021/12/09/reyes-magos-o-papa-noel/). Lo ideal sería regalos que aporten seguridad como peluches o muñecos blancos sin pelo.

[![](/images/uploads/nube-relajante.jpg)](https://www.amazon.es/Fisher-Price-Relajante-Juguete-Mattel-surtido/dp/B07WPBVZH3/ref=sr_1_28?keywords=juguetes+0-3+meses+bebe&qid=1641830096&sprefix=jueguete+0%2Caps%2C920&sr=8-28&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Fisher-Price-Relajante-Juguete-Mattel-surtido/dp/B07WPBVZH3/ref=sr_1_28?keywords=juguetes+0-3+meses+bebe&qid=1641830096&sprefix=jueguete+0%2Caps%2C920&sr=8-28&madrescabread-21)

Por otro lado los arcos de actividad ayudan a mejorar su coordinación y fuerza a medida que van ejercitándose desde sus primeros meses de vida.

[![](/images/uploads/tiny-love-musical.jpg)](https://www.amazon.es/Tiny-Love-Arco-Musical-Forest/dp/B077T1Y6F3/ref=sr_1_12?keywords=juguetes%2B0-3%2Bmeses%2Bbebe&qid=1641830096&sprefix=jueguete%2B0%2Caps%2C920&sr=8-12&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Tiny-Love-Arco-Musical-Forest/dp/B077T1Y6F3/ref=sr_1_12?keywords=juguetes%2B0-3%2Bmeses%2Bbebe&qid=1641830096&sprefix=jueguete%2B0%2Caps%2C920&sr=8-12&th=1&madrescabread-21)

**Para bebés de 3 a 6 meses**

En esta etapa ellos van a tratar de moverse, incorporarse o sentarse solos, de identificar claramente los objetos y hasta a algunos comienzan a salirles los dientes, son recomendables los regalos como **mordedores** para ayudar en la dentición, **sonajeros** para potenciar su coordinación, juguetes para estimular la visión y el enfoque visual o comenzar a utilizar los bloques infantiles para forzar los músculos e intentar que los cojan.

[![](/images/uploads/playgro-set-de-juegos-educativos.jpg)](https://www.amazon.es/Playgro-Twist-Chew-Activity-Pack/dp/B013VVZMB6/ref=sr_1_39?crid=1NGLJBRWMM4EC&keywords=mordedores+bebes+3+meses&qid=1641831973&sprefix=mordedores+bebes%2Caps%2C368&sr=8-39&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Playgro-Twist-Chew-Activity-Pack/dp/B013VVZMB6/ref=sr_1_39?crid=1NGLJBRWMM4EC&keywords=mordedores+bebes+3+meses&qid=1641831973&sprefix=mordedores+bebes%2Caps%2C368&sr=8-39&madrescabread-21)

**De los 6 a los 12 meses**

A esta edad ya controlan las extremidades superiores y comienzan a enfrentarse a poder andar y controlar las inferiores. Comienzan nuevos aprendizajes como el gateo.

En esta etapa es muy importante la atención de los padres al bebé ¡éste es el mejor regalo! Aplicar una rutina o reaccionar a los sonidos y balbuceo del bebé. Sin embargo ¿Cómo se van a quedar sin Papa Noel o Reyes?

Aconsejable juguetes para aumentar coordinación, la motricidad y el desarrollo táctil y auditivo (sonajeros) y no es necesario comprar nuevos ya que se pueden **utilizar lo que ya tenía de otra manera** , por ejemplo, los bloques infantiles blanditos.

[![](/images/uploads/vaca-muusical-peluche.jpg)](https://www.amazon.es/VTech-Vaca-Musical-Peluche-3480-166022/dp/B00UZFU294/ref=sr_1_4?keywords=juguetes+de+6+meses+a+1+a%C3%B1o&qid=1641832768&sprefix=juguetes+de+6+meses%2Caps%2C314&sr=8-4&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/VTech-Vaca-Musical-Peluche-3480-166022/dp/B00UZFU294/ref=sr_1_4?keywords=juguetes+de+6+meses+a+1+a%C3%B1o&qid=1641832768&sprefix=juguetes+de+6+meses%2Caps%2C314&sr=8-4&madrescabread-21)

**Entre los 12 y 18 meses: los primeros libros**

Ahora seguramente comiencen a decir palabras (a lo mejor antes), y a dar los primeros pasitos, por lo que es indicado libros con imagines sencillas para ayudar a su desarrollo del lenguaje.


[![](/images/uploads/libro-interactivo.jpg)](https://www.amazon.es/Fisher-Price-Interactivo-Aprendizaje-Juguete-FRC69/dp/B079MG7JT6/ref=sr_1_17?crid=2ZDCDNI58GZXZ&keywords=juguetes%2Bde%2B12%2Ba%2B18%2Bmeses&qid=1641833117&sprefix=juguetes%2Bde%2B12%2Ba%2B18%2Caps%2C460&sr=8-17&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Fisher-Price-Interactivo-Aprendizaje-Juguete-FRC69/dp/B079MG7JT6/ref=sr_1_17?crid=2ZDCDNI58GZXZ&keywords=juguetes%2Bde%2B12%2Ba%2B18%2Bmeses&qid=1641833117&sprefix=juguetes%2Bde%2B12%2Ba%2B18%2Caps%2C460&sr=8-17&th=1&madrescabread-21)

Los **juguetes de arrastrar** son excelentes ya que también potencian el movimiento.

[![](/images/uploads/tito-robotito.jpg)](https://www.amazon.es/Fisher-Price-HCK45-Fisher-Price-Juguetes-Color/dp/B08WKBZQ25/ref=sr_1_55?crid=2ZDCDNI58GZXZ&keywords=juguetes+de+12+a+18+meses&qid=1641833117&sprefix=juguetes+de+12+a+18%2Caps%2C460&sr=8-55&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Fisher-Price-HCK45-Fisher-Price-Juguetes-Color/dp/B08WKBZQ25/ref=sr_1_55?crid=2ZDCDNI58GZXZ&keywords=juguetes+de+12+a+18+meses&qid=1641833117&sprefix=juguetes+de+12+a+18%2Caps%2C460&sr=8-55&madrescabread-21)

**De 18 a 24 meses, mesas de actividades**

Es el momento de mayor actividad para ellos y sería fantástico las mesas de actividades, los Cd’s de música, juguetes para **desarrollar la imaginación y la creatividad** …

[![](/images/uploads/guarderia-maletin.jpg)](https://www.amazon.es/PLAYMOBIL-1-2-3-70399-PLAYMOBIL-Juguete/dp/B085FLFHKP/ref=sr_1_26?keywords=juguetes+ni%C3%B1os+de+18+a+24+meses&qid=1641833569&sprefix=juguetes+de+18+meses+a+24%2Caps%2C572&sr=8-26&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/PLAYMOBIL-1-2-3-70399-PLAYMOBIL-Juguete/dp/B085FLFHKP/ref=sr_1_26?keywords=juguetes+ni%C3%B1os+de+18+a+24+meses&qid=1641833569&sprefix=juguetes+de+18+meses+a+24%2Caps%2C572&sr=8-26&madrescabread-21)

**De 24 a 36 meses podemos comenzar con las manualidades**

En esta etapa suelen jugar a imitar a personas de su alrededor ya que están muy centrados en lo que es la vida cotidiana. Su vocabulario es más amplio pudiendo decir frases enteras, sin embargo, no tienen la capacidad para describir bien los sentimientos y las emociones.

Un buen juguete para esta edad son las **manualidades** : ceras de colores, libros para colorear, etc… ¡así expresarán bien sus emociones!

Lo ideal sería [prepararles una noche de Reyes mágica](https://madrescabreadas.com/2021/01/04/ideas-noche-reyes-magica/), ya que empiezan a ser conscientes y a atesorar recuerdos.

[![](/images/uploads/plastidecor.jpg)](https://www.amazon.es/BIC-Plastidecor-unidades-surtidos-Plastidecor/dp/B07Y1SLTFQ/ref=sr_1_10?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=GRJC48M6HFGN&keywords=juguetes+ni%C3%B1os+de+24+meses+a+36&qid=1641834431&sprefix=juguetes+ni%C3%B1os+de+24+meses+a+36%2Caps%2C938&sr=8-10&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/BIC-Plastidecor-unidades-surtidos-Plastidecor/dp/B07Y1SLTFQ/ref=sr_1_10?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=GRJC48M6HFGN&keywords=juguetes+ni%C3%B1os+de+24+meses+a+36&qid=1641834431&sprefix=juguetes+ni%C3%B1os+de+24+meses+a+36%2Caps%2C938&sr=8-10&madrescabread-21)

También es un buen regalo los **libros de actividades cotidianas** que le expliquen cómo funcionan cosas normales de la vida. ¿De dónde viene la leche? O ¿Cómo llega el agua al grifo?

[![](/images/uploads/sabes-como-funcion.jpg)](https://www.amazon.es/%C2%BFSabes-c%C3%B3mo-funciona-Conocer-Comprender/dp/8426144004/ref=sr_1_43?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3PKUVQS59MHV0&keywords=libros+de+actividades+cotidianas&qid=1641834677&sprefix=libros+de+actividades+cotidianas+%2Caps%2C1306&sr=8-43&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/%C2%BFSabes-c%C3%B3mo-funciona-Conocer-Comprender/dp/8426144004/ref=sr_1_43?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3PKUVQS59MHV0&keywords=libros+de+actividades+cotidianas&qid=1641834677&sprefix=libros+de+actividades+cotidianas+%2Caps%2C1306&sr=8-43&madrescabread-21)

Y no pueden faltar las **herramientas** de plástico como objetos de cocina, carrito de limpieza, etc

[![](/images/uploads/center-taller.jpg)](https://www.amazon.es/Smoby-Decker-Herramientas-Juguete-360701/dp/B00U9KXO0O/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3JDG6N8K04X6W&keywords=juguetes+herramientas&qid=1641835477&sprefix=juguetes+herramienta%2Caps%2C403&sr=8-3&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Smoby-Decker-Herramientas-Juguete-360701/dp/B00U9KXO0O/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3JDG6N8K04X6W&keywords=juguetes+herramientas&qid=1641835477&sprefix=juguetes+herramienta%2Caps%2C403&sr=8-3&madrescabread-21)

[![](/images/uploads/new-classic-toys.jpg)](https://www.amazon.es/New-Classic-Toys-11067-Cocina-11067/dp/B083V7BYSG/ref=sr_1_15?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2D4QJOPZY70I1&keywords=juguetes+cocina&qid=1641835704&sprefix=juguetes+c%2Caps%2C1091&sr=8-15&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/New-Classic-Toys-11067-Cocina-11067/dp/B083V7BYSG/ref=sr_1_15?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2D4QJOPZY70I1&keywords=juguetes+cocina&qid=1641835704&sprefix=juguetes+c%2Caps%2C1091&sr=8-15&madrescabread-21)

**De 3 a 5 años deja volar su fantasía**

¡Es hora de la fantasía!

A esta edad lo difícil es que se duerman en esta noche tan especial. Aquí te dejo unos [trucos para lograr que los peques se duerman en la noche de Reyes](https://madrescabreadas.com/2021/01/05/noche-de-reyes-ninos-dormir/).

Es increíble la imaginación que tienen ya que en esta época mezclan la realidad con sus deseos, ilusiones…

Algunos padres pensamos que son mentirosos pero nada tiene que ver, nos están explicando cómo entienden ellos la vida. Nosotros debemos guiarles para que aprendan la realidad.

Es hora de que los juguetes puedan ser de fantasía como **disfraces** , **trucos de magia** , **superhéroes** , **juegos de mesas simples** para aprender a relacionarse con otros niños y jugar en equipos.

[![](/images/uploads/disfraz-spiderman.jpg)](https://www.amazon.es/Rubies-Disfraz-Spiderman-Infantil-301201-S/dp/B09BDK2Q2F/ref=sr_1_20?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2BBM4MVOHM2U2&keywords=juguetes+disfraces&qid=1641835905&sprefix=juguetes+disfrac%2Caps%2C361&sr=8-20&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Rubies-Disfraz-Spiderman-Infantil-301201-S/dp/B09BDK2Q2F/ref=sr_1_20?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2BBM4MVOHM2U2&keywords=juguetes+disfraces&qid=1641835905&sprefix=juguetes+disfrac%2Caps%2C361&sr=8-20&madrescabread-21)

[![](/images/uploads/disfraz-infantil-elsa.jpg)](https://www.amazon.es/Frozen-Disfraz-infantil-Rubies-630574-M/dp/B01N41GSB1/ref=sr_1_29?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2BBM4MVOHM2U2&keywords=juguetes+disfraces&qid=1641835905&sprefix=juguetes+disfrac%2Caps%2C361&sr=8-29&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Frozen-Disfraz-infantil-Rubies-630574-M/dp/B01N41GSB1/ref=sr_1_29?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2BBM4MVOHM2U2&keywords=juguetes+disfraces&qid=1641835905&sprefix=juguetes+disfrac%2Caps%2C361&sr=8-29&madrescabread-21)

**Entre los 5 y los 8 años**

A esta edad les encanta el **deporte** , ya pueden cumplir normas, saben diferenciar la mentira y la verdad, saben cuándo un comportamiento es bueno o malo. Su energía es ilimitada. Un balón, una raqueta o equipaciones para jugar a **juegos de equipo** o aprender a montar en bici o patinar (juegos individuales) es fantástico para esta edad.

[![](/images/uploads/stamp.jpg)](https://www.amazon.es/Stamp-sm250302-pat%C3%ADn-l%C3%ADnea-para-ni%C3%B1o/dp/B01NH5434W/ref=sr_1_22?keywords=kit+de+patinaje+ni%C3%B1os&qid=1641836681&sprefix=kit+de+patinaje%2Caps%2C439&sr=8-22&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Stamp-sm250302-pat%C3%ADn-l%C3%ADnea-para-ni%C3%B1o/dp/B01NH5434W/ref=sr_1_22?keywords=kit+de+patinaje+ni%C3%B1os&qid=1641836681&sprefix=kit+de+patinaje%2Caps%2C439&sr=8-22&madrescabread-21)

También los kit de **juegos científicos** o juegos de mesa más complicados con estrategias son juguetes geniales para potenciar su desarrollo cognitivo y los **libros con más texto** y menos imagines para ampliar su vocabulario.

[![](/images/uploads/science4you.jpg)](https://www.amazon.es/Science4you-Super-Kit-Ciencias-Hormiguero-Experimentos/dp/B08CJ9DV3N/ref=sr_1_1_sspa?keywords=juegos+cientificos&qid=1641837197&sprefix=juegos+cient%2Caps%2C474&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzVzA3WlRXWkhCVzE2JmVuY3J5cHRlZElkPUEwNTA1Njc0MUFJT0FBOTRSTDNMRiZlbmNyeXB0ZWRBZElkPUExMDQ1NDk1VUJLWEVIOU9UM1NUJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Science4you-Super-Kit-Ciencias-Hormiguero-Experimentos/dp/B08CJ9DV3N/ref=sr_1_1_sspa?keywords=juegos+cientificos&qid=1641837197&sprefix=juegos+cient%2Caps%2C474&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzVzA3WlRXWkhCVzE2JmVuY3J5cHRlZElkPUEwNTA1Njc0MUFJT0FBOTRSTDNMRiZlbmNyeXB0ZWRBZElkPUExMDQ1NDk1VUJLWEVIOU9UM1NUJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==&madrescabread-21)

**A partir de los 8 años**

A partir de esta edad se complica la cosa. Ellos tienen muchas ideas, saben mucho sobre sí mismos. Tienen una relación privada que los padres no podemos ver.

Les encantan los juegos relacionados con la **robótica,** la **naturaleza** o los **juegos de mesa** **de razonamiento lógico** , **de memoria** …

[![](/images/uploads/robotica-hidraulica.jpg)](https://www.amazon.es/Buki-France-7505-Robotica-Hidr%C3%A1ulica/dp/B071FSNTMS/ref=sr_1_2?keywords=juegos+de+rob%C3%B3tica+ni%C3%B1os+10+a%C3%B1os&qid=1641837478&sprefix=juegos+de+rob%C3%B3%2Caps%2C431&sr=8-2&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Buki-France-7505-Robotica-Hidr%C3%A1ulica/dp/B071FSNTMS/ref=sr_1_2?keywords=juegos+de+rob%C3%B3tica+ni%C3%B1os+10+a%C3%B1os&qid=1641837478&sprefix=juegos+de+rob%C3%B3%2Caps%2C431&sr=8-2&madrescabread-21)

Comienza a ser importante prestar más atención a sus peticiones, es aconsejable hacerles partícipes de las decisiones y elegir juegos juntos.

Te dejamos este post sobre [regalos para adolescentes y jóvenes](https://madrescabreadas.com/2021/12/01/regalos-de-navidad-para-jovenes-adolescentes/)https://madrescabreadas.com/2021/12/01/regalos-de-navidad-para-jovenes-adolescentes/ por si te sirve de ayuda.

También te doy estas [4 ideas para pasar la noche de Reyes de una forma diferente con tus hijos adolescentes](https://madrescabreadas.com/2022/01/03/celebrar-reyes-con-adolescentes/).

Comparte con otras familias y para ayudarnos a seguir escribiendo este blog.