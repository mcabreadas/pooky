---
layout: post
title: Los logros de "Lactancia en Libertad"
date: 2014-01-16T22:20:22+01:00
image: /tumblr_files/tumblr_inline_mziiufRdjJ1qfhdaz.jpg
author: maria
tags:
  - crianza
  - colaboraciones
  - participoen
  - ad
  - lactancia
tumblr_url: https://madrescabreadas.com/post/73543239245/los-logros-de-lactancia-en-libertad
---
Hoy os voy a contar cómo un grupo de mujeres madres se organizaron un día, a raíz de una injusticia, para luchar para que no se volviera a repetir.

Mujeres muy distintas entre sí, y desde distintos puntos de España, que en su mayoría no se conocían entre ellas, pero con algo tan fuerte en común, que consiguieron limar asperezas y acercar sus posturas hasta constituír una Asociación llamada Lactancia en Libertad.

El objetivo principal de esta asociación es promover una ley que proteja a las madres para que tengan libertad para amantar a sus criaturas donde y cuando éstas lo necesiten, sin que por ello sean incomodadas, discriminadas o expulsadas de donde se encuentren, como viene pasando a menudo.

El origen de este movimiento fue [narrado en este blog](https://madrescabreadas.com/2013/08/24/crónica-de-una-tetada-anunciada-18-primark/), ya que una servidora participó activamente en sus inicios a raíz del [caso Primark](https://madrescabreadas.com/2013/08/21/derecho-a-amamantar-en-público/) que todos recordaréis.

Muchos son los logros a pesar de la juventud de esta organización, el último fue conseguir que la multinacional Leroy Merlin pidiera disculpas públicamente y rectificara porque una mamá lactante fue expulsada de una de sus tienda. Además, emitió un comunicado Interno para todos los trabajadores dentro de las instalaciones de la cadena a nivel nacional, donde sean informados de tales medidas, y existan o no salas de lactancia, nunca se podrá llamar la atención a una madre que esté amamantando a su hijo o hija, ya que es un grave acto discriminatorio y que vulnera los derechos del niño/niña.

Que una empresa de esa magnitud cree una norma interna en este sentido es un gran paso y sin duda servirá de ejemplo para otras. Creo que estamos ante los primeros pasos hacia una normativa a nivel estatal. Y si no, al tiempo.

A nivel municipal ya lo han conseguido en Bilbao, donde a partir de ahora nadie podrá decir nada a las mamás que alimenten a sus bebés en lugares dependientes del Ayuntamiento.

Esto son sólo dos ejemplos, pero cada vez Lactancia en Libertad está teniendo más peso e influencia. La prueba está en que Leroy Merlin presentó sus disculpas tan sólo un día después de que la asociación hiciera público el caso.

Por eso os animo a asociaros y a apoyar Lactancia en Libertad. Además, si estás entre las 30 primeras socias o socios te regalan una bolsa de tela como ésta, con un diseño super chulo (yo estoy enamorada desde el principio). A mí me acaba de llegar, y no sé si me hace más ilusión haberla recibido o saber que tengo el honor de estar entre las 30 primeras socias.

 ![image](/tumblr_files/tumblr_inline_mziiufRdjJ1qfhdaz.jpg)

A que os mola?