---
date: '2015-10-25T19:00:41+01:00'
image: /tumblr_files/tumblr_inline_o39z89og9R1qfhdaz_540.jpg
layout: post
tags:
- colaboraciones
- participoen
- ad
- planninos
- familia
title: 'Valencia: nuestro primer medio maratón en familia'
tumblr_url: https://madrescabreadas.com/post/131892147054/medio-maraton-valencia
---

El 18 de octubre corrimos el [medio maratón de Valencia](http://valenciarunner.com/larga-distancia/medio-maraton/). Digo corrimos porque aunque en puridad sólo lo hizo el padre de las fieras, todos acabamos como si hubiéramos hecho también los casi 22 Km.

 ![](/tumblr_files/tumblr_inline_o39z89og9R1qfhdaz_540.jpg)

Además, la familia entera hemos vivido el entrenamiento del padre paso a paso, y hemos ido celebrando con él los progresos que lo han llevado a poder superar esta dura prueba.

Ya sabéis que somos un poco frikis, por eso llevamos la pancarta más grande de todas las que vi (la hice con una sábana bajera de 1,50 m, así, sin anestesia) para dar ánimos a mi marido y a 3 de sus hermanos que también participaron.

 ![](/tumblr_files/tumblr_inline_o39z8amTpE1qfhdaz_540.jpg)

Para sostenerla, no nos cortamos un pelo, dos palos de escoba de los altos nos vinieron de maravilla. Y gracias a esto nos vieron entre la multitud cuando pasaron corriendo, porque lo que es yo, con más de 12.000 corredores, llegó un momento en que todas las caras me parecían iguales.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/FilsCHYK708?rel=0" frameborder="0" allowfullscreen></iframe>

Nos situamos un poco antes de llegar a la meta, y poco a poco fui entendiendo el sentido que tiene participar en una prueba como ésta cuando no tuve más remedio que emocionarme con la cantidad de gestos de solidaridad que vi.

Estas cosas son las que me hacen seguir teniendo fe en la humanidad.

Me di cuenta de que para la mayoría, lo realmente importante de la carrera no era ganarla, sino el motivo por el que corrían. Hay quien corría para superarse a sí mismo, o para demostrarse que podía lograrlo, y hay quien lo hacía por una causa, o para apoyar a un ser querido.

Bastantes corredores y corredoras lo hicieron con un discapacitado, empujando su carrito, y otros, llevaban a su bebé en un cochecito especial de running.

 ![](/tumblr_files/tumblr_inline_o39z8aXF5X1qfhdaz_540.jpg)

No daba crédito cuando me di cuenta de que muchos, a las puertas de la meta, se paraban y cogían a sus bebés de las gradas para cruzarla con ellos en brazos, sin importarles lo más mínimo aminorar su marca.

 ![](/tumblr_files/tumblr_inline_o39z8aAXfi1qfhdaz_540.jpg)

Algunas, tomaban a sus hijos de la mano para hacer los últimos metros con ellos

 ![](/tumblr_files/tumblr_inline_o39z8cRLbm1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39z8dRHme1qfhdaz_540.jpg)

Otros, corrían en grupo y sostenían pancartas, como la [Asociación Española de Fibrosis Quística ](http://fibrosisquistica.org/index.php?pagina=noticias&esnoticia=1007),

 ![](/tumblr_files/tumblr_inline_o39z8dpwsi1qfhdaz_540.jpg)

o llevaban globos rosas contra el cáncer de mama

<iframe width="100%" height="315" src="https://www.youtube.com/embed/cU5lVuzDUN0?rel=0" frameborder="0" allowfullscreen></iframe>

Pero lo que me hizo no poder contener las lágrimas fue un chico que de repente desplegó una pancarta de tela que ponía “Te quiero” (romántica que es una).

Creo que estas pruebas deportivas dan un ejemplo a nuestros hijos de lucha por conseguir una meta, de superación y de constancia. Les ayudan a comprender algo que es difícil que les cale sólo con las palabras: el valor del esfuerzo y el trabajo duro, a no rendirse a pesar de las dificultades.

Además, creo que muestran lo mejor de las personas, que no dudan en ayudarse unas a otras en conseguir algo que va más allá de cruzar la línea de meta.

 ![](/tumblr_files/tumblr_inline_o39z8eL6Wo1qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.