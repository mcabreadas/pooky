---
date: '2015-12-07T16:26:51+01:00'
image: /tumblr_files/tumblr_inline_o39yy5njef1qfhdaz_540.jpg
layout: post
tags:
- recetas
- navidad
- trucos
title: Decora galletas de Navidad en familia
tumblr_url: https://madrescabreadas.com/post/134728295149/decora-galletas-de-navidad-en-familia
---

Para pasar una tarde agradable en familia de lo más entretenido, te recomiendo hacer galletas de mantequilla. Nosotros las hemos hecho con formas de motivos navideños, pero la puedes hacer con las que te apetezcan, sólo dependerá de los moldes que utilices.

## Ingredientes  

- 300 gramos de mantequilla  
- 2 huevos grandes
- 350 gramos de harina de repostería
- 250 gramos de azúcar
- Un poco de canela en polvo (opcional)
- Una pizca de sal
- Pintura comestible (opcional)
- Fondant 
 ![](/tumblr_files/tumblr_inline_o39yy4Rhja1qfhdaz_540.jpg)


## Cómo se hace  

Ponemos en un bol la mantequilla junto al azúcar, y mezclaremos hasta que se integren formando una masa cremosa. 

 ![](/tumblr_files/tumblr_inline_o39yy5njef1qfhdaz_540.jpg)

Una vez hecho esto, ponemos los huevos, volviendo a mezclar con las manos hasta que la masa sea homogénea evitando los grumos. Yo prefiero hacerlo con las manos porque así los niños pueden colaborar, pero se pueden usar varillas. 

 Añadimos la harina poco a poco y volvemos a mezclar, y si queremos, también una cucharadita de canela en polvo.

 ![](/tumblr_files/tumblr_inline_o39yy5YaSC1qfhdaz_540.jpg)

Al final tiene que quedarnos una masa perfectamente homogénea y pegajosa.

La amasamos con las manos, durante unos minutos. Al acabar hacemos una bola con ella, y la dividimos en dos dos para que resulten más manejables, y ponemos cada una de ellas sobre un trozo de papel de horno. 

Las cubrimos con otro trozo de este papel y las estiramos empleando un rodillo de cocina. 

 ![](/tumblr_files/tumblr_inline_o39yy5qX571qfhdaz_540.jpg)

Cuando las tengamos bien extendidas, las dejamos reposar un poco en la nevera para que puedan cortar con los moldes mucho mejor.

Mientras, vamos precalentando el horno a 180ºC, con el calor tanto por arriba como por abajo. 

Sacamos la masa de la nevera y la estiramos un poco más con el rodillo, hasta dejarla del grosor que le daremos a las galletas, unos 5-7 milímetros. 

Retiramos el papel de horno de la parte superior de la masa, y con la ayuda de unos moldes de galletas, comenzamos a cortarla. 

 ![](/tumblr_files/tumblr_inline_o39yy60EPX1qfhdaz_540.jpg)

Conforme las vayamos cortando, las vamos colocando en la bandeja del horno cubierta con papel vegetal.

 ![](/tumblr_files/tumblr_inline_o39yy6fDW21qfhdaz_540.jpg)

Ésta es la parte favorita de los peques!

 ![](/tumblr_files/tumblr_inline_o39yy7lu8z1qfhdaz_540.jpg)

Cuando el horno haya alcanzado los 180ºC, metemos la bandeja con las galletas y horneamos durante unos 15-20 minutos, ya que cada horno es diferente y puede variar un poco de uno a otro. 

 ![](/tumblr_files/tumblr_inline_o39yy7QOlz1qfhdaz_540.jpg)

Cuando las galletas estén doradas por el centro y algo más oscuras por los bordes, sabremos que están en su punto. Entonces las sacaremos y dejaremos enfriar encima de una rejilla, para que la parte de abajo respire (en la foto están sobre el papel de horno, pero es mejor quitarlo y dejarlas directamente sobre la rejilla).

 ![](/tumblr_files/tumblr_inline_o39yy7cCvN1qfhdaz_540.jpg)

Ahora las dejamos enfriar y las guardamos en una caja metálica o tarro de cristal.

## Para decorarlas

Mi consejo es esperar al día siguiente para decorarlas, porque de lo contrario, al estar tan tiernas se pueden romper con facilidad.

Estiramos el fondant con un rodillo de plástico.

 ![](/tumblr_files/tumblr_inline_o39yy8j8ob1qfhdaz_540.jpg)

Lo cortamos usando los mismos moldes que hemos empleado anteriormente con la masa de las galletas.

 ![](/tumblr_files/tumblr_inline_o39yy8UfRL1qfhdaz_540.jpg)

Pintamos la galleta con agua para que el azúcar que lleva se quede pegajosa.

 ![](/tumblr_files/tumblr_inline_o39yy8ILEq1qfhdaz_540.jpg)

Ponemos el fondant cortado con el mismo molde que la galleta y presionamos ligeramente.

 ![](/tumblr_files/tumblr_inline_o39yy83woS1qfhdaz_540.jpg)

También podemos pintarlas con pinturas comestibles.

 ![](/tumblr_files/tumblr_inline_o39yy9t6RN1qfhdaz_540.jpg)
## El resultado

Y éste es el resultado de una tarde en familia haciendo galletas desde el pequeño de 3 años hasta la mayor de 9, incluída la mamá.

 ![](/tumblr_files/tumblr_inline_o39yy9XBut1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yy9vPhE1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yyab0KI1qfhdaz_540.jpg)

¿A que da pena comérselas?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.