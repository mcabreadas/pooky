---
date: '2014-12-16T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_ngisy2Mu671qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- regalos
- premios
- recomendaciones
- familia
- juguetes
- bebes
title: 'Concurso Tu carta a los Reyes Magos: Blocks Super de Miniland'
tumblr_url: https://madrescabreadas.com/post/105337195248/regalo-miniland
---

## Actualización 28-12-14

Con el fin de que los regalos lleguen a su destino a tiempo para Reyes  **se cerrará el concurso el 29-12-14 a las 8:00 am.**

Los cinco participantes cuyas cartas hayan obtenido más comentarios de personas diferentes tendrán que mandarme un email a  **mcabreada@gmail.com**  antes de las 12:00 am del día 29 con:

-su nombre y apellidos, dirección y teléfono e email para el envío

Si algún participante no manda el email con su lista antes del 29-12-14 a las 12:00 el premio pasará al concursante cuya carta sea la siguiente en número de comentarios. Todo ello con la finalidad de que el envío de los regalos se haga lo antes posible.

## Actualización 27-12-14

**SE ADELANTA LA FECHA TOPE PARA EL CONCURSO AL 29 -12**  
Me han propuesto adelantar la fecha de fin del concurso “Tu carta a los Reyes Magos” para que dé tiempo a que lleguen los regalos para esa noche tan especial, y creo que es muy buena idea. Así que se adelanta la fecha tope para subir vuestras cartas al lunes 29 de diciembre a las 00:00 h.

Vamos! Corre, corre!!

_Post originario:_

Este año celebramos la Navidad en el blog con un concurso muy especial, ya que los Reyes Magos de oriente me ha nombrado su paje virtual porque se ha enterado de que sois muchos los padres y madres, incluso abuelas, que lo leéis, y me ha pedido que reparta algunos regalos en su nombre para ayudaros un poquito a hacer felices a vuestros niños estas Fiestas.

Uno de los regalos que podéis incluír en vuestra cata a los Reyes Magos para participar en el concurso es el  **BLOCKS SUPER**  de [Miniland](http://www.minilandeducational.com/), con 46 piezas con ruedas para construír vehículos y simpáticos personajes, que ha recibido [varios premios](http://www.minilandeducational.com/blocks-super-mejor-juguete-del-verano/).

![image](/tumblr_files/tumblr_inline_ngisy2Mu671qfhdaz.jpg)

Las piezas de construcción del Block Super son ideales para bebés porque son grandes, robustas y muy resistentes, pero a la vez tienen un encaje suave. A mi pequeño le resulta muy fácil encajar y desencajar.

![image](/tumblr_files/tumblr_inline_nghgnsetdj1qfhdaz.jpg)

Este juego está recomendado desde los 18 meses hasta los tres años, pero en casa juegan los tres juntos e inventan mil historias, aunque los mayores tengan 6 y 8 años porque pueden construir sus propios vehículos y personajes. 

Además, viene en un práctico contenedor donde poder guardarlo después de jugar para que no quede  la casa sembrada de piezas y al pisarlas sin querer te acuerdes de Miniland y de toda su familia.

![image](/tumblr_files/tumblr_inline_ngit64qesC1qfhdaz.jpg)[![image](/tumblr_files/tumblr_inline_ngit8qCObd1qfhdaz.jpg)](http://www.minilandeducational.com/block-super-de-miniland-educational-recibe-el-premio-dr-toy-best-pick/)

Los juegos de construcción tienen múltiples beneficios para los niños. Al principiosólo manipularán las piezas e intentarán encajarlas, pero poco a poco irán aprendiendo a montar figuras a su gusto, hasta que a los 5 ó 6 años algunos serán auténticos expertos en el arte de la construcción, como mi hijo mediano.

## Beneficios de los juegos de construcciones para los niños:

_“Desarrollan sus habilidades motrices manipulando las piezas_

_Les ayuda a adquirir conceptos espaciales como el volumen; grande-pequeño; alto-bajo; corto-largo; formas geométricas,_

_Adquieren algunas nociones más complejas como el equilibrio, la simetría o la resistencia._

_Son, además, una base para que desarrollen el juego simbólico, también llamado juego de simulación: En el niño recrea situaciones que ve en la vida real._

_Imitar lo que ve y crear nuevas formas activa su propia creatividad._

_Los juegos de construcción contribuyen también a que el niño sea más organizado, pues el juego mismo hace que se acostumbre a ordenar y a clasificar las piezas._

_Son beneficiosos para su desarrollo cognoscitivo y emocional.Este tipo de juego ayuda a que el niño acepte que existen ciertas reglas físicas que debe acatar, que no todo es como él quisiera. Esto le ayudará en el futuro a adaptarse mejor a las normas en todas las situaciones de la vida._

_Ayudan a los niños a desarrollar el razonamiento lógico,_

_Inducen a tratar de encontrar soluciones creativas para los inconvenientes que se presentan en la construcción_

_Apuntan al desarrollo de habilidades de diseño.”_

_([guioteca.com](http://www.guioteca.com/))_

## ¿Cómo paticipar en el concurso para ganar este juguete?

Tenéis que escribir una carta a los Reyes Magos. La carta más popular ganará.

Las bases del concurso las puedes ver pinchando en [aquí](/portada-regalos-reyes-magos).

Tenéis hasta el 5 de enero.

[Participa](/portada-regalos-reyes-magos)!