---
date: '2015-03-28T16:00:20+01:00'
image: /tumblr_files/tumblr_inline_nlw00yquZF1qfhdaz_500.jpg
layout: post
tags:
- mecabrea
- revistadeprensa
- colaboraciones
- ad
title: Revista en papel Ser Padres
tumblr_url: https://madrescabreadas.com/post/114845001404/serpadres-madres-cabreadas
---

Damos el salto a la prensa escrita con la revista [Ser Padres](http://serpadres.com/revistadigital/) gracias a mi compañera blogger Vanesa, del blog [Una madre como tú](http://www.laorquideadichosa.com/), que menciona nuestro [Club de las madres Cabreadas entre los grupos de Facebook](http://www.laorquideadichosa.com/) que ayudan y apoyan a las madres.

Gracias, Vanesa!

 ![image](/tumblr_files/tumblr_inline_nlw00yquZF1qfhdaz_500.jpg)

Así que, ya sabes, si te apetece desahogarte de un cabreo, únete al club, allí intentaremos tomárnoslo con humor y buen rollo para entre todas, superarlo de la mejor forma posible.

[Únete a El Club de las Madres Cabreadas aquí](https://www.facebook.com/groups/clubmadrescabreadas/)

Te atreves?