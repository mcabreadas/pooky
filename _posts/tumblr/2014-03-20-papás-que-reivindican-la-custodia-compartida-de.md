---
layout: post
title: Papás que reivindican la custodia compartida de sus hijos en caso de divorcio
date: 2014-03-20T09:09:00+01:00
image: /tumblr_files/tumblr_inline_pc7s7abKq31qfhdaz_540.jpg
author: .
tags:
  - derechos
tumblr_url: https://madrescabreadas.com/post/80150488480/papás-que-reivindican-la-custodia-compartida-de
---
El día del padre tuve la ocasión de charlar un rato con Ignacio Herranz, presidente de la [Asociación de Padres de Familia Separados de la Region de Murcia (APFS)](http://www.apfsmurcia.es) , y letrado de la misma, sobre su principal reivindicación, bajo el lema “custodia compartida, unamos corazones”, en un acto organizado en una céntrica plaza de mi ciudad donde, en un ambiente lúdico, como corresponde a la festividad que se celebraba, entre concursos de dibujo, títeres, música infantil, cajitas reivindicativas de chuches y globos verdes, exigían un cambio legislativo real en esta materia.

![image](/tumblr_files/tumblr_inline_pc7s7abKq31qfhdaz_540.jpg)

Tuve la oportunidad de debatir con uno de los miembros de la asociación, que en todo momento se mostró amable y receptivo ante un punto de vista diferente, y finalmente su presidente, tuvo la amabilidad de explicarme lo que, seguramente, habría estado diciendo toda la mañana a los medios de comunicación que se acercaron a la plaza, pero que con mucha paciencia volvió a repetir para este humilde blog, además de responder a todas mis preguntas y curiosidades. Cosa que aprecio en lo que vale y desde aquí, mi más sincero agradecimiento, Ignacio.

## Quieren cambiar la ley

Al preguntar a Ignacio sobre qué pedían exactamente, me comentó que lo que se pedía al inicio de crearse esta asociación a nivel nacional en 1993 es muy diferente de lo que se reivindica en la actualidad, ya que la jurisprudencia de 2005 dio un giro a lo que se venía aplicando por los jueces hasta ese momento. Las cosas han cambiado, pero ahora quieren más, quieren que la ley recoja como norma general la custodia compartida a la hora de que un juez decida qué progenitor se queda al cuidado de los menores en casos de separación o divorcio., y caso de que alguno esté en desacuerdo por considerarla perjudicial para éstos, tendría que probar los motivos.

Lograron que se iniciara un proyecto de ley en este sentido a nivel de la Región de Murcia, pero esto se quedó en papel mojado porque el proyecto no llegó a ver la luz.\
 Defienden la flexibilidad de esta fórmula, ya que, creen que hay muchas formas de hacerla efectiva, quizá tantas como familias, incluso, al plantear el caso de un bebé recién nacido que se alimente con lactancia materna, defienden que jamás interferiría en ésta, y que siempre se puede buscar la manera de respetarla.\
 No quieren ser “progenitores de segunda”, ni que sus hijos e hijas vivan en situación de “semiorfandad artificial derivada de decisiones judiciales que perpetúan un sistema anacrónico y machista”.

![image](/tumblr_files/tumblr_inline_pc7s7aIsEI1qfhdaz_540.jpg)

Ignacio se queja de que algunas asociaciones que se tildan de feministas no los apoyan y que se acogen a un modelo que da un paso atrás en la lucha por la igualdad de hombres y mujeres. Ellos quieren huír del modelo de madre criadora de hijos y padre proveedor de alimentos, defienden la igualdad de hombre y mujeres, también a la hora de decidir quien cuida de los hijos cuando se rompe la pareja.

## Situación actual

Actualmente la mayoría de los jueces otorgan la guarda y custodia de los menores a la madre, aunque esto no está recogido en ninguna ley, sí que viene siendo una práctica judicial generalizada. Aunque ya está cambiando esta tendencia hacia la generalización de la custodia compartida, sobre todo en comunidades como Cataluña, Aragón y Valencia, según el presidente de APFS.

Al contrario de lo que se cree, la última jurisprudencia del Supremo no establece que la custodia compartida sea de aplicación automática en los casos de separación o divorcio, y mucho menos que el Juez pueda acordarla sin que ninguna de las partes la haya solicitado, como se ha interpretado por algún Juez, que incluso la impuso en su sentencia de divorcio sin que ningún cónyuge la hubiera solicitado.\
 Varios titulares de prensa y Televisión así lo han venido anunciando desde que se hizo pública la sentencia de fecha 29 de abril de 2013, generando una gran confusión social. Lo que dicha Sentencia señala es que debería ser lo normal y lo deseable, siempre que así lo aconseje el interés del menor, y siempre que sea posible. Para lo que se valorarán por el Juez los siguientes requisitos, que ya había recogido la Sala Primera en Sentencias anteriores.\
 Tales son:\
 – La práctica anterior de los progenitores en sus relaciones con el menor.\
 – Sus aptitudes personales.\
 – Los deseos manifestados por los menores competentes.\
 – El número de hijos.\
 – El cumplimiento por parte de los progenitores de sus deberes en relación con los hijos.\
 – El respeto mutuo en sus relaciones personales.\
 – El resultado de los informes exigidos legalmente.\
 – Y, en definitiva, cualquier otro que permita a los menores una vida adecuada en una convivencia que forzosamente deberá ser más compleja que la que se lleva a cabo cuando los progenitores conviven.

## Mi opinión

Mi opinión difiere ligeramente de la defendida por la APFS, ya que, si bien es cierto que no es correcto que se abuse por parte de los jueces de atribuir la custodia sistemática a la madre por el hecho de serla, también lo sería que atribuírla de forma compartida se convirtiera en una tendencia sistemática, como sería igualmente erróneo atribuírla por sistema al padre.

¿Qué quiero decir con esto? Que es peligroso establecer una norma general en la ley porque los jueces terminarán “acomodándose” a la misma, como han venido haciendo hasta ahora, incluso aunque no venga recogida en la ley.

Actualmente el Código Civil no establece la custodia preferente a favor de nadie, sino que dice que el juez decidirá según el caso concreto, y así debería hacerse estudiando las circunstancias concretas de cada caso, sin prejuicios, ni ideas preconcebidas como la de que la custodia materna es la idónea mientras no se demuestre lo contrario.

No me opongo a la custodia compartida en absoluto, y más viendo que hay padres defendiéndola con tanto ahínco. Pienso que es una suerte y un orgullo para sus hijos que sus padres luchen así por ellos, lo que no me convence es que por ley se imponga como norma general.

No señor. Partamos de cero, partamos de que ninguna fórmula es la idónea, porque no hay recetas mágicas porque cada niño y cada familia es un mundo, quitémonos los prejuicios, sobre todo los jueces, y analicemos cada caso en particular, y cada circunstancia familiar sin predeterminar que un sistema es mejor que otro “per se”.

No olvidemos que la ley pone por encima de cualquier otro derecho que esté en juego, los derechos del menor de edad, lo que prima es su interés por encima del derecho a la igualdad hombre-mujer. Esta primacía del derecho del menor es lo principal que debe mover al juez.

He de confesar que, aunque no estoy del todo de acuerdo con la APFS, me encantó ver a unos hombres defendiendo el ejercicio de su paternidad responsable, que les mueve el amor hacia sus hijos y el querer ser parte activa en su vida.\
 Me pareció un acto muy atractivo para los niños, desenfadado, elegante (últimamente he visto concentraciones de gusto dudoso que lo único que lograban era restar credibilidad a sus pretensiones).

Me gustan los papás de la APFS, me gustan los papás implicados y responsables, y los animo a seguir luchando por sus hijos.

![image](/tumblr_files/tumblr_inline_pc7s7bPEwA1qfhdaz_540.jpg)