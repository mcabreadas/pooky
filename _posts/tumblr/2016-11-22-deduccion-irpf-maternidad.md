---
date: '2016-11-22T07:40:05+01:00'
image: /tumblr_files/tumblr_inline_oh16itLd771qfhdaz_500.gif
layout: post
tags:
- derechos
- crianza
- maternidad
title: La prestación por maternidad, declarada exenta de IRPF por un Tribunal
tumblr_url: https://madrescabreadas.com/post/153507209859/deduccion-irpf-maternidad
---

El Tribunal Superior de Justicia de Madrid declara por primera vez la prestación de maternidad exenta del IRPF!! Esto significa dos cosas: 

-Ojo! Que debéis seguir tributando por ella, pero tenéis una base para reclamar que os lo devuelvan.

-Las que cobrasteis la prestación a partir de 2012 podríais reclamar lo que tributásteis al IRPF por ella, pero sería un proceso lento en principio u con varios pasos. 

Aquí tenéis la [noticia que acaba de salir](http://cincodias.com/cincodias/2016/11/18/economia/1479485552_696729.html). 

Yo, por mi parte voy a investigar más y os iré contando mis conclusiones.

 ![](/tumblr_files/tumblr_inline_oh16itLd771qfhdaz_500.gif)