---
date: '2015-03-29T15:59:48+02:00'
image: /tumblr_files/tumblr_inline_nlvravb9f31qfhdaz_500.jpg
layout: post
tags:
- revistadeprensa
- colaboraciones
- comunion
- ad
- familia
title: De los deberes de los padres, entre otros…organizar la Comunión | Canal Extremadura
tumblr_url: https://madrescabreadas.com/post/114932976964/de-los-deberes-de-los-padres-entre
---

[De los deberes de los padres, entre otros…organizar la Comunión | Canal Extremadura](http://www.canalextremadura.es/alacarta/radio/audios/de-los-deberes-de-los-padres-entre-otrosorganizar-la-comunion)  
 ![image](/tumblr_files/tumblr_inline_nlvravb9f31qfhdaz_500.jpg)

Mi compañera María Hurtado, que escribe el blog “[Asuntos de Familia](http://www.asuntosdefamilia.es/)”, nos menciona en [este programa de radio de Canal Extremadura](http://www.canalextremadura.es/alacarta/radio/audios/de-los-deberes-de-los-padres-entre-otrosorganizar-la-comunion), a partir del minuto 18. 

En concreto, nuestro [grupo de Facebook “Organizando la Comunión”](https://www.facebook.com/groups/1377576085877092/), al que estás invitado si te encuentras, como nosotras, en plena vorágine comunionera.

 ![image](/tumblr_files/tumblr_inline_nlvrb6DsWi1qfhdaz_500.jpg)

Muchas gracias, María.

[Únete al grupo de Facebook Organizando la Comunión aquí.](https://www.facebook.com/groups/1377576085877092/)