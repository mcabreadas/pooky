---
date: '2015-06-30T16:00:23+02:00'
image: /tumblr_files/tumblr_inline_p7woo4lFcz1qfhdaz_540.jpg
layout: post
tags:
- familia
title: Feliz jubilación, papá
tumblr_url: https://madrescabreadas.com/post/122853115648/jubilacion-papa
---

Hemos celebramos la jubilación de mi padre.

Le hemos hecho una gran fiesta. Se lo merece.

Un hombre que ha trabajado por superarse a sí mismo incansablemente, que estudiaba por libre por las noches, mientras de día sacaba un jornal desde bien pequeño para ayudar en su casa, como mayor de los hermanos que es.

Un hombre que salió del pueblo contra todo pronóstico, siguiendo a la mujer de sus sueños, y venció sus miedos rompiendo con lo que hacían los demás y forjando su propio camino.  
Se preparó y se formó por su cuenta, siempre rodeado de los suyos, porque quería progresar.

Y lo consiguió.

Por el camino dejó aquel muchacho que tintineaba las copas en la mesa con sus hermanos, que cantaba la misa en las reuniones familiares imitando al cura, y que tiraba mantecados duros contra las puertas de su casa, para hacer reír. Que se iba de mona con sus amigos, al cine, de paseo…  
Donde un día conocería a mí madre, la conquistaría, la seguiría hasta la “gran ciudad”, que también tuvo que conquistar, se casarían, y nos tendrían a nosotras.  
No sin antes hacerse un hombre en la mili.  
De esa época recuerda muy bien las canciones que cantaba con los otros soldados, los madrugones a toque de diana, que luego usaría para levantarnos a mí hermana y a mí de la cama para ir al cole, y cómo se sacó el carnet de conducir al mismo tiempo, y cuando fue a pedirle permiso a su sargento para acudir al examen, éste le dijo:“ si no apruebas no vuelvas”.

Después se lanzó al mundo empresarial en firmas bastante importantes de la zona, hasta que, siendo fiel a su propósito de ser mejor, decidió correr la aventura de ser su propio jefe.  
Ahora ya toca descansar que, como se suele decir, no significa estar sin hacer nada (cosa que, por otra parte, sería imposible para él) sino cambiar de actividad.  
Y creo que a eso lo podrán ayudar mucho sus nietos, que lo adoran, y están deseando hacer muchas cosas con él porque lo adoran, al igual que nosotras, al igual que mi madre.

Felicidades papá, y gracias por una vida de trabajo, superación y entrega a tu familia.

 ![image](/tumblr_files/tumblr_inline_p7woo4lFcz1qfhdaz_540.jpg)