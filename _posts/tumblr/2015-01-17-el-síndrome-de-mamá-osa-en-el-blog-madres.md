---
date: '2015-01-17T19:59:00+01:00'
image: /tumblr_files/tumblr_inline_nic4ikVJ1w1qfhdaz.jpg
layout: post
tags:
- crianza
- revistadeprensa
- libros
- planninos
- colaboraciones
- ad
- familia
- bebes
title: El síndrome de mamá osa en el blog 'Madres cabreadas'
tumblr_url: https://madrescabreadas.com/post/108366909659/el-síndrome-de-mamá-osa-en-el-blog-madres
---

[El síndrome de mamá osa en el blog 'Madres cabreadas'](http://prensaparaninfo.com/2015/01/16/el-sindrome-de-mama-osa-en-el-blog-madres-cabreadas/)  

> “Libro para mamás primerizas”. Así define el blog ‘Madres cabreadas’ El síndrome de mamá osa, el libro que ha escrito la periodista experta en temas infantiles Luz Bartivas y que constituye toda un… ([leer más](http://prensaparaninfo.com/2015/01/16/el-sindrome-de-mama-osa-en-el-blog-madres-cabreadas/))

La editorial Grupo Paraninfo cita la reseña que hicimos en el blog sobre el libro [El Síndrome de Mamá Osa](https://madrescabreadas.com/2015/01/16/libro-para-mamás-primerizas-el-s%C3%ADndrome-de-mamá/). 

## _Si te apetece adquirir el libro, lo tienes en [aquí](https://amzn.to/3w0xhPq).

[![](/tumblr_files/tumblr_inline_nic4ikVJ1w1qfhdaz.jpg)](http://prensaparaninfo.com/2015/01/16/el-sindrome-de-mama-osa-en-el-blog-madres-cabreadas/)