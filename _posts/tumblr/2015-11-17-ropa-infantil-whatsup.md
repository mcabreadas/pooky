---
date: '2015-11-17T10:00:21+01:00'
image: /tumblr_files/tumblr_inline_o39z3pda5K1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- colaboraciones
- ad
- testing
- recomendaciones
- familia
- moda
title: Ropa infantil loca y divertida con What´s up! Kids
tumblr_url: https://madrescabreadas.com/post/133393320930/ropa-infantil-whatsup
---

Mi Princesita está en esa edad en la que le gusta elegir su ropa, y ya no hay manera de imponerle nada. Por eso me puse a investigar marcas de ropa que ofrecieran un estilo que fuera llamativo para ella, pero que también me guste a mí porque, créeme, es mejor que la ropa no sea un motivo diario de conflicto en casa. 

Mejor llegar a un “ten con ten”, ¿no te parece?

Ya te conté todas las colecciones que se presentaron en el [desfile de moda infantil Madrid Petit Walking](/2015/11/04/pasarela-moda-madrid.html), y que me vine encantada por todo el abanico de posibilidades que hay para vestir a los peques, y no tan peque. 

Pero hubo una marca que me encantó especialmente porque tiene ropa de esa que elegirían tus hijos si los dejaras solos vestirse a su gusto, pero al mismo tiempo nos gusta también a las madres.

Mira si no, y me dices si te gusta:

 ![ropa nina whats up kids](/tumblr_files/tumblr_inline_o39z3pda5K1qfhdaz_540.jpg)<map name="Map15" id="Map15"><area alt="camiseta rosa nina moto" title="" href="/tumblr_files/tumblr_inline_nxae0swBuo1qfhdaz_540.jpg" target="_blank" shape="rect" coords="4,5,182,176">
<area alt="conjunto falda camiseta rosa" title="" href="/tumblr_files/tumblr_inline_nxae0sLBCK1qfhdaz_540.jpg" target="_blank" shape="rect" coords="183,3,358,178">
<area alt="ropa otono nina rosa" title="" href="/tumblr_files/tumblr_inline_nxae0tqM0L1qfhdaz_540.jpg" target="_blank" shape="rect" coords="363,6,536,178"></map>

Pincha sobre cada foto para verla más grande.

Puedes ver [todos los looks que What´s Up! Kids presentó en el desfile en este video](https://youtu.be/ZluXJwm7F-w).

Si te apetece visitar la [web de What´s up! Kids](http://www.shopwspkids.com/), y quieres comprar algún conjunto, puedes hacerlo utilizando este código descuento hasta el 22 de noviembre por ser lector de mi blog:

**Código descuento 30% What´s up Kids: WSPWALKING**

Ya veis que tienen ropa un poco loca y divertida. A mi hija le encantó esta camiseta rosa chicle combinada con la falda con flores en el mismo tono, fondo azul turquesa y volantito de tul que asoma por debajo. 

 ![](/tumblr_files/tumblr_inline_o39z3pmFzB1qfhdaz_540.jpg)

Además, elegimos el chaleco de pelo gris para este tiempo en el que ya hace frío.

 ![](/tumblr_files/tumblr_inline_o39z3qCKsC1qfhdaz_540.jpg)

La calidad de los tejidos es excelente. 

La camiseta es de algodón, y es bastante gruesa, no es de esas que parecen papel de fumar, y la falda tiene un tacto muy agradable y tiene una caída muy bonita. 

¡Nada de sintéticos!

 ![](/tumblr_files/tumblr_inline_o39z3rEdiO1qfhdaz_540.jpg)

El chaleco viene con botones, y un cinturón trenzado en marrón, pero Princesita decidió que le gustaba más llevarlo abierto, y se lo quitó. Y cuando ella tiene una cosa clara, no hay más que hablar.

## Ropa de 0 a 10 años

La tienda on-line de What´s up! Kids es muy fácil de navegar porque está estructurada por colecciones y por edades. 

Te doy una pequeña pincelada.

- [Recién nacido](http://www.shopwspkids.com/c154918-new-born.html)  
 ![](/tumblr_files/tumblr_inline_o39z3r6PEb1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39z3rdy5a1qfhdaz_540.jpg)
- Bebé niña:3-36 meses  
 ![](/tumblr_files/tumblr_inline_o39z3szDNY1qfhdaz_540.jpg)

- Bebé niño: 3-36 meses  
 ![](/tumblr_files/tumblr_inline_o39z3s3Hq91qfhdaz_540.jpg)
- Niña: 2-10 años  
 ![](/tumblr_files/tumblr_inline_o39z3txm2s1qfhdaz_540.jpg)

- Niño: 2-10 años  
 ![](/tumblr_files/tumblr_inline_o39z3tSwIK1qfhdaz_540.jpg)

¿Ytú? ¿Cómo todavía eliges la ropa de tus hijos, o ya no te dejan?

NOTA: Este post no es patrocinado. La marca me ha cedido un conjunto de ropa para que testee las calidades y dé mi sincera opinión.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.