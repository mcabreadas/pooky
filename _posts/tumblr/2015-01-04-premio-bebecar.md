---
date: '2015-01-04T10:02:00+01:00'
image: /tumblr_files/tumblr_inline_nhn7ycKj5W1qfhdaz.jpg
layout: post
tags:
- crianza
- colaboraciones
- puericultura
- ad
- eventos
- familia
- seguridad
- bebes
title: Visita a Bebécar y entrega de Premios
tumblr_url: https://madrescabreadas.com/post/107095924414/premio-bebecar
---

![image](/tumblr_files/tumblr_inline_nhn7ycKj5W1qfhdaz.jpg)

Ayer tuve la suerte de pasar el día en Toledo, ciudad que me encanta, y a la que siempre es una suerte volver, sobre todo si es para recibir un premio en el concurso organizado por [Bebécar](http://www.bebecar.com/) y Madresfera con motivo de la presentación de su nuevo cochecito HipHop, de la Magic Collection, del que os hablé en este post, “[Cinco claves para la elección del cochecito para tu bebé](/2014/12/06/claves-eleccion-cochecito-bebe.html)”, que finalmente resultó finalista.

Para mí es un orgullo que se reconozca el trabajo en este blog, ya que le dedico mucho tiempo y cariño porque disfruto mucho y, sobre todo, porque respeto mucho a quien dedica su tiempo a leerlo.

Por eso me siento muy agradecida a la familia Bebécar porque me he sentido valorada, querida y he descubierto a las personas que hay detrás de una de las marcas de cochecitos de bebé mejor valoradas de España.

Ya os conté que mi primera impresión en la Feria de Madrid es que se trataba de una empresa familiar, con personas comprometidas con sus clientes, y no me equivocaba.

Además, compartí podio y mesa con Joha, del blog [Mimitos de Mamá](http://mimitosdemama.es/), a quien ya conocía de forma virtual, y quien ganó el primer premio: mi más sincera enhorabuena. Y Sonia, del blog [Nonamua.net](http://nonamua.net/), sin duda un gran descubrimiento.

 ![image](/tumblr_files/tumblr_inline_nhnabinlNC1qfhdaz.jpg)

Pero, sin duda, lo mejor del día fue la sobremesa, donde tuvimos la ocasión de intercambiar opiniones con el Presidente de Bebécar y sus hijos David y Natalia en un interesante debate, acalorado en algún momento, donde compartimos nuestros distintos puntos de vista, me abrió la mente y me hizo descubrir el amor con que esta familia realiza su trabajo.

Gracias por todo.