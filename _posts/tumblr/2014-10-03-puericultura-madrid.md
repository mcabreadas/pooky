---
date: '2014-10-03T23:52:00+02:00'
image: /tumblr_files/tumblr_inline_pdpcmyNTu21qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- eventos
- recomendaciones
- familia
- colecho
- bebes
title: Mis siete caprichos de la Feria Puericultura Madrid 2014
tumblr_url: https://madrescabreadas.com/post/99083582024/puericultura-madrid
---

Son siete, como los pecados capitales, los productos de los que me encapriché en la [Feria de Puericultura de Madrid](http://www.ifema.es/puericulturamadrid_01) de este año, donde primó la originalidad, la novedad y la adaptación de los detalles a las necesidades de las familias de hoy en día ofreciendo alternativas para los diferentes formas o estilos de crianza.

 ![image](/tumblr_files/tumblr_inline_pdpcmyNTu21qfhdaz_540.jpg)

**1-Cuna de colecho Next2me de Chicco**

Me sorprendió y me encantó que una gran marca como **Chicco** ofreciera por primera vez una **cuna de colecho** que, además es regulable en altura para que quede reclinado el colchón y así mantener la cabeza del bebé por encima de su estómago. Para niños con reflujo puede ser una estupenda solución, ya sabéis que yo me tuve que fabricar una y elevar el colchón calzándolo porque no encontré una solución en el mercado satisfactoria.

 ![image](/tumblr_files/tumblr_inline_pdpcmzmK8D1qfhdaz_540.jpg)

**2-La silla de automóvil [Doona](https://www.youtube.com/watch?v=JQPf9nWzA0k)[grupo 0 con ruedas](https://www.youtube.com/watch?v=JQPf9nWzA0k)**

Fue diseñada por un papá qué quería una solución práctica para cuando salía a hacer recados con su bebé en coche. Inventó una silla con ruedas que se pliegan cuando se ancla al coche, y se sacan cuando se necesita caminar sin necesidad de estar cambiando al bebé de sitio y sin interferir en su descanso, ya que los mecanismos son tan suaves que no cuesta nada cambiarla de posición. Yo misma la estuve manipulando y es un lujo, en serio. Lo único, que la consejo para coches grandes porque al llevar las ruedas ocupa un poco más que el resto y puede tocar en el asiento de alante.

 ![image](/tumblr_files/tumblr_inline_pdpcmzT5g21qfhdaz_540.jpg)

**3-La silla ligera de paseo de [Asalvo](http://www.asalvo.com/)**

Me conquistó por su fácil manejo, incluso con una sola mano, y su doble capota tipo toldo, a la que se pueden quitar los laterales para proteger al bebé del sol, pero darle ventilación. Me pareció un detalle importante, sobre todo porque en mi ciudad el verano es largo y muy caluroso. Se pliega tipo paraguas sólo con el pie en dos pasos y se puede transportar cómodamente con un asa. El diseño es muy elegante y la tapicería es repelente al agua. El aluminio es de última generación y lleva una franja del mismo tono que la tela. Las empuñaduras son de EVA y las ruedas también, y llevan una franja el color de la silla. La reclinación no es brusca y se puede hacer con una mano. Es totalmente reclinable.

 ![image](/tumblr_files/tumblr_inline_pdpcmzpBr21qfhdaz_540.jpg)

**4-Los juegos de construcción [Blocks super](http://www.minilandeducational.com/category/juguetes-educativos/construcciones-productos/blocks-super/ "Juegos de construcción Blocks super") de Miniland Educational**

Me encantan para mi peque porque las piezas son grandes y suaves en el encaje, tiene coches, que le encantan y le divierte poder construir sus propios vehículos y personajes. Además, han sido elegidos “Mejor Juguete del Verano en la categoría de Juegos de Construcción” por la Asociación Española de Fabricantes de Juguetes (AEFJ).

 ![image](/tumblr_files/tumblr_inline_pdpcn0t1901qfhdaz_540.jpg)

**5-El [Fresh Squeezed de Infantino](http://www.infantino.com/category.cfm?subcategory=5019&showall=1)**

me encantó por lo práctico, inclyuso para mis hijos mayores, que les encantan los Smoothies, que están tan de moda ahora. Se trata de un sistema para rellenar bolsitas con comida casera triturada para que el bebé pueda succionarla por una boquilla, o si todavía no succiona, se le puede poner una boquilla/cuchara para ayudarle. Cuando son más mayorcitos la pueden coger ellos solitos y comer de forma independiente. Yo la usaría, sobre todo para los smoothies caseros, ya que mi peque no llegó a tomar triturados. Hay bolsitas desechables y reutilizables. No me negaréis que original es un rato.

 ![image](/tumblr_files/tumblr_inline_pdpcn0i98S1qfhdaz_540.jpg)

**6-La marca [BabyMoov](http://www.babymoov.es/)**

Me robó el corazón. No la conocía, por eso no sabía que los productos los diseñan las propias madres y padres según sus necesidades diarias en el cuidado de sus hijos. La última línea de muselinas la diseñaron un grupo de blogueras francesas, y son ideales. Pero aparte de la estética, hay más, como una cámara de vigilancia sin emisión de ondas electromagnéticas en la zona del bebé, y un biberón que pretende no interferir en la lactancia materna, con tetina similar al pezón de la madre, y cuando digo similar digo que cuando la vi pensé en la forma del mío cuando lo acababa de soltar mi niño, en serio, muy conseguido.

 ![image](/tumblr_files/tumblr_inline_pdpcn0FotW1qfhdaz_540.jpg)

**7-La nueva silla de paseode [Nuna](http://www.nuna.eu/pepp-luxx), PeppLux**

Me gustó por su diseño y por su comodidad para plegarla. Fue la única que encontré que se mantuviera sola una vez plegada. Parecerá un detalle sin importancia, pero es la diferencia entre necesitar otra persona que te la sostenga, o poder apañártelas sola para meterla en el maletero sin que la niña se te vaya corriendo.

 ![image](/tumblr_files/tumblr_inline_pdpcn19BM21qfhdaz_540.jpg)

Éste fue el resultado de una jornada intensa donde aprendí mucho y compartí un rato con mis compis blogueras Elena, del blog [La Guinda de limón](http://laguindadelimon.blogspot.com.es/), Mónica, del blog Quica & Kids y Lola de Tu bebé box donde hubo charla, risas y, cómo no, fotos.

 ![image](/tumblr_files/tumblr_inline_pdpcn15WRS1qfhdaz_540.jpg)

Gracias, chicas!!!