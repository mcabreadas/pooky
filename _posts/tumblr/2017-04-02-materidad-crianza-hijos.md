---
date: '2017-04-02T18:11:30+02:00'
image: /tumblr_files/tumblr_inline_onshrav7Rx1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- crianza
- cosasmias
- maternidad
title: Cuando ellos te dan más
tumblr_url: https://madrescabreadas.com/post/159115458529/materidad-crianza-hijos
---

Tú eres el responsable del [desbarajuste de mi vida](https://madrescabreadas.com/2017/01/24/maternidad-crianza-conciliacion/) y, al mismo tiempo, quien me salva cada día que decaigo. 

Son tantas las veces que me has levantado por la mañana cuando pensaba que sería imposible, cuando las fuerzas se olvidaban de mí… entonces aparecías por la puerta de mi dormitorio como un ángel, mi ángel salvador, y me arrollabas con tu amor incondicional sin pedir permiso recordándome una vez más lo que de verdad importa, por lo que estoy en este mundo, en esta familia, por lo que soy madre. 

Tú rompiste mis esquemas y gracias a ti me di cuenta de que algunas cosas en mi vida estaban equivocadas a pesar de que seguía empeñada en mantenerlas, pero reordenaste mis prioridades, me enseñaste a escucharme a mí misma, [a confiar en mi instinto](https://madrescabreadas.com/2013/05/20/las-horas-contigo-contigo-las-horas-se-llenan-de/) y a reencontrarme. 

 ![](/tumblr_files/tumblr_inline_onshrav7Rx1qfhdaz_540.jpg)

Tu forma de mirar el mundo, tus verdades como puños, tu increíble sensatez con sólo 4 años, tu sensibilidad y empatía hacia mí… [nuestra conexión](https://madrescabreadas.com/2015/10/10/semana-lactancia/) me sostiene y hace que sonría cada día, hasta los más grises, aquellos en los que antes de despertar pienso que va a ser imposible hacerlo. 

Entonces oigo tus pasos descalzos por el pasillo y suspiro con alivio porque ya escucho a mi ángel salvador que viene a recargarme la vida. 

Ya te vas haciendo mayor, y vienes con menos frecuencia a despertarme, pero tienes el don de aparecer cuando de verdad más te necesito porque una conexión nos une. 

[La maternidad puede ser muy dura](https://madrescabreadas.com/2013/06/22/la-soledad-de-la-crianza/), y a veces hace que nos olvidemos de quiénes somos o cómo éramos antes, pero te aseguro que ellos nos dan mucho más de lo que podamos imaginar, sólo tenemos que saber observar y escuchar.

*Si te ha gustado suscríbete para no perderte nada.*
