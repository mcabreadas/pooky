---
date: '2017-01-25T22:08:52+01:00'
image: /tumblr_files/tumblr_inline_okcl2yC2js1qfhdaz_540.png
layout: post
tags:
- puericultura

title: Cómo encajar 3 sillas en un coche normal
tumblr_url: https://madrescabreadas.com/post/156368454404/silla-auto-regulable
---

Una de las dificultades con las que nos encontramos las familias numerosas de 5 miembros a la hora de viajar con los niños es lograr meter las 3 sillas de coche estrechas en nuestro coche, lo que muchas veces lleva a que nos planteemos incluso comprar un vehículo nuevo.

En nuestro caso, antes de eso, quisimos **agotar todas las posibilidades** de encontrar una silla grupo 3 estrecha adecuada para la mayor, que cupiese junto con las otras dos, ya que cuando un nuevo miembro de la familia llega, lo más práctico es que vaya heredando lo de sus hermanos, y las sillas que teníamos hasta ahora, no nos cabían juntas.

Después de mil rompecabezas, tomar medidas y probar algunas sillas de coche más estrechas del mercado que en teoría sí cabían, pero en la práctica no era así o quedaban tan juntas, que los niños no se podían ni abrochar el cinturón, decidimos probar con la silla Rodifix Air Protect, de Bebé Confort Grupos 2 y 3, para niños de 15 a 36 Kg.

[![](/images/uploads/rodifix-airprotect.jpg)](https://www.amazon.es/dp/B0767LWPZH/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=0cd29ea03d68e2411beff7625201288b&creativeASIN=B0767LWPZH&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B0767LWPZH/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=0cd29ea03d68e2411beff7625201288b&creativeASIN=B0767LWPZH&th=1)

Puedes elegirla en estos colores el nuestro es el azul con topitos: [Colores de la silla Rodifix Air Protect](http://amzn.to/2jqKluz).

Bebé Confort siempre nos ha dado confianza, de hecho la primera silla que compramos después de la del grupo 0 que iba con el trío, fue de esta marca, y la han usado todos mis hijos, dando un **resultado fenomenal tanto en resistencia y durabilidad**, como en comodidad (era subirse al coche y dormirse).

Cuando usamos la Rodifix Air Protect nuestros hijos tenían 4, 9 y 11 años, y viajaban los tres en la parte de atrás, cada uno con su sillita. Éstas son las razones por las que nos decidimos a probar esta silla.

![](https://d33wubrfki0l68.cloudfront.net/a3b59fbe3f8ec74139a77bdad2ca6b252aefd9d9/0aa30/tumblr_files/tumblr_inline_okcl1zjocb1qfhdaz_540.png)

## ¿Por qué la silla de coche estrecha Rodifix Air Protect sí encaja en un coche normal?

1. Porque tiene **una de las bases más estrechas del mercado** , 48 cm, y la verdad, todo centímetro que se gane para lograr encajar el rompecabezas es valiosísimo.
2. Porque es **regulable en anchura** , lo que nos permite adaptarla al hueco que tenemos e ir abriéndola conforme el niño va creciendo.

## Características de la silla de coche estrecha Rodifix Air Protect

En este vídeo te muestro todas las características, y verás la facilidad con la que se regula en altura y anchura y además es una **silla grupo 3 reclinable**:

<iframe width="560" height="315" src="https://www.youtube.com/embed/rnQAF9k5i2g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

\-Es una **silla grupos 2 y 3** , así que ahora mismo nos sirve para los tres niños, ya que el peso indicado es de 15 a 36 Kg, y la edad, de 3,5 a 12 años.

\-Es una **silla de auto con sistema isofix** combinado con cinturón de seguridad. En nuestro caso, para que quepan las tres sillas tenemos que ponerla sin anclar para poder echarla lo más posible hacia un lado para así poder encajar las otras dos.

![](https://d33wubrfki0l68.cloudfront.net/fa7bc97929be77ff5e43cb0e7baf27c3af6f02f5/33483/tumblr_files/tumblr_inline_okcm16qys51qfhdaz_540.png)

\-Es una **silla regulable en altura** , lo que permite que vayan siempre cómodos, aunque crezcan mucho, como mi hija. Además, tiene sujeción lateral, para más seguridad, y es muy muy confortable.

![](https://d33wubrfki0l68.cloudfront.net/5d03ea364184d6ac130ddfe7d431f0dc1e2c0ab4/3e17f/tumblr_files/tumblr_inline_okcln8txvs1qfhdaz_540.png)![](https://d33wubrfki0l68.cloudfront.net/a86a45d918f8cd4cf44443af78df6cfc0e1e5531/0c749/tumblr_files/tumblr_inline_okcll3ezml1qfhdaz_540.png)

\-Es una **silla estrecha y reclinable** de una forma muy sencilla, tal y como te muestro en el vídeo. Así podrán echarse sus siestas.

* La **silla de auto Cabe en el asiento central** , lo que es importante porque en algunos coches este sitio no supone una plaza completa, lo que impide que se sienten dos personas más cuando ponemos la silla en él.

\-Es compacta, fácil de manejar y trasladar, lo que le da **versatilidad** para poder usarla cuando sea necesario, y guardarla en el maletero cuando se necesiten plazas libres en el coche.

\-El pasador del cinturón de seguridad tiene **pestaña de seguridad** para sacarlo, sin embargo, es muy cómodo de meter, porque sólo cede cuando se mete para que lo puedas hacer con una mano, incluso el propio niño, pero para sacarlo tienes que usar las dos, y es imposible que se salga solo. En el vídeo te lo muestro claramente.

\-La silla cabe fenomenal en el maletero, ya que no es voluminosa.

\-El **tejido es dulce** , muy agradable al tacto, de algodón **transpirable** , y los [colores para elegir los podéis ver aquí](http://amzn.to/2jqKluz), son muy bonitos. Nosotros elegimos el azul de topitos pequeños.

\-Es una **silla de coche lavable** porque es fácilmente desenfundable.

![](https://d33wubrfki0l68.cloudfront.net/e6fbb5544ce999a51eedd271d072a2e1df0660d4/48eb1/tumblr_files/tumblr_inline_okcl2yc2js1qfhdaz_540.png)

Éstas son las razones por las que elegimos la silla de retención infantil Rodifix Air Protect de Bebé Confort.

Ya te contaré qué tal nos va con ella.

[![](/images/uploads/britax-romer3.jpg)](https://www.amazon.es/Britax-R%C3%B6mer-Kidfix-Isofix-Cosmos/dp/B079ZNY629/ref=sr_1_3_sspa?keywords=silla%2Bde%2Bcoche%2Bgrupo%2B2-3&qid=1643296599&sprefix=silla%2Bde%2Bcoche%2Caps%2C815&sr=8-3-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFZVlhPWDZFUTdXOTgmZW5jcnlwdGVkSWQ9QTA5NzM5MTYxTkFJQlZGNEROVVpNJmVuY3J5cHRlZEFkSWQ9QTA1MjM5MDExU1RSVE4zWExDWk1GJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Britax-R%C3%B6mer-Kidfix-Isofix-Cosmos/dp/B079ZNY629/ref=sr_1_3_sspa?keywords=silla%2Bde%2Bcoche%2Bgrupo%2B2-3&qid=1643296599&sprefix=silla%2Bde%2Bcoche%2Caps%2C815&sr=8-3-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFZVlhPWDZFUTdXOTgmZW5jcnlwdGVkSWQ9QTA5NzM5MTYxTkFJQlZGNEROVVpNJmVuY3J5cHRlZEFkSWQ9QTA1MjM5MDExU1RSVE4zWExDWk1GJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21)

Por el momento hemos salvado la necesidad de comprar un coche nuevo, que no es poco!

Más info: 

[Qué grupo de silla necesita mi peque](https://madrescabreadas.com/2022/01/31/como-elegir-silla-de-auto-para-mi-hijo/).

[Sillas de coche para ir a contra marcha el mayor tiempo posible](https://madrescabreadas.com/2022/01/19/mejores-sillas-auto-contramarcha/)

[Mitos sobre que los niños viajen a contra marcha](https://madrescabreadas.com/2014/11/12/sillas-automovil-seguridad-ninos/)



*ACTUALIZACIÓN mayo 2021*

Mis hijos ya son mayores y no necesitan silla para coche grupo 3, ni ningún sistema de retención para automóvil. **Seguimos con el mismo coche que teníamos cuando redacté este post** porque no necesitamos cambiar a uno más grande, ya que nos apañamos con las sillas que teníamos, gracias a que encontré una de las [sillas de auto más estrechas del mercado](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/).