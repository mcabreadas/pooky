---
layout: post
title: La receta de mi abuela del jabón casero
date: 2013-11-08T09:00:00+01:00
image: /tumblr_files/tumblr_inline_p7woo4feGu1qfhdaz_540.jpg
author: maria
tags:
  - trucos
  - recetas
tumblr_url: https://madrescabreadas.com/post/66356186901/la-salita-de-la-abuela-jabón-casero
---
Hoy comparto con nosotros la receta del [jabón casero](https://amzn.to/2BVir5a) que tradicionalmente se ha venido haciendo en el pueblo de mi abuela, a quien recuerdo cortando el jabón en el cajón de madera, y del que todavía conservo algún trozo del que hizo en su día.

## Receta del jabón casero

**Dificultad**

Media-alta

### Ingredientes del jabón casero

\-1 Kg de sosa cáustica (precaución, producto abrasivo. Manipular con guantes, mascarilla y siempre al aire libre)

\-6 L de agua

\-5 L de aceite usado(sisas freidora no tardarás en reunir esta cantidad)

### Material necesario para hacer jabón casero

\-1 cajón de madera

\-1 trozo de sábana de algodón vieja o similar

\-1 palo largo de madera para remover

\-1 cuchillo

(También existen [juegos para elaborar jabones caseros](https://amzn.to/2BX8h4a),que quizá os puedan servir si queréis hacerlos en plan mono).

### Modo de hacer jabón casero

* Se disuelve la sosa cáustica en el agua por la mañana removiendo de vez en cuando por la tarde, se añade el aceite poco a poco sin dejar de remover hasta que la mezcla se quede con la textura de la masa de un bizcocho, más bien espesa.
* Esta pasta se vierte en el cajón forrado por dentro con la tela de algodón.
* Al día siguiente se comprueba la dureza. Debe estar como la mantequilla que lleva fuera del frigorífico 5 minutos.
* Entonces se parte con un cuchillo grande en trozos del tamaño y forma que nos guste. Se pueden utilizar moldes metálicos de figuras. Yo usé unos con formas de flor, estrella, corazón…
* Se separan los trozos para que sequen durante una semana. Y ya está listo para usar.

**Muy importante**

Se debe hacer al aire libre, con una mascarilla puesta, y guantes.

**Consejos**

Debe secar en un lugar cubierto por si llueve.

La mejor época del año para hacerlo es el invierno, puesto que las altas temperaturas del verano pueden hacer que “se corte”

**Con aroma**

Si añades a la mezcla inicial agua previamente hervida con lavanda, espliego o romero… o con la esencia que prefieras, el jabón tomará esos olores.

[![](/images/uploads/simply-vedic.jpg)](https://www.amazon.es/Simply-Vedic-limoncillo-esencial-terapéutico/dp/B07TVYLXSM/ref=sr_1_1?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+terapeutico&qid=1637778489&qsid=258-1528510-3213034&sr=8-1&sres=B07TVYLXSM%2CB07TX27D2G%2CB086RHPPWN%2CB0971YWNM5%2CB08W87P9RN%2CB07TX286WT%2CB081QTGTP9%2CB08W8WC8LP%2CB08W9W8QZC%2CB08W8BK2TR%2CB08WB21TZ2%2CB08W8JKTN4%2CB08W9F56SZ%2CB07VDY8T8S%2CB071K3XNRX%2CB08W9BT3CC%2CB07TTXY2TN%2CB08NYR6WWR%2CB07TVYMJTD%2CB07L7653S2&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Simply-Vedic-limoncillo-esencial-terapéutico/dp/B07TVYLXSM/ref=sr_1_1?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+terapeutico&qid=1637778489&qsid=258-1528510-3213034&sr=8-1&sres=B07TVYLXSM%2CB07TX27D2G%2CB086RHPPWN%2CB0971YWNM5%2CB08W87P9RN%2CB07TX286WT%2CB081QTGTP9%2CB08W8WC8LP%2CB08W9W8QZC%2CB08W8BK2TR%2CB08WB21TZ2%2CB08W8JKTN4%2CB08W9F56SZ%2CB07VDY8T8S%2CB071K3XNRX%2CB08W9BT3CC%2CB07TTXY2TN%2CB08NYR6WWR%2CB07TVYMJTD%2CB07L7653S2&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

## Posibles usos del jabón casero

* **Para lavadora:** Bien rallándolo en virutas o en forma líquida. Yo suelo preparar varios bidones para mí y para mis hijas disolviendo algunas pastillas en agua caliente y agitando. Se consigue un blanco espectacular en la ropa. Eso sí, recomiendo usarlo sólo para coladas de ropa blanca, no de color.
  [Receta del jabón casero para lavadora aquí](https://madrescabreadas.com/2022/01/14/jabon-casero-para-lavadora/).

[![](/images/uploads/lagarto-escamas.jpg)](https://www.amazon.es/Lagarto-Escamas-Hipoalergénicas-400-Gr/dp/B08CHXGNJG/ref=pd_sbs_4/261-4500694-6173210?pd_rd_w=jxjmw&pf_rd_p=20680f7e-bc7f-4504-a338-63dbdaa26d7f&pf_rd_r=00455H2M2Z4T7WSD4A36&pd_rd_r=354a74ff-8b5c-436d-aaab-493fa1d9fb4c&pd_rd_wg=chX7o&pd_rd_i=B08CHXGNJG&psc=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Lagarto-Escamas-Hipoalergénicas-400-Gr/dp/B08CHXGNJG/ref=pd_sbs_4/261-4500694-6173210?pd_rd_w=jxjmw&pf_rd_p=20680f7e-bc7f-4504-a338-63dbdaa26d7f&pf_rd_r=00455H2M2Z4T7WSD4A36&pd_rd_r=354a74ff-8b5c-436d-aaab-493fa1d9fb4c&pd_rd_wg=chX7o&pd_rd_i=B08CHXGNJG&psc=1&madrescabread-21)

* **Para quitar las manchas** rebeldes de la ropa antes de meterla en la lavadora en su modalidad de pastilla. Yo dejo para esto los bloques que me han quedado más feos o sin forma.
* **Para lavar las manos**. Las formas más bonitas las pongo en las jaboneras de casa o las regalo a mis amigas.
* **Como ambientador** para armarios y cajones metidos en bolsitas de tela.
* **Para lavar rasguños** antes de poner el betadine.
* **Para infecciones vaginales** leves o irritaciones.
* Para problemas en el cutis como el **acné, atenuar líneas faciales o cicatrices**. Pueden ser **elaborados con avena, rosa mosqueta** u otros ingredientes naturales. Aportan suavidad en el rostro y unifican el tono de la piel.

[![](/images/uploads/lida-jabon-100.jpg)](https://www.amazon.es/LIDA-NATURAL-GLICERINA-MOSQUETA-piezas/dp/B00V6AR9NE/ref=sr_1_7?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+artesanal&qid=1637777892&qsid=258-1528510-3213034&sr=8-7&sres=B001DWAIEW%2CB006G84ILC%2C8441540543%2CB00CSLQTS6%2CB00V6AR9NE%2CB00V6ASF0A%2CB073Q79JLZ%2C8498742773%2CB07HV2YDPG%2CB01MS4ASVG%2CB09BCLJYT7%2CB0855QGH5K%2CB01LZ8CJ6E%2CB0971YWNM5%2CB01F7MWXAU%2CB00V6AS2I0%2CB00V6ARYPM%2CB00V6AS9F6%2C8418252375%2CB07T2QJDPZ&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/LIDA-NATURAL-GLICERINA-MOSQUETA-piezas/dp/B00V6AR9NE/ref=sr_1_7?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+artesanal&qid=1637777892&qsid=258-1528510-3213034&sr=8-7&sres=B001DWAIEW%2CB006G84ILC%2C8441540543%2CB00CSLQTS6%2CB00V6AR9NE%2CB00V6ASF0A%2CB073Q79JLZ%2C8498742773%2CB07HV2YDPG%2CB01MS4ASVG%2CB09BCLJYT7%2CB0855QGH5K%2CB01LZ8CJ6E%2CB0971YWNM5%2CB01F7MWXAU%2CB00V6AS2I0%2CB00V6ARYPM%2CB00V6AS9F6%2C8418252375%2CB07T2QJDPZ&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

\-Para la **psoriasis, los hongos o las espinillas**

[![](/images/uploads/gel-de-ducha.jpg)](https://www.amazon.es/Hidratante-Terapéutico-concentrado-Esencial-Purificante/dp/B071K3XNRX/ref=sr_1_15?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+terapeutico&qid=1637778489&qsid=258-1528510-3213034&sr=8-15&sres=B07TVYLXSM%2CB07TX27D2G%2CB086RHPPWN%2CB0971YWNM5%2CB08W87P9RN%2CB07TX286WT%2CB081QTGTP9%2CB08W8WC8LP%2CB08W9W8QZC%2CB08W8BK2TR%2CB08WB21TZ2%2CB08W8JKTN4%2CB08W9F56SZ%2CB07VDY8T8S%2CB071K3XNRX%2CB08W9BT3CC%2CB07TTXY2TN%2CB08NYR6WWR%2CB07TVYMJTD%2CB07L7653S2&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Hidratante-Terapéutico-concentrado-Esencial-Purificante/dp/B071K3XNRX/ref=sr_1_15?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+terapeutico&qid=1637778489&qsid=258-1528510-3213034&sr=8-15&sres=B07TVYLXSM%2CB07TX27D2G%2CB086RHPPWN%2CB0971YWNM5%2CB08W87P9RN%2CB07TX286WT%2CB081QTGTP9%2CB08W8WC8LP%2CB08W9W8QZC%2CB08W8BK2TR%2CB08WB21TZ2%2CB08W8JKTN4%2CB08W9F56SZ%2CB07VDY8T8S%2CB071K3XNRX%2CB08W9BT3CC%2CB07TTXY2TN%2CB08NYR6WWR%2CB07TVYMJTD%2CB07L7653S2&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

\-Si tienes una **piel muy seca**, te vendrán bien jabones caseros con aceite de almendras, oliva o glicerina. **Calman las irritaciones de la piel** y combaten el envejecimiento prematuro.

[![](/images/uploads/le-petit-marseillais.jpg)](https://www.amazon.es/Petit-Marseillais-almendras-parabenos-Provenza/dp/B07ZZFXFMH/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+con+aceite+de+almendras&qid=1637778911&qsid=258-1528510-3213034&sr=8-4&sres=B08VHS3J57%2CB08WMV4WLV%2CB00B4RA09E%2CB07ZZFXFMH%2CB07WMVGD7L%2CB08SWM2V1H%2CB00FPX4QQG%2CB06Y2T66QK%2CB00DN9NQHE%2CB094K45M4Q%2CB097QVBHDK%2CB00XAIRWDW%2CB07GVPBNHY%2CB0741G5NPK%2CB00XJP6XT0%2CB076QF3LXX%2CB003XU5GJY%2CB07HQMS6WZ%2CB00J5FY4PS%2CB07QF7KM6J&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Petit-Marseillais-almendras-parabenos-Provenza/dp/B07ZZFXFMH/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&keywords=jabón+con+aceite+de+almendras&qid=1637778911&qsid=258-1528510-3213034&sr=8-4&sres=B08VHS3J57%2CB08WMV4WLV%2CB00B4RA09E%2CB07ZZFXFMH%2CB07WMVGD7L%2CB08SWM2V1H%2CB00FPX4QQG%2CB06Y2T66QK%2CB00DN9NQHE%2CB094K45M4Q%2CB097QVBHDK%2CB00XAIRWDW%2CB07GVPBNHY%2CB0741G5NPK%2CB00XJP6XT0%2CB076QF3LXX%2CB003XU5GJY%2CB07HQMS6WZ%2CB00J5FY4PS%2CB07QF7KM6J&srpt=SKIN_CLEANING_AGENT&madrescabread-21)

## Curiosidades del jabón casero

Como **curiosidad** os contaré que en el pueblo de mi madre, aún hay mujeres que sólo usan el jabón hecho por ellas para todo esto que os he contado y más. Hay casos en los que, no sólo no han comprado en su vida detergente para la lavadora ni ningún tipo de jabón para manos, ni cuerpo, sino que presumen de lavarse la cara todos los días con jabón casero y de no haber visitado nunca un dermatólogo.

Hay muchos [tipos de jabón casero](https://madrescabreadas.com/2021/12/16/tipos-de-jabones-artesanales-y-sus-beneficios/) que se adaptan a cada tipo de necesidad. Sólo tienes que encontrar el tuyo.

¿Has usado alguna vez jabón casero? ¿Lo recomiendas?

Comparte para ayudar a más gente y suscríbete para no perderte las novedades