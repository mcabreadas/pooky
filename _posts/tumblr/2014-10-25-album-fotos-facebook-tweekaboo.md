---
date: '2014-10-25T09:37:25+02:00'
image: /tumblr_files/tumblr_inline_petodacX6m1qfhdaz_540.jpg
layout: post
tags:
- trucos
- colaboraciones
- gadgets
- ad
- tecnologia
- recomendaciones
title: Transforma tu Facebook en álbum de fotos con Tweekaboo
tumblr_url: https://madrescabreadas.com/post/100893355368/album-fotos-facebook-tweekaboo
---

¿No os ha pasado que los abuelos de vuestros hijos siempre se quejan de que no tienen fotos en papel de los niños, y que las tienen que ver siempre en el ordenador o en el móvil?. Las fotos digitales están muy bien, pero a veces se echa de menos pasar las hojas de un álbum, sentir su tacto.

Pero si os pasa como a mí, nunca encontráis tiempo para cumplir la promesa de: “mamá, no te preocupes que te voy a hacer un álbum con las fotos de los peques para que lo veas cuando tú quieras”. Por eso me encantó la idea de este papá para ponernos fácil pasar las fotos de Facebook a un álbum de papel, incluídos los comentarios que han ido dejando familiares y amigos.

Me parece un bonito recuerdo!

[![image](/tumblr_files/tumblr_inline_petodacX6m1qfhdaz_540.jpg)](https://www.tweekaboo.com/import)

Tweekaboo es una empresa creada por un padre de familia que conoce la importancia de cada momento que pasamos con nuestros hijos, por eso decidió idear una aplicación muy práctica que permite convertir todos los posts sobre tus peques en Facebook (incluyendo también los comentarios de amigos y familiares) en un álbum en cuestión de minutos.

También podéis importar fotos desde vuestro ordenador e ir combinándolas con las de Facebook a vuestro gusto, añadir comentarios…En fin, lo que queráis.

 ![image](/tumblr_files/tumblr_inline_petoda5hZ61qfhdaz_540.jpg)

Podéis bajaros la App de Tweeaboo para móvil [aquí](https://itunes.apple.com/us/app/tweekaboo-your-childs-life/id440682407?mt=8) 

Y podéis registraros en Tweekaboo desde el PC [aquí](https://www.tweekaboo.com/import).

Espero que lo paséis tan bien como yo mientras montaba mi álbum. 

_NOTA: Este post NO es patrocinado_