---
date: '2018-02-04T13:00:27+01:00'
image: /tumblr_files/tumblr_inline_p3mj380GLF1qfhdaz_540.png
layout: post
tags:
- nutricion
- crianza
title: 'Alimentación infantil a golpe de tweet: IV Blog Trip Puleva'
tumblr_url: https://madrescabreadas.com/post/170491439164/nutricion-infantil-blog-trip-puleva
---

De nuevo tuve la oportunidad de viajar a Granada gracias a [Puleva](https://www.lechepuleva.es/) quien, un año más tuvo la gentileza de invitarme a su Blog Trip sobre nutrición infantil, pero esta vez, además de como blogger asistente, participé como ponente en un taller de comunicación digital, lo que fue una experiencia maravillosa.

 ![](/tumblr_files/tumblr_inline_p3mj380GLF1qfhdaz_540.png)

Ya os conté que en la edición del [Blog Trip Puleva Infantil 2016](/2016/11/13/nutricion-infantil-leche.html) surgió un debate legal sobre la responsabilidad que tenemos como comunicadoras quienes compartimos contenido por internet a través de las redes sociales o los blogs. Pues este punto dio lugar a mi ponencia de esta edición, la cual suscitó un gran interés entre mis compañeros blogueros y blogueras.

Ya sabéis que cuando asisto a alguna charla interesante, suelo sintetizar lo que se va contando en tiempo real y tuitearlo. Es algo que me encanta, y que me ayuda a ir ordenando y asimilando la información.

Así que se me ha ocurrido contaros la increíble vivencia del IV Blog Trip Puleva Infantil a golpe de tweet.

![](/tumblr_files/tumblr_inline_p6m7ts9wJi1qfhdaz_540.gif)

Espero que lo disfrutéis!

<iframe src="//storify.com/madrescabreadas/el-blog-trip-puleva-infantil-a-golpe-de-tweet/embed?border=false" width="100%" height="750" frameborder="no" allowtransparency="true"></iframe><script src="//storify.com/madrescabreadas/el-blog-trip-puleva-infantil-a-golpe-de-tweet.js?border=false"></script><noscript>[<a href="//storify.com/madrescabreadas/el-blog-trip-puleva-infantil-a-golpe-de-tweet" target="_blank">View the story “El Blog Trip Puleva Infantil a golpe de tweet” on Storify</a>]</noscript>

Hasta aquí la parte académica. 

Ahora os resumo en imágenes lo bien que lo pasamos y las increíbles compañeras con las que coincidí (algunas las conocía virtualmente desde hace años, y fue muy emocionante poder abrazarlas por fin).

 ![](/tumblr_files/tumblr_inline_p3mj14cdjp1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p3mjizVgl41qfhdaz_540.png)

El paseo nocturno por los alrededores de la Alhambra con un frío que pelaba fue de lo más divertido, sobre todo por la compañía de mis compañeras de los blogs [Trucos de Mamás](http://trucosdemamas.com/) y [Ni Blog ni Bloga](https://niblognibloga.wordpress.com/)

 ![](/tumblr_files/tumblr_inline_p3mj4qkFNv1qfhdaz_540.png)

Quiero agradeces a todo el equipo de [Puleva](https://www.lechepuleva.es/innovacion) el magnífico trato y el cariño con el que prepararon todo. Se nota cuando las cosas se hacen con corazón y atendiendo al detalle.