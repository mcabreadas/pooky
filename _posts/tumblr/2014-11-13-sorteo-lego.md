---
date: '2014-11-13T16:00:00+01:00'
image: /tumblr_files/tumblr_inline_petodaOTFS1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- premios
- familia
- juguetes
title: 'II Sorteo aniversario: set de LEGO Guardianes de la Galaxia'
tumblr_url: https://madrescabreadas.com/post/102531881932/sorteo-lego
---

Seguimos de fiesta y vamos con el segundo sorteo de la serie de [sorteos de aniversario](/portada-aniversario), que tendrá lugar a lo largo del mes porque 4 años juntos es mucho tiempo y merece una gran celebración, por eso [LEGO](http://www.lego.com/es-es) ha querido acompañarnos en esta fecha tan especial cediéndonos este set de construcción de Guardianes de la Galaxia.

 ![image](/tumblr_files/tumblr_inline_petodaOTFS1qfhdaz_540.jpg)

¿A que mola?

Si vuestro hijo tiene 6 años o más, os aseguro que va a disfrutar de lo lindo con él. Y si no habéis visto la peli os la recomiendo 100% porque a mis fieras les encantó.

Para participar

-Dejar un comentario en este post diciendo que queréis participar, y vuestro nick de Facebook o Twitter.

Y si queréis, aunque no es obligatorio, pero así estaréis al día del resultado del sorteo y de los próximos que vaya sacando a lo largo del mes, podéis suscribiros al blog. Para ello sólo tenéis que introducir vuestro email en el margen derecho.

El sorteo es válido para territorio español, y el plazo acabaría el 20 de noviembre a las 00:00 h.

ACTUALIZACIÓN 21-11-14

 ![](/tumblr_files/tumblr_inline_petodasYtj1qfhdaz_540.jpg)

Enhorabuena a la ganadora, Olga Montero!! Que, además, me hace especial ilusión porque es seguidora activa del blog.