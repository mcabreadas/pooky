---
layout: post
title: El arte de emocionarte en familia
date: 2016-08-28T18:36:32+02:00
image: /images/uploads/mq1-2.jpg
author: .
tags:
  - crianza
  - libros
  - planninos
tumblr_url: https://madrescabreadas.com/post/149607311654/libro-arte-emocionarte
---
Este veranos hemos leído varios libros interesantes, pero quería empezar con uno que nos ha servido para redescubrir las emociones que solemos sentir, identificarlas, manejarlas y tratarlas de una forma positiva, se trata de [El arte de emocionarte](https://amzn.to/43q2ifN).\
Normalmente hago las reseñas de los libros que nos gustan a mis hijos y a mí por escrito, pero esta vez he querido probar a contártelo a través de un vídeo porque quería enseñarte su formato, sus ilustraciones y su estructura, porque creo que te va a llegar mejor de una forma más visual. Además, así conoces mi voz.


<iframe width="560" height="315" src="https://www.youtube.com/embed/UT9zhZSb0X4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

¡Si me vais diciendo que os gusta esta forma de contaros cosas quizá me anime repita!  

[El arte de emocionarte](https://amzn.to/43q2ifN) de la colección Nube de Tinta, escrito por Cristina Núñez Pereira y Rafael R. Valvárcel, es una especie de guía de emociones, que te enseñará a ser más feliz.  
Muy recomendable su lectura en familia, ya que tiene actividades muy divertidas, anécdotas, incluso sugiere una película para cada emoción.  
La inteligencia emocional es un aspecto crucial en la educación de nuestros hijos.  
No la descuidemos!