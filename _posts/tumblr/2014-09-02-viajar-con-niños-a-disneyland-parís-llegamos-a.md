---
date: '2014-09-02T10:11:00+02:00'
image: /tumblr_files/tumblr_nb8so4xfaY1qgfaqto1_640.jpg
layout: post
tags:
- vacaciones
- planninos
- familia
title: viajar con niños a disneyland parís llegamos a
tumblr_url: https://madrescabreadas.com/post/96438410679/viajar-con-niños-a-disneyland-parís-llegamos-a
---

![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto1_640.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto6_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto7_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto9_1280.jpg)  
 ![](/tumblr_files/tumblr_nb8so4xfaY1qgfaqto10_1280.jpg)  
  

## Viajar con niños a Disneyland París

Llegamos a Disneyland París tras atravesar España y Francia de sur a norte por carretera con nuestros tres niños visitando [Coilloure](/post/95916703759/excursion-ninos-coilloure-francia), [Carcasonne](/post/96065174784/viajar-con-ninos-ciudad-medieval-de-carcasonne) y la ciudad de [París](/post/96190020274/viajer-ninos-paris).

Estamos agotados, pero la ilusión en sus caritas lo compensa todo.  
 Mi niña fue una princesa de cuento y mi niño un auténtico Yedi.  
 Nos encantaron las atracciones, ninguna brusca o estridente, y muchas donde poder subir al bebé, lo que agradecimos enormemente porque nos permitió pasar momentos muy divertidos todos juntos en familia.  
 A Princesita la que más le gusto fue la casa encantada, donde la decoración y los efectos eran alucinantes, pero sin sustos (Leopardito casi se duerme), y también el “mundo pequeño” porque íbamos en barca recorriendo distintas partes del mundo.

Y a Osito le entusiasmó el “Tren de la mina”, al que se subió solo con su padre, lo que le hizo especial ilusión.

Todos fuimos Piratas del Caribe por unos minutos, visitamos la cabaña de Robinson Crusoe, aniquilamos a los extraterrestres con Buzz Lighyear, nos lanzaron al espacio en un cohete de Star Wars, los peques condujeron los coches de Cars por una pista de carreras, nos introdujimos de lleno en la magia de los cuentos de Blancanieves y Pinocho, volamos en la alfombra mágica de Aladin…ç

Pero lo que sin duda quedará en nuestras retinas para siempre es el espectáculo de luces y fuegos artificiales de fin de día donde soñamos despiertos y volvimos a la infancia. Lloré como una niña emocionada con la música y los personajes de las películas con los que crecí y, lo reconozco, fue imposible no sucumbir a la magia de Disney.