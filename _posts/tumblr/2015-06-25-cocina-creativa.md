---
date: '2015-06-25T15:37:14+02:00'
image: /tumblr_files/tumblr_inline_nqi507dOYE1qfhdaz_540.jpg
layout: post
tags:
- recetas
- eventos
- trucos
title: Cocina creativa con ingredientes de Olivolea
tumblr_url: https://madrescabreadas.com/post/122419058219/cocina-creativa
---

Si leyéramos los ingredientes de un producto sin saber lo que es, normalmente sabríamos enseguida de qué se trata, ¿verdad?

Pero si leyéramos éstos: “tomate, rosa mosqueta, caléndula, aceite de oliva” quizá dudaríamos entre algo comestible o un  cosmético.

 ![image](/tumblr_files/tumblr_inline_nqi507dOYE1qfhdaz_540.jpg)

Mi embajadora en el taller de cocina creativa organizado por las [cremas Olivolea](http://olivolea.es/es), Mónica Marcos, del blog Quica & Kids, nos descubre cómo es posible cocinar con los ingredientes de una crema de la mano del gran chef  [Juan Pozuelo](http://www.juanpozuelo.com/):

## Por qué un taller de cocina creativa

“El pasado martes 22 de junio confirmé que todo es posible en esta vida. Tuve la oportunidad de asistir, como embajadora del blog [Madres Cabreadas](/),  al taller de cocina creativa que organizó la firma Olivolea en el estudio [Bulthaup](http://www.bulthaup.es/), en Madrid, junto con el famosísimo chef Juan Pozuelo.

Allí, Belen Castro, creadora de Olivolea - licenciada en Farmacia, especialista en dermofarmacia y nutrición- tuvo la gentiliza de dar comienzo el acto contándonos su gran pasión por el aceite, por su profesión y por la cocina. 

Tras enterarse de que iba a ser mamá comenzó a estudiar cómo podía proteger su piel y la de su futura hija. 

Su piel en esta etapa iba a mostrarse especialmente sensible y vulnerable. La piel de la pequeña sería tan fina, tierna y delicada que necesitaría un cuidado especial. 

Prevenir las estrías, proteger la piel del pezón en la lactancia, cuidar la piel de su bebé… “Lo más prudente sería hacerlo con cosméticos naturales,  ya que se produce una mayor penetración de los activos cuanto más parecidos sean al manto hidrolipídico”.

 Con esa idea comienza su trabajo de investigación y el resultado aquí lo tienes. Una gama de cinco productos:

**Olivolea familiar** está indicada para hidratar, nutrir y cuidar la piel de la familia.

**Olivolea crema hidratante cara y cuerpo para el bebé** está orientada para hidratar, nutrir y cuidar la piel del bebé.

**Olivolea Bálsamo regenerante para el bebé**. Aconsejado para ayudar a calmar las irritaciones de la piel en la zona del culito del bebé.

**Olivolea Crema reafirmante antiestrías** , especialmente indicada en la etapa del embarazo y la lactancia.

 ![image](/tumblr_files/tumblr_inline_nqi49cTOyp1qfhdaz_540.jpg)

**Licotriz** crema regeneradora intensiva. Orientada para evitar la formación de costras e irregularidades en la piel y alisa las cicatrices.

 ![image](/tumblr_files/tumblr_inline_nqi3wyes571qfhdaz_540.png)
## Recetas con ingredientes de las cremas

Ahora sí, comenzaba lo que a mí me tenía en vilo. Juan Pozuelo iniciaba su presentación con una pregunta “¿Es posible cocinar con los ingredientes de un cosmético? Tras algunas risotadas nos desveló que en el caso de los cosméticos elaborados por Belén Castro era posible.

Algunos de los ingredientes de las cremas Olivolea y Lecotriz son el aceite de oliva, la caléndula, la rosa mosqueta, el bisabolol, la hamamelis o el licopeno de tomate. Pues con algunas de esas sustancias elaboró dos entrantes que estaban de rechupete:

Primero nos enseñó a elaborar unas  migas de pan tostadas caramelizadas con esferificación de manzanilla y de esencia de rosas.

 ![image](/tumblr_files/tumblr_inline_nqi4o5LoFw1qfhdaz_540.jpg)

Y después creó una versión diferente de pan con tomate y jamón con una emulsión de hierbas, tomate y puntas de jamón para sorprendernos con una original tapa.

 ![image](/tumblr_files/tumblr_inline_nqi4ra5xsB1qfhdaz_540.jpg)

Y éste fue el resultado:

 ![image](/tumblr_files/tumblr_inline_nqgka2nxDd1qfhdaz_540.png)

Para finalizar el encuentro tuve el placer de hacer un poco de networking con los asistentes mientras disfrutábamos de un desayuno elaborado por [El Patio de Acuña](http://www.patiodeac.com/). ¡Espectacular el bizcocho de rosas!

 ![image](/tumblr_files/tumblr_inline_nqi4w5HziL1qfhdaz_540.jpg)

Quisiera agradecerte, María, la oportunidad que me has concedido de poder asistir, en tu nombre, a este evento y colaborar contigo.¡ Muuuuuak!”

Muchas gracias a ti, Mónica, por asistir al evento y contarnos cosas tan interesantes.

## **ACTUALIZACIÓN 8-7-15:**

Juan Pozuelo ha tenido la amabilidad de dejarnos las recetas de las tapas que cocinó en el cocina, y [Olivolea las ha publicado en su blog](http://www.olivolea.es/es/blog/las-recetas-de-juan-pozuelo-para-olivolea).

Con su permiso, os las dejo, por si os atrevéis ha hacerlas. ya me contaréis!

## Crumble de aceite de oliva y caviar de rosas y manzanilla

**Crumble**

Ingredientes: 100 grs de harina, 40 grs de mantequilla, 10 grs de azúcar morena, 10 grs de aceite de oliva.

Trabajamos la mantequilla a punto de pomada y añadimos el resto de ingredientes formando una pasta arenosa.

Extendemos sobre placa de horno y dejamos secar a 170 grados durante 10 minutos.

Dejamos enfriar y terminamos de romper en migas.

**Caviar**

Ingredientes: Manzanilla y esencia de rosas, agua, miel, kits de esferificacion, goma xantana.

Preparamos dos infusiones con el grado de concentración que deseemos, en el caso de la de rosas seguiremos las indicaciones de la esencia que hayamos conseguido.

Ponemos las infusiones a punto de dulzor y procedemos según las instrucciones del kit de esferificacion –los productos de las marca Sosa nos indican para cada tipo de producto la dosificación y proceso-.

## Pan con tomate y jamón

Ingredientes: Panes de tomate -valen molletes pequeños-, 1 kilo de tomates, 100 grs de puntas de jamón, 6 hojas de gelatina, sifón y cargas.

Trituramos los tomates hasta hacer una pasta muy fina y colocamos en colador con servilleta de tela o papel y dejamos filtrar toda la noche.

Ponemos las puntas de jamón en ½ litro de agua y dejamos calentar durante 40 minutos sin que llegue a hervir. Tapamos y dejamos reposar también toda la noche. Colamos.

Calentamos el del jamón y fundimos la gelatina.

Juntamos los dos líquidos hasta obtener 800 ml, aproximadamente 600 de tomate y 200 de jamón.

Rellenamos el sifón y le colocamos la carga. Dejamos reposar una hora.

Vaciamos los panes y rellenamos con la espuma de tomate y jamón.

¿Os atrevéis a intentarlo?