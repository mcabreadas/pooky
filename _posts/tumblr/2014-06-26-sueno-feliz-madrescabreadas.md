---
date: '2014-06-26T10:19:00+02:00'
image: /tumblr_files/tumblr_inline_mhapsdk2vN1qz4rgp.jpg
layout: post
tags:
- crianza
- participoen
- colaboraciones
- ad
- familia
- colecho
- bebes
title: 'Día del Sueño Feliz. El colecho interactivo. #DuérmeteConmigo'
tumblr_url: https://madrescabreadas.com/post/89945787629/sueno-feliz-madrescabreadas
---

Otro año más participo en esta iniciativa. Y de nuevo mi protagonista es el colecho, pero no como os lo imagináis.

Ya os conté el año pasado [cómo hacer una cuna sidecar a partir de una de Ikea](/post/41598512975/convierte-una-cuna-de-ikea-en-cuna-de-colecho), ideal para bebés pequeñitos, pero cuando crecen hay que adaptarse a ellos, así que os voy a contar un nuevo concepto de sueño feliz inventado por mi Leopardito, y llamado por mí misma “colecho interactivo”.

Nuestra odisea con el sueño de mi fierecilla (23 meses ya) ha sido tremenda porque lo hemos probado todo, oiga… menos el método Estivill, que ya tendrá tiempo de sufrir en la vida el pobrecito mío.

Al principio, obviamente, las culpas de que no durmiera “bien” se las llevó todas la lactancia materna y el [colecho en cuna sidecar](/post/41598512975/convierte-una-cuna-de-ikea-en-cuna-de-colecho). Escuchábamos a menudo frases como éstas:

**“claro… es que sabe mucho, ese lo que está es viciado con la teta”**.

[![image](/tumblr_files/tumblr_inline_mhapsdk2vN1qz4rgp.jpg)](/post/41598512975/convierte-una-cuna-de-ikea-en-cuna-de-colecho "cómo hacer una cuna de coclecho con una de ikea")

El [reflujo](/2015/11/20/reflujo-bebes.html) también llevó su parte de mérito (aunque en este caso sí que es verdad que tuvo mucha culpa, las cosas como sean).

Luego, lo [destetamos](/post/72744260791/destete-una-experiencia-y-una-vision-profesional) pero seguía despertándose a menudo… Vaya! Entonces va a ser que no era por tomar pecho!.

Y cuando empezó a mejorar de lo suyo y empezaron a acabársenos las excusas decidimos que es que puede que le molestaran nuestros ronquidos y lo trasladamos de nuestro dormitorio al de su hermano Osito, que duerme como lo que es, un pequeño oso, y que aguantó estoicamente las juergas nocturnas de su hermano pequeño, con trasiegos de papá y mamá por la habitación cada dos por tres para mecerlo, darle agua, leche, cantarle, cambiarle el pañal, sacarle los mocos… qué os voy a contar…

Así estuvo unos meses Leopardito, haciendo de las suyas sin dormir él ni los demás. Hasta establecimos unos turnos de guardia su padre y yo para poder descansar por lo menos 4 horas del tirón cada uno, el día que había suerte.

Pero hace cosa de tres meses me cansé de las idas y venidas a la habitación de Osito, de dormir a trompicones, de no estar con mi marido por las noches, y de un colchón taciturno de usar por turnos, y pusimos una camita de IKEA junto a la nuestra, heredada de Osito, entre la pared y nuestro canapé que, aunque quede más baja no es problema porque el bebé sube y baja de la suya a la nuestra a su antojo durante la noche, se acomoda y se duerme donde le da la gana por su cuenta y nos deja descansar porque no llora al tenernos con él.

 ![image](/tumblr_files/tumblr_n40ss59R031qgfaqto1_500.jpg)

Él elige con quién se acurruca o si le apetece estar más ancho en su espacio. Le hemos dado independencia y la posibilidad de que duerma en su propia cama cuando lo decida.

Él está encantado porque tiene una cama de “mayó”, y se lo cuenta a todo el mundo. A veces desaparece y nos lo encontramos metido en ella jugando a taparse y destaparse.

Es cierto que le sigue costando quedarse dormido, porque es un niño inquieto y muy activo y está sobrestimulado por las fieras de sus hermanos, pero hemos logrado restablecer cierta normalidad en la familia eliminando los odiosos turnos y logrando un sueño más reparador para todos.

Yo lo llamo colecho interactivo, y lo veo como un paso previo y natural a dormir en su propia cama y en su propia habitación compartida con su hermano. Creo que estamos respetando sus ritmos, pero al mismo tiempo nos permite sobrevivir y afrontar el trabajo diario descansados.

Confío en que a partir de los dos años el sueño se le irá regulando poco a poco y llegará a dormir del tirón como sus hermanos, pero mientras tanto, paciencia e imaginación al poder para buscar soluciones buenas para todos. Ah! y toneladas de amor.

¿Y tu bebé, duerme feliz?