---
layout: post
title: Cómo celebrar la Comunión sin arruinarte
date: 2015-05-19T16:26:09+02:00
image: /tumblr_files/tumblr_inline_nojmj4gr6g1qfhdaz_540.jpg
author: .
tags:
  - 6-a-12
  - comunión
  - decoracion
tumblr_url: https://madrescabreadas.com/post/119361907239/cómo-celebrar-la-comunión-sin-arruinarse
---
Recuerdo que hace un par de años me encontré con una amiga cuyo hijo tomaba la Primera Comunión ese mes, y me dio un consejo:

“¡¡Empieza a ahorrar!!”

Yo pensé para mis adentros: “qué exagerada”, pero cuando llegó el [momento de organizar la comunión de Princesita](https://madrescabreadas.com/2015/05/07/comunion/), me di cuenta de que para nada exageraba mi amiga, ya que la oferta de productos y servicios que hay en el mercado para este acontecimiento es brutal.

Empezando por el [vestido o traje de comunión](https://madrescabreadas.com/2018/05/09/vestidos-primera-comunion-nina/), zapatos [esparteñas de comunión](https://amzn.to/45st6y2), diadema o corona de comunión y demás accesorios de la comulganta y familia (en este caso, numerosa, para más inri), peluquería, libro de firmas, restaurante, las flores e la Iglesia, el coro, fotógrafo, recordatorios, decoración, tarta de comunión, regalos para los invitados, galletas decoradas, mesa dulce, monitores, hinchables, magos, payasos, lista de [regalos de Counion](https://madrescabreadas.com/2023/02/24/regalos-de-comunion-2023/), … en fin, podríamos seguir hasta el infinito y más allá...

Entonces me sentí abrumada, y decidí **celebrar la Comunión de mi hija de un modo muy especial**, pero a nuestra manera, y gastando lo menos posible, entre otras cosas, porque nos quedan dos más que celebrar en los años sucesivos…

Estos meses he aprendido mucho, y quiero compartir contigo unos consejos para que no te dejes llevar por el consumismo, y logres una celebración especial e inolvidable, pero sin gastaros una pasta gansa.

## El vestido de Comunión de niña

Nada nos podía hacer más ilusión que llevara el mismo que usé yo para mi gran día. Y no os imagináis el éxito que tuvo porque se veía diferente al resto. **Para nosotros tuvo un significado muy especial**, ya que me lo regaló mi abuela, que esta vez nos acompañó desde el cielo.

Lo lavé con [jabón casero](https://madrescabreadas.com/2022/01/14/jabon-casero-para-lavadora/) varias veces y lo dejé secar al sol para que perdiera el tono amarillento del paso de los años. Después lo llevé a planchar a y a almidonar la enagua, y quedó así de bonito.

![vestido-comunion-cuello](https://d33wubrfki0l68.cloudfront.net/16efb0c4fedcf1a956719e08f17692fb72875a71/c699f/tumblr_files/tumblr_inline_nojmj4gr6g1qfhdaz_540.jpg)

Y como me ahorré ese gasto, aproveché para ponerle una diadema hecha a mano con flores naturales preservadas de los colores que mi Princesa eligió.

Si no tenéis posibilidad de reutilizar ningún vestido o traje, siempre **podéis acudir a un outlet de vestidos de comunión** donde podréis encontrar cosas muy interesantes a precios bastantes reducidos.

## El peinado para la Comunión

Tenemos la suerte de que están de moda las melenas naturales, así que para lograr ese efecto, nada mejor que lavarse el pelo, dar unos toques de secador con cepillo, y listo. **Mi consejo es respetar el pelo de cada niña** sin que quede muy marcado.

Truco para las madres, hermanas… y demás asistentes: no decir en la pelu que vais de Comunión (shhhh…), que si no aprovechan para haceros algo especial y os clavan.

## Centros de flores para la Comunión

Lo mejor es informarse de si hay boda ese mismo día para ponerse de acuerdo con los novios y **pagar una mitad entre todos los comulgantes**, y la otra mitad los novios.

## El reportaje de fotos de la primera Comunión

Si queréis reportaje previo merece la pena pedir prestada una buena cámara. Si no tenéis, y **hacerlo vosotros mismos** porque lo vais a pasar en grande, las fotos saldrán más naturales y espontáneas, y os ahorraréis un pico.

En cuanto a la Iglesia, muchas ponen como **norma que haya un solo fotógrafo**, incluso algunas tienen ya establecida una persona y tienes que conformarte. Así que , o las hacéis de estrangis, o negociáis un buen precio para un pack básico en digital, y luego cada cual que se haga las copias en papel que quiera y donde quiera.

## El restaurante para celebrar la Comunión

**Lo más económico es hacerlo en casa** y preparar nosotros mismos la comida, eso está claro, pero para quienes no podemos hacer tanto trabajo, está la posibilidad de contratar un catering o ir a un restaurante (creo que no hay mucha diferencia de precio entre estas dos opciones).

Huye de los sitios céntricos o más famosos donde todo el mundo celebra las comuniones porque son más caros, están más saturados y seguramente no tengan tanto encanto como uno un poco alejado de la ciudad y con un toque especial. **Salte de lo normal y atrévete con algo diferente.**

Nosotros tuvimos la suerte de disfrutar de una comida deliciosa y una tarde en familia al aire libre en pleno corazón de la huerta.

![restaurante-huerta-murcia](https://d33wubrfki0l68.cloudfront.net/3a705da6b0f0e53a4292cc5c8700841f9bcc4865/dbc99/tumblr_files/tumblr_inline_nojzlmeemr1qfhdaz_540.jpg)

## La decoración de la mesa de los niños de la Comunión

Lo mejor es que la hagáis con vuestros hijos y a gusto de ellos. Tomáoslo con tiempo, un par de meses más o menos, y dedicadle un rato cada fin de semana.

**Pedidles que elijan dos o tres tonos de color**, y no os salgáis de ellos. En nuestro caso fueron el rosa y verde pálido y el blanco.

Pinterest será vuestro mejor amigo, porque todo lo que podáis soñar está allí, y además con tutorial.

Os dejo el [tablero que creé con todas las manualidades](https://www.pinterest.com/madrescabreadas/comuni%C3%B3n/) que hicimos, y algunas más que no nos dio tiempo a hacer porque nuestras cabezas maquinaron demasiado en un principio.

Con paciencia, y muchas ganas de hacer cosas ya veréis qué bonito os queda.

Si no tienes mucho tiempo **te recomiendo que acudas a los** [**globos de helio** para decorar la mesa de la comunión](https://amzn.to/3u6SI0i), que te vestirán en un momento cualquier espacio. Los puedes personalizar y elegir de los tonos que te interese. No olvides hacerte con una [bombona de helio](https://amzn.to/2PBHOk5) para hincharlos.

[![](/images/uploads/nobranded.jpg)](https://www.amazon.es/dp/B0892FVFG4/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=03f857af6b79a3c729acf2aaf65f56a1&creativeASIN=B0892FVFG4&th=1)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B0892FVFG4/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=03f857af6b79a3c729acf2aaf65f56a1&creativeASIN=B0892FVFG4&th=1)

Lo que más llamó la atención a los invitados: los sencillos [molinillos de viento de toda la vida. Aquí os dejo el tutorial](http://facilysencillo.es/2009/06/diy-pinwheel-molinillo-de-viento.html)

![molinillos-viento-cartulina](https://d33wubrfki0l68.cloudfront.net/4ea0a62d6e9aa1d829984d467a57f9ed91852a62/fcbed/tumblr_files/tumblr_inline_nojmezrq3c1qfhdaz_540.jpg)

Los maceteros los decoré con papel vintage y puntillas de mi tía abuela, y los puse sobre una caja de fresas al revés pintada de blanco para que el centro quedara alto.

![materiales-decoracion-fiesta](https://d33wubrfki0l68.cloudfront.net/d479ee533b1c9a2b0e113707c7beb3a9df13815e/10b59/tumblr_files/tumblr_inline_nojniismzh1qfhdaz_540.jpg)

Los servilleteros también fueron todo un éxito: con una pulsera de caramelos y una etiqueta con el nombre en los tonos del resto de la decoración, quedaron muy originales.

El mural lo hicimos con papel continuo de embalar decorado con vinilos de una tienda china, y en el centro pusimos un retrato de la niña que le hizo su tía, pero si no tenéis artistas en la familia, siempre podéis poner una muñeca de comunión dibujada con rasgos de la protagonista, o una foto

Una buena idea es poner en la mesa, a modo de mantelitos individuales un dibujo para colorear y lápices de colores para que los peques se entretengan hasta que les sirvan la comida (funciona!).

## Los monitores y juegos para la Comunión

Nosotros contratamos una amiga de la familia, que los niños conocían previamente y estuvimos muy tranquilos. Teníamos preparados varios juegos y ella los fue dirigiendo.

Se lo pasaron pipa!

Por ejemplo, **un photocall nunca falla** y si además le regalan una cámara de fotos al protagonista de la fiesta, la diversión está asegurada.

[![](/images/uploads/vtech-kidizoom.jpg)](https://www.amazon.es/VTech-Kidizoom-infantil-espa%C3%B1ola-80-163557/dp/B07L3FDP1M/ref=sr_1_2?keywords=c%C3%A1mara%2Bfotogr%C3%A1fica%2Bni%C3%B1os&qid=1643039578&sr=8-2&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/VTech-Kidizoom-infantil-espa%C3%B1ola-80-163557/dp/B07L3FDP1M/ref=sr_1_2?keywords=c%C3%A1mara%2Bfotogr%C3%A1fica%2Bni%C3%B1os&qid=1643039578&sr=8-2&th=1&madrescabread-21)

Unos marcos de IKEA, unos bocadillos con cartulina negra, pintura blanca con frases graciosas, el atrezzo hecho con goma EVA y un palito para sujetar dan para un buen rato de juego, incluso para los mayores.

(idea de [Marga](http://www.subidaenmistacones.com/2015/03/tres-ideas-para-triunfar-en-una-fiesta.html), del blog Subida en mis Tacones)

![photocall-atrezzo-ninos](https://d33wubrfki0l68.cloudfront.net/7b817186035fdba2affc2d7f749bea07d2508301/b3e30/tumblr_files/tumblr_inline_nojzoqh4451qfhdaz_540.jpg)

Si no tienes tiempo para todo esto, siempre puedes **encargar un [marco de photocall personalizado](https://amzn.to/3vv6ha3).**

[![](/images/uploads/photocall.jpg)](https://www.amazon.es/gp/product/B07Q6ZCYLY/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B07Q6ZCYLY&linkId=a25a14ce019f276ab53b028dfedfd59d)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B07Q6ZCYLY/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B07Q6ZCYLY&linkId=a25a14ce019f276ab53b028dfedfd59d)

También podéis hacer un pompero gigante, como os explica Marta, del blog Mà a mà, pell a pell, cor amb cor, pintura de caras o los típicos juegos tradicionales adaptados a la edad de los pequeños invitados.

## ¿Hinchables para comuniones?

Huye de ellos. A mí no me gustan porque se destrozan el traje, **hay riesgo de accidentes** si se mezclan niños de edades muy diversas y sudan como pollos. Por el contrario, hay muchos juegos que pueden hacer más originales y variados (y te ahorras esa partida).

## La mesa dulce de la Comunión

**Para decorarla** no te salgas de los tonos elegidos.

Os dejo [mi tablero de Pinterest de mesas dulces](https://es.pinterest.com/madrescabreadas/mesas-dulces/).

En nuestro caso quisimos darle aspecto de quiosco, para lo que **aprovechamos la estructura de un perchero fijo** que había en el restaurante y la colocamos debajo. Lo forramos con un mantel de papel a rayas rosas y blancas que compramos en una tienda china.

Tuvimos la suerte de encontrar etiquetas en los mismos tonos en IKEA y, como están de moda los bocadillos de cómics, compramos también pegatinas a juego para las bolsitas de regalo y los conos de gusanitos, que los hicimos con mantelitos de papel de blondas.

Los maceteros para los árboles de chuches también los conseguimos en los mismos tonos en IKEA.

![maceteros-etiquetas-ikea](https://d33wubrfki0l68.cloudfront.net/4999fc6409744e249c9878cac7c7d9600601fa90/b0416/tumblr_files/tumblr_inline_nojxr7o53j1qfhdaz_540.jpg)

Los palomiteros, aparte de quedar genial como decoración, son súper prácticos para no tener a todo el mundo agolpado comiendo golosinas en el puesto, sino que permiten que una vez se sirven se pueden retirar y dar paso a los demás.

Os dejo la plantilla por si queréis hacerlos con cartulina, como hice yo.

**Las chuches**

Aquí es donde los tutoriales juegan un gran papel, y las buenas amigas, como Cristina, que se prestan a hacerte los árboles porque ven que no das abasto.

**Escoge golosinas de colores acordes con los tonos elegidos.** Si vas aun almacén de venta al por mayor te saldrán más económicas.

Las rosas hechas con de lenguas de gato fueron las estrellas de la mesa dulce. [Aquí tenéis cómo hacerlas.](http://cocinoydisfruto.blogspot.com.es/2013/02/rosas-de-regaliz.html)

Y los árboles de nubes son más sencillos de hacer de lo que parece. A continuación, te presento un pequeño vídeo del tutorial de Charhadas.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4SvmsCwP01o?start=5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si pones **etiquetas con frases ocurrentes** como “estos corazones te harán querer a todo el mundo” será un éxito. Deja que tus hijos se las inventen.

![nina-partiendo-tarta-comunion](https://d33wubrfki0l68.cloudfront.net/629b9a4b0404c4a41e2b0476ed825eeb47485586/63685/tumblr_files/tumblr_inline_nojluyvm5r1qfhdaz_540.jpg)

La verdad es que nuestro puesto dio mucho juego porque los niños acabaron jugando a “vender” chuches a los mayores y disfrutamos todos de lo lindo.

![ninos-mesa-dulce-comunion](https://d33wubrfki0l68.cloudfront.net/ade1c4fbc6727705ddd2244c60bb46ef0bd018bf/47d9f/tumblr_files/tumblr_inline_nojlnvmvtz1qfhdaz_540.jpg)

## Los detalles para los invitados de la Comunión

Yo no quise meterme en más gastos, y decidí hacer [galletas de mantequilla](https://madrescabreadas.com/2015/12/07/decora-galletas-de-navidad-en-familia.html) y decorarlas con fondant. Y como no encontré rodillo para que quedara con relieve de puntitos, aproveché un bolígrafo de mi hija para marcarlo.

![galletas-decoradas-comunion-caseras](https://d33wubrfki0l68.cloudfront.net/585d501970138eca11c56dca6664bb320617d605/7d836/tumblr_files/tumblr_inline_nojmyg7ing1qfhdaz_540.jpg)

Luego las embolsé en celofán y cerré bien para que aguantaran hasta la fecha de la comunión (las hice 15 días antes), y resultaron deliciosas.

![detalles-invitados-comunion](https://d33wubrfki0l68.cloudfront.net/64ea30ed21dacb922b53e4774dbe3bfbb010b23e/e8d90/tumblr_files/tumblr_inline_nojyoujzsf1qfhdaz_540.jpg)

Para los caballeros, los niños repartieron puros de chocolate. Y más de uno pidió fuego!

![nino-beso-comunion](https://d33wubrfki0l68.cloudfront.net/1f60e843a5211fa8b4668afa9bc6e1018bc29f2f/fd672/tumblr_files/tumblr_inline_nojmaegayo1qfhdaz_540.jpg)

También **puedes encargar unas tazas con mensajes de agradecimiento.** Nosotros todavía conservamos una que regalaron a mi hijo hace 4 años; es un regalo que se suele dar uso y hace que los invitados se acuerden de la comunión a lo largo del tiempo.

[![](/images/uploads/tazas-con-frases.jpg)](https://www.amazon.es/dp/B0784KQK58/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=c974aeed03270d8d9fbead4b318eafee&creativeASIN=B0784KQK58)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B0784KQK58/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=c974aeed03270d8d9fbead4b318eafee&creativeASIN=B0784KQK58)

## Los recordatorios y libro de firmas de la Comunión

En este capítulo las que sois manitas podéis ahorraros un dinerillo. 

También **podéis montarlo vosotras mismas** con este kit de Mr. Wonderful, que además os permitirá crear un álbum de comunión personalizado.

[![](/images/uploads/grupo-erik-pcom004.jpg)](https://www.amazon.es/Grupo-Erik-PCOM003-Primera-Comuni%C3%B3n/dp/B085SQGB1H/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2UO8IV9S0IVGR&keywords=libros%2Bprimera%2Bcomuni%C3%B3n&qid=1643041556&s=kitchen&sprefix=libros%2Bprimera%2Bcomuni%C3%B3n%2Ckitchen%2C357&sr=1-1&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Grupo-Erik-PCOM003-Primera-Comuni%C3%B3n/dp/B085SQGB1H/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2UO8IV9S0IVGR&keywords=libros%2Bprimera%2Bcomuni%C3%B3n&qid=1643041556&s=kitchen&sprefix=libros%2Bprimera%2Bcomuni%C3%B3n%2Ckitchen%2C357&sr=1-1&th=1&madrescabread-21)

O tienes ese don o tienes unas amigas artistas que te hagan un libro de firmas como el que me regalaron las chicas de Kakuii, o unos recordatorios como los que me dibujó con tanto cariño [Mamá Multitarea](http://mamamultitarea.blogspot.com.es/p/recordatorios-de-comunion.html), a quienes estoy muy agradecida por su buen gusto y cariño.

![recordatorios-libro-firmas-comunion](https://d33wubrfki0l68.cloudfront.net/2539e090f85ef75743f24049c555dbf9117a6c31/95751/tumblr_files/tumblr_inline_nol7c8jscv1qfhdaz_540.jpg)

También tengo que agradecer a mi hermana, a Raquel, y todas las chicas del [grupo de Facebook “Organizando la Primera Comunión”](https://www.facebook.com/groups/1377576085877092/), porque sin sus ideas, apoyo moral y ganas de escucharme os aseguro que hubiera abandonado porque os confieso que jamás he hecho manualidades ni decorado nada.

Pero soy capaz de todo por mi Princesita, y si tengo que convertirme en decoradora, repostera o lo que haga falta, pues me convierto. Mientras haya un buen tutorial y buena gente que te ayude todo es posible.

Y tú, te animas a hacerlo tú misma?

A tu disposición el [grupo de Facebook “Organizando la Primera Comunión”](https://www.facebook.com/groups/1377576085877092/).

Más ideas, en la Comunión de mi segundo hijo. Esta vez la [temática de la fiesta fue Pokemon](https://madrescabreadas.com/2017/05/22/comunion-candybar-pokemon.html)

Espero haberte ayudado. Ahora me puedes ayudar tu a mi compartiendo este post :)