---
date: '2014-05-15T16:00:00+02:00'
image: /tumblr_files/tumblr_inline_pb455bNQEd1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- crianza
- cosasmias
- familia
- bebes
title: Mamá, se me mueve un diente.
tumblr_url: https://madrescabreadas.com/post/85817870214/mamá-se-me-mueve-un-diente
---

De camino del cole a casa, como quien no quiere la cosa dice Osito, mi hijo mediano:

“-Mamá se me mueve un diente.

-¿Cómo? Pero si todavía eres un bebé… casi… bueno, lo eras… ejem… ¿pero cuándo has dejado de serlo? ¿Cuándo has crecido tanto? ¿Dónde está mi osito?

-(Ojos como platos) Mamá, ya no soy un bebé, ¿por qué me dices bebé?”

Y tiene razón, pero me di cuenta de que no lo tenía asumido. Osito fue un bebé ideal, entendido como el bebé que toda mujer se imagina cuando no ha sido madre todavía y en su mente fantasea con tener un hijo: rechoncho, redondito, cariñoso, tragoncete, gracioso, guapo… vamos, un angelote.

Reconozco que tengo esa época demasiado idealizada. Quizá porque duró mucho. Hasta que, cuando tenía 4 años, nació su hermano Leopardito y le arrebató el puesto. Ya sé que en parte es culpa mía porque yo lo seguí llamando “bebé” hasta que supe del embarazo el pequeño, y tuve que hacer un gran esfuerzo para no seguir llamándoselo después (confieso que hoy en día a veces se me escapa, a lo que él salta de nuevo: “Mamá, no soy un bebé”)

![image](/tumblr_files/tumblr_inline_pb455bNQEd1qfhdaz_540.jpg)

Pero es que cuando se ríe a carcajadas lo hace igual que cuando era chiquitín  A pesar de sus 6 años, yo sigo viendo a aquel bebé regordete y grandote que nos contagiaba con su risa e inundaba la casa de felicidad sonora con aquel sonido tan lindo.

Eso es lo que me queda de aquel bebé que tanto llenó mi vida y que tanto me realizó como madre. Su boca riendo, con sus diminutos dientecillos, los que fabricó gracias al calcio que yo le di con mi leche, los mismos que ahora se le empiezan a mover, y que cambiará en breve por otros de niño mayor, y que se llevarán con ellos los últimos vestigios de la primera etapa de su vida.

Se cierra una etapa, sí… y guardaré el primer diente que se le caiga como recuerdo de ella.

“Mamá, se me mueve un diente”. Cómo se me clavaron esas palabras, en serio, no estoy preparada todavía. “¡Nooooo!”, exclamé, al tiempo que él saltaba y agitaba los brazos por la calle entusiasmado con la idea de hacerse mayor.

Pero ¿por qué te haces mayor, mi amor?