---
date: '2014-11-20T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petodaKp9x1qfhdaz_540.jpg
layout: post
tags:
- premios
- trucos
- planninos
- familia
title: 'IV sorteo aniversario: lote de 3 entradas para el concierto de María Parrado
  con Nancy'
tumblr_url: https://madrescabreadas.com/post/103111346345/sorteo-entradas-maria-parrado-lavoz
---

[![image](/tumblr_files/tumblr_inline_petodaKp9x1qfhdaz_540.jpg)](http://famosa.solution.weborama.fr/fcgi-bin/performance.fcgi?ID=425167&A=1&L=1110240&C=52312&f=13&P=64597&T=E&URL)

ACTUALIZACIÓN 26-11-14

El primer ganador renunció al premio, así que hemos realizado de nuevo el sorteo:

![image](/tumblr_files/tumblr_inline_petodaOWgr1qfhdaz_540.png)

Esta vez las entradas han correspondido a mfalcon6. Enhorabuena! 

ACTUALIZACIÓN 25-11-14

![image](/tumblr_files/tumblr_inline_petodayb4Q1qfhdaz_540.png)

Enhorabuena, Javicerm! Ya puedes dar la sorpresa a tu mujer y tu hija. Que disfrutéis del concierto!!

Post original:

Bueno, bueno, bueno! Qué sorpresa os traigo para celebrar el [cuarto aniversario del blog!](/portada-aniversario)

Estoy emocionada a la par que entusiasmada de que Nancy me haya dado la oportunidad de sortear un lote de 3 entradas para el concierto que ha organizado para sus fans el 29 de noviembre con motivo de su cumpleaños.

¿Conocéis a [María Parrado](http://mariaparrado.com/), ganadora de La Voz Kids? últimamente ando tarareando todo el día su canción “A prueba de ti”

<iframe frameborder="0" height="315" src="//www.youtube.com/embed/AoJVRnEQJ1w?rel=0" width="560"></iframe>

¿Os mola? ¿Os apetece ir al concierto?

Pues para participar en el sorteo tenéis que dejar un comentario en este post indicando cuál es vuestra Nancy favorita.

Y si queréis, aunque no es obligatorio, pero así estaréis al día del resultado del sorteo y de los próximos que vaya sacando a lo largo del mes, podéis suscribiros al blog. Para ello sólo tenéis que introducir vuestro email en el margen derecho.

El sorteo es válido para territorio español, y el plazo acabaría el 25 de noviembre a las 20:00 h.

Pero hay más!!

¿Os apetece que vayamos juntas al concierto?

Si mi blog es el que obtiene más comentarios me regalarán a mí también 3 entradas para ir con mi familia.

¿Nos vamos de concierto con Nancy?

![image](/tumblr_files/tumblr_inline_petodbM3zv1qfhdaz_540.png)

NOTA: Post patrocinado