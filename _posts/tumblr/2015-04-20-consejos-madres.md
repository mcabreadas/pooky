---
date: '2015-04-20T08:00:25+02:00'
image: /tumblr_files/tumblr_inline_pc7s7fm5Ts1qfhdaz_540.jpg
layout: post
tags:
- hijos
- trucos
- familia
title: Consejos a una madre reciente de familia numerosa.
tumblr_url: https://madrescabreadas.com/post/116891810246/consejos-madres
---

El otro día me encontré con una compañera de la facultad, y me sorprendió la manera de venir hacia mí con ese ansia de quien necesita desesperadamente desahogarse.

Me dijo, entre sorprendida e incrédula de que le estuviese pasando a ella, que desde hace dos meses que había tenido a su tercer hijo, vivía corriendo de un sitio para otro. Que no lograba hacerlo todo, que la semana pasada mandó a su hija mediana al cole sin desayunar (y se “flagelaba” por ello), y que ella quería darle al pequeño lo mismo que le había dado a los demás, pero que veía que no podía, que no tenía tiempo ni fuerzas ni espacio mental para más.  
Me preguntó si eso nos pasaba a las demás o si sólo le pasaba a ella. Y también que si siempre iba a ser así

 ![image](/tumblr_files/tumblr_inline_pc7s7fm5Ts1qfhdaz_540.jpg)

En su mirada había entre sorpresa y recriminación porque nadie la había advertido de lo que se le venía encima y, sobre todo, por comprobar que yo seguía viva y sonriente después de tan “gran hazaña”. Hasta me tocaba el brazo de tal forma como si quisiera comprobar que yo era de carne y hueso, que no era una madre espectro a la que le habían succionado la vida.

Os preguntaréis cuál fue mi respuesta ante tal atropello.

Entendedme, la chica estaba feliz con sus hijos, los adora y tenía la cara iluminada, cansada, pero contenta, pero la situación la estaba empezando a sobrepasar porque quería hacerlo todo perfecto.

Y eso fue lo que le dije. 

## Huye del perfeccionismo

Que tuviera cuidado, que las mujeres que somos perfeccionistas lo pasamos muy mal cuando no llegamos a todo. Que hasta ahora, quizá habíamos podido llevarlo todo al dedillo, pero que cuanto antes tomara consciencia de que el cerebro humano es limitado (y el suyo también), y que no puede estar en cada mínimo detalle de cada uno de sus tres hijos, de su marido, de la casa…, antes empezaría a disfrutar de su triple maternidad.

 ![image](/tumblr_files/tumblr_inline_pc7s7gLmkp1qfhdaz_540.png)
##   

## Cada hermano es diferente

Que no se preocupara por no poder dedicar la misma atención al tercero de sus hijos que había dedicado a los anteriores cuando nacieron porque este último iba a tener los mejores maestros del mundo, además de los mejores compañeros de camino para toda la vida, sus hermanos mayores. Que es el mejor regalo que había hecho a sus hijos.

 ![image](/tumblr_files/tumblr_inline_pc7s7g0fCM1qfhdaz_540.png) ![image](/tumblr_files/tumblr_inline_pc7s7h0EAR1qfhdaz_540.png)
##   

## Obligatorio un rato para ti misma

Le dije que guardara un ratito al día para ella misma (obligatoriamente), por el bien de su salud, porque si ella flaqueaba, la familia se vería afectada.

## Establece prioridades

Que relativizara la importancia de las cosas, que estableciera prioridades, y que se preocupara sólo de lo realmente importante.

## Perdónate los fallos

Que se quisiera mucho y se perdonara los fallos, que no pasaba nada porque la niña se fuera un día sin desayunar al cole, que los míos a veces van sin almuerzo porque se me olvida (y me da vergüenza decirlo, pero seguro que a más de una le ayuda el saber que no es la única que falla a veces).

## Date tiempo para acostumbrarte

En cuanto a la pregunta de si siempre iba a ser así, le dije que sí, que si no eran unas demandas iban a ser otras, que quizá requerirán menos esfuerzo físico en el futuro, pero la preocupación por sus hijos siempre iba a estar ahí, y ya se había adueñado de su cerebro para siempre. 

## Tómalo como una maravillosa aventura

Que esta época de vivir para sobrevivir daría paso a otras no tan “intensas”, pero que se olvidara de volver a la comodidad de antes, que nosotras habíamos elegido el camino difícil, pero el más satisfactorio de tener familia, y que le esperaba un nuevo y apasionante mundo con tres criaturas llenas de vitalidad y con muchas ganas de comerse el mundo, y que ella acabaría comiéndoselo con ellos, que no lo dudara.

 ![image](/tumblr_files/tumblr_inline_pc7s7i5T021qfhdaz_540.png)

¡Bienvenida al maravilloso mundo de las familias numerosas, amiga!

¿Y tú? Añadirías algún consejo más?