---
date: '2016-04-26T16:52:48+02:00'
image: /tumblr_files/tumblr_inline_o68jpjwXiP1qfhdaz_540.jpg
layout: post
tags:
- trucos
- regalos
- colaboraciones
- gadgets
- ad
- tecnologia
- mujer
title: Ocho gadgets para regalar el día de la madre
tumblr_url: https://madrescabreadas.com/post/143431097279/regalo-tecnologia-madre
---

Si buscas regalos diferentes orignales y para el día de la madre porque quieres sorprender a una mamá friki, cool y amante de la tecnología y los gadgets, sigue leyendo porque te vas a divertir, y seguramente acabes con alguna buena idea para regalar en este día tan especial.

Te propongo dos regalos caseros, uno de ellos muy sencillo, que puedes hacer tú mismo:

## 1- Un corazón electrónico DIY  

Si eres un manitas no te resultará difícil hacer este [corazón electrónico](http://www.instructables.com/id/Electronic-Heart-Flashing-LEDs-Mothers-Day-Pr/) que demostrará tu amor hacia mamá. Se activa por remoto, y puede latir por ella. Te aseguro que le sacarás una sonrisa… o una lagrimilla quizá…

En este enlace de la web Instructables se explica paso a paso cómo se hace. ¡Si te atreves a hacerlo espero que me dejes una foto en los comentarios!

[Cómo hacer el corazón electrónico paso a paso](http://www.instructables.com/id/Electronic-Heart-Flashing-LEDs-Mothers-Day-Pr/).

 ![](/tumblr_files/tumblr_inline_o68jpjwXiP1qfhdaz_540.jpg)
## 2- Déjale un mensaje amoroso en CMD

¿Te imaginas arrancar el ordenador y que te salga un mensaje personalizado en una ventana de comandos? A mí me emocionaría bastante. Sólo tienes que abrir tu corazón e inventar un [mensaje amoroso ](http://www.instructables.com/id/A-Beautiful-Mothers-Day-Message-in-CMD/)para tu mamá o esposa, y lograrás sorprenderla y alegrarle el día.

En la [web Instructables se explica cómo hacerlo](http://www.instructables.com/id/A-Beautiful-Mothers-Day-Message-in-CMD/). Parece sencillo!

 ![](/tumblr_files/tumblr_inline_o68jsodw0N1qfhdaz_540.jpg)
## 3- Sensor para plantas

Si la madre de familia es tan despistada como yo, seguro que agradecerá una ayudita a la hora de cuidar las plantas con este [sensor de plantas](http://www.macnificos.com/product/7342/0/0/1/Parrot-Flower-Power-Sensor-Plantas-Wireless-Verde.htm?tduid=f130a6aee39f98c443c2c9b814815a7d).

A mí me encantan, pero confieso que todas las que han pasado por casa han acabado ahogadas. Reconozco que no tengo mano, por eso me ha llamado la atención este sensor para plantas que monitoriza sus necesidades en el móvil. Así nunca se olvidará de regarlas y podrá saber en todo momento la temperatura ambiente, la intensidad de la  luz, si necesitan fertilizantes y como está el nivel de agua.

 ![](/tumblr_files/tumblr_inline_o68jwbuvoK1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o68lz8z9xZ1qfhdaz_540.jpg)
## 4- Calentador tazas cookie usb

Para mamás insomnes que echan horas sin límite frente al ordenador, y que necesitan sentirse reconfortadas con una buena taza de café de vez en cuando, este[calentador de tazas por usb “hot cookie”](http://www.amazon.es/gp/product/B00HZJTGCW/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00HZJTGCW&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00HZJTGCW)evitará que se le quede frío, y le ayudará a inspirarse.

 ![](/tumblr_files/tumblr_inline_o68m3dIa091qfhdaz_540.jpg)
## 5- Lámpara libro

Una de las cosas que echo de menos de mi vida anterior a la maternidad es tener tranquilidad para leer un libro tranquilamente (insisto, tranquilamente). Es cierto que si te gusta leer sacas tiempo de donde sea, aunque todos estén durmiendo y tengas que alumbrarte con la linterna del móvil.

Por eso me encanta esta  [lámpara de lectura para libros](http://www.amazon.es/gp/product/B002PL1GFA/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B002PL1GFA&linkCode=as2&tag=madrescabread-21)

 ![](/tumblr_files/tumblr_inline_o68ttvs6RM1qfhdaz_540.jpg)
## 6- Lámpara Tetris

Como buena Tetris adicta en mis años mozos, no puedo dejar de recomendaros esta[lámpara con diseño de Tetris](http://www.amazon.es/gp/product/B009USUO68/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B009USUO68&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B009USUO68), juego que enganchó a toda una generación en bares y recreativos.

Seguro que a mamá le encanta porque puede poner las piezas a su antojo como si estuviera jugando de verdad.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/kwBnUCrMBig?rel=0" frameborder="0" allowfullscreen></iframe>

## 7-Bandeja para tablet

Llevar el desayuno a la cama es un clásico en el día de la madre, pero si se lo presentas en esta [bandeja, con soporte para tablet](http://www.amazon.es/gp/product/B00BGCPE5W/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00BGCPE5W&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B00BGCPE5W) incluída, para que pueda ver la prensa digital o sus redes sociales, seguro que la sorprendes muy gratamente.

Ponle su desayuno favorito adornado con una flor, y no se le borrará la sonrisa de la cara en todo el día.

 ![](/tumblr_files/tumblr_inline_o68ksnE1sN1qfhdaz_540.jpg)
## 8-Altavoz bolso

Y ya, para las mamás más superfrikis, tienes este bolso tan mono, que en realidad es un [altavoz portátil, estéreo, inalámbrico](http://www.amazon.es/gp/product/B010FP7CQY/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B010FP7CQY&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B010FP7CQY)

¿Cómo se te queda el cuerpo?

Mola, ¿eh?

![](/tumblr_files/tumblr_inline_o68l35JbN01qfhdaz_540.jpg)

Espero haberte dado buenas ideas, y que tengas claro cuál va a seer tu regalo.

¿Con cuál te quedas?