---
date: '2017-10-15T16:49:17+02:00'
image: /tumblr_files/tumblr_inline_oxv25xH6Xm1qfhdaz_500.jpg
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: Bizcocho integral de cacao puro
tumblr_url: https://madrescabreadas.com/post/166429509139/bizcocho-integral-de-cacao-puro
---

Ya te conté mi plan de [desayunos variados y saludables aquí](/2017/05/10/7-desayunos-equilibrados-para-los-ni%C3%B1os.html). Decidí hacerlo porque mis hijos se aburrían de desayunar siempre lo mismo y llegaban incluso a optar por beberse el vaso de leche solamente sin comer nada, y a regañadientes.

 ![](/tumblr_files/tumblr_inline_oxv25xH6Xm1qfhdaz_500.jpg)

La novedad es que he incluído en mi menú de desayunos este bizcocho saludable que proponía mi amiga Silvia, del blog S[er Trimadre, Toda una Aventura](http://sermadreunaaventura.com/receta-de-bizcocho-bajo-en-calorias/), pero adaptado a los gustos de mi famila.

A veces los niños van cansados por las mañanas y ver un bizcocho de chocolate sobre la mesa les alegra el día. En serio. Es magia!

Pero ¿Cómo hacer que además sea un desayuno saludable?

Pues con esta sencilla receta:

## Ingredientes 

- 6 huevos  
- 150 g de harina integral  
- 150 g de azúcar integral de caña  
- 1 sobre levadura  
- 2 cucharadas generosas de cacao puro en polvo (es amargo)  
- 1 cucharadita de Azúcar vainillado o raspadura de piel de naranja (mi opción favorita)  

## Cómo se hace

1. Se precalienta el horno a 170\* por arriba y por abajo con aire (dependiendo del horno, puede variar)  
2. Se engrasa el molde  
3. Se mezclan los huevos con el azúcar en un bol con unas varillas.  
4. Después se añade el resto de los ingredientes y se mezclan muy bien hasta que quede homogéneo.  
5. Se vierte la mezcla en el molde e inmediatamente se mete en el horno, previamente precalentado, durante 12 minutos (aunque el tiempo de cocción puede variar según el horno).  

El truco para que suba más es meterlo en el horno justo después mezclarlo.

 ![](/tumblr_files/tumblr_inline_oxv25xp6r81qfhdaz_500.jpg)

Como ves, he cambiado algunas cosas de la receta de Silvia: y he añadido [cacao puro de este que os recomendé aquí](/2016/02/28/hacer-chocolate-cacao.html), que no aporta azúcar, ya que es amargo, pero sí todas las propiedades del cacao, y he sustituido el anís por ralladura de piel de naranja, porque para mí la combinación del chocolate con la naranja es perfecta.

Te animo a intentar vuestra variante y compartirla!