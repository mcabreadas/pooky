---
date: '2013-10-25T12:10:00+02:00'
image: /tumblr_files/somoscomplices_foto300x168.jpg
layout: post
tags:
- mecabrea
- revistadeprensa
- colaboraciones
- ad
- conciliacionrealya
- conciliacion
title: Entrevista con Radio euskadi
tumblr_url: https://madrescabreadas.com/post/65033876730/entrevista-con-radio-euskadi
---

[Entrevista con Radio euskadi](http://www.eitb.com/es/audios/detalle/932400/nuevos-retos-conciliacion-real-ya-nueva-victima-amianto/)  

Entrevista como portavoz del movimiento Conciliación Real Ya en Radio Euskadi, programa Somos Cómplices. 30 de julio de 2012. 

![](/tumblr_files/somoscomplices_foto300x168.jpg)