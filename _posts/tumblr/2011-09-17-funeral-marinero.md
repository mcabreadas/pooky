---
layout: post
title: Funeral marinero
date: 2011-09-17T17:57:00+02:00
image: /tumblr_files/tumblr_lroc52yjlF1qfhdaz.jpg
author: maria
tags:
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/10318137662/funeral-marinero
---
El cielo estaba triste y la mar, plato, según decían los entendidos. Yo, que soy lega en estos temas de navegación no necesité ninguna explicación para entender a qué se referían. Era un mar tranquilo, paciente, como si estuviera esperando a que terminara todo para volver a su ser. El cielo lo acompañaba, y se entristecía al despedir a nuestro amigo tornándose de un color gris plomizo, como el color de los corazones de todos los que estábamos en aquel barco.

Nunca había asistido a un funeral marinero, y os puedo decir que es un acto calmado, sentido, emotivo, precioso, silencioso… Sólo habla el mar y las salvas, que se clavan en el alma y en el silencio del cielo anunciando que llegó la hora de que esa familia destrozada arroje las cenizas del que era su miembro más joven al mar de la playa del Portús. Ese mar que él amaba tanto, ese mar que fue su pasión, ese mar donde lo conocimos y lo hemos despedido hoy. Vuelve a él para que lo acune. “Está donde quiere”, dice su madre desgarrada por el dolor. Ese dolor visceral que sentimos las madres por nuestros hijos. El padre no dice nada, no puede. Es un hombre recto, correcto, de los de antes, pero el sufrimiento se le escapa al ver las cenizas caer.

Después tiramos las flores, amarillas y rojas, que quedan adornando el mar de la playa del Portús, que dejamos atrás para volver a puerto.

Es la primera vez que veo unos padres despedir a un hijo, y no al revés. 

No creo que haya nada más triste.