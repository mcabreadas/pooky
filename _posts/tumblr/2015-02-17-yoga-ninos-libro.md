---
date: '2015-02-17T16:19:00+01:00'
layout: post
tags:
- trucos
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: Aprendiendo a relajarnos con el libro Yoga en la Infancia.
tumblr_url: https://madrescabreadas.com/post/111281413934/yoga-ninos-libro
---

Siempre he tenido curiosidad por el Yoga para niños. Había leído de sus beneficios para ayudarles a relajarse, tonificar los músculos, ser más flexibles…

Por eso ha sido una suerte que este libro de Maurizio Morelli haya caído en mis manos. “[El Yoga en la Infancia. Ejercicios para divertirse y crecer con salud y armonía](http://www.boolino.es/es/libros-cuentos/el-yoga-en-la-infancia/)” explica paso a paso ejercicios básicos de Yoga adecuados al desarrollo de los niños para practicarlo de manera divertida y en familia.

Primero empezamos con posiciones individuales, a las que nombra con animales de manera que los peques se motivan intentando imitarlos.

Cuando estas posiciones están afianzadas, comenzamos a trabajar en pareja, para terminar haciendo unos divertidos ejercicios de Yoga en grupo.

En cada posición el libro explica los beneficios concretos que aporta al organismo, además de detallar pormenorizadamente la forma de lograrla correctamente, para lo que también acompaña fotografías.

Si lo probáis con vuestros hijos, veréis la gran facilidad que tienen para adoptar cada postura. Y si tenéis un bebé mayorcito, como el mío de 30 meses, os asombrará que el que mejor lo hace es él porque pareciera que son posiciones naturales y que le salen solas de forma casi espontánea.

Os dejo con:

“El elefante que come las hojas”

“La serpiente que levanta la cabeza”

“El gato que hincha el lomo”

¿Os animáis a probarlo?