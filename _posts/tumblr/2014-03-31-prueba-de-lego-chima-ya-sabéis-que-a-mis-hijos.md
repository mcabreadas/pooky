---
layout: post
title: Prueba de Lego Chima
date: 2014-03-31T11:00:21+02:00
image: /tumblr_files/tumblr_n33vb8aWMD1qgfaqto5_r1_400.jpg
author: .
tags:
  - juguetes
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/81276437316/prueba-de-lego-chima-ya-sabéis-que-a-mis-hijos
---
 Ya sabéis que a mis hijos les encantan los juegos de construcción, por eso los de LEGO me han mandado una caja de LEGO CHIMA para que la probemos y contemos nuestra experiencia.

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/PzRccaHaiZs?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

Lo último de LEGO consiste en mezclar las construcciones con un juego de cartas que encierra una elaborada historia, la Leyenda de Chima.

Las tribus de animales han gobernado la tierra de CHIMA durante mil años. Leones, cocodrilos, águilas, lobos, gorilas, cuervos, rinocerontes y osos vivían en paz. Jugaban y trabajaban juntos, colaboraban y prosperaban… hasta que los conflictos estallaron en CHIMA.

Cuando Cragger, el Príncipe Cocodrilo, acusó a los leones de haber eliminado a sus padres y se convirtió en el Rey Cocodrilo, estalló un conflicto a gran escala entre cocodrilos y leones. El conflicto continúa creciendo y el destino del Reino de CHIMA pende de un hilo…

![](/tumblr_files/tumblr_n33vb8aWMD1qgfaqto5_r1_400.jpg)

Nos ha gustado especialmente porque se trata de un concepto más novedoso que un simple juego de construcciones al incluír también un juego de cartas relacionadas con la historia.

Los coches son bastante robustos y no se sueltan las piezas porque están muy bien diseñados para que no se caiga el muñeco, a pesar de que nuestro bebé de 20 meses hiciera de las suyas y se empeñara en aprender a lanzarlos, tal y como se ve en el video.

![](/tumblr_files/tumblr_n33vb8aWMD1qgfaqto6_r1_500.jpg)

Fue un gran reto para mis fieras montar los coches, que finalmente terminaron con éxito con la ayuda de su padre.

La experiencia dio para una tarde casera de entretenimiento y disfrutaron mucho.

![](/tumblr_files/tumblr_n33vb8aWMD1qgfaqto3_r1_500.jpg)

Os recomiendo los juegos de construcciones porque los niños se lo pasan genial y al mismo tiempo les estamos ayudando y motivando para desarrollar habilidades muy importantes para el futuro.

Nota: Este post NO es patrocinado. LEGO me ha mandado una muestra para tester y he dado mi opinión según nuestra experiencia en casa.