---
date: '2016-05-08T17:31:00+02:00'
image: /tumblr_files/tumblr_o6v745n6NN1qgfaqto1_400.gif
layout: post
tags:
- colaboraciones
- comunion
- ad
- familia
- moda
title: comunion ceremonia 2017
tumblr_url: https://madrescabreadas.com/post/144045733314/comunion-ceremonia-2017
---

![](/tumblr_files/tumblr_o6v745n6NN1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_o6v745n6NN1qgfaqto2_400.gif)  
 ![](/tumblr_files/tumblr_o6v745n6NN1qgfaqto3_400.gif)  
  

## Vestidos y trajes de Comunión: Amaya, Paqui Barroso y Rubio Kids

## Paseo Mágico de FIMI

Este año las Comuniones nos quieren trasladar a otra época. Los vestidos románticos o decimonónicos, y los aires vaporosos son los protagonistas para las niñas.

Para los niños, encontramos los marineros renovados, donde el lino es la clave, y en los que los niños se sentirán cómodos.

Cada año, las principales marcas de ropa de Comunión y ceremonia presentan sus novedades en un evento organizado por FIMI, que se llama “Paseo Mágico”, al que tuve la suerte de asistir para mostrarte las novedades para la temporada 2017.

En los gifs de arriba te muestro trocitos del desfile, que fue espectacular. Por orden, encontramos las siguientes firmas:

[Amaya](http://en.artesania-amaya.com/)

En Amaya siempre han tenido en cuenta que la moda infantil es mucho más que la confección de ropa de los niños. Han perseguido la esencia de la moda a través de la simplicidad y sofisticación desde sus inicios, la combinación de diseño, telas y texturas, tratando de mostrar al mundo su particular visión de la moda infantil.   
Esta independencia creativa, resultado de la experiencia acumulada durante su extensa carrera, se ha dirigido a la construcción de su propio universo personal que permite reconocer las creaciones de Amaya a simple vista. 

Por esta razón, sus colecciones seducen y hablan por sí mismos, ya ves que no te dejan indiferente.

[Paqui Barroso](http://www.paquibarroso.es/)

Esta firma de nuevo apuesta por el estilo romántico, cuerpos de talle altos y el detalle de flores en tonos empolvados creando una armonía junto a encajes de guipur venidos de la india.  
Una colección inspirada en el buen gusto y la combinación de tejidos naturales que caracterizan esta Marca.

Soñar, crear y combinar cada idea en una prenda especial es lo que caracteriza a esta gran marca.

[Rubio Kids](http://www.rubiokids.com/en)

En la colección Bohemian Garden encontramos vestidos largos y cortos creados con materiales seleccionados en los mejores telares franceses, italianos y españoles. Crepe de seda, sutiles plumetis, muselinas, gasas y tules que se convierten en un juego de capas que imitan, a la perfección, los pétalos de las flores al superponerse sobre las delicadas figuras de los patrones de Rubio Kids. Tejidos que contrastan con detalles en materiales más terrenales, como el punto o el lino, que dotan a las piezas de una fuerza especial.

Toda la colección Spring/Summer 17 de la firma respira un aire vaporoso y una dulzura sofisticada, gracias a la paleta en tonos rosa pastel y amelocotonados que envuelven a sus diseños y que nos evocan a las praderas en flor y a las suaves puestas de sol.
## Galería completa de fotos

Para ver la galería completa de fotos del desfile de Comunión y ceremonia 2017 pincha [aquí](https://www.flickr.com/photos/142190598@N06/).

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).