---
layout: post
title: Celebración de cumpleaños solidario. Un cumpleaños diferente.
date: 2014-01-25T09:43:00+01:00
image: /tumblr_files/tumblr_inline_mzy6vbI47K1qfhdaz.jpg
author: maria
tags:
  - 3-a-6
tumblr_url: https://madrescabreadas.com/post/74474118432/celebración-de-cumpleaños-solidario-un-cumpleaños
---
Os cuento cómo fue el “cumpleaños solidario” de mis hijos?\
 Me va a costar ser objetiva porque tuve algunos problemas con el sitio donde lo celebré que me dejaron un terrible sabor de boca, dejaré para otro post el cabreo que me cogí con los responsables del local de bolas porque no quiero empañar algo tan especial y tan esperado para mis niños.\
 Lo importante es que ellos lo pasaron pipa, y logré reconducir el tema de la [avalancha de regalos](https://madrescabreadas.com/2013/05/31/el-valor-de-los-regalos-de-cumpleaños/) que suele haber en las celebraciones conjuntas del colegio hacia un “regalo solidario”, que compartieron con la ONG Aldeas Infantiles.

Al plantearnos cómo organizaríamos este año el cumple de los dos mayores, ya que coinciden prácticamente, teníamos claro que esta vez no podría ser un [cumpleaños tradicional](https://madrescabreadas.com/2011/06/20/aquellos-cumpleaños-felices/) en casa, como nos hubiera gustado, por el espacio, así que nos adelantamos por unirnos con otros niños del colegio para hacer una fiesta conjunta, pero como esto normalmente supone muchísimos invitados y regalos, decidimos sugerirles donar parte de su importe a alguien que lo necesitara más que ellos.\
 Así qué entramos en la web de Aldeas Infantiles y creamos un evento solidario, que consiste en una página web personalizable donde puedes subir incluso una foto y un texto donde explicar el tipo de evento. También existe la opción de mandar invitaciones por email con el enlace.\
 Ante nuestra propuesta las reacciones fueron diversas según la edad de los niños. Está claro que tienen que tener cierta madurez para comprender el significado del gesto de compartir sus regalos. Creo que a partir de los 8 años es la edad ideal para hacerlo, pero depende de la madurez de cada uno.

 ![image](/tumblr_files/tumblr_inline_mzy6vbI47K1qfhdaz.jpg)\
 A Princesita le encantó la idea y se sintió muy orgullosa ante sus compañeros de haber sido tan generosa por haber cedido parte de sus regalos a la ONG. La experiencia fue muy educativa y positiva. Ella recibió unos detalles muy bonitos de sus invitados, como un set de jardinería para cuidar de su planta, una mochila más grande de la que tiene, y una camiseta. Me pareció comedido y cosas necesarias. Agradecimos y apreciamos cada regalo y entregamos unos diplomas a las “cumpleañeras solidarias” que nos enviaron muy amablemente los de Aldeas infantiles.\
 ![image](/tumblr_files/tumblr_inline_n0h0asCFV31qfhdaz.jpg)

Para los invitados preparamos unas bolsitas con unos globos y unas pulseras solidarias, que lucieron orgullosos de haber colaborado. Fueron pequeños detalles que hicieron del cumple algo diferente, y con un sentido solidario.\
 A Osito, de 6 años, sin embargo, no le pareció bien la idea, ya que es pequeño todavía para comprender algunas cosas. A esa edad viven en un mundo donde son el centro, y les cuesta mirar más allá.\
 Por eso no lo forcé y decidí que viviera la experiencia a través de su hermana, y creo que le picó el gusanillo para el año que viene. Ya os contaré.\
 Lo curioso de todo fue ver las distintas reacciones de los padres al plantarles la donación de parte del regalo. Hubo a quien no le hizo mucha gracia, pero a la mayoría les encantó la idea y se mostraron colaborarivos. No os extrañe que cunda el ejemplo.\
 Princesita y yo agradecemos el trato y los detalles que tuvieron con ellas y con los invitados los de Aldeas Infantiles, y seguramente el año próximo repetiremos doblemente, si conseguimos convencer a Osito jajajaja…

Habéis hecho un cumple diferente alguna vez?