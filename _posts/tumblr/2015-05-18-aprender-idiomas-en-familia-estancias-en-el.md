---
date: '2015-05-18T12:58:40+02:00'
image: /tumblr_files/tumblr_inline_nog8olRPgL1qfhdaz_540.jpg
layout: post
tags:
- crianza
- vacaciones
- planninos
- educacion
- familia
title: Aprender idiomas en familia. Estancias en el extranjero.
tumblr_url: https://madrescabreadas.com/post/119269882504/aprender-idiomas-en-familia-estancias-en-el
---

Según lo expertos en los primeros años de vida es cuando la neuroplasticidad de nuestro cerebro es más florida. Esto quiere decir que en estas etapas nuestro cerebro esta mas preparado para aprender con mayor facilidad puesto que nuestras neuronas aún no se han especializado. Además en las fases de asimilación del lenguaje el niño cuenta una gran variedad de fonemas que con el tiempo si no se estimulan irán desapareciendo. 

_(psicóloga Maria Mora Ferron)_

Hoy entrevistamos a Nadia Poloni y Raquel Martín, responsables de la agencia [Language-in Study & Go](http://www.language-in.com/ES/s18-cursos-de-idiomas-para-familias-en-el-extranjero-2015.html), que organiza estancias en el extranjero para aprender idiomas en familia.

Me ha llamado la atención este tipo de viajes (y por eso os lo traigo al blog) porque ya sabéis que me ha picado el gusanillo de que los peques salgan fuera a reforzar el inglés que aprenden en el cole, ya que a pesar de que sea bilingüe de inglés, le veo muchas carencias, que creo que podrían ser salvadas con una buena inmersión lingüística en vacaciones.

El problema es que no me atrevo a mandarlos solos todavía, por eso veo interesante informarme sobre estos programas para toda la familia, porque incluso el más pequeño tendría cabida en el plan.

Os dejo la entrevista que he hecho a Nadia y Raquel:

 ![](/tumblr_files/tumblr_inline_nog8olRPgL1qfhdaz_540.jpg)

“ **- ¿Cómo fueron los inicios de Language-in?**

Desde 1999 empezamos a trabajar trayendo extranjeros aquí para estudiar cursos de español. Más adelante, en 2007, comenzamos a enviar a españoles al extranjero para que realizaran cursos en todas partes del mundo. Actualmente trabajamos con más de 300 escuelas repartidas por todo el globo.

**- ¿En qué consisten los programas para familias?**

R: Este tipo de programas son unas vacaciones donde toda la familia puede aprender idiomas realizando cursos de inglés y actividades en familia. Los cursos se organizan en grupos internacionales divididos por edades y niveles. Cada escuela tiene opciones diferentes, algunas realizan actividades todos juntos, otras ofrecen actividades adaptadas a cada edad…

 ![](/tumblr_files/tumblr_inline_nog8mmN6ME1qfhdaz_540.jpg)

**- ¿Qué tipo de familias reservan normalmente con vosotros? **

Tenemos familias de todo tipo: monoparentales, padres o madres que viajan solos con sus hijos, familias completas con 3, 4, 5 o más miembros de la familia (madres, padres, hermanos, abuelos, tios o amigos). 

 ![](/tumblr_files/tumblr_inline_nog8niOMQE1qfhdaz_540.jpg)

**-¿Es necesario un gran presupuesto para este tipo de viajes?**

Tenemos presupuestos para todos los bolsillos, desde los **programas Low Cost** como pueden ser los programas de Malta o Cork, hasta los programas para familias presupuesto más alto, como por ejemplo los programas de Hawái, Australia. Tenemos una amplia variedad de programas dependiendo de las edades de los niños y las necesidades y gustos de cada familia.

**- ¿Qué servicios ofrecéis a las familias?**

Ofrecemos cursos, servicio de guardería o canguro, actividades, traslados, alojamiento en familia de acogida o en apartamentos o casas privadas e incluso podemos ofrecerles los vuelos para más comodidad. Hacemos cada viaje a medida, dependiendo de qué necesita cada familia. Estamos a disposición de nuestros clientes las 24 horas del día, y si surge algún problema, lo solucionamos  lo antes posible. “ 

¿Qué os ha parecido? ¿conocíais este tipo de viajes?