---
date: '2015-08-13T16:22:34+02:00'
image: /tumblr_files/tumblr_inline_nsk2asqfp61qfhdaz_540.jpg
layout: post
tags:
- decoracion
- trucos
- planninos
- familia
title: Cumpleaños del peque con los Little Einsteins
tumblr_url: https://madrescabreadas.com/post/126589967219/decoracion-fiesta-littleeinsteins
---

Dice que ya no es un bebé, y con razón, aunque para mí sigue siéndolo.

Ha cumplido los 3 añitos, y la ocasión merecía una gran fiesta con la familia. Así que sus hermanos y yo nos pusimos manos a la obra par dale una sorpresa con los personajes de su serie de dibujos animados favorita, los [Little Einsteins](http://www.disney.es/disney-junior/brand/little-einsteins.jsp).

Hicimos algo sencillo, pero quedó muy resultón, la verdad.

Usamos tonos azules y rojo inspirándonos en el color del personaje favorito de mi niño, “Nave”, y el cielo por donde vuela.

 ![image](/tumblr_files/tumblr_inline_nsk2asqfp61qfhdaz_540.jpg)
## Los imprimibles

Compré una [banderola](https://www.etsy.com/es/listing/201903120/on-sale-30-off-diy-printable-little?ga_order=most_relevant&ga_search_type=all&ga_view_type=gallery&ga_search_query=little%20einsteins&ref=sr_gallery_10)  y unos imprimibles que estaban de oferta en Etsy con el dibujo de [Nave con los personajes de los Little Einsteins](https://www.etsy.com/es/listing/201902102/on-sale-30-off-little-einsteins-birthday?ga_order=most_relevant&ga_search_type=all&ga_view_type=gallery&ga_search_query=little%20einsteins&ref=sr_gallery_1) dentro, y me lo personalizaron con su nombre y la edad. 

Me sirvieron para el plato del cumpleañero, los vasos, los globos y para unos marcapáginas hechos con palo de polo como regalito para los invitados.

Los imprimibles no salen caros, y sin embargo dan un toque especial a la fiesta si sabemos jugar con los tonos de color de la vajilla, servilletas…

 ![image](/tumblr_files/tumblr_inline_nsk2bfRbPH1qfhdaz_540.jpg)

Los globos no quedaron perfectos, porque la curvatura que tienen hace que la pegatina no que de limpia, pero el efecto es chulo, y a mi peque le encantaron. 

 ![globos little einsteins](/tumblr_files/tumblr_inline_nshxrl9ZZ71qfhdaz_540.png)
##   

## El cartel

Su hermana le hizo un cartel en cartulina roja con la frase típica que repite Leo en cada capítulo los Little Einsteins:

“Tenemos… Una misión!!!”

 ![cartellittle einsteins cumpleanos](/tumblr_files/tumblr_inline_nsk2g3ENpV1qfhdaz_540.jpg)

## La fuente de chocolate

De postre pusimos una [fuente de chocolate](http://www.amazon.es/gp/product/B002UXQK4U/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B002UXQK4U&linkCode=as2&tag=madrescabread-21)

 ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B002UXQK4U)

para bañar en ella trozos de fruta, y fue todo un éxito. 

La compramos en Amazon porque estaba rebajada, y tenía opiniones muy favorables, y la verdad es que a los niños les entusiasmó (y a los mayores, que rebañaron el plato también).

El chocolate que se utiliza para las fuentes es especial para ello, y no es barato, la verdad, pero está de muerte. Viene en trocitos pequeños, que hay que calentar previamente en el microondas y echarlo ya líquido a la fuente.

Otro dato sobre las fuentes de chocolate, es que no se pueden usar al aire libre porque al ser tan fina la cortina de chocolate que cae, cualquier movimiento del aire podría ponerlo todo perdido, además de que cualquier partícula que cayera dentro podría estropear el rico manjar, incluso la fuente. Por eso hicimos la fiesta en el sótano.

 ![image](/tumblr_files/tumblr_inline_nshz3t4Fzd1qfhdaz_540.png)

Puse las brochetas para pinchar la fruta en moldes rojos de playa para hacer castillos de arena, pero resultó más fácil hacerlo con tenedores.

En los palomiteros puse nubes, aunque triunfó más la fruta.

También puse cuenquitos pequeños para llenar de chocolate para quien no quisiera estar de pie mojando cada trozo (aunque a los niños lo que les mola es eso, claro).

 ![image](/tumblr_files/tumblr_inline_nshybwFtZB1qfhdaz_540.png)
##   

## La tarta

Depués vino la tarta con las velas. Hice una ligera para no saturar al personal de dulce. La [receta de la tarta de obleas, que bauticé en este post como de Ferrero Rocher, la tenéis aquí](/2015/06/15/tarta-obleas-nutella.html).

 ![image](/tumblr_files/tumblr_inline_nshyesoEnr1qfhdaz_540.png)
##   

## La búsqueda del tesoro

Después, para quemar energía hicimos una búsqueda del tesoro de los Little Einsteins con un mapa que dibujó mi hija mayor, y se lo pasaron pipa.

 ![image](/tumblr_files/tumblr_inline_nshyim9b6q1qfhdaz_540.png)
##   

## El regalo

Su regalo fue, como no podía ser de otro modo, su Nave soñada…

 ![nave littlr einsteins](/tumblr_files/tumblr_inline_nshyl6iSYs1qfhdaz_540.png)

Pasamos una tarde en familia muy divertida, y la ilusión de sus ojos nos contagió a todos.

Felicidades, mi Little Eisntein!!

Que cumplas muchos más!