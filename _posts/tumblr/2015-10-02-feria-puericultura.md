---
layout: post
title: "Feria de Puericultura: Guía de compras para un recién nacido en 1 minuto"
date: 2015-10-02T18:09:04+02:00
image: /tumblr_files/tumblr_inline_o39zbmPsZB1qfhdaz_540.jpg
author: .
tags:
  - puericultura
  - 0-a-3
tumblr_url: https://madrescabreadas.com/post/130340966729/feria-puericultura
---
Este año la Feria de Puericultura de Madrid [ha vuelto a sorprenderme](/2014/10/03/puericultura-madrid.html).\
Cuando parecía que ya estaba todo inventado, resulta que no, que vuelven a salir productos innovadores, colores diferentes, tejidos de pret a porter, y mejoras en la seguridad.

## Color, seguridad e innovación en la Feria de Puericultura de Madrid

La puericultura es un sector en el que no se descansa, por lo visto, ya que las diferentes marcas siempre están tratando de mejorar lo que ya ofrecen para intentar adaptarse cada vez más a las necesidades de los padres y madres. Algunas parece que se han metido en nuestra cabeza para saber lo que pensamos porque las madres no paramos de dar vueltas a nuestra cabecita desde que nos enteramos que estamos embarazadas, incluso antes de ver por primera vez a nuestro bebe en la [primera ecografia](https://madrescabreadas.com/2021/05/25/ecografia-4-semanas-no-se-ve-nada/).

 ![](/tumblr_files/tumblr_inline_o39zbm6Lex1qfhdaz_540.jpg)

En mi paso por los diferentes stands, he ido seleccionando los productos que me han parecido más recomendables por su durabilidad (evolutivos), seguridad (cumplen normativa), originalidad (diseños rompedores) e innovación tecnológica (ofrecen algo diferente).

He elaborado esta guía de compras para ayudamos a decidir, pero ya sabéis que la última palabra siempre es vuestra:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SqhSJn2G364" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Bicicletas con carrito para llevar a los niños Thule

Me quedo con sus soluciones para llevar en bici a los más pequeños. Las sillas Thule Rider Along (9 meses a 6 años) y Thule Rider Along Mini (9 meses a 3 años) ¡Estoy deseando probar su carrito [carrito Chariot Cougar](https://amzn.to/3UJO0Uw) 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ca9ups7BPbA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Silla de coche a contra marcha Axkid

Nos presenta sus sillas de auto a contra marcha. Aquí tenéis la Rekid:

<iframe width="560" height="315" src="https://www.youtube.com/embed/a1PnAswhjcw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Yo estoy testeando con mi pequeño la Minikid. El resultado os lo contaré próximamente.

## Cochecitos seguros Bebecar con tejidos innovadores

Enamora con un stand con colores y tejidos de última moda.

Lo que más me ha gustado es la silla Spot+, similar a la [Spot que ya testeé en este blog](/2015/09/20/test-spot-bebecar.html), pero el manillar pasa a ser corrido, aunque se seguirá plegando en bastón.

 ![](/tumblr_files/tumblr_inline_o39zbn2uf41qfhdaz_540.jpg)

## Muebles para habitaciones infantiles Trama

Innova con el color “mint” en muebles de habitaciones infantiles, y con un estilo romántico con madera envejecida que da un toque vintage.

 ![](/tumblr_files/tumblr_inline_o39zbo5VgS1qfhdaz_540.jpg)

## Silla a contra marcha Gavity de Jané

Os recomiendo muy mucho su silla a contra marcha Gravity hasta los 105 cm de estatura del niño, y reversible, para dar la opción de ponerla en sentido de la marcha si se prefiere (aunque yo siempre recomiendo que aguanten a contra marcha el mayor tiempo posible por su seguridad).

<iframe width="560" height="315" src="https://www.youtube.com/embed/hO4397g2zUk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Ya [testeamos en el blog la anterior silla EXO](/2015/04/14/seguridad-infantil-en-autom%C3%B3vil-testing-silla-exo.html), pero ahora, Jané se ha adelantado a la implementación definitiva de la normativa ISize, y lanza también su silla grupos I, II y III Grand.

Hace unos meses tuve el honor de [visitar el Crash Test Center de Jané](/2015/02/18/seguridad-retencion-infantil.html), y quedé impresionada con lo importante que es invertir en seguridad para salvar vidas.

## Bañera inteligente  de Babymoov

Nos facilita la vida con sus bañeras inteligentes (las tenéis en el video de guía de compras):

[Aquanest, que ya probamos en el blog con la pequeña Rocío](/2015/09/14/banera-bebe-aquanest.html), y que mantiene la temperatura del agua estable durante 10 minutos

Aquascale, con monitor donde aparece el peso del bebé y mide la temperatura.

Y con su hamaca alta Swoon Up, para que el bebé esté a la altura de los demás, y participe más de la vida familiar, además de evitarnos muchos dolores de espalda. Además, como embajadora de Babymoov tendré la suerte de testearla para compartir mi experiencia con vosotros en el blog a través de la iniciativa #PadresTester

## Osito para dormir Buddy de Miniland

El osito Buddy, con un dispositivo que se maneja desde el móvil o Tablet, capaz de reproducir sonidos maternos para relajar al bebé con sonido del corazón o del vientre materno o, incluso podemos grabar nuestra voz, o cantarle nanas… ¡Una buena idea para regalar estas Navidades!

<iframe width="560" height="315" src="https://www.youtube.com/embed/qOe6yOmMKEo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Silla de paseo ligera de Asalvo

Rompe con diseños muy originales coordinables entre distintos productos.

Puedes elegir uno, y llevarte todo igual. Mi favorito, el de los chinitos (en el video de guía de compras).

Destaco también su silla Boop, con un diseño muy novedoso y una amplísima y accesible cesta muy práctica.

 ![](/tumblr_files/tumblr_inline_o39zbr9suX1qfhdaz_540.jpg)

## Trona convertible evolutiva de Chicco

Apuesta por la puericultua evolutiva y lanza su trona convertible Polly, que abarca desde el nacimiento hasta que el niño se siente en la mesa como uno más.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8D03mH8BQSk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Diseños originales de Tuc Tuc

Muestra antagonismo con la colección Weekend, que nos transporta al pasado, y nos da la sensación de estar viendo una película en blanco y negro.

Realmente sorprendente, frente a su colorido y viveza tradicional.

## Sillas de bebe de Inglesina

Es viajar en primera, desde luego, y me conquista con su silla todo terreno.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XOLzSb9v4EE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Retractor automatico de arnes de las sillas de CasualPlay

También apuesta `por la seguridad, y presenta algo que deberían tener todas las sillas de auto: el retractor automático de arnés. Es decir, un sistema similar al cinturón de seguridad de los adultos, que sujeta el cuerpo de una forma más segura.

Las marcas comprometidas con la seguridad me dan mucha confianza.

## Personaliza todo con Tutete

Me demostró que puede personalizar casi cualquier cosa. Y si no la tienen, la buscan. Preocupados por ofrecer materiales prácticos y seguros, sus baberos son de algodón orgánico y el dibujo con que los personalizan es de pintura ecológica y que no se cuartea con el uso.

Sus vasos son de un material especial que se puede meter al microondas y lavaplatos, además de tener una rugosidad agradabel al tacto que evita que se les resbalen a los más pequeños.

 ![](/tumblr_files/tumblr_inline_o39zbsFCVE1qfhdaz_540.jpg)

Dentro del stand de Tutete me encontré con este tesorito de Kakuii, a cuyas creadoras tuve el placer de entrevistar no hace mucho. Mi favorita sigue siendo la canastilla japonesa de shushi.

## El Carro gemelar mas estrecho de Baby Monster

Siempre me rompe los esquemas, esta vez con el gemelar Easy Twin 2.0, que cabe por las puertas normales.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hXAAzsbhmdI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Y, si el año pasado fueron los lunares, este año ha estampado sus cochecitos con la carta de ajuste.

## Alimentador para guardar la comida del bebe regulable de Saro

Da una vuelta de tuerca más a los accesorios de alimentación con este alimentador regulable en altura para que el trocito de fruta que le metamos al bebé vaya subiendo conforme lo va acabando. Un acierto!

<iframe width="560" height="315" src="https://www.youtube.com/embed/DrK9yUwA6SI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Moises para bebes de Uzturre

Lo clásico siempre está de moda, y esta marca lo borda. Su estética deliciosa me embaucó. Yo sigo apostando por los moisés. Me parecen ideales y muy recogiditos para los bebés recién nacidos.

 ![](/tumblr_files/tumblr_inline_o39zbsdOEI1qfhdaz_540.jpg)

## Pijamas termicos de disfraces de animales  de Sacopingüino

Rompe con pijamas térmicos que son disfraces de lo más originales para que la hora de irse a la cama se una fiesta, y no haya que estar persiguiendo a los niños para que se pongan el pijama. (El problema será que no se lo querrán quitar por la mañana).

 Los tienes en <https://www.penguinbag.com>

Mis favoritos: el pigüino, el pirata y el astronauta.

 ![](/tumblr_files/tumblr_inline_o39zbt5ZVi1qfhdaz_540.jpg)

Mi agradecimiento a la organización de IFEMA por haber contado con este blog para haceros llegar las novedades de un sector que cada año se supera.

 ![](/tumblr_files/tumblr_inline_o39zbuBvh21qfhdaz_540.jpg)



Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.