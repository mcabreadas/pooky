---
date: '2016-06-29T10:00:47+02:00'
image: /tumblr_files/tumblr_inline_o9it8w08wZ1qfhdaz_540.png
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: Cuatro recetas fresquitas de verano
tumblr_url: https://madrescabreadas.com/post/146646133539/recetas-verano-ninos
---

A veces las madres tememos la hora de irnos a la playa en verano porque nos cabrea que todos estén de vacaciones menos nosotras, ya que tenemos que hacer las mismas tareas que en casa, pero en otro escenario agravado por la omnipresente arena, los niños descontrolados, el calor y, probablemente, menos comodidades que en nuestra casa.

Si a eso le añadimos el tener que pasarnos la mañana en la cocina preparando la comida, pues oiga, yo me quedo donde estoy, y que se vaya quien quiera a la playa.

Por eso te doy estas cinco ideas de platos nutritivos y fresquitos, con los que mancharemos poco al elaborarlos y que te podrás dejar hechos en poco tiempo antes de bajar a la playa con los niños, para que cuando vuelvas, sólo tengas que servirlos.

¡Además, a los niños les encantan!

## 1-Ensalada legumbres
 ![](/tumblr_files/tumblr_inline_o9it8w08wZ1qfhdaz_540.png)

**Ingredientes:**

- Un bote de habichuelas(cuando lo compres guárdalo boca abajo para que no cueste vaciarlo y se te rompan)  
- 2 latas de atún en aceite de oliva  
- ½ bote de picadillo para ensaladilla: (variante) zanahoria, pepinillo… en vinagre -1 cebolla tierna pequeña (opcional)  
- 4 trozos pimiento morrón de bote  
- 2 huevos duros  
- sal  
- aceite  
- vinagre  
- perejil  

**Como se hace: **

- Echa las alubias en una escurridera y lávalas con agua abundante (si previamente el bote ha estado almacenado en tu despensa del revés te resultará más fácil sacarlas y no se te romperán).Puedes darles un hervor para quitarles el sabor a bote si eres muy quisquillosa. Si no, basta con la selas bien. Deja escurrir.  
- Mientras, cuece los huevos y escurre bien el picadillo variante de verduras.  
- Pon las alubias en un bol o fuente y añade el picadillo, los huevos troceados, el atún y, si le gusta a los niños, cebolla muy picadita y el pimiento a trocitos pequeños.  
- Añade sal, aceite y vinagre al gusto.  
- Termina con el perejil.  
- Remueve y al frigorífico.  
- Sirve fresquito.  

## 2- Arroz a la cubana
 ![](/tumblr_files/tumblr_inline_o9it1eHdKv1qfhdaz_540.jpg)

**Ingredientes:**

- 1 tacita de arroz por persona  
- 1 bote de tomate frito  
- 1 huevo por persona  
- salchichas frankfurt  
- 1 paquete de bacon  
- 1 plátano (opcional)   
- harina  

**Como se hace:**

- Cuece el arroz en abundante agua con sal durante 20 minutos: 10 a fuego fuerte y 10 a fuego lento para que quede suelto.  
- Lava y deja escurrir.  
- El bacon, las salchichas y los huevos los puedes hacer al horno para que sean más saludables y ensuciar menos.Usa papel de horno.  
- Si os gusta puedes añadir el plátano: córtalo en rodajas, pásalo por harina y fríelo con aceite a temperatura media.  
- Coloca el arroz escurrido en una fuente, cúbrelo con el tomate, y presenta el resto de alimentos aparte para que cada uno se componga su plato como quiera.  

## 3-Ensalada de verano o campera

**Ingredientes:**

- 4 tomates  
- 2 patatas  
- 2 huevos  
- 2 latas atún en aceite  
- ½ cebolla tierna  
- 1 bote aceitunas  
- sal  
- aceite  

**Cómo se hace:**

Parte los tomates a trocitos (si eres tiquismiquis, pélalos)

Parte las patatas en dados medianos y cuécelas

Cuece los huevos y trocéalos

Parte la cebolla en juliana

Pon todos los ingredientes en un bol y sazona con sal y aceite al gusto.

Mete al frigo un par de horas

Sirve frío

## 4-Pastel de pan de molde
 ![](/tumblr_files/tumblr_inline_o9it3tvMjr1qfhdaz_540.png)

**Ingredientes:**

- 1 paquete de pan de molde sin corteza  
- Mayonesa  
- Ketchup  
- 2 huevos  
- 1 Lechuga  
- 2 tomates  
- 2 latas de atún  
- 2 Zanahoria  
- Sal  

**Cómo se hace**

- En una fuente cuadrada coloca una capa de rebanadas de pan de molde.  

- En un bol mezcla la lechuga picada con el atún y la mayonesa y un poco de ketchup al gusto y pizca de sal, y cubre el pan de molde.  

- Coloca otra capa de pan de molde y úntala con mayonesa.  

- Coloca encima zanahoria rayada, 1 huevo duro a rodajas y 2 tomate a rodajas.  

- Cúbrelo con otra capa de pan de molde y coloca sobre ella el resto de mezcla de lechuga.  

- Pon una última capa de pan de molde, úntala con mayonesa y coloca de nuevo zanahoria rayada, huevo duro.   

- Mete al frigo un par de horas.  

- Sirve frío (cuidado con la mayonesa en verano)  

Espero que te hayan servido estas ideas y les encanten a tus peques.

¡A disfrutar del verano!

Si te ha sido útil, comparte en tus redes sociales este post pinchando el icono que tienes justo debajo :) 

¡Gracias!