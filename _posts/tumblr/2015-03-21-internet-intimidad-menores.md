---
date: '2015-03-21T11:09:11+01:00'
image: /tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- tecnologia
- derechos
- seguridad
title: Intimidad de los menores. Derecho al olvido en Internet
tumblr_url: https://madrescabreadas.com/post/114206854729/internet-intimidad-menores
---

Os propongo un experimento que quizá muchos no hayáis hecho nunca: Introducid vuestro nombre y apellidos en el buscador de Google. Nos ofrecerá una serie de enlaces con la huella digital que hemos ido dejando desde que empezamos a usar internet, y quizá alguno de ellos no nos guste porque esta información está al alcance de cualquiera que quiera saber de nosotros.

Esto da bastante qué pensar, ¿verdad?

Ahora pensad que dentro de 20 años nuestros hijos están participando en un proceso de selección de personal de una empresa multinacional, y el director de recursos humanos de la misma hace el mismo experimento que os acabo de proponer, pero con la variante de que nuestros hijos han empezado a aparecer en internet desde sus primeros momentos de vida, incluso desde el seno materno cuando nos dio por subir a Facebook nuestra barriguita pintada.

 ![image](/tumblr_files/tumblr_inline_nlk26p2NY51qfhdaz_500.jpg)

Este dato puede carecer de relevancia para el director de recursos humanos, incluso puede llegar a enternecerse, pero quizá cuando vea su primera borrachera u otras fotos haciendo el ganso en Instagram nuestro hijo pierda esta oportunidad de empleo.

Esto es sólo un ejemplo gráfico para mostraros la importancia de la huella digital que todos vamos dejando en la red, y para hacer una llamada a la reflexión sobre el uso de internet que hacen o harán nuestros hijos, o incluso que hacemos sus padres cuando publicamos cosas sobre ellos alegremente.

Desde luego que hay una solución para “borrar” lo que no nos guste de Google, pero más vale ser consciente y prevenir, que curar.

## Doctrina del Derecho al Olvido en Internet

Menos mal que el Tribunal de Justicia de la Unión Europea admitió la doctrina del “Derecho al olvido” el año pasado. 

Este derecho ha sido acogido como una necesidad para dar respuesta a las divulgaciones que los motores de búsqueda realizan sobre personas físicas sin su autorización ni consentimiento y que pudieran perjudicarles a su honor o intimidad.

Sabed que si consideramos que alguna atenta contra nuestra intimidad o la de nuestros hijos, podemos exigir que Google la retire de los resultados de búsqueda de nuestro nombre o el de nuestros hijos.

Tras la [Sentencia de 13 de mayo de 2014 del Tribunal de Justicia de la Unión Europea](http://curia.europa.eu/juris/document/document.jsf?text=&docid=152065&amppageIndex=0&doclang=es&mode=lst&dir=&occ=first&part=1&cid=276332) Google ha facilitado un [formulario para solicitar la retirada de los resultados de búsqueda](https://support.google.com/legal/contact/lr_eudpa?product=websearch&hl=es) que nos puedan perjudicar. 

## Pasos para exigir la retirada de un resultado de Google

1- **Solicitarlo a la fuente** mediante un email con una petición formal de que retire el enlace que nos perjudica.

2- **Solicitarlo a Google** mediante el formulario anteriormente mencionado.

Tras su estudio por un comité, Google resolverá si accede a la petición o no. 

3-Caso de que sea desestimada, podremos **recurrir a la Agencia Protección de Datos. **

Si se estima nuestra solicitud, el resultado será eliminado de los que aparecen cuando alguien busca nuestro nombre… pero sólo en España, ojo, en otros países sí que saldrá el resultado indeseable.

Por eso, como os decía antes, más vale prevenir, ¿no crees?