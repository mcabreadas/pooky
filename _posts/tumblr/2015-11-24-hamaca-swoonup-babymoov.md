---
layout: post
title: Probamos la hamaca alta Swoonup de Babymoov
date: 2015-11-24T10:30:37+01:00
image: /tumblr_files/tumblr_inline_o39z14JwEV1qfhdaz_540.jpg
author: .
tags:
  - puericultura
  - 0-a-3
tumblr_url: https://madrescabreadas.com/post/133854373034/hamaca-swoonup-babymoov
---
Te presento en primicia la [hamaca alta Swoonup](https://amzn.to/43BMWG8),  que descubrí en la [feria de puericultura Madrid 2015, como os contaba en este post](/2015/10/02/feria-puericultura.html), que es totalmente innovadora por varios motivos. 

 ![hamaca bebe babymoov](/tumblr_files/tumblr_inline_o39z14JwEV1qfhdaz_540.jpg)

Pero antes de contaros más, os dejo este video que hice cuando la desempaquetamos y montamos:

## Unboxing de la hamaca Swoonup

<iframe width="560" height="315" src="https://www.youtube.com/embed/togH-7EJHzI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Innovación por 3 motivos

1. Porque es regulable en altura, lo que permite que el bebé no esté en un plano inferior al resto de la familia, como ocurre con las hamacas de suelo, y también la espalda de los papás y mamás lo agradecerán, al no ser necesario agacharnos cada vez que tengamos que introducirlo o sacarlo de la hamaca.  
2. Se puede girar 360º, lo que nos facilita tener al bebé siempre a la vista.  
3. Sistema de plegado ultracompacto, y viene con una bolsa para trasladarla.   

 ![hamaca plegada babymoov](/tumblr_files/tumblr_inline_o39z15yZ8u1qfhdaz_540.jpg)

## Diseño

Como veis, tiene un diseño espectacularmente bonito que la convierte en un elemento decorativo más del salón, por lo que no querrás guardarla, te aviso. Se fabrica en dos colores: zinc y aluminio

## Regulable en altura

Tiene dos alturas:

* La posición base, de 55 cm, es la más baja, y permite que los pequeños descansen cuando no queramos que se entretengan con demasiados estímulos externos.  
* La posición alta, a 75 cm de altura, la usaremos cuando queramos que participen de la vida familiar.  

Podéis ver la demostración de las alturas en el video del unboxing del principio.

## Respaldo y asiento regulables

También podemos regular al gusto e independientemente,  tanto la inclinación del respaldo (3 posiciones) como del asiento (2 posiciones).

Podéis ver la demostración en el video del unboxing.

## Desde el nacimiento hasta los 9 Kg

La hamaca Swoonup viene con un cojín reductor para poder usarla desde los 0 meses, y quitarlo cuando el bebé sea más mayorcito. 

 ![reductor hamaca babymoov](/tumblr_files/tumblr_inline_o39z15jIc11qfhdaz_540.jpg)

La hamaca se puede usar hasta que el bebé pese 9 Kg.

El cojín es lo más mullido y suave que puedas soñar, en serio. Dan ganas de dormir sobre él cuando lo tocas. Los bebés deben de sentirse como en una nube, además de adaptarse totalmente a su fisionomía.

## Arco de juegos

Cuenta con un arco de juegos ajustable en tres posiciones, que no impide meter y sacar al bebé fácilmente.

 ![hamaca movil bebe](/tumblr_files/tumblr_inline_o39z15EeMM1qfhdaz_540.jpg)

## Seguridad

Para sujetar al bebé tiene un **arnés de seguridad de 5 puntos** acolchado y regulable a dos alturas.

 ![hamaca babymoov arnes](/tumblr_files/tumblr_inline_o39z16SVYQ1qfhdaz_540.jpg)

Tiene un sistema de bloqueo de rotación de la hamaca con un solo click para evitar que gire por accidente, por ejemplo cuando hay más niños correteando en las proximidades.

 ![hamaca swoonup bloqueo](/tumblr_files/tumblr_inline_o39z16WPTj1qfhdaz_540.jpg)

Patas con base antideslizante para evitar que resbale cuando introduzcamos o saquemos al bebé.

## Garantía de por vida

Babymoov invierte tanto en la calidad y seguridad de sus productos, que ofrece una garantía de por vida.

 ![hamaca swoonup babymoov](/tumblr_files/tumblr_inline_o39z17IMtJ1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39z17nvbe1qfhdaz_540.jpg)

Hemos probado la hamaca Swoonup con la familia de Blanca, un bebé de 8 meses, y 9 Kg de peso.

## Testimonio de la familia tester

 ![hamaquita alta bebe swoonup](/tumblr_files/tumblr_inline_o39z18IAqH1qfhdaz_540.jpg)

> Hay dos características de la hamaquita que yo destacaría como positivas fundamentalmente y que la diferencia de la mayoría de las hamaquitas que conozco.
>
> Una es que está elevada del suelo, porque eso da mucha comodidad a los papas a la hora de dejar y coger al bebe. Además permite al bebé, cuando ya tiene algunos meses, visualizarlo todo desde la misma perspectiva que los demás.
>
> El único inconveniente de la altura podría ser que el bebé se cayera pero atándolo bien no hay ningún problema.
>
> La otra característica destacable es que lleva un cojín reductor que acoge al bebe cuando es recién nacido y que si lo quitas te permite usar la hamaquita aun cuando el bebe tiene mayor tamaño.
>
> Otra cosa importante es que se puede trasladar de una habitación a otra fácilmente ya que el bebe normalmente no quiere estar solo. E incluso si tuviera algún asa para poder arrastrarla aun cuando el bebe esta encima seria aún más ideal.
>
> El lavado es muy bueno, la funda es fácilmente extraíble mediante cremallera, y se seca súper rápido. La estructura se queda como nueva con un paño húmedo.

Muchas gracias a nuestros padres tester por contarnos su experiencia con la [hamaca Swoonup](https://www.amazon.es/gp/product/B00N8YWSKK/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=madrescabread-21&camp=3638&creative=24630&linkCode=as2&creativeASIN=B00N8YWSKK&linkId=409db60c8e75a7290a39791f889d4959), y a Babymoov por haber elegido este blog para dar a conocer en primicia su novedosa hamaca elevable.

¿Qué os ha parecido?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.