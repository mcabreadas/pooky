---
date: '2013-06-14T12:11:00+02:00'
layout: post
tags:
- mecabrea
- salud
- crianza
- familia
title: Protegete contra el cancer de piel
tumblr_url: https://madrescabreadas.com/post/52938121399/día-europeo-del-cáncer-de-piel-ayer-13-de
---

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/_Mv3Bmvc5wU?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

## Día Europeo del Cáncer de piel

Ayer,  **13 de junio**  2012 se celebró el  **DÍA EUROPEO DE LA PREVENCIÓN DEL CÁNCER DE PIEL, ** con el objetivo de concienciar a la población sobre la importancia de adoptar buenos hábitos relativos a la exposición solar.

La campaña del Euromelanoma es una iniciativa europea para informar a la población sobre la prevención y el diagnóstico precoz del cáncer de piel.

La incidencia de melanoma se ha duplicado en Europa entre los años 60s-90s, atribuible a la exposición intensa y creciente al sol. La incidencia de cáncer espinocelular y basocelular ha aumentado en todos los países europeos. Aunque mucho menos peligrosos para la vida que el melanoma, estos tumores representan el 95% de todos los cánceres de piel.

La mejor protección contra el sol es:

-  **evitar en lo posible la exposición** , al menos entre las 11:00 y las 16:00 horas (período en que la exposición ultravioleta es mayor).

-  **usar pantallas**  (cremas solares).

Con este motivo os dejo este divertido video elaborado por Isdin que nos muestra en clave de humor la evolución en la manera de protegernos del sol a lo largo de la historia.

Nuestra labor como padres es educar a nuestros hijos en la importancia de protegerse adecuadamente del sol, no sólo limitándonos a aplicarles cremas adecuadas y de alto factor de protección, sino haciéndoles ver el por qué, enseñándoles cómo inciden los rayos solares en nuestra piel, y el efecto de los protectores solares. Y quizá dejen de protestar a la hora de poner la crema, incluso se animen a ponérsela solos, y acabarán recordándonos a nosotros que la saquemos.

Para ello, Isdin ha lanzado una  **App gratuita **** que enseña a los niños a protegerse bien del sol con un divertido juego  **que explica el contraste entre una mala y una buena fotoprotección, con un**  juego de plataformas ** ** y acción. ** Los IsdinSunProtectors, los superhéroes de la fotoprotección, vivirán ** una apasionante aventura** en la Isla Mágica, donde lucharán contra los poderosos rayos ultravioleta.

La aplicación se complementa con un juego de cartas coleccionables, pruebas en las que demostrar lo aprendido, una sección de consejos útiles sobre la exposición al sol y el concurso IsdinSunFest, donde el jugador que consiga la mayor puntuación podrá ganar un iPad Mini.

La nueva App está ya disponible para iPhone a través del Apple Store, para Android en Google Play y en la [plataforma digital IsdinSunLab.com](http://mobile.isdinsunlab.com) a través de:

Me dejas tu comentario?