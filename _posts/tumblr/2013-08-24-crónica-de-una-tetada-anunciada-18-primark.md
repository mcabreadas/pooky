---
layout: post
title: "Crónica de una tetada anunciada: 18 Primark."
date: 2013-08-24T19:46:00+02:00
image: /tumblr_files/tumblr_inline_pe6xvhATwI1qfhdaz_540.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - lactancia
  - bebes
tumblr_url: https://madrescabreadas.com/post/59212837815/crónica-de-una-tetada-anunciada-18-primark
---
[![image](/tumblr_files/tumblr_inline_pe6xvhATwI1qfhdaz_540.jpg)](http://www.laverdad.es/murcia/v/20130824/region/quien-mira-incomodo-20130824.html)

Foto gracias a La Verdad

A las 18:45 h del 23 de agosto me subía al tranvía camino de Primark con mi bebé de un año y mi hija mayor de siete. -“Mamá, ¿adónde vamos? ¿a eso de la lactancia?”. Y es que no es la primera vez que me acompaña a algún encuentro con otras mamás lactantes y sus hijos. La última fue en la semana mundial de la lactancia materna, en una tetada en una de las plazas más típicas de nuestra ciudad. -“Sí, hija, vamos a encontrarnos con otras mamás que dan teta a sus hijos para que la gente vea que lo que hacemos es algo normal”.

Pasadas las 19:00h llegábamos a la quedada, más coloquialmente llamada “tetada”, aunque a las organizadoras no les gusta este término porque en realidad no se trataba de llegar y ”Hala, todas tetas fuera!”, sino de reunirnos las familias y entregar una queja al responsable de la tienda Primark, y si en el transcurso de los acontecimiento algún niño pedía pecho, pues, con toda normalidad, se le daría, como en cualquier otra situación cotidiana. Pero en realidad es el término que ha usado la [prensa](http://www.huffingtonpost.es/2013/08/23/tetada-madres-amamantar-primark_n_3805094.html?utm_hp_ref=spain) finalmente para hacerse eco de los encuentros en diferentes puntos de España.

Al llegar ya vi a Fuensanta Pina, la organizadora del evento en Murcia, afanada con las hojas de firmas para solicitar que se impulse una ley que proteja la lactancia en público, similar a la que existe en otros países como Reino Unido. Enseguida supe que era ella… bueno, en realidad la confundí con su hermana, ya que tienen cierto parecido. Me puso delante dos copias (una para el Defensor del Pueblo y otra para el responsable de la tienda) y me informó de que a una de las tetadas había ido una periodista del Huffington Post. Eso así, nada más empezar me dio idea de la magnitud del asunto.

 ![image](/tumblr_files/tumblr_inline_pe6xviwEYQ1qfhdaz_540.jpg)

Tras cambiar algunas impresiones y conocer a [Daniela](https://m.facebook.com/TenemosTetas/posts/340333966011952), la chica que fue expulsada hace un año por dar de mamar precisamente del Primark en el que ahora nos encontrábamos, nos dispusimos a entrar en el local todas las familias junto con un periodista del diario [La Verdad](http://www.laverdad.es/murcia/v/20130824/region/quien-mira-incomodo-20130824.html) de Murcia para presentar la queja. Subimos a la segunda planta bajo la atenta mirada del guardia de seguridad en un ejemplo de civismo, educación y tranquilidad, incluídos los niños. Algunas madres incluso dieron de mamar a sus hijos mientras caminábamos por la tienda sin que nadie les dijera nada.

La responsable de la tienda nos acompañaba con una sonrisa en los labios en todo momento, fruto más bien de la tensión que estaba soportando la pobre, no sé si pensaría que íbamos a organizar algún escándalo, pero la verdad es que tanto su comportamiento como el nuestro fue ejemplar, y aceptó firmar la recepción del escrito sin problemas. Menos mal que una empleada rompió el hielo y empezó a bromear con los niños y a hablar de sus sobrinos pequeños.

Al terminar salimos a la puerta del Centro Comercial Nueva Condomina para inmortalizar el momento con una foto de grupo, que acabamos con un aplauso espontáneo y una gran satisfacción, ya que habíamos estado preparando los diferentes encuentros en [18 Primark](http://tetadaprimark.wordpress.com/lugar-tetadas-23-agosto-2013/) de España durante mucho tiempo, y era como la culminación de los esfuerzos de muchas mamás desde que el día 10 de agosto Carmen Vega hiciera público su incidente con la cadena de tiendas.

 ![image](/tumblr_files/tumblr_inline_pe6xviCWqi1qfhdaz_540.jpg)

Pero lo más emocionante fue la cantidad de mamás que nos acompañaron desde las redes sociales sin poder estar en Primark físicamente. Fue un auténtico aluvión de tuits con fotos amamantando a sus bebés lo que inundo Twitter a las 19:00h en una preciosa tetada virtual. Gracias a todas! Sentimos vuestro apoyo! Estas son algunas de las fotos que nos regalaron:

 ![image](/tumblr_files/tumblr_inline_pe6xvjwnJD1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pe6xvjDdRl1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pe6xvjKEaF1qfhdaz_540.jpg) ![image](/tumblr_files/tumblr_inline_pe6xvj7rxd1qfhdaz_540.jpg)

Gracias a [@Mousikh](https://twitter.com/Mousikh), @Padresenpanales, [@Ana_biznaga](https://twitter.com/ana_biznaga) y [@PicatosteAinara](https://twitter.com/PicatosteAinara) por las preciosas fotos.

FIRMA LA PETICIÓN PARA UNA LEY  que proteja la Lactancia materna en público:

Pero esto no acaba aquí. Este movimiento no ha hecho más que empezar y tomar impulso, ahora viene la carrera de fondo para conseguir una ley que proteja la lactancia materna en público. 

Os mantendré informados.

Os dejo los enlaces de prensa que se hacen eco de la movilización en toda España:

**MEDIOS NACIONALES:**

\-[Huffington Post ”Tetada española”](http://www.huffingtonpost.es/2013/08/23/tetada-madres-amamantar-primark_n_3805094.html?utm_hp_ref=spain) [](http://www.huffingtonpost.es/2013/08/23/tetada-madres-amamantar-primark_n_3805094.html?utm_hp_ref=spain)

 ![image](/tumblr_files/tumblr_inline_pe6xvkAOFG1qfhdaz_540.jpg)

\-La Razón: “Luchamos por normalizar un instinto natural”

 ![image](/tumblr_files/tumblr_inline_pe6xvkBv2L1qfhdaz_540.jpg)

\-[La Sexta TV](http://m.youtube.com/watch?feature=youtube_gdata_player&v=u9NQtNtYs1s&desktop_uri=%2Fwatch%3Fv%3Du9NQtNtYs1s%26feature%3Dyoutube_gdata_player)

\-REPORTAJE LA RAZON 

**MEDIOS LOCALES**

\-MADRID: [Madridiario.es](http://madridiario.es/galerias/primark/tetada/lactancia-materna/7278) : “Tetada contra Primark”

\-ELCHE: Elche Diario:[](http://www.elchediario.com/display.aspx?id=11137)“La tetada contra Primark reúne más de 100 personas en Elche”

 ![image](/tumblr_files/tumblr_inline_pe6xvkrXHe1qfhdaz_540.jpg)

\-ELCHE: Diario Informacion: [“Tetada e protesta en Elche”](http://www.diarioinformacion.com/elche/2013/08/24/tetada-protesta-elche/1408480.html)

\-MURCIA: La Verdad [“Aún hay quien nos mira incómodo”](http://www.laverdad.es/murcia/v/20130824/region/quien-mira-incomodo-20130824.html)

\-VALLADOLID: El norte de Castilla: “Decenas de mujeres reivindican la lactancia materna frente a Primark”

\-ALICANTE: Diarioinformación.es: “Protesta de lactantes en Primark”

\-CANTABRIA: Eldiariomontanes.es: “Las madres lactantes en contra de Primark”

\-BARCELONA: La Vanguardia: Video

\-CASTILLA Y LEÓN: Radiotelevisión Casitlla y León: Video “700 madres de toda España protestan contra el tabú de amamantar”

**OTROS MEDIOS DIGITALES**

\-[e](http://www.elboletin.com/smartphone2/index.php?noticia=82215)lboletin.com: ”Convocan una “tetada” en tiendas Primark de toda España contra la expulsión de madres que amamantan a sus hijos.

\-eldiario.es:[“Tetadas frente a Primark contra la discriminación de mujeres que dan el pecho”](http://www.eldiario.es/sociedad/Primark-madreslactantes-tetada_0_167633707.html%20http://www.periodismodigno.org/tetada-protesta-de-madres-lactantes-contra-primark/)

 ![image](/tumblr_files/tumblr_inline_pe6xvlC60Y1qfhdaz_540.jpg)

MEDIOS EXTRANJEROS

\-TV RUSIA: Ruptly TV

\-TV CHINA

\-ITALIA