---
date: '2013-07-29T00:36:00+02:00'
image: /tumblr_files/tumblr_inline_mqi8jjmyYo1qz4rgp.jpg
layout: post
tags:
- trucos
- recomendaciones
- familia
title: 'Bebidas sanas sin burbujas: Pompadour "refrescos sin agua"'
tumblr_url: https://madrescabreadas.com/post/56726475353/bebidas-sanas-sin-burbujas-pompadour-refrescos
---

Pompadour me invitó a probar y dar mi opinión sobre su nueva línea de**[](http://www.pompadour.es/refresco-sin-agua/informacion.html)infusiones y té**  **Pompadour**  **“Agua Fría”,** que te permiten hacerte una bebida refrescante al gusto añadiendo sólo agua fría, hielo y azúcar, si lo deseas, en el momento de consumirlo. Está disponible en 3 sabores: Té con limón, Manzana con hibisco, y Té verde con hierbabuena.

Me pareció una bebida versátil en el sentido de que se le puede dar la presentacion que se ajuste a la ocasión y la hora del día en que se quiera disfrutar.

Se me ocurrieron dos ideas:

Un día servirla bien fría en una reunión chillout con amigos al atardecer, y otro día tomarla a la hora del té de una forma más tradicional, aunque fresquita porque la época del año en la que estamos así lo requiere.

 ![image](/tumblr_files/tumblr_inline_mqi8jjmyYo1qz4rgp.jpg)

Como mi madre es una amante del té, quedamos una tarde en su casa de campo para probarlos juntas, pero en realidad la entendida es ella y lo que os voy a contar es su opinión.

Además, como buena aficionada al té, lo sirvió al estilo tradicional, pero lo que había en la tetera era agua fría! Y tuvo el gusto de decorar la mesita donde siempre lo toma con sus amigas según el sabor que degustábamos en cada caso con hierbas y flores de su jardín, y frutos de su huerto.

Lo hizo con tanto mimo que si gano el concurso de posts de Pompadur le regalo el ipad mini a ella que, además está deseando iniciarse en esto de las nuevas tecnologías.

**Opinión sobre el té con limón**

Se trata de té negro acompañado de hibisco, raíz de regaliz y corteza de limón.

El te de limón contiene altas dosis de vitamina C, lo cual nos ayuda a preservar el colágeno y dar mas fuerza y elasticidad a nuestros huesos, articulaciones y las paredes de nuestras venas y arterias.

La vitamina C que contiene también nos ayuda a reforzar nuestro sistema inmunológico, a mejorar la absorción de hierro y combatir ciertas enfermedades como la artritis, hipertensión y menorragia.

También es rico en calcio.

Mi madre define su sabor como “tradicional, clásico, intenso y amable”, además de ser el que más le ha gustado.

 ![image](/tumblr_files/tumblr_inline_mq9xj5qrrw1qz4rgp.jpg)

**Opinión sobre el té de manzana con hibisco**

Combinación de manzana e hibiscus sabdariffa, también llamada rosa o flor de Jamaica. La mezcla se redondea con hibisco blanco, flor de sauco y escaramujo.

Son muchas las propiedades del hibiscus: **favorece la digestión** , tiene un ligero poder laxante y es **muy diurética.**

Existen estudios que están corroborando que este poder depurativo favorece el control del **colesterol** y los **triglicéridos** , por lo tanto, **previene enfermedades cardiovasculares** y actúa sobre la hipertensión ( **baja la tensión** ).

Éste lo describe como con un “regusto ácido y un bonito color”, y es el que menos le ha gustado de los tres.

 ![image](/tumblr_files/tumblr_inline_mq9xk4ZYWv1qz4rgp.jpg)

**Opinión sobre el te de manzana con hierbabuena**

Té verde con dos aromáticas plantas: hierbabuena y menta.

Es uno de los más poderosos antioxidantes. Su alto contenido de catequinas e isoflavonas lo transforman en un perfecto aliado para luchar contra el envejecimiento, colaborar con la circulación y evitar el endurecimiento de las paredes arteriales, entre muchas otras cosas.

Mi madre destaca su “Sabor intenso, muy refrescante y muy aromático.”. A mí, la verdad, es el que más me gusta, por eso le regalé en juna ocasión el juego de té moruno que veis en la foto.

 ![image](/tumblr_files/tumblr_inline_mq9xl4Fn2O1qz4rgp.jpg)

Curiosidades sobre el té Pompadur con agua fría

¿Os imagináis que todos los refrescos de un camión se pudieran transportar en una bicicleta? ¿Os imagináis una bebida refrescante sana, barata y ecológica? ¿Os imagináis que por algo más de 2 € pudiérais comprar 20 refrescos?

A los refrescos tradicionales, se les añade el agua durante la fabricación (hasta un 95% de su contenido). Con este sistema no hacen falta latas ni botellas. Ni tantos camiones y combustible para distribuirlos. Ni llevar luego esos envases a reciclar… De paso libramos al océano de millones de botellas y tapones a la deriva.

Además son sanos y ligeros, ya que sólo contienen ingredientes naturales sin colorantes, conservantes ni azúcar añadido. Y conservan todo su sabor hasta el momento en que las activas con agua fría, y el contenido del sobrecito infusiona durante unos minutos, sorprendentemente, igual de rápido que si lo hiciéramos con el agua caliente

 ![image](/tumblr_files/tumblr_inline_mq9y0nnLzI1qz4rgp.jpg)