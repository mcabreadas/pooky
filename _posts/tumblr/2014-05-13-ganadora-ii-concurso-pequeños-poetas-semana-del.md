---
date: '2014-05-13T16:00:00+02:00'
image: /tumblr_files/tumblr_inline_pb455biJe11qfhdaz_540.jpg
layout: post
tags:
- crianza
- premios
- libros
- planninos
- educacion
- familia
title: Ganadora II concurso Pequeños Poetas. Semana del libro.
tumblr_url: https://madrescabreadas.com/post/85623224461/ganadora-ii-concurso-pequeños-poetas-semana-del
---

La madre ganadora del concurso de poemas infantiles [Pequeños Poetas](/post/83387119183/concurso-pequenos-poetas-ii-semana-del-libro) con motivo de la Semana del libro de este año nos cuenta en este post su experiencia con su hija de tan sólo siete años. Merece la pena vivir algo así, ya veréis cómo le hace despertar su inquietud por escribir y la ternura de su texto:

_“ **PRIMEROS VERSOS…** _

 ![image](/tumblr_files/tumblr_inline_pb455biJe11qfhdaz_540.jpg)

_Pasé varias veces de largo cuando vi la convocatoria en las Redes Sociales, pero mi mente la archivó en esa parcelita reservada a proyectos "futuribles” -una palabra muy de periodista deportivo- que desarrollar con las niñas. El blog “Madres Cabreadas, ahora familia numerosa” convocaba, con motivo del día del libro, la segunda edición del [Premio Pequeños Poetas](/post/83387119183/concurso-pequenos-poetas-ii-semana-del-libro). Una iniciativa encaminada a fomentar entre los más pequeños de la casa el gusto por la poesía poniéndolos a ellos mismos en la tesitura de escribir su propio poema. _

_Pero el pasado sábado, martirizada por la culpabilidad de tener que trabajar en fin de semana y “abandonar” a la prole a la buena de Dios, con padre y abuelos, me propuse aprovechar al máximo las dos horitas que, al mediodía, teníamos para disfrutar juntas. Unas pulseritas de gomas por aquí, un dibujito por allá y ¡zas! se me encendió la bombilla. Mi víctima, ¡quién si no! Flower Power. Para esta madre de letras y que ha hecho de contar historias su forma de vida, tener en casa a una niña que despunta como escritora es un privilegio. No os voy a decir que la criatura es un fenómeno -ni mucho menos-, pero que, con siete años y medio, le ponga tanto interés, ya es un primer paso. Su de por sí tendencia natural a escribir y, sobre todo, su capacidad para ilusionarse con cualquier nuevo proyecto -aunque la ilusión se le vaya pasados diez minutos- fueron el caldo de cultivo perfecto para que mi propuesta de participar en el concurso fuese recibida con semejante algarabía infantil. _

_¿Qué os voy a contar de la experiencia? Sinceramente, que no ha podido ser más satisfactoria. El resultado no es, de lejos, el que yo me esperaba, pues Flower Power tiene sus ritmos y si le aprietas un poquito las clavijas abandona fácilmente, así que desistí tras su segunda negativa a hacer una poesía con rima. Por lo demás, repasamos el proceso creativo -de qué quieres hablar, qué quieres contar, cómo lo quieres contar… ¿te gusta como ha quedado?, ahora dale otra vuelta a ver si sale mejor…-, hablamos de la figura del editor y, en este caso, mi papel como correctora ortográfica, practicamos caligrafía y también le dimos un empujón a las TICs, porque fue ella misma la encargada de pasarlo a máquina y subir el comentario al blog. Os voy a confesar que solo verla colocar sus dos manos sobre el teclado, en una posición y actitud que yo -y me considero pionera en mi quinta- no logré hasta bien entrado BUP, me dio vértigo… ¡Qué mundo tan diferente al nuestro! Diversión, refuerzo educativo, un buen número de competencias básicas potenciadas… no me digáis que no hice méritos a “la mejor madre de la semana”… Ahora nos queda la segunda parte, empaparme de algunos poemas más allá de los de Gloria Fuertes, que ya se que Primeros le encantan._

_ Y ahora llegó el momento de que conozcáis, casi en primicia mundial, el resultado. No voy a hacer ningún tipo de comentario acerca de que el protagonista sea su padre, y no yo, o de su capacidad para embellecer la realidad. _

**_“Papá, tu eres mi estrella_**

**_cada noche pienso en ti._**

**_ _**

**_Siempre eres cariñoso_**

**_y me ayudas._**

**_ _**

**_Cuando me caigo me levantas_**

**_y me curas con tus besos._**

**_ _**

**_Me gusta cuando te bañas conmigo,_**

**_me salpicas y saltamos olas._**

**_ _**

**_Tu me haces feliz _**

**_y yo estoy feliz contigo._**

**_ _**

**_TE QUIERO MUCHO”_**

_ _

Muchas gracias a Caludia y a su mamá, [@MerakLuna](https://twitter.com/MerakLuna) por participar, y muchas gracias por compartir vuestra experiencia. Ya tengo el libro listo para enviar, con una dedicatoria muy especial para nuestra pequeña artista.

[![image](/tumblr_files/tumblr_inline_pb455buBoI1qfhdaz_540.gif)](http://dl.dropboxusercontent.com/u/1942174/grandes_poes%C3%ADas_de_peque%C3%B1os_poetas.pdf)

Si os lo queréis descargar en formato digital podéis hacerlo [aquí](http://dl.dropboxusercontent.com/u/1942174/grandes_poes%C3%ADas_de_peque%C3%B1os_poetas.pdf) de forma gratuita.