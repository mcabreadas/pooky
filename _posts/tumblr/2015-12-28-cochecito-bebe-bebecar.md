---
date: '2015-12-28T10:00:14+01:00'
image: /tumblr_files/tumblr_inline_o39ysrwfSz1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- bebes
title: Un carrito Bébécar para mi sobrina
tumblr_url: https://madrescabreadas.com/post/136102496941/cochecito-bebe-bebecar
---

Queridos Reyes Magos:

Este año he intentado portarme lo mejor posible, pero a veces he metido la pata. Prometo mejorar, sobre todo ahora que voy a ser tía.

En enero nacerá mi sobrina, y puede que llegue con Sus Majestades si se adelanta un poco pero, aunque todos tenemos muchas ganas de que nazca, preferimos que venga para la fecha prevista.

Mis hijos están locos de contentos esperando a su nueva primita, y deseando cuidarla y pasearla con su cochecito (ya he comenzado a aleccionarlos para que no se peleen, y lo hagan por turnos).

Este año me atrevo a pedirles un regalo muy especial, y muy gordo, la verdad, soy consciente, pero sepan que no es para mí, sino para la niña que está en camino, y que alegrará nuestra familia en breve.

Me gustaría que nos dejaran un cochecito, pero no uno cualquiera, sino [Bébécar](http://www.bebecar.com/bebecar/es/) por varios motivos que intentaré explicarles, a ver si les convenzo:

## Seguridad y calidad
 ![](/tumblr_files/tumblr_inline_o39ysrwfSz1qfhdaz_540.jpg)

Estos cochecitos tienen una estructura sólida y están hechos con materiales de primera, que tienen una larga duración. Son de fabricación europea y cumplen toda la normativa escrupulosamente, lo que que garantiza la máxima protección para el bebé.

Y como, seguramente tengamos más sobrinos, Bébécar es la mejor opción, porque la idea es que el cochecito dure para todos.

## Fácil manejo

Sus ruedas delanteras grandes y giratorias, y su sistema de suspensión trasero hacen que el carrito se deslice suavemente, incluso si lo empujamos sólo con una mano, y permite salvar barreras arquitectónicas más fácilmente porque, además, la longitud del brazo está estratégicamente pensada para hacer palanca al levantarlo para subir bordillos sin necesidad de ejercer una gran fuerza.

 ![image](/tumblr_files/tumblr_inline_o39ystHOOV1qfhdaz_500.gif)

Las ruedas delanteras, aunque son giratorias, se pueden bloquear para mecer al bebé o para pasear por calles con adoquines, por ejemplo.

El freno es fácilmente accionable y seguro, lo que será importante para mi sobrinita, ya que su mamá lo va a manejar mucho por cuestas empinadas.

## Comodidad bebé

Este requisito es fundamental, comprenderán sus Majestades. El cochecito que pido lleva capazo y también silla para cuando mi sobrina crezca y ya se siente.

Tengan en cuenta que el capazo de Bébécar es reclinable, lo que me parece fundamental para mantener ligeramente incorporado al bebé después de las tomas, por las incómodas regurgitaciones.

En cuanto a la silla o hamaca, me encanta que esté lo suficientemente acolchada y que sea amplia para que la niña pueda dormir a gusto incluso cuando ya esté crecidita. Es es totalmente reclinable, y está homologada desde los 0 meses.

Además, como no sabemos si le gustará pasear de cara al público o mirando a sus papás, es importante que la silla sea reversible para adaptarla a sus gustos.

Que el brazo de seguridad sea extraíble proporciona una mayor comodidad a la hora de introducirla o sacarla.

## Aislamiento

El tejido del carrito que nos dejen sus Majestades debe ser especialmente aislante porque donde trabaja la mamá del bebé hace muuuucho frío, y como va a nacer en pleno invierno es fundamental que esté protegida.

Pero, al mismo tiempo, si fuera posible, también sería fundamental que aislara del calor, porque en verano en esta zona donde vivimos, las temperaturas se elevan mucho.

## Fácil plegado  

Para salvaguardar la espalda de la mamá, que tendrá que coger cada día el coche para ir a trabajar transportando el carrito el carrito, es fundamental que tanto la extracción del capazo, 

 ![image](/tumblr_files/tumblr_inline_o39ysvQzFJ1qfhdaz_500.gif)

como el plegado del chasis 

 ![image](/tumblr_files/tumblr_inline_o39ysxQRjo1qfhdaz_500.gif)

se hagan de un modo suave y fácil. 

Pero lo más importante es que no se tenga que agachar para hacerlo porque posiblemente sus lumbares se queden resentidas por el embarazo, o lleve puntos tras el parto.

Queridos Reyes Magos, espero haberles convencido de la bondad del cochecito que pido para mi sobrina, y de que no elijo Bébécar por capricho (bueno, también, porque los diseños son ideales, y de última moda), sino porque me parece la mejor opción por calidad, seguridad, comodidad y manejabilidad.

 ![](/tumblr_files/tumblr_inline_o39ysy5dQQ1qfhdaz_540.jpg)

Así que no tarden en dejármelo por favor, y póngan nuestra casa de las primeras en el recorrido de la noche del 5 de enero, no vaya a ser que llegue antes la niña que Uds.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.