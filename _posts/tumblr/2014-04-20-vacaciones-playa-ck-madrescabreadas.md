---
layout: post
title: Vacaciones en la playa con familia numerosa
date: 2014-04-20T19:00:00+02:00
image: /tumblr_files/tumblr_inline_pb455cC9hx1qfhdaz_540.jpg
author: maria
tags:
  - planninos
  - 0-a-3
  - 3-a-6
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/83316700351/vacaciones-playa-ck-madrescabreadas
---
Así me imaginaba yo de niña cómo sería tener una familia:

 <iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/BP_pE6JXV1U?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Me encantaba este anuncio. Lo he intentado buscar muchas veces, pero no lo había encontrado hasta hoy, cuando hemos vuelto de nuestras vacaciones en la playa y me ha entrado la morriña.

Siempre me ha encantado la playa. Puedo decir que me he criado en ella al haber pasado allí todos los veranos de mi vida desde que tenía un año. Y siempre he sabido que quería formar una familia numerosa, así que este anuncio representaba para mí lo que esperaba de la vida.

Hasta el nombre del perfume acompañaba mis fantasías infantiles: Eternity. El amor eterno entre un hombre y una mujer, natural como una playa, apasionado como las olas, puro como la brisa, del que brotan los hijos, esos niños que corretean alrededor y que juegan con sus padres en armonía.

Cuánta perfección, ¿no? Pues eso es lo que yo creía que sería tener una familia.

Pero cuando tienes hijos y vas a la playa por primera vez con ellos te das cuenta de que la realidad dista mucho del idílico anuncio de Calvin Klein. Para empezar, la playa está de bote en bote, nada de intimidad familiar. Y los niños no corren por la orilla del agua a cámara lenta, sino que lo hacen como rayos y cada uno en una dirección y si te descuidas se te meten hasta el fondo del mar en un descuido. 

![image](/tumblr_files/tumblr_inline_pb455cC9hx1qfhdaz_540.jpg)

Apenas tienes tiempo de depilarte, y mucho menos, de buscar un pareo mono a juego con el bañador. El pelo se te encrespa y pareces un fantasma con la crema solar FSP 50 pediátrica de tus peques en la cara porque es más práctico echaros todos de la misma para no tener que acarrear mil frascos, porque ya acarreas bastante con las toallas, la sombrilla, la bolsita del bebé con pañales y todos los aparejos, la fruta para media mañana, la botella del agua, los flotadores, los cubos y las palas…

Así que cuando llegas y embadurnas a todos con la crema, te peleas con las cucurritas de detrás por haber clavado la sombrilla delante de ellas, lo más cerca del agua posible para vigilar a los niños, te das cuenta de que no estás nada glamurosa con las bermudas de tu madre bien anchas para poder practicar posturas imposibles en la arena para controlar al bebé, dormirlo, darle el yogur…, el bañador de hace unos años que te está pequeño porque has engordado ha encogido por la humedad ambiental y deja ver más de lo que debe, las viejas chanclas de dedo descoloridas y con la goma pasada, los pelos engrifados y descontrolados en una coleta que no cumple su función y los ojos guiñados para no perder de vista a las otras dos fieras porque las gafas de sol están llenas de crema, arena y dedos de niños y no te dejan ver demasiado.

Entonces recuerdas el anuncio de Calvin Klein y te entra la risa floja ante la atónita mirada de tu esposo que se encuentra levantando un castillo de estilo gótigo churrigueresco de lo más sofisticado que posteriormente será meado por un perro que por allí pasaeaba incumpliendo impunemente la normativa municipal (verdad verdadera).

Menos mal que existen los momentos mágicos. Es cierto que lo del anuncio no existe así en general, pero sí hay instantes en los que he sentido lo que sentía aquella niña que lo veía entonces. Son pequeños ratitos que miras a tus pequeños retozar con su padre en la arena y te das cuenta de que lo has hecho bien, de que era esto lo que querías y te sientes agradecida por esos tres hijos geniales, sanos, preciosos, cariñosos, un marido que se deja la piel por ellos, por ti, por el proyecto común de formar una gran familia. 

El sol y el mar haciéndoles disfrutar, inundando el aire de risas puras llenas de inocencia, un pequeño cachorrillo rebozado de arena trepando por tu espalda y acurrucándose…

![image](/tumblr_files/tumblr_inline_pb455cK1O11qfhdaz_540.jpg)

Estamos todos juntos, nos queremos, llenan mi vida, la desbordan. Vale, no es el anuncio de Calvin Klein, pero ¿acaso tenemos algo que envidiar?

![image](/tumblr_files/tumblr_inline_pb455dOsFA1qfhdaz_540.jpg)