---
date: '2017-03-07T12:32:02+01:00'
image: /tumblr_files/tumblr_inline_omepucxSZt1qfhdaz_540.png
layout: post
tags:
- colaboraciones
- planninos
- ad
- participoen
- familia
title: Reforestamos el bosque con Leroy Merlín
tumblr_url: https://madrescabreadas.com/post/158105818414/taller-reforestacion-leroymerlin
---

Yo que nunca había cogido una azada en mi vida, me tendrías que haber visto en el taller de reforestación del Bosque Cañaverosa en Calasparra, Murcia, organizado por [Leroy Merlín](http://www.leroymerlin.es/) el pasado fin de semana.

Al principio mis fieras se peleaban por cavar, pero pronto le dejaron ese qué hacer a papi, que es más fuerte.

## El Bosque Cañaverosa
 ![](/tumblr_files/tumblr_inline_omepucxSZt1qfhdaz_540.png)

El autobús nos dejó una fría y lluviosa mañana de sábado en el Santuario de la Esperanza, en Calasparra, y tuvimos que andar unos 30 minutos hasta llegar al Bosque Cañaverosa, ya que se trata de una reserva natural protegida, donde se limita tanto la manera de acceder, como el número de personas que lo pueden visitar. 

 ![](/tumblr_files/tumblr_inline_omep0gVaio1qfhdaz_540.png)

Accedimos a este paraje a través de la ruta de la Huertanica, que se localiza en el curso alto de Río Segura en el noroeste regional. 

Tiene una longitud de de 12 km y su superficie protegida es de 225 hectáreas entre los términos municipales de Moratalla y Calasparra.

## La reforestación

Después de cavar un poco valoro (ahora más) el duro trabajo que hacen las asociaciones y entidades que se dedican a esta ardua tarea de reforestación, sobre todo porque, tal y como nos explicaron, quien comienza la reforestación de una zona, no llega a ver el resultado de su labor, ya que ésta tarda más de un siglo en verse consolidada.

 ![](/tumblr_files/tumblr_inline_omep3lfcD61qfhdaz_540.png)

Éste es el motivo por el que nunca vayamos a ver repoblada la zona en la que estuvimos plantando, pero nos queda la satisfacción de haber puesto nuestro granito de arena para ayudar a proteger uno de los parajes más mágicos de la Región de Murcia, ya que se trata de nuestro último bosque ribereño; un trocito de norte en pleno sureste peninsular, ya que su vegetación podría confundirse con la que se da en el norte de España.

En la banda más próxima al agua, afectada directamente por las crecidas, se encuentran especies flexibles como sauces, zarzas, cañaverales y carrizales. A continuación, el tramo viene marcado por un elevado nivel freático, además de estar sometido a las crecidas naturales; aquí se mezclan álamos, chopos, fresnos, sauces, adelfas y tarajes. Y ocupando las bandas más alejadas encontramos olmo, almez y pino carrasco.

En cuanto a la fauna, la nutria, el galápago leproso o el sapo corredor, por cierto, pueden hacer aparición por la zona, aunque no sea fácil verlos..

Para mí fue emocionante plantar un árbol con mis hijos, todos en equipo (a mí me tocó ir al río a por agua para darle su primer riego), a papá, cavar con la azada, a Princesita, enterrar el arbolito, al mediano colocar la red con los palos tutores, y al pequeño contagiarnos con su entusiasmo (gracias a él todos estuvimos allí reforestando).

 ![](/tumblr_files/tumblr_inline_omep6jHOqA1qfhdaz_540.png)
## Ecoherencia y Eplan: asociaciones comprometidas

Nos acompañaron dos asociaciones comprometidas y apasionadas con el medio ambiente: [Ecoherencia](http://ecoherencia.es/) y [EPlan](http://www.earthplanassociation.org/)

 ![](/tumblr_files/tumblr_inline_omeph1Fbdf1qfhdaz_540.png)

En **Ecoherencia** son ambientólogos, permacultores y restauradores de ecosistemas con formación y experiencia en proyectos en diferentes países (Argentina, Australia, Brasil, Costa Rica, Chile, Francia, Inglaterra, Portugal y Suecia). También son expertos en educación ambiental en acción, desarrollan programas de formación y actividades de educación ambiental, permacultura, agroecología, conocimiento del medio, tecnologías apropiadas… desde hace más de 10 años.

El compromiso de **EPlan** es promover un cambio de valores en la sociedad para alcanzar un modelo de vida que haga compatible el desarrollo socioeconómico de las poblaciones humanas con la preservación del patrimonio natural y cultural. Todo ello a través de acciones de conservación, sensibilización, participación local e intercambio de experiencias con profesionales y población de otras comunidades y países.

La jornada fue dura, debido a la lluvia intermitente, y la dificultad de las actividades, donde las familias tuvimos que trabajar unidas, y ayudando a los más pequeños, que se portaron como campeones en lo que sin duda recordarán como una día de auténtica aventura.

## Las cajas nido

Después de dejar nuestro pino de Carrasca plantado, y un arbusto de Enebro, construímos una caja nido para ayudar a los pájaros a criar. Tampoco fue fácil, menos mal que la maña de papá y la ayuda de Albert, de Leroy Merlín, hicieron que termináramos con más o menos éxito nuestra cajita. ¿Te gusta?

 ![](/tumblr_files/tumblr_inline_omeour3Voz1qfhdaz_540.png)

Los niños disfrutaron y trabajaron en equipo, y creo que aprendieron a valorar y respetar más la naturaleza.

## Concurso escolar Hazlo Verde

Leroy Merlín ha organizado un concurso que me parece muy interesante para educar a los niños como ciudadanos responsables con el medio ambiente y hacer del mundo un lugar más verde y ecológico, convirtiéndolos en “embajadores del medio ambiente”.

Se trata de preparar un trabajo en el colegio, y para facilitar la labor de los profesores y profesoras, se les facilita materiales didácticos evaluables.

Hay dos categorías de participación:

-A: 3º y 4º de Primaria

-B: 5º y 6º de Primaria.

Entre todos los trabajos presentados a nivel autonómico se seleccionará uno por categoría para competir en la fase nacional, de la cual saldrán los ganadores finales, que tendrán la ocasión de asistir al **Campus Hazlo Verde** durante tres días a finales de junio.

Más información sobre el [concurso Hazlo Verde aquí](http://hazloverde.es/).

## Agradecimientos

Quiero agradecer a Paco, Albert, y su compañera (que me perdone, pero olvidé el nombre…), de Leroy Merlín por lo bien que nos trataron, a los chicos de Ecoherencia y EPlan, por su dedicación y paciencia para explicarnos todo tan bien y ayudarnos con los talleres, y a Leroy Merlín por habernos invitado a una actividad tan constructiva y gratificante.

 ![](/tumblr_files/tumblr_inline_omeoz4HkW61qfhdaz_540.png)

Mi enhorabuena a todos, y hasta la próxima!