---
date: '2015-01-14T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_ni2ukj2PiZ1qfhdaz.jpg
layout: post
tags:
- familia
- comunion
title: Organizando la Primera Comunión de mi niña. Recuerdos...
tumblr_url: https://madrescabreadas.com/post/108059229448/organizando-la-primera-comunión-de-mi-niña
---

![image](/tumblr_files/tumblr_inline_ni2ukj2PiZ1qfhdaz.jpg)

**Foto gracias a http://compritasparalospeques.com/**

Parece mentira que haya llegado el momento. Mi niña ya se hace mayor, ya os lo contaba en [este post](/2013/07/05/preadolescentes.html), pero parece que hasta que no la vi vestida de Comunión en una prueba de vestidos no fui consciente del todo.

Creo que tuve la misma sensación que quizá tendré cuando la vea vestida de novia, si llega el momento, porque ambos acontecimientos simbolizan el paso a una etapa nueva de la vida.

Ya sé que me diréis que soy un poco exagerada, pero al verla vestida así me di cuenta de que mi niñita pequeña se está convirtiendo en una mujercita.

Me hace mucha ilusión que mi pequeña haga la Primera Comunión, además de la motivación religiosa, pienso que para ella será muy bueno ser protagonista por un día entre nuestros familiares y amigos, y quiero que sea un día muy especial. Sin ostentación, pero muy especial, tal y como recuerdo la mía.

Creo que recuerdo cada momento del día de mi Primera Comunión. Cómo mis primos me acompañaron por el camino a la Iglesia, cómo todos querían estar a mi lado, el lugar que ocupaba entre mis compañeras en el Altar, los cantos que entonamos, el momento en que “recibí a Jesús”, el recogimiento con el que lo hice, lo feliz que me sentí…

 ![image](/tumblr_files/tumblr_inline_ni6251rj7T1qfhdaz.jpg)

Después llegaron las felicitaciones. Recuerdo muchos besos y abrazos, muchas sonrisas de familiares y amigos, vecinos…

También me acuerdo de algunos regalos, a destacar, una caja de bombones Lindt gigante y una cámara de fotos.

Aún guardo el libro de firmas, y de vez en cuando leo las dedicatorias y me emociono. Estaban todos a quienes quería… y, aunque en la de mi niña no lo estarán, habrá otros que ni entonces soñaba que existirían…

Por eso me preocupa, quizá en exceso, que ese día todo salga bien, y mi hija tenga un día inolvidable.

NOTA: Si estás organizando la Comunión, puedes apuntarte al grupo de Facebook de papás y mamás que estamos en ese mismo trance, para ayudarnos unos a otros y aportar ideas y experiencias. 

[Solicita tu entrada al grupo “Organizando la Primera Comunión”](https://www.facebook.com/groups/1377576085877092/)

Puedes consultar el post [Cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).