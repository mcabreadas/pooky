---
date: '2016-11-06T20:04:45+01:00'
image: /tumblr_files/tumblr_og8ic14Mha1qgfaqto1_1280.jpg
layout: post
tags:
- decoracion
- trucos
- familia
title: fiesta bautizo buffet
tumblr_url: https://madrescabreadas.com/post/152821078999/fiesta-bautizo-buffet
---

![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_og8ic14Mha1qgfaqto6_1280.jpg)  
  

## Fiesta buffet sencilla para celebraciones 
Conjugando dos colores y con un poco de imaginación se puede obtener un resultado así de dulce para una fiesta de Bautizo.  
Juega con las rayas y los lunares en los mismos tonos para servilletas, pajitas, platos…  
Busca un mantel liso para no sobrecargar la decoración y hacer destacar lo que pongamos sobre él.  
Los globos de helio consiguen resultados muy vistosos y son muy prácticos para ambientar espacios con poco coste.  
Coloca comida y bebida con cierto orden y deja un espacio para platos y vasos para que los invitados se vayan sirviendo.  
Esmérate en la mesa de niños poniendo cosas que especialmente les gusten colocadas de manera que les sean accesibles y fácil de servirse y comer ellos solos (sándwiches pequeños, croissants rellenos, tortilla de patatas cortada a tacos, mini pizzas…)  
Viste la pared con una bonita guirnalda a tu gusto. Algo sencillo bastará para dar un toque alegre.  
Pon una mesa de bebidas sólo para adultos con todo lo necesario para que se sirvan ellos mismos.  
Ayúdate de carteles para indicar qué es cada cosa o cómo se debe servir para que los  
Invitados tengan independencia para tomar lo que les apetezca sin que tengas que estar demasiado pendiente y tengas oportunidad de hablar con todos y disfrutar de la fiesta tú también.