---
date: '2015-09-06T18:27:19+02:00'
image: /tumblr_files/tumblr_inline_o39zfoAeSn1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- educacion
title: Periodo de adaptación al colegio. 10 tips para afrontarlo
tumblr_url: https://madrescabreadas.com/post/128488309134/adaptacion-colegio
---

Enseguida mi niño pequeño empieza el colegio, y aunque haya un periodo de adaptación en el que los alumnos de primero de infantil se van incorporando poco a poco de forma progresiva y continua en pequeños grupos, tengo mucha incertidumbre sobre cómo lo va a llevar.

A pesar de que sea mi tercer hijo, eso no me garantiza nada. Ni que yo vaya a saber hacerlo bien (cada uno es un mundo, y éste es el único que no ha ido a guardería),ni que él vaya a llevarlo mejor porque ha visto a sus hermanos entrar mil veces al cole, conoce el centro, a algunos niños, incluso a algún profesor, ni que no vaya a llorar (ni él ni yo).

Este verano hemos estado trabajando este tema en casa de varias formas. Porque creo que la preparación más importante para afrontar el que será uno de los mayores cambios en su vida, tiene que hacerse en casa.

 ![](/tumblr_files/tumblr_inline_o39zfoAeSn1qfhdaz_540.jpg)

Los aspectos que hemos trabajado nosotros son:

## Control de esfínteres

Aprovechar el calor y la playa o piscina para llevarlo todo el día semi desnudo para facilitarle el acceso al orinal o vater es una buena idea.

Como no es algo que se consiga en un día, es mejor tomárselo con tiempo y tranquilidad.

Algunos niños lo tendrán más difícil porque entrarán al colegio con dos añitos, porque cumplen los 3 en noviembre o diciembre, y en estas edades la diferencia de un año es enorme. Tanto que algunos entrarán al cole como bebés todavía, y otros, los nacidos en enero, serán niños más independientes.

Hay colegios que permiten que los profesores o personal especializado limpien a los niños que sufren escapes de pipí o caca, pero otros, como es nuestro caso, llaman a los padres para que vayan a cambiarlos.

## Habla de vez en cuando del cole (sin pasarse)

Para que se vaya mentalizando. Y siempre en positivo, claro. Si tiene hermanos mayores que ya vayan, es más fácil porque el tema suele surgir de forma natural.

## Evita comentarios negativos

 Tanto vuestros, como de familiares y amigos. Pedid a vuestro entorno que eviten frases como:

**“Anda, qué pronto se te va a acabar la buena vida, ¿eh?….”**

**“Ya verás, te van a llevar más recto que una vela”**

**“Tu madre estará deseando que te vayas al cole para quedarse tranquila”**

**“Prepárate cuando vayas al cole…”**

y otras lindezas de este tipo.

 Todo esto, aunque parezca que el niño no se da cuenta, nada más lejos de la realidad, ya que le va calando y le creará un rechazo instintivo del colegio.

## Llévalo previamente al colegio

 Cuando tienen hermanos, procura que durante el curso anterior a la escolarización, te acompañe el pequeño a recoger o llevarlos, así se familiarizará tanto con el entorno, que entrará mucho más seguro.

 Mi peque sale corriendo cuando llega al patio y se mete a las clases si me descuido.

## Preséntale a sus nuevos compañeros  

 A veces coinciden con algún compañerito de guardería, o de parque. Si es así, puedes intentar fomentar esa relación en las semanas previas al comienzo de las clases.

 Si no conoces a ningún niño que coincida con él en su clase, haz por conocerlos. La reunión de bienvenida para el nuevo curso, que suele ser antes de vacaciones de verano, al menos en nuestro cole, puede ser un buen momento para esto.

En nuestro caso, como coinciden varios hermanos de compañeros de los mayores, lo hemos tenido muy fácil, ya que se han ido viendo a lo largo de la vida escolar de sus hermanos en cumpleaños, por ejemplo.

##  Foméntale la autonomía/dale seguridad

Alaba cualquier pequeño logro que consiga, como abrocharse un botón, cerrar la cremallera de la mochila o subirse, bajarse los pantalones solo, recoger sus juguetes…

No te pongas nervioso/a o pongas mala cara cuando intente hacer algo y no lo consiga a laprimera. Esto es importante, ya que lo va a percibir y va a pensar que no sabe y que por eso te enfadas.

Ya sé que a veces vamos con prisas y no tenemos tiempo ni paciencia para esperar a que hagan las cosas solos, y que es más eficaz hacerlas nosotros, pero créeme, aprenden rápido. Ya verás como la segunda y tercera vez lo harán mucho mejor. 

Tómatelo como una inversión en la que los resultados positivos se ven a medio plazo.

## Explícale que lo vas a recoger

Para evitarle la sensación de abandono, es importante decirle desde el principio que lo vamos a acompañar al colegio, luego nos vamos a ir, y después volveremos a por él.

La mejor manera de trabajar esto, al menos en nuestro caso, es provocando situaciones similares, dejándolo en casas de los abuelos, por ejemplo. Y explicárselo cada vez que nos vayamos. Y cuando volvamos, hacer mucho hincapié en que siempre volvemos a por él/ella.

De este modo comprenderán qué significa el concepto de “recogerlo después”, ya que con esa edad no piensan mucho en el futuro, sino que lo único que entienden es que te vas y ellos se quedan.

##   

## Llorar es normal

Prepárate porque seguramente llorará, y si no lo hace los primeros días, probablemente lo hará después, cuando se dé cuenta de que ir al cole no es algo esporádico, sino que tiene que ir cada día. 

Ten en cuenta que para ellos es el mayor cambio de su vida, como os he comentado antes.

Evita culpabilizarlo porque llore. Dile que es normal estar triste, que todos nos sentimos así cuando algo no nos gusta, pero que enseguida se va a poner contento cuando pase tal o cual cosa.

Dile que lo quieres mucho, y lo orgullosa/o y contenta/o que estás de que se haga mayor y de que aprenda mucho en el cole.

Nada de decirle llorica, llorón, enfadarte o ridiculizar sus sentimientos, que son tan legítimos como los nuestros.

No te avergüences porque llore o arme rabieta. No mires alrededor, céntrate en tu hijo/a, que es lo más importante que tienes que hacer en ese momento.

##  Hazlo/a partícipe de los preparativos

Que no se sienta sujeto pasivo mientras nos oye hablar y decidir cosas sobre su entrada en el cole. Para eso: 

  **Llévalo a comprar el uniforme**

  **Que elija su mochila, estuche…**

**Pruébale su equipación de deporte** , esto les hace especial ilusión

 ![](/tumblr_files/tumblr_inline_o39zfoqGa61qfhdaz_540.jpg)

## Sin prisas

Es preferible levantarse un poco antes, y en días sucesivos ajustar la hora según vaya haciendo las cosas más rápidas, a ir con prisas, nervios y hacer de la primera mañana una experiencia estresante para todos.

Procura estar tranquila/o, sonreír, y transmitirle seguridad. 

Pero lo más importante, además de todos estos consejos, es que vosotros en casa afrontéis con ilusión esta nueva etapa, ya que si vosotros lo tomáis así, ellos lo van a percibir, y poco a poco se irán adaptando sin problemas.

Espero que te sirva mi experiencia.

¿Tienes algún consejo más?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.