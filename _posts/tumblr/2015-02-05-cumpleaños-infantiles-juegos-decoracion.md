---
date: '2015-02-05T08:00:00+01:00'
layout: post
tags:
- decoracion
- trucos
- planninos
- familia
title: Ideas para cumpleaños infantiles caseros. Juegos, decoración, tarta, galletas...
tumblr_url: https://madrescabreadas.com/post/110142991555/cumpleaños-infantiles-juegos-decoracion
---

No negaré que los cumpleaños de toda la vida llevan más trabajo y más mareos de cabeza, pero nos podemos complicar hasta el límite que queramos. No hay normas, y son adaptables a cualquier bolsillo, edad de los niños, espacio del que disponemos, clima…

El secreto es prepararlos con mucho cariño, poniéndonos en la piel de nuestros hijos y, lo más importante, hacerles partícipes de los preparativos, implicarlos, por ejemplo, en la elección de los juegos, elaboración de la tarta, los dulces, regalitos para los invitados, o incluso la decoración.

## Los juegos para los niños

Imprescindible tener todo el tiempo que dure la fiesta cubierto con juegos organizados, y dos o tres adultos que los dirijan para tener todo el rato a los niños entretenidos para que no se desmanden y se forme el caos.

Nosotros hicimos una **gimcana** de juegos interiores porque hacía un día horrible. En un principio íbamos a salir al jardín, pero no pudimos, menos mal que teníamos un plan B preparado.

Echamos a un lado los muebles del salón para ganar espacio y lo dividimos en dos campos con cinta carrocera pegada al suelo, que aunque sea de madera no se estropea, hicimos dos equipos y empezamos con juegos diversos, como **llevar una pelota en una cuchara** en una carrera (quien se atreva que lo haga con huevos), o **guerra de pelotitas** de papel y el equipo que más meta en el campo contrario, gana.

Otro juego que se puede hacer es la **pelota musical** , que consiste en fabricar una pelota con papeles de periódico a capas e irla pasando de uno a otro al ritmo de la música. Cuando ésta para, quien la tenga, tiene que quitar todo el papel que pueda antes de que vuelva a sonar, momento en el que deberá pasarla de nuevo. El niño que consiga quitar todo el papel se lleva el regalo que hay dentro.

Pero el más divertido fue el **concurso de baile** , que nos llevó a un desempate mediante un cara a cara entre los capitanes de cada equipo, que fue tronchante.

Una idea chulísima es compra un **lienzo y pintura de dedos** para que cada invitado deje un dibujo, la huella de su mano, o lo que quiera, en él que quedará como recuerdo para el cumpleañero.

Los **marcapáginas con palos de polo**  anchos (los mismos que se usan para las piruletas de galleta) decorados con rotuladores también son una buena opción.

Cada juego puntúa, y el equipo que más puntos consiga al final de todos es el ganador.

## La comida

Nada complicada, pizza y perritos calientes. Y los niños, encantados. Usamos platos de usar y tirar para ahorrar esfuerzos.

## La decoración

Para la decoración es fundamental elegir un tema, y hacerlo todo en relación con él. En nuestro caso fue “picnic”.

Para ello empezamos la semana anterior a prepararlo todo poco a poco. 

Decoramos las **botellas** para el batido de chocolate con la cuerda y etiquetas de cuadritos rojos y blancos, también adquiridas en el chino.

Los **vasos** eran de plástico duro transparente, y los rodeamos con cinta de cuadros a juego con lo demás pegada con pegamento de tela. Quedaron resultones, ¿eh?

La **guirnalda** fue o más divertido, con acuarelas, los tres peques dibujaron en mantelitos de blondas de los que se ponen en las bandejas de dulces de cartón , una letra en cada uno, luego se doblan por la mitad y se cuelgan en la cuerda.

Hicimos unos **barquitos con servilletas** rojas y con cinta de tela de cuadros rojos y blancos, y pusimos dos **macetitas**  naturales de la llamada flor de papel de color amarilla, muy vistosas, que contrastaba con el rojo.

## La tarta

Más fácil imposible, mi madre me hizo un bizcocho de los suyos, lo rodeamos de kit kats atados con una cinta de cuadritos igual a juego con el resto, y lo cubrimos de lacasitos. Vistosa es un rato, y si cubrimos con chocolate fundido el bizcocho y pegamos los lacasitos, también es una buena idea.

## Las piruletas de galleta

Toda una experiencia hacerlas, aquí os lo contaba [como hacer piruletas de galleta](/2015/02/03/libro-recetas-ninos.html). Además de decorar, están buenísimas.

## Los regalitos para los invitados

Que quedaron así de cucos con unos frascos de cristal de la tienda china, unas etiguetas adhesivas para poner el nombre de la cumpleañera, y cuerda finita para atar alrededor y dar un toque rústico.

Los rellenamos de caramelos “picotas”, que están deliciosos y son rojos, para no salirnos de los tonos de la decoración.

Los repartimos al final de la fiesta a cada niño cuando fueron a recogerlos sus padres para que no hubiera peligro con el cristal.

Pero sin duda, el secreto del éxito del cumpleaños fue el ambiente familiar que se respiraba y el cariño con el que preparamos todo, que hicieron que vaya a ser un cumpleaños difícil de olvidar.

¿Te atreves con un cumpleaños casero?