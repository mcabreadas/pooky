---
date: '2016-10-15T16:56:19+02:00'
image: /tumblr_files/tumblr_inline_of3g0pclP51qfhdaz_540.png
layout: post
tags:
- mecabrea
- crianza
- revistadeprensa
- colaboraciones
- ad
- derechos
- embarazo
- mujer
title: Derechos laborales de la mujer durante el embarazo | El club de las madres
  felices
tumblr_url: https://madrescabreadas.com/post/151838953339/derechos-laborales-de-la-mujer-durante-el-embarazo
---

[Derechos laborales de la mujer durante el embarazo | El club de las madres felices](http://elclubdelasmadresfelices.com/derechos-laborales-embarazo/)  

Os dejo este post que escribí para El Club de las Madres Felices, y que interesará a quienes estéis embarazadas o lo tengáis en proyecto:

[Tus derechos laborales durante el embarazo](http://elclubdelasmadresfelices.com/derechos-laborales-embarazo/)

 ![](/tumblr_files/tumblr_inline_of3g0pclP51qfhdaz_540.png)