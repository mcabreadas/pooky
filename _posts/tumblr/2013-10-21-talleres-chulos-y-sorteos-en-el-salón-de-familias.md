---
date: '2013-10-21T06:43:00+02:00'
image: /tumblr_files/tumblr_inline_pe8xeeXfgU1qfhdaz_540.jpg
layout: post
tags:
- trucos
- planninos
- participoen
- colaboraciones
- ad
- recomendaciones
- familia
title: Talleres chulos y sorteos en el Salón de Familias Numerosas de Cataluña
tumblr_url: https://madrescabreadas.com/post/64655380277/talleres-chulos-y-sorteos-en-el-salón-de-familias
---

![image](/tumblr_files/tumblr_inline_pe8xeeXfgU1qfhdaz_540.jpg)

Si eres familia numerosa y vives en Cataluña, reserva el fin de semana del 9 y 10 de noviembre para llevarte a la tropa al Salón de Familias Numerosas que organiza FANOC.

Normalmente las ferias de productos en general suelen ser muy comerciales y poco atractivas para los niños, pero en esta ocasión os la recomiendo porque hay un programa complementario de actividades infantiles con talleres para que chicos y grandes disfruten de un día en familia.[Aquí](http://www.salofamiliesnombroses.com/es) podéis ver todos los detalles del evento.

Como actividad interesante os recomiendo el taller simultáneo de Portabebés y Reflexología Podal que organiza Laura, de Pekefriendly, empresa patrocinadora de la sala de lactancia donde, además, los más peques podrán disfrutar de un menú degustación HIPP, marca especializada en alimentación biológica para bebés (a mi Leopardito le encanta) y, participar en un montón de sorteos de cosas muy chulas, como escapadas de fin de semana, talleres familiares, lotes de estimulación sensorial para bebés, cositas de Medela, hasta un reportaje de recién nacido del fotógrafo [Xavi Olivé](http://www.xaviolive.com/2013/04/19/fotografia-exterior-familia/?lang=es) , con la entrega de un álbum con una selección de 15 fotos, y muchas sorpresas más.

 ![image](/tumblr_files/tumblr_inline_pe8xeeHeeY1qfhdaz_540.jpg)

Para participar en el taller y los sorteos lo ideal es que os regitréis en la web de Pekefriendly, y para descargar las entradas al recinto, que son gratuítas para las familias numerosas, lo podéis hacer [aquí](http://www.eventbrite.es/event/8529807885?ref=ebtn), sin olvidar llevar vuestro título o carné de familia numerosa.

Listos para pasarlo en grande?

Luego me contáis qué tal lo han pasado los peques.

NOTA: Este post no es patrocinado. Con él pretendo apoyar la promoción de la lactancia materna y la ayuda a los emprendedores de empresas familiares.

Foto gracias a [http://107 Espacio abierto](http://107espacioabierto.com)