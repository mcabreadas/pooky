---
date: '2015-02-03T12:58:00+01:00'
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- colaboraciones
- ad
- nutricion
- recomendaciones
- familia
- recetas
title: Libros de recetas para niños. Con la comida sí se juega
tumblr_url: https://madrescabreadas.com/post/109969820044/libro-recetas-ninos
---

Os tengo que hablar de un libro que ha hecho que mi Princesita se interese por la comida y por aprender a cocinar, porque para ella es como un juego.

“[Con la comida sí se juega](http://www.boolino.es/es/libros-cuentos/con-la-comida-si-se-juega/)” no es un libro de recetas de cocina al uso, sino que va dirigido a los niños, y consigue una complicidad con ellos que hace que cuando practican cualquiera de sus recetas se sienten auténticos protagonistas de la labor que están realizando.

Las recetas están especialmente pensadas para que llamen la atención de los más pequeños por su presentación, facilidad de manejar los ingredientes y colorido. Sólo es necesario la supervisión de un adulto y el manejo del horno o fuego por él.

¡Dejemos que todo lo demás lo hagan ellos, te van a sorprender!.

Nosotros hicimos en casa la receta de las piruletas de galletas, con motivo del cumpleaños de la mayor, y os puedo decir, que lo pasamos pipa pesando los ingredientes, glaseando el azúcar, usando el rodillo (todo un descubrimiento), haciendo las figuras con los moldes y, cómo no, decorándolas con tinta de colores comestible.

Os podéis imaginar la emoción de los niños al poder pintar sobre las galletas y dibujar lo que quisieran. ¡La imaginación al poder!

Quedaron preciosas, no tan perfectas como en la foto del libro, pero tuvieron un éxito brutal en el cumpleaños, y mis niños las repartieron orgullosos entre sus amiguitos.

Pero o sólo hay recetas dulces, también hay unas muy divertidas con verduras, el caballo de batalla de muchas familias, y otras muchas y variadas.

Ya iremos probando poco a poco a hacerlas.

Os recomiendo que cocinéis con vuestros hijos.

Y si os sucede como a mí, que no se me da muy bien la cocina, de paso aprendemos, ¿no creéis?