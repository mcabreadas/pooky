---
date: '2015-05-01T11:51:47+02:00'
image: /tumblr_files/tumblr_inline_nno0lkbPH71qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- adolescencia
- libros
- planninos
- colaboraciones
- ad
- recomendaciones
- educacion
- familia
title: 'Libros para niños: Monday y May contra el doctor Lecter'
tumblr_url: https://madrescabreadas.com/post/117844050699/libros-ninos
---

[Monday & May](http://www.boolino.es/es/libros-cuentos/monday-may-contra-el-dr-lecter/) son dos hermanos (chico y chica) que van al mismo Instituto, y que son tan diferentes entre sí como la noche y el día (como la mayoría de los hermanos). Este libro tiene todos los ingredientes para gustar y enganchar a niños a partir de 9 años porque, aunque no vayan todavía al Instituto, ya tienen sus miras puestas en ese momento, y les atrae todo lo que tenga que ver con ser mayores.

 ![image](/tumblr_files/tumblr_inline_nno0lkbPH71qfhdaz_540.jpg)

La forma en que está estructurado el libro es muy curiosa porque cada capítulo lo escribe uno de los hermanos, lo que aporta dos visiones de una misma situación, a veces tan dispares, que resulta disparatado.

Aunque yo ya estoy crecidita, a mí también me ha gustado mucho, y lo he leído con una sonrisa en los labios por su tono fresco y lo original de la historia porque es muy loca. 

Veréis,la historia transcurre en Texas, tierra de petróleo y vacas, pero los padres de Monday y May son dos científicos ecologistas, vegetarianos, además de un poco hippies, por lo que son la nota discordante de la zona, junto con la amiga de la familia, la Sra. Leopardi, dueña del único restaurante vegetariano entre tanta hamburguesería.

Por otro lado están, el magnate del petróleo de la ciudad, el Sr. Busher, el director del instituto apodado por los alumnos Sr. Lecter en honor al famoso psicópata Anibal, (cosa que, por otra parte no le importa), y el Sherif del condado, que no se entera de nada.

Este choque de personajes, de estilos de vida e ideales está presente en toda la lectura de un modo muy divertido, que hace muy atractiva esta genial historia, cuya autora es la tejana Lillian Wallaby que podría haberse convertido en una de las grandes escritoras norteamericanas de su época, de no ser porque era analfabeta.

 ![image](/tumblr_files/tumblr_inline_nno03aIew31qfhdaz_540.jpg)

Sí, sí, no os sorprendáis, ella contaba esta historia y muchas más de forma oral, y fueron otros quienes la escribieron y fruto de ello nació este libro que no podéis dejar de dar a vuestros hijos para que lo disfruten tanto como nosotros.