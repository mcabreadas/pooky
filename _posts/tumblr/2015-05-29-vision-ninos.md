---
date: '2015-05-29T11:44:42+02:00'
image: /tumblr_files/tumblr_inline_np3toaj4EY1qfhdaz_540.jpg
layout: post
tags:
- crianza
- colaboraciones
- ad
- eventos
- familia
- salud
- bebes
title: Problemas de visión en niños.Recomendaciones y soluciones.
tumblr_url: https://madrescabreadas.com/post/120175897899/vision-ninos
---

Cuando mi pequeño tenía tres mese notamos que vizqueaba, y empezamos a preocuparnos. Lo llevé al oftalmólogo y me explicó que era normal que a tan corta edad no se alinearan los ojos todavía, que era algo totalmente normal. Entonces me di cuenta de lo poco que sabía sobre la salud de los ojos de los más pequeños, y de que era un tema que tenía olvidado.

Por eso cuando recibí la invitación de la clínica Baviera para asistir a una charla sobre oftalmología pediátrica, a quien agradezco enormemente la oportunidad, me interesé en el momento, y pensé que a vosotros también os sería de interés, así que envié a mi amiga y colaboradora, Elena, del blog La Guinda de Limón, a quien ya conocéis, para que nos lo contara todo tan bien como ella sabe:

> “La semana pasada acudí al I Encuentro **#KidsEyes&Tips** organizado por la **Clínica Baviera** e invitada por Madres Cabreadas, esta vez volví a ser sus ojos en un evento, nunca mejor dicho! 

 ![image](/tumblr_files/tumblr_inline_np3toaj4EY1qfhdaz_540.jpg)

> En este encuentro el **Dr. Valentín Jiménez** , oftalmólogo pediátrico nos contó los posibles problemas que podían tener nuestro pequeños en los ojos.

 ![image](/tumblr_files/tumblr_inline_np3tqaEMdH1qfhdaz_540.jpg)

> Nos contó que los bebés cuando nacen tienen los órganos de la visión muy inmaduroS, como casi todos los órganos del resto de su cuerpecito. Es hasta los 2 años de edad donde se producen los mayores cambios y suele llegar a su estado de maduración total llegados los 6 años.
> 
> Durante los primeros meses de vida los movimientos oculares no están bien coordinados, con lo que no debemos preocuparnos, suelen alinearse en torno a los 3-6 meses, que es cuando comienzan a seguir objetos con la vista.
> 
> Las patologías más frecuentes en los niños son:
> 
> 1. Defectos de refracción: es decir, miopía, hipermetropía y astigmatismo.
> 2. Ojo vago o Ambliopía.
> 3. Estrabismo.
> 4. Otros menos frecuentes: catarata congénica, retinoblastoma (tumor), etc.
> 
> 1. ¿Cómo sabemos que nuestro pequeño puede tener algún defecto en la visión?
> 
> Hay que estar atentos a los siguientes indicadores:
> 
> - Mala visión
> - Cefaleas
> - Cansancio, enrojecimiento de los ojos tras esfuerzo visual.
> - Dificultades de lectura o acercamiento de los libros a los ojos.
> - Dificultades de concentración para las tareas que requieran buena visión, puede que no les interese jugar o evadir ese tipo de tareas por no enfrentarse al problema.
> 
> **¿Qué hacer?**
> 
> Acudir al oftalmólogo
> 
> y tras una correcta evaluación determinará que es lo que necesita el pequeño.
> 
> ¿Cuáles son los tratamientos que le puede poner?
> 
> - Gafas
> - Lentes de contacto: no aconsejables hasta al menos 10-10 años que el niño tenga la habilidad suficiente de manipularlas él solito.
> 
> 2. ¿Cómo sabemos que nuestro hijo tiene el ojo vago?
> 
> Es la alteración en el desarrollo visual en la infancia, puede ser una disminución de uno o de los dos ojos. Si se coge a tiempo y con el tratamiento adecuado, es reversible sólo en la infancia, antes de los 4 años y con edad límite los 7-8 años.
> 
> **Importante el diagnóstico precoz**
> 
> Por ello es muy muy importante el Diagnóstico precoz con revisiones, la primera a los 3,5-4 años y la segunda a los 5,5-6 años.
> 
> **Cómo se trata**
> 
> Con tres tipos de tratamientos, penalización de la gafa con un filtro, dilatar la pupila con Atropina o lo más extendido tapando el ojo con un parche. Ahora hay unos muy chulos y divertidos.

 ![image](/tumblr_files/tumblr_inline_np3tuoiUPH1qfhdaz_540.jpg)

> 3. ¿Qué hacemos si nuestro hijo tiene estrabismo?
> 
> Otro de los problemas es el ESTRABISMO, que es la desviación ocular. Puede aparecer en cualquier momento lo más frecuente es de 2 a 4 años y puede ser causada por tener uno ojo vago. Las repercusiones suelen ser sobre todos estéticas y con gran impacto personal y social.
> 
> **Cómo tratarlo**  
> 
> Básicamente de cuatro formas:
> 
> - Parches: para el estrabismo causado por el ojo vago.
> - Gafas para estrabismo acomodativo.
> - Cirugía: si no se tuerce el ojo con la gafa no es recomendable operar.
> - Toxina Botulinica
> 
> Mis recomendaciones, que son las dadas por el Dr. Jiménez
> 
> :
> 
> - **Revisiones:** la primera a los 3,5 años y la segunda a los 5,5 años. Periódicas cada dos años si no hay síntomas.
> - **El uso de dispositivos tecnológicos de manera prolongada no afecta a la visión** , tampoco la lectura y es muy bueno la luz natural y estar al aire libre.
> - **Protección solar** : gafas de sol en exposiciones prolongadas en playa, piscina, campo, nieve y si el niño tiene molestias y giña los ojos cuando hay mucha claridad.
> - Y sobre todo **vigilancia** , estar muy encima de nuestros hijos, vigilarles para que en el momento que tenga algún síntoma de que puede tener un problema, actuar de la manera más rápida posible.
> 
> ​Es fundamental tener muy presente la salud de nuestros ojos, que sólo tenemos dos para toda la vida y son muy muy importantes en nuestra vida diaria.
> 
> Muchas gracias al Dr. Jiménez y a la Clinica Baviera por una exposición tan tan interesante, por portarse tan bien con las blogueras que allí estabámos pero sobre todo con mi Guinda, estuvieron muy atentas con ella.

 ![image](/tumblr_files/tumblr_inline_np3ttjtwKA1qfhdaz_540.jpg)

Y cómo no! a María de **[Madres Cabreadas](/),** el blog con el que tengo el honor de poder colaborar, por darme la oportunidad de acudir a este I Encuentro **#KidsEyes&Tips.”**

Gracias a ti, Elena, por tu buen hacer, y por darnos datos tan oportunos para la edad en la que están nuestros hijos.

¿Y vosotros, habéis notado alguno de estos problemas en vuestros hijos?