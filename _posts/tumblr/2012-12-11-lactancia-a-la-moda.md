---
date: '2012-12-11T10:00:00+01:00'
image: /tumblr_files/tumblr_mewyploc6b1qfhdaz.jpg
layout: post
tags:
- crianza
- colaboraciones
- ad
- lactancia
- moda
title: Lactancia a la moda
tumblr_url: https://madrescabreadas.com/post/37704854022/lactancia-a-la-moda
---

Pues no, la lactancia materna no tiene por qué ser incompatible con ir a la moda y sentirse guapa. Y me cabrea sobremanera que cueste tanto encontrar prendas con fácil abertura por delante que sean bonitas, elegantes, modernas y, sobre todo, económicas.Pero haberlas, hay las.

Yo llevo 4 meses amamantando a mi bebé, y pretendo seguir haciéndolo mucho tiempo más, por eso he decidido buscar ropa cómoda y práctica para desenfundar rápidamente en cualquier momento o lugar que mi bebé lo pida, y a la vez, como una es presumida, que sea mona y estilosa.

Aquí, una muestra de lo que he encontrado por si os sirve y os ahorra tiempo y energías de búsquedas interminables, y algún que otro quebradero de cabeza.

Para empezar, me hice con unas camisetas básicas de lactancia para ponérmelas debajo de las prendas de invierno para no congelarme cuando me levante el jersey en el parque. Son de H&M y cuesta alrededor de 25 € el pack de dos de manga corta, y alrededor de 20€ las de manga larga, que se venden sueltas. Son súper prácticas y te quitan la pereza de salir con el bebé y amamantarlo en público donde sea, ya que son muy discretas.

 ![image](/tumblr_files/tumblr_mewyploc6b1qfhdaz.jpg)

Pero como a veces me apetece salir de los típicos tejanos y jersey, y ponerme algún vestido, me empeñé y encontré estos para usarlos con leggins y botas o con medias:

 ![image](/tumblr_files/tumblr_mewyqd5kIw1qfhdaz.jpg)

Vestido camisero de Mango

 ![image](/tumblr_files/tumblr_mewyqza3Y51qfhdaz.jpg)

Vestido con cremallera de H&M (también está en estampado tonos azules)

 ![image](/tumblr_files/tumblr_mewyt0Cvks1qfhdaz.jpg)

Desmangado de Mango

 ![image](/tumblr_files/tumblr_mewytjnRzg1qfhdaz.jpg)

Vestido Zara

- Sin duda el mayor reto fue encontrar un vestido de fiesta para Noche Vieja, pero lo conseguí. Fue en H&M:
 ![image](/tumblr_files/tumblr_mewyufQ5Pf1qfhdaz.jpg)

Espero que mi incursión en el mundo de la moda para madres lactantes os haya servido de ayuda. No dudéis en dejarme vuestros comentarios y, sobre todo, si tenéis más sugerencias serán bienvenidas.