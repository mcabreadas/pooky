---
layout: post
title: Cómo hacer pulseras de gomitas de dos
date: 2014-03-27T10:00:00+01:00
image: /tumblr_files/tumblr_n32335ZBzG1qgfaqto1_250.jpg
author: .
tags:
  - planninos
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/80864879779/pulseras-de-gomitas-en-vista-del-éxito-del
---
<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/hfq5G0j37XU?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

## Pulseras de gomitas

En vista del éxito del anterior post sobre las pulseras de gomitas de dos, os dejo otro tutorial hecho por mi niña también, esta vez para hacer pulseras de cuatro.

Princesita les da otras utilidades, mirad cómo me la encontré el otro día al salir del cole:

 ![](/tumblr_files/tumblr_n32335ZBzG1qgfaqto1_250.jpg)

Y, como hace tantas y me las encuentro en los sitios más insospechados de la casa, se nos ocurró fabricar un expositor con el cartón de un rollo de papel higiénico forrado de papel de aluminio, y metido en un portarrollos vertical de madera para el papel de la cocina.

 ![](/tumblr_files/tumblr_n32335ZBzG1qgfaqto2_250.jpg)

Esta moda se está extendiendo como la pólvora por todos los puntos de España, por lo que he podido comprobar en las redes sociales:

* **En Twitter** , @casinez comparte esta foto, y dice que su casa está invadida por estos pequeños elásticos de colorines:

[@madrescabreadas](https://twitter.com/madrescabreadas) mira como estamos!! Jajaja [pic.twitter.com/y1Ujqc1az2](http://t.co/y1Ujqc1az2)

* **En Facebook** , las reacciones van en el mismo sentido, incluso, hay quien fabrica telares caseros porque con el que compraron no dan abasto:

[una publicación](https://www.facebook.com/286180668083063/photos/a.635520059815787.1073741827.286180668083063/740066582694467/?type=1)

de

[Blog Madres cabreadas.Ahora familia numerosa](https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063)

Pero esto no acaba aquí, las posibilidades son infinitas, y hemos adquirido un telar por 2 € en el “chino”, para dar rienda suelta a nuestra creatividad y que no nos frene nada.

[una publicación](https://www.facebook.com/286180668083063/photos/a.635520059815787.1073741827.286180668083063/740066582694467/?type=1)

de

[Blog Madres cabreadas.Ahora familia numerosa](https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063)

. Ya me contaréis cómo os ha ido. Os recuerdo que podéis subir fotos a los comentarios del blog con vuestras creaciones o con telares de fabricación propia o lo que se os ocurra. Y si queréis, podéis suscribiros al

[canal de Youtube de Madres Cabreadas](http://www.youtube.com/channel/UCMkzI7f4qxHj3xEmluf4K3Q)

, donde encontraréis más sorpresitas.