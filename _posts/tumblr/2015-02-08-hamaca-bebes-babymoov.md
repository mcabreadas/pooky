---
layout: post
title: Hamaca evolutiva de Babymoov. Tumbona para bebés y puf para niños
date: 2015-02-08T11:13:00+01:00
image: /images/uploads/1-doomoo-nid-giraf-grey_1800x1800.jpg.webp
author: .
tags:
  - 0-a-3
  - puericultura
tumblr_url: https://madrescabreadas.com/post/110426274269/hamaca-bebes-babymoov
---
Me encantan los productos de puericultura que son prácticos por ser adaptables a la edad de los bebés. Y si, además, tienen un diseño innovador, mejor que mejor. Si compras una hamaquita y te dura 6 meses (o menos, como fue mi caso porque mi hijo pesaba mucho), no compensa tener que arrumbarla enseguida, además de que es una pena no poder aprovecharla más tiempo.

Por eso en casa aceptamos el testing de la [tumbona evolutiva Doomoo Nid que nos propuso Babymoov](https://amzn.to/3Qgzd3m), que sirve desde recién nacido como tumbona con arnés de seguridad, hasta que pesen 30 Kg, que lo pueden seguir usando como puf.

## Puf para niños mayores

En casa ya no tenemos recién nacido, pero, tanto mi peque dos años, como sus hermanos de 7 y 9, mirad cómo la disfrutan:

Una improvisada consulta médica:

Hasta sirve para estudiar, no digo más!

## Tumbona para bebés

Lo bueno es que, además, el Doomoo Nid se adapta a los recién nacidos gracias a su forma ergonómica y a sus suaves tejidos. 

La veo fresquita para verano porque va rellena de microesferas, y el tejido que la recubre es de algodón muy suave y agradable, y en invierno se puede usar con una mantita por encima para mantener al bebé calentito.

## La limpieza de la hamaca

Viene con dos fundas intercambiables y fácilmente extraíbles mediante cremallera, lo que permite un lavado práctico en lavadora: una con arnés con tres puntos regulables, para cuando se usa como tumbona, y otra sin arnés para usarla como puf.

Cuando decidí lavar una, puse la otra mientras, y listos! 

El lavado es en lavadora con programa normal y detergente para ropa blanca. Yo centrifugué a 800, como normalmente hago con el resto de la ropa.

Después la tendí para que se secara al aire en el tendedero, y se secó enseguida quedando perfecta para volver a usar.

Quizá esto a simple vista no os parezca tan importante, sobre todo si aún no tenéis un bebé que mancha a diario, pero tener inutilizada la hamaquita mientras se lava y se seca la funda es un rollo, lo digo por experiencias anteriores. Cuando te ves entre vómitos y caquitas, te hace un mundo la facilidad y rapidez de lavado.

Y para la base y los laterales, como son de plástico, basta pasar un paño húmedo, y queda como nueva. Podéis arrastrarla por toda la casa porque es dura y resistente… a prueba de fieras, vamos. Eso, os lo garantizo…

## Convertible en balancín

La versatilidad del Doomoo Nid hace que pueda convertirse en balancín colocando este accesorio:

Quedaría así:

No sé vosotros, pero yo he trabajado  y criado a la vez, y estar en el ordenador y dándole con el pie a la hamaquita para que se duerma después de las tomas no tiene precio.

## Arco de juegos

Y seguimos evolucionando… cuando el bebé empieza a moverse un poco y a fijarse en los objetos conviene estimularlo. Para ello, este arco de juegos adaptable a la hamaquita es ideal:

Tenéis varios diseños de tumbona para elegir, que podéis ver en la web de Babymoov (confieso que a mí me costó decidirme porque son preciosos).

¿Os ha gustado la hamaquita? ¿Qué opináis?