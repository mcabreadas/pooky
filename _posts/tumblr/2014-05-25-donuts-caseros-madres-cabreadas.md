---
date: '2014-05-25T19:00:33+02:00'
image: /tumblr_files/tumblr_n63gtlNB4q1qgfaqto1_1280.jpg
layout: post
tags:
- recetas
- familia
title: donuts caseros madres cabreadas
tumblr_url: https://madrescabreadas.com/post/86806413125/donuts-caseros-madres-cabreadas
---

![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto1_1280.jpg)  
Estirar la masa para que quede de unos 2 cm de grosor. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto2_1280.jpg)  
cortar la masa con forma redonda y hacel el agujerito con unos moldes. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto4_1280.jpg)  
Dejar que suba la masa en un lugar a unos 30 grados centígrados. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto5_1280.jpg)  
Freír con el aceite no muy fuerte para que se hagan bien por dentro. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto6_1280.jpg)  
Para el glaseado: Hacer un almíbar y cubrirlos. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto7_1280.jpg)  
Hornear a 140 grados centígrados hasta que el almíbar escarche. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto8_1280.jpg)  
Servir. Ya están listos para tomar. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto9_1280.jpg)  
Opcional: rellénalos de lo que más te guste. ![](/tumblr_files/tumblr_n63gtlNB4q1qgfaqto10_1280.jpg)  
A disfrutar!  

## Receta: Donuts glaseados rellenos de chocolate

Este fin de semana nos apetecía preparar una merienda especial para las fieras. Les encantan los donuts, y la verdad, tenemos poca ocasión de hacer repostería, así que fue todo un acontecimiento. Os cuento cómo los hicimos.

## Ingredientes donuts de chocolate

450 g harina fuerte

25g levadura de pan

30 g azúcar

1dl leche

2 huevos

60g mantequilla

5 g sal

aceite de girasol para untar la mesa y la bandeja

## Modo de hacer los donuts rellenos dechocolate

Mezclar todos los ingredientes en la batidora con el accesorio de amasar, o en la Thermomix, como lo hicimos nosotros, unos 10 minutos hasta que quede lisa.

Antes de que suba, estirar la masa con un rodillo sobre una superficie lisa untada conaceite de girasol formando una capa de unos 2 cm de grosor aproximadamente.

Cortarla con dos cortapastas redondos, uno más grande que otro, pero el del centro no demasiado pequeño porque si no, al subir la masa puede quedar cerrado.

Si se quiere rellenar algún donuts, dejarlos sin agujero en el centro, no como hicimos nosotros, porque luego cuesta más meter el relleno.

Colocar los donuts en una bandeja untada conaceite de girasol y dejar subir en un lugar templado. Nosotros pusimos el horno a 30 grados centígrados, pero en verano no hará falta.

Una vez hayan subido, los freímos en la freídora con el aceite limpio, y a temperatura media, unos 140 grados centígrados. Tenemos que conseguir que estén dorados y hechos por dentro. Para quienes tenemos poca experiencia, un truco es abrir uno y regular la temperatura de la freidora.

## El glaseado de los donuts

Hacemos un almíbar con más cantidad de azúcar que de agua para que nos quede consistente y los bañamos uno a uno.

Los colocamos en una bandeja de horno y los metemos a temperatura suave para que escarche.

## El relleno de los donuts

Esto es opcional, he de confesar que una vez hechos no me pude resistir a rellenar alguno con Nutella. Así que, como no tenía manga pastelera, improvisé con esta jeringa. Derretí la crema de avellanas un poco en el microondas y fui inyectando muy lentamente (porque si no, se sale) haciendo varios agujeritos por la parte de abajo.

Lo mejor para rellenarlos es hacerlos sin agujero en el centro, no como hicimos nosotros. Pero lo logramos!

El resultado fue que mis fieras se pelearon por los de Nutella. 

## Consejo práctico para hacer donuts glaseados rellenos de chocolate

Como es una receta que lleva su tiempo, se puede hacer la masa, cortarla, y congelarla antes de que suba. Así podremos improvisar una merienda especial cuando queramos.

¿Os apetece probarlos?