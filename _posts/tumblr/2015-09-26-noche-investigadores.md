---
date: '2015-09-26T10:29:05+02:00'
image: /tumblr_files/tumblr_inline_o39zck4uBd1qfhdaz_540.jpg
layout: post
tags:
- crianza
- planninos
- gadgets
- colaboraciones
- ad
- tecnologia
- educacion
- familia
title: La noche de los Investigadores despierta vocaciones
tumblr_url: https://madrescabreadas.com/post/129902154354/noche-investigadores
---

La [Noche de los Investigadores](http://ec.europa.eu/research/researchersnight/index_en.htm) es un evento que se celebra simultáneamente en 280 ciudades europeas, en las que los científicos salen a la calle y acercan su saber a los ciudadanos, sobre todo niños, mediante talleres sobre cosas básicas, como “el poder de la molécula H2O”, o experimentos muy vistosos que a los más pequeños impresionan y que despiertan el científico curioso que todos llevamos dentro.

Se trata de un proyecto financiado por el HORIZON2020 en el que cada año investigadores de más de un centenar de ciudades europeas dedican una noche a la Divulgación de la Ciencia.

 ![](/tumblr_files/tumblr_inline_o39zck4uBd1qfhdaz_540.jpg)

Este año, además, la Noche Europea de los Investigadores conmemora su X aniversario, y nosotros hemos querido disfrutarla con los [científicos de la Universidad de Murcia](http://spin.udg.edu/rn13/la_noche_de_los_investigadores_13/?lang=es) en una jornada divertidísima de juegos, sensaciones, ilusiones ópticas, robots de Lego, coches solares donde aprendimos mucho.

 ![](/tumblr_files/tumblr_inline_o39zclBjfG1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39zclJoUX1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39zclyysr1qfhdaz_540.jpg)

Desde aquí, mi enhorabuena la [Universidad de Murcia](http://www.um.es/prinum/rn15/) por la organización y a los científicos con los que estuvimos hablando porque lograron transmitirnos la pasión con la que trabajan, y quizá despertaron alguna que otra vocación.

 ![](/tumblr_files/tumblr_inline_o39zclWtPU1qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.