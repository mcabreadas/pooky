---
date: '2017-01-15T15:30:19+01:00'
image: /tumblr_files/tumblr_inline_ojtsad5XNK1qfhdaz_540.png
layout: post
tags:
- bautizo
- trucos
- decoracion
- comunion
- familia
title: Mesa dulce para bautizos, bodas y comuniones
tumblr_url: https://madrescabreadas.com/post/155897023229/mesa-dulce-bautizo
---

El fin de semana pasado estuvimos en el bautizo de la hija de unos amigos, y lo primero que nos llamó la atención a Princesita y a mí fu la mesa dulce tan bonita y sencilla que elaboraron.

Se notaba que estaba hecha con todo el cariño y con mucho gusto.

Tras preguntar, porque soy curiosa por naturaleza, resultó que la había hecho [Isa García](https://www.instagram.com/isagar89/), un familiar del padre, lo que me animó a compartirla contigo, y a animarte a que hagas tu propio candy bar si tienes Bautizos, Boda o Comunión a la vista, ya que creo que con tiempo, paciencia, y mucho amor, no es complicado lograr resultados tan chulos como éste:

 ![](/tumblr_files/tumblr_inline_ojtsad5XNK1qfhdaz_540.png)

La temática está basada en las mariposas, los elementos naturales, como la rafia, la madera y el cartón , en colores rosa bebé, blanco, servilletas en azul turquesa, y vasos de cartón rosa con topos blancos. el toque de rojo de las regalices hace contraste, y queda muy bien.

 ![](/tumblr_files/tumblr_inline_ojtsaerONQ1qfhdaz_540.png)

Como ves, las mariposas parece que están volando en bandada sobre la mesa, y son de diferentes tamaños, pero siempre respetando los tonos que hemos dicho antes.

Te dejo esta [plantilla de mariposas](/tumblr_files/032df8ee4f09d0abe6a1df3fcbfe62a2.jpg) que he encontrado, pero puedes buscar la que más te guste, sólo necesitas ir recortando a ratitos la cartulina o [papel decorativo](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_il_tl?ie=UTF8&camp=3626&creative=24790&index=aps&keywords=papel%20decorativo&linkCode=as2&tag=madrescabread-21) que elijas.

Me encantan las mariposas porque me parecen super elegantes y bonitas. Ya sabes que las usé para la decoración del mural de la [Comunión de Princesita](/2015/05/19/c%C3%B3mo-celebrar-la-comuni%C3%B3n-sin-arruinarse.html).

Los recipientes para colocar las chuches y dulces son tarritos de cristal rodeados por [cinta de arpillera con encaje](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_il_tl?ie=UTF8&camp=3626&creative=24790&index=aps&keywords=cinta%20de%20arpillera&linkCode=as2&tag=madrescabread-21).

 ![](/tumblr_files/tumblr_inline_ojtsaeNxtY1qfhdaz_540.png)

Cajas de madera de fresas decoradas con esa misma cinta, con una blonda de papen por dentro, y pintadas de blanco, que puedes pedir en tu frutería, y etiquetas en cartón natural con cordón neutro que explican el contenido de cada recipiente:

 ![](/tumblr_files/tumblr_inline_ojtsafFTde1qfhdaz_540.png)

Bandejas de cristal y un [expositor de Cupcakes](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_il_tl?ie=UTF8&camp=3626&creative=24790&index=aps&keywords=expositor%20cupcakes&linkCode=as2&tag=madrescabread-21). Además, yo pondría también unas velas o farolillos para lograr una iluminación más cálida, lo que será el broche de oro de la presentación.

 ![](/tumblr_files/tumblr_inline_ojtsafP96v1qfhdaz_540.png)

Las golosinas son rosas y blancas la mayoría:

 ![](/tumblr_files/tumblr_inline_ojtsagLzg21qfhdaz_540.png)

Y el árbol de nubes parece más complicado de hacer de lo que realmente es. Te dejo este tutorial de [Charhadas](https://www.youtube.com/watch?v=4SvmsCwP01o) por si quieres intentarlo.

 ![](/tumblr_files/tumblr_inline_ojtsahquyC1qfhdaz_540.png)

Pero, sin duda, las grandes triunfadoras de la mesa dulce fueron las galletas de mantequilal decoradas con fondant, y las cupcakes de oreo, cuyas recetas te dejo:

-[Receta de galletas de mantequilla para decorar](/2015/12/07/decora-galletas-de-navidad-en-familia.html)

 ![](/tumblr_files/tumblr_inline_ojtsahTkXS1qfhdaz_540.png)

-[Receta de cupcake de oreo de Dulcesentimiento](http://www.dulcesentimiento.com/cupcakes-de-oreo-mejor-receta/)

 ![](/tumblr_files/tumblr_inline_ojtsajEEvn1qfhdaz_540.png)

¿No me digas que no te han entrado unas ganas locas de hacerlo tú misma?

Cuéntame, ¿alguna vez has preparado un buffete dulce de este tipo?