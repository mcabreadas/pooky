---
date: '2014-12-15T08:00:21+01:00'
image: /tumblr_files/tumblr_inline_pgauzmKhpf1qfhdaz_540.jpg
layout: post
tags:
- conciliacion
- crianza
- hijos
- educacion
title: Aprender a educar mejor gestionando hijos y acompañándolos.
tumblr_url: https://madrescabreadas.com/post/105250979653/educar-gestionando-hijos
---

Sabéis que los temas relacionados con la educación de mis hijos me interesan muchísimo, por eso quise estar presente en la jornada [“Gestionando hijos”](http://www.gestionandohijos.com/) del pasado 11 de diciembre en Madrid para aprender y también poder contároslo en el blog.

El evento iba especialmente dirigido para los trabajadores de las empresas invitadas, de ahí el carácter empresarial del nombre “gestionando hijos” en un intento, sin duda de acercar el mundo familiar y empresarial.   
El guiño a la conciliación familiar y laboral fue evidente cuando el presentador resaltó que un trabajador que sabe gestionar en casa la educación de sus hijos, estará en su puesto de trabajo mucho más feliz y el día de mañana, esos hijos serán grandes trabajadores felices. 

No me fue posible asistir en persona, por eso agradezco a Olga Montero, del blog [Cuéntamelo bajito](http://cuentamelobajito.blogspot.com.es/), que está abriéndose camino con fuerza en la blogosfera maternal, y que recomiendo por su originalidad y sensibilidad, que fuera en mi nombre y nos haya hecho una crónica tan interesante de todas las ponencias.

Os aseguro que cuando la leáis tendréis la misma sensación que yo he tenido de haber estado allí en primera fila:

“El lleno absoluto de la sala, unas 800 butacas, y la disposición del escenario, ya me hacían imaginar que estaba ante algo grande, algo que me iba a dejar muy buen sabor de boca.

Así fue.

Leo Farache, presentador de la jornada explicó el objetivo de la misma:

“que os llevéis a casa ilusión para educar”.

**1-Enrique Sánchez de León** , Director General de la APD (Asociación para el progreso de la dirección) nos convenció de la importancia de conectar el mundo de la empresa con la educación porque los niños de hoy pasarán las tres cuartas partes de su vida trabajando.

**“Conectar el mundo de la empresa con la educación”**

![image](/tumblr_files/tumblr_inline_pgauzmKhpf1qfhdaz_540.jpg)

En su opinión, la gran mayoría de los niños no tienen desarrollada la habilidad para triunfar, ni se les fomenta la ética, ni el compañerismo, y no son capaces de enfrentar grandes retos.  
 Si se les orienta y educa en este sentido conseguiremos que mañana aprendan a trabajar para vivir y no vivir para trabajar.

**2-Javier Urra** , seguramente el ponente más conocido por todos, psicólogo forense en la Fiscalía del Tribunal Superior de Justicia y Juzgados de Menores de Madrid, y recordado por ser el primer Defensor del Menor desde 1996 hasta 2001, dio una charla que, personalmente, me gustó y sorprendió.

**“¿Por qué es importante que padres y madres lo hagamos bien?”**

![image](/tumblr_files/tumblr_inline_pgauznx9uH1qfhdaz_540.jpg)

Javier insistió mucho en la importancia de preparar a los niños para que toleren la frustración, la soledad, a manejar el aburrimiento, los problemas, la pérdida… para que más adelante sepan gestionar tanto lo bueno como lo malo, porque la vida no es fácil, y hay que estar preparado emocionalmente para poder afrontarla sin miedo. 

Uno de los grandes retos que nos tocará vivir como padres y madres es enseñarles a no depender de las redes sociales, de los amigos virtuales (esto personalmente me da mucho miedo) de las opiniones del resto que muchas veces tanto daño hacen. Que sepan relacionarse con cualquiera, sin miedo y con decisión.

Precisamente hay más muertes en niños por suicidio que por cáncer o accidentes de coche, lo que significa que no estamos preparando a nuestros hijos para los retos y complicaciones de la vida.

Se dan cada vez más casos de niños con TDH, con angustia y depresión. Por eso hay que prepararles para interaccionar, para que vean que la vida no es como quisieran (luego os contaré la ponencia de María Jesús Álava “Sobreprotección”) para que sean niños adaptables, que sepan levantarse al caer.

**3-David Cuadrado** es Licenciado en Psicología del Trabajo y las Organizaciones y coach.  
 Además autor de un libro que va en la línea de la ponencia que desarrolló y tengo que hacerme con él ya mismo.  
 Se llama “Coaching para niños (o mejor dicho, para padres)”

**“Coaching para padres que quieren ser coachers de sus hijos”**

![image](/tumblr_files/tumblr_inline_pgauznmNFj1qfhdaz_540.jpg)

Me gustó muchísimo la presentación de David, fue clara, divertida y muy interesante.   
 Nos habló de crear hábitos, de hacerlo en 1 minuto y jugando.   
 Y de cómo intentamos desmontar hábitos que no nos gustan pero que nosotros mismos hemos fomentado repitiéndolos a diario y, posiblemente, sin darnos cuenta.  
  
 Lo esencial es crear POCOS hábitos pero BUENOS.   
  
 Luego nos habló del TEAMBUILDING y de cómo lo practica con sus 3 hijos.  
 Este concepto hace referencia  al trabajo en equipo,   
 Hay que educarles para que echen una mano cuando los demás lo necesiten, cosa que les ayudará en el presente y el futuro. Y así se creará un hábito.   
  
 Por último nos explicó qué es COOPETIR, palabra que representa la colaboración y la competición. Que se aplica fomentando hábitos de inteligencia emocional y solidaridad.

**4-Eva Bach** , pedagoga, hizo mucho hincapié en el apoyo que debemos dar y la confianza que debemos tener en los profesores de nuestros hijos y así demostrárselo a los pequeños.  
 De ahí el nombre de su fantástica ponencia:

**“Madres, padres y profesores remando en una misma dirección”**  
  
 ![image](/tumblr_files/tumblr_inline_pgauznAUgB1qfhdaz_540.jpg)

Eva nos dio 10 puntos clave para que padres y profesores se compenetren en la educación de los niños y a su vez estos últimos sean conscientes de este hecho y se encuentren más a gusto.

1.- La Confianza.

2.- Sintonía de fondo.  
Lo ideal es que ambas partes tengan un objetivo común, con unas motivaciones y sentimientos parecidos. Lo que no quiere decir estar de acuerdo en todo.

3.-Tener claras las funciones de cada cual.  
 No pretender los padres enseñar a los profesores a hacer su trabajo, ni los profesores juzgar o dar lecciones a los padres  
  
 4.-En el cole, las normas del cole, en casa, las de casa.  
  
 5.- Comunicación asertiva  
Se debe ser exquisito en el fondo y la forma cuando nos dirigimos a los profesores de nuestros hijos (hasta cuando no estemos de acuerdo).  
  
 6.-Los profes menos “brillantes” también educan  
 Paulo Coelho decía  “Incluso un reloj estropeado está acertado 2 veces al día”.

7.-Responsabilizar a nuestros hijos.  
 Facilitarles recursos para relacionarse de forma correcta con sus profesores.

8.- Contagio emocional positivo.  
 Es muy importante trasmitirles entusiasmo cuando hablamos de sus profesores.

9.-Ser buenos aliados de los profesores

"No tenemos que aliarnos con nuestros hijos contra su profesor si no con su profesor a favor de nuestros hijos”

10.- Revalorizar el prestigio social del profesorado.

**5** -Y ahora tengo que hablaros de la que fue mi favorita, sin duda, [**Catherine L’Ecuyer**](https://twitter.com/CatherineLEcuye).  
 No sé si fue su sensibilidad al exponer su ponencia, la dulzura con la que hablaba o su maravillosa forma de describir la Belleza con B.  
 Me atrapó hasta la última palabra.  
  
**“Educar en el asombro y la Belleza”**

![image](/tumblr_files/tumblr_inline_pgauzo7kX01qfhdaz_540.jpg)

El asombro es no dar las cosas por supuesto, es sorprenderse por algo que no se esperaba.  
 Actualmente los niños tienen tal cantidad de cosas que ya no se sorprenden, las tienen incluso antes de pedirlas.  
  
Por otro lado les cargamos de tareas, de actividades que no les permiten concentrarse. Lo que provoca cansancio y falta de atención.  
 Deberíamos respetar el ritmo de los niños y no lo   
  
Tampoco respetamos las etapas de la infancia. Los niños tienen que ser niños, no pedir ropa de Tommy Hilfiger o pendientes de Tous para su Comunión. Las niñas no deberían maquillarse.  
 Acortamos la infancia y alargamos la adolescencia.

![image](/tumblr_files/tumblr_inline_pgauzolT1w1qfhdaz_540.jpg)

Catherine dijo una frase que me encantó y que es de la marca cosmética DOVE:

“Habla con tu hija de belleza antes de que la industria de la belleza hable con ella”

**6-[Mª Jesús Álava](https://twitter.com/mjalavareyes)** es psicóloga y nos habló de sobreprotección. Y del peligro que conlleva.

**“Sobreprotección. Pasarse de padres y madres”**  
  
 ![image](/tumblr_files/tumblr_inline_pgauzoG9Uo1qfhdaz_540.jpg)

 

La sobreprotección suele ocurrir por el sentimiento de culpa de los padres.  
 No debemos centrarnos en las cosas que no podemos hacer con nuestros hijos, sino en las que sí hacemos. Aprovechar ese tiempo al máximo. Y tener en cuenta que el sentimiento de culpa no exime de la responsabilidad de educar. Sobreproteger no es educar.  
  
  
 La sobreprotección se puede detectar fácilmente:  
 Cuando no dejamos a los hijos hacer cosas para las que están preparados, evitando que desarrollen su autonomía, nos anticipamos a satisfacer sus necesidades o lo hacemos en exceso, no les dejamos desarrollar recursos y estrategias que les serán útiles en el futuro.

En definitiva los niños deben tener con sus padres un vinculo que les aporte seguridad, estabilidad y amor, pero debe ser un vinculo sano, no de absoluta dependencia.  
 

**7** -Después la pedagoga **Maite Vallet** , nos habló de castigos y consecuencias.   
  
**“No a los castigos, sí a las consecuencias”**

![image](/tumblr_files/tumblr_inline_pgauzpiY1d1qfhdaz_540.jpg)

El castigo es espontaneo, lo solemos “poner” cuando estamos enfadados por algo que el niño ha hecho mal.   
 Por otro lado la consecuencia está pactada. “no podrás ir al parque si no recoges tu habitación” sin gritos, con dialogo, explicando todo claramente.   
 No debe haber castigos y aunque tampoco debe haber premios, la consecuencia de hacer algo es el premio en sí.  
 Esto tan simple es básico para que el niño colabore.

**8** -A continuación subió al escenario **Antonio Tobalina** que trabaja por las noches en el SAMUR, imaginaos lo que ha podido ver.  Forma cada año a miles de alumnos en los colegios para prevenirles de los peligros que se pueden encontrar en la noche con su programa ¿La noche es tuya?  
 Fue el encargado de ponernos los pelos de punta dándonos datos de consumo de alcohol en adolescentes.

**“Educar en la noche. Nunca es demasiado pronto ni demasiado tarde** ”

![image](/tumblr_files/tumblr_inline_pgauzpqATd1qfhdaz_540.jpg)

Comenzó dándonos estadísticas. En la foto podemos ver que el 10% de adolescentes llegan más tarde de las 8 de la mañana a casa después de una noche de fiesta.  
 Aunque también le preocupaba el dato de los chicos que no salen.  
  
 El problema según Antonio es que los adolescentes no saben gestionar la noche.

No son conscientes de los problemas, ya no solo físicos y psíquicos de la ingesta de alcohol. Por eso hay que explicarles que puede tener como consecuencia detenciones, multas por beber en la calle o se pueden arrepentir al día siguiente por haber hablado de más cuando iba bebido, incluso en las redes sociales. 

La sociedad, la publicidad, nuestras costumbres, enseñan a los niños desde pequeños que el alcohol no es malo.  
 Es recomendable que no nos vean beber alcohol, no olvidemos que somos su ejemplo.  
 Y cuidar mucho sus estados emocionales. Cuando un chico es feliz, puede plantearse que si bebe tendrá consecuencias negativas y puede que incluso no beba. Pero cuando uno está triste le da igual lo que pueda pasar y es mucho más fácil beber sin medida.   
 Por eso es tan importante educar a niños seguros de sí mismos. Así no se dejarán influenciar por su grupo de amigos.  
  
 Por último nos dijo que les expliquemos que en la calle no se puede beber, que no monten en un coche con un conductor que haya bebido, que no cojan el coche si han bebido, que solo tengan relaciones sexuales consentidas y siempre con protección. Que tengan cuidado con las fotos y videos que puedan hacer durante una noche de fiesta y las redes sociales, ya no solo por el daño que pueden hacer a terceros, también porque pueden cometer un delito.  
 Todo esto que nos parece tan evidente, ellos ni lo piensan.   
  
 Y para acabar, nunca debemos alardear o contar como algo gracioso que nosotros a su edad hacíamos lo mismo, o peor.

**9-** Y apareció en escena **Iñigo Pirfano** , filósofo y director de orquesta. Su forma de hablar, con tanta pasión, tanta fuerza, hipnotizaba.

Nos habló de:

**“La resonancia: El mejor aliado”**

![image](/tumblr_files/tumblr_inline_pgauzpuxi31qfhdaz_540.jpg)

Iñigo nos dijo, como muchos pensamos, que el problema de la educación está tratar a todos los niños por igual.

“Cada niño es una joya especial”. Por ello no se les puede educar con patrones establecidos, en serie.  
 Nos habló de la empatía, de su importancia y de cómo las personas necesitamos sentirnos queridos y comprendidos.   
 Y nos dio las cualidades que él cree que son fundamentales en los profesores y educadores;

Comprensión, exigencia, corazón, prudencia, grandeza, humildad y seguridad.

**10-Fernando Botella** , por último nos convenció de que la vida el bella, y así podemos transmitirlo a nustros hijos si les enseñamos a vivir con ITV: Ilusión, talento y valentía

Como podéis ver fue una jornada intensa, de 6 horas. Por suerte nos dieron un desayuno fantástico a las 12 donde pudimos compartir opiniones.  
 Realmente he tenido muchísima suerte de poder vivirlo en primera persona.”

Ahora toca reflexionar y poner en práctica lo aprendido.

¿Crees que tienes que cambiar cosas en tu forma de educar?