---
layout: post
title: Vuelta al trabajo tras la baja de maternidad. Mujer, rompe el cascarón
date: 2014-02-18T08:00:24+01:00
image: /images/uploads/depositphotos_321577370_xl.jpg
author: maria
tags:
  - mecabrea
  - crianza
  - postparto
  - cosasmias
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/77048151652/vuelta-al-trabajo-tras-la-baja-de-maternidad
---
Te has convertido en una mujer más fuerte. Quizá las sombras te impidan verlo, pero, creemé. Puedes con lo que te propongas, amiga.

Todavía te envuelve el aroma de tu bebé, la suavidad de su dulce piel, el calor de su contacto, la seguridad de tu hogar donde te has refugiado todo este tiempo, donde te sientes cómoda y protegida… pero sabes que puedes. 

Puedes volver ahí fuera, eres una mujer nueva, llena de vida, que has participado en una de las grandes proezas de la naturaleza. Es normal que a veces te invadan la inseguridad y el miedo, que el mundo de repente se haya convertido en un lugar hostil a tus ojos, pero son ellos los que no saben con quién se van a enfrentar ahora, en esta nueva etapa de tu vida.

Van a ver a una mujer que defiende a los suyos con uñas y dientes, una mujer que, si antes no se atrevía a veces a decir lo que pensaba, va a hablar con vehemencia, una mujer que va a proteger y cuidar a su familia por encima de todo sacando energías de donde otros ni saben, que sabe compatibilizar tareas y encajar horarios imposibles en cuestión de segundos. Una mujer empática, más humana, que es capaz de ganar la confianza de las personas que la rodean y lograr grandes cosas, una mujer que pisa fuerte, que tiene magia que saca de sus hijos, de su instinto, de su naturaleza de madre que ha esperado, ha parido, ha criado, ha luchado, ha llorado, ha sufrido, ha temido,que sabe sobrevivir sin dormir, comiendo a deshoras, y aún así logra dar lo mejor de sí misma a su familia.

![image](/images/uploads/depositphotos_195259206_xl.jpg)

Porque sabes que lo has hecho bien, sabes que eres única y que al menos para una personita eres la mejor y más importante del mundo. Ese es tu secreto, mujer, lo que te mueve, lo que te hace mágica porque te da más poder que cualquier otra motivación del mundo…

No lo dudes ni por un instante rompe el cascarón, es fácil, tú puedes, ya estuviste fuera antes, antes de ser madre. 

Ahora eres mejor, ahora puedes más. 

Cómete el mundo, mujer.



*Foto gracias a [sp.depositphotos](https://sp.depositphotos.com/)*