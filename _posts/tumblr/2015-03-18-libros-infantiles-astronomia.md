---
date: '2015-03-18T08:00:20+01:00'
image: /tumblr_files/tumblr_inline_nlb9wfrKWW1qfhdaz.jpg
layout: post
tags:
- crianza
- trucos
- libros
- planninos
- recomendaciones
- educacion
- familia
title: 'Libros: Astronomía para niños'
tumblr_url: https://madrescabreadas.com/post/113943227921/libros-infantiles-astronomia
---

![image](/tumblr_files/tumblr_inline_nlb9wfrKWW1qfhdaz.jpg)

Aprovechar la curiosidad natural de los niños por la ciencia y el conocimiento es la mejor baza con la que contamos los padres para introducir a nuestros hijos en la lectura.

Ya sabéis que soy partidaria de completar lo aprendido en el cole con otras materias que a ellos les llamen la atención de forma personalizada.

Es el caso de mi hijo mediano, a quien le apasiona todo lo relacionado con el espacio, los planetas, agujeros negros…

Por eso pedí a Boolino que me enviara este libro, con la intención de que mi peque lo devorara y contaros nuestra opinión.

Se trata de [“Astronomía para niños](http://www.boolino.es/es/libros-cuentos/astronomia-para-ninos/)”, de la editorial Parramón, un libro con un diseño especial con cubierta dura con tapa frontal que llama mucho la atención de los niños y de entrada, les invita a abrirlo con curiosidad.

 ![image](/tumblr_files/tumblr_inline_nld3bniXBn1qfhdaz.jpg)

Lo veo ideal como regalo de comunión, por ejemplo, porque es justo para esa edad entre 9 y 10 años.

El contenido también está estructurado de una forma muy atractiva y amena con datos curiosos, entrevistas ficticias a los grandes sabios de la historia, experimentos que despiertan el lado más científicos de los chavales y que les permitirán  saber cómo funciona el universo, qué percepción del mundo se ha tenido a lo largo de la historia, las dificultades de los astrónomos para transmitir sus descubrimientos… 

A mi hijo le ha apasionado, y os lo recomienda para niños que cuando miran al cielo por la noche no paran de hacer preguntas.

Es vuestro caso?