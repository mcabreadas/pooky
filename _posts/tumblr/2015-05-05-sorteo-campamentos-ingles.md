---
date: '2015-05-05T17:30:24+02:00'
image: /tumblr_files/tumblr_inline_nnvtez45tm1qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- planninos
- recomendaciones
- educacion
- familia
title: Evento presentación y sorteo de campamentos de inglés para niños
tumblr_url: https://madrescabreadas.com/post/118204880209/sorteo-campamentos-ingles
---

¿Estáis pensando en aprovechar el verano para mandar a vuestros hijos a aprender inglés a un [campamento](http://www.tecs.es/tecs/campamentos/) o a algún [curso de inglés en el extranjero](http://www.tecs.es/tecs/viajes-idioma-jovenes/)?

Nosotros sentimos que ya llegó la hora de dar un paso más, que la formación en el colegio y academia está muy bien, pero falta ese plus que les da a los niños la inmersión lingüística, sin embargo, todavía no llegó el momento de salir al extranjero y preferimos que se queden cerquita de casa.

Nos da un poco de reparo porque nunca han salido, y nos preocupa mucho que sientan seguros y atendidos aunque no estén con nosotros. En una palabra, que se van a preocupar de ellos y vamos a estar puntualmente informados de todo, y si puede ser a diario, mejor.

No sé si soy demasiado protectora, pero es que tienen 7 y 9 añitos, y quiero que para ellos no suponga un problema, ni lo vivan angustiados.

Por eso me he estado informando de las alternativas que hay en el mercado, y me han gustado especialmente, por el calor que se desprende, al menos en su web, los [campamentos de TECS](http://www.tecs.es/tecs/), y creo que merece la pena informarse muy bien.

Por eso, por si estáis planteándoos lo mismo que nosotros, nos viene que ni pintado este evento donde TECS nos presentará sus campamentos de verano y cursos de idioma en el extranjero que creo que nos puede venir muy bien para aclarar ideas y quitarnos ese posible miedo a que nuestros hijos salgan de nuestras faldas.

 ![image](/tumblr_files/tumblr_inline_nnvtez45tm1qfhdaz_540.png)

Además de para informarnos, merece mogollón la pena ir porque **sortean 4 campamentos de verano y descuentos del 50% en 4 cursos de inglés** , dos de ellos en Inglaterra y dos en Irlanda. 

Os dejo este video, donde por si queréis más información:

<iframe width="500" height="315" src="https://www.youtube.com/embed/0YD49iqgNA4" frameborder="0"></iframe>

Si os apetece y podéis, acercaos este jueves a las 19:00 h, a Rafael Hoteles Atocha, en C/ Méndez Álvaro, 3, Madrid, y así podréis informaros de primera mano. 

¿Y, quién sabe?

Lo mismo tu peque se va gratis de campamento este verano! 

¿Te imaginas?