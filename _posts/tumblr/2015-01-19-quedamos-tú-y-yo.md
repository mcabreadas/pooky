---
date: '2015-01-19T08:49:24+01:00'
image: /tumblr_files/tumblr_inline_p9ttekOvjB1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- crianza
- hijos
- maternidad
- cosasmias
- familia
- mujer
- bebes
title: Quedamos tú y yo
tumblr_url: https://madrescabreadas.com/post/108529359454/quedamos-tú-y-yo
---

![](/tumblr_files/tumblr_inline_p9ttekOvjB1qfhdaz_540.jpg)

Cambios que dan miedo, que nunca planeé, para los que no me preparé, que ni siquiera sospeché.

Mucho tiempo cerrando los ojos a la realidad, hasta que sentí su bofetada. 

Soy tan importante para ellos que no puedo flaquear, no puedo estar sin estar. Dependen de mí, me necesitan desesperadamente, y me necesitan contenta, tranquila, amorosa… Y, además, lo merecen.

Y creo que yo también lo merezco, aunque lo temo al mismo tiempo.

Temo dejar de ser yo, o no saber quién soy realmente. Y temo también no saber quién seré en el futuro. Qué me quedará de mí misma, de la mujer que he sido hasta ahora.

Ahora las mañanas son nuestras… quedamos tú y yo… y todo tu cariño inmenso, tu risa contagiosa y tus intensos ojos de almendra que me sigues clavando cada vez que me miras.

Tengo suerte de llevarme lo mejor de ti en estos años que no volverán, de ser la primera en presenciar tus logros y aprendizajes, y de ser la única capaz de traducir tus primeras frases algunas veces.

Tengo suerte de poder estar para lo que tus hermanos mayores necesiten, y más atenta con papá… más sonriente.

Espero hacerlo bien, y también espero reencontrarme a mí misma en el camino. Sin fallaros, sin fallarme.