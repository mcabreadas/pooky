---
layout: post
title: El valor de los regalos de cumpleaños
date: 2013-05-31T14:43:00+02:00
image: /tumblr_files/tumblr_mnnxx1JSVk1qgfaqto1_400.jpg
author: maria
tags:
  - mecabrea
  - crianza
tumblr_url: https://madrescabreadas.com/post/51799891944/el-valor-de-los-regalos-de-cumpleaños
---
Últimamente he ido a varios cumpleaños infantiles de amiguitos de mis hijos, y sigo sorprendiéndome, por lo antinatural, de la cantidad salvaje de regalos que se le hacen al cumpleañero. Ya no me meto con la cantidad de niños, ni con las actividades enlatadas que se suelen hacer en las macrofiestas, ([porque ya lo hice](https://madrescabreadas.com/2011/06/20/aquellos-cumpleaños-felices/)), pero ver al pobre protagonista en su trono con cara de pasmado, sin saber a qué atender, ni cuál abrir, porque si se pone a disfrutar o apreciar el primero destapando el cartón tranquilamente para investigar cómo se monta o cómo funciona, se le va el tiempo de abrir los otros 20 que tiene en cola, me parte el alma.

## Multitud de regalos de compleaños

Con lo que el angelito no logra disfrutar ni apreciar cada detalle de sus amigos. Lo siento, pero lo veo un espectáculo dantesco, casi de película de terror. En serio. Imaginaos: 20 niños gritando y corriendo alrededor del homenajeado por el subidón de azúcar de la tarta y la emoción de entregar su regalo, los flashes de las cámaras de fotos disparándose para inmortalizar el mortal momento, una montaña de papeles de regalo arrancados a toda prisa y arrugados, otra de regalos ya abiertos, y otra de los que quedan por abrir. El cumpleañero suda, se aturde, no es capaz de coordinar ante tal avalancha de presentes que, por otra parte, le inducirán a pensar que eso es lo normal. A él y al resto de asistentes a la fiesta.

## Se tiende a normalizar el mogollón de regalos decumpleaños

Por eso, cuando mi hijo recibe por su cumple 3 ó 4 regalos (y ya es mucho, porque tiene para jugar todo el año) se queda ojiplático y piensa. “Qué he hecho yo para merecer esto?” Menos mal que se lo explicamos y lo va entendiendo, pero se desarrolla en él un ansia de consumismo y de tener muchísimos juguetes, contra lo que es difícil luchar.

## Regalos que parecen de comunión

Creo que estamos logrando un efecto perverso en nuestros niños con esta costumbre. Igual que la idea de juntar todo el dinero y comprar un super regalo del nivel del que haría un Padrino en la Primera Comunión de su ahijado. Que yo he visto regalar bicicletas, eh? Que estamos perdiendo los papeles!. Antes la bici era un regalo especial y muy esperado, que esperaba su momento: unos Reyes, por ejemplo, o la Comunión… Pero ahora se está perdiendo el valor de los detalles.

Sé que es difícil, casi suicida ir contra corriente, pero hay cosas por las que no quiero pasar.Y vosotros?