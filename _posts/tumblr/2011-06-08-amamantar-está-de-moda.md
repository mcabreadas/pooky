---
layout: post
title: Amamantar está de moda
date: 2011-06-08T18:11:00+02:00
image: /images/posts/amamantar/p-amamantar.jpg
author: maria
tags:
  - crianza
  - lactancia
tumblr_url: https://madrescabreadas.com/post/6322209013/amamantar-está-de-moda
---
A raíz de la indignación de [@gem_rilo](http://twitter.com/gem_rilo): *“un señor m llamó la atención por amamantar a mi hijo en la sala d espera dl médico, porq n estos tiempos no tenemos q hacer eso”*, y mi consiguiente cabreo en tuiter decidí compartir mi experiencia como amamantadora de bebés, dos, para ser exactos.

Yo soy muy vergonzosa por naturaleza, y me preocupaba enormemente tener que descubrirme los pechos delante de la gente, en la calle, en la tienda, incluso en mi despacho, que para eso es mío y hago lo que quiero en él. 

Pero ese tema me preocupaba porque tenía claro que iba a dar el pecho a mi bebé a demanda y con todas las de la ley, donde estuviera. Entonces se me encendió la bombilla viendo un capítulo de la serie “Mujeres desesperadas”, en el que Lynette Scavo, en el funeral de su amiga, da de mamar a su bebé cubriéndose ligeramente su hombro y la cabeza del niño con un arrullo. Me pareció elegante a la par que discreto.

Entonces lo probé, y quedé encantada con el resultado porque es comodísimo; el bebé está encantado porque le sirve como de refugio para estar tranquilito, le quita luz para quedarse dormido después de comer y, en invierno está calentito. 

En verano me hice una colección fulares de algodón finos y de diversos colores para conjuntar con mi ropa, llevándolo siempre al cuello o sobre los hombros a modo de complemento. No veáis cómo se entretenía mirando los colores y jugueteando con los flecos mientras mamaba. Estábamos las dos encantadas con la idea de Lynette!.

Ya me podían invitar a fiestas, cenas, viajes… que yo no tenía ningún problema, incluso en la mesa del restaurante cenando con una mano y con la otra sujetando al bebé. Y lo curioso es que quien miraba pensaba que el bebé estaba durmiendo y por eso lo tenía así. Ni se percataban de que estaba mamando.

Me sentía libre. Con mi fular y mi bebé podía ir donde quisiera.

*Photo by Jan Kopřiva on Unsplash*