---
date: '2017-03-16T13:02:03+01:00'
image: /tumblr_files/tumblr_inline_omwmupCzSb1qfhdaz_540.png
layout: post
tags:
- ad
- colaboraciones
- moda
- familia
title: Moda infantil. Los colores de primavera
tumblr_url: https://madrescabreadas.com/post/158471715084/colores-moda-primavera
---

No hay cosa que más pereza me dé que el momento de hacer el cambio de armario de los niños, pero esta temporada me he propuesto empezar con tiempo porque no quiero que me pase lo mismo que el año pasado, que llegó el calor de golpe y, como no había sacado la ropa de primavera del año anterior todavía, al final compré cosas de más, y me junté con un montón de camisetas y [pantalones cortos de niño](http://www.mayoral.com/es/espana/bermudas-y-shorts-300000000000089958--5) para mis 2 hijos y otro tanto para la niña, que no necesitaban realmente porque les servían muchas cosas del año anterior.

Princesita es muy delgadita, así que me podría haber ahorrado comprar algún que otro [pantalón corto de niña](http://www.mayoral.com/es/espana/pantalones-300000000000090119--5), porque como se llevan mucho los shorts, no importaba que le estuvieran más cortos que antes. Y la verdad es que al pequeño lo podría haber equipado casi del todo con la ropa heredada del mediano.

Ya os di unos trucos para [ahorrar en la ropa de los niños](/2015/02/27/ahorro-ropa-ninos.html), que os vendrán muy bien sobre todo si sois familia numerosa, como nosotros.

Así que ya estoy manos a la obra, y me hecho una lista de lo que necesitan, y si luego quiero darles algún capricho, pues se lo doy, pero es bueno tener las cosas claras antes de lanzarse a comprar porque con el colorido tan chulo que hay para esta primavera-verano, va a ser difícil resistirse.

Los colores de esta temporada son fascinantes, por eso he estado repasando las propuestas que vimos en la [Feria Internacional de Moda Infantil](http://www.fimi.es/), y los colores que más se llevarán esta temporada.

## El fucsia

Es alegre, tropical y festivo y, sobre todo a las niñas, les encanta. Llama mucho la atención, y es el color de las flores más bonitas. Es mejor combinarlo con tonos neutros o con negro para darles un estilo más cañero.

 ![pantalon nina fuxia](/tumblr_files/tumblr_inline_omwmupCzSb1qfhdaz_540.png)
## El azul agua

No puede faltar en verano el color del agua de la playa de las islas tropicales con el que llevamos soñando todo el invierno para descansar y relajarnos. Es un color fresco con un punto verdoso.

Lo pueden llevar en total look o combinado con rosa palo o con neutro, o con el naranja para lograr un contraste súper divertido para sus looks veraniegos.

 ![ropa verano ninos](/tumblr_files/tumblr_inline_omwn4z66MQ1qfhdaz_540.png)
## El rojo 

Un rojo vivaz y extravagante que irradia energía y pasión es una de las tonalidades más atrevidas para esta temporada.

Lo pueden llevar en total look y sus combinaciones estrella se harán con el negro/blanco, una coordinación que nunca falla, o con el azul para un look más náutico.

 ![look verano amaya](/tumblr_files/tumblr_inline_omwnqbXP7X1qfhdaz_540.png)
## El azul lapis

Es un tono de azul muy intenso, un azul lapislázuli fuerte, un color frío y a la vez vivo. 

Para combinarlo puedes jugar con el contraste del rojo o blanco, aunque en total look me fascina. 

 ![mono azul amaya](/tumblr_files/tumblr_inline_omwodiL4sR1qfhdaz_540.png)
## El verde

Es el color de las hojas de primavera y el primero en el que pensamos cuando hablamos de ella.

Esta sensación de frescura queda enfatizada con la combinación con el blanco.

 ![corona flores nina](/tumblr_files/tumblr_inline_omwo40FyrQ1qfhdaz_540.png)
## El amarillo

El amarillo es veraniego, cálido y lleno de energía, es un color alegre y potente que transmiteuna sensación de calor, es el color de las flores y de los días soleados, un tono brillante con el que dar alegría a sus looks.

Lo pueden llevar en total look o combinado con el neutro para un resultado más tenue o con el negro creando contraste. 

 ![falda rayas](/tumblr_files/tumblr_inline_omwo8yfMMz1qfhdaz_540.png)

Espero haberte orientado sobre los colores que más se llevarán esta primavera-verano, aunque la verdad, a mí siempre me gusta que ellos tengan la última palabra, y se sientan a gusto con la ropa que llevan dentro de un orden.

¿No me digas que no te apetece que llegue ya el buen tiempo para que estos colores inunden tu casa?

_Fotos: Marcos Soria_