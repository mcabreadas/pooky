---
date: '2015-10-20T17:00:20+02:00'
image: /tumblr_files/tumblr_inline_o39z96H5XT1qfhdaz_540.jpg
layout: post
tags:
- halloween
- cine
- planninos
- colaboraciones
- ad
- eventos
- familia
title: Preestreno de Hotel Transilvania 2
tumblr_url: https://madrescabreadas.com/post/131556962047/premier-hotel-transilvanya
---

Se acerca Halloween y para calentar motores asistimos a la premier de[Hotel Transilvania 2](http://sonypicturesreleasing.es/watch/hotel-transilvania-2) que se estrena para el público el 23 de octubre, a la que Sony Pictures tuvo la amabilidad de invitar a este humilde blog para alegría nuestra y, sobre todo de nuestra colaboradora [@Dandocoloralosd](https://twitter.com/dandocoloralosd) y su familia, quienes tuvieron la suerte de disfrutar de esta divertida película, y del tinglado que organizó la productora para el gran día del preestreno, que fue una pasada.

## La trama

Si os gustó la primera de Hotel Transylvania, ésta os va a encantar porque todo parece estar cambiando en el Hotel Transilvania… La rígida política de “sólo para monstruos” establecida por Drácula para el hotel, se ha relajado por fin y se han abierto las puertas también a huéspedes humanos.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/2-JK6fmmzMg?rel=0" frameborder="0" allowfullscreen></iframe>

Pero, de ataúdes para dentro, Drácula está preocupado al ver que su adorable nieto Dennis, medio humano y medio vampiro, no muestra signos de ser vampiro. Así que mientras Mavis está ocupada visitando a sus suegros humanos en compañía de Johny, e inmersa ella misma en su propio choque cultural, el abuelito Drácula recluta a sus amigos Frank, Murray, Wayne y Griffin para hacer que Dennis pase por un campamento de “entrenamiento de monstruos”.

Lo que ellos no saben es que el gruñón  padre de Drácula, Vlad, está a punto de visitar a la familia en el hotel. Y cuando Vlad descubra que su bisnieto no es de sangre pura, y que los humanos también son ahora bienvenidos en el Hotel Transilvania, las cosas se van a poner complicadas.

 ![](/tumblr_files/tumblr_inline_o39z96H5XT1qfhdaz_540.jpg)
##   

## El sarao

Alfombra roja, famosos, photocall, pintacaras, dulces terroríficamente deliciosos, concursos molones…  hicieron de la monstruosa espera para ver la peli una delicia.

**Famoseo del bueno**

Santiago Segura, Arturo Fernández y Dani Martínez, todos ellos parte del espléndido cast de voces de la película, estuvieron presentes, además vimos muchas caras conocidas como Sonia Ferrer, Inma del Moral, Juan José Padilla, Cristina Piaget, Eva Hache, Rosario Mohedano, Roberto Álamo, Gabino Diego, Antonio Garrido, Lluvia Rojo, Joaquín Reyes, Poti, Irma soriano, El Langui, Coral Bistuer, Amaya Valdemoro, Loreto Valverde, Camela, Manolo Royo, Lorena Verdum, Paca Gabaldón, Eva Isanta, Mónica Pont, Soledad Mallol y Sergi Arola… Casi ná! Famoseo del bueno!

 ![](/tumblr_files/tumblr_inline_o39z97v2Gr1qfhdaz_540.jpg)

**Blogueras de excepción**

También nos encontramos con blogueras de excepción como [No sin mis hijos](http://www.nosinmishijos.com/), con bebé incluído para hacer honor a su nombre, [Trucos de Mamá](http://trucosdemamas.com/), con sus pequeños vampiritos, Urbanandmom, [Contras y pros](http://contrasypros.com/), [Blog de Madre](http://blog-demadre.com/), [Trastadas de mamá](http://trastadasdemama.blogspot.com.es/), las chicas de [MyMMagazine](http://www.mujeresymadresmagazine.com/), [39 semanas](http://www.39semanas.com/), que tuvo que soportar al hombre invisible en el asiento de delante durante toda la película, [Historietas de Mamá](http://lashistorietasdemama.blogspot.com.es/)…

## La crónica

Pero mejor os dejo con [@Dandocoloralosd](https://twitter.com/dandocoloralosd), que nos lo contará todo con más detalle:

> El pasado sábado tuve la suerte de poder asistir al preestreno de la segunda parte de Hotel Transilvania , como colaboradora de mi querida [@Madrescabreadas](https://twitter.com/madrescabreadas) , y ésta es la crónica de esa sesión de cine matutina y especial:
> 
> **La llegada**
> 
> La llegada estuvo cargada de nervios. Normalmente no solemos decir a las peques donde vamos cuando son cosas que sabemos van a hacerles mucha ilusión por si acaso los elementos se ponen contra nosotros y al final no podemos ir como nos ha ocurrido ya en alguna que otra ocasión ante planazos especiales.
> 
> Según empezamos a subir la calle Gran Vía desde Plaza España empezaron a tratar de adivinar si era día de cine, o algunos amigos de fuera… Y al llegar a la última manzana, allí estaba el gran Cine Capitol con sus luces de neón poniendo Hotel Transilvania 2.

 ![](/tumblr_files/tumblr_inline_o39z97EjUI1qfhdaz_540.jpg)

> Al llegar a la altura vimos las figuras expuestas y alfombras rojas, mucha gente y muchos nervios. Hicimos un poco de cola y nos identificamos y ale-hop: teníamos nuestras entradas ( color verde que representábamos a un medio digital!). 
> 
> **Pintacaras, photocall, dulces y concurso de sustos**
> 
> Según entramos, y pese a acabar de desayunar nos ofrecieron unos monstruosos cupcakes, a los que mis pequeñas no pudieron resistirse ni el Santo tampoco,  como podréis suponer, y nos indicaron que subiésemos a la primera planta para poder participar del pintacaras y del concurso de sustos en el photocall preparado al efecto.

 ![](/tumblr_files/tumblr_inline_o39z977DOc1qfhdaz_540.png)

> Bendita paciencia la de las dos chicas y el chico que estaban pintando a los niños y niñas allí presentes. 
> 
> Nosotros tras esperar nuestro turno tuvimos una pequeña crisis y sólo mi mediana quiso maquillarse cual vampirita. ( Debo decir que hubo quien iba tan metido en la película y el concurso que llegaron vestidos de vampiritas o monstruitos! Deben tener padres con más recursos concursiles que los de mi hijas!)
> 
> Llegamos a los asientos y allí teníamos nuestro pack de botellita de agua y bolsita de gominolas, todo un detallazo yendo con peques! Gracias señores de Sony Pictures!
> 
> Mientras yo me quedaba con las mayores en los asientos haciendo tiempo, la peque con el Santo estuvo de un lado para otro viendo llegar a los invitados vips y haciéndose más fotos en el photocall.
> 
> **La presentación de la película**
> 
> De pronto llegó la hora,silencio, expectación y Santiago Segura salió a presentar la película junto con Daniel Martínez y el gran Arturo Fernández con su mítico “chatín”.

 ![](/tumblr_files/tumblr_inline_o39z98t0h41qfhdaz_540.jpg)
## Opinión sobre la película

> Y a partir de ahí se apagaron las luces y empezó una hora y media de diversión y entretenimiento familiar.
> 
>  No voy a hacer spoiler, porque eso estaría muy feo, pero debo decir que :
> 
> Una vez más la llamada al que ser diferente o no ser “normal” no es malo sino que hay que aceptar a cada uno como es y sacar lo positivo de cada uno de nosotros sigue ahí;  
> La llamada a la amistad, a que gracias a la amistad, el amor y el respeto al otro uno se encuentra así mismo y consigue ser quien es;  
> Por no olvidar la importancia de la familia y los verdaderos amigos en nuestras vidas. Que serán los únicos que estén a nuestro lado cuando menos lo esperemos.

 ![](/tumblr_files/tumblr_inline_o39z98Ky1n1qfhdaz_540.jpg)

> Lástima no poder poner en qué momentos y escenas se pone más de manifiesto cada uno de estos aspectos pero estoy segura de que según la veáis los iréis identificando.
> 
>  Espero que no tardéis en ir a verla y la disfrutéis tanto como lo hemos hecho nosotros cinco, que ya estamos con ganas de volverla a ver!
> 
>  Hasta la próxima!

## Gracias

Muchas gracias a nuestra cronista por este resumen, y por entrar a valorar la película desde el lado que más nos gusta a las madres, que es el de los valores que podemos transmitir a nuestros hijos.

Gracias a Sony Pictures por confiar en este blog [una vez más](/2015/01/20/preestreno-pelicula-annie-sorteo-entradas.html)porque nos encanta asistir a estos saraos para poder compartirlo con vosotros.

## Concursos molones

Y para los fans fans de Hotel Transylvania 2, os dejo esta [concurso que Comansi ha puesto en marcha, y con el que podéis ganar un set de figuritas de la peli](https://www.facebook.com/juguetescomansi/posts/1191688730857890:0).

 ![](/tumblr_files/tumblr_inline_o39z999Wrx1qfhdaz_540.jpg)

También os recuerdo que sigue en marcha el [concurso de Sony Pictures en el que podréis ganar un lote de productos Sony](https://www.facebook.com/sonypicturesSpain/app_401071313433238) si subís un video con un buen susto.

 ![](/tumblr_files/tumblr_inline_o39z99zopt1qfhdaz_540.jpg)

¿Estáis listos para el día 23? Espero que me contéis si les gusta a vuestro peques!

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.