---
date: '2012-12-06T10:58:00+01:00'
image: /tumblr_files/tumblr_melss8StO61qfhdaz.jpg
layout: post
tags:
- mecabrea
- crianza
- maternidad
- lactancia
- cosasmias
- familia
- parto
- bebes
title: 'Lactando Murcia por un #parto normal'
tumblr_url: https://madrescabreadas.com/post/37325183646/lactando-murcia-por-un-parto-normal
---

Ayer estuve por primera vez en una reunión de las que se organizan todos los meses por la asociación [Lactando de Murcia](http://lactando.wordpress.com/about/ "Lactando Murcia") porque quería consultar sobre la posición de amamantamiento de mi bebé de 4 meses por problemas de reflujo.

Pero, cuál fue mi sorpresa cuando recibimos la visita de Nieves, una enfermera matrona que está realizando un estudio para la Consejería de Sanidad sobre estrategias para la normalización del parto en la Región de Murcia, para recoger testimonios reales de partos.

Por fin alguien realmente interesado en mis experiencias de parto! Es más, por fin alguien que pude hacer algo para cambiar las cosas!

Éramos alrededor de treinta madres, con sus respectivos hijos (unos, bebés, otros, no tanto), y 3 padres… me encantó verlos allí tan implicados.

![image](/tumblr_files/tumblr_melss8StO61qfhdaz.jpg)

Una a una fuimos contando lo que más nos gustó de nuestros partos y lo que menos. El respeto y la atención con la que nos escuchó fueron exquisitos. Tomó buena nota de todo, incluso al final tuvimos la oportunidad de charlar en privado con ella quienes quisimos.

Tuve la oportunidad de hablar de mis tres partos delante de un grupo de personas a las que veía por primera vez, y me sentí cómoda, como en casa. Nunca pensé que me podría abrir así de repente, sin previo aviso a contar algo tan íntimo, y de lo que no se suele hablar en la vida cotidiana, al menos no de aquella manera como la que se habló ayer.

Todos nos escuchábamos con atención, empatizábamos y nos comprendíamos. Hubo risas y hubo lágrimas, pero sobre todo la certeza de que las cosas van a cambiar. El cambio será lento, es cierto, pero estamos en el camino.

Muchas gracias a Ana, a Rocío y a la moderadora (no recuerdo el nombre, sorry…), de [Lactando Murcia](http://lactando.wordpress.com/about/ "Lactando Murcia") por la acogida y por los consejos. Y muchas gracias a Nieves por hacer una gran labor de enlace entre las madres y sus bebés (los principales afectados), y el sistema sanitario, al que hay que dar la vuelta como a un calcetín para que algún día, al menos nuestras hijas lleguen a tener partos diferentes a los que tuvimos nosotras, partos respetados y normales.

Y tu parto, fue normal?