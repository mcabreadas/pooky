---
date: '2015-11-13T12:56:57+01:00'
image: /tumblr_files/tumblr_inline_o39z4rF09h1qfhdaz_540.jpg
layout: post
tags:
- colaboraciones
- ad
- eventos
- familia
- moda
title: Adelanto moda primavera verano 2016
tumblr_url: https://madrescabreadas.com/post/133127745489/primavera-moda
---

Seguro que lo habéis hecho alguna vez.

Habéis cerrado los ojos y os habéis imaginado en un lugar paradisíaco, lejos del mundanal ruido. 

¿A que sí?

Pues así mismo me sentí yo ayer, pero lo curioso es que no tuve que cerrar los ojos para verme rodeada del colorido de Brasil, samba, frutas tropicales, [caipirinha, que ya sabéis que me encanta porque os di la receta este verano](/2015/08/20/cocktail-caipirinha.html), y hasta un guacamayo encantador que posó así de bonito conmigo en una foto.

 ![papagayo caipirinha](/tumblr_files/tumblr_inline_o39z4rF09h1qfhdaz_540.jpg)

Y es que algunas marcas presentaron avances de sus colecciones de primavera verano 2016, y realmente nos trasladamos al trópico con [#DestinoRío](https://twitter.com/search?q=%23destinorio&src=typd).

 ![hamaca tucan](/tumblr_files/tumblr_inline_o39z4rDwYi1qfhdaz_540.jpg)

Me entra la risa porque no paré de acordarme durante todo el tiempo de la película [Inside Out](/2015/07/22/insideout-pixar-reves.html), cuando el piloto brasileño le dice a la mamá de Riley mientras le tendía la mano:

 “vola conmigo, gatinha”.

 ![vola conmigo gatinha](/tumblr_files/tumblr_inline_o39z4sBLOs1qfhdaz_540.jpg)
## La colección de Chicco

Los zapatitos de [Chicco](http://www.chicco.es) me volvieron loca. Siguen de moda las esparteñas en diferentes colores para combinar. Pero éstas son muy cómodas de poner a los niños por el elástico que tienen arriba, además de que sujetan el pie de los peques para que anden correctamente, y eso es importante.

 ![alpargatas chicco](/tumblr_files/tumblr_inline_o39z4s6Oe41qfhdaz_540.jpg) ![espartena chicco](/tumblr_files/tumblr_inline_o39z4t27fQ1qfhdaz_540.jpg)

Éstas últimas las combinaría con este vestidito playero que me encantó

 ![vestido lunares chicco](/tumblr_files/tumblr_inline_o39z4uW1OK1qfhdaz_540.jpg)

También presentaron bañadores

 ![banadores bebe chicco](/tumblr_files/tumblr_inline_o39z4vulew1qfhdaz_540.jpg)

Y una nueva bici sin pedales para que los más pequeños aprendan a montar. 

 ![bici sin pedales chicco](/tumblr_files/tumblr_inline_o39z4vWZcV1qfhdaz_540.jpg)

Mirad cómo aprendía Leopardito con el modelo de Chicco anterior. Creedme si os digo que es la mejor manera para iniciarlos.  

<iframe width="100%" height="315" src="https://www.youtube.com/embed/-PoRE_kZ63k?rel=0" frameborder="0" allowfullscreen></iframe> 

Y, como el nacimiento de mi sobrinita está próximo, no lo puede evitar, se me fueron los ojos a esta canastilla.

 ![canastilla bebe chicco](/tumblr_files/tumblr_inline_o39z4wZjAw1qfhdaz_540.jpg)

Impactante a la par que novedoso es el carrito Urban Plus Summer Edition este carrito totalmente veraniego con capota y cubierta con apertura de red para una ventilación perfecta para que el bebé esté fresquito fresquito.

 ![cochecito bebe amarillo](/tumblr_files/tumblr_inline_o39z4wHG0u1qfhdaz_540.jpg)

¡Que corra el aire!

 ![carricoche amarillo chicco](/tumblr_files/tumblr_inline_o39z4xoyeO1qfhdaz_540.jpg)
## Las auténticas Havaianas  

[Havaianas](http://www.havaianas-store.com/es/ninos/chanclas) para pies grandes y pequeños. Toda la familia a conjunto. Pero para los más chiquitines esta marca refuerza la sujeción con un elástico en el talón para que puedan andar sin que se les salga la chancla, y así, evitar traspiés.

 ![hawaianas chanclas playa](/tumblr_files/tumblr_inline_o39z4yCVhK1qfhdaz_540.jpg)

Y para las mamás y papás, alpargatas en los colores de moda para combinar con la ropa. Me quedo con las de rayas. ¿Y tú?

 ![alpargatas hawaianas](/tumblr_files/tumblr_inline_o39z4z03o81qfhdaz_540.jpg)
## IKKS

De [IKKS](http://www.ikks.com) me quedo con la capaza y el sobrero para mi Princesita, y la mini mochila y la gorra para mi campeón.

 ![cpaza ikks](/tumblr_files/tumblr_inline_o39z50wEFu1qfhdaz_540.jpg)

El conjunto de pantalón rosa y cazadora, con la camiseta de tirantes es genial.

 ![pantalon sudadera ikks](/tumblr_files/tumblr_inline_o39z50YvZ51qfhdaz_540.jpg)
## Disney

Disney y la firma de calzado inglesa [Irregular Choice](http://www.irregularchoice.com) lanzan para mamás frikis de Star Wars, o que quieren conquistar a los papás fans de la saga, estos botines con tacón láser que se enciende al pisar (doy fe) se convertirán en su objeto de deseo.

Por no hablar de las cuñas del guerrero imperial.

 ![zapatos starwars](/tumblr_files/tumblr_inline_o39z519bSO1qfhdaz_540.jpg) ![botin tacon starwars](/tumblr_files/tumblr_inline_o39z51j5Xq1qfhdaz_540.jpg)

Pero para las más discretas, estos dos modelos de slippers con la cara de Darth Vader o de R2D2 (mis favoritos) son ideales. La firma inglesa de calzado Irregular Choice ha creado esta colección justo a tiempo par ael estreno de la película El Despertar de la Fuerza. ¿Con cuál te quedas?

 ![manoleinas starwars](/tumblr_files/tumblr_inline_o39z52xGKv1qfhdaz_540.jpg)

Para ver más [modelos de zapatos de Star Wars](http://www.irregularchoice.com).

## Comida típica brasileña

Y para terminar de meternos en el ambiente tropical deleitamos nuestros paladares con unos platos típicos del Brasil que estaban espectaculares de buenos.

 ![catering brasileno](/tumblr_files/tumblr_inline_o39z52ztae1qfhdaz_540.jpg)

Muchas gracias a [Nota Bene](http://notabene.es/) por invitarme a su Open day primavera-verano 2016, y por la excelente organización. Que sepáis que me me han entrado unas ganas de irme de vacaciones que no son normales.

¿Nos vamos a Río?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.