---
date: '2017-04-23T18:33:24+02:00'
image: /tumblr_files/tumblr_inline_oovd7vG6hn1qfhdaz_540.png
layout: post
tags:
- postparto
- crianza
- embarazo
title: No es un cuento chino, es depresión postparto
tumblr_url: https://madrescabreadas.com/post/159903969654/depresion-postparto
---

La depresión postparto existe, no es como el hombre del saco, o un falso mito o simplemente que estamos muy cansadas después de dar a luz.  
A estas alturas no creo que haya que aclarar esto… o sí… 

Por si acaso me ha parecido muy revelador el estudio llevado a cabo por [Suavinex](http://elclubdelasmadresfelices.com/) basado en una encuesta a través de canales online.

## ¿Cómo sabemos si tenemos depresión post parto?

Los principales síntomas son los sentimientos extremos de tristeza, ansiedad y cansancio, aunque pueden ir mucho más allá.

Los patrones más repetidos responden a llantos injustificados, [tristeza](/2013/06/22/la-soledad-de-la-crianza.html), desesperanza, vacío emocional y estrés. La ansiedad, la angustia, el miedo, la [falta de energía](/2017/04/02/materidad-crianza-hijos.html) y la irritabilidad también se encuentran entre los síntomas más padecidos por las encuestadas.

Además, la duración de estos síntomas es significativa, pues un 23,03% de las madres los padecen durante más de 6 meses y un 10,65% durante más de un año.

Esto implica que en muchos de esos casos, no se trata de sentimientos aislados en el tiempo. 

 ![](/tumblr_files/tumblr_inline_oovd7vG6hn1qfhdaz_540.png)
## Ni son todas las que están…

Más de un tercio de las mujeres españolas encuestadas sufren depresión postparto. En concreto, un 35,14% confiesa haber pasado por esta situación.

Sin embargo, podrían ser muchas más, ya que hasta el 90,61% de las madres que declara no haberla sufrido sí ha tenido síntomas.

## ¿Por qué no somos conscientes de este transtorno?

Existe un gran  desconocimiento, y  hasta un 35,28% de las  madres encuestadas  afirma no haber sabido  lo que era antes de quedarse embarazadas. 

El dato es aún más concluyente si lo contrastamos con el casi 40% de mujeres que aún no son madres y que desconocen este trastorno.   
El estudio revela que las mujeres españolas no acuden a centros médicos cuando aparecen los síntomas. Así sólo un cuarto de las mujeres españolas que sufren depresión postparto lo descubre gracias a un diagnóstico profesional.

## La comunidad de internet ayuda 

El 43,3% que afirma  haber descubierto su  situación gracias a la  información encontrada en blogs, foros y redes  sociales. El porcentaje  restante, más de un 30%, confía en el diagnóstico de su entorno cercano.

Los blogs de maternidad [hemos hecho visibles problemas de los que anteriormente no se hablaba por considerarse íntimos o incómodos](/2013/06/22/la-soledad-de-la-crianza.html).

Creo que es un alivio para las madres poder escribir en Google lo que les preocupa y obtener información en cualquier momento (aunque hay que saber discernir la buena de la mala información).

## ¿Qué puede desencadenarla?

Hasta  un 52% de las mujeres que sufren esta situación tiene antecedentes de depresión o bipolaridad en anteriores etapas de su vida. 

También, destacan  hechos como el de que más del 20% de las mujeres que lo sufrieron viviese  [complicaciones que afectaron  directamente al parto](/2013/05/23/tres-partos-tres-semana-del-parto-respetado.html); o que casi un 13% de ellas no sintiese el suficiente apoyo emocional por parte de su pareja. 

Se detecta también cierta reincidencia, ya que el 32,89% de las mujeres del estudio que sufren depresión postparto ya la habían sufrido en embarazos anteriores.   
Solo el 25,88% de las mujeres españolas con depresión postparto es diagnosticada profesionalmente.

 ![](/tumblr_files/tumblr_inline_oovda51RAK1qfhdaz_540.png)
## ¿Cómo nos curamos?

Sólo el 17% de las mujeres que han sufrido esta depresión afirma haber recibido tratamiento profesional, ya sea mediante fármacos, terapia psicológica, o una combinación de ambas. 

De hecho, ni siquiera las mujeres que sí fueron diagnosticadas profesionalmente acuden a recibir un tratamiento (un 53,18% de ellas no ha recibido ningún tratamiento). 

Más de la mitad de las mujeres que la han sufrido (56,88%) señala haberlo superado por sí mismas, sin ayuda alguna.

## La mayoría no acudimos al médico

El estudio también indica que las mujeres españolas no recurrimos a profesionales para tratar de resolver nuestros trastornos de depresión postparto: el 75% no ha sido diagnosticado por un profesional. 

Por otra parte, cabe destacar que esta falta de apoyo en profesionales se sigue manifestando en el tratamiento de la patología. Así, solo el 46,82% de las mujeres que fueron diagnosticadas profesionalmente decidió tratar su trastorno con fármacos, terapia psicológica o la combinación de ambas.

## Mi conclusión

O sea, y esto ya lo deduzco yo, no el estudio, que entre que muchas no pensamos que estamos deprimidas, y otras tantas tiramos para adelante como si nada porque no nos queda otra, no le estamos haciendo caso a algo que en principio podría tener fácil solución, pero que si se agrava por no poner remedio podría desencadenar en un problema más serio.

Así que, lo que siempre te digo. Es cierto que [las madres podemos mucho](/2011/04/28/al-rescate-de-supermam%C3%A1.html), pero tenemos que pararnos a veces, ser conscientes de nuestros límites, no auto exigirnos demasiado ni pretender ser perfectas, pedir ayuda y dejarnos ayudar (y me lo estoy diciendo a mí misma la primera).

 ![](/tumblr_files/tumblr_inline_nkw013iLAo1qfhdaz.jpg)