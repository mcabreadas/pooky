---
date: '2015-02-20T14:08:00+01:00'
layout: post
tags:
- crianza
- trucos
- colaboraciones
- gadgets
- ad
- tecnologia
- recomendaciones
- educacion
- familia
- juguetes
title: TIC en los colegios de la mano de Miniland
tumblr_url: https://madrescabreadas.com/post/111559357269/tic-en-los-colegios-de-la-mano-de-miniland
---

<iframe src="https://www.youtube.com/embed/f97aTCzAEjI" width="500" height="315" frameborder="0"></iframe>

Ya sabéis que en casa nos gustan mucho los juguetes educativos porque creemos importante complementar lo aprendido en el cole con actividades informales o diferentes, y si son en familia, mucho mejor. Por eso somos fans de Miniland, que se acaba de subir al barco de las TIC, presentando un modelo de aprendizaje que promueve la combinación del juego con juguetes reales con la utilización de innovadores recursos digitales **.**

Nos guste o no, los niños prestan mucha más atención a una pantalla que a un papel. Es la época que les ha tocado vivir a nuestros hijos, por eso, los recursos educativos que combinan lo físico y lo virtual creo que son un acierto.

Miniland ha presentado recientemente 36 recursos educativos que promueven el juego con más de 200 juguetes

Las actividades propuestas se centran en contenidos marcados por el curriculum escolar. Los contenidos digitales pueden utilizarse en el ordenador, la pizarra digital, pantalla del aula o tablets. En sus hogares, los padres pueden reforzar conocimientos a través de las tablets, los smartphones o el ordenador de casa.

Todos estos recursos pueden encontrase en la web de Play Miniland [www.playminiland.com](http://www.playminiland.com/) **.** Los recursos están clasificados por edad, temática y contenido educativo y presentados en secciones especialmente diseñadas para profesores, padres y niños.

## El método Play Miniland

El método Play Miniland ofrece a educadores y padres unos innovadores recursos educativos para el desarrollo de sesiones de aprendizaje a través del juego.

Con este método se pretende **facilitar la tarea de los profesores** de Educación Infantil y Primaria, optimizando sus recursos y ampliando el banco de actividades digitales que encuentran a su disposición. En casa, **los padres podrán reforzar determinados aprendizajes,** combinando el uso de juguetes Miniland con los juegos digitales.

Las sesiones de aprendizaje propuestas con Play Miniland comienzan con la presentación del tema y los conceptos que se deben aprender, continúan con el juego con Juguetes Miniland Educational y concluyen con la utilización de los Juegos Digitales, que refuerzan los conceptos aprendidos.

## Aprendizaje activo a través de actividades divertidas

Play Miniland se fundamenta en la combinación de las dos tendencias más importantes de aprendizaje actual: Active Play y Game-Based Learning.

Se parte de un aprendizaje activo a través del juego con juguetes que se tocan, se comparten y se utilizan para aprender los conceptos trabajados en el aula, utilizando el elemento motivador del juego.

Las sesiones de aprendizajes terminan con el refuerzo que supone la utilización de juegos digitales, afianzando los nuevos conceptos adquiridos por los niños.

Este modelo de aprendizaje activo respeta el ritmo individual de cada niño, le permite aprender de los errores y aumenta su autoestima, poniendo así las bases para su motivación en la adquisición de nuevos aprendizajes.

La verdad es que si nos llegan a decir cuando nosotras íbamos al colegio que nuestros hijos iban a tener pizarras digitales en vez de encerados, y punteros, en vez de tizas no lo hubiéramos creído, pero así es, y hay tenemos que aprovechar estos recursos educativos en beneficio de nuestros hijos.