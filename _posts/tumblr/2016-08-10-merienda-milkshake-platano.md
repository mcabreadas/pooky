---
date: '2016-08-10T23:21:23+02:00'
image: /tumblr_files/tumblr_obppcicfca1qgfaqto1_1280.jpg
layout: post
tags:
- recetas
- crianza
- nutricion
- familia
title: merienda milkshake platano
tumblr_url: https://madrescabreadas.com/post/148756145259/merienda-milkshake-platano
---

![](/tumblr_files/tumblr_obppcicfca1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_obppcicfca1qgfaqto2_1280.jpg)  
  

## Milkshake energético para la merienda

En verano los niños no paran de jugar y de bañarse en la piscina o en la playa, y es difícil hacerles parar para que merienden en orden como a nosotras nos gustaría, por eso nos hemos inventado en casa este milkshake, ideal para reponer energía de un trago, y con ingredientes muy saludables y nutritivos.

En lugar de chocolate, ponemos [cacao puro del que os explicaba sus propiedades en este otro post.](/2016/02/28/hacer-chocolate-cacao.html)

Y lo endulzamos con [miel, tan beneficiosa para nuestro organismo,](http://www.natursan.net/como-tomar-la-miel-para-disfrutar-de-sus-propiedades-medicinales-y-curativas/) en lugar de usar azúcar.

Y como está delicioso, no podrán resistirse, y tú estarás tranquila porque estarán alimentados. Te explico cómo lo hacemos.

## Ingredientes

3 plátanos

½ l de leche

1 cucharada sopera colmada de cacao puro

2 cucharadas de miel

## Modo de hacerlo

Se mezclan todos los ingredientes con la batidora.

Meter al congelador media hora antes de servir para tomarlo muy frío.

También se puede tomar como helado si se vierte la mezcla en unas poleras, así no pararán de jugar para merendar y exprimirán el verano a tope.

 ![](/tumblr_files/tumblr_inline_obppznoDyt1qfhdaz_500.gif)