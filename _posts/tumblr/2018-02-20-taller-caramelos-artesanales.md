---
date: '2018-02-20T09:00:15+01:00'
image: /tumblr_files/tumblr_inline_p4cv6jtwoW1qfhdaz_540.png
layout: post
tags:
- familia
- planninos
- local
title: Taller de caramelos artesanales con Papabubble
tumblr_url: https://madrescabreadas.com/post/171084055675/taller-caramelos-artesanales
---

Este fin de semana participado en un [taller de piruletas de la mano de Papabubble](http://www.papabubble.com/es/), una empresa que nació en Barcelona en 2004 con el deseo de recuperar la magia de los auténticos caramelos artesanales. En la actualidad tienen 35 tiendas distribuidas por todo el mundo, en ciudades como Tokio o Nueva York, donde  se puede presenciar la fabricación en vivo en sus obradores y saborear su amplia gama de caramelos de altísima calidad fabricados con ingredientes tradicionales y sin conservantes.

 ![](/tumblr_files/tumblr_inline_p4cv6jtwoW1qfhdaz_540.png)

En [Murcia capital tenéis un Papa Bubble en la Plaza Hernández Amores](http://www.papabubble.com/es/tiendas/murcia/) (para los que somos de aquí, la “Plaza de La Cruz”, junto a la Catedral).

Después de haber observado cómo trabajan, diría que miman el caramelo porque los fabrican con las manos, sin maquinaria y con una gran habilidad en un proceso 100% artesanal. Si entras en una de sus tiendas podrás sentir el caramelo, olerlo, probarlo…

 ![](/tumblr_files/tumblr_inline_p4cvaeRpD91qfhdaz_540.png)

Además, si tienes un evento importante, como una comunión, bautizo o un cumpleaños especial, puedes encargar caramelos personalizados: tú eliges el sabor, los colores, el envase, incluso puedes poner el nombre del protagonista en el propio caramelo. ¿Te imaginas qué especial?

 ![](/tumblr_files/tumblr_inline_p4cvbrDPi11qfhdaz_540.png)

También puedes contratar un taller, y te montan un espectáculo Papabubble donde quieras. Me parece una actividad genial para colegios o para un cumpleaños infantil.

Nosotros participamos el sábado pasado en un taller de piruletas artesanales, y fue increíble.

## Taller de piruletas artesanales

Ya lo conté en vivo a través de mi [Instagram Stories](https://www.instagram.com/madrescabreadas/), donde he dejado la historia como destacada por si no lo has visto todavía:

Primero el maestro moldea el bloque de caramelo templado, y hace como un churro, que luego gira y enrolla como un caracol.

 ![](/tumblr_files/tumblr_inline_p4cvxtk5uX1qfhdaz_540.png)

Luego corta la piruleta

 ![](/tumblr_files/tumblr_inline_p4cvygzgjV1qfhdaz_540.png)

y la mete en un molde donde le termina de dar la forma redonda perfecta y la aplana. 

 ![](/tumblr_files/tumblr_inline_p4cvloJimW1qfhdaz_540.png)

Es entonces cuando los niños le ponen el palo.

 ![](/tumblr_files/tumblr_inline_p4cvmpI1M41qfhdaz_540.png)

¡Lo mejor es la cara de ilusión cuando sacan su piruleta ya montada!

 ![](/tumblr_files/tumblr_inline_p4cvnpDfz41qfhdaz_540.png)

Después la embolsan y esperamos un poco a que se termine de enfriar apelando a la poca paciencia de los niños, que se pusieron a hacer como que luchaban con ellas (sin comentarios…).

 ![](/tumblr_files/tumblr_inline_p4cvqf5Svg1qfhdaz_540.png)

Y cuando por fin las probamos… mmmm… deliciosas!

Muchas gracias a los chicos de Papabubble por la mañana mágica que nos hicieron pasar y por su amabilidad.

Hasta la próxima!

 ![](/tumblr_files/tumblr_inline_p4cw32XLIB1qfhdaz_540.png)