---
date: '2017-07-26T13:36:50+02:00'
image: /tumblr_files/tumblr_inline_otp24ltJeZ1qfhdaz_540.png
layout: post
tags:
- crianza
- colaboraciones
- puericultura
- ad
- seguridad
title: Joie y la contramarcha. No más excusas
tumblr_url: https://madrescabreadas.com/post/163443698274/a-contramarcha-sillas-joie
---

En verano la seguridad vial infantil es una de las mayores preocupaciones para los padres, ya que tiene lugar la [operación salida de la DGT](http://www.dgt.es/es/el-trafico/recomendaciones/) con motivo de las vacaciones y la necesidad de las familias de salir de viaje por carretera buscando la playa, la montaña o la tranquilidad del pueblo para reponernos de las duras jornadas de trabajo estival, y lograr la tan ansiada (y necesaria) desconexión de la rutina.

Hallar la mejor forma de que nuestros hijos viajen seguros en el coche con un sistema de retención adecuado a su peso y altura es uno de nuestros mayores quebraderos de cabeza.

Ya me he pronunciado en muchas ocasiones a favor de la contramarcha como el sistema más seguro para que los pequeños viajen en el automóvil, pero siempre he recibido muchos comentarios diciéndome que a muchas de vosotras os gustaría adoptar este sistema, pero no veis factible adquirir una silla de auto a contra marcha, bien por motivos económicos, ya que antes no había mucha oferta en España, o bien porque vuestro vehículo no era lo suficientemente grande para albergar una silla demasiado voluminosa.

## Para todas las familias

Por eso os quiero mostrar una alternativa muy interesante que he descubierto gracias a[Joie](http://es.joiebaby.com/), una marca que aterrizó hace dos años en el mercado español, y que me ha cedido una de sus sillas a contramarcha para que la pruebe y os dé mi opinión.

 ![](/tumblr_files/tumblr_inline_otp24ltJeZ1qfhdaz_540.png)

Se trata de la i-Anchor Advanced que, además de cumplir la normativa i-Size ECE R19/00 y poderse usar desde el nacimiento hasta los 4 años a contramarcha, y opcionalemte en el sentido de la marcha a partir de los 15 meses (aunque para qué le vais a dar la vuelta, si es más segura en contra…), es compacta, robusta y sumamente estable.

## Para todos los coches

Lo que más me ha gustado, porque sabía que a muchas familias les iba a solucionar un problema, es que se puede adaptar a coches pequeños. Sí, sí, pequeños… ni siquiera normales, sino realmente pequeños.

Para ello, la hemos probado en este coche de la foto, y realmente cabe. 

 ![](/tumblr_files/tumblr_inline_otp269WegS1qfhdaz_540.png)

Así que el tamaño ya no es excusa!

## Para todos los bolsillos

La excusa del precio tampoco me vale, ya que habría que comprar una sola silla en 4 años, porque sirve desde el nacimiento, con su correspondiente reductor, hasta los 4 años de edad.

Os invito a sumar el precio de todas las sillas que necesitaríais  utilizando otros sistemas durante esta etapa en la que los niños crecen y cambian tanto…

## Lo que más nos ha gustado

-Es una silla muy estable, una vez anclada tanto al isofix como al suelo  se queda muy firme y apenas se mueve, incluso forzándola cuesta.

- Me gustan los dispositivos de absorción de golpe lateral porque dan sensación seguridad y un plus de protección.

-El reposa cabezas y el arnés se ajustan simultáneamente, lo que se agradece porque hay que estar regulándolos con mucha frecuencia; cada vez que el peque pega un estirón.

![](/tumblr_files/tumblr_inline_p7n3eeZihL1qfhdaz_540.gif)  
-Que hayan optado por el neopreno para el tejido me parece muy buena idea, ya que es un material de gran resistencia y, contrariamente a lo que se pudiera pensar, el niño no suda demasiado en contacto con él. No es una silla calurosa. Además, lleva ventilación en la parte trasera

-El reductor para recién nacidos es genial, y se puede quitar fácilmente cuando ya no se necesita.

 ![](/tumblr_files/tumblr_inline_otp2kneTPn1qfhdaz_540.png)

- Que los cubre-cinturones lleven en la cara interna una película antideslizante, me parece genial, porque así no se mueven de donde son colocados. 

 ![](/tumblr_files/tumblr_inline_otp25kzRKq1qfhdaz_540.png)

-El sistema de sujeción del niño a la silla en general me parece muy fiable, y el cierre es muy seguro.

![](/tumblr_files/tumblr_inline_p7n3efTjOu1qfhdaz_540.gif)

- No cuesta ningún trabajo sacar la silla del anclaje de la base ni volver a sujetarlo, prácticamente cae por su peso y ahí se queda (nuestra espalda lo agradecerá).

-Es fácilmente desenfundable y lavable

## Caracteríasticas técnicas reseñables

-El reposa cabezas tiene 3 capas de seguridad

-Espuma intelli-fit en el interior para la absorción de impactos laterales.

 ![](/tumblr_files/tumblr_inline_otp2ik4UD41qfhdaz_540.png)

-Lleva barra anti vuelco

 ![](/tumblr_files/tumblr_inline_otp27aE4Er1qfhdaz_540.png)

-Estructura interior de acero reforzado.

-Reclinable en 7 posiciones con una sola mano (aunque no queda demasiado reclinada)

![](/tumblr_files/tumblr_inline_p7n3egtpSe1qfhdaz_540.gif)

Más [información y ficha técnica sobre la silla I-Anchor Advanced de Joie, aquí](http://es.joiebaby.com/product/silla-de-coche-i-anchor-advance/).

## Conclusión: me ha sorprendido

Me ha sorprendido porque esperaba una silla quizá demasiado básica, a juzgar por el modelo anterior, pero este nuevo está muy mejorado y fabricado con materiales de primera y detalles que marcan la diferencia.

Muy interesante para familias con coche pequeño y que no quieran gastarse un dineral en la silla.

La base y la silla, que se compran por separado, cuestan alrededor de 400€.

Pasa la prueba, y con nota!

 ![](/tumblr_files/tumblr_inline_otp2vhj4gi1qfhdaz_540.png)
## Galería de fotos

Os dejo más [fotos de la silla i-Anchor Advanced de Joei aquí](https://www.flickr.com/photos/142190598@N06/albums/72157686785573415).