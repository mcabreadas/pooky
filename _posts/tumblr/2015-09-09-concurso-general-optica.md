---
date: '2015-09-09T11:13:15+02:00'
image: /tumblr_files/tumblr_inline_o39zewxfzT1qfhdaz_540.jpg
layout: post
tags:
- premios
- crianza
- bebes
- familia
title: 'Concurso General Óptica #trastadas'
tumblr_url: https://madrescabreadas.com/post/128698132674/concurso-general-optica
---

## Ganador

Muchas gracias a tod@s por vuestras trastadas, la verdad es que tanto el equipo de General Óptica como yo hemos pasado una semana muy divertida, y la elección ha sido muy difícil porque vuestros peques son unos artistas!! Pero, como sólo puede haber un ganador… LA TRASTADA MÁS DIVERTIDA y merecedora del premio es la contada por Francisco Chatao Novelles, y su fiesta de la espuma jajaja…

“Mi hija me escucho decir que quería preparar un baño de espuma a su madre por el día de las madres y, sin pensarselo 2 veces, empezó a llenar la bañera con un tarro de un litro de gel de baño dentro. Se fue a jugar al patio mientras yo preparaba un bizcocho sin enterarme de nada. Cuando termine, fui a prepararselo antes de que ella llegase y me encontré el baño como una piscina, todo anegado y lleno de espuma hasta el pasillo! Me faltaba la piragua!!!!”

Enhorabuena!! Pásame tu email por privado, por favor, para hacerte llegar el premio

Post original:

Por una vez las trastadas de tus peques tienen premio.

Así que en vez de reñirles, alégrate de sus travesuras porque te darán la oportunidad de ganar.

Si te han pintado el tresillo, la pantalla de la tablet o han hecho un agujero en la puerta no te cabrees. Párate un momento, respira hondo y cuéntanoslo todo.

Estos ejemplos no los he puesto al azar, sino que los he sufrido en casa este verano. Es que las vacaciones dan para mucho. Mira: 

 ![](/tumblr_files/tumblr_inline_o39zewxfzT1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39zexHv811qfhdaz_540.jpg)

La trastada o anécdota más divertida, optimista y graciosa que nos contéis ganará una **tarjeta regalo de General Óptica por valor de 100 €.**

Ya hablábamos en este blog de la importancia de las [revisiones en los niños pequeños](/2015/05/29/vision-ninos.html) para detectar cuanto antes cualquier problema en su visión, ya que lo que puede tener una fácil solución a edades tempranas, luego puede ser más difícil.

Por eso General Óptica ofrece su [servicio AZ de forma gratuita, y pone al alcance de todos una revisión de la vista infantil](http://www.generaloptica.es/es/content/8-servicio-az-de-revision-de-la-vista?utm_source=mamasbloggers&utm_medium=Infantil&utm_campaign=RevisionSep15) con motivo de la vuelta al cole, para que empiecen con buen pie las clases… Mejor dicho, con buenos ojos.

 ![](/tumblr_files/tumblr_inline_o39zeyNum41qfhdaz_540.jpg)

[Quiérele mucho](http://www.generaloptica.es/es/content/165-quierele-mucho?utm_source=mamasbloggers&utm_medium=Infantil&utm_campaign=RevisionSep15), y además de no enfadarte por sus trastadas, llévalo al [punto de General Óptica más cercano para pasarle la revisión. Puedes consultarlos aquí](http://www.generaloptica.es/es/tiendas).

Y si finalmente necesita gafas, las tienes desde 39,95€.

## Para participar en el concurso #trastadas

Gana una **tarjeta regalo de General Óptica por valor de 100 €** contándonos la trastada o anécdota más graciosa de tus peques. Tienes que:

- **Deja un comentario en este post contándonos la trastada** con arte y mucho humor. Déjanos también tu nick de Facebook.

-Dale a **compartir el concurso en Facebook** aquí. Asegúrate de que lo haces de forma pública porque si no, no podremos comprobarlo: [https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063](https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063)

- **Sigue la** [**página de Facebook de General Óptica** aquí](https://www.facebook.com/generaloptica?fref=ts).

También puedes (aunque no es obligatorio para concursar) seguir [mi página de Fecebook aquí](https://www.facebook.com/pages/Blog-Madres-cabreadasAhora-familia-numerosa/286180668083063), y suscribirte al blog para estar al día de las novedades aquí (en la columna lateral)

Tenéis las [bases del concurso aquí](http://www.generaloptica.com/static/concursos_tw/Bases-Legales-Concurso-Trastadas.pdf).

La fecha máxima para publicar tu comentario con la trastada o anécdota será el 15/9/15, y el ganador o ganadora se publicará en este blog en los días sucesivos.

La trastada o anécdota ganadora será la más divertida, y será elegida por el equipo de marketing de General Óptica.

Se valorará la originalidad, el optimismo y el humor.

¿Cuál es la más gorda que te han hecho tus peques?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.