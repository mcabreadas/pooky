---
date: '2016-03-01T10:00:35+01:00'
image: /tumblr_files/tumblr_inline_o37unzti0Z1qfhdaz_540.jpg
layout: post
tags:
- trucos
- regalos
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: Libros personalizados para niños Superprota
tumblr_url: https://madrescabreadas.com/post/140264797383/libros-personalizados-ninos
---

A mi hijo de 8 años le encanta leer. A veces le cuesta ponerse, pero con la motivación adecuada, si coge un libro y le interesa, no parará hasta que lo termine. Se nota que realmente disfruta.

Creo que los padres y madres tenemos un papel crucial en fomentarles el interés por la lectura, ya lo he comentado muchas veces, por eso cuando descubrí que existen libros personalizables con el nombre del protagonista que tú quieras no me pude resistir a encargar uno para mi peque.

Yo lo hice en la página de [Superprota](http://www.superprota.com/), que es muy sencilla de navegar y no te llevará demasiado tiempo.

Sólo tienes que escribir el nombre de tu hijo o la persona que quieras poner de protagonista,  y redactar un prólogo si te apetece. Yo aproveché y le puse uno muy sentimental porque soy un poco cursi, pero puedes hacerlo en clave de humor, o de misterio…

 ![](/tumblr_files/tumblr_inline_o37unzti0Z1qfhdaz_540.jpg)

Imagínate la cara del pequeño lector cuando abra el libro y vea que el prólogo va dirigido especialmente a él, y cuando empiece a leer la historia, ¡que el protagonista es él!

Nuestra historia se titula “[El Bastón Mágico](http://www.boolino.es/es/libros-cuentos/el-baston-magico/)”, y le enganchó desde el principio, ya que se vio a él mismo volviendo del cole y entrando en la biblioteca a buscar un libro que resultó ser mágico y acabó dentro de él.

 ![](/tumblr_files/tumblr_inline_o37unfaQZZ1qfhdaz_540.jpg)

Es curioso cuando me contaba el argumento porque lo hacía en primera persona, como si realmente todo le hubiera pasado a él mismo de verdad.

Creo que es un bonito detalle, y un regalo que puede llegar a sentirse muy especial al libro que lo reciba.

Lo veo ideal para regalar en Comuniones, ya que a veces no sabemos qué regalar porque realmente tienen de todo, y con un libro personalizado nos podemos distinguir con algo realmente original.

¿Conocías los libros personalizados?