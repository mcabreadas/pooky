---
date: '2016-02-10T13:14:49+01:00'
image: /tumblr_files/tumblr_inline_o39yo3bkpl1qfhdaz_540.jpg
layout: post
tags:
- eventos
- moda
- familia
title: La moda de invierno en el Showroom de FIMI
tumblr_url: https://madrescabreadas.com/post/139047146419/moda-infantil-fimi-showroom
---

Tal y como os prometí en el post donde os contaba al detalle el [desfile FIMI Kids Fashion Week](/2016/01/27/fimi-moda-infantil.html), os voy a contar lo que más me gustó del Showroom de la Feria Internacional de Moda Infantil y Juvenil Madrid otoño-invierno 2016.

En lo que más me fijé fue en las marcas que tienen línea especial para teens, que va desde los 8-10 años hasta los, 14, los 16, y en algunos casos incluso llegan hasta los 18 años.

Esta opción para mí es muy interesante porque nos permite vestir a los niños y niñas con un toque de “mayor” como a ellos les gusta llegadas ciertas edades, pero sin que parezcan “mini adultos”, conservando cierta frescura y un punto juvenil.

Visité muchos stands, pero os voy a resumir los que más me llamaron la atención:

## Oh Soleil

Hay firmas, como [Oh Soleil que, como ya os contaba en este otro post, son específicas para teens y adolescentes](/2016/01/09/ropa-adolescentes-ohsoleil.html), incluso llega hasta los 18 años.

Aunque también tienen una línea desde los 2 hasta los 12 años.

 ![](/tumblr_files/tumblr_inline_o39yo3bkpl1qfhdaz_540.jpg)

Esta colección con con guiños retro que es a la vez urbana y cosmopolita, y sobre todo invita a elegir, mezclar y volver a combinar para que creen su propio estilo.

 ![](/tumblr_files/tumblr_inline_o39yo4Y6kD1qfhdaz_540.jpg)

El boho chic que caracteriza a esta marca se construye para el invierno que viene con figuras _oversize_ llenas de matices a partir de la combinación de pantalones sastre holgados, faldas largas cruzadas de algodón, camisas _oversize_ y chaquetas de punto peluche.  

## Bóboli

Me encantó el stand de [Bóboli](http://www.boboli.es/), lleno de vida y colorido.

 ![](/tumblr_files/tumblr_inline_o39yo4zqkU1qfhdaz_540.jpg)\>

Esta marca también tiene ropa hasta los 16 años, y de estilos muy variados, creo que muy cómoda ponible. A los niños les encanta.

 ![](/tumblr_files/tumblr_inline_o39yo5RpKl1qfhdaz_540.jpg)

## iDO

Ya os hablé de esta marca italiana en este otro post sobre la pasarela [Madrid Petit Walking, de la revista Petit Style](/2015/11/04/pasarela-moda-madrid.html), y me ha seguido encantando en su colección de invierno para la temporada que viene.

 ![](/tumblr_files/tumblr_inline_o39yo6YGtC1qfhdaz_540.jpg)

Esa mezcla de romanticismo y espontaneidad me ha encantado para mis niños.

 ![](/tumblr_files/tumblr_inline_o39yo6LZgV1qfhdaz_540.jpg)

## Conguitos
 ![](/tumblr_files/tumblr_inline_o39yo6WFGO1qfhdaz_540.jpg)

La marca de zapatos infantiles [Conguitos](http://www.conguitos.es/es/) presentó looks completos con su calzado.

 ![](/tumblr_files/tumblr_inline_o39yo7himf1qfhdaz_540.jpg)

Me encantaron para mi niña estas botas australianas súper calentitas (bueno, ya sabéis que le encanta el rosa, así que no me pude resistir a ellas)

 ![](/tumblr_files/tumblr_inline_o39yo7ZbCq1qfhdaz_540.jpg)

## J.V. José Varón y Cocote

Dos firmas andaluzas, la primera, más tradicional, pero reinventándose continuamente, y la segunda más ligera.

 ![](/tumblr_files/tumblr_inline_o39yo8SC8W1qfhdaz_540.jpg)

Espectacular el pequeño aviador!

 ![](/tumblr_files/tumblr_inline_o39yo8yxZB1qfhdaz_540.jpg)

Me quedo con los abrigos de [J.V. José Varón](http://www.jvjosevaron.com/), de paño, piel vuelta, lana… todos de excelente calidad y un tacto dulce ideal para que los niños los lleven cómodos.

 ![](/tumblr_files/tumblr_inline_o39yo8nw5K1qfhdaz_540.jpg)

De [Cocote](http://www.cocoteturopita.com/) elijo este mono súper original con una combinación de rojo y azul preciosa.

 ![](/tumblr_files/tumblr_inline_o39yo9iUc51qfhdaz_540.jpg)p\>  

## La Ormiga
 ![](/tumblr_files/tumblr_inline_o39yo9Vl3d1qfhdaz_540.jpg)

[La Ormiga](http://laormiga.com/) es otra de las marcas que llega hasta los 16 años, y que me parece ideal para vestir a las niñas grandes.

Aunque también tiene tallas más pequeñas, como estos jerseys de punto azules con lunares blancos, que me encantaron.

 ![](/tumblr_files/tumblr_inline_o39yo9AE671qfhdaz_540.jpg)

Y para señoritas, mis favoritos son estos dos modelos, con un aire romántico irresistible.

 ![](/tumblr_files/tumblr_inline_o39yoaAvBn1qfhdaz_540.jpg)

Ya sabéis que los ponchos son mi debilidad.

 ![](/tumblr_files/tumblr_inline_o39yoaeok51qfhdaz_540.jpg)

## JoyKids
 ![](/tumblr_files/tumblr_inline_o39yobsaIZ1qfhdaz_540.jpg)

Me encontré con esta sorpresa mientras paseaba por los pasillos de FIMI. [JoyKids](http://www.joykids.es/), una marca de crianza respetuosa con cantidad de productos novedosos y originales para cuidar de nuestros tesoros.

Mis favoritos, este calzado específico para cuando los bebés empiezan a caminar.

 ![](/tumblr_files/tumblr_inline_o39yobckSB1qfhdaz_540.jpg)

Rigurosamente estudiado el movimiento del pie, Attippas ha creado estas botitas con un diseño cuya forma y anchura respetan los movimientos que hace un niño cuando está aprendiendo a caminar.

La goma de la suela y la puntera evita resbalones, haciendo que el bebé se sienta más seguro a la hora de ponerse en pie y dar sus primeros pasos. 

Me encantó también este fular fácil de poner con anillas en los laterales de Simplygood .

 ![](/tumblr_files/tumblr_inline_o39yoc2ts41qfhdaz_540.jpg)

Y este cepillo de dientes especial para bebés hecho entero de goma flexible para que lo puedan manejar ellos solos sin peligro de hacerse daño o clavárselo.

 ![](/tumblr_files/tumblr_inline_o39yocFzzH1qfhdaz_540.jpg)

## Sun City

Menos mal que no vino me pequeño de 3 años, porque si no, no hubiéramos salido del stand de [Sun City](http://suncity.pt/) sin llevarse puestas esta camiseta y esta sudadera de Patrulla canina, serie canadiense que está haciendo furor entre los infantes.

 ![](/tumblr_files/tumblr_inline_o39yodQkdz1qfhdaz_540.jpg)

## Babela

Vuelven los cuadros tradicionales con diseños actuales, y los pudimos ver en el stand de [Babela](http://babela.es/).

 ![](/tumblr_files/tumblr_inline_o39yodtwyy1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39yodcl6A1qfhdaz_540.jpg)

Ya sabéis que me encanta coordinar a los hermanos, y con estos conjuntos están para comérselos.

 ![](/tumblr_files/tumblr_inline_o39yoe55a71qfhdaz_540.jpg)

## El refrigerio

Y después de tantos stands, ¡menos mal que hay marcas majas que te invitan a un refrigerio!

Gracias, [Lion of Porches](https://www.lionofporches.pt/es/?op=intro&to=http%3A%2F%2Fwww.lionofporches.pt%2Fes%2Flion_of_porches.2%2Fconcepto.19%2Fthe_original_british_style.a1.html) por vuestro amable trato.

 ![](/tumblr_files/tumblr_inline_o39yofTANC1qfhdaz_540.jpg)

Después de ver tantas cosas bonitas, llegué a casa con una sonrisa.

 ![](/tumblr_files/tumblr_inline_o39yofLFMF1qfhdaz_540.jpg)

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.