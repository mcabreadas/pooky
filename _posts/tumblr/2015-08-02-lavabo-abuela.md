---
date: '2015-08-02T16:01:27+02:00'
image: /tumblr_files/tumblr_inline_p7woo4feGu1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- cosasmias
- familia
title: El lavabo de mi abuela
tumblr_url: https://madrescabreadas.com/post/125673736724/lavabo-abuela
---

![image](/tumblr_files/tumblr_inline_p7woo4feGu1qfhdaz_540.jpg)

Todavía, si abro los cajones, huelen a polvos de arroz.  
Es curioso cómo los sentidos activan los recuerdos que tenemos dormidos en el cerebro.

Cuando era pequeña nos gustaba entrar en su cuarto y jugar a empolvarnos la cara.  
Siempre me ha cautivado el lavabo de mi abuela. Me hace pensar qué distinta debió de ser la vida para ella…para ellas…  
Las mujeres de aquella época sí que eran luchadoras.  
Sólo imaginar cómo sería mi vida sin lavadora, me pone el bello de punta (y es sólo un ejemplo).  
Pienso en cómo logró criar a 4 hijos sin demasiados medios y sin las comodidades que tenemos ahora, y realmente me doy cuenta de que fue una heroína.  
  
Me siento por un momento frente al espejo, y repaso en mi mente la que seguramente sería su rutina diaria de aseo.  
Primero calentaría agua en el fogón, la vertería en la vasija de porcelana y después a la palangana.  
Humedecería una toalla de hilo y frotaría un poco de jabón de rosas en ella para asearse.  
La imagino coqueta, arreglándose para mi abuelo y mirándose de reojo en el espejo.  
Luego atusaría su pelo moreno, ondulado y brillante, que no vería las canas hasta la seneptud (qué suerte haberlo heredado), para dar paso a la polvera que dejaría la piel de su rostro aún más blanca.  
Fue una mujer sonriente y bella, menuda, discreta y de maneras refinadas, a pesar de ser de familia humilde. Fue costurera en su juventud.  
  
Pero lo que más admiro de ella es la fortaleza que tuvo para criar y amamantar a 4 hijos en aquellos tiempos. Lo pienso y me siento ridícula por haberme quejado alguna vez…  
Y a aún así, no pasaba un día sin que se pusiera sus polvos de arroz.