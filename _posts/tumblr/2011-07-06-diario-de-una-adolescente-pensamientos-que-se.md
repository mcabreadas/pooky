---
layout: post
title: Pensamientos que se escapan
date: 2011-07-06T18:54:00+02:00
image: /images/uploads/dibujo-luna-pensamientos-que-se-escapan.jpeg
author: maria
tags:
  - cosasmias
tumblr_url: https://madrescabreadas.com/post/7306508819/diario-de-una-adolescente-pensamientos-que-se
---
Te abro la puerta a mi yo adolescente con este escrito que mereció el primer premio en el concurso de literatura anual modalidad prosa de mi Instituto. 17 años tenía... 

> Creo que estoy empezando a enamorarme. ¿O lo he estado siempre? De niña soñaba, soñaba quimeras que se desvanecían al abrir los ojos, inventaba historias cuyo final no importaba. Tenía una gran idea de la vida. Creía que la vida era amor; Pensaba que la gente nacía por amor, crecía por amor, se mantenía viva por amor e incluso  después de la muerte, continuaba amando. No importaba a quién, a qué, ni por qué, sino sólo el sentimiento que mantenía a las personas unidas.
>
> He tenido siempre una idea platónica del amor. Quizá de esa idea sé más que nadie, podría defenderla hasta la muerte, podría encarnarla en personas, materielizarla… Esas personas a las que creo he amado… quizá no haya sido para ellas mi amor, quizá haya sido para ese sueño, para esa leve espuma que, apenas vas a tocar, desaparece.
>
> Mi amor es algo más que un deseo, mi amor es algo más que una pasión, mi amor no es un amor normal, si no, ¿Cómo explicar estar enamorada sin saber de quién?
>
> Quiero, siento, río, lloro, me sorprendo… Gracias a ello me mantengo viva. Viva, pero con miedo. Un miedo que me aterra, que me hace temblar. Miedo a no encontrarlo nunca, miedo a seguir imaginando, miedo a mi romanticismo, miedo a descubrir que mi ideal se ha desvanecido, miedo a que el amor no exista, a que sea tan sólo una rutina. Miedo a que un ideal de juventud se pierda con la edad.
>
> ¿Y si solamente existen los ideales cuando se es joven? Entonces… ¿No sirven de nada? ¿Se pierden cuando por fin puedes llevarlos a cabo? Me resisto a creerlo, pero la duda me aterra. Quizá algún día la vida me venza, me quite mi ilusión, me dije tan destrizada que ya no crea en nada… ¿Por qué iba a luchar entonces?
>
> Pero… ¿Por qué me siento derrotada? ¿Por qué me estoy rindiendo? ¿No era invencible? ¿No defendería mi ideal hasta la muerte? No puedo permitir que estas palabras se queden impresas en unpapel, tienen que salir, tienen que demostrar que se puede creer en el amor, que hay cosas en esta sociedad descarriada por las que aún merece la pena luchar. En este mundo absurdo, ¡Tiene que quedar algo puro, algo incorrupto, algo sagrado, inmanchable!; Algo por lo que morir… Algo.
>
> Ese algo, esa idea, esa persona, esa actitud, esa pretensión, esa virtud, ese privilegio, esa fortuna, esa poesía… Ese es mi amor, eso es por lo que yo lucho.

*Dibujo de Jose Alberto Saez*