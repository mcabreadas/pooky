---
date: '2016-06-19T19:00:26+02:00'
image: /tumblr_files/tumblr_inline_o90it5vi4x1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- bebe
title: Operación pañal fuera ¿misión imposible?
tumblr_url: https://madrescabreadas.com/post/146162188044/cuandopanalbebe
---

Ya llega el buen tiempo y ya nos ha dado a las madres por buscar trucos sobre cómo quitar el pañal a los niños, sobre todo si empiezan el cole en septiembre, y es un requisito exigido, como fue mi caso el año pasado.

 ![](/tumblr_files/tumblr_inline_o90it5vi4x1qfhdaz_540.jpg)

Te voy a dar unos consejos basados en la experiencia que otorga haberlo hecho con mis tres hijos, por si te ayudo en algo.

## El momento lo marca el niño

Elige también una época en la que estés tranquila, evita momentos de especial estrés en el trabajo, o en que la situación familiar no sea estable.

Tener esto claro nos ahorrará sudor y lágrimas. No se pueden forzar las cosas porque son bebés todavía, no lo olvidemos, y se enfrentan a la primera cosa en su vida que les coarta su libertad, y que les obliga a interrumpir lo que están haciendo para hacer otra cosa que quizá no les parezca tan interesante. (Piénsalo un momento…)

**Atenta a las señales** porque no se puede establecer una edad idónea, ni una época del año, aunque es cierto que en verano es más práctico porque al no darles frío pueden controlar mejor los esfínteres, y además la ropa es más fácil de quitar y poner.

Una señal que podemos tener en cuenta para empezar es cuando son conscientes del momento en que se hacen pipí en el pañal. A veces lo dicen porque, además, se sienten incómodos mojados.

Ayúdale a identificar ese momento:

“¿Estás haciendo pipí? Muy bien! ¿La próxima vez me vas a avisar?”

## Búscate compinches

Abuelas (ellas saben mucho), pareja, hermanos…

Si hay hermanos mayores o si van a la guardería es más fácil porque imitarán a los demás niños.

## Etapa de mentalización

Tómate tu tiempo antes de iniciar la operación pañal, y empieza a actuar ya como si no lo llevara.

Una buena idea es **ir juntos a comprar un orinal** y darle protagonismo en la elección (recomiendo uno sencillo, no hace falta que tenga luces y música, ni que se oiga una ovación cuando caiga el chorrito). 

 ![](/tumblr_files/tumblr_inline_o90j1aNJ3Y1qfhdaz_540.jpg)

Muy importante que tenga goma anti deslizante en la base para que no se le mueva cuando se vaya a sentar, y el peque se sienta seguro.

Le tiene que gustar y sentirse cómodo.

Una buena opción también son las [bragas pañal](http://www.amazon.es/gp/product/B01BVADWSO/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B01BVADWSO&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B01BVADWSO), que se suben y se bajan, y que las hay de dibujos muy chulos. Preséntaselas como braguitas o calzoncillos de mayor.

## Cuándo quitar el pañal

**Cuando te empieza a avisar** de que se ha hecho o está haciendo pipí, para mí, es el momento clave para empezar.

Aprovecha su **curiosidad natural** Los niños imitan, y lo lógico y normal es que sientan curiosidad cuando te ven en el váter, y pregunten, o si no, tú le puedes explicar que los mayores hacen pipí ahí. Aprovecha esa curiosidad innata Cuando te empiecen a avisar puedes probar a dejarlo sin pañal.

Le puedes poner un **pantalón de punto de algodón con goma en la cintura** , de esos que se bajan y suben fácilmente y, de momento, sin ropa interior. Explícale que se mojará si no te avisa, y estáte preparada, y **mentaliza al resto de la familia** , porque los primeros días se va a hacer por toda la casa. 

Lo puedes sentar cada hora en su orinal, y no te preocupes, que alguna ve acertará, entonces la ovación se la tenéis que hacer vosotros.

Enséñale su pipí, porque probablemente nunca lo haya visto, y dile que siempre lo tiene que hacer ahí.

No obstante a veces dan un paso hacia adelante y dos hacia atrás. Es normal. Si ves que pasan días y no hay manera, no fuerces, ponle su pañal de forma amorosa, corre un tupido velo, y déjalo para más adelante.

 Recuerda:

"siempre positivo, nunca negativo”

## Fase sin pañal

Si ya estamos en la fase sin pañal, la mayor dificultad la vas a tener tú, no tu hijo. Verás charquitos en los lugares más insospechados cada dos por tres, y no te atreverás a salir a la calle con él en un mes.

**No le transmitas tu desasosiego** , por favor. Es una carrera de fondo, no de velocidad.

Ten preparado siempre el cubo de la fregona con agua, lejía y amoniaco (lejos de su alcance, por supuesto), de esta manera te ahorrarás tener que cambiar el agua cada vez que hace pipí en el suelo porque no olerá demasiado y será más práctico.

Ve a un mercadillo y hazte con **docenas de braguitas o calzoncillos** y muchos pantalones de punto para no agobiarte por no dar abasto a poner lavadoras.

Cálzalo con unas **sandalias cangrejeras** , de esas de plástico para la playa, para que sea fácil lavarlas y secarlas. Así no estropeara sus zapatos.

## Salir a la calle

Consigue un [orinal portátil](http://www.amazon.es/gp/product/B0016L0MMS/ref=as_li_qf_sp_asin_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B0016L0MMS&linkCode=as2&tag=madrescabread-21) ![](https://ir-es.amazon-adsystem.com/e/ir?t=madrescabread-21&l=as2&o=30&a=B0016L0MMS) para cuando salgáis a la calle, y lleva siempre una **bolsa de plástico** en el bolso, **varias mudas y toallitas**.

Si vais en coche es mejor que los primeros días le pongas la braga pañal en aras de la serenidad familiar, durante el trayecto.

 ![](/tumblr_files/tumblr_inline_o90jsuldYu1qfhdaz_540.jpg)
## Sé comprensiva

Si está jugando le costará sentarse en el orinal, aunque tenga ganas. Ten en cuenta que es la primera vez en su vida que algo le coarta su libertad. Por eso suelen aguantarse las ganas de pipí tanto cuando están jugando a algo divertido, y cuando quieren ponerle remedio ya no llegan a tiempo al orinal.

 ![](/tumblr_files/tumblr_inline_o90jru41U11qfhdaz_540.jpg)

Sé comprensiva y ármate de paciencia, **nunca lo presiones ni le riñas** , y menos en público.

Tampoco cuentes a los demás delante de él las veces que se ha hecho fuera, y **no lo compares con otros niños.**

## Si te agobias, para un tiempo

Si te empiezas a agobiar ponle puntualmente la braga pañal y respira.

 ![](/tumblr_files/tumblr_inline_o90jjpWglh1qfhdaz_540.jpg)

Es importante que estés tranquila.

Retómalo cuando te sientas con fuerzas, y relajada.

## Por las noches

Por la noche, mi consejo es que no le intentes quitar el pañal nocturno a la vez que el diurno, salvo que te lo pida y veas que está preparado.

Ya llegará el momento en que te lo demande.

 ![](/tumblr_files/tumblr_inline_o90jmuBg8h1qfhdaz_540.jpg)

Eso sí, en cuanto se levante por la mañana, al orinal a hacer el “pipí de la mañana”.

Y si alguna vez el colchón amanece mojado, no te preocupes, [aquí](/2015/10/14/limpiarpipicolchon.html) te cuento cómo dejarlo como nuevo, y sin olores, por muy grande que haya sido el escape.

En cuanto a la caca, merece un post aparte, pero puedes empezar aplicando estos mismos consejos que para el pipí.

Espero haberte ayudado con mi experiencia.

Por favor, si tienes algún consejo más, no dudes en dejármelo en los comentarios, seguro que será de gran ayuda a muchas mamás.