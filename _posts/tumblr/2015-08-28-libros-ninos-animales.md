---
date: '2015-08-28T08:51:55+02:00'
image: /tumblr_files/tumblr_inline_o39zgu2l9g1qfhdaz_540.jpg
layout: post
tags:
- trucos
- libros
- planninos
- recomendaciones
- familia
title: Libros infantiles National Geographic Kids La selva y El delfín
tumblr_url: https://madrescabreadas.com/post/127773265839/libros-ninos-animales
---

Si eres fan de la revista National Geographic, amante de la naturaleza o te apasionan los animales, entonces seguramente querrás introducir a tus hijos en este fascinante mundo. 

Te voy a dar una idea que te va a fascinar… os va a fascinar a toda la familia. 

[National Geographic Kids](http://kids.nationalgeographic.com/) ha sacado unos libros en español especiales para los niños enfocados desde el punto de vista científico propio de la publicación, y muy atractivos para ellos.

Las ilustraciones son una pasada, y el formato es la propia de un libro infantil, pero el rigor en la información no falta, lo que ha hecho que mi hijo los devore.

[Los temas están divididos por edades y podéis verlos en este enlace](http://www.nationalgeographickids.es/?utm_source=nl_boolino&utm_medium=mailing). 

OS pongo unos ejemplos:

-De 3 a 6 años, tenéis la **colección Descubrir el Mundo** , con los libros [La Selva](http://www.boolino.es/es/libros-cuentos/descubrir-el-mundo-la-selva/) y [El delfín](http://www.boolino.es/es/libros-cuentos/descubrir-el-mundo-el-delfin/), que son los que hemos leído ahora en casa. 

 ![](/tumblr_files/tumblr_inline_o39zgu2l9g1qfhdaz_540.jpg)

Sobre todo, mi mediano, muestra un gran interés por el comportamiento de los animales, y le gusta memorizar datos curiosos sobre ellos.

No sé vuestros hijos, pero a los míos les encantan los delfines, y se han pasado el verano jugando a saltar en la piscina como ellos.

 ![](/tumblr_files/tumblr_inline_o39zgu0xPL1qfhdaz_540.jpg)

-De 4 a 8 años tenéis ** “Mi primer gran libro”** ideales para aprender sobre dinosaurios, el espacio, el océano… y un poco de todo.

-De 4 a 15 años está la colección  **“Temas fascinantes”** para aprender sobre el cuerpo humano, animales, bichos… donde las fotografías y los datos curiosos son los protagonistas.

-También tenéis para los más pequeños **libros de adhesivos** para empezar a despertar su curiosidad sobre el mundo animal.

 ![](/tumblr_files/tumblr_inline_o39zgvyUr61qfhdaz_540.jpg)

¿Nos vamos de aventura?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.