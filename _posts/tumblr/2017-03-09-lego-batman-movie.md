---
date: '2017-03-09T12:06:00+01:00'
image: /tumblr_files/tumblr_inline_omhxbb07Bk1qfhdaz_540.jpg
layout: post
tags:
- crianza
- juguetes
- planninos
- familia
title: Montamos el Batmovil de la Lego Batman movie
tumblr_url: https://madrescabreadas.com/post/158186660649/lego-batman-movie
---

¿Habéis ido ya a ver Batman, la Lego Película? Es obligatorio ir en familia a verla porque os vais a reír tanto los niños como los mayores; a veces en las mismas escenas, y otras por cosas distintas, pero que os vais a reir os lo garantizo.

 ![](/tumblr_files/tumblr_inline_omhxbb07Bk1qfhdaz_540.jpg)

La película, de [Warner Bros Pictures](https://www.warnerbros.es/) dirigida por Phil Lord y Chris Miller y producida por Dan Lin y Roy Lee, es muy ocurrente, tiene alusiones a otras películas, y trata con humor tanto la personalidad de Batman como la relación amor/odio con el Joker.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/pfgH2mOfO60" frameborder="0" allowfullscreen></iframe>

Se centra en el personaje de Batman y en el universo de los [superhéroes de DC Cómics](/2016/11/27/munecas-super-hero-girl.html), quien intentará salvar la ciudad de Gotham del Joker. Pero no podrá hacerlo solo, y tendrá que aprender a trabajar en equipo, cosa a la que no está acostumbrado, ya que, como todos sabemos 

> “Batman trabaja solo”

 ![](/tumblr_files/tumblr_inline_omhxaiBRjV1qfhdaz_540.jpg)

La animación de las figuras de Lego es espectacular, como la anterior [Lego película](/2014/01/26/sorteo-set-lego-pel%C3%ADcula.html), ya que los movimientos están muy conseguidos.

Con motivo del estreno de la peli, LEGO recrea con 10 sets algunas de las principales escenas de ‘LEGO Batman Movie’ dando vida a las peripecias del Batman de LEGO más divertido para niños de 6 a 14 años.

Además, también tienes una [colección de minifiguras de edición limitada](http://amzn.to/2lDwIuE) de los personajes de la película entre los que podrás encontrar a Batman en algunas de sus escenas cotidianas más divertidas.

 ![](/tumblr_files/tumblr_inline_omhxaq1E1Z1qfhdaz_540.jpg)

Nosotros hemos montado en casa el Batmóvil, y hemos disfrutado un montón haciéndolo.

 ![](/tumblr_files/tumblr_inline_omhxazIHC11qfhdaz_540.jpg)

He de decir que mi mediano es un crack de los Legos, y monta estructuras que yo sería incapaz. Pero es que el pequeño le sigue, y a pesar de que tiene 4 años sólo, se atreve a intentarlo, eso sí, con ayuda de papi.

 ![](/tumblr_files/tumblr_inline_omhxbfC43O1qfhdaz_540.jpg)

La verdad es que lo hicieron en tres ratitos, pero el último empujón se lo dio papá, casi de madrugada, porque no podía parar de montarlo hasta que finalmente lo terminó para sorpresa de todos cuando se levantaron al día siguiente.

 ![](/tumblr_files/tumblr_inline_omhxbixhLn1qfhdaz_540.jpg)

Además del Batmóvil, que ya ves lo chulo que es, tienes otros sets de la película: 

[Globos de fuga de The Joker](http://amzn.to/2lDFd95)

[Ataque gélido de Mr. Freeze](http://amzn.to/2lDnTRN)

[Moto felina de Catwoman](http://amzn.to/2lDjuy6)

[Coche misterioso de The Riddler](http://amzn.to/2mlhMjL)

[Ataque cenagoso de Clayface](http://amzn.to/2ml8Z1b)

[Batmóvil](http://amzn.to/2lDr23M)

[Coche modificado de The Joker](http://amzn.to/2ml40O6)

[Reptil todoterreno de Killer Croc](http://amzn.to/2lDDDEf)

[Criatura](http://amzn.to/2mletZK)

[Intrusos en la Batcueva](http://amzn.to/2mldOYo)

¿A que es difícil decidirse?