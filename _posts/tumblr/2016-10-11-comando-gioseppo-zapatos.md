---
date: '2016-10-11T10:17:23+02:00'
image: /tumblr_files/tumblr_oeu6qsbq5Q1qgfaqto1_400.gif
layout: post
tags:
- ad
- colaboraciones
- moda
title: comando gioseppo zapatos
tumblr_url: https://madrescabreadas.com/post/151651489749/comando-gioseppo-zapatos
---

![](/tumblr_files/tumblr_oeu6qsbq5Q1qgfaqto1_400.gif)  

## El comando Gioseppo vuelve a salvar el planeta

Mamás, papás, tengo que avisaros de algo importante:

Se está fraguando una rebelión en las bases: los niños se han plantado contra el cambio climático y han decidido ponernos firmes a los adultos que no respetemos las normas que, curiosamente, nosotros mismos hemos puesto para evitar este temido fenómeno.

Han hecho su propio [reglamento](file:///Users/maria/Downloads/Reglamento_Comando-Gioseppo.pdf), y están llamando a filas a niños y niñas comprometidos con el planeta para su nueva misión: PLANET FAIR PLAY.

 ![](/tumblr_files/tumblr_inline_oetswtQb8F1qfhdaz_540.png)

Su propósito es hacernos cumplir las normas de ahorro de energía, agua y reciclaje a toque de silbato y hasta se atreven a sacarnos tarjeta si incumplimos alguna.

Aviso: !Son implacables, y no perdonan ni un despiste!

Se organizan en grupos, y se hacen llamar “[Comando Gioseppo](http://www.gioseppokids.com/)”, y cada miembro tiene un nombre en clave, unas tarjetas para señalar infracciones, un silbato al cuello y el reglamento en el bolsillo. 

Este fin de semana me topé con un comando cuando paseaba tranquilamente por un centro comercial, y mi calma se vio truncada de repente cuando me acerqué a una papelera y me equivoqué al tirar una bolsa de plástico en cubo que no era.

 ![](/tumblr_files/tumblr_inline_oetsxfPXES1qfhdaz_540.png)

Entonces, me interceptaron y me sacaron la tarjeta verde del reciclaje. Me puse tan nerviosa por los pitidos, que volví a equivocarme, pero esta vez con el papel.

 ![](/tumblr_files/tumblr_inline_oetsy985lz1qfhdaz_540.png)

Eran tres, y eran implacables. La mayor se hacía llamar “En pie con el puño en alto”, el mediano, “Oso sentado”, y el más pequeño, pero no por eso menos bravo, “Leopardo salvaje”.

Prometí no volver a incumplir las normas del Reglamento Gioseppo, y me explicaron muchas más cosas que yo podía hacer para proteger el planeta, como ahorrar agua y energía con pequeños gestos cotidIanos que no cuestan nada y, sin embargo, si los hacemos todos, se consigue mucho, como por ejemplo cambiar las bombillas por otras de bajo consumo, desenchufar los cargadores del móvil cuando no los estamos usando, cerrar el grifo mientras nos cepillamos los dientes o afeitamos, no usar el water como si fuera un cubo de basura… y algo muy divertido, hacer manualidades con material reciclable, como estas cajitas para regalitos hechas con el carrtón del rollo del papel higiénico.

 ![](/tumblr_files/tumblr_inline_oetrmdfVFX1qfhdaz_540.png)
## Un juego para divertirnos en familia
Juegos como éste tan divertido son perfectos para que los niños, no sólo tomen conciencia de la importancia de proteger nuestro entorno, sino que se sientan responsables de ello, ya que ellos, si te fijas, son mucho más rigurosos a la hora de cumplir y hacer cumplir algo en lo que se sienten implicados.  

Si quieres jugar con tus hijos puedes descargar el “Reglamento Gioseppo”[aquí](file:///Users/maria/Downloads/Reglamento_Comando-Gioseppo.pdf), así como las tarjetas para señalar infracciones [aquí](file:///Users/maria/Downloads/Tarjetas_Comando-Gioseppo.pdf), son de tres colores:

- Tarjeta verde: por no reciclar  

- Tarjeta naranja: por despilfarrar energía  

- Tarjeta azul: por derrochar agua  

## La nueva colección de zapatos de Gioseppo Kids

Gioseppo es una marca de zapatos comprometida con el medio ambiente, y ha lanzado esta campaña para concienciar y responsabilizar, no sólo a los niños, sino a los adultos a través de nuestros hijos.

Su nueva colección de niños para este otoño-invierno está llena de botas, zapatos y zapatillas con diversidad de estilos, que  recogen las últimas tendencias y se adaptan a las necesidades de calidad y confort que los peques de la casa necesitan.

Nosotros hemos elegido [sneakers](http://gioseppo.com/es-es/sneakers-de-ni-o-azul-marino-de-piel-con-detalles-en-marron-harrison) para el mediano (“Oso sentado”), [sneakers abotinados](http://gioseppo.com/es-es/sneaker-de-ni-o-en-marino-richie) para el pequeño (“Leopardo salvaje”), y [balarinas](http://gioseppo.com/es-es/svetlana-plomo-girls-ballerinas-moccas) para la mayor (“En pie con el puño en alto”).

 ![](/tumblr_files/tumblr_inline_oetrywGk9F1qfhdaz_540.png)

Tienen línea para bebés, para niña y para niño.

Me ha encantado la terminación de los zapatos de bebé porque la piel de los zapatos es blandita y van forrados por dentro. 

 ![](/tumblr_files/tumblr_inline_oets2kNARk1qfhdaz_540.png)

Además, se las puede poner el sólo porque llevan una cremallera para hacerlo más fácil y fomentar su autonomía.

 ![](/tumblr_files/tumblr_inline_oets1zByBu1qfhdaz_540.png)

Y menos mal, porque después de ejercer de “Comando Gioseppo”, nos fuimos de cumpleaños a un parque de bolas donde contiuamente tenían que estar descalzándose y volviéndose a calzar porque algunas de las actividades sólo se podían hacer en calcetines.

 ![](/tumblr_files/tumblr_inline_oetszj7Uki1qfhdaz_540.png)

Menos mal que, mientras, los zapatos quedaban a buen recaudo, porque estaban tan contentos por estrenarlos que no querían soltarlos.

 ![](/tumblr_files/tumblr_inline_oetseoqcco1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_oetsdpi7A61qfhdaz_540.png)

Y así despedimos un día lleno de emociones, y muy satisfechos de haber contribuído a la sostenibilidad del planeta.

 ![](/tumblr_files/tumblr_inline_oetsgpMGuW1qfhdaz_540.png)

¿Te unes al Comando Gioseppo?