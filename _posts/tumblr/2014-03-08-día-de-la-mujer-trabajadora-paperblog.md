---
layout: post
title: Día de la mujer trabajadora - Paperblog
date: 2014-03-08T08:41:00+01:00
image: /tumblr_files/tumblr_n23xl0O7qs1qgfaqto1_400.jpg
author: .
tags:
  - revistadeprensa
  - conciliacion
tumblr_url: https://madrescabreadas.com/post/78929147613/día-de-la-mujer-trabajadora-paperblog
---
[Día de la mujer trabajadora - Paperblog](http://es.paperblog.com/dia-de-la-mujer-trabajadora-2469540/)  

La Revista en Femenino recoge [mi artículo sobre el día de la mujer trabajadora](https://madrescabreadas.com/2014/03/07/día-de-la-mujer-trabajadora-valga-la-redundancia/) y le añade una ilustración que me encanta. 

Muchas gracias.

Te gusta?