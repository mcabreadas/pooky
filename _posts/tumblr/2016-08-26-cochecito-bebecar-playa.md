---
date: '2016-08-26T10:00:32+02:00'
image: /tumblr_files/tumblr_inline_ocmo3hMBH21qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- bebes
title: 'Prueba extrema Bébecar II: playa, calor y arena'
tumblr_url: https://madrescabreadas.com/post/149500588489/cochecito-bebecar-playa
---

Quien conoce nuestra tierra no necesita más explicaciones para entender que cuando digo calor me refiero a un calor de mil demonios.

 Y eso que este año nos ha dado una tregua porque no ha sido tan sofocante como el verano pasado, pero aún así, está siendo duro.

Proteger a los niños con gorras, gafas de sol, crema solar y mantenerlos hidratados es nuestra obligación, pero cuando son bebés la preocupación de los padres es mayor porque son más vulnerables.

Por eso nuestra principal preocupación este verano ha sido Chiquita, que todavía es muy pequeña, y no queríamos que lo pasara mal en la playa. 

 ![](/tumblr_files/tumblr_inline_ocmo3hMBH21qfhdaz_540.jpg)

Ya sabéis que cuando los bebés son tan pequeños llevarlos a la playa suele ser complicado porque no les puede dar el sol, no conviene bañarlos, hay que llevarlos en su carrito para que echen sus siestas y estén cómodos… Si queremos incluirlos en los planes familiares, tenemos que adaptarnos todos al nuevo miembro.

Así que hemos sometido a nuestro [cochecito Ip Op](/2016/03/09/coleccion-primavera-bebecar.html) de [Bébécar](http://www.bebecar.com/bebecar/es/) a la II prueba extrema (la [primera prueba extrema Bébécar la tenéis aquí](/2016/04/28/coche-bebes-bebecar.html)). 

 ![](/tumblr_files/tumblr_inline_ocmo3iIwOr1qfhdaz_540.jpg)

Esta prueba ha consistido en pasar un día en la playa en pleno agosto, en Murcia, con el calor en su punto álgido, incluso meterlo en la arena. 

## Nos vamos a la playa!

Chiquita va encantada en su grupo 0 Bébécar con su primo al lado y mirando el paisaje.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/hTddoWODAsk" frameborder="0" allowfullscreen></iframe>

Tiene una base que se puede instalar en cualquier vehículo, y el asiento del bebé es amplio y mullido, lo que hace que se encuentren cómodos en él.

Es fácilmente extraíble de la base para poder colocarlo fácilmente en el chasis del carrito (en sustitución del capazo) si lo vamos a usar un ratito corto, porque ya sabéis que lo ideal es que la espalda del bebé se mantenga recta, y para eso lo mejor es el capazo.

## Cubos y palas a la cesta

El traslado del coche a la playa lo hemos hecho en su carrito con capazo sin problemas. 

He de decir que la cesta me ha sorprendido porque caben más cosas de las que parece, entre otras, los cubos y palas de todos mis hijos, agua, dos toallas y un bocata.

 ![](/tumblr_files/tumblr_inline_ocmo3i44YM1qfhdaz_540.jpg)
## Que Corra el aire! 

Una vez allí, lo hemos metido en la arena sin miedo (en eso consisten las pruebas extremas), y para que Chiquita no pasara calor, hemos retirado la parte de atrás de la capota para dejar la malla trasera protectora solamente y así facilitar que el aire circulara, pero al mismo tiempo, le hemos quitado la luz solar directa por la parte de arriba. 

 ![](/tumblr_files/tumblr_inline_ocmo3j5ABx1qfhdaz_540.jpg)
## Base del capazo transpirable 

También hemos abierto los agujeros de la base del capazo, como te muestro en este video, ya que tiene la opción de invierno, con los orificios cerrados, y la de verano, momento en que es aconsejable abrirlos para que transpire por debajo logrando que el bebé esté más fresquito.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/GTD23UAuQlU?start=100&amp;end=125" frameborder="0" allowfullscreen></iframe>
## Interior de algodón

Como también se aprecia en el video, el tejido del interior del capazo es de algodón 100%, lo que, unido a la capota con ventilación y la base del capazo transpirable, hace que el confort esté garantizado, por eso nuestra Chiquita durmió de lo lindo en su siesta. 

 ![](/tumblr_files/tumblr_inline_ocmo3jHlxw1qfhdaz_540.jpg)

Estos detalles son los que marcan la diferencia en un cochecito, Y los que debéis tener en cuenta a la hora de escoger el vuestro.

## Maniobrabilidad en terreno hostil 
 ![](/tumblr_files/tumblr_inline_ocmo3kwDzM1qfhdaz_540.jpg)

La maniobrabilidad en terreno hostil ya quedó demostrada en la prueba extrema I “aglomeraciones”, y también quedó superada en ésta. a veces los accesos a las playas son algo complicados, y en esta ocasión hasta Princesita pudo manejarlo sin problemas.

## Resultado de la prueba

El cochecito cumplió las expectativas:

Superó el acceso a la playa

Resistió la inmersión en la arena, después de la cual, sus ruedas siguieron funcionando a la perfección

Mantuvo fresca a Chiquita: la prueba está en que echó su siesta de rigor

Protegió de la luz directa del sol a chiquita, pero al mismo tiempo le proporcionó ventilación gracias a su capota con malla

Fue el objeto de deseo de algunas de las mamás que había por allí, que quedaron prendadas por su diseño y, como no, por lo bonita que es nuestra Chiquita.

 ![](/tumblr_files/tumblr_inline_ocmo3kMezY1qfhdaz_540.jpg)