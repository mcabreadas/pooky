---
date: '2015-01-05T08:00:00+01:00'
image: /tumblr_files/tumblr_inline_nhqyqgDUfC1qfhdaz.jpg
layout: post
tags:
- hijos
- familia
title: ¿Mamá, quiénes son los Reyes Magos?
tumblr_url: https://madrescabreadas.com/post/107196674487/mamá-quiénes-son-los-reyes-magos
---

## Actualización 6 de enero

![image](/tumblr_files/tumblr_inline_nhqyqgDUfC1qfhdaz.jpg)

Magia en la cabalgata de esta noche, donde mi niño volvió a creer en los Reyes Magos…

Bueno, y creo que yo también…

_Post originario:_

[![image](/tumblr_files/tumblr_inline_nhjyc5Bbo81qfhdaz.jpg)](http://saraillamas.blogspot.com.es/2013/12/carta-los-reyes-magos-para-descargar.html)

“¿Mamá, quienes son los Reyes Magos?”

Así, sin anestesia ni nada, y sabiendo ya la respuesta, pero con necesidad de confirmación me asaltó mi Osito, de 7 años, hace semanas.

Es curioso porque yo misma escuché cómo la Navidad pasada su hermana mayor le desvelaba el misterio, pero él desechó esa información que su cerebro consideró tóxica y siguió creyendo en los Reyes Magos como si nada.

Pero algo debió de quedar en su cabecita pensante, y empezó a plantearse cosas que no le parecían lógicas. Así que este año parece tenerlo claro, y yo no se lo desmentí porque una cosa es crear una ilusión y otra cosa es ante una pregunta directa negarle la mayor.

Después, reflexionando, decidí sacar de nuevo el tema y comprobar cómo había vivido el momento de enterarse y cómo se sentía. Y para mi sorpresa parece que se sintió aliviado por el desconcierto que le había creado creer en algo que no le parecía lógico. Supongo que cuando empezó a madurar y a plantearse cosas empezó a darle vueltas a la cabeza y, de hecho, recuerdo el año pasado insistentes preguntas del tipo:

¿Cómo es posible que repartan los regalos en una noche a todos los niños? ¿cómo puede ser que estuvieran en la Cabalgata hace un momento y ya estén los regalos aquí? ¿por qué viven tantos años?

![image](/tumblr_files/tumblr_inline_nhk8cltkDz1qfhdaz.jpg)

Lo que más me alivió es que no se sintió engañado por nosotros, ya que me esforcé en que comprendiera que era una ilusión maravillosa de la que participamos mayores y pequeños, y que hace que la Navidad sea especial.

Ahora tratamos de mantener la magia en casa por el bebé, que este año está viviendo su [Primera Navidad](/2014/12/23/la-primera-navidad.html) en que es consciente de todo, como os contaba aquí.

Yo de pequeña no lo viví así, ya que mis padres siempre me hablaron, desde el primer momento, de “regalos de Navidad”, nunca me dijeron que los traía Papá Noel, o los Reyes Magos. Sin embargo, siempre respeté la creencia de mis amigas y jamás dije nada.

El motivo por el que no me hicieron creer en los Reyes es porque mi madre recuerda que de niña, cuando se enteró de quiénes eran realmente, sintió una profunda decepción, rabia y cierta traición por parte de sus padres: lo vivió como una mentira según me cuenta. Entonces decidió que no quería que sus hijos pasaran por eso.

Yo creo que ambas opciones son respetables, porque yo no sufrí ningún trauma por no haber creído en los Reyes Magos, ni tampoco estoy viendo en mis hijos ningún efecto adverso por haber creído hasta ahora, pero entiendo que cada niño es un mundo.

Lo que sí tuve claro desde pequeña es que me hubiera gustado creer para saber qué se sentía, así que a mis hijos les creamos la ilusión, y seguiremos manteniéndola siempre, aunque se hagan mayores.

![image](/tumblr_files/tumblr_inline_nhk8brcQ4Q1qfhdaz.jpg)

¿Y tú a los tuyos?