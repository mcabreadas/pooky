---
date: '2014-07-21T10:02:21+02:00'
image: /tumblr_files/tumblr_inline_n8179iRgsE1qfhdaz.jpg
layout: post
tags:
- mecabrea
- crianza
- hijos
- derechos
- familia
- igualdad
- bebes
title: Se retrasa el fin de la prevalencia masculina en el orden de los apellidos
  de los hijos
tumblr_url: https://madrescabreadas.com/post/92414942689/apellidos-hijos-nacimiento-madrescabreadas
---

Traigo una mala noticia para la igualdad de género. Se ha retrasado la aplicación de la Ley que elimina la prevalencia del apellido masculino a la hora de inscribir el nacimiento de un hijo.

La nueva Ley del registro Civil 20/2011, de 21 de julio,  que introduce cambios en trámites tan habituales para el ciudadano de a pie, como inscribir el nacimiento de un hijo, casarse, o pedir un certificado de defunción, comenzará a aplicarse el 15 de julio de 2015, un año después de lo previsto.

## ¿Qué novedad introduce la Ley?

Establece que los progenitores tendrán que elegir el orden de los apellidos. Ya sabéis que hasta ahora el orden establecido consistía en que primero iba el apellido del padre y después el de la madre. Y si se quería cambiar, requería un trámite para hacerlo. Pues ahora, con la nueva ley esto va a cambiar, ya que será necesario el acuerdo del padre y de la madre para determinar qué apellido tendrá su hijo como primero, lo que se irá arrastrando al resto de sus hermanos.

## ¿Qué pasará cuando los padres no se pongan de acuerdo?

En caso de desacuerdo o cuando no se hayan hecho constar los apellidos en la solicitud de inscripción, el Encargado del Registro Civil requerirá a los progenitores, o a quienes ostenten la representación legal del menor, para que en el plazo máximo de tres días comuniquen el orden de apellidos. Transcurrido dicho plazo sin comunicación expresa, el Encargado acordará el orden de los apellidos atendiendo al interés superior del menor.

![image](/tumblr_files/tumblr_inline_n8179iRgsE1qfhdaz.jpg)

Me cuesta entender qué criterios se usarán para decidir cómo afecta el orden de los apellidos al interés superior del menor. Es decir, cómo un apellido puede favorecer el interés del menor más que otro, o al contrario. La ley deja en manos del Encargado del Registro esta importante decisión sin determinar un criterio claro para tomarla. En un principio se propuso usar el orden alfabético, pero parece que se abandonó esta idea porque al cabo de muchos años España sería un país lleno de Abenza, Abarca, Abellán… y otros apellidos cuyas primeras letras coincidieran con las primeras del alfabeto, lo cual no es un criterio muy acertado.

De todos modos, los apellidos los vamos a poder seguir cambiando en cualquier momento de nuestra vida, incluso la nueva ley facilitará los trámites para hacerlo.

¿Qué apellido pondrías primero a tu hijo si pudieras elegir? 

_Fuentes: Ley del Registro Civil 20/2011, de 21 de julio,  [Real Decreto 8/14](https://www.boe.es/boe/dias/2014/07/05/pdfs/BOE-A-2014-7064.pdf) de 4 de julio de 2014_

_Foto gracias Hola_