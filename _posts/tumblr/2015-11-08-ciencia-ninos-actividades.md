---
date: '2015-11-08T20:42:27+01:00'
image: /tumblr_files/tumblr_inline_o39z59Jkwf1qfhdaz_540.jpg
layout: post
tags:
- crianza
- planninos
- gadgets
- colaboraciones
- ad
- tecnologia
- educacion
- familia
title: Ciencia en todas partes
tumblr_url: https://madrescabreadas.com/post/132816937389/ciencia-ninos-actividades
---

Este fin de semana ha tenido lugar la decimocuarta edición de la Semana de la Ciencia y la Tecnología en Murcia, y de nuevo hemos tenido la ocasión de acercar a los niños a este maravilloso mundo, y despertarles el gusanillo.

 ![noche investigadores](/tumblr_files/tumblr_inline_o39z59Jkwf1qfhdaz_540.jpg)

El lema de este año es “Ciencia en todas partes”, y se ha desarrollado en 375 actividades en las que participan 45 instituciones relacionadas con la investigación, la ciencia y la educación como Institutos, Universidades, diversas fundaciones…

El recinto estaba hasta la bandera de familias.

 ![noche investigadosres murcia](/tumblr_files/tumblr_inline_o39z5a3jAU1qfhdaz_540.jpg)

Ya [os contaba en otra ocasión que a mis niños les encantan los experimentos](/2015/09/26/noche-investigadores.html), y esta vez han tenido la oportunidad de participar activamente en ellos.

Por ejemplo, mi mediano sopló en un alcoholímetro y aprendió el principio por el que se sabe si una persona ha bebido alcohol o no.

 ![experimento ninos cientifico](/tumblr_files/tumblr_inline_o39z5aRr1S1qfhdaz_540.jpg)

También manejaron tubos de ensayo, midieron los decibelios de sus gritos (lo que les hizo conscientes de que tienen que gritar menos en casa). Os dejo este video, pero advierto de que bajéis el sonido del altavoz.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/OI30Jhe45YU?rel=0" frameborder="0" allowfullscreen></iframe>

También inflaron un globo mezclando vinagre con bicarbonato, movieron un molinillo con vapor de agua 

 ![cientifico experimento](/tumblr_files/tumblr_inline_o39z5aL7mK1qfhdaz_540.jpg)

Hasta el pequeño de tres años se interesó por cada cosa que veía.

De verdad que recomiendo este tipo de actividades para despertar y satisfacer la curiosidad innata de los niños.

Quién sabe si tenemos en casa un pequeño Einstein o una pequeña Marie Curie.

¿Lo habéis pensado alguna vez?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.