---
layout: post
title: No hace tanto...
date: 2013-11-07T16:19:00+01:00
image: /images/uploads/depositphotos_4435261_xl.jpg
author: maria
tags:
  - crianza
  - lactancia
  - cosasmias
  - bebe
tumblr_url: https://madrescabreadas.com/post/66280975269/no-hace-tanto
---
No hace tanto, mi amor, que [me buscabas a cada instante](https://madrescabreadas.com/2013/05/20/las-horas-contigo-contigo-las-horas-se-llenan-de/) impaciente y te enganchabas a mi piel.

No hace tanto que te dormías sobre mí mientras te daba mi vida en forma de alimento.

No hace tanto, apenas un mes, que me pedías “tata”, que [me buscabas en la noche para consolarte](https://madrescabreadas.com/2013/09/29/nuevos-caminos-hoy-me-siento-triste-me-has/) con mi calor, mi olor, mi sabor…

No hace tanto pero… ¡ay, cómo lo echo de menos, cachorrillo! Cuando no te podías esperar y entre sollozos insistentes salivabas mientras me preparaba para darte el pecho.

No hace tanto, pero a mí me parece una eternidad sin sentir tu calorcito de ese modo, la suavidad de tu boquita, tu naricita, la música que hacías al tragar, al respirar…

Ahora ya corres tras tus hermanos, subes y bajas… Te haces mayor.

Sin embargo no hace tanto, mi vida, que dormías sobre mí y yo notaba tu corazóncito latiendo junto al mío.

Pero ¡Ay, pillín! Aún buscas mi piel; te sorprendo chupándome la mano, haciéndome pedorretas en el brazo o jugando a que me muerdes la nariz.

Y es que tú y yo siempre tendremos una conexión química, casi animal, inexplicable a la razón.

Será nuestro secreto.



*Foto gracias a [sp.depositphotos](https://sp.depositphotos.com/)*