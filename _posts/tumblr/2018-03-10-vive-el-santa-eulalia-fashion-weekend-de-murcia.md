---
date: '2018-03-10T18:10:31+01:00'
image: /tumblr_files/tumblr_inline_p5dx1fzB0e1qfhdaz_500.jpg
layout: post
tags:
- eventos
- participoen
- moda
- local
title: Vive el Santa Eulalia Fashion Weekend de Murcia
tumblr_url: https://madrescabreadas.com/post/171729380634/vive-el-santa-eulalia-fashion-weekend-de-murcia
---

He tenido la oportunidad de colaborar con el evento solidario de moda [Santa Eulalia Fashion Weekend](https://www.facebook.com/Santa-Eulalia-fashion-165068300762886/?fref=mentions) de Murcia, y ha sido una experiencia increíble.

 ![](/tumblr_files/tumblr_inline_p5dx1fzB0e1qfhdaz_500.jpg)

Se desarrolló en la Plaza de Santa Eulalia en Murcia del viernes 9 al domingo 11 de marzo y contó con la presencia de grandes diseñadoras de ropa y complemento para mujer y una pasarela especial con grandes diseñadoras de ropa infantil para los más peques de la casa.

 ![](/tumblr_files/tumblr_inline_p5dx1frYQr1qfhdaz_500.png)

Hubo una gran carpa con mercadillo solidario en beneficio de las familias más desfavorecidas.

 ![](/tumblr_files/tumblr_inline_p5dx1gK9Ag1qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1gRjEX1qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1gYn0Q1qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1hQWO21qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1iI6DB1qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1iNLId1qfhdaz_500.jpg) ![](/tumblr_files/tumblr_inline_p5dx1j1YLA1qfhdaz_500.jpg)

Todo amenizado con foodstruck, musica con dj en directo, rifa, photocall y mucho más, cortes de pelo solidarios, pintacaras, manicura…

## La pasarela de moda infantil Santa Eulalia Fashion Weekend de Murcia

Orgullosa del talento murciano y de todo lo que las diseñadoras de esta tierra pueden ofrecer. Muy muy top todo lo que hemos visto en el Santa Eulalia Fashion Weekend.

Los diseños originales para niños de [Margus Handmade](https://www.facebook.com/MargusHandMade/) conquistaron la pasarela con la espontaneidad y frescura de los niños, y sus superposiciones en camisetas y sudaderas con prints que marcan la diferencia.

Los conjuntos de braguita y camiseta para bebés son ideales.

 ![](/tumblr_files/tumblr_inline_p5fu1vw5Ex1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu2pqZeg1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu37rXvs1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu3sf2K61qfhdaz_540.png)

La dulzura de [Eleconele](https://www.facebook.com/eleconelees-969575083147004/) con gran variedad de creaciones, a cual más original en su combinación de tejidos, colores, texturas y estampados logrando un estilo diferencial.

 ![](/tumblr_files/tumblr_inline_p5fu4wZ2Rg1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu5eOi6Y1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu5wna8S1qfhdaz_540.png)

El romanticismo y elegancia de Ana Franco, cuyos diseños son ideales para ocasiones especiales.

 ![](/tumblr_files/tumblr_inline_p5fu6e6xuR1qfhdaz_540.png) ![](/tumblr_files/tumblr_inline_p5fu6tKOtw1qfhdaz_540.png)

Los modelos, unos campeones, la organización 🔝🔝🔝y la compañía, inmejorable.

 ![](/tumblr_files/tumblr_inline_p5fu79pySS1qfhdaz_540.png)