---
date: '2015-10-18T18:00:26+02:00'
image: /tumblr_files/tumblr_inline_o39z9rt62f1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- cine
- adolescencia
- planninos
- colaboraciones
- ad
- recomendaciones
- testing
- familia
- juguetes
title: Construímos la nave Lego X-Wing Star Wars
tumblr_url: https://madrescabreadas.com/post/131423269998/xwing-lego-starwars
---

[Lego](http://www.lego.com/es-es/starwars/products/episode-vii) desvela algunas de las naves espaciales que aparecerán en la nueva película de la saga Star Wars, Episodio VII “El Despertar de la Fuerza”, que se estrenará en cines el 18 de Diciembre.

## La película

En 1977 se estrenó la primera película de la saga Episodio IV: Una Nueva Esperanza. Tras 38 años y 6 películas, repartidas en 2 trilogías. El Despertar de la Fuerza se desarrollará 30 años después de los sucesos acaecidos en el Episodio VI: El Retorno del Jedi.

Os dejo el trailer. ¡No sé si podremos esperar tanto para verla!

<iframe width="100%" height="315" src="https://www.youtube.com/embed/V8qlIZsutAQ?rel=0" frameborder="0" allowfullscreen></iframe>

No sé vosotros, pero en casa somo muy frikis fans de Star Wars, por eso alucinaron cuando los llevamos a [Disneyland París](http://www.disneylandparis.es/) y llegamos a la zona de Star Wars.

 ![](/tumblr_files/tumblr_inline_o39z9rt62f1qfhdaz_540.jpg)

Tanto es así, que hasta se han empeñado en ir al estreno disfrazados (no es la primera vez):

 ![](/tumblr_files/tumblr_inline_o39z9rT61m1qfhdaz_540.jpg)
## Construímos la Nave Poe’s X-Wing Fighter

Hemos tenido la suerte de que Lego nos haya mandado la nave Poe’s X-Wing Fighter, una de las que aparecerán en la peli, para montarla en casa y compartirlo con vosotros, lo que se ha convertido en nuestra gran aventura familiar/espacial de este fin de semana.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/Qc42AA8Pwx0?rel=0" frameborder="0" allowfullscreen></iframe>

Este caza estelar modificado está equipado con numerosas funciones: cuatro cañones automáticos, dos cañones convencionales, tren de aterrizaje retráctil, alas abatibles, cabina que se abre con espacio para una minifigura 

 ![](/tumblr_files/tumblr_inline_o39z9sJwZQ1qfhdaz_540.jpg)

y espacio detrás para el droide astromecánico BB-8, aunque mis hijos dicen que hubiera sido aún mejor si hubieran puesto un R2D2. 

 ![](/tumblr_files/tumblr_inline_o39z9sCDOw1qfhdaz_540.jpg)

Cuenta incluso con una bodega de carga con armería, misiles de reserva y munición, y un asiento para una minifigura. 

 ![](/tumblr_files/tumblr_inline_o39z9tuqRy1qfhdaz_540.jpg)

Incluye tres minifiguras con diferentes armas y accesorios: Poe Dameron, tripulación de tierra de la Resistencia y un Resistance X-Wing Pilot, así como un droide astromecánico BB-8 

![](/tumblr_files/tumblr_inline_o39z9tvJqw1qfhdaz_540.jpg)

## Hora de recoger las piezas

Así que hemos pasado el fin de semana rodeados de piezas LEGO por todos sitios, menos mal que, a la hora de recoger, me he hecho con algo que debería haber en toda casa donde viva un fan de LEGO, por el bien de él mismo, y de su madre, que evitará ir recogiendo piezas de los sitios más insospechados (la última la encontré en el vaso de la batidora), y algún que otro tropiezo por el pasillo.

![](/tumblr_files/tumblr_inline_o39z9uy2VE1qfhdaz_540.jpg)

Estos [sacos para guardar piezas Lego](https://www.tutete.com/tienda/es/83_novedades/13450_saco-play--go-turquesa.html) son super prácticos porque también les permite jugar sobre él cuando se extiende del todo, de manera que las piezas no tienen por qué salir gracias a su cierre. Y cuando llega la hora de recoger, sólo hay que tirar de la cuerda y ya está! ¡Todo recogido! ¿Lo puedes creer? ¡Cuando lo descubrí lloré de emoción me encantó!

Lo puedes encontrar en [Tutete.com](https://www.tutete.com/tienda/es/83_novedades/13450_saco-play--go-turquesa.html) a un precio muy bueno.

No tengo ni que decir que mis hijos lo han pasado en grande montando la nave con ayuda de su padre, y después, jugando a mil aventuras, cada uno metido en su papel porque,desde luego la X-WIng vuela en casa tan alto como su imaginación.

## Halcón Milenario

Pero, sin duda, la nave más emblemática es el mítico Halcón Milenario, que ha sido reformada con muchísimas novedades externas, como un diseño más aerodinámico y detallado, cabina desmontable con espacio para dos minifiguras, torretas láser superior e inferior giratorias con escotilla y espacio para una minifigura, cañones automáticos dobles, antena de rastreo, rampa y escotilla de entrada. Si abres las placas del cascaún hay más detalles en el interior, como la cubierta principal con área de asiento y tablero de holoajedrez, un sistema de hipervelocidad más detallado, compartimento secreto, cajas y cables y espacio para munición de cañón automático. 

¡Y también incluye Han Solo y Chewbacca!, entre otros personajes.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/iJ6SYxRVHIg?rel=0" frameborder="0" allowfullscreen></iframe>
## Las 7 naves LEGO El Despertar de la Fuerza

Aquí te dejo los videos animados de cada una de las naves para que te decidas por una… si puedes:

1. [Millennium Falcon](https://www.youtube.com/watch?v=iJ6SYxRVHIg)  
2. [Poe’s X-Wing Fighter ](https://www.youtube.com/watch?v=qNzKZUX5t9o)  
3. [Rey’s Speeder](https://www.youtube.com/watch?v=B116kc4u2oc)  
4. [First Order Snowspeeder](https://www.youtube.com/watch?v=iFF1oW5W3N4)  
5. [First Order Special Forces TIE Fighter](https://www.youtube.com/watch?v=n6iR7meAIX4)  
6. [First Order Transporter](https://www.youtube.com/watch?v=OWpdBnEztiA)  
7. [Nave de Combate de Kylo Ren](https://www.youtube.com/watch?v=1mmoiqFQORk)  

¿Con cuál te quedas?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.