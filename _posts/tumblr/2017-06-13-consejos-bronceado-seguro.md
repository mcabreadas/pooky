---
date: '2017-06-13T09:00:22+02:00'
image: /tumblr_files/tumblr_inline_orfmm4RG9q1qfhdaz_540.jpg
layout: post
tags:
- salud
- crianza
- familia
title: 'Día de la prevención del cáncer de piel: consejos para un bronceado seguro'
tumblr_url: https://madrescabreadas.com/post/161766592218/consejos-bronceado-seguro
---

El día 13 de junio es el Día Europeo de la Prevención Contra el Cáncer de Piel. Durante esta jornada se pretende sensibilizar de la importancia de prevenir esta enfermedad con buenos hábitos y con la detección precoz.

La piel tiene memoria, así que lo que nuestros hijos sufran ahora puede perjudicarles en el futuro, por ello en este blog he tratado en varias ocasiones sobre la importancia de [proteger del sol la piel de los niños](/2014/07/11/crema-solar-bebes-madrescabreadas.html), los [ojos con unas buenas gafas de sol](/2016/07/19/gafas-sol-ninos.html), incluso la [piel en el embarazo](/2014/07/11/crema-solar-bebes-madrescabreadas.html).

 ![](/tumblr_files/tumblr_inline_orfmm4RG9q1qfhdaz_540.jpg)

En el 90% de las ocasiones el cáncer de piel se origina por las largas exposiciones a los rayos ultravioletas del sol y sólo el 10% se debe a condicionantes genéticos. 

Te dejo estos [consejos para la prevenir el cáncer de piel](http://www.canceryvida.es/13junio_2017.php) porque los he encontrado muy interesantes, y creo que no los debemos pasar por alto.

Significa esto que no podemos broncearnos este verano? No hace falta llegar a extremos, pero sí tener claro estos consejos para un bronceado seguro que nos propone la [Fundación Hospitales Nisa](http://www.hospitales.nisa.es/):

1. Aplicarse el **protector solar** antes de la exposición al sol y **renovar** frecuentemente la aplicación, sobre todo después de cada baño.

2. Exponerse progresivamente al sol y **evitar la exposición solar entre las 12 y las 16 horas.**

3. **Evitar**  las sesiones bronceadoras con **lámparas de rayos UVA** , que contribuyen a la aparición de cánceres cutáneos y aceleran el envejecimiento cutáneo.

4. **No** exponer a insolación directa a los **niños menores de 3 años**. **En las horas de débil insolación, aplicarles fotoprotector** de alta protección **también**. La piel conserva la memoria de todas las radiaciones recibidas durante la infancia: cuanto más importante ha sido la dosis, mayor es el riesgo de la aparición de cánceres en la edad adulta.

5.  No olvidar que también podemos quemarnos realizando cualquier **actividad al aire libre** : montar en bici, hacer deporte, regar el jardín… en todas estas ocasiones, aplicar un **fotoprotector.**

 ![](/tumblr_files/tumblr_inline_orfmnn54SJ1qfhdaz_540.jpg)

6. No fiarse de las circunstancias que comporten un riesgo suplementario o una falsa seguridad: **altitud, nubosidad, superficies reflectoras** (nieve, arena, hierba, agua), viento fresco…

7. Protegerse con **gorra y gafas de sol** con cristales homologados y capaces de filtrar los rayos UVA y UVB. A los niños, además, ponerles camisetas secas y opacas: **una camiseta mojada deja pasar los rayos UV!**.

8. Secarse bien después de cada baño. El **efecto lupa de las gotas de agua** favorece las quemaduras solares y disminuye la eficacia de los protectores solares aunque sean resistentes al agua.

9. **Beber agua en abundancia y frecuentemente**. El sol deshidrata nuestro organismo. Vigilar especialmente a las personas mayores, cuya sensación de sed está atenuada, y a los niños, cuya necesidad de agua es importante y sus centros de termorregulación son todavía inmaduros.

10. Consultar con el dermatólogo si se advierte que una **peca o lunar cambia** de forma, tamaño o color.

La piel es el órgano más grande que tenemos, así que merece la pena mimar la nuestra y la de nuestros hijos.

¡Cuídate, cuídalos!