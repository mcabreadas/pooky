---
date: '2017-05-17T11:50:07+02:00'
image: /tumblr_files/tumblr_inline_oq38b0hykx1qfhdaz_540.gif
layout: post
tags:
- tecnologia
title: Estamos a salvo del virus de Telefónica?
tumblr_url: https://madrescabreadas.com/post/160762946904/virus-telefonica-wannacry
---

Hace unos días todos nos sentimos vulnerables cuando un virus atacó la red interna de Telefónica, una de las empresas más potentes del mercado español, y nos echamos a temblar pensando si nos podría afectar de algún modo a nivel doméstico.

Ante tal alarma social, hablé con mi informático de cabecera, que sabe mucho de todas estas cosas y que siempre está al día, y lo acribillé a preguntas.

Las comparto aquí para tu tranquilidad porque, es cierto que no guardamos archivos de seguridad nacional, pero para nosotras ciertas cosas son tanto o más importantes.

## ¿Qué es el Wannacry? 

Es un virus del tipo _ransomware_ que afecta solo a Window, cifra tus archivos y luego te piden dinero para recuperarlos (como si fuera un secuestro).

## ¿El virus puede afectar a la red de mi casa?

Sólo si pinchas el archivo adjunto de un correo que venga infectado.

## ¿Cómo puedo evitar que se me infecte el ordenador?

Las medidas preventivas son muy poco eficaces ya que los piratas se aprovechan de fallos en el sistema operativo, y es el propio usuario el que arranca el virus, con lo que la mejor defensa contra este tipo de virus es hacer copias de seguridad de nuestros archivos más valiosos en discos externos o en la nube.

## Tengo contrato con Telefónica, ¿me puedo contagiar?

No porque tu router hace de muro, además, sólo se ha infectado la red interna de Telefónica, no la que da servicio a los clientes porque al funcionar con sistema operativo Unix no es vulnerable a este tipo de virus. 

## Consejos

- Copia tus archivos más importantes en un pen drive o disco externo, y guárdalo en un lugar seguro sin enchufarlo a ningún ordenador. Es mejor perder un mes de archivos que todos tus archivos.  
- Si son archivos de un pequeño negocio o valen dinero, hay opciones en la nube como [Backblaze](https://www.backblaze.com/es_ES/) o [Crashplan](https://crashplan.uptodown.com/windows), que por una pequeña cuota mensual los suben a la nube y los puedes recuperar en caso de catástrofe. En el caso de [Crashplan](https://crashplan.uptodown.com/windows), los puedes recuperar aunque se hayan borrado.  
- Actualiza tu ordenador: activa las actualizaciones automáticas de Windows   
- Recuerda que los anti virus son muy poco efectivos.  

No sé tú, pero yo me he quedado más tranquila, lo que no voy a olvidar es hacer copias de seguridad cada cierto tiempo, sobre todo de mis posts y mis fotos. 

## ¿Qué es lo primero que salvarías tú?
 ![](/tumblr_files/tumblr_inline_oq38b0hykx1qfhdaz_540.gif)