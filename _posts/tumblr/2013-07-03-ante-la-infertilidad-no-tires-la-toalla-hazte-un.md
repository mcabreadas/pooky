---
date: '2013-07-03T21:10:00+02:00'
image: /tumblr_files/tumblr_inline_mpdjrfVSZQ1qfhdaz.jpg
layout: post
tags:
- ad
- colaboraciones
- mujer
- participoen
title: Ante la infertilidad "No tires la toalla, hazte un bonito turbante".
tumblr_url: https://madrescabreadas.com/post/54530978153/ante-la-infertilidad-no-tires-la-toalla-hazte-un
---

![image](/tumblr_files/tumblr_inline_mpdjrfVSZQ1qfhdaz.jpg)  
 Es duro cuando por fin te decides a buscar un bebé y no viene. Creo que en esas ocasiones la naturaleza pone a prueba la fortaleza de la pareja y de la mujer, sobre todo, ya que se tambalea toda nuestra vida y se necesita mucha entereza y optimismo para afrontarlo y seguir luchando.  
 Miriam Cisterna es una mujer que quiere ser mamá y, a pesar de las dificultades no tira la toalla, es más, ha escrito un libro que versa sobre cómo sobrellevar la infertilidad de un modo positivo y sin venirte abajo.

A partir del día 1 de Junio comienzó la distribución a nivel nacional del libro “NO TIRES LA TOALLA: HAZTE UN BONITO TURBANTE" de Marian Cisterna, publicado por MIRA EDITORES, y tanto la editorial como ella están encantados con el primer arranque ya que la respuesta de lectoras que ya se han hecho con él, ha sido fantástica y para la autora esto resulta muy emocionante.  
 Puedes conocer más sobre Mariam y sobre la obra en [www.mariancisterna.com](http://www.mariancisterna.com)  
 Y también en mi Facebook: [https://www.facebook.com/mariancisterna](https://www.facebook.com/mariancisterna)

Desde aquí le mando un fuerte abrazó a Mariam y la felicito por su iniciativa que , sin duda, ayudará a muchas mujeres en los momentos duros.  
 Gracias por tu valentía!

NOTA: Este post no es patrocinado, creo en la causa que defiende el libro y por eso colaboro en su promoción.