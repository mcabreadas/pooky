---
layout: post
title: Cómo organizar una beauty party y triunfar con tu hija adolescente
date: 2017-09-03T16:02:10+02:00
image: /tumblr_files/tumblr_inline_ovpizh8XP21qfhdaz_540.png
author: .
tags:
  - adolescencia
tumblr_url: https://madrescabreadas.com/post/164932385309/beauty-party-adolescentes
---
Estamos entrando en esa [edad preadolescente](/2013/07/05/preadolescentes.html) en la que las fiestas infantiles, no es que no les gusten, sino que ya no las disfrutan como antes. Es decir, el año pasado se podían poner un disfraz de Frozen o de [pirata](/2017/02/05/fiesta-infantil-pirata.html) sin problemas, pero cuál fue mi sorpresa cuando este año me confesaron que … atención:

> Les daba vergüenza!

Mi cara de entre asombro y pena mientras sostenía el traje de Elsa en la mano lo decía todo…

> ¿Pero cuándo habéis crecido tanto?
>
> ¿Dónde están mis niños pequeños?

Así que toca calentarse el coco y renovar ideas para las fiestas con amigos y amigas.\
La última que hicimos fue con las amigas de Princesita para despedir el verano, y se me ocurrió hacer una *beauty party_que se note la deformación bloguera*, _o lo que viene siendo una fiesta de belleza .

 ![](/tumblr_files/tumblr_inline_ovpizh8XP21qfhdaz_540.png)

Cierto es que la mayoría de madres, cuando les mandé la invitación por Whatsapp no sabían de lo que les estaba hablando, pero se apuntaron todas (me junté con 11 en casa), y después comprobaron que el éxito fue abrumador.

Casi no dirigí las actividades de las niñas, simplemente puse sobre la mesa una caja con peines, otra con rulos, horquillas y coleteros, una cesta con quita esmalte, toallitas desmaquillantes y espejos de mano, un tarro con pintaúñas, y otro con todo tipo de sombras de ojos, coloretes, brillos de labios, rimel… que tenía arrumbados desde mi más tierna juventud y que ya no uso.

 ![](/tumblr_files/tumblr_inline_ovpiziRRit1qfhdaz_540.png)

Hasta un marco rosa de les puse para el photocall con letras adhesivas que rezaban: “beauty party”, para que las fotos quedaran de recuerdo.

 ![](/tumblr_files/tumblr_inline_ovpizjgyJl1qfhdaz_540.png)

El caso es que me surgió un imprevisto y llegué cuando ya había empezado el evento, y cuando llegué me encontré que todas, con mayor o menor fortuna, ya se habían pintado las uñas, incluso algunas se habían puesto pegatinitas de esas de manicura, y estaban haciéndose trenzas por parejas.

Entonces metí un poco de baza para que se animaran a ponerse rulos (me va el\
lío), y por último se maquillaron la cara, o bien, quien no quiso, se hizo unos tatuajes con pintura de face panting (os dejo el enlace del nuestro porque es espectacular cómo se trabaja y cómo queda).

Después les puse un almuerzo cursi cuqui digno de la atmósfera rosada que habíamos creado cuyo protagonista fue la [pink lemonade (receta aquí)](/2016/08/18/receta-pink-lemonade.html), acompañada de palomitas (un valor seguro), y sandwiches de Nutella (no quedó ni uno).

 ![](/tumblr_files/tumblr_inline_ovpizlJr0b1qfhdaz_540.png)

Eso sí, el mantel rosa las pajitas en tonos “*rosa y azul Mr. Wonderful”*, y las botellitas de agua de color rosa le dieron el toque de glamour con la que toda beauty party que se precie debe terminar.

 ![](/tumblr_files/tumblr_inline_ovpizmpUDQ1qfhdaz_540.png)

Así que ya ves, simplemente con un poco de imaginación y con cosméticos que ya no uses puedes organizar un sarao en el que tu hija y sus amigas adolescentes lo pasen en grande.

Atrévete con una fiesta de belleza y me cuentas qué tal!