---
date: '2015-06-21T17:00:37+02:00'
image: /tumblr_files/G3X4gpZ.png
layout: post
tags:
- trucos
- vacaciones
- planninos
- familia
title: Viajar con niños en avión. Servicio de acompañante
tumblr_url: https://madrescabreadas.com/post/122083287930/ninos-avion
---

No sé si alguna vez os habéis encontrado en la tesitura de que vuestros hijos viajen en avión solos.

Sabed que esto es posible, y os dejo [este estudio elaborado por Edreams](http://www.edreams.es/planificar-viaje/vuelos-ninos-solos/) de las principales compañías que ofrecen esta posibilidad, requisitos de edad, servicio de acompañante, precios…

[![image](/tumblr_files/G3X4gpZ.png)](/tumblr_files/G3X4gpZ.png)

[Consultar fuente infografía](http://www.edreams.es/planificar-viaje/vuelos-ninos-solos/)  
  

## Las compañías más flexibles

Air Europa y Air France son las dos compañías aéreas que disponen de mayor flexibilidad a la hora de viajar con menores. Doce años es la edad mínima para que puedan hacerlo solos y los dos años, la edad a partir de la que se puede solicitar el servicio de acompañante.

Lufthansa ofrece la posibilidad de que los menores de hasta diecisiete años viajen acompañados de su personal.

 

Por su parte, Air Berlín tiene a disposición de sus clientes unos horarios especiales de facturación para menores.

  

## Las compañías lowcost

Del estudio también se extrae que viajar con menores en easyJet o Ryanair es más complicado. En el caso de la primera, los menores que viajan solos pueden hacerlo a partir de los catorce años mientras que para la compañía irlandesa solo pueden volar solos a partir de los dieciséis. Ninguna de las dos aerolíneas low cost ofrecen servicio de acompañante a menores.

  

## Precio del servicio de acompañante

Para viajar por la Península y Baleares, los precios oscilan entre los treinta euros que cobra Iberia y los treinta y cinco euros que exige Vueling.

 

En el caso de Canarias y Europa las tasas se incrementan por encima de los cincuenta euros.

Los billetes de Europa a América con servicio de acompañante a menores en Iberia, tienen una recarga de setenta y cinco euros.

Para viajes intercontinentales se puede llegar a pagar entre ochenta y noventa euros si los niños viajan acompañados en Lufthansa o Air Berlín.

¿Tienes la experiencia de que tus hijos hayan viajado solos alguna vez en avión?