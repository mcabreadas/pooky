---
date: '2018-07-26T18:58:33+02:00'
image: /tumblr_files/tumblr_inline_pche9gwH9l1qfhdaz_540.png
layout: post
tags:
- crianza
- educacion
- planninos
- familia
title: Ver el eclipse lunar con niños
tumblr_url: https://madrescabreadas.com/post/176305870454/ver-el-eclipse-lunar-con-niños
---

El 27 de julio tendrá lugar el eclipse lunar más largo del siglo XXI. Se conoce con el nombre de luna de sangre o luna roja, comenzará a las 19:14 h, hora peninsular española, y durará 3 horas y 55 minutos, aunque su fase plena durará 1 hora y 44 minutos y podrá verse en casi toda Europa, África, Oriente Medio, parte de Asia Central y en América del Sur. La Tierra oscurecerá la Luna por completo al interponerse entre ella y el Sol y dejará el satélite con ese tono rojizo a nuestra vista.

La totalidad del eclipse empezará a las 21:30 horas y durará hasta las 23:13 horas aproximadamente. Después la Luna empezará a salir de la sombra de la Tierra hasta recuperar la normalidad hacia la 01:30 horas.

## Prepara el terreno

Podemos comenzar preparándolos para ese momento dándoles información adecuada su edad y dejándolos que pregunten aquello que les suscite curiosidad. Dejémosles a ello que nos digan lo que les interesa saber , ya que la curiosidad natural de los niños será nuestro mejor aliado.

Conocer la luna y sus fases puede ser un comienzo, por eso os dejo este divertido póster de [PJ Masks](http://pjmasks.es/home/) 

 ![](/tumblr_files/tumblr_inline_pche9gwH9l1qfhdaz_540.png)

[Descarga aquí el poster de las fases de laguna de los PJ Masks](/tumblr_files/Aprende%20las%20fases%20de%20la%20luna%20con%20los%20PJ%20Masks.jpg?dl=0)

## Dormid una buena siesta

Si tus hijos no la suelen dormir, intenta convencerlos porque la cosa va para largo, y tendrán que estar muy atentos!

## Huye de la contaminación lumínica

Busca con tiempo un lugar estratégico alejado de las luces de la ciudad. En este caso, los que estéis en la costa, en el campo o en la montaña estarás de enhorabuena.

Para ello existen diferentes aplicaciones que nos permiten conocer cuáles son los puntos con menos contaminación lumínica. Es el caso, por ejemplo, de páginas web como [Light Pollution Map](https://www.lightpollutionmap.info/#zoom=5.749291241327494&lat=5169058&lon=-45624&layers=B0FFFFTFFFF) o [The New World Atlas of Artificial Sky Brightness](https://cires.colorado.edu/Artificial-light)

## ¿Hace falta protección para los ojos?

Al contrario que en los eclipses solares, en los de luna no hace falta protección para los ojos.

## Usa prismáticos o un  telescopio

Para los niños supone una oportunidad de oro para aprender cosas sobre el universo, los astros, las estrellas… Con unos prismáticos o un telescopio disfrutarán mucho más del fenómeno.

## Cómo conseguir una buena foto

Si no tenéis una cámara reflex o no entendéis mucho de fotografía también podéis conseguir una foto medio decente de la luna con unos prismáticos o un telescopio y una cámara compacta (como la del móvil). Sólo hará falta que acoples directamente la lente de tu cámara con la de los prismáticos o el telescopio, aguantes la respiración y dispares para conseguir la instantánea. No te desanimes si las primeras te salen movidas, ten paciencia porque el eclipse es largo y tendrás muchas oportunidades de llevarte el recuerdo del eclipse a casa.

Espero que disfrutéis mucho y entre todos llenemos las redes sociales de fotos de este bonito fenómeno!