---
date: '2016-05-08T22:03:06+02:00'
image: /tumblr_files/tumblr_o6vjpoe8RT1qgfaqto1_400.gif
layout: post
tags:
- familia
- eventos
- moda
- comunion
title: día mágico de fimi iv pasarela de moda de
tumblr_url: https://madrescabreadas.com/post/144057648739/día-mágico-de-fimi-iv-pasarela-de-moda-de
---

![](/tumblr_files/tumblr_o6vjpoe8RT1qgfaqto1_400.gif)  
 ![](/tumblr_files/tumblr_o6vjpoe8RT1qgfaqto2_400.gif)  
 ![](/tumblr_files/tumblr_o6vjpoe8RT1qgfaqto3_400.gif)  
 ![](/tumblr_files/tumblr_o6vjpoe8RT1qgfaqto4_400.gif)  
  

## Día Mágico de FIMI (IV)

Pasarela de moda de comunión y ceremonia para 2017

Magnífica Lulú

Hortensia Maeso

Barcarola

Miquel Suay

## Galería completa de fotos

Para ver la galería completa de fotos del desfile pincha [aquí](https://www.flickr.com/photos/142190598@N06/)

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).