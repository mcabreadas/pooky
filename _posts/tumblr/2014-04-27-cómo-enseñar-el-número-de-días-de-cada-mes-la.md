---
layout: post
title: Cómo enseñar el número de días de cada mes
date: 2014-04-27T19:00:24+02:00
image: /images/uploads/calendar-icons.jpg
author: .
tags:
  - planninos
  - 6-a-12
tumblr_url: https://madrescabreadas.com/post/84033218655/cómo-enseñar-el-número-de-días-de-cada-mes-la
---
La abuela les hace el juego de los nudillos no falla. Hace que lo aprendan para siempre de una forma divertida.

<iframe width="400" height="225" id="youtube_iframe" src="https://www.youtube.com/embed/I-N7a3_HtJQ?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Se ponen las manos como en la foto, y se empieza a decir los meses asignando cada uno a un nudillo el primero, a un huequito entre nudillos el segundo, un nudillo el tercero y un huequito el cuarto.\
Los nudillos corresponden a los meses que tienen 31 días, y los huequito a, a los que tienen 30.

Como en todo, siempre hay excepciones. En este caso, febrero correspondería a un huequito, pero hay que explicarles que tienen 28 días o 29, caso de que sea año bisiesto.

Yo lo aprendí así de niña y sigo haciendo este truco cuando tengo dudas sobre los días que tiene un mes. Ni siquiera miro un calendario.