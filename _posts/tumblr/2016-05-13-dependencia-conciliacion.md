---
date: '2016-05-13T12:12:13+02:00'
image: /tumblr_files/tumblr_inline_o75ivpnLQr1qfhdaz_540.png
layout: post
tags:
- mecabrea
- hijos
- cosasmias
- familia
- conciliacion
- mujer
title: Dos generaciones dependientes
tumblr_url: https://madrescabreadas.com/post/144291659559/dependencia-conciliacion
---

Mujer que ronda los 40, con hijos y con padres que van haciéndose mayores.

Ésta es la situación en la que estamos entrando muchas de nosotras, y para la que no sé si estamos preparadas. 

La mayoría, apenas hemos salido de los primeros años de la crianza de nuestros hijos, en los que nuestras preocupaciones rondaban la lactancia, el sueño infantil, la alimentación o el tipo de educación y, de repente, seguramente mucho antes de lo que esperábamos, nos damos cuenta de que otra generación empieza a depender de nosotras para muchas cosas.

 Entonces tomamos consciencia de que acabará dependiendo del todo cuando nos encontramos al tiempo preparando un examen y cuidando a nuestra madre  en el hospital. Dos generaciones dependientes sobre los hombros de una mujer. 

 ![](/tumblr_files/tumblr_inline_o75ivpnLQr1qfhdaz_540.png)

Si tiene suerte recibirá ayuda de su pareja, de sus hermanos… pero no creo que reciba mucho apoyo en su trabajo ni por parte de la Administración Pública. No. Éste es un toro, más grande que el anterior, cuando se quejaba de la falta de conciliación para atender a sus hijos.

Ahora que se apriete bien los machos porque a la dedicación de tiempo a sus mayores y a sus hijos, tendrá que sumar el desgaste emocional que supone ver a sus padres envejecer, y pasar poco a poco de apoyarse en ellos a ser su propio brazo el que los sostenga. 

Si antes hacía encaje de bolillos para poder cuidar de sus hijos y su hogar y, además, trabajar fuera de casa, ahora no sé cómo lo va a hacer. Si antes tenía el corazón dividido en dos entre su carrera profesional y sus hijos, ahora se partirá en tres porque sus padres la necesitan, todos la demandan, y a todos los quiere con locura. 

Además, los tiempos no acompañan. El estilo de vida actual dista mucho del de antes, cuando las familias vivían más cerca propiciando la convivencia y la ayuda entre ellos. Ahora se vive de otra manera, más independiente. Cada hermano quizá en una ciudad distinta, lo que dificulta bastante las cosas. 

Esta reflexión no es mía. 

Viví una situación similar a la de una buena amiga, y ella me abrió los ojos. Pero tiene toda la razón, podemos hacerlo. 

Aunque nos tambaleemos tenemos que ser fuertes, y apoyarnos en quienes tenemos al lado porque estamos llamadas a sostener sobre nuestros hombros a quienes nos sostuvieron desde que nacimos, y a quienes sostuvimos cuando nacieron. 

Gracias, P.