---
layout: post
title: Un entierro sin cura
date: 2014-01-06T17:15:00+01:00
image: /images/uploads/depositphotos_167484128_xl.jpg
author: maria
tags:
  - mecabrea
tumblr_url: https://madrescabreadas.com/post/72448128263/un-entierro-sin-cura-pensaba-contaros-hoy-sobre
---


Pensaba contaros hoy sobre nuestro día de Reyes en familia, sobre el Furby de Princesita, el escudo de Osito o el correpasillos de Leopardito, pero no. A veces lo inesperado irrumpe en nuestras vidas en el momento más… eso, inesperado.\
 Mi tía abuela ya era mayor, tenía sus achaques, pero no lo esperábamos. Estuvo en pie hasta el último momento. Tuvo una muerte “buena”, en su casa, sin hospitales, sin hijos agotados de hacer turnos…\
 Ella era Católica, sus hijos no mucho, pero querían respetar los deseos de su madre y la llevaron a enterrar a su pueblo. Un pueblo manchego que no tiene la entidad como para tener Párroco propio, sino compartido con otros dos más de la zona.\
 Mi tía abuela tuvo la mala fortuna de necesitar un Cura el día de Reyes para que oficiara la Misa de su último adiós en el pueblo que la vio nacer, crecer, reír, llorar, casarse, tener hijos, criarlos, hacerse mayor, pasear por sus áridas calles…\
 Pero el cura no acudió. Sus hijos no salían de su asombro. Ellos no son muy de Iglesia, pero sí que sabían que una de las Bienaventuranzas que Jesús nos enseñó fue “enterrar a los muertos”. Por eso su dolor se agravó aún más si cabe al no recibir el consuelo del representante de Dios en la tierra, y pensaron que si en un momento como ése no podían contar con un Cura, a pesar de haber llamado a todos los de la zona, cuándo iban a poder acudir a él.\
 Además, el motivo aducido por éste no deja de ser curioso, y es que la pobre difunta, vino a necesitarlo a las 13:00 h , hora que coincidía con la comida del Párroco, siendo ésta cita ineludible para él… o por lo menos más que la de enterrar a la muerta…\
 Menos mal que mi madre (la que vosotros conocéis por “[la abuela](/portadasalitaabuela)”) estaba allí, y ni corta ni perezosa, inspirada por sus años de educación religiosa, subió al Altar y “condujo” un responso muy especial en el que participó toda la familia porque, como bien dijo cuando tomó la palabra, parafraseando a Jesús: ” allí donde halla dos o más reunidos en mi nombre, allí estoy yo”, y la verdad es que no necesitamos más.\
 La Iglesia estaba llena, todos rezamos juntos e hicimos las lecturas. Finalmente quien quiso dijo unas palabras de despedida a la difunta.\
 Puedo deciros que fue un acto muy emotivo, íntimo, personal y que transmitía calor, a pesar del frío que hacía en aquel templo húmedo.\
 Pero no puedo dejar de pensar qué hubiera pasado de no haber intervenido mi madre.\
 Y estoy, más que cabreada, descorazonada. Y me pregunto cómo se puede dejar a un difunto sin un acto religioso cuando ese era su deseo?\
 Qué puede haber más importante que enterrar a un muerto y más ineludible e inaplazable?

## El apoyo de Xiskya

ACTUALIZACIÓN 7-1-14:

Quiero agradecer el apoyo recibido por mi amiga Xiskia, mi querida “monja del futuro”, como yo la llamo.

Personas así son las que necesita la Iglesia. Las que necesitamos todos en nuestra vida:

> [@madrescabreadas](https://twitter.com/madrescabreadas) Cuánto lo siento, amiga. Comprendo tu cabreo. Yo tb lo estaría. Dios tenga misericordia de quién no cumplió con su deber.
>
> — © Xiskya #iMision (@xiskya) [enero 7, 2014](https://twitter.com/xiskya/statuses/420394376138072065)

<script charset="utf-8" src="//platform.twitter.com/widgets.js" type="text/javascript"></script>

> [@madrescabreadas](https://twitter.com/madrescabreadas) Y siento muchísimo que hayas tenido tan mal día este día de Reyes, justo x quién tenía q haber sido consuelo y alivio.
>
> — © Xiskya #iMision (@xiskya) [enero 7, 2014](https://twitter.com/xiskya/statuses/420394566483992576)

<script charset="utf-8" src="//platform.twitter.com/widgets.js" type="text/javascript"></script>