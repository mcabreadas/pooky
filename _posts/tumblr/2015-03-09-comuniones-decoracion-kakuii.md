---
date: '2015-03-09T08:00:11+01:00'
layout: post
tags:
- trucos
- decoracion
- comunion
- recomendaciones
- familia
title: Mamás emprendedoras.Entrevistamos a Kakuii
tumblr_url: https://madrescabreadas.com/post/113148455061/comuniones-decoracion-kakuii
---

Os quiero presentar a dos murcianas que tienen, a mi juicio, uno de los gustos más exquisitos para decorar espacios, además de las canastillas de bebé más originales con las que me he encontrado.

Os presento a Laura e Inma, dos mamás emprendedoras, que un día decidieron dar un giro a sus vidas para poder conciliar trabajo y familia. Así, crearon[Kakuii](http://kakuii.es/).

Pero mejor que sean ellas mismas las que os lo cuenten todo en esta entrevista que les hice:

**“¿Qué es Kakuii?**

           Para nosotras, Kakuii es un estilo de vida. No podemos definirnos únicamente como una tienda online porque no nos sentimos así, para nosotras Kakuii es una extensión de nuestra personalidad y deja una huella en todos nuestros trabajos,es el _estilo Kakuii_.

  **¿Qué ofrecéis?**

           Principalmente ofrecemos canastillas de bebé no convencionales, con un toque diferente. 

Poco a pocohemos ido añadiendo otros productos, como una línea más moderna de bodis ybaberos que llamamos Jispe, láminas de nacimiento, cojines de cuna, álbumes de bautizo 

e incluso detalles para las mamás y los papás.

           Además tenemos otras tres líneas de negocio:

 ·         **Craft party**. Hacemos talleres de manualidades para niños y adultos desde que empezamos, pero nos parecía que se había llegado a un punto de saturación, que había demasiada oferta en Murcia. Quisimos darle una vuelta al concepto creando la Craft Party (_fiesta de manualidades_ en inglés), que consiste en llevarte el taller a casa. Tenemos una carta con varios para elegir, pero montamos talleres personalizados, de cualquier temática.

·         **Decoración efímera**. Es la evolución de nuestra idea de negocio inicial, empezamos organizando y decorando eventos. Ahora decoramos espacios utilizando materiales como el papel y el cartón, que no están pensados para durar mucho tiempo y nos permiten realizar una instalación temporal a un precio más económico.

·         **Papelería creativa.** Felicitaciones personalizadas para ocasiones especiales, basando el diseño en las aficiones del destinatario, recordatorios de comunión o bautizo, felicitaciones de Navidad y, en general, cualquier elemento de papelería con un diseño exclusivo y personalizado.

  **¿Quién está detrás de Kakuii?**

Dos mamás recientes, profesionales sectores muy distintos     (Laura es Ingeniera de Telecomunicación e Inma, publicista), que en un punto de inflexión decidieron dar un giro de 180º a su vida profesional para poder conciliar trabajo y familia.

 Pero Kakuii no sólo somos nosotras, Kakuii no existiría sin el apoyo incondicional de nuestros maridos (Juan Pablo y Pedro), que además de tener sus propios trabajos son informáticos, fotógrafos, comerciales, decoradores, coachs… y sobre todo papás.

**¿Cuándo surge?**

           En junio de 2013, durante la organización del bautizo de Pablo.

**¿Por qué el nombre de Kakuii?**

Buscábamos una palabra para definir nuestro estilo, lo que queríamos hacer. Desde el principio nos ha influido la cultura japonesa, así que cuando descubrimos que Kakuii significaba cool en japonés lo tuvimos claro.

**¿Os dedicáis exclusivamente o además tenéis otro trabajo?**

           Nos dedicamos en exclusiva a Kakuii.

**¿Cómo compagináis todo?**

           Con mucho esfuerzo, trabajando muchas horas y también con mucha ayuda.

**¿Hacéis los trabajos de Kakuii en casa? ¿Cómo repartís el espacio con el resto de la familia?**

Si, todavía no tenemos un estudio, aunque soñamos con abrir un espacio en el que podamos tener un showroom, hacer talleres…

**¿Vendéis sólo en Murcia o en el resto de España?**

Vendemos a toda España mediante nuestra tienda online,  a través de otras tiendas tanto físicas como online y en ocasiones puntuales, en showrooms con otros diseñadores (el próximo, los días 13 y 14 de marzo en el Real Casino de Murcia)

**Acabáis de lanzar vuestro [catálogo de Comunión](http://kakuii.es/categoria-producto/comuniones/) para este año, ¿cuál es vuestro producto estrella?**

Pensamos que no hay un producto estrella en este catálogo. Cada una tenemos debilidad por un producto en concreto, pero eso no lo vamos a decir ;)

 [Ver el catálogo de Comunión de Kakuii](http://kakuii.es/wp-content/uploads/2015/02/Cat%C3%A1logo-Comuni%C3%B3n-2015.pdf)

**¿Personalizáis cada trabajo?**

Ofrecemos siempre al cliente la posibilidad de hacerlo, creemos firmemente que la personalización del producto  le aporta un valor. Al fin y al cabo, se convierte en algo único.

**¿Cómo lográis acertar con la idea de cada cliente?**

Nos gusta establecer una comunicación estrecha con el cliente, que nos cuente qué idea tiene en mente y, a partir de ella, trabajamos.

Además tenemos la suerte de que muchos de nuestros clientes buscan nuestro estilo y se dejan asesorar por nosotras, confiando desde el principio en el resultado que les vamos a ofrecer.

**¿Cuál es vuestro sello? ¿Qué ofrecéis diferente al resto?**

Nuestro sello es nuestra personalidad, nuestra propia forma de ver la vida. Imprimimos la huella Kakuii en todo lo que hacemos, y mimamos al máximo los detalles.

Nuestros productos están fundamentalmente realizados a mano, siempre utilizamos bodis y baberos 100% orgánicos certificados GOTS y de comercio justo. Hacemos diseños diferentes, muy cuidados y presentados en packagings especiales.”

Gracias, Laura e Inma por vuestra amabilidad y por satisfacer mi curiosidad sobre vosotras y vuestro trabajo. 

Y muchas gracias por el detalle que tuvísteis con mi hija, que recibió su primer regalo de Comunión de vuestra parte. Este fantástico libro de firmas hecho a mano con todo vuestro cariño (recordad que está pendiente vuestra firma)

¿A que os gusta?

Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).