---
date: '2014-08-27T17:15:00+02:00'
image: /tumblr_files/tumblr_naz125OpP41qgfaqto1_1280.jpg
layout: post
tags:
- vacaciones
- planninos
- familia
title: excursion niños coilloure francia
tumblr_url: https://madrescabreadas.com/post/95916703759/excursion-niños-coilloure-francia
---

![](/tumblr_files/tumblr_naz125OpP41qgfaqto1_1280.jpg)  

## Viajar con niños:Acantilados de Coilloure, Francia
 Comenzamos nuestra aventura por este país hasta llegar en coche a [Disneyland París](/post/96438410679/viajar-con-ninos-sueno-cumplido-llegamos-a) con nuestros tres niños.  
 Coilloure es un pueblo costero del sur de Francia que merece la pena visitar por su belleza y la de sus acantilados y calas.

Aunque la playa es de piedras y no muy bañable para los niños, no deja de ser una maravilla. La prueba es que estaba lleno de visitantes, incluso bastantes españoles.

A la entrada al pueblo hay una zona de picnic bastante grande y muy recomendable para ir con niños, desde donde se ve un el mar color plomo que tanto me ha llamado la atención.

Está muy cerca de la frontera con España, así que si vivís por Cataluña no dejéis de visitarlo porque es una excursión muy bonita.

Ah! Y si vais con bebés no olvidéis mochila o fulard para portearlos para poder acceder a pie sin limitaciones. Merece la pena explorar la zona.