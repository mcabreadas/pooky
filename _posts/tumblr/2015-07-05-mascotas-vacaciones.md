---
date: '2015-07-05T19:42:22+02:00'
image: /tumblr_files/tumblr_inline_nr0x3tHuPw1qfhdaz_540.png
layout: post
tags:
- vacaciones
- planninos
- colaboraciones
- ad
- eventos
- familia
- mascotas
title: Calor, vacaciones, mascotas y niños.
tumblr_url: https://madrescabreadas.com/post/123294793084/mascotas-vacaciones
---

De nuevo nuestra mamá colaboradora, [Mónica Marcos, de Quica & Kids](https://twitter.com/Quicaykids)acudió a un evento como embajadora del blog. 

Me interesaba de forma especial todo lo que aprenderíamos de la mano de Melisa Tuya, y Kiwoko sobre mascotas por varios motivos: 

-Es verano y llegan las vacaciones, momento en que muchas mascotas son abandonadas por sus dueños, 

-Me encató que el tema fuera enfocado a los niños, y cómo deben tratar a los animales

-Y, por último, Marisa tiene una sensibilidad especial con los animales, además de ser compañera blogger con la que he tenido el placer de coincidir en alguna ocasión.

Así que no dudé en pedir a Mónica que fuera en mi nombre y se enterara muy bien de todo para compartirlo con vosotros:

 ![image](/tumblr_files/tumblr_inline_nr0x3tHuPw1qfhdaz_540.png)

“¡Madre mía! Como todo los años pasamos del frío al calor en nada más y nada menos que… ¡24 horas! O incluso menos tiempo. Es una barbaridad porque nuestros cuerpos tienen que adaptarse a los cambios climáticos en muy reducido espacio de tiempo. De igual manera les ocurre a nuestra mascota. Tenemos que estar muy pendiente de ella; Nuestra mascota es frágil, delicada y vulnerable. Su salud puede verse perjudicada.

Un día más tuve la oportunidad de asistir a una merienda riquísiiima como embajadora de nuestra amiga María de[madrescabreadas.com](/) con [Kiwoko](http://www.kiwoko.com/), [Melisa Tuya](https://twitter.com/melisatuya) y, como no, compañeras blogueras.

 ![image](/tumblr_files/tumblr_inline_nr114xycdU1qfhdaz_540.jpg)
## #ConsejosKiwoko

Kiwoko, cadena de tiendas de mascotas ubicada en España, nos facilitó **algunos consejitos para cuidar a nuestra mascota en esta época repleta de caloré** y Melisa Tuya, animalista, periodista en [@20m](https://twitter.com/20m), escritora de [#Galatea](https://twitter.com/hashtag/Galatea?src=hash) y[#Enlasaladeespera](https://twitter.com/hashtag/Enlasaladeespera?src=hash), nos habló de **la relación entre animales y niños, su acercamiento en los parques, el papel de los padres y la tendencia responsable de los animales en casa**.

¡Comenzamos!

## _Calor_

Es importante que sepas que la temperatura de perros y gatos es similar a la de los seres humanos, por lo tanto, todas las precauciones que tienes contigo y con tu familia tienes que tenerla con tu mascota: No sacar a pasear al perro a las horas de más calor (en el caso de mascotas caseras, mantenerlas en un lugar fresquito y si es posible climatizado), beber mucha agua (para ello una buena opción es poner en distintos lugares del hogar recipientes con agua y cambiar ese agua frecuentemente, muy recomendable para gatos), comer más veces al día ( es aconsejable ponerles la comida húmeda), cortales el pelo, etc…

**¿Cómo puedes saber que tu mascota tiene un golpe de calor?** tu mascota no habla, sin embargo, notarás que el gesto de su cara no es el habitual, o notarás que pertenece mucho rato en el mismo lugar sin moverse. Cuando ocurra esto es recomendable que le pongas paños de agua fresca en las almohadillas de las pezuñas, en las orejas o incluso que le empapes un poquito el cuerpo. Y por favor…. ¡No dejes NUNCA en el coche ni a tu/s hijos ni a tu/s mascotas! Puede ser mortal, cada minuto que estén encerrados en él aumenta 0.07 grados la temperatura del coche.

## _Viajes_

¿Qué hacer cuando llega la época estival con tu mascota?

Hoy en día existen multitud de opciones para poder disfrutar de las vacaciones en familia sin que sea un problema tener una mascota en casa.

Puedes llevártelo de viaje en coche, con su correspondiente trasportín, arnés, en el maletero con su malla para evitar que se vaya hacia la parte de los asientos, etc… (todo dependerá del animal que tengas). Para evitar que se mareen en los viajes no le des de comer a partir de dos horas antes de que tengas pensado partir de viaje. Con los gatos, es algo más complicado, porque si te marchas de vacaciones un periodo corto de tiempo no merece la pena que te lo lleves porque se estresan mucho. Déjales en casa.

·         Puedes llevarle a residencias caninas donde te cuidarán a tu animal. ¡Estará como en un hotel!

·         O puedes contratar a algún particular de acogida de animales para que en el periodo que te vayas de vacaciones cuide de tu mascota. Esta opción se está poniendo muy de moda porque el animal sigue residiendo en un ambiente familiar y notará menos el cambio.

## _Alimentación_

Con el aumento de temperatura disminuye el apetito (eso te pasa a ti y a los animales). Para asegurarte que tu mascota está bien alimentada en verano, debes de darle de comer con más frecuencia, cambiar los hábitos. Es aconsejable que coman en las horas de menos calor, con el pienso algo humedecido ¿Sabes una chuche que es digna y que se comerá estupendamente tu mascota? La manzana. Trocitos de manzana fresquita ¡Que rica! Con ella aumentará su nivel de agua en su cuerpo y además le ayudarás a combatir el sarro de sus dientes.

## Mejor adoptar que comprar

Melisa Tuya, amante de los animales, etóloga de afición, posee en adopción a una perrita adulta llamada Troya y dos gatos. Le apasiona los derechos de los animales y trasmitir a la gente lo importante que es tomar la decisión de tener animales en casa. Por supuesto, muy recomendable, pero quiso dejarnos claro que conlleva una tremenda responsabilidad por parte de toda la familia.

Melisa nos aconsejó que si ya hemos decidido tener una mascota en casa que adoptemos, nunca compremos, por favor, animales de cristalera. Hay que educar a nuestros hijos a entender que los seres vivos no se compran ni se venden; Que a nadie le gustaría vivir encerrado en cuatro paredes. 

## Educar a las mascotas

A las mascotas, como a nuestros hijos, hay que educarlas. Las familias que sepan muy bien cómo hacerlo serán adecuadas para tener cachorros. Por el contrario, Melisa, nos aconsejó que si no poseemos mucha idea de ello, adquiramos mascotas adultas.

¡Mira qué video más cuqui de cómo Troya es una perra adulta educada!

<iframe src="https://www.youtube.com/embed/pwj0B3IU5CY" frameborder="0"></iframe>
##  Animales y niños

Por último, Melisa nos dijo que si alguna vez tu hijo quiere acercarse a un perro desconocido SIEMPRE, SIEMPRE tiene que preguntar a su dueño si se puede. Es un acto que te quitará de algún que otro disgusto. Además NUNCA debe echarse encima del perro, subirse a caballito ni hacer ningún movimiento brusco porque el animal se asustará y se pondrá a la defensiva. Es aconsejable que le acaricie por debajo del morro.”

 ![image](/tumblr_files/tumblr_inline_nr0xakFDHA1qfhdaz_540.png)

Muchas gracias por tan valiosa información, Mónica. Como siempre, un placer!

Y tú ¿Cómo haces que tu mascota combata el calor?