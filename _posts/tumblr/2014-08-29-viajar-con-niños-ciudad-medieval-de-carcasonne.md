---
date: '2014-08-29T07:37:00+02:00'
image: /tumblr_files/tumblr_nb1zm7v05k1qgfaqto1_1280.jpg
layout: post
tags:
- crianza
- vacaciones
- planninos
- educacion
- familia
title: viajar con niños ciudad medieval de carcasonne
tumblr_url: https://madrescabreadas.com/post/96065174784/viajar-con-niños-ciudad-medieval-de-carcasonne
---

![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto6_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto7_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto8_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto9_1280.jpg)  
 ![](/tumblr_files/tumblr_nb1zm7v05k1qgfaqto10_1280.jpg)  
  

## Viajar con niños: Ciudad medieval de Carcasonne, Francia. 

La segunda parada en nuestro viaje a [Disneyland París](/post/96438410679/viajar-con-ninos-sueno-cumplido-llegamos-a) en coche ha sido   
 la ciudad medieval de Carcasonne, que es la más antigua fortaleza de Europa y probablemente la que en mejor estado se conserva. 

Para los niños es toda una experiencia imaginarse caballeros andantes, reyes, princesas y fosos con cocodrilos (y tiburones, según mi peque), bajo el puente levadizo de la entrada del Castillo. 

Recomiendo hacer la ruta por dentro a partir de los 6 años de edad, ya que el recorrido dura más de una hora, pero merece la pena porque está totalmente restaurada y en perfecto estado.  
 Si vais con niños más pequeños, lo ideal es hacer la ruta en el tren que rodea la fortaleza por fuera, donde una locución va explicando los pormenores del monumento. 

A los alrededores de la fortaleza hay todo un entramado de hoteles, restaurantes y tiendas donde se pueden comprar armaduras, arcos, espadas de madera, escudos y cascos para los niños con lo que se sentirán auténticos caballeros medievales.  
 Las calles están llenas de encanto y hay rincones dignos de fotografiar.  
 A mis mayores les encantó la visita, y el bebé alucinó con el tren.