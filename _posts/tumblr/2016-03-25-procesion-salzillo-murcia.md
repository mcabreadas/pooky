---
date: '2016-03-25T13:58:21+01:00'
image: /tumblr_files/tumblr_o4lipf5ITY1qgfaqto1_1280.jpg
layout: post
tags:
- vacaciones
- planninos
- familia
title: procesion salzillo murcia
tumblr_url: https://madrescabreadas.com/post/141657903419/procesion-salzillo-murcia
---

![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto1_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto2_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto3_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto4_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto5_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto6_1280.jpg)  
 ![](/tumblr_files/tumblr_o4lipf5ITY1qgfaqto7_1280.jpg)  
  

## Mañana de procesión: los Salzillos de Murcia

Y no una procesión cualquiera, sino la de los “Moraos” o “Salzillos”, que el Viernes Santo viste de este color las calles de Murcia. Esta mañana mi hija nos ha despertado temprano para que no nos la perdiéramos, ya que sale a las 8 de la mañana.

Así que nos hemos puesto guapos y hemos salido a su encuentro. La hemos visto en la Plaza de Santo Domingo, y los pasos han lucido así de bonitos.

Francisco Salzillo es el escultor barroco por excelencia. Sus tallas dan vida a esta procesión, una de las instituciones pasionales más antiguas e importantes de la ciudad por su honda tradición y su espectacular patrimonio de imágenes. La Semana Santa de Murcia es una fiesta religiosa declarada de Interés Turístico Internacional.

Se trata de una de las Semanas Santas españolas de mayor importancia tanto por su excelente patrimonio histórico, como por poseer un estilo propio (el estilo tradicional) originario del siglo XVIII y que supone una forma única en España de celebrar la pasión, lo que la convierte en una semana santa especial en el panorama nacional.

 Si puedes, no te la pierdas, porque merece la pena disfrutarla.

[Aquí te dejo más información, fotos y videos](http://bit.ly/1n2BJU5)