---
date: '2011-01-29T19:44:00+01:00'
image: /tumblr_files/tumblr_lfss30pBXQ1qgfaqto1_1280.jpg
layout: post
tags:
- mecabrea
title: Colegios que ponen pegas para la conciliación
tumblr_url: https://madrescabreadas.com/post/2996837000/colegios-que-ponen-pegas-para-la-conciliación-me
---

![](/tumblr_files/tumblr_lfss30pBXQ1qgfaqto1_1280.jpg)  

## Colegios que ponen pegas para la conciliación

Me gustaría contaros experiencias propias y ajenas, que me llegan por distintos medios y que, siendo cotidianas, nos ayuden a quejarnos y denunciar las injusticias y a defender a nuestros hijos a capa y espada. Ahí va la primera:

Parece mentira que en la era de la conciliación de la vida laboral y familiar se reciban convocatorias para la reunión de la clase del colegio de los niños excluyéndolos expresamente en negrita y subrayado. Estoy de acuerdo con que la reunión fluiría mejor sin la presencia de los pequeños, pero… No sería mejor ofrecer una alternativa para que no entorpezcan la asamblea en lugar limitarse a “aconsejar” que no asistan? Está claro que los padres y/o madres que llevan a sus hijos consigo a estas cosas es porque no tienen más remedio, ya que suelen acontecer fuera del horario escolar, y no todo el mundo puede permitirse cuidadora para asistir, o no entra dentro de su organización domestica, pero siendo en un colegio la asamblea, y disponiendo de espacios para juegos, se me ocurren varias alternativas para facilitar la asistencia a los padres y madres que estén interesados y no tengan con quién dejar a sus hijos. Por ejemplo, los niños podrían jugar en un aula o espacio seguro del colegio mientras los padres y madres los vigilan por turnos a lo largo de la reunión; alternativa sencilla y gratuita que facilitaría la asistencia a todo el mundo. Otra opción es saltarte el “consejo” y presentarse con la criatura en la reunión soportando las recriminaciones de la maestra y las miradas inculpatorias de algunos de los asistentes (lo digo porque lo he presenciado); “oigan, que no traigo a mi hijo por gusto, sino porque no tengo más remedio, no me lo ponga más difícil”. En fin, parece mentira que cuando parece que vamos avanzando en la conciliación se vean estas cosas, y más en una institución que nunca te lo hubieras esperado… Es descorazonador.