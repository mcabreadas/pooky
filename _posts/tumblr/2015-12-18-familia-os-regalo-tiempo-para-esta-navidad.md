---
date: '2015-12-18T09:25:06+01:00'
image: /tumblr_files/tumblr_inline_o06eblAAAH1qfhdaz_540.jpg
layout: post
tags:
- navidad
- trucos
- regalos
- premios
- planninos
- recomendaciones
- familia
title: Familia, os regalo tiempo para esta Navidad. Sorteo Kiddy´s box
tumblr_url: https://madrescabreadas.com/post/135433740514/familia-os-regalo-tiempo-para-esta-navidad
---

## Ganadora:

Muchas gracias a todos por participar, me alegro de que os haya gustado la idea de Kiddy´s Box de regalar actividades y tiempo para vuestros niños. La ganadora del sorteo es Lamamafaelquepot. Enhorabuena!!

 ![](/tumblr_files/tumblr_inline_o06eblAAAH1qfhdaz_540.jpg)

Decía Michael Ende en su libro Momo que “El tiempo es vida y la vida reside en el corazón”.

 ![](/tumblr_files/tumblr_inline_nzjopqyzqe1qfhdaz_540.jpg)

Muchas veces no nos damos cuenta, pero los recuerdos que construímos con nuestros hijos son lo más importante que les podemos regalar porque es único que les acompañará siempre.

Echa la vista atrás y piensa en el primer recuerdo que se te venga a la cabeza sobre tu infancia.

Ahora dime, ¿era un objeto, algo material, o era una vivencia con tu familia?

Creo que la mayoría tenéis en el corazón momentos especiales vividos con vuestro padres o hermanos, por eso os propongo un regalo diferente con el que triunfaréis seguro estas Fiestas con vuestros hijos, sobrinos…

## Construye recuerdos en familia

¿No os pasa que os gustaría hacer mil cosas en familia, pero nunca os paráis a planearlo, y al final, cuando tenéis un día libre no se os ocurre qué hacer?

¡Con la cantidad de actividades súper chulas que hay!

Imagínate que os vais a saltar por los árboles, avistar ballenas, de escalada, a pasear a caballo, aprender repostería, conducir motos de nieve, pasear en un barco pirata, hacer arte con chocolate, practicar tiro con arco o a hacer un curso de rock and roll.

 ![](/tumblr_files/tumblr_inline_nzjopqplhx1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_nzjoprVgp61qfhdaz_540.jpg)
## ¿Cómo pongo debajo del árbol de Navidad una experiencia?

Quizá pienses cómo se puede regalar estas cosas para Navidad si no las puedes poner debajo del árbol, ¿no?

Pues con [Kiddy´s box](http://www.kiddysbox.com/) sí puedes porque son unas cajas de vivencias divertidas, educativas y que transmiten valores para los niños y que se presentan con un original packaging reutilizable, una lunch box de plástico con autocierre que sirve como fiambrera y que en su interior contiene una tarjeta regalo con el código de activación; una guía de actividades para escoger y una pulsera recordatorio para que los niños se acuerden siempre del regalo y la experiencia.

 ![](/tumblr_files/tumblr_inline_nzjopsmUMs1qfhdaz_540.jpg)
## Actividades Kiddy´s box   

Las actividades se dirigen a un público de entre 3 y 14 años y están avaladas por un comité científico que selecciona cuidadosamente todas las experiencias, siempre teniendo en cuanta su valor pedagógico y educativo.

 ![](/tumblr_files/tumblr_inline_nzjopsddVr1qfhdaz_540.jpg)

Su variedad de oferta es tan amplia que seguro que cada niño encuentra su actividad favorita. Tienen desde talleres y cursos (cocina, canto, fotografía, yoga, magia…), naturaleza y deportes (barranquismo, snowboard, equitación, golf…), y también escapadas con los padres que incluyen estancia en un hotel y alguna actividad lúdica y formativa. 

##   

## Qué sorteamos

Kiddy´s box me ha cedido una caja de experiencias “¡Saborea la vida!”

 ![](/tumblr_files/tumblr_inline_nzjopt0NE31qfhdaz_540.jpg)

En ella encontraréis actividades para hacer un adulto y un niño como una clase de equitación, un taller de galletas, una sesión de belleza, visita a la granja, o una aventura acuática, para que elijáis la que más os guste.

 ![](/tumblr_files/tumblr_inline_nzjoptlfsA1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_nzjoptsQw11qfhdaz_540.jpg)
## Requisitos para el sorteo

- Deja un comentario en esta entrada explicando qué os gustaría hacer juntos, más tu alias de fb o tw donde vayas a compartir el sorteo.  
- Sigue a kiddy´s box en Facebook o [twitter](https://twitter.com/kiddysbox)  
- Comparte el sorteo con el hastag #tiempoenfamilia en Facebook o Twitter  

El plazo finaliza el 29 diciembre, y el ganador se publicará en este post

El ámbito del sorteo es España 

Me reservo derecho de eliminar participaciones fraudulentas bajo mi criterio.

¿Te apuntas?