---
date: '2016-03-03T10:00:13+01:00'
image: /tumblr_files/tumblr_inline_o37vm23n1s1qfhdaz_540.jpg
layout: post
tags:
- trucos
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- familia
title: 'Libros infantiles: La Guerra de Sexto A'
tumblr_url: https://madrescabreadas.com/post/140380927932/libro-infantil-guerra-sexto
---

![](/tumblr_files/tumblr_inline_o37vm23n1s1qfhdaz_540.jpg)

Mi niña es bastante guerrera, así que sabía que este libro le iba a enganchar sí o sí.

Además, está en una edad parecida a la de los protagonistas, Inés, Álvaro, Joaquín, Hugo.. y al centrarse la historia en el ambiente escolar, sabía que iba a sentirse identificada enseguida con muchas de las situaciones que se cuentan en este libro tan divertido de la editorial [Alfaguara](http://www.megustaleer.com/editoriales/alfaguara/AL/) (una de mis favoritas desde que era niña), escrito por Sara Cano, en el que la autora parece haber retrocedido en el tiempo a su época escolar para adentrarnos en una clase sexto de Primaria (Sexto A), y en sus alumnos.

Tenemos todos los ingredientes: el grupo de las divinas, los frikis, el que siempre está en medio (llamado cariñosamente el “estorbo”), la típica profesora hueso, el profesor guay de gimnasia… Y la rivalidad con el otro grupo de sexto (sexto B), que llevará a los protagonistas a la más disparatada guerra por ganar una competición que se haya visto.

<iframe width="100%" height="315" src="https://www.youtube.com/embed/QlmwnCi78X0" frameborder="0" allowfullscreen></iframe>

Yo recuerdo que mi grupo de clase era el C, pero el más guay siempre era el A. La verdad es que cierta rivalidad siempre había entre los grupos, si bien no llegábamos a las situaciones disparatadas que se cuentan en el libro, sí que es cierto que me he sentido identificada en muchas de las situaciones, y sentimientos de los personajes.

Es curioso cómo plasma los sentimientos de Inés por el guaperas de la clase rival que, si bien no reconoce en público, sí que los siente, pero ni ella misma los comprende.

La afición a los videojuegos, los amores prohibidos, los grupos de whatsapp, los concursos de ciencias, los insufribles Test de Cooper, las profesoras que saben lo que estás pensando con solo echarte un vistazo y el sudor frío que resbala por la espalda cada vez que hay que salir a la pizarra se conjugan en esta primera entrega de una serie descacharrante y de plena actualidad perfecta para que los niños de a partir de 10 años descubran el placer de la lectura.

El libro incluye ilustraciones, diagrama y plan de acción para ganar la competición.

 ![](/tumblr_files/tumblr_inline_o37wnhSg3Q1qfhdaz_540.png)

La verdad es que es difícil dejar de leerlo porque, además de divertido, y de estar muy bien escrito, y con con un lenguaje actual (anque no me gustan las abreviaturas que se usan en Whathsapp), sabe mantenerte el interés por descubrir el final.

¿Y a tus hijos, les gusta leer?

Si te ha gustado, compártelo. No te cuesta nada, y a mí me harás feliz :)