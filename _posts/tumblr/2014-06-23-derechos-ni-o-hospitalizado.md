---
date: '2014-06-23T08:00:00+02:00'
layout: post
tags:
- mecabrea
- crianza
- derechos
- familia
- salud
- bebes
title: Derechos del niño hospitalizado en la práctica
tumblr_url: https://madrescabreadas.com/post/89631865736/derechos-ni-o-hospitalizado
---

Tras la última visita de mi Leopardito al Hospital, y mi consiguiente cabreo, y tal y como os prometí, os voy a hablar sobre sobre los derechos que tienen nustros hijos cuando entran en un hospital, ya sea para ser ingresados o para realizar cualquier prueba diagnóstica y, lo más importante, cómo hacerlos valer cuando es necesario.

**¿Qué es la Carta Europea de lo Derechos del Niño?**

[La Carta Europea de los Derechos del Niño hospitalizado](http://www.lecturafacil.net/media/resources/Carta_infants_hospitalitzats_cast_LF.pdf) de 1986 es un gran avance para que nuestros pequeños sean tratados en los hospitales por el personal como lo que son, niños. Porque ellos no dejan de serlo cuando cruzan la puerta de un centro hospitalario, por tanto, es el sistema el que tiene que adaptarse a ellos, y no viceversa.

Por ello en UNICEF España y la Sociedad Española de Pediatría Social se han comprometido a velar para que los hospitales y centros de salud incorporen los derechos de los niños en su trabajo de atención sanitaria, siguiendo los parámetros de la Carta Europea de Derechos del Niño Hospitalizado, creando la Red de Promoción de la Salud y Derechos de la Infancia en España (REPSDI) 

**¿Qué derechos tienen nuestros hijos en un hospital?**

Los Derechos que nuestros hijos tienen cuando son atendidos en un hospital son los siguientes:

**A)** Derecho del menor a que no se le hospitalice sino en el **caso de que no pueda recibir los cuidados necesarios en su casa o en un Centro de Salud** y si se coordinan oportunamente con el fin de que la hospitalización sea lo más breve y rápida posible.

**B)** Derecho del menor a la **hospitalización diurna** sin que ello suponga una carga económica adicional a los padres.

**C)** Derecho a **estar acompañado** de sus padres o de la persona que los sustituya el máximo de tiempo posible durante su permanencia en el hospital, no como espectadores pasivos sino como elementos activos de la vida hospitalaria, sin que eso comporte costes adicionales; el ejercicio de este derecho no debe perjudicar en modo alguno ni obstaculizar la aplicación de los tratamientos a los que hay que someter al menor.

**D)** Derecho del niño a recibir una **información adaptada a su edad** , su desarrollo mental, su estado afectivo y psicológico, con respecto al conjunto del tratamiento médico al que se le somete y a las perspectivas positivas que dicho tratamiento ofrece.

**E)** Derecho del niño a una recepción y seguimiento individuales destinándose en la medida de lo posible los **mismos enfermeros y auxiliares** para dicha recepción y los cuidados necesarios.

**F)** El derecho a **negarse** (por boca de sus padres o de la persona que los sustituya) como **sujetos de investigación** y a rechazar cualquier cuidado o examen cuyo propósito primordial sea educativo o informativo y no terapéutico.

**G)**  **Derecho de sus padres** o de las personas que los sustituya a recibir todas las **informaciones** relativas a la enfermedad y al bienestar del niño, siempre y cuando el derecho fundamental de éste al respecto de su intimidad no se vea afectado por ello.

**H)**  **Derecho de los padres** o de la persona que los sustituya a **expresar su conformidad con los tratamientos** que se aplican al niño.

**I) Derecho de los padres** o de la persona que los sustituya a una **recepción adecuada y a un seguimiento psicosocial** a cargo de personal con formación especializada.

**J)** Derecho a **no ser sometido a experiencias farmacológicas o terapéuticas.** Sólo los padres o la persona que los sustituya, debidamente advertidos de los riesgos y de las ventajas de es tos tratamientos, tendrán la posibilidad de conceder su autorización, así como de retirarla.

**K)** Derecho del niño hospitalizado, cuando esté sometido a experimentación terapéutica, a estar **protegido por la Declaración de Helsinki** de la Asamblea Médica Mundial y sus subsiguientes actualizaciones.

**L)** Derecho a no recibir tratamientos médicos inútiles y a **no soportar sufrimientos** físicos y morales que puedan evitarse.

**M** ) Derecho (y medios) de **contactar** con sus padres o con la persona que los sustituya, **en momentos de tensión**.

**N** ) Derecho a ser tratado con **tacto, educación y comprensión** y a que se respete su **intimidad**.

**O** ) Derecho a recibir, durante su permanencia en el hospital, los cuidados prodigados por un **personal cualificado** , que conozca perfectamente las necesidades de cada **grupo de edad** tanto en el plano físico como en el afectivo.

**P)** Derecho a ser hospitalizado **junto a otros niños** , evitan do todo lo posible su hospitalización entre adultos.

**Q)** Derecho a disponer de **locales amueblados y equipados** de modo que respondan a sus necesidades en materia de cuidados, de educación y de juegos, así como a las normas oficiales de seguridad.

**R** ) Derecho a proseguir su **formación escolar** durante su permanencia en el hospital, y a beneficiarse de las enseñanzas de los maestros y del material didáctico que las autoridades es colares pongan a su disposición, en particular en el caso de una hospitalización prolongada, con la condición de que dicha actividad no cause perjuicios a su bienestar y/o que no obstaculice los tratamientos que se siguen.

**S)** Derecho a disponer durante su permanencia en el hospital de **juguetes** adecuados a su edad, de libros y medios audiovisuales.

**T)** Derecho a poder **recibir estudios** en caso de hospitalización parcial (hospitalización diurna) o de convalecencia en su propio domicilio.

**U)** Derecho a la seguridad de **recibir los cuidados que necesita** -incluso en el caso de que fuese necesaria la intervención de la justicia- si los padres o la persona que los sustituya se los niega por razones religiosas, de retraso cultural, de prejuicios o no están en condiciones de dar los pasos oportunos para hacer frente a la urgencia.

**V)** Derecho del niño a la necesaria **ayuda económica y mora** l, así como **psicosocia** l, para ser sometido a exámenes y/o tratamientos que deban efectuarse necesariamente en el extranjero.

**W** ) Derecho de los padres o de la persona que los sustituya a **pedir la aplicación de la presente Carta** en el caso de que el niño tenga necesidad de hospitalización o de examen médico en países que no forman parte de la Comunidad Europea.”

** **

**Cómo exigir que se aplique esta Carta a nuestros hijos**

Se aplica a menores de 18 años de cualquier nacionalidad

La implementación de esta Resolución del Parlamento Europeo no está lograda del todo, ya que conlleva un cambio de mentalidad de muchos profesionales, requiere formación de personal, incluso en ocasiones, cambios en la estructura de los servicios de pediatría e instalaciones.

Es aquí donde está nuestra **labor como padres de informar al personal** que nos atienda de la existencia de esta resolución del Parlamento Europeo y exigir su cumplimiento.

Lo primero es informarnos en la **web del hospital** donde vayamos a llevar a nuestro hijo, en el apartado de guía o derechos y deberes del paciente o similar, si tiene recogida la Carta. Si es así, todo será más fácil porque se supone que el personal está familiarizado con la misma, aunque no debemos bajar la guardia y recordar si hiciera falta, que en la normativa del centro viene contemplada.

Si no es así, nos podemos **llevar una copia** por si fuera necesario mostrarla. Si vemos que se vulnera algún derecho del pequeño, primero de palabra, explicaremos que nos avala una norma internacional,  si no lo logramos, pediremos hablar con el **jefe del servicio** , y si fuera necesario, pondremos una queja por escrito ante el órgano o persona competente, quedándonos una copia de la misma sellada con la fecha del registro de entrada.

Por ejemplo, en la Comunidad de Madrid, han hecho suya esta Carta en algunos Hospitales como [El Hospital de La Paz](http://www.madrid.org/cs/Satellite?cid=1142400305990&language=es&pageid=1142399374814&pagename=HospitalLaPaz%2FHOSP_Contenido_FA%2FHPAZ_generico),  y el [Hospital Universitario Infantil Niño Jesús](http://www.madrid.org/cs/Satellite?cid=1142402220421&language=es&pagename=HospitalNinoJesus%2FPage%2FHNIJ_contenidoFinal), entre otros, en cuyas webs podremos encontrar sus derechos. 

¿Has tenido alguna experiencia en la que no se hayan respetado estos derechos?

Fotos gracias a:

[Geisinger](https://www.google.es/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&docid=3blq42oxZJW4_M&tbnid=029fGl5Xpol1qM:&ved=0CAQQjB0&url=http%3A%2F%2Fwww.geisinger.kramesonline.com%2FSpanish%2FHealthSheets%2F3%2CS%2C89312&ei=KH7ZUs-UDcjI0wWx24CgCQ&bvm=bv.59568121,d.d2k&psig=AFQjCNHTUybzOz4GA0n3Aw6epXXVaJi5oA&ust=1390071713492636)

C[recerfeliz](https://www.google.es/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&docid=-AkqsntC-FRQrM&tbnid=qPYBCjWBCpeX6M:&ved=0CAQQjB0&url=http%3A%2F%2Fwww.crecerfeliz.es%2FNinos%2FPsico%2FNinos-en-el-hospital&ei=E3zZUo6TMoOO0AXj_YHYBg&bvm=bv.59568121,d.d2k&psig=AFQjCNEr9OwkuKlooXc7o7iIT9JUYJhc6w&ust=1390071150536658)

365 Ideas de Negocio