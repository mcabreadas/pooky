---
date: '2017-05-24T16:43:55+02:00'
image: /tumblr_files/tumblr_inline_oqgn9fMC2M1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- mujer
- cosasmias
title: Mujer, 40 años busca... a sí misma
tumblr_url: https://madrescabreadas.com/post/161022426869/crisis-cuarenta-mujer
---

Cumplo 40 años y sin quererlo me acucia la necesidad de saber quién soy, en qué punto del camino me encuentro, qué he hecho hasta ahora, qué me queda por hacer, cómo y por qué.

Me da la sensación de que antes era una sola cosa cada vez, pero ahora soy tantas a la vez que no sé realmente quién soy, qué me queda de mi niñez, adolescencia, juventud, y qué es lo nuevo.

La [maternidad](/2017/01/24/maternidad-crianza-conciliacion.html) parece que ha hecho que olvide cosas de mí que poco a poco afloran ahora que mis tres hijos ya van siendo mayores y vuelvo a recuperar un poquito de mi espacio para pensar.

A menudo me pregunto qué hay de esa niña tímida e insegura, sensible, incapaz de encajar la maldad y la crueldad del mundo. Esa niña que se crió feliz en una familia que la adora, con unos padres que aún hoy se desviven por ella, y que poco a poco fue descubriendo los sabores y sin sabores de las amistades, los primeros desengaños vitales… cosas normales.

 ![](/tumblr_files/tumblr_inline_oqgn9fMC2M1qfhdaz_540.jpg)
## Las primeras veces

Recuerdo la primera vez que probé el chocolate. Fue mi [abuelo](/2013/10/14/remedio-casero-para-el-resfriado-el-ponche-del.html) quien me dio un Lacasito. Yo pensaba que se trataba de un caramelo normal, y cuál fue mi sorpresa cuando se derritió y noté en mi boca por primera vez ese intenso sabor del que aún no me he desenganchado.

Mi abuela me cuidaba. Era la mujer más buena del mundo. Siempre me atendía cuando estaba enferma. Siempre estaba.

También recuerdo la primera vez que sufrí de verdad. Fue cuando mi primera mejor amiga dejó de serlo. Recuerdo estar tumbada sobre mi cama intentando entender por qué y con esa sensación de profunda tristeza. Estaba empezando a aprender a vivir, pero yo no lo sabía.

 ![](/tumblr_files/tumblr_inline_oqgog2aUmc1qfhdaz_540.png)

Recuerdo mis juegos infantiles con mi hermana, y la primera vez que la vi, recién nacida; pensé que era una viejecita porque tenía las manos muy arrugadas.

La quise desde siempre, desde antes que naciera, desde antes de que existiera. No sentí celos jamás porque era mi regalo. Pensé que jamás volvería a aburrirme ni a estar sola.

Recuerdo lo bien que lo pasaba con mis primos en el pueblo. Lo feliz que me sentía y lo rápido que pasaban las horas con ellos. Fueron mis primeros amigos.

Una vez pusimos petardos dentro de una caca de perro para ver cómo explotaba, con eso te digo todo… una de tantas trastadas…

## Sueños de adolescente

Me pregunto qué queda de esa adolescente incapaz de encontrar su sitio, que luchaba por salir de su caparazón y vencer esa timidez que tan mal le hacía sentir, esa chica romántica, idealista y soñadora que ya empezaba a fantasear con que algún día se casaría y sería madre.

 ![](/tumblr_files/tumblr_inline_oqgnmuGL3Z1qfhdaz_540.png)

Claro, que en mis sueños todo era perfecto, no contaba yo con que [trabajar y tener familia](/2015/03/08/dia-mujer-conciliacion.html) sería un acto heroico tan difícil.

Uno de los años más felices de mi vida fue el único que pasé en el Instituto, donde me metí en el grupo de teatro, y fue una de las mejores experiencias de mi vida. También allí gané mi primer [premio de literatura](/2011/07/06/diario-de-una-adolescente-pensamientos-que-se.html), y descubrí con asombro que a la gente le parecía bueno lo que yo escribía.

## Nostalgia universitaria

Pero mi etapa universitaria fue la mejor. Seguramente la tengo idealizada en mi mente, pero es que, a pesar de los exámenes orales, los nervios y mi primer desengaño amoroso que me dejó traumatizada pensando en plan Nietzsche, “el amor ha muerto”, lo viví todo tan intensamente y con tantas ganas de comerme el mundo, que cuando la recuerdo no puedo evitar esa semi sonrisa nostálgica en mi cara.

Además, fue durante esos años cuando conocí al padre de mis hijos, aunque no lo supe hasta más tarde…

## Las primeras decisiones vitales

La etapa post universitaria la recuerdo llena de incertidumbres porque en poco tiempo tuve que tomar una serie de decisiones que marcarían mi vida para siempre, además en un momento en que todavía no sabía de la “Misa la media”, y tenía una imagen del mundo muy a mi manera.

Mi vocación jurista fue tardía pero convencida. De hecho mi bachillerato fue de ciencias puras, y mis compañeros me llamaron entre risas desertora cuando elegí estudiar Derecho.

Los 5 años de carrera me aportaron mucho. Gran parte de cómo soy ahora se lo debo para bien o para mal a ella. Creo que todo el mundo debería saber algo de Derecho para entender cómo funcionan las cosas y evitar que lo atropellen.

## Mi profesión

Mi camino profesional tampoco estuvo claro desde el principio, tras unos pinitos en la Universidad y la Escuela de Práctica Jurídica, me decidí por ser abogada, como Tom Cruise en Algunos Hombres Buenos . ajena a todas las dificultades que me esperaban para compaginar esta profesión con la maternidad.

 ![](/tumblr_files/tc12.gif)

Los primeros años viví la profesión con dedicación exclusiva y con pasión sin importarme el número de horas que pasaba en el despacho mientras consolidaba mi relación con el que sería mi marido, que siempre me apoyó en todo dejándose la piel igual que yo en mis proyectos.

## El amor

Recuerdo nuestra boda como uno de los días más felices de mi vida, a pesar de que cayó una tromba de agua de un cielo tan negro que a las 11:00 de la mañana parecía de noche. Pero ya se sabe, novia lluviosa, novia dichosa. Para mí todo fue perfecto, además, paró de llover justo a la entrada de la Iglesia y a la salida.

## La maternidad llegó a mi vida como una ola…

Poco después me convertí en madre dos veces, pasé por un [aborto](/2011/06/30/el-a%C5%84o-pasado-por-estas-fechas.html), un embarazo difícil que terminó con una [cesárea](/2013/05/23/tres-partos-tres-semana-del-parto-respetado.html) y con un bebé enfermo de [reflujo gastroesofágico](/2015/11/20/reflujo-bebes.html) que nos tuvo sin dormir más de 2 años. 

Ahora ya van siendo mayores, y me encuentro de sopetón a mí misma, pero con 40 años. Así, de repente, sin previo aviso. 

 ![](/tumblr_files/tumblr_inline_oqgohbWcLm1qfhdaz_540.png)

Y es cuando me pregunto qué ha pasado estos 10 últimos años, en qué hito del camino me encuentro, quién o qué soy: 

¿estudiante, profesional, madre, esposa, ama de casa, hija, amiga, bloguera globera como dicen en mi tierra…?

¿Me quieres explicar esto cómo se come?