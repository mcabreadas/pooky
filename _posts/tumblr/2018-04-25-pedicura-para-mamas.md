---
date: '2018-04-25T09:03:56+02:00'
image: /tumblr_files/tumblr_inline_p7p6eeiCp81qfhdaz_400.gif
layout: post
tags:
- familia
title: Pedicura para mamás a la carrera
tumblr_url: https://madrescabreadas.com/post/173285268339/pedicura-para-mamas
---

Las madres necesitamos buscar nuestro espacio sí o sí. Y a veces nos supone un gran esfuerzo porque tenemos que encajar muchas piezas hasta lograr sacar un ratito para nosotras mismas, pero no caigas en la tentación de desistir. Lo necesitas, y lo sabes.

Da igual que sea haciendo punto de cruz, tomando un café con amigas, en un SPA, o haciéndote tú misma la [pedicura panasonic](https%3A%2F%2Fwww.panasonic.com%2Fes%2Fconsumer%2Fcuidado-personal%2Fpedicura.html&t=ZDNkYzQ2ZjIzNWMzMjc3ODMzMWE4MTZmMjU1ZDE2NTk2N2I5ODVkZiwwNzU5NTQzZTExMTkwZDUzYzMyZjJkNzQ2YzgzZTk3MDdlOThlNTkx), pero tienes que lograrlo, no te olvides de ti!

Estos pequeños gestos hacia ti misma hacen que te quieras más, te sientas mejor, y vayas pisando fuerte por la vida.

Te cuento una cosa: jamás me había hecho la pedicura a pesar de tener los pies más terribles de la historia, ya que, como te he contado alguna vez, apoyo mal al andar y me duelen una barbaridad, por eso siempre llevo calzado cómodo en invierno, como te contaba en este post sobre [consejos para comprar calzado on line](http%3A%2F%2Fmadrescabreadas.com%2Fpost%2F171371817119%2Fcomprar-zapatos-online&t=ZjJkMjM1ZTZkNWQxOTU3ZjNiYTQxODNkOTcyMmE3NGU1OGQ4ZjMyYiw1OTkzNDBiOTRiMzMxYjY2NjY1ZmJjYjVmZjVmNTg1ZDQyYzdjMWE4), y en cuanto puedo me pongo las sandalias, para regocijo de mis pies.

Pero, quieta! Antes de desenfundar mis lindos piececitos tras todo el invierno a cubierto para calzarme unas [bonitas sandalias](https%3A%2F%2Famzn.to%2F2HmdIvL&t=NTFiYmE4YmFkNzAyNTJjNzA3NWY1NDUwN2IxMDNkMDBjMTM3M2I4ZCwzMjYyNjg0OGI3NDFlZWQ1ODFlOWJjNmU5ZDMwNjNmZjE5YzcxYTE1) he necesitado adecentarlos un poco y librarme de las terribles durezas, callosidades, uñas de mejillón y horrores varios. 

 ![pedicura spa](/tumblr_files/tumblr_inline_p7p6eeiCp81qfhdaz_400.gif)

Por es de justicia mimar nuestros pies y esta vez he querido agasajarlos con una pedicura profesional en vez de arreglármelos (oye, y mis lumbares me lo han agradecido).No sé tú, pero yo me paso el día andando de acá para allá, no sólo por mi trabajo, sino recogiendo a los niños del cole, llevándolos a extraescolares, al pediatra y mil cosas más que hacen que al final nos convirtamos en mujeres nómadas, que no paran en todo el día. 

Además, he probado el esmaltado pemanente porque cuando me las pinto yo con el normal, al final nunca tengo tiempo de quitarme el color cuando se estropea y acabo llevándolas desconchadas, lo que hace que sea peor el remedio que la enfermedad. 

 ![esmaltado permanente unas](/tumblr_files/tumblr_inline_p7opjikrwm1qfhdaz_1280.png)

Así que estoy encantada con mis pies que parecen recién estrenados, dispuesta a ir pisando fuerte para llegar a todo, y no descarto repetir la experiencia!

¿Cuándo fue la última vez que te mimaste? Sé sincera…

 ![mujer_salon_pedicura](/tumblr_files/tumblr_inline_p7opek45lC1qfhdaz_1280.png)