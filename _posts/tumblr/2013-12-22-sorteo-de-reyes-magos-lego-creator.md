---
date: '2013-12-22T20:49:00+01:00'
layout: post
tags:
- crianza
- trucos
- premios
- recomendaciones
- familia
- juguetes
title: 'Sorteo de Reyes Magos: LEGO CREATOR'
tumblr_url: https://madrescabreadas.com/post/70813126945/sorteo-de-reyes-magos-lego-creator
---

Ya han llegado las vacaciones de Navidad de los niños. Lo estaban deseando para descansar y pasarlo en grande con la familia en estas fiestas tan hogareñas. Tienen dos semanas por delante para disfrutar pero, como decía la abuela hace un par de semanas, lo mejor es planear estos días con cierta rutina y actividades atractivas ya que el aburrimiento es el peor enemigo de los más pequeños, más si tenemos en cuenta que no es bueno pasar de ir a tope de actividades a lo largo del trimestre, a de repente no tener nada que hacer en vacaciones. Esto no quiere decir que les hagamos seguir el mismo ritmo, sino que planeemos actividades relajadas de ocio para que lo pasen bien al mismo tiempo que descansan y disfrutan de la familia.

Para los que viváis en Madrid tenéis una **LEGO TECA** chulísima en el Centro Comercial H2O, de Rivas Vaciamadrid donde, si a vuestros niños les gustan las construcciones, van a alucinar con 300 m2 de juegos LEGO para todas las edades. Además el acceso es gratuíto.

También podéis visitar la **EXPOSICIÓN DE EDIFICIOS EMBLEMÁTICOS** construídos a escala con piezas LEGO que alberga el Colegio de Arquitectos de Madrid hasta el 4 de enero, en C/ Hortaleza, 63, en la que los niños,además, pueden participar en talleres de construcciones LEGO y sesiones de cine con la proyección de películas como LEGO Star Wars, todo ello también de forma gratuita. Podéis consultar el horario aquí.

Pero para los días en que os apetezca plan casero, hacerse con unos buenos juegos adecuados a la edad de cada uno es una muy buena opción para los días de lluvia o mucho frío en que decidáis no salir. Las construcciones son las favoritas de mi hijo de 5 años, Osito. Se puede pasar las horas muertas construyendo figuras de LEGO. Cuando era más pequeño se entretenía con LEGO DUPLO, pero ahora le encanta la caja de BUILD AND REBUILD, donde las piezas son más pequeñas y más variadas para poder construír vehículos y todo lo que se le ocurra. Aquí una muestra:

Tengo que tener mucho cuidado porque Leopardito, de 16 meses, siempre quiere coger lo que usa su hermano, pero estas piezas pueden ser peligrosas para él, por eso a él le pongo el LEGO DUPLO que usaba su hermano cuando era pequeño y, aunque todavía le cuesta encajar y desencajar las piezas, le encanta intentarlo y, con ayuda de su hermana mayor, Princesita, se lo pasa pipa haciendo torres o escaleras.

Pero para ella, el ideal es el LEGO Friends, con 5 muñequitas con las que vivir aventuras.

Para que os hagáis una idea de lo que pueden desarrollar vuestros hijos con este tipo de juegos, os dejo [este video](http://www.cjwho.com/post/70479335568/full-scale-model-hot-rod-powered-by-air-and-lego) de un chico que ha logrado construír un coche con piezas de LEGO. Y funciona!!!

¿Quién sabe si no tenemos en casa futuros arquitectos o ingenieros? Por eso LEGO me cede para sorteo 3 unidades de LEGO CREATOR 3 EN 1, para niños de entre 6 y 12 años.

**PARA PARTICIPAR SÓLO TENÉIS QUE HACER DOS COSAS** :

1-Escribir un comentario a este post explicando si vuestros hijos juegan con LEGO y, si es así, qué gama prefieren. No olvidéis especificar vuestro Nick de Facebook

2- Compartir la foto del sorteo en vuestro muro de Facebook con la opción de “público”

una publicación de Blog Madres cabreadas

El ámbito del sorteo es para el territorio español, y el plazo para participar finaliza el 4 de enero de 2014 a las 23:00 h, y el sorteo se llevará a cabo a través de [SORTEA2](https://www.sortea2.com/)

El ganador se publicará en este blog al día siguiente de finalizar el plazo, y tendrá 24 horas para facilitarnos sus datos.

Vamos, apúntate, que vienen los Reyes Magos! Hay 3 unidades esperándote!