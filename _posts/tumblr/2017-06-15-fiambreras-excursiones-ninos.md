---
date: '2017-06-15T09:00:30+02:00'
image: /tumblr_files/tumblr_inline_or69jfhFEc1qfhdaz_540.jpg
layout: post
tags:
- planninos
- 
title: 'Excursiones con niños: qué preparo'
tumblr_url: https://madrescabreadas.com/post/161844189170/fiambreras-excursiones-ninos
---

Este fin de semana vamos a pasar un día a la playa, y la verdad es que ya me sale casi solo el preparar todo lo necesario para que sea un día genial, pero recuerdo las primeras veces que íbamos de excursión con los niños, cuando los dos mayores aún eran bebés, y tardaba más en preparar todos los aparejos que necesitábamos que en la propia excursión (no te rías, que no exagero).

Es verdad que **nos daba mucha seguridad llevar un montón de cosas,** pero cuando estás cargando con los niños, los cochecitos, las mochilas, el cojín de la abuela, y ves que no llegas al sitio es cuando descubres que no hace falta llevar tantas cosas.

## Conservar correctamente la comida

Para mí, lo que no puede faltar son **utensilios que conserven agua y comida en perfecto estado**, sobre todo en esta época de calor, y tratándose de niños, para evitar posibles intoxicaciones.

[![](/images/uploads/packit-lunch-bag.jpg)](https://www.amazon.es/PackIt-Enfriador-personal-color-negro/dp/B0795XM9QC/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IYM2UI25H9G4&keywords=Freezable%2BLunch%2BBag&qid=1649507640&sprefix=freezable%2Blunch%2Bbag%2Caps%2C230&sr=8-3&th=1&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/PackIt-Enfriador-personal-color-negro/dp/B0795XM9QC/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IYM2UI25H9G4&keywords=Freezable%2BLunch%2BBag&qid=1649507640&sprefix=freezable%2Blunch%2Bbag%2Caps%2C230&sr=8-3&th=1&madrescabread-21)

Eso, y elegir un tipo de comida que sea adecuada más allá del típico y a veces aburrido bocadillo, evitando el huevo crudo o poco cuajado de las tortillas, por ejemplo, las salsas, ensaladilla rusa, etc., es fundamental.

Preparar la comida la noche antes, y meterla en la mochila justo antes de salir de casa en **bolsas térmicas para que conserven la temperatura** es un truco que utilizo siempre.

[![](/images/uploads/gloppie-bolsa-de-almuerzo-termica.jpg)](https://www.amazon.es/Gloppie-almuerzo-t%C3%A9rmica-acolchada-ajustable/dp/B0925V1SFS/ref=sr_1_28?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IYM2UI25H9G4&keywords=Freezable+Lunch+Bag&qid=1649507640&sprefix=freezable+lunch+bag%2Caps%2C230&sr=8-28&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Gloppie-almuerzo-t%C3%A9rmica-acolchada-ajustable/dp/B0925V1SFS/ref=sr_1_28?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3IYM2UI25H9G4&keywords=Freezable+Lunch+Bag&qid=1649507640&sprefix=freezable+lunch+bag%2Caps%2C230&sr=8-28&madrescabread-21)

Os recomiendo la web de MiBabyClub, donde podéis encontrar gran variedad de bolsas térmicas ideales para conservar todo tipo de alimentos, además de **fiambreras con diseños ideales** para el almuerzo de los peques, botellas de aluminio (la cantimplora de toda la vida pero en cool), y termos para las papillas de los bebés. No entres porque es monísimo y lo vas a querer todo.

## Agua fresquita y saludable

Huye de los refrescos y zumos azucarados y ten claro que **el agua es la mejor bebida para quitar la sed**. Y si está fresquita, mejor!

[![](/images/uploads/botella-de-agua-de-chilly-s.jpg)](https://www.amazon.es/Botella-Chillys-Inoxidable-Reutilizable-transpiraci%C3%B3n/dp/B07RXLG9RF/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=HKGL4028YS5P&keywords=Chilly%27s+Bottle&qid=1649508500&sprefix=freezable+lunch+bag%2Caps%2C876&sr=8-4&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Botella-Chillys-Inoxidable-Reutilizable-transpiraci%C3%B3n/dp/B07RXLG9RF/ref=sr_1_4?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=HKGL4028YS5P&keywords=Chilly%27s+Bottle&qid=1649508500&sprefix=freezable+lunch+bag%2Caps%2C876&sr=8-4&madrescabread-21)

Ya sabes que el plástico es un material poco eco-friendly, y si se recalienta puede llegar a contaminar el agua. Así que como alternativa os propongo estas [botellas](http://www.mibabyclub.com/catalogsearch/result/?q=chilly%0A) de aluminio forradas con silicona que, además de ser de lo más cuqui, ayudarán a mantener la temperatura del agua más tiempo y se pueden reutilizar sin problemas.

## Alternativas al triste típico bocata

El refrán dice que “a buen hambre no hay pan duro”, pero yo digo que si hay cositas ricas y apetecibles, pues mejor que mejor, sobre todo si vas con niños muy pequeños para bocadillo, o quieres comer de forma equilibrada.

Es cierto que el bocata es lo fácil, pero piénsalo, **así comeréis todos más sano,** y ya que vais a pasar un saludable día al aire libre y haciendo ejercicio, qué mejor colofón que una nutritiva y equilibrada comida.

Te doy unas ideas:

* Por ejemplo puedes preparar unos filetes empanados y cortarlos en trocitos para que vayan pinchando fácilmente, acompañados de unas tiras de zanahoria, pepino, tomatitos cherry y bolitas de queso mozzarela.
* El típico sándwich vegetal con pechuga de pavo, lechuga, tomate y queso o atún también es buena opción, pero **recomiendo llevar sobres de mayonesa aparte,** y ponerla justo cuando vayáis a comer.
* Una tortilla de trigo rellena con champiñones, bacon, huevo duro y queso rallado os la quitarán de las manos.
* Unos macarrones con atún, tomate, olivas y alcaparras son los favoritos de mis fieras.
* Ensalada de legumbres con lo que os guste
* Arroz cocido con tomate y salchichas
* Y de postre, aunque sea tentador, **huye de los dulces industriales y pon fruta en piezas enteras** tipo manzanas, mandarinas, peras, uva… o, si son pequeños, en trocitos : la sandía y el melón les encanta.

En ocasiones la solución para una buena bocata resulta tan fácil como preparar sencillos sándwich de jamón y queso aplastados. La acción del aplastado de las sandwicheras te permitirá llevar una buena cantidad de panes preparados colocados dentro del empaque donde vino originalmente al comprarlos. También, puedes introducir un **mayor número de sándwiches dentro de tu fiambrera** al estar más comprimido.

[![](/images/uploads/camry-cr-3023.jpg)](https://www.amazon.es/Camry-CR3023-Sandwichera-Plateado/dp/B00QD1VUYA/ref=sxin_14?asc_contentid=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&asc_contenttype=article&ascsubtag=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&creativeASIN=B00QD1VUYA&cv_ct_cx=sandwicheras&cv_ct_id=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&cv_ct_pg=search&cv_ct_we=asin&cv_ct_wn=osp-single-source-earns-comm&keywords=sandwicheras&linkCode=oas&pd_rd_i=B00QD1VUYA&pd_rd_r=6291b27a-3ee9-44c3-81a6-7100f1708762&pd_rd_w=jzpoF&pd_rd_wg=63iyM&pf_rd_p=4de2bc6a-87e5-44a4-bce4-96d6a8de741b&pf_rd_r=Q8WBRWEWP0GKHBXKQGX0&qid=1649509894&sprefix=sandw%2Caps%2C273&sr=1-2-61f4c597-7a45-4a97-bd75-c52903f8ee93&tag=amazon-urbantecno-21&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Camry-CR3023-Sandwichera-Plateado/dp/B00QD1VUYA/ref=sxin_14?asc_contentid=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&asc_contenttype=article&ascsubtag=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&creativeASIN=B00QD1VUYA&cv_ct_cx=sandwicheras&cv_ct_id=amzn1.osa.ef4ae4a2-14e9-4f1c-91ed-0d28f1380d98.A1RKKUPIHCS9HS.es_ES&cv_ct_pg=search&cv_ct_we=asin&cv_ct_wn=osp-single-source-earns-comm&keywords=sandwicheras&linkCode=oas&pd_rd_i=B00QD1VUYA&pd_rd_r=6291b27a-3ee9-44c3-81a6-7100f1708762&pd_rd_w=jzpoF&pd_rd_wg=63iyM&pf_rd_p=4de2bc6a-87e5-44a4-bce4-96d6a8de741b&pf_rd_r=Q8WBRWEWP0GKHBXKQGX0&qid=1649509894&sprefix=sandw%2Caps%2C273&sr=1-2-61f4c597-7a45-4a97-bd75-c52903f8ee93&tag=amazon-urbantecno-21&madrescabread-21)

Teniendo los envases adecuados ya no tienes problemas para llevar casi cualquier cosa cuando vayáis de excursión, evitando el huevo crudo de las tortillas poco cuajadas o mayonesas de ensaladillas rusas y demás, y alimentos más peligrosos de cara a una intoxicación.

Echa un vistazo a la gran variedad de fiambreras, bolsas térmicas y botellas de aluminio y demás envases que hay en [MiBabyClub](http://www.mibabyclub.com/), y se te acabarán las excusas para no comer sano fuera de casa.

¿Ya has pensado dónde va a ser vuestra próxima excursión?

Aquí te dejo unas recomendaciones de los [mejores sitios para viajar con bebés](https://madrescabreadas.com/2021/06/29/mejores-sitios-para-viajar-con-bebes/).