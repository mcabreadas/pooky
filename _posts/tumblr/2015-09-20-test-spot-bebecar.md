---
date: '2015-09-20T19:05:47+02:00'
image: /tumblr_files/tumblr_inline_o39zd5m1u51qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
- familia
- bebes
title: 'Testing silla Spot de Bébécar: Resultado'
tumblr_url: https://madrescabreadas.com/post/129502452789/test-spot-bebecar
---

Tras 5 meses de prueba de la [Spot de paseo de Bébécar](http://www.bebecar.com), os voy a contar nuestra experiencia, y si la recomendaríamos como silla de continuación, después de abandonar el cochecito de bebé, y hasta que sea grandecito.

Ya os contaba aquí [por qué elegí la silla de paseo Spot de Bebécar para mi hijo.](/2015/04/29/silla-paseo-grande.html) Os lo resumo:

## Por qué elegí Bébécar Spot

Hay sillas que, aún estando homologadas para bastante peso, no son lo suficientemente amplias o estables para aguantar la vitalidad y movimientos de un bebé grande, o los niños no se encuentran cómodos en ellas para dormir, por ejemplo, o se les sale la cabeza por arriba. 

Por eso, y para no volver a meter la pata, como lo hice con la silla de mi hijo mediano, busqué una silla con estas características concretas:

 ![](/tumblr_files/tumblr_inline_o39zd5m1u51qfhdaz_540.png)
- **Amplitud** : asiento ancho y altura de respaldo.

- **Comodidad** : para que aguanten largos caminos, si es necesario, y no quieran bajarse cada dos por tres.

- **Habitabilidad** : para que puedan cambiarse de posición, incluso dormir sin problemas 
 ![](/tumblr_files/tumblr_inline_o39zd5V1l41qfhdaz_540.png)
- **Fácil maniobrabilidad** : suspensión regulable según el peso del bebé/niño.

![image](/tumblr_files/tumblr_inline_o39zd5rSRd1qfhdaz_500.gif)

- **Manejabilidad** en barreras arquitectónicas: amortiguación en ruedas  

<iframe width="100%" height="300px" src="https://www.youtube.com/embed/jgxKHSgmJHE" frameborder="0" allowfullscreen></iframe>

- **Suave deslizamiento** : para poder manejarla sin esfuerzo, incluso por un niño  
 ![](/tumblr_files/tumblr_inline_o39zd6Tkni1qfhdaz_540.jpg)
- **Reclinación total** , porque sigue necesitando su siesta cuando pasamos el día fuera de casa.
- **Bloqueo de ruedas delanteras** sin agacharse: para moverla adelante y atrás y dormir al bebé.  
 ![](/tumblr_files/tumblr_inline_o39zd7kCgk1qfhdaz_540.jpg)
- **Gran capota extensible** : debido al clima soleado de mi ciudad  
 ![](/tumblr_files/tumblr_inline_o39zd78I5c1qfhdaz_540.png)
- **Plegado fácil y compacto en bastón** y con sistema de seguridad para evitar que los dedos de los niños queden atrapados.  

## Tras 5 meses de uso ¿ha cumplido mis expectativas?

Tras 5 meses usando la silla Spot de Bébécar estoy en condiciones de analizarla, según mi experiencia con un bebé/niño desde los 2 años y medio hasta los 3 años, y todavía la seguiré usando hasta el peso indicado por el fabricante de 20 Kg, porque su tamaño y resistencia lo permite.  

 ![](/tumblr_files/tumblr_inline_o39zd8ip9I1qfhdaz_540.jpg)
## Calidad de los materiales

Los materiales son resistentes y de la mejor calidad. 

 ![](/tumblr_files/tumblr_inline_o39zd86eoR1qfhdaz_540.png)
- Por ejemplo, las ruedas llevan **cojinetes de acero** , no de plástico, como la mayoría de las sillas, lo que hace que su duración sea muy larga.  
- No es muy pesada para la calidad de los materiales y su resistencia, **8,5Kg.**  
 ![](/tumblr_files/tumblr_inline_o39zd9utYI1qfhdaz_540.jpg)
- La **empuñadura** del manillar es ergonómica y antideslizante, lo que es importante, sobre todo cuando llueve, o si hace mucho calor y nos sudan las manos.  

## Limpieza

![image](/tumblr_files/tumblr_inline_o39zdbMjkI1qfhdaz_500.gif)

- Se puede elegir con el **tejido “Magic”** antimanchas que os expliqué cuando os hablé de los [cochecitos Hip Hop Tech](/2014/12/06/claves-eleccion-cochecito-bebe.html), así, os ahorraréis muchos disgustos y ratos de limpieza,   
- Pero si optáis por el **tejido de poliester** , como el de mi silla, con un cepillo, agua y jabón, se limpia fácilmente, y seca bastante rápido.  

## Apta para trote

- Tiene **gran estabilidad lateral** : difícilmente volcable, incluso por mis fieras. Mis hijos la han traqueteado, incluso se han subido a ella los mayores, y no se ha roto, así que dura es un rato.  

## Complementos

- Su **cobertor de lluvia** se adapta genial y nos ha salvado la vida en más de una ocasión.  
 ![](/tumblr_files/tumblr_inline_o39zdcDvFU1qfhdaz_540.jpg)
- En verano siempre la llevo con una funda ligera de algodón para que vaya fresquito, pero para el frío son ideales estos[sacos de abrigo impermeables](http://www.amazon.es/gp/product/B00M0O3M2C/ref=as_li_tf_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=B00M0O3M2C&linkCode=as2&tag=madrescabread-21) ![](/tumblr_files/tumblr_inline_o39zdcbW401qfhdaz_500.gif) .  
 ![](/tumblr_files/tumblr_inline_o39zddVzBF1qfhdaz_540.jpg)
- Se le puede adaptar un **portabiberones** muy práctico  
 ![](/tumblr_files/tumblr_inline_o39zde37uS1qfhdaz_540.jpg)
##   

## Colores y acabados

- Podéis elegir entre varios acabados del **chasis** de acero, todos ellos sin remaches para ofrecer un acabado limpio: mate, cromado, blanco o negro y combinarlos con los distintos colores de tapicería a vuestro gusto.  

- Y en cuanto el **tejido** , podéis optar por el “Magic” que repele las manchas, o el normal, como el que yo tengo.  

- En cuanto a **colores** , tenéis los clásicos azul y rosa pastel, beige claro, topo, azul marino, negro, tierra, blanco y gris claro  

## Lo que menos me ha gustado  

La verdad es que la silla es casi perfecta, pero podría mejorar si:

- La capota tuviera una **ventana transparente** para ver al niño mientras lo vas empujando. Aunque creo que en los modelos anteriores de la silla era así, en este último, al menos la mía no lo lleva.  
- La **cesta** de abajo es bastante amplia, pero tiene un acceso difícil para mi gusto.  

## Lo que más me ha gustado

Pero sin duda, lo que más me ha gustado, y a mi niño también, es que se reclina totalmente y se eleva el reposapiés. Esto se traduce en que puedo irme donde quiera a pasar el día, que mi peque hará sus siestas y él estará feliz, y nosotros tendremos libertad para ir donde queramos.

Tanto es así, que este verano nos llevamos la silla spot de viaje:

<iframe width="100%" height="300px" src="https://www.youtube.com/embed/wHa-cSaMuxA" frameborder="0" allowfullscreen></iframe>

## Conclusión

Tras 5 meses de prueba la recomiendo como silla de paseo definitiva para un uso prolongado por su seguridad, resistenca, comodidad, amplitud, manejabilidad y facilidad de transporte.

¿Y tú, qué buscas en una silla de paseo?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.