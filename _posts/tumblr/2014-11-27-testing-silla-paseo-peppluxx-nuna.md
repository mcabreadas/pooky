---
date: '2014-11-27T10:00:00+01:00'
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- familia
- bebes
title: Testing. Los nueve secretos de la silla de paseo Pepp luxx de nuna.
tumblr_url: https://madrescabreadas.com/post/103711664730/testing-silla-paseo-peppluxx-nuna
---

<iframe src="//www.youtube.com/embed/wvcQEa-lYQc" frameborder="0"></iframe>

Me siento una privilegiada por haber sido la primera mamá de España en probar en exclusiva el nuevo modelo de la premiada silla de paseo PEPP con el toque Luxx que conocí en la [Feria de Puericultura Madrid](/2014/10/03/puericultura-madrid.html) 2014, y que gané en un concurso organizado por [Nuna](http://www.nuna.eu/) y Madresfera a quienes agradezco la confianza depositada en mí y en este blog.

La primera impresión que tuve cuando llegó la Pepp luxx a casa fue la de estar ante una silla especial por su línea original.

Me pareció elegante y moderna.

La silla no llegará a España hasta enero de 2015, pero yo os voy a contar sus nueve secretos y nuestra experiencia tras 15 días de testing.

**Secreto número uno**

**Posición horizontal completa** , gracias a su respaldo regulable mediante cremalleras y reposapiés de 2 posiciones, que ha permitido a mi peque echar unas siestas estupendas, a pesar de que a mi niño se le quedaba un poco justa de tamaño al tener ya 27 meses.

**Secreto número dos**

**Barra protectora delantera** , que es extraíble por si queremos lavar la funda que trae, pero con la suficiente dificultad para que los bebés mayorcitos no la quiten a su antojo (lo digo por el mío, que es un terremoto).

**Secreto número tres**

Ruedas más grandes para un deslizamiento aún más suave, lo que hace que sea de muy ** fácil manejo** , incluso con una mano, porque no exige ningún esfuerzo empujarla, ni siquiera en los giros. Esto lo he notado mucho porque tenía las muñecas destrozadas de llevar mi anterior silla porque mi bebé ya pesa bastante, y con la Pepp luxx he notado un gran alivio.

**Secreto número cuatro**

**Capota extensible y extragrande** que ofrece una mayor protección solar, con dream tape incluida que es una pasada, y el secreto de las largas siestas de mi Leopardito, que me permiten hacer la compra.

**Secreto número cinco**

Mayor protección para la piel del bebé y bienestar general con **tejidos certificados por Oeko-Tex**. A pesar de ello, me ha resultado un poco áspera al tacto.

La limpieza no la he probado aún, pero en las instrucciones del fabricante indica lavar normal y secar colgada.

**Secreto número seis**

**Plegado** todavía más plano y **en un solo paso** en libro. La verdad es que siempre he preferido las sillas que se pliegan en paraguas porque ocupan menos en el coche, pero la facilidad con que se pliega ésta y el hecho de que una  vez plegada se sostenga sola en posición vertical me hace cuestionármelo.

**Secreto número siete**

**Cesta inferior de mayor tamaño** que la anterior versión.

No obstante, para mí que tengo tres niños, me falta sitio por todos lados, y me he tenido que mentalizar de que la silla no es un carro de la compra porque, además, no se puede colgar nada en el manillar por el riesgo de que se venza hacia atrás.

**Secreto número ocho**

**Manillar de altura ajustable**. Esto es fundamental para las mamás altas que comparten silla con abuelas más bajitas o incluso con hermanos mayores del bebé.

**Secreto número nueve**

**Bloqueo del giro en ruedas delanteras**. Esto es importante para mí porque siempre he dormido a mis peques moviéndoles el carrito hacia adelante y hacia atrás, con lo que si las ruedas delanteras no se quedaran fijas sería mucho más trabajosa la operación siesta.

En general me parece una silla de paseo estéticamente con mucho estilo, cómoda de manejar y práctica porque cumple su función de silla sencilla para bebés mayores, a la que se le suman detalles que la completan y nos facilitan la vida. Mis favoritos, la capota extensible y la horizontalidad total.

Pero shhhhh…!! Guardadme el secreto, porque hasta enero no llegará a España

Tenéis gran variedad de colores para elegir, estos son mis favoritos:

Silla Pepp Luxx 2015 Scarlet

Silla Pepp Luxx 2015 Blackberry

NUNA - Silla de Paseo Nuna Pepp Luxx gris