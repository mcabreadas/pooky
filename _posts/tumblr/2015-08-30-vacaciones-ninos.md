---
layout: post
title: Vacaciones con niños en Urueña, Galicia y Baiona
date: 2015-08-30T18:00:34+02:00
image: /tumblr_files/tumblr_inline_o39zgjULWm1qfhdaz_540.png
author: .
tags:
  - planninos
tumblr_url: https://madrescabreadas.com/post/127950386037/vacaciones-ninos
---
Este verano ha sido muy loco porque es de los primeros que nos vamos como familia numerosa de vacaciones.

Hemos tenido que [adaptar nuestras viejas costumbres vacacioneras a las nuevas circunstancias](/2015/06/28/viajes-ninos.html). 

El hecho de ser cinco, por un lado te limita, pero por otro, te abre todo un mundo de nuevas posibilidades que antes ni nos planteábamos.

Por ejemplo, hemos empezado a alojarnos con [Airbnb](https://www.airbnb.es/c/msanchez1919?s=8), en lugar de en hoteles, ya que no hay habitaciones para cinco personas, hemos cambiado el avión por los largos viajes por carretera en nuestro coche, los restaurantes a diario por los divertidos picnics y la nevera, y hemos aprendido a buscar en cada lugar que visitamos, actividades específicas para niños.

¡Os aseguro, que así también se pasa divinamente!

## En Urueña, la Villa del Libro

El viajar por carretera con niños te permite ir parando a menudo en multitud de sitios, y descubrir lugares interesantes.

Por ejemplo, quedé gratamente sorprendida con Urueña, donde pasamos una tarde y una noche de camino a Galicia, nuestro destino.

Doce librerías y doscientos habitantes. 

 ![](/tumblr_files/tumblr_inline_o39zgjULWm1qfhdaz_540.png)

Museo de Miguel Delibes, Museo del Cuento, un taller de encuadernación tradicional… 

 ![](/tumblr_files/tumblr_inline_o39zgju2eP1qfhdaz_540.png)

Y su increíble muralla desde la que ver la puesta del sol. 

 ![](/tumblr_files/tumblr_inline_o39zgkuusn1qfhdaz_540.png)

Calles empedradas, y muros que te cuentan cuentos mientras escuchas el zumbido de las abejas. 

 ![](/tumblr_files/tumblr_inline_o39zgl6dV71qfhdaz_540.png)

Buena gente, y buena mesa. ¿Qué más se puede pedir?

## En Galicia lo dimos todo

Al no ir de hotel, y tener que alojarnos en apartamento, tienes que hacer las tareas domésticas tú mismo, peeeeero hay formas y formas de tomárselo.

Es cierto que no son unas vacaciones de tumbarte a la bartola y que te lo hagan todo, pero ya nos llegará la hora de viajar con el IMSERSO jajajajaja…

Repartimos las tareas, compramos vajilla de usar y tirar, hicimos compra de alimentos prácticos para no tener que cocinar demasiado y abusamos de las empanadas gallegas que, al ser tan variadas no nos dio tiempo a probarlas todas en los días que estuvimos allí.

Estuvimos en la zona de Sanxenxo disfrutando de sus increíbles playas, y también hicimos excursiones a Santiago, Valença do Minho (Portugal), Baiona…

También nos acercamos a Vigo, donde vimos la [película del verano, Inside Out (Del Revés) de Disney Pixar](/2015/07/22/insideout-pixar-reves.html).

 ![](/tumblr_files/tumblr_inline_o39zglywMv1qfhdaz_540.png)

En Galicia lo pasamos así de bien:

<iframe width="100" height="56" src="https://www.youtube.com/embed/wHa-cSaMuxA?feature=oembed" frameborder="0" allowfullscreen></iframe>

## De aventuras en Baiona

Sentirse conquistadores de tierras lejanas visitando la carabela Pinta, aprender trozos de nuestra historia en la Casa de la Navegación o vislumbrar el puerto desde su muralla son algunas de las actividades de las que podréis disfrutar con los niños en Baiona. Merece la pena organizar una excursión con los peques y pasar el día, os aseguro que van a disfrutar.

 ![](/tumblr_files/tumblr_inline_o39zgli5ZN1qfhdaz_540.png)

Si tuviera que definir en una palabra nuestras vacaciones, diría que han sido intensas… intensamente familiares jajajaja…

Ahora en serio, no creo que mis hijos las olviden nunca.

¿Cómo de locas han sido las tuyas?

NOTA: Si quieres conocer más a fondo la silla de paseo que nos ha acompañado en nuestras vacaciones, puedes ver este post donde os contaba las características de la [silla Spot de Bébécar](/2015/04/29/silla-paseo-grande.html).

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.