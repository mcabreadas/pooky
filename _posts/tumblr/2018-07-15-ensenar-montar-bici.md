---
date: '2018-07-15T19:31:52+02:00'
image: /tumblr_files/tumblr_inline_pbx4r5tapz1qfhdaz_540.gif
layout: post
tags:
- crianza
- juguetes
- planninos
- familia
title: Cómo enseñar a los niños a montar en bici
tumblr_url: https://madrescabreadas.com/post/175920815554/ensenar-montar-bici
---

¿Método tradicional o bicicleta sin pedales? Ante el interés que ha suscitado en redes el tema de enseñar a los niños a montar en bicicleta y cuál es la mejor forma de que aprendan sin presiones, pero motivándolos y dándoles seguridad parque pierdan el miedo, he decidido recopilar las sugerencias y consejos basados en vuestra experiencia que me habéis estado dando porque pienso que entre todas tenemos una valiosa información que puede servir a otras madres.

Por lo que veo hay dos grupos de corrientes en esto de enseñar a montar en bici: el de toda la vida, donde se emplean ruedines y cuando se quitan, un adulto va sujetando la parte de atrás de la bici corriendo tras ella hasta que intuye que el niño ya guarda el equilibrio y lo suelta, y el método más innovador de la bicicleta sin pedales. 

## El método tradicional
 ![](/tumblr_files/tumblr_inline_pbx4r3wFOY1qfhdaz_540.gif)

Es por el que yo aprendí, tú aprendiste y la mayoría de nuestra generación gracias a los riñones de nuestros padres, madres y abuelos. En mi caso fue gracias a la paciencia de mi padre y abuelo porque tardé lo mío en aprender.

Este método es el que estamos empleando este verano con mi hijo pequeño, pero tuvimos que echar marcha atrás porque se negó a ir sin ruedines.

Por mi experiencia, y por la de la mayoría de vosotras, este sistema es bastante pesado, y requiere de grandes dosis de paciencia y riñones.

Es mejor empezar, cuanto más pequeños mejor porque tienen menos miedo, pesan menos y son más manejables para nosotras.

Una buena idea para ir soltándolos es practicar sobre una superficie mullida, como puede ser el césped, donde el peque se sienta seguro si se cae y no coja miedo.

Otro truco es que la bici sea más bien pequeña, para que lleguen mejor al suelo y tengan sensación de controlar la situación.

Es importante respetar su ritmo y nunca forzar ni presionar, aunque nos desesperemos muchas veces (la paciencia no es precisamente mi virtud). Si ven a más niños a su alrededor sin ruedines, acabarán pidiendo ellos mismos que se los quitemos.

## La bicicleta sin pedales

Parece que se ha comprobado que el mejor método para que aprendan de forma progresiva y natural a mantener el equilibro es que comiencen desde pequeños a montar una [bicicleta sin pedales](https://amzn.to/2mg5KXY), ya que el paso a una bicicleta con pedales será más fácil, y evitaremos usar los ruedines, que tan difícil nos resultarán de quitar.

En [este post os presenté la novedad de bicicleta sin pedales de Chicco](/2015/11/13/primavera-moda.html).

Las bicicletas sin pedales también les ayudarán a conseguir mantenerse encima mediante un trabajo conjunto de sus extremidades inferiores y superiores a base de coordinación para conseguir no caerse, frenar, girar…

De este modo desarrollarán su sistema psicomotor y les ayudará a tonificares músculos porque el hecho de que no tengan pedales les obligará a tener que hacer uso de los músculos de sus piernas y de sus brazos para conducirlas, para mantener el equilibrio, saber girar cuando es necesario, afrontar pendientes…

Estas bicicletas sin pedales son más seguras porque no se puede alcanzar con ellas una gran velocidad, y tampoco cuentan con una gran altura. Además de que en el caso de caídas, no se clavarán los pedales ni se golpearán con ellos al manipularlas para subirse o bajarse.

Nosotros le compramos esta [bicicleta sin pedales](https://amzn.to/2mg5KXY) al pequeño, pero no hizo mucho uso de ella, yo creo que por vivir en un piso y coincidir con el invierno. Ahora me arrepiento porque le está costando deshacerse de los ruedines.

Os dejo este video del primer día que la usó por el pasillo de casa. Quizá era demasiado pequeño, pero con constancia, lo hubiera logrado. 

¿No os parece?

<iframe width="560" height="315" src="https://www.youtube.com/embed/-PoRE_kZ63k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Por nuestra parte, nosotros seguimos intentándolo porque estamos convencidos de que de este verano no pasa de liberarnos por fin de los ruidosos ruedines.

![](/tumblr_files/tumblr_inline_pbx4r5tAPz1qfhdaz_540.gif)

¿Cómo enseñaste a tus hijos a montar en bici?

_Gracias a Lucy, Andrea, Sai, Carol, Meritxell, Sara, Trasteadora, María del Mar, y todas las que habéis contribuido con vuestros consejos a la elaboración de este post para ayudar a otras madres. _