---
layout: post
title: Rollos de mistela para Navidad
date: 2013-12-18T22:00:00+01:00
image: /images/uploads/tumblr_my062x2q731qgfaqto10_1280.jpg
author: .
tags:
  - navidad
  - recetas
tumblr_url: https://madrescabreadas.com/post/70418929371/haz-tus-propios-dulces-de-navidad-rollos-de
---
![](/tumblr_files/tumblr_my062x2Q731qgfaqto1_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto2_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto3_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto4_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto5_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto6_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto7_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto8_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto9_1280.jpg) ![](/tumblr_files/tumblr_my062x2Q731qgfaqto10_1280.jpg)  

Esta mañana fui al cole de mis hijos a hacer en la clase de Osito rollos de mistela, típicos de estas fechas navideñas con todos los niños. Es una receta muy fácil que podéis hacer en casa toda la familia porque para los niños es como hacer plastilina, pero con mesa, y luego les hace una ilusión tremenda verlos terminados y comerse algo que han hecho con sus propias manos.

Pero lo que más ilusión le ha hecho a mi niño es verme entrar con el resto de madres y padres colaboradores, ya que no le había dicho nada para darle una sorpresa. Su cara de emoción durante toda la mañana de verme allí en su clase ha sido el mejor premio que me he podido llevar hoy.

Es curioso con qué poco se puede hacer feliz a un niño.

Comparto la receta y os dejo las fotos de cada paso. Ya veréis qué sencillo es y qué ricos salen. Y si sirve para compartir un rato en familia, aún os sabrá mejor.

## Ingredientes rollos de mistela

\-1 L de aceite de maíz

\-2 Kg de harina

\-1/2 L de mistela

\-azúcar glass al gusto

## Modo de hecer los rollos de mistela

Se echa la harina en un recipiente grande, se añade el aceite y la mistela y se amasa hasta que quede uniforme.

A continuación se hacen bolitas pequeñas, se rulan sobre la mesa hasta conseguir alargarlas, y se unen los dos extremos para hacer el rollito.

Luego se colocan en bandejas de horno cubiertas con papel de horno, se cuece a ojo, y cuando se sacan se espolvorean con el azúcar glass.

![](/tumblr_files/tumblr_my06cyyoHp1qgfaqto1_250.jpg)

![](/tumblr_files/tumblr_my06cyyoHp1qgfaqto2_250.jpg)

![](/tumblr_files/tumblr_my08oc5s9Y1qgfaqto1_250.jpg)

En el cole de vuestros hijos hacen también actividades compartidas con papás y mamás?