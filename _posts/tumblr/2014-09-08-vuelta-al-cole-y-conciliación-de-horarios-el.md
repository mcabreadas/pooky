---
date: '2014-09-08T10:00:25+02:00'
image: /tumblr_files/tumblr_inline_pgauzmstjW1qfhdaz_540.jpg
layout: post
tags:
- mecabrea
- conciliacion
title: Vuelta al cole y conciliación de horarios. El drama de muchas familias.
tumblr_url: https://madrescabreadas.com/post/96957502904/vuelta-al-cole-y-conciliación-de-horarios-el
---

Hoy me pongo seria. Seria y cabreada porque no se escucha hablar de otra cosa en este inicio de curso. Septiembre se ha convertido en un mes dramático para las familias que intentan compaginar los horarios escolares con los de los padres para lograr la quimera de la conciliación.

Y es que, en España la conciliación laboral y familiar no es una prioridad para los políticos, que están más ocupados intentando salir de una crisis que ellos mismos han provocado. Total, si no hay trabajo, y la mitad de la población está en paro, ¿para qué necesitamos la conciliación? Los que no trabajan (la mayoría mujeres) que cuiden de los niños, ancianos y dependientes, y ya está. Solucionado. No?

Pues no, señores. Esto no es así. Luego correrán, ya veréis. Cuando no haya jóvenes en activo suficientes para mantener a los dependientes, cuando la tasa de nacimiento decaiga más aún, y España se convierta en el país con la población más vieja de Europa, entonces se les ocurrirá de repente preocuparse por la conciliación y la protección de la maternidad.

Actualmente la tasa de fecundidad está por los suelos porque, además de que hay pocas mujeres en edad fértil y otros factores, según las [estadísticas del INE](http://www.seg-social.es/prdi00/groups/public/documents/binario/162456.pdf), las pocas que hay no se atreven a ser madres, o lo son a una edad muy tardía porque tienen que pagar la hipoteca y se arriesgan a que las echen del trabajo o las degraden en cuanto se les note el bombo. O a que cuando vuelvan a su puesto les hayan comido la merienda, o porque no van a tener suficiente sueldo como para pagar una guardería 8 horas, o una nanny, y se verán obligadas a dejar su carrera profesional.

¿De verdad creéis que no ayudarían unas buenas y certeras medidas de conciliación a aumentar la natalidad?  

¿De verdad no os parece urgente solucionar esto?

Si no, mirad esta gráfica de Fundspeople.com  que os pondrá los pelos de punta: 

 ![image](/tumblr_files/tumblr_inline_pgauzmstjW1qfhdaz_540.jpg)

Además, según el INE, se prevé que en un futuro inmediato el número de muertes supere al de nacimientos.

¿Van a esperar a entonces para tomar medidas? Pues yo creo que ya vamos tarde.

Así que, señores políticos, dejen de decir que la conciliación no es un tema prioritario porque estamos en crisis, porque, una crisis económica se superará tarde o temprano  pero una crisis demográfica… eso sí que sería una tragedia.