---
date: '2015-12-26T10:08:16+01:00'
image: /tumblr_files/tumblr_inline_o39yu1Xbci1qfhdaz_540.jpg
layout: post
tags:
- navidad
- crianza
- trucos
- regalos
- colaboraciones
- gadgets
- ad
- tecnologia
- familia
- juguetes
title: Guía AIJU para elegir el juguete adecuado para mis hijos
tumblr_url: https://madrescabreadas.com/post/135964966534/guia-aiju-juguetes
---

¿Qué criterios sigues para elegir los juguetes de Reyes para tus hijos? 

¿Te lo has planteado alguna vez? 

¿La publicidad? 

¿La moda?

¿O te basas en algún criterio educativo o de calidad?

Ante la avalancha de juguetes que presenciamos cada Navidad por todos los medios habidos y por haber es difícil decidir qué juguete es el más adecuado para nuestros hijos, más allá de sus preferencias. 

Por eso me ha parecido interesante esta aplicación para móvil con la [Guía de juguetes de AIJU ](http://www.guiaaiju.com/2015/landingapp/), Instituto Tecnológico especializado en juguete, producto infantil y ocio, ubicado en  España, que está formado por un equipo multidisciplinar con más de 70 expertos en seguridad infantil, psicología, educación…

Creo que no debemos tomarnos a la ligera este tema, ya que el juego es un aspecto muy importante en el desarrollo de nuestros hijos.

 ![](/tumblr_files/tumblr_inline_o39yu1Xbci1qfhdaz_540.jpg)

[AIJU](http://www.guiaaiju.com/2015/quienes-somos.php) es una entidad sin ánimo de lucro, fundada en 1985, cuya labor principal es potenciar la investigación, la seguridad y la calidad en todos los sectores de producto infantil y ocio.

Desde hace 25 años, edita esta Guía con el fin de ofrecernos información detallada y útil sobre productos de puericultura, juegos y juguetes de calidad, que resulten adecuados a las necesidades psicopedagógicas y lúdicas de los niños según las edades.

Te recomiendo que si todavía no has comprado los juguetes de Reyes te [descargues la aplicación aquí](http://www.guiaaiju.com/2015/landingapp/).

O, si lo prefieres, te puedes descargar la guí completa en pdf [aquí](http://www.guiaaiju.com/2015/guia_completa.php).

Y decidas con criterios serios basados en las conclusiones de expertos, y no sólo tengas en cuenta los anuncios de la tele o los catálogos de juguetes.

Estas Navidades no te la juegues, y ve más allá a la hora de elegir los juguetes.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.