---
date: '2015-09-18T20:35:01+02:00'
image: /tumblr_files/tumblr_inline_o39zdkaCu21qfhdaz_540.jpg
layout: post
tags:
- trucos
- adolescencia
- decoracion
- planninos
- familia
title: Decoración World of Warcraft (WOW) para fiestas de cumpleaños
tumblr_url: https://madrescabreadas.com/post/129362905319/fiesta-warcraft-wow
---

En mi familia tenemos un punto friki, y nos gustan las fiestas temáticas, ya lo sabéis. Pero esta vez nos hemos superado con una sobre el juego de moda en casa, el [World of Warcraft (WOW)](http://eu.battle.net/wow/es/). 

Creo que es la más rara que he preparado nunca, sobre todo porque al principio me sonaba a chino todo lo que me iba contando mi hijo, y no sabía por dónde empezar ya que existen cantidad de mundos, personajes, escenarios…, pero luego le fui tomando el gustillo, y me quedó así de bien (no seáis muy críticos, por favor):

## La tarta

Todo cumpleaños debe tener una tarta. Esta vez la hice de queso, la cubrí con mermelada de fresa, y le puse el símbolo de la Horda con fideos de colorines. Me ayudó esta [plantilla en varios tamaños con el símbolo de la Horda](http://blog.localkinegrinds.com/wp-content/uploads/2010/08/for_the_horde.pdf) que encontré en este blog.

 ![](/tumblr_files/tumblr_inline_o39zdkaCu21qfhdaz_540.jpg)

Viéndolo ahora, quizá hubiera quedado mejor con cacao.

## Los disfraces WOW

Cualquier ocasión es buena para disfrazarnos, y como vamos acumulando disfraces a lo largo de los años, ahora casi no compramos nada porque vamos adaptando lo que tenemos. 

La mayor se vistió de Elfa de la Sangre con un traje de una función de ballet, una peluca larga y rubia, y un brazalete. 

 ![](/tumblr_files/tumblr_inline_o39zdkK8nl1qfhdaz_540.jpg)

Yo me vestí de india del poblado de los Taurem porque siempre me ha gustado hacer el indio.

Pero sin duda lo que más triunfó fue la capa de brujo con el símbolo de la Horda que hice con la capa de Halloween del año pasado, fieltro rojo pegamento de tela. 

Usé la misma plantilla que para la tarta para el símbolo, puse un borde rojo en la capucha y listos. Así de resultona quedó.

 ![](/tumblr_files/tumblr_inline_o39zdliFUk1qfhdaz_540.jpg)

## El escudo de la Horda

El escudo también fue sencillo, ya que customicé uno de mi hijo mediano que compré en Eureka Kids hace tiempo.

Cartulina roja, rotulador negro y cinta de doble cara tuvieron la culpa de esto:

 ![](/tumblr_files/tumblr_inline_o39zdl8Cyf1qfhdaz_540.jpg)

## Las piedras del hogar

En el juego sirven para volver a casa desde el punto donde estés, por eso mi hijo decidió que no podían faltar. 

También fueron sencillas, con cartulina marrón y azul celeste.

## Los globos de la Horda

Se nota de qué facción somos en casa, ¿no?

Simplemente pintamos sobre globos rojos con rotulador de tinta indeleble guiándonos con la plantilla.

 ![](/tumblr_files/tumblr_inline_o39zdmfoGu1qfhdaz_540.jpg)\>

¿A que molan?

## El poblado Taurem

Y, por último, el rincón donde más disfruté, el poblado Taurem. 

Yo en mi otra vida debí de ser india, porque me lo pasé pipa montándolo. Sólo me faltó [hacer un Teepee, pero os dejo este enlace](http://www.lachicadelacasadecaramelo.com/2013/05/diy-tipi-indio-para-ninos.html) por si lo queréis hacer vosotros.

 ![](/tumblr_files/tumblr_inline_o39zdm9IYd1qfhdaz_540.jpg) ![](/tumblr_files/tumblr_inline_o39zdmZlhy1qfhdaz_540.jpg)

Bueno, creo que no me dejo nada.

¿Conocíais el WOW?

¿Os ha gustado?

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.