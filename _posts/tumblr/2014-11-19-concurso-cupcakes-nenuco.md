---
date: '2014-11-19T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petodaIQ1O1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- premios
- colaboraciones
- ad
- recomendaciones
- familia
- juguetes
title: Concurso "Un cupcake ideal para tu Nenuco"
tumblr_url: https://madrescabreadas.com/post/103030062608/concurso-cupcakes-nenuco
---

El mejor regalo que podemos hacer a nuestros hijos es pasar tiempo con ellos, aunque a veces las madres de hoy en día, vamos tan desbordadas que nos cuesta hacer cosas con ellos. Por eso os propongo que hagáis un hueco para participar en el concurso organizado por Nenuco, ideal para compartir un ratito con los peques de lo más dulce, además de poder ganar un premio que bien merece la pena.

 ![image](/tumblr_files/tumblr_inline_petodaIQ1O1qfhdaz_540.jpg)

El concurso se lanza con motivo del nuevo juguete de Famosa para estas Navidades, la “Tienda de Cupcakes” de Nenuco, recomendado a partir de 4 años, y pensado para desarrollar la imaginación, la sociabilidad y las habilidades manuales de los niños.

¿Os gustan los cupcakes, esas magdalenas cubiertas de crema de infinitos colores y sabores, adornadas de forma tan bonita que da hasta pena comérselas?

 ![image](/tumblr_files/tumblr_inline_petoda7SbE1qfhdaz_540.jpg)

Pues Nenuco os reta a que hagáis junto con vuestro hijo o hija el cupcake ideal para su muñeco Nenuco. Preguntaos cómo sería el cupcake ideal para él y poneos manos a la obra!

Un consejo: podéis preguntar a los niños por los colores o adornos que creen que le gustarían a su muñeco, seguro que os dan ideas chulísimas y originales… aunque al principio os parezcan una locura.

El premio para los 6 mejores cupcakes consiste en un taller de repostería para las mamá y los peques ganadores impartido por la mismísima [Alma Obregón](http://almascupcakes.es/), quien ha introducido en gran parte en nuestro país el arte de estas exquisitas magdalenas, y a quien seguro conoceréis por su libro, su blog, y por la tele.

 ![image](/tumblr_files/tumblr_inline_petodaqUhH1qfhdaz_540.jpg)

**Para participar**

Entra** al [perfil de Facebook](https://www.facebook.com/nenucofamosa.es/app_154581087931912)**[de Nenuco](https://www.facebook.com/nenucofamosa.es/app_154581087931912) de Famosa y **sube la foto y la receta de un cupcake** hecho por ti y tu hija/o y que creáis que representa a la marca Nenuco. Así de fácil!

Tenéis hasta el 9 de diciembre!!

¿Aceptáis el reto Nenuco Cupcake?

 ![image](/tumblr_files/tumblr_inline_petodbMK6J1qfhdaz_540.jpg)