---
date: '2014-11-24T10:00:00+01:00'
image: /tumblr_files/tumblr_inline_petod94WKB1qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- premios
- colaboraciones
- puericultura
- ad
- familia
- bebes
title: 'V sorteo de aniversario: mochila de bebé de Monstruitos'
tumblr_url: https://madrescabreadas.com/post/103450189370/v-sorteo-de-aniversario-mochila-de-bebé-de
---

![image](/tumblr_files/tumblr_inline_petod94WKB1qfhdaz_540.png)

ACTUALIZACIÓN 8-12-14

Por fin tengo el resultado del sorteo. Gracias por vuestra paciencia,pero ha sido un aniversario agotador con tanto regalo. No os quejaréis eh??

 ![](/tumblr_files/tumblr_inline_petodaD1Oz1qfhdaz_540.jpg)

Enhorabuena, Cristina Zamora! A mí también me encanta la roja de topos. Es una monada! Por favor, pásame tus datos por privado para el envío.

Post originario:

Mis amigas de monstruítos.es quieren celebrar también el [cuarto aniversario](/portada-aniversario) con nosotros porque son super fans del blog. Tanto que fuimos el primer blog en ser elegido blog del mes en sus redes sociales, lo que me hizo especial ilusión.

Fue entonces cuando descubrí las cositas que venden on line en su tienda monstruosa, y son verdaderamente originales. Me encantaron sus camisetas de monstruos, los babis para guardería, de bebés y profes, y ahora han sacado unos gorritos de crochet que son para comérselos.

Pero lo que realmente me enamoró son sus mochilitas para bebés hechas a mano porque se nota el cariño que les ponen, así que no me pude resistir y le regalé una a Leopardito de calaveras. Os lo imagináis?

 ![image](/tumblr_files/tumblr_inline_petodalUjk1qfhdaz_540.jpg)

Le hizo tanta ilusión ponerse  su mochila imitando a sus hermanos cuando van al cole, que no paraba de saltar y correr por la calle. Se sentía mayor, y hasta quería meterse en clase con ellos!

Tienen varios modelos y, no creáis, me costó decidirme.

 ![image](/tumblr_files/tumblr_inline_petodbmbFM1qfhdaz_540.jpg)

Las veo prácticas incluso para el cole en la etapa de educación infantil porque los peques necesitan algo para llevar el almuerzo y el agua solamente, y no suelen llevar deberes ni carpetas, al menos los míos. Las prefiero a la bolsita de bocadillo porque se la pueden colgar en los hombros y se les quedan las manos libres. 

 ![image](/tumblr_files/tumblr_inline_petodbZXxp1qfhdaz_540.jpg)

En cuanto al material con que están hechas, son de algodón de calidad y la confección es fuerte, y son bastante amplias. 

Compensa comprarla para guardería y educación infantil, de manera que yo creo que hasta los 6 años se pueden aprovechar perfectamente, o sea, hasta que empiezan primaria, que ya necesitan mochila grande para libros, carpetas, libretas…

No me digáis que no os las llevaríais todas?

Además, en Monstruitos intentan colaborar con fundaciones que tengan una labor social para porque les gusta poner su granito de arena para mejorar las cosas. Por ejemplo, las mochilas están hechas por [Fundació Ared](http://www.fundacioared.org/), que trabaja con mujeres en riesgo de exclusión social.

Para participar:

-Dejar un comentario en este post diciendo la mochila que elegiríais caso de resultar ganadores (según disponibilidad), y vuestro nick de Facebook o Twitter.

-Suscribiros al boletín de monstruitos.es

-Seguir a Monstruítos en Facebook y Twitter

Y si queréis, aunque no es obligatorio, pero así estaréis al día del resultado del sorteo y de los próximos que vaya sacando a lo largo del mes, podéis suscribiros al blog. Para ello sólo tenéis que introducir vuestro email en el margen derecho.

El sorteo es válido para territorio español, y el plazo acabaría el 30 de noviembre a las 00:00 h.

¿Cuál quieres?