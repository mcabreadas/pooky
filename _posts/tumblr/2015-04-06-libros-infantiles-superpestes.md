---
date: '2015-04-06T12:44:20+02:00'
image: /tumblr_files/tumblr_inline_nmdmva30fD1qfhdaz_540.jpg
layout: post
tags:
- crianza
- trucos
- libros
- colaboraciones
- planninos
- ad
- recomendaciones
- educacion
- familia
title: 'Libros para niños: Colección Sami Superpestes'
tumblr_url: https://madrescabreadas.com/post/115658028784/libros-infantiles-superpestes
---

Sí, sí, “[Sami Superpestes](http://www.boolino.es/es/libros/coleccion/samy-superpestes/)”, éste es el nombre del niño super detective al que os recomiendo que se enganchen vuestros hijos a partir de los 9 años porque les va a divertir, no sólo por su nombre, que ya dice mucho de él (es una rata), y del lugar donde vive (una cloaca), sino por lo ingenioso que es y por todas las aventuras que vive con su tío el capitán Ratas, acompañado de su mano derecha, la cucaracha Cuca a bordo su barco que surca las aguas fétidas de la alcantarilla, “El Viejo Tallarín”.

Tenéis títulos como “[Sami Superpestes contra los piratas de la cloaca](http://www.boolino.es/es/libros-cuentos/samy-superpestes-contra-los-piratas-de-la-cloaca/)”, y “[Sami Superpestes héroe de Alcantarilla](http://www.boolino.es/es/libros-cuentos/samy-superpestes-heroe-de-alcantarilla/)”, ambos súper divertidos de leer!

 ![image](/tumblr_files/tumblr_inline_nmdmva30fD1qfhdaz_540.jpg)

Historias de Piratas, que no se sabe muy bien si lo son, secuestros, robos de valiosas colecciones de chapas y mapas sospechosamente modificados inundarán la imaginación de vuestros peques quienes, irán deduciendo por ellos mismos la resolución de los casos con ayuda de los dibujos, esquemas y croquis que incluye cada historia permitiendo que los jóvenes lectores se hagan una idea gráfica del misterio que ha de resolver Sami Superpestes, y se adelanten a él en su resolución, lo que les fomentará el pensamiento lógico y la habilidad para resolver problemas.

Los recomiendo por su originalidad y su capacidad para captar la atención de los chavales porque, no os engañemos, están en una edad en que lo escatológico les divierte mucho.