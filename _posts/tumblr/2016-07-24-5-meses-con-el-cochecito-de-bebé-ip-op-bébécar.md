---
date: '2016-07-24T19:01:42+02:00'
image: /tumblr_files/tumblr_inline_oat2zyievF1qfhdaz_540.png
layout: post
tags:
- crianza
- trucos
- colaboraciones
- puericultura
- ad
- recomendaciones
- testing
title: 5 meses con el cochecito de bebé Ip Op Bébécar
tumblr_url: https://madrescabreadas.com/post/147900575730/5-meses-con-el-cochecito-de-bebé-ip-op-bébécar
---

Mi sobrina Chiquita ya está muy grande, y próximamente la pasaremos a la silla porque es muy curiosa y sólo quiere mirar a su alrededor cuando pasea, además, el calor arrecia por estos lares y, sin duda, irá más fresquita en ella.

Pero antes de abandonar el capazo (¡Qué rápido crecen!), su madre quería hacer una valoración de su experiencia con el Ip Op XL de Bébécar para contártelo:

## Movilidad y diseño

Llevo algo más de cinco meses con mi carrito Bebecar y qué voy a decir… ¡estoy contentísima con él! Mi niña va muy cómoda dentro del capazo, las ruedas funcionan de maravilla y le dan una movilidad al carro que me tiene sorprendida, y encima tiene estilazo. ¿Qué más se le puede pedir?

 ![](/tumblr_files/tumblr_inline_oat2zyievF1qfhdaz_540.png)

Después de probar unos meses mi carrito Bebecar, puedo decir que ha sido un gran acierto. Me lo llevo a todos sitios sin miedo, porque me es comodísimo, y a mi hija ya ni os cuento.

El kit lleva el capazo, la silla y el sistema de retención para automóvil grupo 0.

 ![](/tumblr_files/tumblr_inline_oat2zzz9Q21qfhdaz_540.png)
## Giro con una sola mano

Lo primero que destacaría es su movilidad. Las ruedas giran a la perfección. Con una sola mano, puedo hacer girar el carro sobre sí mismo. Aunque parezca una tontería, a mí me ha venido fenomenal, porque la entrada al portal de mi edificio no está precisamente preparada para carritos de bebés (ni para sillas de ruedas, etc.), estoy obligada a hacer dos giros de 90º en un espacio muy reducido y el que las ruedas respondan tan bien se agradece infinito.

## Posibilidad de ruedas fijas

Aparte tiene la posibilidad de ponerlas fijas, de forma que el carro se mueva hacia delante y hacia atrás, lo que me viene muy bien cuando he necesitado dormir a mi niña en el capazo. Al principio no me aclaraba mucho con la forma de ponerlas fijas, pero el truco lo descubrí pronto: después de tocar los botones oportunos, debía hacer una “S” con el carro y así se fijaban inmediatamente.

 ![](/tumblr_files/tumblr_inline_oat2zzbtGy1qfhdaz_540.png)
## ¿Es práctico el capazo XL?

Otra cosa que me gusta mucho del carro es tener un capazo XL. Qué queréis que os diga… habrá gente que piense que es muy incómodo, pero lo cierto es que yo no he notado más dificultad que con carros más pequeños para meterme en sitios como ascensores o para dejar el carro al lado de mi silla en una terraza, y desde luego mi hija ha ganado en comodidad.

 ![](/tumblr_files/tumblr_inline_oat2zzjYp71qfhdaz_540.png)

Tiene mucho más espacio, y eso me está permitiendo alargar un poco la vida del capazo, es decir no haberlo cambiado todavía por la silla. Sobre todo porque todavía veo a mi hija pequeña (por edad) para sentarla ya (prefiero esperar un poco, aunque la silla esté homologada desde los 0 meses), pero está grande de tamaño para los meses que tiene. Un capazo pequeño me obligaría a utilizarlo menos meses de lo que yo quería y haberla pasado ya a la silla.

## Capota con ventilación

Me gusta mucho también que la capota que tiene este carro tiene una zona de rejilla para ventilar el interior, que se puede usar cuando hace más calor. Y una visera por delante, para quitar sol y luz. Creedme si os digo que en una ciudad como Murcia es algo utilísimo. 

 ![](/tumblr_files/tumblr_inline_oat3007Ge31qfhdaz_540.png)

También puede levantarse la solapa de la cubierta para dejar al bebé totalmente protegido del frío.

Plegar y desplegar la capota me costó al principio, porque hasta que no cedió un poco la piel, me costaba, y es verdad que es algo complicado; si intentas hacerlo con una mano ocupada (porque tienes a tu bebé en el otro brazo, por ejemplo). Ese es quizá un pequeño inconveniente que le veo.

 ![](/tumblr_files/tumblr_inline_oat300kCBF1qfhdaz_540.png)
## El peso

Me habían dicho de este carro que una pega que tiene es el peso: al parecer, pesa algo más que los demás. Yo no lo he apreciado, y eso que era algo que me preocupaba un poco, porque el sitio donde yo trabajo es un pueblo lleno de cuestas, y tener que tirar de un carro pesado no me apetecía mucho, la verdad. 

 ![](/tumblr_files/tumblr_inline_oat301fZx81qfhdaz_540.png)

Pero lo cierto es que no lo he visto tan pesado como pensaba, y en cualquier caso, si el peso es la contrapartida para que mi hija vaya más cómoda y segura, ya que da una estabilidad al carro enorme… pues bienvenido sea, al menos en mi caso.

## La suspensión

También donde yo trabajo hay mucho suelo empedrado y quería un carro con una buena suspensión, que cuidara por tanto de la espalda y el bienestar de mi pequeña. Y creo que lo he conseguido.

 ![](/tumblr_files/tumblr_inline_oat301RfJT1qfhdaz_540.png)
## El plegado

Plegar y desplegar el carro es una gozada, se hace con comodidad y es bastante intuitivo. Sobre todo desplegarlo, plegarlo cuesta algo más como pasa con cualquier carro. Ahí, al plegar y meter el carro en el coche, es cuando más noto el tema del peso, pero como os he comentado, para mí es un mal menor porque creo que es algo que sufres con cualquier carrito, y desde luego, mi prioridad es la seguridad y estabilidad del carro.

## Gran cesta

Y si hay algo que me ha sorprendido también, y para bien, es la cesta que lleva el carro. Cuando veía fotos (incluso la primera vez que vi los carros por la calle) tenía la impresión de que la capacidad de la cesta era pequeña, que no cabía nada. Y os sorprendería saber todo lo que he llegado a meter dentro. Paquetes de pañales, una botella de agua, una mochila con ropa de la niña, una carpeta con cosas del trabajo, mi bolso… Impresionante. 

 ![](/tumblr_files/tumblr_inline_oat301SYto1qfhdaz_540.png)

De todas formas, lo que sí he echado de menos es un gancho o algo parecido para poder colgar ahí el bolso, porque es verdad que dejarlo en la cesta te obliga a estar agachándote, y yo al menos cuando voy con el carrito no me gusta llevarlo encima (y lo dejo en la cesta). Además, eso me permitiría ampliar la capacidad de cosas que puedes llevar en el carro, que siempre viene bien.

## Fácil limpieza

Y en cuanto al lavado… Muy cierto lo que me dijeron: un poco de agua y jabón neutro, y solucionado. Y eso que mi carro es de ecopiel…

## Balance positivo

En fin, y en resumen, que estoy muy contenta con el carro. Me lo he llevado al monte, a la playa, de turismo rural, lo he metido en aglomeraciones, en ascensores que parecían más pequeños que él, lo he bajado por escaleras… y me ha respondido de lujo. De momento he utilizado el capazo y el grupo 0 para el coche y estoy muy contenta con el resultado. Enseguida empezaré a probar la silla. ¡Ya os contaré!