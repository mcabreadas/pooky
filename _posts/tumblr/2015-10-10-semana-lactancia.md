---
date: '2015-10-10T09:26:06+02:00'
image: /tumblr_files/tumblr_inline_o39zajfFv81qfhdaz_540.jpg
layout: post
tags:
- crianza
- iniciativas
- lactancia
- familia
- nutricion
- colecho
- bebes
title: Lactancia materna. Toda la vida y más
tumblr_url: https://madrescabreadas.com/post/130865588414/semana-lactancia
---

Del 4 al 10 de octubre se celebra la semana europea de la lactancia materna, y como todos los años, he querido participar compartiendo un trocito de nosotros. Aunque mi pequeñín tiene 3 años y hace tiempo que toma leche de vaca, todavía quedan reminiscencias de aquella época.

Yo ya no amamanto, pero sí que disfruto cada día de la huella que esos 14 meses dejaron en mí, en ti…en nosotros.  
Esa necesidad primaria de tocarnos, de sentirnos, de olernos, de dormir acurrucados es algo que no podemos negarnos el uno al otro.  
Esas siestas en las que  cerramos juntos los ojos y nuestros corazones se vuelven a acompasar uno sobre el otro.   
En las que respiramos al unísono y buscas mi piel para acariciar como antes, para calmarte y abandonarte sobre mí al sueño más placentero que podamos soñar.

 ![](/tumblr_files/tumblr_inline_o39zajfFv81qfhdaz_540.jpg)

Es nuestro momento, y los dos lo buscamos cada día, y sé que lo extrañas si no llega porque yo no me encuentro si me falta.  
Es nuestro vínculo indisoluble, el que tendremos siempre, el que irá cambiando y evolucionando con nosotros, el que nos acompañará toda la vida… y más.

Si te ha gustado compártelo, no te cuesta nada, y a mí me harás feliz.