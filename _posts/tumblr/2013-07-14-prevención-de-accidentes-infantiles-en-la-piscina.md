---
layout: post
title: Prevención de accidentes infantiles en la piscina
date: 2013-07-14T12:01:00+02:00
image: /tumblr_files/tumblr_mpvew6nGo31qgfaqto1_1280.jpg
author: .
tags:
  - trucos
  - 6-a-12
  - 3-a-6
tumblr_url: https://madrescabreadas.com/post/55413820214/prevención-de-accidentes-infantiles-en-la-piscina
---
Hace mucho calor y la piscina nos salva la vida en verano. Imaginaos tres niños encerrados en un piso sin poder desfogarse… se me ponen los pelos como escarpias de pensarlo!

Pero este año estamos teniendo algunos percances. Caídas, raspaduras… nada de cuidado, pero que nos han hecho tener que poner unas normas para evitar accidentes.

Son pocas pero muy claras e inquebrantables porque la experiencia dice que cuantas más se pongan menos se cumplen, por no dar la importancia que merecen a las esenciales.

Para hacer partícipes a los niños, y para que las interioricen y comprendan su importancia, mi madre tuvo la brillante idea de que las establecieran ellos mismos y las plasmaran en unos dibujos a modo de señales para colocarlos en la piscina:

## Prohibido correr por el bordillo

![](/tumblr_files/tumblr_mpvew6nGo31qgfaqto1_1280.jpg) 

## Prohibido hacer aguadillas (o ahogadillas, como prefiráis)

![](/tumblr_files/tumblr_mpvew6nGo31qgfaqto2_1280.jpg) 

## Usar protección solar

![](/tumblr_files/tumblr_mpvew6nGo31qgfaqto3_1280.jpg) 

La sorpresa fue cuando me pusieron a mí una norma: Prohibido que mamá hable con el móvil mientras nos estemos bañando (vamos, que les preste atención a ellos en vez de andar llamando por teléfono).

![](/tumblr_files/tumblr_mpvew6nGo31qgfaqto4_1280.jpg)  

Así que más claro, agua.

Y tú? Qué normas pones a tus hijos cuando se bañan en una piscina?