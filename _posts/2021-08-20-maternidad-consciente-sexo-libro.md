---
layout: post
slug: maternidad-consciente-sexo-libro
title: Lo que nadie me dijo sobre la maternidad (y el sexo)
date: 2021-08-24T08:17:48.877Z
image: /images/uploads/screen-shot-2021-04-22-at-11.55.56.png
author: maria
tags:
  - invitado
---
*Actualización 28 abril de 2022*

Los éxitos de nuestras madres cabreadas son nuestros éxitos, así que he querido actualizar este post para felicitar a Isabel por las ventas de su libro que vino a presentar a este humilde blog, y le quedo un articulo tan bueno, que lo usó de [prólogo para las siguientes ediciones de Lo que nadie me dijo sobre la maternidad y el sexo](https://isabeldenavasques.com/2022/01/21/prologo-a-las-nuevas-impresiones-de-lo-que-nadie-me-dijo-sobre-la-maternidad-y-el-sexo/), que le deseo sean muchas.



Hoy merendamos con la escritora [Isabel de Navasqüés y de Urquijo](https://isabeldenavasques.com) [@isabeldenavasques](https://www.instagram.com/isabeldenavasques/), quien compagina la escritura con su trabajo como *Health Coach* y madre. Es autora de *La Profecía de Gaia*, primer libro de la serie de literatura fantástica: *Las Fabulosas Aventuras de Kiso Maravillas*.

Isabel es madre de dos hijas muy seguidas y el encontronazo que se llevó con la maternidad fue el origen de *Lo que nadie me dijo sobre la maternidad (y el sexo)*, su segunda publicación, libro el que vamos a hablar y que os recomiendo desde ya para desmitificar la maternidad y comprender muchas de los sentimientos encontrados que sufrimos las madres, y de os que casi nadie habla con tanta honestidad y transparencia. 

En la actualidad se encuentra escribiendo una novela urbana ambientada en el Madrid de finales de S.XX.

![](/images/uploads/isaferia4.jpeg)

Te dejo con ella y con su brutal experiencia cn la maternidad:

> Parir, criar y educar a mis hijas está siendo el acontecimiento más bestia que me haya sucedido

*Lo que nadie me dijo sobre la maternidad (y el sexo)* fue un libro que escribí por necesidad personal, un desahogo catártico, yo soy escritora y los escritores escriben. Escribimos sobre lo que vemos, oímos, sobre lo que pensamos, sentimos, sobre lo que nos pasa, nos conmueve, lo que nos sorprende, lo que nos hiere… 

Creo que gestar, parir, criar y educar a mis hijas está siendo **el acontecimiento más bestia que me haya sucedido**, ¿cómo no iba a escribir sobre ello? Parte del choque cultural que sufrí se debió a que no comprendía cómo siendo el hecho más disruptivo de mi existencia podía, además, ser tan desconocido para mí, se me planteó una duda inmensa: 

> ¿Cómo es posible saber tan poco acerca de un proceso tan viejo como la vida misma?

Una vez publicado el libro, conocí el concepto de matrescencia. **La matrescencia es el período de adaptación** físico, emocional, social, familiar… que sufrimos las mujeres al convertirnos en madres.

![Lo que nadie me dijo sobre la maternidad (y el sexo)](/images/uploads/lo-que-nadie-me-dijo-sobre-la-maternidad-y-el-sexo-.jpg "Lo que nadie me dijo sobre la maternidad (y el sexo)")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/8418238135/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=41d066dcb0fccf8f5455cfd107020e23&creativeASIN=8418238135) (enlace afiliado)

Nada más saberlo sentí un alivio inmenso. 

La matrescencia es un concepto forjado desde los años 70, lo que quiere decir que no soy la única ni la primera mujer que ha sufrido un *shock* al convertirse en madre. Este libro habla de la mía, de mi experiencia personal durante el embarazo, el parto, el puerperio y los primeros años de crianza de dos hijas que se llevan dieciocho meses entre sí. 

Al sentarme a recopilar mis apuntes sobre el tema y plantearme la estructura del libro, contar los hechos en orden cronológico me pareció lo más normal, y, así, se me planteó también la necesidad de hablar sobre lo que viene justo antes del embarazo que curiosamente es otro de los tabús de nuestra sociedad: el sexo. 

> A pesar de las cejas que iba a levantar, a mí me parece lo más natural del mundo enlazar sexo y maternidad, ¿a ti no?

Este es un libro coyuntural, que habla de un momento de mi vida muy concreto pero muy profundo, donde he vivido unas situaciones que me han cambiado para siempre, a mí la maternidad me atropelló como si fuera el fin del mundo porque literalmente lo fue, el mundo del que yo procedía se acabó y dio comiendo a una nueva era y como tras cualquier cataclismo, necesité de un período de adaptación para poder encajarlo. Creo que hoy, a los cinco años de nacer mi hija pequeña, puedo decir que ya me he adaptado y que ya **no cambiaría mi realidad por nada.**

> La maternidad me atropelló como si fuera el fin del mundo

El relato de nuestra maternidad es una historia sin fin, que crece y se desarrolla al ritmo de nuestros hijos y de nuestra madurez como madres, yo para este libro puse un tope, contar los tres primeros años de crianza de mis hijas, que para mí han sido los más duros, porque **adaptarme a esta nueva realidad de madre me ha costado un poco**, y me cuesta, es una transición dónde se va asentando y conformando una nueva yo. 

![madre con movil y dos ninos](/images/uploads/vitolda-klein-l8oeiaz59_g-unsplash.jpg)

Yo no crecí con un deseo imperioso de casarme, ni de tener hijos, ni de formar una familia convencional, ni de hacer la compra, ni programar menús, ni nada parecido. Todavía añoro y siento un duelo (a veces desgarrador) por mi antiguo ser y mi antigua vida, todavía no tengo claro si estaba preparada para esta metamorfosis, pero esta es mi nueva persona, soy una madre y solo las madres sabemos todo lo que de nosotras se ha trasferido a nuestros hijos. 

> Yo no crecí con un deseo imperioso de \[...] hacer la compra, ni programar menús, ni nada parecido.

Ser madre te cambia la vida para siempre, ¿para mejor? ¿para peor? Hay momentos para todo, para pensar que estabas mejor antes y para agradecer cada segundo de lo que tienes ahora, la única verdad suprema es que **ser madre no te va a dejar indiferente**, no vas a poder hacer borrón y cuenta nueva si no te encaja la experiencia. 

No puedes escapar de ello como de un novio que no te conviene, tenlo muy en cuenta antes de lanzarte a obedecer *la llamada de la selva* porque, tal vez, tu “mono de niño” se pueda suplir con ser “tía dedicada” de sus sobrinos. Aclarar este punto sobre ti misma te podrá ahorrar mucho sufrimiento.

Esto hay que saberlo, has de saber que como mujer tienes muchas más salidas que “acabar de madre”, y lo digo porque puede existir una vida en la que te quedas como estabas, no sufres la crisis que la maternidad produce y **te dedicas el resto de la vida a desarrollarte dentro del individualismo** que impera en nuestra sociedad, sin crisis personal, choque cultural ni ruptura socio-familiar.

>  como mujer tienes muchas más salidas que “acabar de madre”

Desde luego que mi vida sería otra sino hubiera tenido hijos, hijas en este caso, dos, pero no puedo decir que me arrepienta de haberlo hecho. Lo hecho, hecho está y en este campo no se admiten devoluciones… este fue uno de los motivos que me llevó a publicar este libro.

Debemos ser padres conscientes y maternar a nuestros hijos en presencia, porque traer una vida a este mundo es un acto de generosidad inmenso que acarrea una responsabilidad igual de grande, una que **te compromete para toda la vida** y de la que no vas a poder desentender cuando te hartes, *y te vas a hartar*. Esto conviene saberlo, no se oye lo suficiente, te crees que la única que estaba *harta* era tu madre porque estaba *loca*. 

![mujer con edredon y cafe](/images/uploads/laura-chouette-ia4ivhz6gfk-unsplash.jpg)

Pero no, tu madre no es un bicho raro, tu madre se ponía como una hiena y te tiraba la zapatilla porque la crianza acaba con la paciencia de cualquiera y esto lo tienes que saber. El anuncio de navidades *del corteinglés* no existe, lo que existe es **mucho esfuerzo, mucha dedicación y mucha intensidad**. Lo que existe es un sistema establecido que no te va a premiar, ni a apoyar, ni a acompañar, sino que te va a poner la zancadilla por tu gesto desprendido de traer vida a este mundo y ocuparte responsablemente de ella.

> tu madre se ponía como una hiena y te tiraba la zapatilla porque la crianza acaba con la paciencia de cualquiera

Vivimos en una sociedad desagradecida y sumamente contradictoria que pretende que produzcas y te reproduzcas, que críes en tus ratos libres (si eso), el resto del tiempo el bebé crece en una elipsis temporal fundida a negro como en las películas, mientras tú  te mantienes contenta, delgada y equilibrada, capaz de sostener tu matrimonio, tu trabajo y tu persona…

Han pasado unos años desde que terminé de escribir este libro, muchas de las cosas que cuento ya se me han olvidado, otras ahora no las siento de modo tan intenso y, tal vez, ya no las compartiría o no del mismo modo en el que han quedado plasmadas en mi libro; ciertas fases de la maternidad como pueden ser la soledad, la dependencia, la vulnerabilidad, la sensibilidad extrema o el miedo, **son emociones que transitan, cambian y evolucionan.** Lo que no quiere decir que sean menos reales o que no se repitan en momentos puntuales, pero tal vez, no se habla en abierto de ellas por esto, porque se desdibujan con el tiempo. 

La verdad es que yo hasta que no lo he vivido en primera persona no he sabido de qué iba el tema y me parece a mí que es porque no nos lo han dejado claro, así que para mí escribir *Lo que nadie me dijo sobre la maternidad (y el sexo)* ha sido un ejercicio muy natural porque **necesitaba compartir con pelos y señales mi desgarro, mi frustración y mi perplejidad ante la injusticia social y biológica que ser madre en este mundo representa**.

¡Espero que os guste!

*¿Quieres merendar con nosotras?*

*Queremos daros voz en este blog para que se os oiga y podáis así ayudar a otras familias, contar vuestra experiencia para servir de inspiración a otras mujeres, vuestro proyecto, vuestra historia de superación personal o contar al mundo vuestra causa.*

*[Pincha aqui para participar en el blog](https://madrescabreadas.com/invitados)*

Photo by Vitolda Klein on Unsplash

Photo by Laura Chouette on Unsplash