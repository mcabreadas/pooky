---
layout: post
slug: recopilacion-nuestras-invitadas-seccion-posts-invitados
title: Un recorrido por nuestras invitadas del blog
date: 2022-01-07T11:31:19.353Z
image: /images/uploads/istockphoto-1011756218-612x612-1-.jpg
author: faby
tags:
  - invitado
---
Una de las novedades de 2021 de las que mas nos sentimos orgullosas es la creación de la sección de invitadas, en la que vosotras sois las protagonistas.

Hemos tenido la suerte de tener participantes inspiradoras, luchadoras, empoderadas y reveladoras.

También, emprendedoras que han burlado la conciliación familiar para sacar adelante sus proyectos y han recibido el apoyo de toda la comunidad

En esta entrada, hablaremos de cómo nos fue con nuestros invitados de este año. ¡Empecemos!

## Voces que fueron escuchadas en la seccion “invitados”

En esta sección pudimos **ver el mundo maternal desde el punto de vista de madres totalmente diferentes.** Es cierto que la etapa materna es hermosa, pero hay quienes experimentan un shock difícil de describir y comprender por muchas otras. ¿Has sentido pánico en tu labor de mamá?

A continuación, hemos recopilado el contenido más relevante de este año en esta sección.

### Isabel de Navasqüés

La entrevista publicada en el post “*[Lo que nadie me dijo sobre la maternidad (y el sexo)”](https://madrescabreadas.com/2021/08/24/maternidad-consciente-sexo-libro/)*, fue sin duda alguna, uno de los más cabreados.

[![](/images/uploads/2021-12-15_13-4230.jpg)](https://madrescabreadas.com/2021/08/24/maternidad-consciente-sexo-libro/)

Esta afamada escritora es madre de dos hijas muy seguidas, pero **experimentó sentimientos contradictorios.** Por fortuna, no se estaba volviendo loca, en realidad experimentaba la matrescencia.

Isabel admite en su libro: “**la maternidad me atropelló como si fuera el fin del mundo”**. Sentir pánico y confusión es una etapa natural. ¿Estás pasando por lo mismo? Harías bien en leer su libro.

### Irene Muñiz

Irene es maestra infantil y tiene un máster en educación emocional. En el artículo, *[Educar las emociones para prevenir el bullying](https://madrescabreadas.com/2021/07/08/educacion-inteligencia-emocional/)*, Irene señala que la **inteligencia emocional** es más importante que la calificación académica.

[![](/images/uploads/2021-12-15_13-4359.jpg)](https://madrescabreadas.com/2021/07/08/educacion-inteligencia-emocional/)

Claro, muchos padres no tienen una buena salud emocional y eso complica todo. Ella sostiene que existe la necesidad de que los padres ayuden a los pequeños a comunicarse mejor, y **fomentar valores como la responsabilidad, solidaridad, resiliencia,** etc.

Si la escuela y las familias trabajan como equipo se puede **prevenir el acoso y el bullying,** debido a que los niños gozarán de un estado emocional equilibrado y saludable.

### Rocio y Miguel Angel

Son pioneros en la formación de un congreso online en el que participan casi **30 profesionales.** Las voces de Rocío y Miguel fueron escuchadas en Madres Cabreadas. En el artículo *[Congreso on line Creando Nuevas Familias](https://madrescabreadas.com/2021/06/06/separacion-respetuosa-congreso-nuevas-familias/)*, pudimos compartir parte de sus programas.

[![](/images/uploads/2021-12-15_13-4512.jpg)](https://madrescabreadas.com/2021/06/06/separacion-respetuosa-congreso-nuevas-familias/)

Ellos dan consejos y **brindan ayuda para familias ensambladas**. Sabemos que conformar una familia no es tarea fácil, pero cuando se trata de una ensamblada los retos son mayores.

Por otra parte, aportan **herramientas útiles para facilitar etapas difíciles como la separación** para que las cosas sean más soportables para los niños.

## Orgullosas de nuestras invitadas

En realidad, los invitados que acabamos de mencionar son tan solo algunos de los estuvieron con nosotros en el 2021. Fue un gran privilegio conversar con todas ellas:

*  **Majo Gimeno.** Es fundadora de la ONG [](https://mamasenaccion.es/)Mamás en Acción. Puedes ver nuestra charla con ella en el post [Ni un niño solo con Mamás en Acción](https://madrescabreadas.com/2021/01/19/mamas-en-accion/).
*  **Lolita Soubrier**. Es una madre inconformista que a pesar de su vida ocupada participa en la lucha para vencer la crisis climática. Puedes ver sus esfuerzos en este campo en [Madres por el clima contra el cambio climático](https://madrescabreadas.com/2021/04/19/madres-por-el-clima-contra-el-cambio-clim%C3%A1tico/).
* **Mamen Baeza Hernández.** Es madre y Terapeuta Ocupacional experta en Atención Temprana, Sexóloga, asesora de Porteo y de Lactancia. Tuvimos el placer de ver los servicios que presta en [Cómo me puede ayudar un sexólogo.](https://madrescabreadas.com/2021/06/03/sexologo-murcia-on-line/)
* **Agustina Rico.** Mamá de dos peques, abogada especializada en Derecho y maternidad. En el post [Una mamá muy legal](https://madrescabreadas.com/2021/07/01/baja-maternidad-abogadas/) se menciona su trabajo en contra de la violencia de género y el aporte que ella dispensa en los conflictos familiares.
*  **Fundación MásFamilia**. Tuvimos el placer de invitar al equipo de la  Fundación MásFamilia con gran reputación en España. Se aboca a la protección, defensa y promoción de la familia. Puedes leer nuestra charla con estos profesionales en el artículo [Fundación Másfamilia por la conciliación familiar](https://madrescabreadas.com/2021/07/20/ayudas-familia-fundacion-masfamilia/).
* **Néstor de San Lázaro Rubio.** Es un profesor de Música murciano con gran estrategia musical. Por medio del plan de estudio Mr. Music aporta herramientas para facilitar el aprendizaje musical en los jóvenes. En [Tú también puedes montar tu propia banda de rock con tu familia y que suene bien ](https://madrescabreadas.com/2021/09/08/actividades-para-adolescentes-en-murcia/)puedes enterarte de nuestra interesante charla.
* **Laura Monge y Sonia López**. Son de la escuela LEMON; una escuela de orientación familiar. Para saber más lee el artículo[ Escuela Lemon para madres y padres también de adolescentes](https://madrescabreadas.com/2021/03/24/padres-de-adolescentes-consejos/).
* **Mª Angustias Salmerón Ruiz.** Pediatra y autora de artículos sobre las repercusiones de las TIC en la salud de niños y adolescentes. Merendamos con ella y plasmamos su percepción de madre y profesión en[ Pediatras también para adolescentes](https://madrescabreadas.com/2021/03/04/edad-pediatra-adolescentes/). 
* **Raquel Peinado**. Asesora de familias quien defiende la disciplina positiva. Para saber más lee [Cómo puede ayudarte la disciplina positiva.](https://madrescabreadas.com/2021/02/17/que-es-disciplina-positiva/)
* **Leticia Gozalo.** Madre y maquilladora profesional. En nuestro post [](https://madrescabreadas.com/2021/02/02/maquillaje-adolescentes/)[Mi hija quiere maquillase. Cómo puedo orientarla](https://madrescabreadas.com/2021/02/02/maquillaje-adolescentes/), da consejos útiles a las madres para manejar esta situación.
*  **José Luis Torres Revert.** Autor del recién publicado libro [](https://labibliadelecommerce.es/)La Biblia del e-commerce. Puedes leer los consejos útiles que da a madres en nuestro post [Madres emprendedoras, es el mejor momento para vender on line.](https://madrescabreadas.com/2021/01/07/vender-online-madres-emprendedoras/)

[](https://madrescabreadas.com/2021/07/01/baja-maternidad-abogadas/)[](https://madrescabreadas.com/2021/06/03/sexologo-murcia-on-line/)En conclusión, la sección “invitados” ha cumplido su propósito este año. Este espacio pretende dar voz a madres, padres, ONG, profesores... entre otros para que puedan contribuir a **formar familias estables, felices y sostenibles.**

Desde luego, hay más entrevistas en esta sección. Te invitamos a navegar en la categoría INVITADOS y leer los consejos de nuestros expertos y madres emprendedoras.

Creemos en las personas que suman, y que la comunidad Madres Cabreadas tiene la mision de dar voz, crear sinergias, y reivindicar todo lo que afecte a las familias a ya nuestros hijos e hijas.