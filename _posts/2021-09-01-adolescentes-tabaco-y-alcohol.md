---
layout: post
slug: adolescentes-tabaco-y-alcohol
title: ¿Puedo evitar que mi hijo fume?
date: 2021-09-02T11:18:38.756Z
image: /images/uploads/portada-adolescentes-y-tabaco.jpg
author: luis
tags:
  - adolescencia
---
Que el mundo está lleno de peligros para nuestros pequeños, sobre todo cuando estos van creciendo y entrando en una etapa tan complicada como puede llegar a ser la adolescencia es una verdaad como un piano. Uno de esos riesgos es el consumo de tabaco, una droga legal en nuestro país que el año pasado dejó más de 50.000 muertos por enfermedades derivadas por este mal hábito.

No es la única droga legal que nos preocupa a los padres y madres, también lo es el alcohol, cuyo consumo también está muy extendido entre los adolescentes, pese a las restricciones que existen en España sobre la venta de bebidas alcohólicas a menores de 18 años. 

Además, estas drogas legales se suelen considerar que son la puerta de entrada a otras mucho más peligrosas y por ello es importante prevenir el consumo de cualquiera de ellas.

## ¿Cómo prevenir el consumo de alcohol y tabaco en adolescentes?

En el mundo que nos rodea es casi inevitable aislar a nuestros hijos y que no conozcan de la existencia de este tipo de drogas legales por lo que tenemos que aprender a convivir con ellas e introducir buenas pautas en su educación. Ignorar el tema es una fórmula que no suele funcionar ya que le damos a estos elementos cierto misterio que puede hacerlo más atrayente para los jóvenes.

![drogas, alcohol y tabaco adolescentes](/images/uploads/drogas-alcohol-y-tabaco-adolescentes.jpg "drogas, alcohol y tabaco adolescentes")

En el trabajo de prevención por no nos puede faltar una [comunicación fluida](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) desde bien pequeños, ya que eso hará que nuestros hijos sean conscientes de que pueden tratar con total confianza cualquier tema con nosotros. 

El truco sería tratar el tema con la importancia adecuada y que quede clara la postura adversa ante este tipo de comportamientos en edades tan tempranas. Hablar sobre los efectos que el alcohol o el tabaco puede causar a su organismo puede ser la manera de disuadirles. También es importante que conozcan las consecuencias del consumo de alcohol y tabaco y los problemas de adicciones en adolescentes en las que cda vez es mas frecuente que caigan.

Debemos tener en cuenta también los factores externos y es que a estas edades son muy influenciables y pueden dejarse llevar por la presión grupal. Es por eso que hay que educar en valores y enseñarles a decir "no" cuando realmente no quieren hacer algo. 

Por otra parte, habría que enseñarles que el alcohol no consigue que te lo pases mejor o que seas más divertido, además de que conozcan alternativas de ocio saludable en la que no necesitarán ni del tabaco, ni del alcohol ni de ninguna otra droga para pasarlo bien. Lo mejor es aficionarlos al deporte desde bien pequeños.

## ¿Qué pasa con el alcohol y el tabaco?

Sin duda alguna, al hablar del alcohol y del tabaco, estamos haciendo referencia a uno de los grandes problemas con el que se encuentra la Salud Pública, siendo aún mayores las consecuencias del alcohol y drogas en adolescentes.

El consumo de estas sustancias es más perjudicial en los más jóvenes ya que su organismo, y sobre todo el cerebro, está aún en desarrollo. Tomar estas sustancias solo puede acarrear riesgos importantes tanto para la salud física como para la mental. 

Las campañas de prevención consiguen disminuir el porcentaje de jóvenes que ya han probado alguna de estas denominadas drogas legales, pero seguimos estando ante un problema preocupante.

![alcohol en adolescentes](/images/uploads/alcohol-en-adolescentes.jpg "alcohol en adolescentes")

El tabaco es el responsable de un gran número de patologías cardiovasculares, cáncer y otras enfermedades respiratorias. Las estadísticas hablan de que la mitad de los fumadores acaban muriendo por enfermedades que se relacionan al consumo del tabaco.

En cuanto al alcohol, los efectos son ambivalentes y es en elevadas cantidades cuando produce efectos muy nocivos tanto cardiovasculares como sistémicos. Todos hemos escuchado cómo alguien de la familia cuenta que una copita de vino al día está recetada por el médico y es cierto que en bajas dosis puede tener efectos beneficiosos para el árbol vascular, pero se olvidan hablar del resto de efectos negativos que surte sobre otros órganos.

Lo que está claro es que la ingesta de alcohol supone una conducta de riesgo en jóvenes los cuales son mucho más vulnerables ante cualquier consumo de alcohol, considerándose un consumo de riesgo.

## ¿Cómo se considera el consumo de tabaco, alcohol y drogas?

En nuestra sociedad, la mayoría de personas consideran el consumo de alcohol y tabaco como algo común. Esto es debido a que tenemos tan arraigado el concepto de droga legal que incluso se nos olvida que estamos hablando de eso, de droga. Socialmente está aceptado el consumo de alcohol en periodos puntuales como puede ser una celebración, la navidad, una boda o una comunión y es importante que a la hora de conversar con nuestros hijos seamos capaces de discernir entre un hecho puntual y un hábito.

En el caso del consumo de drogas en adolescentes en España el rechazo es más unánime por parte de la población, aunque entre estos tres elementos la única diferencia que existe es exactamente esa, su aceptación y consideración.

Nos gustaría conocer un poco sobre tu opinión, si alguien se ha enfrentado ya a esta etapa y todo lo que pueda ser útil para el resto de lectores podéis dejarlo en los comentarios. Toda aportación es bienvenida para la prevención del consumo de alcohol, tabaco y drogas entre la población adolescente.

Mas informacion sobre [tabaco, adolescentes y entorno familiar](https://www.analesdepediatria.org/es-prevalencia-del-consumo-tabaco-adolescentes--articulo-13101240) aqui.

Si te ha sido de utilidad, comparte con quien pueda servirle de ayuda.

Suscribete para no perderte nada

*Portada by Anastasia Vityukova on Unsplash*

*Photo by Grav on Unsplash*

*Photo by Jacob Bentzinger on Unsplash*