---
layout: post
slug: influencers-adolescentes
title: 8 influencers de adolescentes
date: 2019-08-01T09:00:00+02:00
image: /images/posts/2019-08-01-influencers-adolescentes/p-palo-selfie.jpg
author: maria
tags:
  - adolescencia
---
Cuando os enseñaba cómo [configurar la privacidad de Tik Tok](https://madrescabreadas.com/2017/11/26/privacidad-musically-adolescentes/) para vuestros hijos ya era una red social que prometía, aunque aún no se gestaban influencias adolescentes de tal magnitud como los de ahora.

El nombre original de la aplicación es Douyin que significa "sacudir la música" en chino, y consiste en compartir pequeños clips musicales en los que **el propio usuario es el protagonista haciendo play back** y bailando o gesticulando exageradamente con manos y brazos al son de los últimos hits del momento siguiendo los pasos de las principales influencias españolas o extranjeras.

Puedes [descargar Tik Tok aquí](https://vm.tiktok.com/ZMR8Pw8UY/) si aun no la tienes y sientes curiosidad por la red social que ha superado incluso a Instagram, y que esta conquistando a jóvenes y adultos.

Algunos suelen ataviarse con originales estilismos y otros hacen parodias divertidas usando las letras de las canciones más populares entre los jóvenes (la imaginación no tiene límites).

Este nuevo arte encierra uno de los mayores negocios de Internet, ya que algun@s "musers", como se denomina a los protagonistas de los videos, se han convertido en verdaderos **macro** **influencers con decenas de miles de seguidores**, con cuenta también en YouTube o Instagram, donde se completa el negocio.

Estos influencers pueden llegar a cobrar por publicar un video o una foto varios miles de Euros, dependiendo del número de seguidores y el engangement (interacción con comentarios y likes en la publicación), y las marcas se los rifan para que promocionen sus productos entre el público adolescente. Es por esta razón que muchos no escatiman en realizar una buena inversión para **contar con todos los elementos necesarios** que le garanticen una grabación de calidad.

Como este post ya tiene un tiempo, hemos actualiado los [infuencers de moda actuales aqui]({% post_url 2021-11-12-influencers-que-mas-influyen-en-los-adolescentes %}).

![Kit para influencers](/images/uploads/jusoo-10.2-rgb-luz-de-anillo-led-con-tripode.jpg "Jusoo 10.2\\\\\\\\\\\" RGB Luz de Anillo LED con Trípode")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Jusoo-Tr%C3%ADpode-Soporte-Bluetooth-Regulable/dp/B08FHBS5GR/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=249RVG5KNABCS&dchild=1&keywords=tripode%2Bpara%2Bmovil%2Btik%2Btok&qid=1629145848&sprefix=tripode%2Bpara%2Bmovil%2Btik%2Caps%2C500&sr=8-1&th=1&madrescabread-21) (enlace afiliado)

Además, **algun@s tienen su propia marca de ropa**, han publicado libros sobre su vida, agendas escolares o han sacado algún single propio.

Aparentemente superficial, este arte de hacer Tik Tok no es fácil, ya que hay que sincronizar a la perfección la vocalización de la letra con la canción, inventar una coreografía, crear un estilismo o unas uñas perfectas y lo más importante, dominar las transiciones entre escenas. Y todo ello en un minuto!. ¿Lo has probado?

No obstante **conviene centrarnos en el fenómeno influencia** para entender a nuestros adolescentes, así que voy a responder a algunas de las preguntas que seguramente asestaréis haciendo.

## ¿Cuántas influencers hay en España?

Esta nueva moda aglutina ya va más de 400.000 influencers sólo en Instagram.

Para que conozcáis más de cerca los influencers de vuestros hijos adolescentes o preadolescentes, os voy a presentar a diez de ellos, casi todos gestados en Tik Tok, aunque hay muchos más:

![Lola Lolita libro](/images/posts/2019-08-01-influencers-adolescentes/lolalolita.jpg)

* [@lolaloliitaaa](https://www.instagram.com/lolaloliitaaa/) es una maestra del Tik Tok, y **crea tendencia en el vestir de las chicas** con su propia [marca de ropa "Lola Lolita"](https://www.lolalolitashop.com/sobre-mi/). Tiene publicados unos libros como "Nunca dejes de bailar: los sueños de una muser", y Nunca Dejes de Soñar, y para ella su familia es un gran apoyo en todo lo que hace.

![Influencers Lola Lolita](/images/uploads/nunca-dejes-de-bailar-nunca-dejes-de-sonar.jpg "Nunca dejes de bailar+Nunca dejes de soñar")

**Nunca dejes de bailar**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/dp/841742430X/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=742a19914a5e99897ef9c27fa6aaaf69&creativeASIN=841742430X) (enlace afiliado)

**Nunca dejes de soñar**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/Nunca-dejes-so%C3%B1ar-Lola-Lolita/dp/8417424970/ref=pd_bxgy_img_1/257-1133676-5220328?pd_rd_w=bVyDh&pf_rd_p=4be0678a-50bc-4d88-a02a-5fb31b66be11&pf_rd_r=18G98AKVV3ZTC98072G2&pd_rd_r=8ad65214-4627-44b8-95e0-9ee8c9ea8003&pd_rd_wg=sc8Bz&pd_rd_i=8417424970&psc=1&madrescabread-21) (enlace afiliado)

![amarmolmmc](/images/posts/2019-08-01-influencers-adolescentes/amarmolmmc.jpg)

* [@amarmolmmc](https://www.instagram.com/amarmolmmc/) domina los Tik Tok, **sus viajes hacen soñar a sus seguidores** y sus sorteos les entusiasman, pero sin duda lo que más valoro en ella es su compromiso con ciertas causas como la lucha contra el bullying.

![Influencers Amarmolmmc](/images/uploads/sonrie-aunque-te-cueste.jpg "Sonríe aunque te cueste")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/8448026462/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=9bcdef856d38b2408a24e1af1a4bef5a&creativeASIN=8448026462) (enlace afiliado)

![monismurf](/images/posts/2019-08-01-influencers-adolescentes/monismurf.jpg)

* [@monismurf](https://www.instagram.com/monismurf/) ha comercializado la "Agenda de Mónica Morán" que es la más deseada para el próximo curso, y la libreta. También ha publicado el libro "Diario de una muser" y **destaca por hacer las mejores transiciones en Tik Tok.**

![Influencers Monismurf](/images/uploads/agenda-be-your-best-diario-de-una-muser.jpg "Mónica Morán. Agenda: Be your best")

**Agenda de Mónica Morán**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://amzn.to/2YiBsJN) (enlace afiliado)

**Diario de una muser**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://amzn.to/2K9cTWI) (enlace afiliado)

![la diversion de martina](/images/posts/2019-08-01-influencers-adolescentes/martina.jpg)

* [@la_diversion_de_martina](https://www.instagram.com/la_diversion_de_martina/) **con tal solo 14 años ha publicado varios libros de aventuras** y también es youtuber. Además, es actriz en película de Santiago Segura, "Padre no hay más que uno"

![Influencers Martina](/images/uploads/suenos-por-cumplir-nadie-como-ella-nadie-como-el.jpg "Sueños por cumplir+Nadie como ella+Nadie como él")

**Sueños por cumplir**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/gp/product/B08N5H9CPK/ref=dbs_a_def_rwt_hsch_vapi_taft_p1_i0&madrescabread-21) (enlace afiliado)

**Nadie como ella**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/gp/product/B093CPDVMX/ref=dbs_a_def_rwt_hsch_vapi_taft_p1_i11&madrescabread-21) (enlace afiliado)

**Nadie como él**

[![](/images/uploads/boton-comprar-amazon-72x.jpg)](https://www.amazon.es/gp/product/B08NTZBCCP/ref=dbs_a_def_rwt_hsch_vapi_tkin_p1_i8&madrescabread-21) (enlace afiliado)

![twin melody](/images/posts/2019-08-01-influencers-adolescentes/twin.jpg)

* [@twin_melody](https://www.instagram.com/twin_melody/) son unas de mis favoritas! Son unas gemelas a las que el arte les sale por sus poros. No sólo bailan y hacen coreografías de los más originales jugando con el sorprendente parecido entre ambas y las transiciones en sus videos musicales, sino que además, **cantan maravillosamente y hacen versiones impresionantes**. Pero lo que más me gusta de ellas es que son auténticas y tienen claro que los likes no definen a una persona.

![Influencers Twin Melody](/images/uploads/twin-melody.jpg "Twin Melody")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/B07NHQHWVB/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=5e492ddfa917f1828443a661263d2f45&creativeASIN=B07NHQHWVB) (enlace afiliado)

![lucas bojanich](/images/posts/2019-08-01-influencers-adolescentes/lucas.jpg)

* [@lucasbojanich](https://www.instagram.com/lucasbojanich/) me sorprendió cuando lo vi dentro de una bañera llena de cereales de chocolate con leche. Pero nos creáis que cae en lo fácil, ya que **se curra sketches super divertidos de cualquier tema cotidiano** siempre con la música como protagonista. Podéis encontrarlo también en Youtube.

![Influencers Lucas bojanich](/images/uploads/en-mi-mundo-conectad-s-.jpg "En mi mundo (Conectad@s)")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/En-mi-mundo-Conectad-s/dp/8417921923/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Lucas+Bojanich&qid=1628891610&sr=8-1&madrescabread-21) (enlace afiliado)

![adelina fominykh](/images/posts/2019-08-01-influencers-adolescentes/adelina.jpg)

* [@adelina_fominykh](https://www.instagram.com/adelina_fominykh/) formó parte de la selección española de gimnasia rítmica, es vegana y nos impresiona con sus posturas imposibles en los momentos más cotidianos.

![Influencers Adelina Fominykh](/images/uploads/volar-entre-cintas-de-colores-4you2-.jpg "Volar entre cintas de colores (4You2)")

![fabio muñoz](/images/posts/2019-08-01-influencers-adolescentes/fabio.jpg)

* [@fabiomnz](https://www.instagram.com/fabiomnz/) es pura imaginación y locura para hacernos reír, aunque parte del mérito es de su abuela, a le que pega unos sustos de muerte. **Es capaz de interpretar varios personajes cambiándose simplemente de pelucas** para dar vida a situaciones diarias de cualquier adolescente desde el prisma de un humor estridente que a nadie deja impasible

![Influencers Fabio Muñoz](/images/uploads/quien-soy-yo-en-realidad-y-otros-enigmas-planetarios-4you2-.jpg "Quién soy yo en realidad y otros enigmas planetarios")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Qui%C3%A9n-realidad-otros-enigmas-planetarios/dp/8427047398/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Fabio+Mufan%CC%83as&qid=1629309514&sr=8-1&madrescabread-21) (enlace afiliado)

Y después de este repaso al panorama adolescente de los influencers quizá podáis comprender un poco mejor a vuestros hijos y si no, os animo que grabéis un Tik Tok con ellos. Os vais a divertir!! 

Te atreves? [Descarga Tik Tok aqui](https://vm.tiktok.com/ZMR8Pw8UY/).

Quizá te interesen estas [ideas de regalos para adolescentes y jóvenes](https://madrescabreadas.com/2020/11/26/ideas-regalos-jovenes/).

Si te ha gustado, comparte y suscríbete,  me ayudarás a seguir escribiendo este blog.

*Foto de portada by Steve Gale on Unsplash*