---
author: belem
date: '2021-02-04T10:10:29+02:00'
image: /images/posts/celos-hermanos/p-columpio.jpg
layout: post
tags:
- crianza
title: Cómo evitar los celos entre hermanos
---

Los celos forman parte de nuestra naturaleza. Estos pueden no ser muy agradables, pero es casi un hecho que todos los hemos experimentado. Los celos son una emoción esencial y no son exclusivos de los seres humanos; muchos animales también los experimentan. En todos los núcleos sociales pueden existir situaciones que den lugar a los celos, y más específicamente en los núcleos familiares, ya que los celos no son exclusivos de las parejas. Los celos entre hermanos son un asunto tan común que muchas veces pasa inadvertido como algo que se solucionará por sí mismo con el tiempo. 


![3 cachorros león hermanos](/images/posts/celos-hermanos/leones.jpg)

Si bien esto puede ser cierto, conviene conocer las raíces de nuestra situación específica, de esta manera, encontraremos una solución que beneficie tanto a nuestros hijos como a la dinámica de nuestra familia. 

Quizá también te interese este post sobre el [lugar que ocupan los hermanos en las familias](https://madrescabreadas.com/2014/10/22/hermanos-familia-numerosa/).

## Posibles causas de celos entre hermanos

Los celos entre hermanos son muy comunes. No pensemos que sólo a nosotros nos pasa, o que los hijos de nuestros amigos se llevan tan bien que nunca hay episodios de celos entre ellos. Todas las familias con más de un hijo seguro que en algún momento los han sufrido. Es más, ni siquiera es necesario que un bebé haya nacido ya para que el hermanito o hermanitos mayores comiencen a sentir celos o, incluso, que haya un recién nacido para que los hermanos sientan celos. 

Los celos entre hermanos se producen cuando uno o unos de los hermanos creen que el afecto y la atención de los padres podría disminuir o incluso desaparecer por completo. Los casos más comunes los vemos cuando llega un nuevo integrante a la familia. El nacimiento de un hermanito o hermanita es un suceso muy grande para la vida de los hermanos mayores. 

Pero también podemos experimentar celos entre hermanos cuando alguno de ellos recibe más atención que el otro, por ejemplo, por practicar y sobresalir en alguna disciplina, por recibir un festejo de cumpleaños o un regalo. En fin, las posibilidades son infinitas si pensamos en lo complejas que son las emociones y las interacciones humanas.  

Puede que sin darnos cuenta estemos favoreciendo a alguno de nuestros hijos más que a los demás. Puede que alguna acción por nuestra parte hacia alguno de nuestros hijos no sea muy bien recibida por el resto (por ejemplo, comprar un nuevo par de zapatos). En todo caso, mantener los canales de comunicación abiertos es clave para evitar este tipo de malentendidos. 

Te recomendamos el [libro "Hermanos", de Tania García](https://amzn.to/3rvbaOD) (enlace afiliado), no sólo para abordar los celos sino para que la relación entre tus hijos sea sana y fuerte.
    
        
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8418045353&linkId=37fe6d21ba64be69f3b9d7f9228c4468&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

## ¿Cómo evitar celos entre hermanos?

La prevención es muy importante; consiste en cortar el problema antes de que éste se genere. Como padres, somos lo suficientemente intuitivos como para saber cuándo una situación podría evolucionar de manera diferente a lo ideal. Nadie mejor que nosotros para saber qué podría desencadenar una crisis de celos, pero incluso así, habrá situaciones de celos que se desarrollen inesperadamente. Lo mejor es estar preparados en todo momento. 

### Hagamos introspección

Las madres y padres somos una parte importante de la prevención de los celos entre hermanos. Obviamente en primer lugar debemos trabajar a nivel personal. Se recomienda hacer una especie de introspección para encontrar si, tal vez, nosotros somos los causantes involuntarios, directa o indirectamente, de ellos. 

Pensemos que quizás a un nivel subconsciente, favorecemos más a un hijo que a otro. Esto puede ser por cuestión de que uno es del sexo que tanto deseábamos, es un bebé arcoíris o se inclina por las mismas actividades que nos gustaban a nosotros de niños. 

### Tiempo exclusivo para cada hijo

Recordemos que todos nuestros hijos merecen la misma atención, pensemos en que son seres únicos y diferentes, en lo especiales que son por ser quienes son. Una buena idea es dedicar algún tiempo (aunque sea pequeñito) a cada uno de nuestros hijos y luego un tiempo para todos, en donde podamos compartir y platicar de nuestro día y otras cosas cotidianas. Favorezcamos la diversidad, celebremos sus personalidades y acompañemos sus viajes personales en búsqueda de sus identidades. 

### Evitar las comparaciones entre hermanos

Asimismo, debemos evitar en todo momento las comparaciones, las descalificaciones y regaños frente a los demás miembros de la familia. La mejor forma de corregir a un hijo siempre será en privado. 

### El respeto es muy importante

La educación por supuesto, es muy importante. Por educación me refiero a los básicos, decir por favor, gracias, saber pedir perdón, saber perder y saber ganar y aceptar que no siempre tenemos la razón y que siempre habrá quien piense distinto a nosotros, pero que en todo momento el respeto es lo más importante. Recordemos, el ejemplo es la mejor forma de enseñanza. 

### Repartir equitativamente las tres del hogar
Otra forma de prevenir los celos entre hermanos es implementando un cronograma de tareas en el hogar del que todos formemos parte equitativa. Toda la familia debe compartir las tareas del hogar y tener responsabilidades y obligaciones propias. 


![niños cocinando con un bol](/images/posts/celos-hermanos/cocinando.jpg)

### Hagámoslos partícipes

Por último, hablemos con nuestros hijos. Por muy pequeños que sean, aprendamos a escucharlos. Escuchemos sus necesidades, sus miedos, sus alegrías. 

Hablemos con ellos si un cambio importante se acerca. Hagámoslos partícipes si un hermanito está pronto a arribar a la familia. Ellos pueden ayudarnos a elegir un juguete o un conjunto de ropa. Si se puede y así lo deseamos, pueden acompañarnos a la cita en el médico, para que vea a su hermanito o hermanita en la ecografía. No hagamos comentarios como “ya no serás mi bebé”, “lo tendrás que cuidar cuando llore” o “ahora tus juguetes serán para tu hermanito”. Démosle a cada uno de nuestros hijos sus tiempos, sus espacios y sus cosas. Pero enseñemos también de la importancia de compartir, recordándoles que compartir es un asunto temporal, no permanente.    

## Trucos y actividades para trabajar los celos

### Actividades en familia

Los celos deben trabajarse dentro de la familia. Las actividades familiares son buenas para ellos. Podemos salir a hacer una caminata o un picnic y cada quién tendrá una responsabilidad: tú llevas el agua, tú empacas la comida, tú te harás cargo de ver que todo quede limpio cuando nos retiremos. 

De nuevo, puede ser una buena idea establecer momentos para cada uno de nuestros hijos y momentos de convivencia familiar. Así, cada miembro de la familia se sentirá valorado y apreciado. 


![hermanos beso](/images/posts/celos-hermanos/beso.jpg)

### Su opinión importa

Involucrar a los hijos en los procesos de la familia siempre es una buena idea. Informemos a nuestros hijos de los asuntos que creamos son pertinentes. Pidamos sus opiniones cuando las necesitemos, dejemos que cada uno se establezca como un individuo en la familia.

### No olvides que los celos son un proceso

Finalmente, recordemos que los celos son solo una etapa. Son transitivos, un proceso adaptativo que tendrá un comienzo y un final. A veces pueden tomarles días completar el proceso, pero en muchas ocasiones éste llegará y se irá el mismo día, incluso en cuestión de horas. 

### El acompañamiento es fundamental

Es importante que estemos siempre acompañando a nuestros pequeños en este camino. Estar al pendiente de los involucrados y sus interacciones será útil para saber si necesitamos ayuda extra o externa. Esto sucederá si vemos que los celos no van reduciendo o no desaparecen en absoluto, incluso con el paso del tiempo. Entonces la mejor opción será consultar con un profesional, como un psicólogo.  
 
Los celos entre hermanos son normales. No desesperemos y en vez de ser reactivos, seamos pro activos para evitarlos antes de que aparezcan y sepamos tratarlos una vez que lleguen.

*Photo by Annie Spratt on Unsplash columpio*

*Photo by Annie Spratt on Unsplash beso*

*Photo by Travis Grossen on Unsplash corazón* 

*Photo by Annie Spratt on Unsplash cocinando*

*Photo by Annie Spratt on Unsplash leones*