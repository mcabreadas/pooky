---
layout: post
slug: libro-infantil-rock-indie-tales
title: Si te gusta el rock regala Indie Tales a tus peques
date: 2022-01-20T11:15:08.213Z
image: /images/uploads/captura-de-pantalla-2022-01-20-a-las-12.21.09.png
author: maria
tags:
  - invitado
  - libros
---
> Hoy merendamos con un papá emprendedor y su hija Paula, de 7 años, que se han embarcado en la aventura de hacer realidad un sueño: contar historias inspiradas en canciones de rock donde la pequeña es la principal autora.
>
> En tiempos del Reggaeton recibo esta iniciativa con los brazos abiertos, la comparto con vosotras y os animo a que la apoyéis, la difundáis y regaleis el libro a vuestros peques porque tenemos que culturizar a las generaciones futuras con música de verdad, que ya tienen bastante Rosalía, Tra tra.. (con todos mis respetos, que a veces a mí también me encanta bailarla... pero un rato pequeño).
>
> La cultura pop rock es la nuestra, con la que hemos crecido, y debemos transmitirla a nuestros hijos e hijas, aún a riesgo de que nos tachen de [boomers,](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/)aunque luego les encanta nuestra música porque es música de verdad.
>
> Os dejo con Mikie, que nos va a hablar del [libro infantil Indie Tales](https://www.indietales.me/?lang=es).

![](/images/uploads/captura-de-pantalla-2022-01-20-a-las-13.01.57.png)

Hola madres y padres rockeros, me gustaría presentaros un libro muy especial que ha hecho una niña de tan solo 7 años con su padre Mike Flores: INDIE tales. Un maravilloso libro con 10 cuentos y poemas imaginados por Paula mientras sonaba la música de Radiohead, Garbage, Foo Fighters, The Cardigans, Pearl Jam, [The Cranberries](https://es.wikipedia.org/wiki/The_Cranberries), Incubus, Florence + The Machine, Queens Of The Stone Age, YYYs...

Cada capítulo del libro es un cuento infantil diferente inspirado por un disco de música, son historias independientes pero la narrativa se entrelaza dando como resultado una obra sorprendente. 

El libro también incluye 2 poemas creados por Paula, escritos con su propia letra y decorado con algunos dibujos hechos por ella misma, el acabado de ellos sorprende al pensar que los ha escrito una niña que apenas tiene 7 años.

## Referencias molonas para padres ochenteros

Las ilustraciones son una preciosidad, el nivel de detalle es increíble, de verdad. Todas las páginas esconden un montón de referencias y secretos para los padres ochenteros y los más pequeños de la casa. 

Estos "tesoros", como los llaman Paula y Mike Flores, son alusiones a un montón de películas, discos de música, libros, series, juegos... es una pasada. 

Por ahí podéis encontrar las gafas de Kurt Cobain, la peli de Los Goonies, a Totoro, a Bill Murray, un Heroquest, Star Wars, Tintín, El gigante de hierro... y paro ya porque no acabaría nunca.

## Actividades rockeras en familia

Como colofón final incluye también algunas actividades rockeras para hacer, unas para hacer en familia y otras para vuestros hijos solos. Como en el resto INDIE tales, estás actividades son muy entretenidas y desprenden un humor muy divertido. 

El libro está cuidado al máximo, tiene una maquetación extraordinaria, impreso de manera ecológica en España y encuadernado en tapa dura.

INDIE tales esta a la venta con envío gratis a través de [www.indietales.me](http://www.indietales.me/)

Rock on!

Comparte para que llegue a mas familias y ayudarnos a seguir excribiendo este blog ;)