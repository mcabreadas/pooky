---
author: maria
date: 2021-02-18 08:13:29
image: /images/posts/vlog/p-agendas.jpg
layout: post
slug: vlog-agendas-feministas-mujeres-historia
tags:
- vlog
- mujer
title: Vlog agendas Mujeres Brillantes en la Historia
---

Porque para que nosotras estemos donde estamos otras han tenido que luchar y no siempre se les ha reconocido, y para inspirar a las mujeres de hoy en día 💪🏻, ha surgido este proyecto de agendas atemporales personalizadas con una caja- lámina hecha a mano con la mujer que te inspira para crecer, crear, empodérarte y seguir adelante con fuerza y ganas cada día.

Elige a tu heroína personal de entre más de 40 que encontrarás en la web de @esquematica.e y personaliza tu agenda y tu taza con su retrato.

Además la agenda contiene fechas clave con hitos históricos de nuestras mujeres brillantes y días a recordar por ser importantes para la mujer.

También tienes frases inspiradoras de todas ellas para los momentos en que necesitamos aliento.

Al tratarse de una agenda atemporal puedes empezar a usarla cuando tú quieras personalizándola con los stickers a tu gusto.

Llénala de sueños y proyectos!

Por ser lectora de este blog tienes un [descuento especial para comprar la agenda feminista](https://www.esquematica.es/categoria-producto/agendas/?coupon-code=EsquemadreA).

<a href="https://www.esquematica.es/categoria-producto/agendas/?coupon-code=EsquemadreA" class='c-btn c-btn--active c-btn--small'>Quiero la agenda</a>

<iframe width="560" height="315" src="https://www.youtube.com/embed/p5iCXATyQlA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<div class="" style="background-color: rgba(249, 182, 164, 0.32);padding: 20px;border-radius: 20px;">
<p>
    <img src="/images/posts/vlog/banner-momo.png" alt="banner momo" data-action="zoom">
</p>

<h3 id="la-propuesta" style="text-align: center;">
    La propuesta
</h3>

<p>
    Momo Clothes es un comercio local en pleno centro de Murcia de ropa para mujer, chicas adolescentes y Comunión. Es un espacio vivo donde podrás encontrar esas piezas exclusivas que te definen.
</p>
<p>
    Esta firma busca y selecciona a través del mundo prendas diferentes que te permitan expresar tu esencia y respeten tu confort; que te hagan sentir especial y perduren en el tiempo.
</p>
<p>
    Sus colecciones están basadas en su interpretación de la moda y de la vida, porque la vida es simplemente extraordinaria, y Momo Clothes te la quiere hacer extraordinariamente simple.
</p>

<div style="text-align: center;margin-top: 2em;">
        <a href="https://momo.com.es">
            <button class="c-btn c-btn--small">Visita la web de Momo Clothes</button>
        </a>
    </div>
</div>
