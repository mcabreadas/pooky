---
author: maria
date: '2021-03-11T10:13:29+02:00'
image: /images/posts/vlog/p-dia-mujer.png
layout: post
tags:
- vlog
- conciliación
- mujer
title: 'Vlog 8M: Reivindicamos los cuidados domésticos como trabajo'
---

Esta semana hablo con Encarna Talavera con motivo del 8M Día de la Mujer, y reivindico en nombre de la comunidad de Madres Cabreadas que se reconozcan los trabajos domésticos de cuidado de la familia y el hogar como un trabajo digno, y se valore socialmente por la importancia que tiene para el bienestar y salud de la sociedad.

<iframe width="560" height="315" src="https://www.youtube.com/embed/IynqlylA8wM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Comparte para que llegue a mucha gente y suscríbete para no perderte nada!