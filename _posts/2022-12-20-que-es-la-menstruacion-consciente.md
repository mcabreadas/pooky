---
layout: post
slug: que-es-la-menstruacion-consciente
title: Educa a tu hija en la menstruación consciente
date: 2023-02-15T13:01:17.489Z
image: /images/uploads/menstruacion-consciente.jpg
author: faby
tags:
  - adolescencia
---
Es necesario conocernos, ya que el **ciclo menstrual puede afectar nuestro estado de ánimo.** De acuerdo a la opinión de los expertos, los procesos que ocurren durante el ciclo menstrual pueden aumentar o bajar la productividad de las mujeres.

En esta oportunidad hablaremos de la menstruación consciente, qué es y por qué es importante desarrollar conciencia de la menstruación tambien en nuestras hijas adolescentes.

Te dejo este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que la comunicacion fluya :)

## ¿Qué es la menstruación consciente?

Tal como lo indica el título, la menstruación consciente significa **estar consciente de lo que implica el periodo o la regla;** como también se le dice. Es una forma diferente de ver el ciclo menstrual.

No se trata solamente de los días de sangrado. Más bien, **significa estar consciente de las fases de todo el ciclo.**

## Fases del ciclo menstrual

![Ciclo Menstrual](/images/uploads/ciclo-menstrual.jpg "Ciclo Menstrual")

Los días de sangrado son típicos de la menstruación. En general, en esa fase es que decimos “estoy en mis días”o “me llegó el periodo”. Sin embargo, durante todo el mes las mujeres experimentamos algunos cambios en nuestro cuerpo.

A continuación, te indicaré las fases del ciclo menstrual.

### Fase folicular

En esta fase el cuerpo se prepara para **liberar el óvulo**. En este sentido, experimentarás una subida de los niveles de estrógeno. También se le conoce como preovulatoria, fase proliferativa.

**Inicia el primer día de la menstruación y culmina con la ovulación.**

### Fase ovulatoria

Este es el punto de mayor **fertilidad en la mujer**. En general, la mujer puede experimentar un mayor deseo sexual. Ocurre a la mitad del ciclo, en el día 14 aproximadamente.

Con relación al cuerpo, el **óvulo ya está en plena maduración para concebir.** Los niveles de estrógeno se disparan.

### Fase lútea

**En esta fase se da el embarazo** debido a la fecundación del óvulo. El cuerpo empieza a producir progesterona para facilitar la implantación.

En caso de que no ocurra el embarazo, entonces la mujer experimenta los síntomas asociados al **síndrome premenstrual** (tensión mamaría, cambios de humor, etc.).

### Fase menstrual

Comienza el sangrado causado por la expulsión del óvulo. En este punto reinicia el ciclo.

## ¿Por qué conocer bien y apreciar tu menstruación?

![Consciencia de la Menstruación](/images/uploads/consciencia-de-la-menstruacion.jpg "Consciencia de la Menstruación")

Tal como has podido notar en cada fase, el cuerpo experimenta cambios. En tal sentido, **conocer en qué día de tu fase te encuentras puede contribuir a que te sientas cómoda.** De esta forma, podrás mantener tu estado de ánimo más estable.

Por ejemplo, en la fase folicular sube los niveles de estrógeno, por lo que tendrás buen ánimo y energía. Aprovecha esta fase para aumentar tu productividad. ¡Serás invencible!

Por otra parte, en la fase lútea, consiéntete un poco. Procura llevar las cosas con calma. Si intentas mantener el ritmo te frustrarás y terminarás con cambios de humor que ni tú misma entenderás.

## ¿Cómo vivir una menstruación consciente?

El primer paso es **tomar nota de tu ciclo menstrual**, pero debes hacer más que eso. Procura escuchar tu cuerpo.

Aprovecha los días en que te sientas con energía, no pares. Sin embargo, minimiza los cambios bruscos de humor al bajar el ritmo en los dias de sangrado. 

Que nuestras hijas sepan afrontar su [ciclo menstrual](https://helloclue.com/es/articulos/ciclo-a-z/el-ciclo-menstrual-mas-que-solo-tu-periodo) de esta forma les ayudara a vivir mas plenamente y a no exigirse demasiado en determinados momentos, y a aprovechar su energia en otros.

Saber que son seres ciclicos y no planos sera fundamental para que les vaya mejor en la vida.