---
layout: post
slug: resumen-del-ano-2021-blog
title: "Resumen del año: Madres Cabreadas en Latinoamérica, video blog y adolescentes"
date: 2021-12-30T12:00:42.889Z
image: /images/uploads/istockphoto-1279483651-612x612.jpg
author: .
---
En Madres Cabreadas llevamos acompañándote mas de 10 años, pero  este último año ha sido especialmente difícil. Aun así, hemos intentado que estos 12 meses hayan sido “más llevaderos” aportando contenido de calidad y respondiendo a las consultas y necesidades que recibimos.

**La labor de los padres y madres no ha sido sencilla**. Hemos demostrado ser perseverantes, resilientes, valientes y audaces. 

Este último año, hemos abierto las puertas a madres cabreadas en latinoamérica, incorporando al equipo redactoras de México y Venezuela ya que sois muchas las que nos leéis desde allá. Estamos encantadas de abrir fronteras y enriquecernos unas de otras porque creemos que pocas cosas hay en este mundo tan universales como la maternidad y la crianza.

## ¿Cómo nos fue en el blog este año?

![Cómo nos fue en el blog este año](/images/uploads/maria-sanchez.png "Cómo nos fue en el blog este año")

**Nuestra comunidad de madres está en constante crecimiento** y eso nos llena de orgullo. Parece mentira que ya llevamos más de una década con este blog y dando guerra en redes sociales.

Pues bien, hemos compartido variados **temas que son verdaderamente útiles relacionados a la crianza, la adolescencia, ocio, trucos**… 

Sin duda este tipo de contenido formado por madres y para madres nos ayudan a ser más competentes en la difícil pero emocionante labor de ser mamá.

Hemos publicado contenido que atiende necesidades específicas, desde [qué lucir durante la gestación](https://madrescabreadas.com/2021/11/04/zalando-ropa-embarazada/) hasta la idea de *[planificar un segundo hijo](https://madrescabreadas.com/2021/05/13/tener-un-segundo-hijo/)*. Al mismo tiempo, hemos hablado sobre [los mejores pañales](https://madrescabreadas.com/2021/05/27/panales-carrefour-opiniones/) y [sillas del coche](https://madrescabreadas.com/2021/11/15/mejores-sillas-de-paseo-2021/) en el que te ofrecemos una guía para elegir la mejor para ti.

[![](/images/uploads/2021-12-15_12-4647.jpg)](https://madrescabreadas.com/2021/05/13/tener-un-segundo-hijo/)

[Ver Blog ](https://madrescabreadas.com/2021/05/13/tener-un-segundo-hijo/)👆🏻

Las madres primerizas necesitan mucho apoyo y comprensión. Este año hemos querido compartir con las **madres jóvenes** la experiencia de nuestra comunidad de madres, para acompañarlas en algunos temas que más les preocupan.

¿Cuáles han sido algunos de los temas tendencias del blog?

Algunos de los temas tendencias están relacionados con la **categoría de crianza**.

* [Cuál es la mejor leche de fórmula](https://madrescabreadas.com/2021/04/13/cu%C3%A1l-es-la-mejor-leche-de-f%C3%B3rmula/)
* [Consejos para portear en verano](https://madrescabreadas.com/2021/07/16/mejor-portabebes-verano/)
* [¿Qué hago si mi bebé solo quiere estar en brazos?](https://madrescabreadas.com/2021/05/20/mi-bebe-solo-quiere-brazos/)

Claro, los temas relacionados a la **adolescencia** también fueron tendencia. Y es que la sociedad está cambiando muy rápido, así que como madres debemos **adaptarnos a los retos de la tecnología y los nuevos tiempos.**

Entre los temas más interesantes estuvieron:

* [Cómo conocer más a tu hijo adolescente](https://madrescabreadas.com/2021/02/05/como-conocer-a-tu-hijo-adolescente/)
* [Un reloj inteligente que también protege a tu hijo en Internet](https://madrescabreadas.com/2021/11/05/reloj-inteligente-ninos/)
* [Mi hijo adolescente no tiene amigos, ¿qué puedo hacer yo?](https://madrescabreadas.com/2021/01/21/hijo-adolescente-no-tiene-amigos/)
* [Los 4 hábitos que debes inculcar a tu adolescente para ayudarle a comer sano](https://madrescabreadas.com/2021/09/30/como-alimentarse-adolescente/)
* [¿Puedo evitar que mi hijo fume?](https://madrescabreadas.com/2021/09/02/adolescentes-tabaco-y-alcohol/)

Somos conscientes de que os gustan los videos, as que este año hemos creado una nueva seccion de **Vlog donde también damos orientación** sobre cda etapa de la crianza, consejos practicos y trucos para organizar la familia, y nuestro vioe mas exitoso: expresiones que forman parte de la jerga juvenil, conocerlas nos ayuda a entender a los adolescentes *[aprendiendo a hablar como los adolescentes](https://madrescabreadas.com/2021/01/27/vlog-diccionario-palabras-adolescentes/)*.

[![](/images/uploads/captura-de-pantalla-de-2021-12-15-12-22-12.jpg)](https://www.youtube.com/watch?v=TTolRNCzYQw)

[Ver vídeo](https://www.youtube.com/watch?v=TTolRNCzYQw) 👆🏻

Por otra parte, también compartimos contenido relacionado a temas más íntimos y de los que no se habla tan a menudo, pero igualmente necesario, como por ejemplo [el sexo después del parto.](https://madrescabreadas.com/2021/10/14/cuando-sexo-postparto/)

Y ¿qué decir de algunos trucos? Las madres necesitamos conocer los secretos de las madres experimentadas. Así que, uno de los temas que estuvo en tendencia fue trucos para [evitar que el colchón se manche de pipí.](https://madrescabreadas.com/2021/11/11/evitar-que-colchon-se-manche-de-pipi/)

En definitiva, este año estuvo cargado de contenido para las **madres emprendedoras, luchadoras y valientes.** Mujeres que no se dejan vencer, sino más bien, se enfrentan a los retos con ahínco y pasión. No importa si eres ama de casa, autónoma, madre soltera o casada, aqui tienes tu espacio**.** 

Esperamos seguir acompañándote el próximo año. 

Contamos contigo?

Te gusta lo que hacemos? Asoma la patita y dejanos un comentario, nos haria mucha ilusion!!

Comparte con otras familias para que sigamos creciendo y acompañandonos en el camino.

Photo Portada by patpitchaya on Istockphoto