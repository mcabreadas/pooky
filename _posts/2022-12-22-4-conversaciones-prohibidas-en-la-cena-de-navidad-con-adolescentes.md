---
layout: post
slug: temas-de-conversacion-en-navidad
title: 4 temas  prohibidos en la cena de Nochevieja con adolescentes
date: 2022-12-30T11:50:44.982Z
image: /images/uploads/adolescente-molesta-navidad.jpg
author: faby
tags:
  - adolescencia
---
Durante la época decembrina las familias suelen compartir deliciosos banquetes. Sin embargo, las **cenas de Navidad y Nochevieja son las más especiales del año,** tanto niños como adolescente la esperan con ilusión, ¿sabías que hay conversaciones que pueden arruinarla?

Mejor repasa este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para echaros unas risas y hacerlos sentir comodos.

En esta entrada te presentaré **4 temas que es mejor no tocar si en tus comidas navideñas hay adolescentes.** ¡Empecemos!

## Que no decir en una cena navideña

### 1. Hablar sobre su situación sentimental

Los adolescentes no son abiertos con los adultos; así pues, ¿qué te hace pensar que te hablará de su novia delante de toda la familia? **¡Quizás ni siquiera tenga y eso lo avergüence!**

![Adolescente triste en navidad](/images/uploads/adolescente-triste-navidad.jpg "Adolescente triste en navidad")

Por otro lado, hay que respetar su privacidad. Así pues, si sabes que tu adolescente tiene novia, no lo divulgues en la cena navideña, y mucho menos cuentes **anécdotas de la vida sentimental de tus hijos**. No los utilices como “medio de entretenimiento”.

### 2. No hables sobre los estudios

En general, **los estudios suelen ser causa de muchas discusiones en casa.**

![Adolescente incomodo en navidad](/images/uploads/adolescente-incomodo-navidad.jpg "Adolescente incomodo en navidad")

Si tan solo haces una simple pregunta como: “¿Has aprobado todo?”, puedes allanar el camino para que los padres le echen caña al adolescente, iniciándose “una tercera guerra mundial”.

De modo que, si deseas comer en “santa paz”, mejor no toques ese tema.

### 3. No hagas comentarios de su estilo personal

¿Recuerdas tus años de adolescencia? De seguro usabas un “estilo cool”, que tus padres consideraban “rebelde”. Si te han invitado a la cena navideña y llevas mucho sin ver a los adolescentes de la casa, **no menciones de forma negativa su estilo.**

![Adolescente estilo propio en navidad](/images/uploads/adolescente-estilo-propio-navidad.jpg "Adolescente estilo propio en navidad")

Comentarios como “Vaya pelos de colores traes” “¿Este es el arbolito de navidad?”, entre otros, propiciarán una avalancha de comentarios “jocosos” que harán reír a todos, pero no al adolescente. En su lugar, **alaba algún aspecto positivo o sencillamente no hagas ningún comentario.**

### 4. No menciones nada de su aspecto físico

Este tema está muy relacionado con el anterior. Mencionar detalles de su aspecto, como los **granitos, la delgadez, la gordura,** pueden hacer sentir muy mal a los jóvenes.

![Adolescente con acne](/images/uploads/adolescente-acne.jpg "Adolescente con acne")

En la adolescencia hay mucha presión para tener el rostro y el cuerpo más atractivo, así que si resaltas algún “defecto” puedes generar más acomplejamiento.

Incluso, si tu intención es decir “Vaya grano que tienes, hay una crema que puede ayudarte”. Ese comentario inocente será una tragedia.

Y, por dupuesto, no hagas comentarios sobre su peso, si ha engordado o si se ve muy delgado... Cosas que ni siquiera se deberian comentar sobre nadie, ni en Navidad ni nunca.

Solo puedes mencionar algún **problema físico si el mismo adolescente es el que saca el asunto.**

## ¿Por qué evitar temas incómodos con adolescentes en las cenas de Navidad con familiares?

Estos temas se deben evitar en cualquier momento, no solo en la cena navideña.

En realidad, los **adolescentes suelen luchar por mantener su [autoestima](https://lamenteesmaravillosa.com/la-autoestima-en-los-adolescentes/),** así pues, hay que aprender a comunicarse sin herir sus sentimientos.

![Adolescente discute en Navidad](/images/uploads/adolescente-discute-en-navida.jpg "Adolescente discute en Navidad")

Si eres madre de adolescentes, debes aprender a manejar estas conversaciones en Navidad preservando un ambiente cómodo. Si eres amigo de padres con adolescentes, evita estos temas candentes, ya que le harás la tarea más difícil a los padres.

En conclusión, si deseas conversar con adolescentes trata de hablar de algo que le interese e incluso puedes mencionar algo sobre tu vida para despertar su interés.