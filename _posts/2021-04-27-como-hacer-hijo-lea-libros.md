---
author: maria
date: 2021-04-27 09:13:45.459000
image: /images/uploads/mq2-4.jpg
layout: post
slug: como-hacer-hijo-lea-libros
tags:
- vlog
- planninos
- libros
title: Cómo hacer que mi hijo lea desde pequeño
---

Fomentar la lectura en nuestros hijos es algo que debemos empezar a hacer desde pequeños. En este video te dejo una pautas sencillas por edades, recomendaciones de libros, y el papel que debemos adoptar los padres y madres para hacer de nuestro pequeño o pequeña un gran lector.

Tienes titulos concretos de [libros para todas las edades aqui](https://madrescabreadas.com/2021/04/24/recomendaciones-libros-adolescentes-ninos/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/eR_AOHjIfbA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha servido de ayuda compartre y suscribete!