---
author: ana
date: '2021-03-18T10:13:29+02:00'
image: /images/posts/21-de-marzo-dia-del-sindrome-de-down/p-amor-y-reconocimiento.jpg
layout: post
tags:
- salud
- crianza
title: Por qué se celebra el día del síndrome de Down el 21 de marzo
---

Una manera de advertir la relevancia que el tema del síndrome de Down va ganando en la sociedad se observa atendiendo a la esperanza de vida. En efecto, la media de las personas con esta condición ha pasado en las últimas tres décadas de apenas 30 años a más de 60.

Y a principios del siglo XX no se esperaba que vivieran más allá de los 10 años. ¿Qué indica? Mayor comprensión, visibilidad, más atención y cuidados, más humanidad.

Y como parte de ese reconocimiento a un esfuerzo mundial que no distingue geografías, razas, credos o culturas, llegó el establecimiento, en diciembre de 2011, por parte de la [Organización de Naciones Unidas, del 21 de marzo como el día del Síndrome de Down](https://www.un.org/es/observances/down-syndrome-day).

## Día del Síndrome de Down: celebración y reivindicación de derechos

![apoyo-familiar](/images/posts/21-de-marzo-dia-del-sindrome-de-down/apoyo-familiar.jpg)

La [Resolución 66/149 aprobada el 19 de diciembre de 201](https://issuu.com/martinpassini/docs/resoluci__n_onu_d__a_del_sd)1 parte de que el síndrome es una combinación cromosómica natural que siempre ha formado parte de la condición humana. Por lo tanto existe en todas las regiones del mundo. El documento insta a los estados a garantizar el acceso a la salud, a la educación inclusiva, reconociendo la dignidad y los aportes de las personas con síndrome de Down al bienestar y a la diversidad de sus comunidades. Fomentando la autonomía, la independencia y la toma de decisiones.

## Por qué se celebra el día 21 de marzo el día del Síndrome de Down

La fecha elegida recuerda la alteración cromosómica, base del síndrome. Las células del cuerpo humano poseen 46 cromosomas y, precisamente en el par 21 en lugar de los dos que existen normalmente, hay tres. No es la única combinación, pero es la más citada. Por eso, también se conoce como trisomía 21.

En el movimiento que logró el reconocimiento del día internacional se encuentran la Asociación Europea del Síndrome de Down (EDSA), una organización sin fines de lucro que apoya y representa a las personas con síndrome de Down en toda Europa y la DSI, organización internacional de personas con discapacidad con sede en el Reino Unido, integrada por personas y organizaciones de todo el mundo, comprometidas con la mejora de la calidad de vida de las personas con síndrome de Down, promoviendo su derecho a ser incluidas en pie de igualdad con los demás.

### Qué derechos se promueven en el Día del Síndrome de Down

Entre otros:

- Prestar asesoramiento genético cuando se confirma el diagnóstico
- Establecer programas de salud y atención temprana con énfasis en el apoyo familiar
- Fomentar la [diversidad y las oportunidades educativas](https://madrescabreadas.com/2011/06/24/nuestro-compañeros-down-y-asperger/)
- Asegurar su integración en la comunidad, a través del deporte y el ocio
- Promover imágenes sociales positivas
- Garantizar tutela y atención especializada

![inclusion](/images/posts/21-de-marzo-dia-del-sindrome-de-down/inclusion.jpg)

### Cómo se celebrará el Día del Síndrome de Down 2021

### Para el Día Mundial del Síndrome de Down 2021, DSI busca unir al mundo en #[CONNECT](https://www.ds-int.org/Blog/dsi-does-world-down-syndrome-day-2021) a través de una serie de eventos en línea.  Llama a registrarse y a participar en eventos que se llevarán a cabo en torno a la fecha.

La idea básica consiste en comprender que las mejores personas para responder sobre el síndrome de Down son las personas con síndrome de Down. Ese es el espíritu que promueve esta conexión mundial con familias, defensores, profesionales y organizaciones que trabajan para borrar los límites y superar con amor las dificultades.


*Photo de portada by Nathan Anderson on Unsplash*

*Photo by Nathan Anderson on Unsplash*

*Photo by Nathan Anderson on Unsplash*