---
author: maria
date: '2021-02-02T11:10:29+02:00'
image: /images/posts/maquillaje-adolescentes/p-aro.jpg
layout: post
tags:
- adolescencia
- invitado
title: Mi hija quiere maquillarse. Cómo puedo orientarla
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy repetimos merienda con Leticia Gozalo, de [Make Up by Ele](https://makeupbyele.es) para que nos aconseje sobre cómo orientar a nuestras hijas adolescentes para maquillarse. Acordaos que ya nos dio unos [consejos súper útiles para el cuidado de la piel de los adolescentes aquí](https://madrescabreadas.com/2021/01/12/piel-adolescentes-consejos/).

> Leticia es una mujer emprendedora que apostó por su sueño de ser maquilladora y ha creado una comunidad de mujeres que se quieren y se cuidan, entre las que me encuentro. Puedes seguirla en su [Instagram @makeupbyele](https://www.instagram.com/makeupbyele_/), donde encontrarás consejos prácticos y honestos para el cuidado de tu piel y para maquillarte correctamente sin gastarte una pasta en productos y brochas.
> 
> El maquillaje siempre ha sido una constante en su vida. De niña maquillaba a sus muñecos y de adolescente estaba esperando la ocasión perfecta para desenfundar los pinceles y poder maquillarme a sí misma… ¡y a quien se pusiera por delante!
> Durante años creyó lo que le decían sobre que el maquillaje no era una profesión, así que se formó en protocolo y organización de eventos. Pero un buen día decidió dejar de escuchar a los demás y empezar a escribir su propio cuento.
> Así nació Make Up by Ele.
> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os dejo con Leticia:

![mujer con paleta de sombras](/images/posts/piel-adolescentes/paleta.jpg)

¡Hola! 
Mi nombre es Leticia y soy maquilladora profesional. Estoy aquí para hablarte de algo que como madre, es posible que te inquiete. Se trata de cuándo debe comenzar tu hija a maquillarse. 

## Mamá, quiero maquillarme

Desde niña he tenido curiosidad por la cosmética y el maquillaje. Tanto que barra de labios que cabía en mis manos, barra de labios que usaba (y en ocasiones, destrozaba). Y cuando tenía 5 años mis padres lo veían como algo gracioso porque “a la niña le gusta disfrazarse”. Pero cuando fui más mayor comenzó el conflicto: era demasiado joven para maquillarme y yo sin embargo quería hacerlo. 

Las negativas cada vez eran más firmes y mi cabezonería era todavía mayor. No entendía por qué era algo negativo que quisiera usar máscara de pestañas y un poquito de gloss (fui adolescente en los primeros años del siglo XXI y el gloss cuanto más pegajoso, mejor).

## No debemos prohibir maquillarse a nuestra hija adolescente

Mis padres (sobre todo mi madre), seguían insistiendo en que con 14 años no debía maquillarme y lo único que consiguieron es que me maquillara fuera de casa y con productos poco adecuados. Me maquillaba con cosméticos de calidad dudosa que compraba con mi paga y utilizando brochas compartidas con amigas o incluso mis propios dedos. O me apropiaba de algún producto de los que tenía mi madre que obviamente no eran nada adecuados para mi piel y mi edad. Y aún no me explico cómo no me destrocé la cara. 

## Ayuda a tu hija adolescente a maquillarse

A estas alturas imaginarás que negarte a que tu hija se maquille no es la solución. Lo mejor es que, como su madre, la ayudes y le proporciones todo lo que necesite para maquillarse.  Que te asegures de que utiliza productos de buena calidad, y sobre todo que conozca qué medidas de higiene debe llevar a cabo para que sea seguro para su salud. 
Y ahora, aquí van unos tips para que ayudes a tu hija a que se inicie en el mundo del make up. 

Enséñale que la piel hay que prepararla antes de aplicar cualquier producto. 

![chica joven morena sentada](/images/posts/maquillaje-adolescentes/sentada.jpg)

## Base de maquillaje para adolescentes

Las pieles jóvenes no necesitan bases muy cubrientes. Seguramente querrá ponerse algo que le cubra mucho los granitos que pueda tener, pero con una base que cubra mucho, lo único que va a conseguir es que se note todavía más. Una base muy fluida (como el [velo de color de Bare With Me de NYX](https://amzn.to/3cqTKyh)) (enlace afiliado) es una buena opción. 
    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=B07SFC1DRM&linkId=ff3154b3b0fe90fd520a98579c24124e&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

## Maquillaje efecto buena cara para adolescentes

Lo que más favorece a niñas que están empezando a maquillarse es un efecto buena cara. El eyeliner infinito y super definido igual es demasiado para empezar, pero si les apetece ir probando, una buena opción es que se maquillen la línea de agua superior. Así se marca la línea de pestañas, pero no queda una mirada muy intensa. 

Como labial puede optar por algo muy ligero, como un gloss que no sea muy pegajoso o un labial cremoso. 

Si quiere aplicarse colorete, lo más cómodo es que pruebe con los formatos en polvo y que recurra a tonos más discretos. 

## Máscara de pestaña para adolescentes

La mejor máscara de pestañas para comenzar puede ser una transparente (que también le serviría para fijar las cejas en caso de que las tenga muy rebeldes). Y si quiere utilizar algo con color, las hay de muchos tipos, con lo que será fácil que encuentre una que haga que su mirada se vea natural. 

Explícale los problemas que puede tener como consecuencia de una falta de higiene al compartir brochas o labiales. Desde una conjuntivitis a un herpes. 

Lo que está claro es que no hay un momento perfecto para comenzar, será tu hija la que te muestre su inquietud sobre el tema, pero es importante que sepa que estás ahí para orientarla. 

![chica pelirroja tumbada](/images/posts/maquillaje-adolescentes/tumbada.jpg)

## Regálale un curso de automaquillaje para adolescentes

Y si crees que tus conocimientos no son suficientes, los profesionales del maquillaje y la belleza estamos para ayudarte. En mi caso, imparto cursos de automaquillaje tanto presenciales como online que puede hacer ella sola o contigo, con los que pasaréis un rato divertido y conectaréis todavía un poquito más. 

Aunque si quieres hacerle un regalo que nunca olvidará, ¿qué opinas de montar una beauty party con sus amigas? Comida, juegos, maquillaje… ¡un planazo!

Si quieres saber más, puedes visitar mi web o mi perfil en RRSS y así conocerme un poquito mejor y preguntar todas las dudas que te surjan.

Si te ha gustado, comparte y suscríbete al blog para no perderte nada (no mandamos spam).


*Photo portada by Creative Christians on Unsplash*

*Photo by Jose Martinez on Unsplash chica tumbada*

*Photo by Brooke Cagle on Unsplash chica sentada*