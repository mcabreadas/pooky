---
author: ana
date: '2021-01-16T10:10:29+02:00'
image: /images/posts/cuesta-de-enero/p-cuesta-de-enero.jpg
layout: post
tags:
- trucos
title: Cómo ahorrar en la cuesta de enero
---

Llegó la cuesta de enero y con ella el reflejo de nuestros gastos de las fiestas. Seguro que haremos todo lo posible para pasarla bien, sobre todo después de un año tan particular. ¡Y cómo nos lo merecemos! Por eso vale la pena tomar previsiones que nos permitan no cargar enero y más allá, a cuestas.

Para que se haga menos empinada tendríamos que haber ahorrado  dinero al máximo, aunque con el encierro de estas Navidades lo hemos tenido fácil para ahorrar. 

## A qué se debe la cuesta de enero

Estamos acostumbrados a que en Navidad no se ahorra en regalos, viajes, ocio y comida, pues solemos ser muy espléndidos en las fiestas familiares y de trabajo. 

¿Cómo ahorrar en enero? Por precaución, recortar los viajes, no recurrir al crédito y ajustarnos a nuestros ingresos, serán un signo de que aprendimos la lección que nos dejó el año que termina.

## Consejos sobre cómo ahorrar en la cuesta de enero

Este 2020 aprendimos a vivir prácticamente sin excesos, a comer en casa y con visitas reducidas al mínimo necesario o urgente. Sin darnos cuenta, aprendimos a ahorrar como lo hicieron nuestros abuelos, por lo que la cuesta de enero esta vez no será difícil de remontar. Aquí unos consejos que pueden ser de utilidad para lograr el propósito del ahorro.

### Gastronomía en casa para ahorrar en la cuesta de enero

![pareja cocinando](/images/posts/cuesta-de-enero/ahorrar-al-maximo.jpg)

Lo esencial es hoy lo más valioso así que planificar en familia los menús semanales y cocinarlos en casa será siempre la mejor oportunidad para el encuentro con los sabores y las esencias tradicionales. 

### Darle una nueva vida a la ropa para ahorrar en la cuesta de enero

![reutilizarsar para ahorrar](/images/posts/cuesta-de-enero/reusarparaahorrar.jpg)

¿Cuántas prendas quedaron sin estrenar o apenas han sido lucidas? Hoy lo nuevo puede ser lo que dejamos meses atrás en un armario, en el altillo, en un rincón de la casa. Démosle un giro de color y espontaneidad a la cotidiano de cara al nuevo año. 

### Recreación en familia

![plan de ahorro](/images/posts/cuesta-de-enero/recreaciónenplandeahorro.jpg)

Las películas, los juegos de mesa, la lectura de cuentos, inventarnos manualidades para decorar; son actividades que podemos hacer con nuestros hijos en casa. Un paseo por sitios al aire libre que nos permitan contemplar la naturaleza, son opciones que pueden sustituir el deseo del viaje. Mientras llega un momento más indicado, podemos encontrar en lo más próximo, satisfacción y plenitud.

### Planificar para contrarrestar la incertidumbre

![planificar el ahorro](/images/posts/cuesta-de-enero/planificarelahorro.jpg)

Es importante que organicemos los gastos, nos permitirá prever en qué podemos ahorrar. Si remontamos un año difícil con pocos recursos, administremos los ingresos extras ajustados a los gastos corrientes, dejando un margen generoso para imprevistos.

## ¿Es posible anticiparse a la cuesta de enero?

Sí, es posible anticiparse y seguro muchos ya lo hicieron al comprar en noviembre lo necesario (os recuerdo este post de [cómo ahorrar en Navidad](https://madrescabreadas.com/2020/11/21/consejos-ahorrar-navidad/)), aprovechando precios antes de la subida tradicional de diciembre. Pero si no hemos sido previsores toca ahorrar ahora, en esta época en la que todo nos invita a gastar más de la cuenta debemos contenernos ante lo superfluo y apretarse el cinturón.

**El impacto será menor si llegamos sin deudas**, o estas no exceden demasiado nuestros gastos regulares.

**Debemos apostar a que la cuesta dure solo durante enero**. Y en el peor de los casos nos ocupe sólo el primer trimestre. Ahorremos, para no tener que sobrellevarla todo el año.

Si te ha gustado comparte!


_Foto de portada by Marc Schaefer on Unsplash_

_Foto by Soroush Karimi on Unsplash_

_Foto by Priscilla Du Preez on Unsplash_

_Foto by Patrick Fore on Unsplash_

_Foto by Gabrielle Henderson on Unsplash_