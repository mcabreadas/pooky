---
layout: post
slug: postres-sudafricanos-faciles-de-hacer
title: Los mejores postres sudafricanos que debéis probar en casa
date: 2023-06-20T12:29:35.369Z
image: /images/uploads/grupo-de-amigos-disfrutando-de-comer-postres.jpg
author: faby
tags:
  - 3-a-6
---
**La comida sudafricana es deliciosa y variada.** De hecho, puedes encontrar platillos salados para el almuerzo y deliciosos dulces para merendar. La verdad es que hasta los más pequeños pueden disfrutar de las **recetas tradicionales sudafricanas**.

Ya os hablamos sobre [los mejores helados caseros](https://madrescabreadas.com/2021/07/15/helados-caseros-saludables/) que podeis hacer en casa.

En esta oportunidad te presentaré los mejores **postres sudafricanos** que definitivamente debes probar. ¡Buen provecho!

## Cuales son los postres de Africa

### Postre sudafricano Koeksisters

Empezamos con los famosos Koeksisters, un postre que hará que tus hijos se vuelvan locos. Estos dulces buñuelos fritos tienen una textura esponjosa por dentro y están cubiertos con un almíbar dulce y pegajoso. Para prepararlos, necesitarás:

* 500 g de harina
* 10 g de levadura en polvo
* 1/4 cucharadita de sal
* 2 cucharadas de mantequilla
* 1 huevo
* 250 ml de leche
* Aceite para freír

Para el almíbar:

* 500 g de azúcar
* 250 ml de agua
* 1/2 cucharadita de esencia de vainilla
* 1/2 cucharadita de canela en polvo

Mezcla los ingredientes secos en un bol, agrega la mantequilla y mezcla hasta obtener una textura similar a las migas. En otro recipiente, bate el huevo y la leche, y luego agrégalo a la mezcla de harina. Amasa hasta obtener una masa suave. Deja reposar durante 30 minutos, luego estira la masa y córtala en tiras. Trenza las tiras y fríe en aceite caliente hasta que estén doradas y crujientes.

Mientras tanto, prepara el almíbar hirviendo el azúcar, el agua, la vainilla y la canela hasta obtener una consistencia de almíbar. Sumerge los koeksisters en el almíbar caliente y déjalos absorber durante unos minutos. ¡Listo para disfrutar!

### Malva Pudding

Esta receta es muy conocida entre las antiguas colonias holandesas de Sudáfrica. Se elabora a partir de **yemas de huevo, mantequilla, leche y mermelada de albaricoque.** Sin embargo, cada cocinera puede hacer su propia versión. Por ejemplo, puede utilizar cualquier conserva en vez de albaricoque.

![Budín de malva](images/uploads/budin-de-malva2.jpg "Budín de malva")

Este budín tiene una textura esponjosa y húmeda. La mezcla se coloca en un bol para **hornear.** Aunque parece un pastel se le considera budín debido a su **textura mojada,** esto se debe a la concentración de mermelada. El budín de malva es uno de los platos típicos de la Ciudad del Cabo.
Si buscas un postre reconfortante y delicioso, no puedes dejar de probar el Malva Pudding. Esta maravilla esponjosa se sirve caliente y está impregnada de un irresistible sabor a caramelo. Aquí tienes la receta:

* 1 taza de harina
* 1 taza de azúcar
* 2 cucharaditas de bicarbonato de sodio
* 1 huevo
* 1 cucharada de mantequilla
* 1 cucharada de vinagre blanco
* 1 taza de leche
* 1 taza de azúcar moreno
* 1 taza de agua hirviendo
* 1/2 taza de crema espesa

Precalienta el horno a 180°C. En un bol, mezcla la harina, el azúcar y el bicarbonato de sodio. En otro recipiente, bate el huevo, la mantequilla, el vinagre y

 la leche. Combina los ingredientes secos y líquidos y vierte la mezcla en un molde engrasado. Hornea durante aproximadamente 40 minutos o hasta que esté dorado y firme al tacto.

Mientras tanto, prepara el almíbar mezclando el azúcar moreno, el agua hirviendo y la crema espesa. Una vez que el pudín esté listo, vierte el almíbar caliente sobre él. Deja que el pudín absorba el almíbar durante unos minutos antes de servir. ¡Una verdadera delicia sudafricana!

### Melktert

Esta tarta será la favorita de tus hijos. Es una **crema a base de leche con un delicioso sabor a canela**, en ocasiones se le puede adicionar cardamomo o cáscara de naranja.

![Tarta de leche (Melktert)](images/uploads/tarta-de-leche-melktert-.jpg "Tarta de leche (Melktert)")

Este postre se consigue en cualquier panadería sudafricana. Claro, hay versiones con una elaboración más minuciosa que se ofrece en los mejores hoteles. Es un **postre frío y muy nutritivo.**

El Melktert, o pastel de leche, es un postre clásico sudafricano que se convertirá en uno de tus favoritos. Su base crujiente y su relleno cremoso te transportarán a una tierra de sabores maravillosos. Aquí está la receta para que puedas disfrutarlo en casa:

Para la base:

* 200 g de galletas María trituradas
* 100 g de mantequilla derretida

Para el relleno:

* 500 ml de leche
* 2 huevos
* 125 g de azúcar
* 50 g de maicena
* 1 cucharadita de esencia de vainilla
* 1 cucharadita de canela en polvo

Mezcla las galletas trituradas con la mantequilla derretida y presiona la mezcla en el fondo de un molde para tarta. En una cacerola, calienta la leche hasta que esté casi hirviendo. En un bol aparte, bate los huevos, el azúcar, la maicena, la vainilla y la canela. Vierte lentamente la leche caliente sobre la mezcla de huevo, revolviendo constantemente.

Vuelve a colocar la mezcla en la cacerola y caliéntala a fuego medio hasta que espese, revolviendo constantemente. Vierte el relleno en la base de galleta y espolvorea con un poco de canela en polvo. Refrigera durante al menos 2 horas antes de servir.

### Buñuelos de calabaza

Los buñuelos pueden servir como **comida ligera y como postre.** Se prepara haciendo puré la calabaza previamente hervida, se combina con harina leudante, azúcar morena, un huevo y un toque de vainilla. Se amasa y se le da forma de pequeños buñuelos.

![Buñuelos de calabaza](images/uploads/bunuelos-de-calabaza.jpg "Buñuelos de calabaza")

Después de hacer buñuelos se ponen a freír hasta que quede crujiente. Al final se le coloca canela con azúcar glass. ¡Es un postre delicioso! Además, **es una buena forma de que tus hijos coman verduras.**v

Estos [postres sudafricanos](https://es.wikipedia.org/wiki/Gastronomía_de_Sudáfrica) nos brindan una experiencia culinaria única y deliciosa. ¡No hay nada mejor que disfrutar de un pedacito de Sudáfrica en nuestra propia cocina! Así que atrevete a explorar nuevas culturas a traves del paladar de tu familia.

## ¿Qué influencia tienen las costumbres en lo que comemos?

Las costumbres influyen en la nutrición. En general, las personas se quedan atrapadas en sus propias costumbres restringiendo la admisión de nuevos alimentos.

Es cierto que los platos típicos surgen de los productos de la tierra que se dan en regiones específicas, no obstant**e la exportación permite que se pueda tener acceso a nuevos productos.**

En tal sentido, es beneficioso para la familia preparar platos típicos de otras regiones, o quizás **jugar con nuevos sabores y texturas.** Esto ofrece la ventaja de aprovechar los nutrientes de “los nuevos alimentos”.

## ¿Qué tan importante es la selección de alimentos?

**La comida saludable es aquella que incluye varios ingredientes naturales.** Las verduras, vegetales y frutas son una fuente de vitaminas y minerales. Al seleccionar frutas o verduras diferentes el cuerpo puede absorber nuevas vitaminas.

Los beneficios de una cuidadosa selección de alimentos se perciben en mayor **energía, sistema inmunológico más fuerte, piel sana y un peso saludable.**

En conclusión, es de suma importancia preparar nuevas recetas, esto contribuye a una alimentación variada y nutritiva. Además, ayudas a tus niños a disfrutar de sabores diferentes.