---
layout: post
slug: ocio-familiar
title: Pantallas en casa. Recuperemos el ocio familiar
date: 2019-04-04T18:19:29+02:00
image: /images/posts/ocio-familiar/p-ocio-familiar-carrera-sacos-oreo.jpg
author: maria
tags:
  - 6-a-12
  - planninos
  - eventos
---
Me ha sorprendido mucho y en varios aspectos el estudio de “Hábitos de Ocio Familiar desde la Irrupción de las Nuevas Tecnologías”, elaborado por Ipsos para [Oreo](https://www.oreo.com) porque ha revelado que, a pesar de que la mayoría de los padres encuestados desearía pasar más tiempo con sus hijos, ahora son ellos los que en un 50% piensan que es suficiente el tiempo que comparten, y el 29% prefieren dedicarlo a jugar con amigos y a juegos en pantallas.

## La mayoría queremos pasar más tiempo con nuestros hijos

Esto me desconcierta, pero creo que hemos recibido una lección como sociedad que dedica demasiado tiempo al trabajo y poco a la vida personal y familiar por la carencia de medidas adecuadas de [conciliación de la vida laboral y familiar](https://madrescabreadas.com/tagged/conciliacion).

Y digo que hemos recibido una lección como sociedad porque los niños son unos supervivientes, y parece que están aprendiendo se están conformando a ver poco a papá y a mamá, y se están refugiando en otras cosas, como los video juegos, los [juguetes conectados a internet](https://madrescabreadas.com/post/181073863949/juguetes-tecnologicos-conectados-internet) o las pantallas. Perdonad si soy demasiado cruda, pero es mi opinión personal.


## Tenemos que ofrecerles alternativas atractivas

Esto no quiere decir que esté todo perdido, sino que yo lo tomaría como un toque de atención para que hagamos familia, para que fomentemos las actividades que nos gustan a todos encontrando un punto de encuentro en el que si todos cedemos en alguna de nuestras preferencias, seguro que podemos llegar a pasarlo engrane todos juntos. Y os habla una madre de familia numerosa con hijos adolescentes y también pequeños , edades aparentemente incompatibles para compartir divertimiento, sobre todo si añadimos al grupo a los padres carcamanes (que pensarán ellos) ...

En mi opinión  el mensaje que debemos transmitirles es que estamos aquí para ellos, que queremos disfrutar haciendo cosas divertidas con ellos, y que tenemos alternativas molonas (no sé si esa palabra la siguen usando) a las pantallas. Lo que está claro es que hay que darles alternativas atractivas, no vale con decir: “vamos a pasar la tarde juntos”.

## Ocio en familia también con adolescentes

Ya sé que la adolescencia es una etapa más difícil de encajar todos juntos y que los niños y niñas a partir de los 12 años prefieren estar con sus amigos, pero aún así, el estudio de Oreo revela que un 35% de niños entre 12 y 16 años prefieren actividades al aire libre, sea con quien sea, a quedarse en casa con el videojuego.  Y si se trata de niños más pequeños, el porcentaje ese eleva al 81%. Vamos a aprovecharlo!!


## Recuperemos los juegos de mesa

Otra alternativa a las actividades en familia al aire libre, porque no siempre hay posibilidad de salir de casa por el clima o por las circunstancias, es la de los juegos de mesa, que ya sabéis que hay cantidad de ellos, para coger ideas os recomiendo el blog de [Bebé a Mordor](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwixjb6UmqLhAhXrYN8KHcN1AcgQFjAAegQIBBAC&url=https%3A%2F%2Fbebeamordor.com%2F&usg=AOvVaw0QKiWZgtBElBcC9NrC4_nZ). Además, según la encuesta el 80% de padres e hijos comparten juegos de mesa, así que no lo estamos haciendo tan mal!

## Otras actividades en familia

Otras alternativas a las pantallas es el deporte en familia, como [montar en bicicleta](https://madrescabreadas.com/post/175920815554/ensenar-montar-bici), [senderismo](https://madrescabreadas.com/post/173199088449/excursion-ninos-castillos-monteagudo), [talleres](https://madrescabreadas.com/post/171084055675/taller-caramelos-artesanales), las [excursiones con bocatas y cantimplora](https://madrescabreadas.com/post/161844189170/fiambreras-excursiones-ninos), o ver una buena peli en el cine, hay títulos que pueden atraer a todos, y aunque suponga situarse frente a una pantalla, creo que al implicar salir de casa y compartir peli, incluso palomitas, es muy diferente a que cada uno esté con su tablet en el salón de casa.

Os recomendaría ver una peli en casa (todos la misma) con palomitas, pero según la encuesta este electrodoméstico ha perdido totalmente su protagonismo encuentras vidas (quién nos lo iba a decir), pasando a ser totalmente residual en la mayoría de nuestros hogares.

## Oreo Festival, el gran encuentro de las familias

Pensando en todas aquellas familias que quieren pasar más tiempo de ocio juntos en diferentes actividades culturales, la marca de galletas Oreo puso en marcha el sábado pasado en la Casa de Campo de Madrid [Oreo Festival](https://oreofestival.es), un evento familiar en el que tanto padres como hijos y amigos pudieron disfrutar de un día de diversión conjunta bajo una premisa: no usar móviles, consolas ni ningún dispositivo electrónico para lograrlo.
! [carrera sacos evento oreo ocio familiar](imagen/posts/ocio-familiar/ocio-familiar-carrera-sacos-oreo)

“Con esta iniciativa, Oreo busca ayudar a las familias a tener momentos de encuentro y ocio conjuntos y de desconexión multimedia que además sean divertidos para padres e hijos”.


Si te ha parecido interesante, comparte!! Gracias!