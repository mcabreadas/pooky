---
author: maria
date: '2017-10-10T10:10:29+02:00'
image: /images/posts/prensa/p-selfie.jpg
layout: post
tags:
- tecnologia
- revistadeprensa
title: 'Colaboración con Bebés Más: fotos de mis hijos en Internet'
---

Tema interesante el que trata el blog Bebés y Más, y para el que Silvia Jiménez me pidió mi opinión como abogada. Yo enes tema soy muy radical con mis hijos, pero respeto y comprendo la postura de otras madres y padres siempre y cuando se respete la legalidad, ya que estamos hablando de menores de edad.

"Los padres, como ostentadores de la patria potestad de nuestros hijos, tenemos el deber de salvaguardar este derecho y de protegerlo hasta que cumplan la mayoría de edad"

"No quiero ponerme alarmista ni decir con todo esto que no hay que subir ni una fotografía de los niños a Internet, pero sí cuidar mucho cómo lo hacemos"

Yo personalmente, cuando subo fotos de mis hijos protejo su identidad y no doy sus nombres, localizaciones, colegio donde estudian ni ningún otro dato personal. Además, procuro que físicamente no se les vea la cara. Lo hago por prudencia.

Si aún así, queremos subir fotografías de nuestros hijos a Internet, lo que yo recomendaría es que les preguntemos primero su opinión y les pidamos permiso. Obviamente, cuando son bebés o están en el vientre materno eso no se puede hacer por eso yo abogo siempre por proteger su intimidad lo máximo posible.

<a href="https://www.bebesymas.com/consejos/puede-mi-hijo-denunciarme-por-subir-fotografias-suyas-a-las-redes-sociales" class='c-btn c-btn--active c-btn--small'>Leer artículo completo Bebés y Más</a>


Si te ha gustado comparte y suscríbete al blog para no perderte nada.