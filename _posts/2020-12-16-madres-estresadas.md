---
author: maria
date: '2020-12-16T11:19:29+02:00'
image: /images/posts/madres-estresadas/p-estresada.jpg
layout: post
tags:
- adolescencia
- invitado
title: Madres estresadas educando a jóvenes
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puedes inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
>  
> Hoy merendamos con una amiga 2.0 que hace tiempo traspasó la frontera virtual y se convirtió en casi de la familia. Raro es que abra Facebook o Twitter y no me salte su último comentario en la pantalla, lo que normalmente me hace esbozar una sonrisa o me incita a reflexionar.
> Marisa Casas Madres Estresadas es una de las mamás twiteras de la vieja guardia, y esas somos de otra pasta. Auténtica como ella misma y sin pelos en la lengua, siempre tiene una crítica para quien la merece y una palabra de aliento para quien la necesita.
> Marisa es de esas personas que son buenas sin esforzarse, y si te puede ayudar, te ayuda de corazón.
> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os presento a Marisa Casas Madres Estresadas:

Soy Marisa, tengo casi 60 años, y con un poco de suerte, si las vacunas nos dejan,  los podré celebrar con la familia.
Casada, con dos hijos universitarios y una madre a la que ya nos toca cuidar.

Me gusta coser, leer, las manualidades, las tardes de verano... y disfruto como una enana con este mundo de las redes sociales  

Twitter [@madresestresadas](https://twitter.com/madrestresadas?lang=es)

Facebook [@madresestresadassinfronteras](https://www.facebook.com/madresestresadassinfronteras)

Instagram [@madrestresada](https://www.instagram.com/madrestresada/)

![pareja junto al mar](/images/posts/madres-estresadas/bio.jpg)


## ¿Por qué el nombre de Madres Estresadas?

¿Tú sabes esos momentos de recoger niños en el cole y tener que llevar a uno al cumple de un amigo y a la otra a otro cumple, cada uno en un extremo del barrio, y decidir si ese día  olvidas las extra escolares y aprovechas para ir al mercado, o tú llevas a tu hijo y al amigo y la madre del amigo se lleva a tu niña, y abren las puertas del cole y ya ni sabes qué tienes que hacer porque los niños ya han decidido???

Pues eso...

## Educando a dos jóvenes

Pablo  acaba de cumplir 25 y María, 21... ¿dónde están mis bebés?

Eso de verles crecer tanto y tanto... es una mezcla de qué alegría me dais cada día de ver que sois tan autónomos, tan razonables, tan sensatos y ¿por qué crecen tan deprisa y me hacen tan mayor? ¿Por qué os vais a cenar con los amigos en vez de estar con mamá y papá tooodo el rato?

Cuando empecé en esto de las redes sociales leía  más de una discusión de si había que dar a los bebés  el chupete  o no, o teta o biberón...

Y mi frase era... os espero en ciencias o letras.

> Creo que el superpoder de mandar en ellos lo perdí cuando me dijo María con dos años que si tenía frío que me pusiera la chaqueta yo, y que la dejase jugar en paz.

Cada vez tengo más  claro que nuestra labor es más de aplaudir lo que vayan decidiendo y estar ahí por si se equivocan y abrazarles y apoyarles mientras deciden el siguiente paso.

## Y de repente un día se van de Erasmus

Y de pronto dice... estoy mirando el Erasmus y... aaayyy nooooo!!! Espérate a los 30!!!!

Hay un problema económico gordo con esto, te conceden una beca para que se vayan... pero empiezan a pagar casi cuando vuelven, así que hay que tener el dinero preparado.

Cada carrera y cada Universidad tiene unas normas, y también pueden personalizar algo en tiempo de estancia, en asignaturas... Pablo  estuvo todo el curso pasado y María, si puede irse, serán 4 meses... poco más sé. 

Cuando entran bachillerato  o te pones detrás de ellos cotilleando el ordenador o te vas quedando en un rincón sin acabar de entenderlo.

## Consejos para mis Erasmus

Consejos... de esos que no hacen caso, un puñado!!! Pórtate  bien, llama cada media hora, no te gastes dinero...

De los que importan... Muchos de cocina "donde fueres cocina lo que vieres". A Pablo le gusta mucho cocinar y sabe lo que se hace. Si no hay cebolla, usa algo que se parezca, o hazlo sin cebolla, que saldrá rico también.
No juntes ropa mojada con ropa seca, que la tendrás toda mojada... y poco más.

Pablo se fue a una residencia de estudiantes, a lo que aquí llamaríamos un estudio. Cocina, cama y rincón de estudiar en una misma sala, cuarto de baño y entrada, los muebles estaban, pero ni sábanas ni platos o sartenes...

Casi el único  consejo que me dejó  darle fue que buscase un vuelo para llegar con el día por delante, que comiese algo al llegar, que el hambre estresa mucho, y luego ya fuera pensando qué necesitaba urgente y qué podría pensar al día siguiente.

Como se iba para todo el año, y el clima de los países nórdicos  es mucho más  frío, pensamos que le haría falta comprar ropa sí o sí, así que mejor no sobrecargar maletas, y a la vuelta... pues mira qué no vas a usar aquí y busca quién lo pueda utilizar allí.

No merece la pena traerse tres platos, o los calcetines que vas a tirar al llegar

## La comunicación es clave para mi familia

Para lo que sí nos sentamos, aunque está hablado desde siempre, es lo de mantenernos informados.

Yo no escondo malas o buenas noticias. Tuvimos un problema grave, de salud en la familia. Se fue enterando de todo a la vez que los que estábamos aquí. Fue en mayo, imposible venir, tampoco podía hacer mucho aquí, pero a mí me tranquilizaba  contárselo, y él sabe que si se lo pide el cuerpo, se puede ir de juerga nos pase lo que nos pase aquí. 

No sé bien cómo se sintió tan lejos, pero en ningún momento pensé en ocultarle nada.

> Lo peor es cuando te llama y te dice "tengo fiebre, voy al médico" ahí sí que dan ganas hasta de comprarte un avión y tardar 5 minutos en recorrer Europa entera.

## Vivir la pandemia a distancia

Esto del Covid a distancia... como donde estaba había bastantes menos contagios que aquí, pero tanto nosotros como sus amigos le íbamos contando, y leía las noticias... suspendieron las clases en su Universidad, pero podían ir y seguir estudiando allí, y nadie llevaba mascarilla: 
-¡cómpralas! 
-Pero para qué?
-¡Para usarlas!
-Pero no las usa nadie.
-¡cómpralas, que aquí se agotaron!

Valoramos el que se volviera, aunque perdiese el curso, pero nos pareció que el riesgo de cruzar Europa, los aviones que "empezaban a escasear"... ¿hicimos bien? Pues ahora creo que sí.

Una de las cosas que más pena me da, es que se ha perdido, aunque  no del todo, lo de viajar por el país en el que estaba, lo de visitar amigos en otros países, o que le visitasen a él. Nosotros íbamos a finales de marzo a verle!!!


Todas las noches le mandaba wasap de buenas noches, también estaba hablado que yo agradecía  un montonazo cualquier respuesta, pero que obligatorio, obligatorio no era.

## Orgullo de hijos

La verdad verdadera  es que sigo con la boca abierta viendo en qué se van convirtiendo.
Y te confieso un secreto.
Mi siguiente miedo es...
Mamá,  papá... este/esta es mi novi@.

*Si te ha gustado comparte y suscríbete al blog para no perderte nada!*

*Photo portada by boram kim on Unsplash*