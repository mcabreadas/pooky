---
date: '2020-03-19T15:15:29+02:00'
image: /images/posts/dia-del-padre/p-beso.jpg
layout: post
tags:
- conciliacion
title: Dia del padre con mas permiso de paternidad
---

En 2020 todos los padres de España han recibido por adelantado el mejor regalo del Día del Padre posible. El [RDL 6/19 de 1 de marzo](https://www.boe.es/buscar/act.php?id=BOE-A-2019-3244) les regaló la ampliación del permiso de paternidad a 12 semanas para este año, y un nuevo nombre para el mismo más inclusivo con todo tipo de familias: "Prestación por nacimiento y cuidado del menor por el progenitor distinto de la madre biológica".

## En nuevo permiso de paternidad

Cuatro de estas semanas son de obligatorio disfrute después del parto, y las diez restantes pueden ser distribuidas al gusto según las necesidades de cada familia, incluso de forma intermitente hasta que el bebé cumpla los doce meses, ya sea de forma simultánea o sucesiva a la madre, siempre y cuando ambos progenitores no trabajen en la misma empresa y afecte la productividad de la misma.

![padre con recién nacido](/images/posts/dia-del-padre/recien-nacido.jpg)

Pero la Ley guarda otra sorpresa más para el 2021 cuando entre en vigor la tan ansiada equiparación con el permiso de maternidad a 16 semanas intransferibles.

## Una larga lucha por la equiparación de los permisos

La lucha por la equiparación de los permisos de maternidad y paternidad es antigua; todavía recuerdo cuando [reivindicábamos en las redes sociales hace 10 años](https://madrescabreadas.com/2011/10/21/mis-propuestas-legislativas-para-la-conciliación/) que esto sucediera, aunque yo siempre he sido más partidaria de que fueran transferibles para que cada familia se pudiera organizar como quisiera de forma libre, pero más vale esto que seguir como hasta ahora, o como yo estaba cuando nació mi primera hija hace 14 años, que con 2 días despachaban a la mayoría de los padres.

![bebé en brazos con chupete](/images/posts/dia-del-padre/chupete.jpg)

En 2007 pasaron a ser 15 días, y hasta 2017 no lo ampliaron hasta 4 semanas, para pasar a ser 5 semanas en 2018, 8 semanas en 2019 , 12 semanas en 2020 y 16 semanas en 2021.

## Permiso por corresponsabilidad en el cuidado del lactante

Pero en este Día del Padre tenemos más cosas que celebrar! Y es que los papás recientes también pueden disfrutar de una nueva prestación por corresponsabilidad en el cuidado del lactante que dura hasta los 12 meses de edad del bebé, pero siempre y cuando lo disfruten los dos progenitores, aunque de los 9 a los 12 meses sólo cobraría la prestación uno de los dos.


## La Ley promueve la corresponsabilidad

Me quedo con la definición que hace el RDL 6/19 del concepto de corresponsabilidad como colofón al regalo del Día de Padre 2020 porque sé que os va a gustar tanto como a mí y porque es la primera vez que se contempla en un texto legislativo algo tan necesario y tan reivindicado para la consecución de la igualdad real:

> Corresponsabilidad: “Reparto equilibrado de las tareas domésticas y de las responsabilidades familiares tales como su organización, el cuidado, la educación y el afecto de personas dependientes dentro del hogar con el fin de distribuir de manera justa los tiempos de vida de mujeres y hombres.”

![papá abrazando mujer embarazada](/images/posts/dia-del-padre/embarazo.jpg)


- *Photo by Andrea Bertozzini on Unsplash embarazo*
- *Photo by Mikael Stenberg on Unsplash beso*
- *Photo by Wes Hicks on Unsplash recién nacido*
- *Photo by Jude Beck on Unsplash bebé con chupete*