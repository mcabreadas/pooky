---
layout: post
slug: mi-hijo-no-quiere-estudiar
title: "Podcast: Qué tengo que hacer para que mi adolescente estudie"
date: 2022-12-28T12:40:19.035Z
image: /images/uploads/yellow-black-and-purple-modern-podcast-cover-800-800-px-1200-800-px-.png
author: .
tags:
  - podcast
---
Hoy nos descabreamos con Diana Al Azem [@adolescencia_positiva](https://www.instagram.com/adolescencia_positiva/) en Instagram, es una mamá granadina de dos adolescentes, profesora de secundaria y escritora de novela juvenil: “Escondidos entre aulas”, con la que llegó a ser número 1 en descargas en Amazon y que, finalmente, fue reeditada por el grupo Planeta bajo el título «El algoritmo del amor».

Diana jamás imaginó convertirse en una madre exasperada y gritona, pero se vio envuelta en ese círculo vicioso de manera automática. Ella Trataba de calmar su voz, pero tenía que repetir las cosas una y otra vez hasta que al final perdía la paciencia y, entre la presión y el estrés, la relación con sus hijos se veía dañada.

Ahora es la madre que siempre ha querido ser y está muy orgullosa de haberlo conseguido, por ella misma y por sus hijos.

Pero lo que ha aprendido no se lo ha quedado para sí misma, sino que ha formado una comunidad con más de 140.000 personas, la comunidad de  [Adolescencia positiva](https://adolescenciapositiva.com/), y no puede estar más feliz sabiendo que esas 140.000 familias comienzan a poner en práctica la educación respetuosa que todos merecemos y que hoy va a compartir contigo, conmigo, con las madres cabreadas para que lo estemos un poco menos

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/7iogYd4cnoB1j0jOxNGSKe?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Diana responde a las preguntas que habeis dejado en Instagram para ella:

## ¿Qué puedo hacer si mi hijo no quiere estudiar?

\-Donde se acaban los consejos para llegar a la exigencia, o no es positiva la exigencia después de haber dialogado y aconsejado?

\-A veces nos encontramos paralizadas porque hacen algo que sabemos que no debemos permitir, sabemos lo que no debemos hacer, entendemos por qué actúan así, pero no sabemos cómo reaccionar, teniendo en cuenta que si cerebro no está desarrollado, y sabiendo que no son del todo responsables de algunos de sus reacciones o comportamientos, como corregir comportamientos inaceptables respetando su proceso de biológico. ¿Donde está el punto?

\-Corregir a largo plazo es ok, pero como atajamos una situación que explota y debe ser reducida ya: violencia, peligro…

\-Sigue estando muy extendida la cultura del chancletazo?

## ¿Qué pasa cuando un adolescente no quiere estudiar?

\-Muchas madres me han comentado que sus hijos andan despistados, con la cabeza en otra parte y que no hay manera de que se centren y se sienten a estudiar. ¿como podemos motivarlos?

\-Qué papel tenemos que adoptar las madres? Cómo debemos actuar para que estudien?

\-¿es conveniente premiar si sacan buenas notas o castigar si sacan malas notas?

\-¿es conveniente que hagan extraescolares si no aprueban todo en la evaluación?

Diana tambien nos presenta su nuevo programa [Brújula](https://adolescenciapositiva.com/labrujula/), con precio especial hasta el 1 de enero de 2023, dirigido a adolescentes que se sienten un poco perdidos y necesitan una orientacion para encontrar su camino, superar sus obstaculos y encontrar respuestas.
<iframe width="560" height="315" src="https://www.youtube.com/embed/R1wHd47ctFg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>