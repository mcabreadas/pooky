---
author: maria
date: '2021-02-10T10:13:29+02:00'
image: /images/posts/vlog/p-internet-segura.jpg
layout: post
tags:
- adolescencia
- tecnologia
- vlog
- seguridad
- crianza
title: Vlog Día de Internet segura. Qué hacer frente a los peligros de las redes sociales
---

Los días 9 y 10 de febrero de 2021 se celebra el Día de Internet Segura “Safer Internet Day” (SID, por sus siglas en inglés), unas fechas para tomar consciencia de la importancia de hacer un uso seguro y positivo de las tecnologías digitales sobre todo entre niños y jóvenes, y de protegernos y proteger a nuestros hijos en la red.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CwLiK23h_UY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Te he ayudado? Ahora ayúdame tú a mí si quieres: suscríbete y comparte!