---
author: ana
date: '2021-02-14T09:10:29+02:00'
image: /images/posts/dulces-de-san-valentin/p-trufas.png
layout: post
tags:
- recetas
title: 5 dulces de San Valentín fáciles
---

¿El amor es grande y el tiempo corto? ¿Se te viene la fecha encima? ¿Te gusta la cocina, pero no eres como se dice una experta cocinera? ¿Cómo responder con sencillez y de manera espléndida a la amistad y al amor que la vida te dispensa?

He aquí unas **recetas sencillas, para que el paladar de San Valentín se alegre y endulce** con trufas, galletas, chessecake chocolate y fresas. Veámoslas. 

## Qué se puede hacer para el día de los enamorados
![galletas-de-corazon](/images/posts/dulces-de-san-valentin/galletas-de-corazon.png)

**Sin dudar, galletas.** Mezclas en un bowl 180 gr de mantequilla, dos tazas de harina de trigo todo uso, una taza de harina leudante y una taza de azúcar. Luego agregas el huevo junto a una cucharadita y media de vainilla. Unes todos los ingredientes hasta formar una masa. La llevas al refrigerador por dos horas o mejor de una día para el otro. Al cabo estiras las masa y con un cortador, modelas las galletas como desees y llevas al horno 10 minutos a 200°.  

Y si las quieres decorar con un glaseado, anota: bate una clara de huevo y le vas agregando poco a poco 250 gr de azúcar pulverizada sin dejar de batir. Agrégale también un chorrito de limón o vinagre. Como aromatizante puedes añadir media cucharadita de vainilla. Una vez listo decides si vas a usar algún colorante para darle un toque especial.

![chesscake](/images/posts/dulces-de-san-valentin/chesscake.png)

**Fácil de hacer también, un chessecake** y más si nos encontramos una receta que no lleve horno. Para ello, troceamos y pulverizamos un paquete de galletas María (150 gr.) y se le añaden 65 gr. de mantequilla derretida. Se revuelve hasta hacer una masa la cual se colocará en la base del molde que elijamos, desmoldable o enharinado.

Para el relleno, un sobre de gelatina del sabor que nos guste lo disolvemos en 200 ml de leche y lo llevamos en el fuego a hervor. Se retira del fuego y dejamos entibiar. Por otro lado, se mezclan 200 gr. de nata y 250 gr de queso filadelfia y se le añade la gelatina.

La mezcla la vertemos en un molde de preferencia desmoldable con la base de galleta y llevamos a enfriar unas cuatro horas. Para la cobertura, preparamos una gelatina de fresa o frambuesa y la colocamos al tope de la chessecake, llevamos nuevamente a la nevera.

## Dulces para San Valentín

**Trufas de coco y leche condensada.** Sólo necesitas 140 gr de coco rallado y 140 gr de leche condensada. En un bowl juntamos la leche y 100 gr del coco, revolvemos bien y llevamos a la nevera por una hora. Luego ya podremos hacer las trufas y las rebosamos con los 40 gr de coco.

Otra ideas para San Valentín nos las sugieren los deliciosos usos del chocolate, ese maravilloso liberador de endorfinas.

**Trufas de chocolate con leche condensada.** Derretimos 200 gr. chocolate blanco o negro. Por otro lado derretimos 100 gr. de mantequilla y le añadimos 100 gr. de leche condensada. Revolvemos bien y añadimos el chocolate derretido. Seguimos revolviendo hasta el punto en que la mezcla se despegue del cazo. Cubrimos con papel film y presionamos para que no tenga aire.  

![chocolate](/images/posts/dulces-de-san-valentin/chocolate.png)

Dejamos enfriar y llevamos a la nevera, mejor hasta el otro día. Llegado el momento, con una cucharita sacamos porciones de la mezcla, hacemos bolitas que podemos rebozar con cacao en polvo si es chocolate negro o azúcar glass si es chocolate blanco. Y al refrigerador, hasta la hora de servir.

Muchas opciones nos ofrecen las trufas, si las rebozamos con nueces molidas, coco rallado, o lluvia de colores.

![fresas-con-chocolate-oscuro](/images/posts/dulces-de-san-valentin/fresas-con-chocolate-oscuro.png)

Y si tienes unas lindas **fresas, las puedes bañar en chocolate blanco o negro fundido**, las colocas en un molde con papel parafinado y llevas a la nevera hasta la hora de servir.

Con las **galletas, las trufas, las fresas y el chocolate** sólo falta la alegría revoloteante de los tuyos para que la fecha se haga especial e inolvidable.


Photo de portada by [amirali mirhashemian](https://unsplash.com/@amir_v_ali?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/truffles?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
Photo by [Luis González](https://unsplash.com/@porfgonzs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/biscuits?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
Photo by [Obed Hernández](https://unsplash.com/@yabbath?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash]
Photo by [Tamas Pap](https://unsplash.com/@tamasp?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/white-chocolate-truffles-with-icing-sugar?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
Photo by [Jessica Johnston](https://unsplash.com/@jdjohnston?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash]