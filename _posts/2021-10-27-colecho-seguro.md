---
layout: post
slug: colecho-seguro
title: ¿Es seguro hacer colecho con mi bebé?
date: 2022-02-16T14:10:21.985Z
image: /images/uploads/colecho-seguro.jpg
author: faby
tags:
  - crianza
---
Hace varias décadas los padres tenían la costumbre de **dormir con sus bebés,** era algo que ni siquiera se cuestionaba, pero los tiempos han cambiado.

Ahora no solo se le coloca al bebé en su propia cuna sino también en su propia habitación. Algunas sociedades pediátricas no recomiendan el colecho bajo ninguna circunstancia, debido a ciertos estudios epidemiológicos en el que relacionan el colechar con el **síndrome de muerte súbita del lactante.**

Pero, **¿es realmente malo el colecho?** En esta entrada hablaremos de todo lo que está implicado en el colecho.

## [](<>)¿Qué es el Colecho?

El colecho es un término relativamente nuevo que no aparece en el diccionario de la RAE (Real Academia Española). A pesar de ello, este término evoca a**l “lecho o cama”.** Es decir, a la costumbre de **compartir la cama o el lecho con los hijos.** Puede tratarse de tu misma cama, o una cuna unida a tu cama.

![Familia durmiendo en colecho](/images/uploads/istockphoto-1184108281-612x612.jpg "Familia durmiendo en colecho")

La verdad es que esta costumbre era valorada por la comunidad, por lo que era habitual en el pasado, pero también es popular actualmente en muchas culturas. Por ejemplo, **en la India 93% de los niños de entre 3 y 10 años duermen con sus padres.** Esto permite que haya un mayor nivel de vínculo y apego.

Hoy se ha vuelto un tema algo polémico, por lo que algunos padres dudan, colecho sí o no.

## [](<>)Beneficios del Colecho

El colecho permite que se desarrolle y **fortalezca los vínculos emocionales entre los padres y el bebé.** Además, hace posible que el bebe se sienta seguro. A continuación, indicaremos los principales **beneficios de esta práctica según estudios realizados por profesionales.**

### [](<>)Favorece la lactancia materna

Hay evidencia científica que señala que las fórmulas lácteas infantiles pueden acarrear problemas estomacales al bebé. En cambio, la leche materna favorece el crecimiento y desarrollo del bebé e incluso previene el Síndrome de Muerte Súbita del Lactante.

El colecho facilita la lactancia debido a que **el crío está muy cerca de su madre, por lo que puede “servirse de la leche” sin siquiera llorar.**

De acuerdo a un artículo publicado en la[](https://scielo.isciii.es/scielo.php?script=sci_arttext&pid=S1139-76322012000100010#:~:text=Colecho%20se%20define%20como%20la,dormir%20juntos%20en%20un%20sof%C3%A1.) *[Revista Pediátrica Atención](https://scielo.isciii.es/scielo.php?script=sci_arttext&pid=S1139-76322012000100010#:~:text=Colecho%20se%20define%20como%20la,dormir%20juntos%20en%20un%20sof%C3%A1.) Primaria de Madrid* se concluyó mediante varios estudios que “**El colecho favorece la práctica de la lactancia materna y no aumenta el riesgo de muerte súbita del lactante**”.

![Madre alimentando a su bebé](/images/uploads/istockphoto-1090396168-612x612.jpg "Madre alimentando a su bebé")

Aunque los objetores del colecho destacan otros estudios, se pudo constatar que hay una baja incidencia SMSL.

### [](<>)Mejora el ciclo de sueño

Tanto el ciclo de sueño, la respiración y la temperatura se puede sincronizar con tu peque, favoreciendo un mejor descanso para ambos. A su vez, **sentir la cercanía transmite sensación de seguridad.**

Según cierto estudio, cuando el bebé percibe la presencia de sus padres se estabiliza su ritmo cardíaco, mejora su nivel hormonal y se fortalece el sistema inmune. Se puede compartir la misma cama o colocar una cuna colecho.

### [](<>)Los bebés lloran menos

Los bebés suelen llorar por varias razones: frío, inseguridad, hambre, etc. Pues bien, al **colechar le das calor, le ofreces seguridad y lo sacias con la leche materna,** así que tu hijo llorará menos. De este modo, le evitas dolores de garganta, dolor de cabeza, entre otras molestias que pueden surgir tras llorar mucho.

### [](<>)Beneficia a los padres

Los padres tienden a agotarse en los primeros meses. Sin embargo, el colecho permite que el niño duerma más tiempo, esto también favorece a los padres, ya que de este modo su **sueño no se ve interrumpido constantemente.**

## [](<>)¿Por qué es malo el colecho?

El **colecho seguro no es malo,** todo lo contrario es beneficioso tanto para el bebé como para los padres. Sin embargo, hay ciertas precauciones que se deben tomar en cuenta. De hecho, la **Asociación Española de Pediatría** señala que el colecho no debe hacerse en colchones blandos o en un sofá.

Aunque es evidente que si se han dado muertes en niños que dormían con sus padres, en general se trata de las **circunstancias en la que se da el colecho.** Por ejemplo, se analizó los datos de 400 bebés con muerte súbita, comparándolos con 1386 niños vivos. La cifra parece muy alta, pero al verificar los datos se descubrió que algunos dormían en un sofá, mientras en otros casos uno de los padres o los dos consumían alcohol.

Entonces, si los padres duermen con el bebé siguiendo las recomendaciones de los organismos de salud oficiales, el riesgo de muerte súbita no es mayor que los que duermen en su cuna.

## [](<>)¿Cuánto tiempo dura el colecho?

[El tiempo que dure el colecho es una decisión de los padres](https://madrescabreadas.com/2022/01/10/hasta-cuando-colecho/). No obstante, La OMS recomienda que “**el bebé comparta la habitación contigo durante los primeros 6 meses”**. Esto permite que puedas dar lactancia de forma más cómoda. Además, generalmente el niño tiende a comer más, lo que contribuye a su desarrollo.

Ahora bien, algunos médicos tienen su propia opinión con relación al tiempo que debe durar el colecho. Uno de ellos es Nils Bergman, **Pediatra de la Universidad de Ciudad del Cabo,** Sudáfrica quien llevó a cabo algunos estudios sobre patrones del sueño, ha sido categórico al señalar que **“los niños deben dormir con los padres hasta los 3 años”.**

Por otro lado, Margot Sunderland señala en su libro “La ciencia de ser padres” que los niños deben dormir junto a sus padres hasta los 5 años.

**Practicar el colecho en niños grandes** es una decisión personal que puede estar influenciado por la cultura. En algún punto, los padres tendrán que hacer la [transición del niño a su propia cama](https://madrescabreadas.com/2014/06/26/sueno-feliz-madrescabreadas/).

## [](<>)¿Cuándo no se puede hacer colecho?

![Cuando no se debe hacer colecho](/images/uploads/istockphoto-1220143383-612x612.jpg "Cuando no se debe hacer colecho")

El colecho puede darse de forma ocasional o habitual. Ahora bien, hay ciertas circunstancias en las que dormir con tu peque no será una buena decisión. Presta atención a los siguientes inconvenientes en el colecho:

* Si alguno de los padres está enfermo.
* El colecho no debe practicarse en bebés prematuros.
* No se recomienda dormir con niños menores de 3 meses.
* No hacerlo si experimentas cansancio extremo.
* Los padres no deben fumar, consumir drogas o sedantes.

En conclusión, **el colecho fomenta el bienestar del niño o noña, incentiva el desarrollo neuronal y le aporta seguridad y calma**. Al mismo tiempo, favorece el vínculo entre padres e hijos. Claro, para reducir el riesgo de ahogo o aplastamiento, debes colocar la cuna justo al lado de la cama y seguir las recomendaciones de la OMS.

Te interesa: [Como hacer tu propia cuna de colecho a partir de una cuna de Ikea](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/)

*Photo Portada by YakobchukOlena on Istockphoto*

*Photo by evgenyatamanenko on Istockphoto*

*Photo by Amax Photo on Istockphoto*