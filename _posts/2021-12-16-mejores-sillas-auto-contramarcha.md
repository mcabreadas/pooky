---
layout: post
slug: mejores-sillas-auto-contramarcha
title: Sillas de coche para ir a contra marcha el mayor tiempo posible
date: 2022-01-19T11:33:31.448Z
image: /images/uploads/tumblr_inline_o39z09r6yk1qfhdaz_540.jpg
author: faby
tags:
  - puericultura
---
Los viajes familiares nos unen como familia, sin embargo, es de suma importancia asegurarnos de que los niños vayan seguros. ¿Qué silla de coche es la más segura? Según indican las estadísticas **las sillas a contra marcha son las más seguras**, tal y como os contamos en este post sobre [falsos mitos de llevar a los niños a contramarcha en el coche](https://madrescabreadas.com/2014/11/12/sillas-automovil-seguridad-ninos/), ya que protegen al niño de mejor forma tras un frenazo inesperado.

Hay muchos modelos de sistemas de retención infantil a contramarcha. En esta oportunidad te mostraré algunos modelos cuya relación calidad precio nos parece adecuada. ¡Empecemos!

## Babify Onboard Silla de Coche Giratoria 360º

**Este modelo dispone de 11 posiciones** para que el peque vaya cómodo y seguro. 

Puede ser usada por niños recién nacidos y grandes, **¡Soporta más de 30 kg!** Es reclinable y espaciosa. Dispone de todos los accesorios de seguridad.

Un aspecto destacado es su **diseño giratorio**, el cual hace posible que la instalación en el coche sea mucho más fácil. ¿Sabías que los niños deben viajar a contra marcha al menos hasta los 4 años? Puedes usarla a contra marcha o de frente cuando crezca.

[![](/images/uploads/babify-onboard1.jpg)](https://www.amazon.es/Babify-Onboard-Silla-Coche-Giratoria/dp/B07RYWKS9Z/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas%2Ba%2Bcontramarcha&qid=1638407489&sr=8-2&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Babify-Onboard-Silla-Coche-Giratoria/dp/B07RYWKS9Z/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas%2Ba%2Bcontramarcha&qid=1638407489&sr=8-2&th=1&madrescabread-21) (enlace afiliado)

## BRITAX RÖMER Silla Coche DUALFIX Z-LINE Gira 360°

Si buscas flexibilidad esta es tu silla ideal. Es capaz de posicionarse frontal o de reversa. Incluso **permite realizar un giro de 90° para quedar dispuesto hacia la puerta del coche** en cualquier dirección aún estando en una posición reclinada.

Posee una capacidad de hasta 18 kg (de **3 meses a 4 años**) con una arquitectura robusta pero ligera. Es muy fácil de instalar y brinda al bebé una comodidad única.

[![](/images/uploads/britax-romer.jpg)](https://www.amazon.es/Britax-R%C3%B6mer-Silla-coche-meses/dp/B0823WZ2P8/ref=sxin_15?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&asc_contentid=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&asc_contenttype=article&ascsubtag=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&creativeASIN=B0823WZ2P8&cv_ct_cx=sillas%2Ba%2Bcontramarcha&cv_ct_id=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&cv_ct_pg=search&cv_ct_we=asin&cv_ct_wn=osp-single-source-pecos-desktop&keywords=sillas%2Ba%2Bcontramarcha&linkCode=oas&pd_rd_i=B0823WZ2P8&pd_rd_r=e0c9c882-8a20-40d8-b60a-0b8fdedec864&pd_rd_w=PjsgZ&pd_rd_wg=obFtv&pf_rd_p=7d9f144b-2893-43c3-8ff0-628fadff0e55&pf_rd_r=DMG7K8N5R85ZBMY3GTXV&qid=1638407489&sr=1-1-c84eb971-91f2-4a4d-acce-811265c24254&tag=compramejo0bb-21&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Britax-R%C3%B6mer-Silla-coche-meses/dp/B0823WZ2P8/ref=sxin_15?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&asc_contentid=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&asc_contenttype=article&ascsubtag=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&creativeASIN=B0823WZ2P8&cv_ct_cx=sillas%2Ba%2Bcontramarcha&cv_ct_id=amzn1.osa.2350f756-0b4e-4074-a567-039be04c6256.A1RKKUPIHCS9HS.es_ES&cv_ct_pg=search&cv_ct_we=asin&cv_ct_wn=osp-single-source-pecos-desktop&keywords=sillas%2Ba%2Bcontramarcha&linkCode=oas&pd_rd_i=B0823WZ2P8&pd_rd_r=e0c9c882-8a20-40d8-b60a-0b8fdedec864&pd_rd_w=PjsgZ&pd_rd_wg=obFtv&pf_rd_p=7d9f144b-2893-43c3-8ff0-628fadff0e55&pf_rd_r=DMG7K8N5R85ZBMY3GTXV&qid=1638407489&sr=1-1-c84eb971-91f2-4a4d-acce-811265c24254&tag=compramejo0bb-21&th=1&madrescabread-21) (enlace afiliado)

## Nurse Driver 2 Silla de Coche Grupo 0 1 2 3

En caso de que tus necesidades incluyan un mayor peso, la silla contramarcha Nurse **ofrece un soporte de 36 Kg.** Catalogándola como un equipo de larga duración. 

También se incluye un **reductor para recién nacidos**.

Si tu coche no incluye el sistema Isofix esta silla es la ideal para ti. **Puedes asegurar la silla con el cinturón de seguridad prescindiendo del Isofix**. En el interior tu bebé siempre se sentirá muy cómodo gracias a la perfecta sincronía entre la reclinación y el ajuste del arnés.

[![](/images/uploads/nurse-driver-2.jpg)](https://www.amazon.es/Nurse-Jan%C3%A9-Driver-unisex-Isofix/dp/B07R4W6KMC/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas+a+contramarcha&qid=1638407489&sr=8-1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Nurse-Jan%C3%A9-Driver-unisex-Isofix/dp/B07R4W6KMC/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas+a+contramarcha&qid=1638407489&sr=8-1&madrescabread-21) (enlace afiliado)

## Silla de Coche Grupo 0 1 2 3 Isofix Star Ibaby Travel 2.0

**¿Deseas hacer una compra que dure toda la infancia de tu hijo?** Este modelo pueden usarlo los niños recién nacidos hasta los 12 años. Es decir, acompañará a tu crío durante toda la infancia.

Es una silla a contra marcha con sistema giratorio para que puedas colocar al niño con mayor comodidad. A medida que crezca dispones de Isofix regulable, sistema de Protección lateral SPS.

Se caracteriza por ser ampliamente reclinable, lo que aporta confort y seguridad a tu hijo. Además, incluye capota parasol.

[![](/images/uploads/isofix-star-ibaby.jpg)](https://www.amazon.es/Silla-Isofix-Star-Ibaby-Travel/dp/B08X13N3X6/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas%2Ba%2Bcontramarcha&qid=1638407489&sr=8-3&th=1%EF%BB%BF&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Silla-Isofix-Star-Ibaby-Travel/dp/B08X13N3X6/ref=sr_1_3?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=sillas%2Ba%2Bcontramarcha&qid=1638407489&sr=8-3&th=1%EF%BB%BF&madrescabread-21) (enlace afiliado)

## ¿Qué es viajar a contra marcha?

Viajar a contra marcha **significa colocar al bebé mirando hacia atrás o al sentido contrario a la marcha del auto.** Esta posición protege al niño de lesiones graves en caso de colisión.

Un freno inesperado puede causar tracción sobre el cuello del niño. En cambio, según las estadísticas, **viajar a contra marcha protege al peque de la energía del impacto.**

## ¿Hasta cuándo se debe llevar el bebé a contra marcha?

Según la [normativa i-Size](https://www.fundacionmapfre.org/educacion-divulgacion/seguridad-vial/temas-clave/sistemas-retencion-infantil/sillas-mas-seguras/i-size/) señala que los niños **deben viajar a contra marcha hasta los 15 kg.** De hecho, cuanto más tiempo mejor, porque mas seguros irán.

Los peques no tienen suficiente fuerza cómo para soportar una desaceleración. El cinturón de seguridad no protege su cuello. Por eso, lo mejor es llevar a los niños a contra marcha todo el tiempo posible.

Te puede interesar este post sobre las [sillas de coche mas estrechas del mercado](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/).