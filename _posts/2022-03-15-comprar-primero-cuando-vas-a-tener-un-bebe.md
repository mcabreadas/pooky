---
layout: post
slug: comprar-primero-cuando-vas-a-tener-un-bebe
title: Qué necesito comprar o que me presten para la llegada del bebé
date: 2022-03-18T10:27:24.134Z
image: /images/uploads/embarazada-ir-de-compras.jpg
author: faby
tags:
  - puericultura
  - embarazo
---
Mientras se acerca día tras día la fecha fijada para el alumbramiento, los nervios y la emoción se intensifican. **Queremos tener todo listo** para que su recibimiento sea cómodo y placentero.

Es imposible no ponernos nerviosas, y la avalancha de anuncios publicitarios nos inundan. Pero no te preocupes más, te ayudaré a repasar una lista muy útil de todo lo que debes tener preparado para recibir a tu pequeño retoño sin contratiempos.

Si vas a trasladarlo del hospital a casa en coche, necesitaras un [sistema de retencion de automovil o silla de coche](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/).

## Pañales

Evidentemente tu bebé necesitará usar muchas veces este recurso, así que no lo debes desestimar. Te recomiendo **no comprar mucha cantidad en tallas pequeñas**, pues estos peques crecen muy rápido en pocos días. Por tal razón los kit de [pañales para recién nacido](https://madrescabreadas.com/2021/05/27/panales-carrefour-opiniones/) combinan la cantidad ideal para no desperdiciar en la compra

[![](/images/uploads/dodot-sensitive-kit-recien-nacido.jpg)](https://www.amazon.es/Dodot-Sensitive-Kit-Reci%C3%A9n-Nacido/dp/B0937QKCYW/ref=sr_1_6?keywords=pa%C3%B1ales+recien+nacido&qid=1647363341&sprefix=pa%C3%B1ales+recien%2Caps%2C3445&sr=8-6&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Dodot-Sensitive-Kit-Reci%C3%A9n-Nacido/dp/B0937QKCYW/ref=sr_1_6?keywords=pa%C3%B1ales+recien+nacido&qid=1647363341&sprefix=pa%C3%B1ales+recien%2Caps%2C3445&sr=8-6&madrescabread-21) (enlace afiliado)

Una vez vayan creciendo, deberás ir ajustando el tamaño del pañal según la contextura del bebé y su edad. Más grandecitos si puedes optar por comprar un pack más resuelto para evitar ir de compras a cada momento.

[![](/images/uploads/panales-para-bebe-huggies-ultra.jpg)](https://www.amazon.es/dp/B074KN47GS/ref=redir_mobile_desktop?_encoding=UTF8&aaxitk=c314f39c3e69197e7688147263820406&hsa_cr_id=6988000380302&pd_rd_plhdr=t&pd_rd_r=10fc0e5a-62cd-41dc-92de-024af17c459f&pd_rd_w=aUSXx&pd_rd_wg=5Zto1&ref_=sbx_be_s_sparkle_mcd_asin_0_img&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B074KN47GS/ref=redir_mobile_desktop?_encoding=UTF8&aaxitk=c314f39c3e69197e7688147263820406&hsa_cr_id=6988000380302&pd_rd_plhdr=t&pd_rd_r=10fc0e5a-62cd-41dc-92de-024af17c459f&pd_rd_w=aUSXx&pd_rd_wg=5Zto1&ref_=sbx_be_s_sparkle_mcd_asin_0_img&madrescabread-21) (enlace afiliado)

## Cuna de colecho

Si eres una de las mamás que se han decidido por el *[colecho](https://madrescabreadas.com/2022/02/16/colecho-seguro/)* por todas sus [bondades](https://www.aeped.es/comite-nutricion-y-lactancia-materna/lactancia-materna/documentos/colecho-sindrome-muerte-subita-lactante-y), pues ya es momento de **adquirir una cuna que más se adapte a tu cuarto** y tus gustos. En el mercado hay infinidades de modelos y marcas a escoger, pero te dejo un par recomendado.

[![](/images/uploads/chicco-next2me-cuna-de-colecho.jpg)](https://www.amazon.es/Chicco-Next2Me-Colecho-Apertura-Ajustable/dp/B08KFKGQ1C/ref=sr_1_10?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=cuna+colecho&qid=1647363985&sr=8-10&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Chicco-Next2Me-Colecho-Apertura-Ajustable/dp/B08KFKGQ1C/ref=sr_1_10?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=cuna+colecho&qid=1647363985&sr=8-10&madrescabread-21) (enlace afiliado)

La cuna es una compra obligada, bien sea por ser tu primer bebé o quieras reemplazarla.

Tambien puedes [fabricar tu propia cuan de colecho a partir de una normal](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/).

[![](/images/uploads/skiddou-cuna-de-madera-para-ninos.jpg)](https://www.amazon.es/skiddo%C3%BC-Ajustable-colchones-barandilla-extra%C3%ADbles/dp/B0964DWBFK/ref=sr_1_2_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=cuna%2Bcolecho&qid=1647363985&sr=8-2-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyNDc1SkRGSUdMNDQxJmVuY3J5cHRlZElkPUEwMDk1MDU0MVlCWVpQT0E2QjJZTiZlbmNyeXB0ZWRBZElkPUEwODQ5NTY0U1ROREIwVDM1RDdJJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/skiddo%C3%BC-Ajustable-colchones-barandilla-extra%C3%ADbles/dp/B0964DWBFK/ref=sr_1_2_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=cuna%2Bcolecho&qid=1647363985&sr=8-2-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyNDc1SkRGSUdMNDQxJmVuY3J5cHRlZElkPUEwMDk1MDU0MVlCWVpQT0E2QjJZTiZlbmNyeXB0ZWRBZElkPUEwODQ5NTY0U1ROREIwVDM1RDdJJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21) (enlace afiliado)

## Biberón para leche y agua

Ciertamente la *[lactancia materna trae numerosos beneficios](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwiH9O2L1cj2AhWY8eMHHQv4DPgYABAAGgJ5bQ&ae=2&ohost=www.google.com&cid=CAESWuD2UdZp0rEWmBXrrqfiVc49dD-TBRWpVoNoEFc5nM8VCSmtBlaKr9iwJiRUaOkOLzKSg6o6iPR5-hM8NK_QtrajNpx3m54blB1PQTo-nSC6EeqXbrT7SMvXsg&sig=AOD64_1vBsp7Csv47-K-3sYfzdQvgnPx_w&q&adurl&ved=2ahUKEwiqiOaL1cj2AhW7RjABHUJRBNsQ0Qx6BAgDEAE)* a la salud tanto de la madre como del bebé. Sin embargo, podemos apoyar esta excelente práctica con excelentes **biberones ideados especialmente para simular nuestro pecho**.

[![](/images/uploads/suavinex-zero.jpg)](https://www.amazon.es/dp/B07J2JQP9N?tag=exmpd-21&linkCode=osi&th=1&psc=1&keywords=biberon%20lactancia%20materna%20suavinex&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B07J2JQP9N?tag=exmpd-21&linkCode=osi&th=1&psc=1&keywords=biberon%20lactancia%20materna%20suavinex&madrescabread-21) (enlace afiliado)

Además, puedes usar biberones para agua antiderrames, donde también evitan que tu bebé se ahogue.

[![](/images/uploads/philips-avent2.jpg)](https://www.amazon.es/dp/B07F98PKK6?tag=biberon-com-es-21&linkCode=osi&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B07F98PKK6?tag=biberon-com-es-21&linkCode=osi&th=1&madrescabread-21) (enlace afiliado)

## Bañera

El aseo del crío es una actividad diaria y constante, pues **los bebés tienden a ensuciarse con mucha facilidad y frecuencia**. Tener a la mano una bañera es un elemento ideal para hacer de este proceso más cómodo y práctico.

[![](/images/uploads/chicco-cuddle-bubble-banera.jpg)](https://www.amazon.es/Chicco-Cuddle-Bubble-Cambiador-Plegable/dp/B08LM1KGCL/ref=sr_1_2?keywords=ba%C3%B1era+bebe&qid=1647367502&sprefix=ba%C3%B1era+%2Caps%2C468&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Chicco-Cuddle-Bubble-Cambiador-Plegable/dp/B08LM1KGCL/ref=sr_1_2?keywords=ba%C3%B1era+bebe&qid=1647367502&sprefix=ba%C3%B1era+%2Caps%2C468&sr=8-2&madrescabread-21) (enlace afiliado)

La seguridad que aportan las bañeras no tienen igual, evitando caídas por resbalón y haciendo la experiencia del baño agradable al bebé.

[![](/images/uploads/babify-lagoon-banera-plegable.jpg)](https://www.amazon.es/Babify-Lagoon-Plegable-Plegado-compacto/dp/B087PN1SPJ/ref=sr_1_4?keywords=ba%C3%B1era+bebe&qid=1647367502&sprefix=ba%C3%B1era+%2Caps%2C468&sr=8-4&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Babify-Lagoon-Plegable-Plegado-compacto/dp/B087PN1SPJ/ref=sr_1_4?keywords=ba%C3%B1era+bebe&qid=1647367502&sprefix=ba%C3%B1era+%2Caps%2C468&sr=8-4&madrescabread-21) (enlace afiliado)

## Coches de paseo

La llegada del bebé no tiene que apresarte en la casa. Los coches de paseo son el ayudante **ideal para esas caminatas matutinas** tan necesarias, para que el peque tome su primer baño de sol de la mañana y también para que tu puedas estirar las piernas y realices tus pendientes.

[![](/images/uploads/lionelo-amber-coche-de-bebe.jpg)](https://www.amazon.es/multifuncional-Conjunto-regulable-Mosquitero-Protector/dp/B08WHW1XLD/ref=sr_1_1_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1JYLFXMZOSDQB&keywords=cochecito%2Bbebe&qid=1647370908&sprefix=cochecito%2Bbebe%2Caps%2C417&sr=8-1-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUE4QVhYT05TODA1REkmZW5jcnlwdGVkSWQ9QTA1NzI3MTAyUlBPWkZVU0cxQVUyJmVuY3J5cHRlZEFkSWQ9QTA1NjE3NDYzUDRXVFdXV1BIWFI1JndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/multifuncional-Conjunto-regulable-Mosquitero-Protector/dp/B08WHW1XLD/ref=sr_1_1_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1JYLFXMZOSDQB&keywords=cochecito%2Bbebe&qid=1647370908&sprefix=cochecito%2Bbebe%2Caps%2C417&sr=8-1-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUE4QVhYT05TODA1REkmZW5jcnlwdGVkSWQ9QTA1NzI3MTAyUlBPWkZVU0cxQVUyJmVuY3J5cHRlZEFkSWQ9QTA1NjE3NDYzUDRXVFdXV1BIWFI1JndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ&th=1&madrescabread-21) (enlace afiliado)

## Portabebés: Fular o Bandolera

Si los coches te dan libertad, **el portabebés te dará alas.** Mientras el bebé se mantiene calentito y tranquilo por estar muy cerquita de ti, tú podrás tener los brazos libres. La experiencia que da un portabebés no tiene explicación, debes comprobarlo por ti misma.

[![](/images/uploads/mochila-y-fular-portabebes-en-uno-.jpg)](https://www.amazon.es/Manduca-4250371703598-Mochila-portabeb%C3%A9-duo/dp/B0759QMN9V/ref=sr_1_6?keywords=fular+portabebe&qid=1647372962&sprefix=fular%2Caps%2C804&sr=8-6&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Manduca-4250371703598-Mochila-portabeb%C3%A9-duo/dp/B0759QMN9V/ref=sr_1_6?keywords=fular+portabebe&qid=1647372962&sprefix=fular%2Caps%2C804&sr=8-6&madrescabread-21) (enlace afiliado)

Las camisetas de porteo son fenomenales para salir a pasear con tu retoño y disfrutar del aire libre

[![](/images/uploads/camiseta-de-porteo.jpg)](https://www.amazon.es/Camiseta-Portabeb%C3%A9s-Amarsupiel-Manga-larga/dp/B01J5BLGD4/ref=sr_1_58_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2ESMZRVAPCCF1&keywords=bandolera%2Bportabebe&qid=1647373060&sprefix=bandolera%2Bportabebe%2Caps%2C618&sr=8-58-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyNUFFOFlJRDZHTktIJmVuY3J5cHRlZElkPUEwOTM1MTY5MTJOUlhJRVNJNFpOSyZlbmNyeXB0ZWRBZElkPUEwMDQ2NTc3MlFOTzJMU0pCTlVLNyZ3aWRnZXROYW1lPXNwX2J0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Camiseta-Portabeb%C3%A9s-Amarsupiel-Manga-larga/dp/B01J5BLGD4/ref=sr_1_58_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2ESMZRVAPCCF1&keywords=bandolera%2Bportabebe&qid=1647373060&sprefix=bandolera%2Bportabebe%2Caps%2C618&sr=8-58-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyNUFFOFlJRDZHTktIJmVuY3J5cHRlZElkPUEwOTM1MTY5MTJOUlhJRVNJNFpOSyZlbmNyeXB0ZWRBZElkPUEwMDQ2NTc3MlFOTzJMU0pCTlVLNyZ3aWRnZXROYW1lPXNwX2J0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

## Silla Comer o Trona

Prontito tu criatura comenzará a **comer sus primeros alimentos sólidos**, así que te será muy útil una sillita de comer o trona, donde podrá ir adquiriendo poco a poco buenos hábitos para alimentarse.

[![](/images/uploads/maxi-cosi-moa-trona-8-en-1.jpg)](https://www.amazon.es/Maxi-Cosi-Ajustable-Evolutiva-Multiusos-Graphite/dp/B094D7SV3M/ref=sr_1_1_sspa?keywords=trona+bebe&qid=1647372175&sprefix=trona+%2Caps%2C1322&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEySVlWMUFRVVFXWU9ZJmVuY3J5cHRlZElkPUEwOTQzMzEwMks5WVMxWVBBM1IzVCZlbmNyeXB0ZWRBZElkPUEwNjE5MTM0M0JLR0lGQ0kxUFlaTCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Maxi-Cosi-Ajustable-Evolutiva-Multiusos-Graphite/dp/B094D7SV3M/ref=sr_1_1_sspa?keywords=trona+bebe&qid=1647372175&sprefix=trona+%2Caps%2C1322&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEySVlWMUFRVVFXWU9ZJmVuY3J5cHRlZElkPUEwOTQzMzEwMks5WVMxWVBBM1IzVCZlbmNyeXB0ZWRBZElkPUEwNjE5MTM0M0JLR0lGQ0kxUFlaTCZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&madrescabread-21) (enlace afiliado)

Una de las ventajas de esta, es que podrá acompañar a toda la familia a la mesa a la hora de comer, compartiendo este momento tan unificador familiar.

[![](/images/uploads/trona-de-madera.jpg)](https://www.amazon.es/Safety-1st-Cherry-aproximadamente-Natural/dp/B08QWM7X5F/ref=sr_1_14?keywords=trona+bebe&qid=1647372175&sprefix=trona+%2Caps%2C1322&sr=8-14&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Safety-1st-Cherry-aproximadamente-Natural/dp/B08QWM7X5F/ref=sr_1_14?keywords=trona+bebe&qid=1647372175&sprefix=trona+%2Caps%2C1322&sr=8-14&madrescabread-21) (enlace afiliado)

## Mueble cambiador

Parece ser un elemento descartable, pero créeme que no lo es. Contar con un cambiador es muy cómodo y práctico, sobre todo teniendo en cuenta las veces que lo usarás al día.

[![](/images/uploads/roba-26016-v105-mueble-cambiador.jpg)](https://www.amazon.es/Roba-26016-V105-Mueble-cambiador/dp/B005EVNEOE/ref=sr_1_8?crid=2PU1DHDMSSYZO&keywords=mueble+cambiador+bebe&qid=1647372336&sprefix=mueble+cambiador%2Caps%2C368&sr=8-8&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Roba-26016-V105-Mueble-cambiador/dp/B005EVNEOE/ref=sr_1_8?crid=2PU1DHDMSSYZO&keywords=mueble+cambiador+bebe&qid=1647372336&sprefix=mueble+cambiador%2Caps%2C368&sr=8-8&madrescabread-21) (enlace afiliado)

Puedes hallar en el mercado una gran cantidad de modelos, desde los más simples hasta los más completos.

[![](/images/uploads/cambiador-de-espuma-plastificada.jpg)](https://www.amazon.es/Jan%C3%A9-080263-S91-Colchones-cambiador/dp/B01N5V35QN/ref=sr_1_12?crid=2PU1DHDMSSYZO&keywords=mueble+cambiador+bebe&qid=1647372336&sprefix=mueble+cambiador%2Caps%2C368&sr=8-12&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Jan%C3%A9-080263-S91-Colchones-cambiador/dp/B01N5V35QN/ref=sr_1_12?crid=2PU1DHDMSSYZO&keywords=mueble+cambiador+bebe&qid=1647372336&sprefix=mueble+cambiador%2Caps%2C368&sr=8-12&madrescabread-21) (enlace afiliado)

Hasta aquí dejaré la lista. Seguramente cada necesidad varía según nuestro entorno y costumbres, así que habrá una que otra cosa que querrás añadir a esta lista. **Lo más importante es recibir a nuestro crío con todo el amor que se merece.**



Photo Portada by M_a_y_a on Istockphoto