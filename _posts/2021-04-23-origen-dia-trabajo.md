---
author: ana
date: 2021-04-29 09:35:27.258000
image: /images/uploads/teletrabajo.jpg
layout: post
slug: origen-dia-trabajo
tags:
- conciliacion
- derechos
title: 'Origen del día del trabajo: ¿por qué se celebra el 1 de mayo?'
---

Este 1 de mayo debemos preguntarnos qué queda de las 8 horas de trabajo. Una bandera levantada por los trabajadores y por la que hubo a finales del siglo XIX y durante todo el siglo siguiente, ¡y lo que va del XXI, innumerables manifestaciones.

El [teletrabajo](https://madrescabreadas.com/2020/09/27/teletrabajo/) y las diversas formas de flexibilización del [Empleo en España](https://es.jooble.org) y en todo el mundo, han convertido aquellas horas en un recuerdo casi mítico. Sin embargo, están en la base de lo que celebramos como el día del trabajador.

![infografia conciliacion trabajo familia](/images/uploads/infografia.jpg)

Ahora bien, no porque el tiempo laboral se encuentre desdibujado entre lo presencial y lo on line, han desaparecido los conflictos y reclamos. Justamente por estos días una gigantesca multinacional enfrenta problemas de imagen porque trascendió que sus empleados trabajan por encima de las 8 horas y el tiempo de hacer sus necesidades está estrictamente vigilado.

Ello sin contar que con el trabajo en casa que crece de manera exponencial, desaparece la organización sindical y entremezcla las tareas de jefes difusos con las labores cotidianas (por ejemplo, atención duplicada a los hijos y exigencias de teleeducación), hoy acrecentadas por la pandemia.

Quiza te interesen estos post sobre las dificultades diarias de las madres trabajadoras:

[Crianza vs. trabajo, mi decision](https://madrescabreadas.com/2017/01/24/maternidad-crianza-conciliacion/)

[Mujer trabajadora que no logra conciliar y se cabrea](https://madrescabreadas.com/2015/03/08/dia-mujer-conciliacion/)

[Cuidados domesticos como trabajo](https://madrescabreadas.com/2021/03/11/vlog-dia-mujer/)

![teletrabajo conciliacion trabajo tareas domesticas](/images/uploads/trabajo-en-casa.jpg)

## Qué celebramos el día del trabajador

Celebramos la importancia de los trabajadores y trabajadoras en la construcción de la economía mundial. Llamar la atención sobre ello, al menos un día al año, vale para procurar visibilizar lo que con frecuencia no se advierte. Que las fábricas no paren, que las calles estén limpias, que los alimentos lleguen al mercado es posible porque hay detrás trabajadores.

## Por qué el 1 de mayo es el día del trabajador

El 1 de mayo como día del trabajador nació en el Congreso Obrero Socialista de la Segunda Internacional, celebrado en París en 1889. Participaron organizaciones obreras y laboristas que buscaban en el marco del capitalismo industrial naciente una mejora en la legislación y exigían 8 horas para trabajar, 8 para dormir y 8 para la casa.

La fecha del 1 de mayo fue postulada en esa reunión en Bruselas, para reivindicar el sacrificio de los llamados "Mártires de Chicago".

## Este es el origen del día del trabajo

En efecto, el 1 de mayo de 1886 comenzó en Chicago, capital industrial de EEUU una huelga que duró tres días. Al cuarto estalló lo que se conoce como la Revuelta de Haymarket, en la que participaron unos 20 mil obreros.

Así fueron los hechos: durante el discurso del anarquista Samuel Fielden ante la multitud un desconocido arrojó una bomba contra la policía.

Fue la excusa que detonó la represión y desató la violencia que dejó 38 obreros muertos y más de 100 heridos. Las crónicas de la época detallan que seis policías alcanzados por el explosivo murieron en el hospital. Los destrozos y la pérdida de vidas humanas de parte de la fuerza pública dieron el leitmotiv para que los líderes de las protestas (sindicalistas con formación política) fueran procesados y cinco, condenados a la horca.

Paradójicamente, en el país donde sucedieron estos acontecimientos, no se celebra el 1 de mayo sino el "Labor Day" el primer lunes de septiembre. Por otro lado, la iglesia católica a través del papa Pío XII, en 1955 instituyó el 1 de mayo como el día de San José Obrero, con el propósito de reconocer la dignidad del trabajo.

Photo de portada by alevision.co on Unsplash

*Photo by Shridhar Gupta on Unsplash*

*Photo by Bench Accounting on Unsplash* 

*Photo by Alexander Dummer on Unsplas*h