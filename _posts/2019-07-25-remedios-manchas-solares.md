---
layout: post
slug: remedios-manchas-solares
title: Remedios para eliminar las manchas solares
date: 2019-07-25T07:00:00+02:00
image: /images/posts/2019-07-25-remedios-manchas-solares/p-nariz-pecosa.jpg
author: .
tags:
  - embarazo
  - salud
---
No sé si es sensación mía pero este año el sol está más potente que nunca, por eso en casa llevamos un cuidado extremos y aplicamos siempre foto protector antes de exponernos directamente. Aún así, me ha salido una mancha sobre el labio superior que me lleva por el camino de la amargura. Sé que hay tratamientos de [laser facial](http://cirugiaesteticabarcelona.es/dermatologia-laser/) para eliminarlas, y tengo amigas que me los han recomendado mucho, incluso me han hablado de clínicas concretas como [centros de estética barcelona](https://cirugiaesteticabarcelona.es), pero también sé que existen tratamientos con cremas, por eso pregunté a mi comunidad de Facebook por experiencias con la eliminación de manchas solares, y obtuve múltiples soluciones: remedios caseros, tratamientos de cosmética... pero sin duda ganó por goleada el láser. 

No obstante, voy a compartir con vosotras lo que he aprendido sobre el tema, y espero que me dejéis en comentarios vuestro consejo si habéis tenido alguna experiencia al respecto.

## Tipos de manchas solares

No todas las manchas de nuestra piel son iguales, ni todas son provocadas por el sol, ni todas son perjudiciales, aunque debemos tener cuidado porque algunas pueden resultar incluso cancerígenas.

Podemos distinguir dos tipos principales de manchas, los léntigos, en los que aumenta el número de melanocitos, que al estar pigmentados se ven como una mancha y son  directamente consecuencia del exceso de exposición solar y el paso del tiempo, y los melasmas, que son debidos habitualmente a cambios hormonales, como el embarazo, la toma de anticonceptivos, determinados fármacos como antibióticos, antiacné, diuréticos...

Los léntigos son manchas oscuras y marrones, planas e irregulares, y suelen aparecer en las zonas que más exponemos al sol, como cara, escote y manos.  Pero no las confundamos con las pecas (de ésas yo tengo bastantes, por cierto) ya que éstas son congénitas y más pequeñas, mientras que la mancha forma como una placa y es bastante más antiestética.
![piel de mujer con pecas](/images/posts/2019-07-25-remedios-manchas-solares/ojos-cara-pecosa.jpg)

## ¿Los remedios caseros son efectivos contra las manchas solares?

Es cierto que estas manchas no desaparecen solas, ni es fácil que se vayan con cosméticos o remedios caseros, pero quizá podrían aclararlas un poco, aunque tengo claro que no es la solución definitiva, y que la misma pasa por un tratamiento médico.

Me han sugerido, aunque no he probado, aplicar una infusion de té de perejil todas las noches o una mascarilla de cúrcuma y limón una vez a la semana. ¿Conocéis más remedios caseros?

## ¿Los cosméticos quitan las manchas solares?

Está claro que algunos cosméticos ayudan a paliarlas, incluso algunas experiencias dicen que se han llegado a eliminar, pero no es mi caso.

Hay jabones y cremas con vitamina C, cremas despigmentantes, leche virginal, rosa mosqueta... que se pueden probar, pero a mí no me han funcionado.


![mujeres playa bikini](/images/posts/2019-07-25-remedios-manchas-solares/mujeres-playa-bikini.jpg)

## Tratamentos estéticos para quitar manchas solares

Por lo que he investigado existen al menos dos tratamientos para las manchas de la piel causadas por el sol, y que un dermatólogo deberá aconsejarnos cuál es el adecuado para nuestro caso.

La crioterapia consiste en congelar el lentigo con nitrógeno líquido de manera que la mancha se descama y se cae, generalmente tras varias aplicaciones. 
Este tratamiento desgraciadamente lo conozco bien por dos vías: las verrugas de mi hijo pequeño, que son testarudas, y se empeñan en resurgir a pesar de las numerosas aplicaciones, y las manchas en la piel de mi padre que le suelen salir en el cuero cabelludo y cara, pero que lleva muy controladas con este método que, aunque doloroso, es efectivo y, en su caso, necesario. 

El láser que generalmente se suele utilizar el Erbio-Yag, Alejandrita o Q-Switched. Los aparatos láser actúan específicamente sobre la mancha de la piel, ya que captan el color de la melanina, y una vez aplicado la mancha cambia y se produce una costra, que viene siendo una lesión en respuesta a la agresión que ha sufrido nuestra piel, y que a los pocos días desaparecerá, pero hay que cuidarla con crema cicatrizante, y evitar totalmente la nueva exposición solar. 

## Prevención

Ya habréis adivinado, si habéis leído hasta aquí, que la conclusión que podemos sacar de todo esto es que lo más importante es la prevención, tanto para nosotras, como para nuestros hijos e hijas. Tema que no es nuevo en este blog de familia, y que he tratado en otros posts como ["Protección solar para la piel de niños y bebés en verano"](https://madrescabreadas.com/2014/07/11/crema-solar-bebes-madrescabreadas/) y ["Las gafas de sol para niños son más que moda"](https://madrescabreadas.com/2016/07/19/gafas-sol-ninos/).

Ahora necesito que me cuentes tu experiencia si has tratado de eliminar alguna mancha solar de tu piel. ¿Cómo te ha ido?

*Photo by Artem Beliaikin on Unsplash*

*Photo by Simon John-McHaffie on Unsplash*

*Photo by Tom Rogers on Unsplash*

*Photo by Jonathan Borba on Unsplash*