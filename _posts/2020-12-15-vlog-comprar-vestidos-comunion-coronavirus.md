---
date: '2020-12-15T11:19:29+02:00'
image: /images/posts/vlog/p-vlog-comunion.jpg
layout: post
tags:
- familia
- moda
- comunion
title: Vlog Consejos para comprar el vestido de comunión 2021 y sorteo casa rural
  Moratalla
---

Hoy os traigo la entrevista que me hizo Kika Frutos sobre consejos para comprar el vestido de comunión en plena pandemia por coronavirus.

Al final del video: Sorteo de una estancia para una familia de hasta 6 personas en la casa rural Cora de Tudmir de Mortalla, Murcia. Podéis participar en el [Instagram de Madres Cabreadas](https://www.instagram.com/p/CIv-rdBlYoj/?utm_source=ig_web_copy_link)


Comprar el vestido de Comunión de niña o el traje de Comunión de niño, según lo que os toque, es algo con lo que andamos algo perdidas porque no sabemos cuándo encargarlo, dónde comprarlo, o incluso hay quien se plantea si comprarlo o no dado lo incierto de la situación y la posibilidad de que se vuelvan a suspender las ceremonias religiosas y celebraciones.

Por eso he querido daros unos consejos prácticos sobre lo que debemos tener en cuenta a la hora de comprar el traje de Comunión de nuestros hijos en plena pandemia de coronavirus, una situación atípica totalmente y que hace que tengamos que adaptarnos a un posible cambio de fecha incluso talla del niño, y por ello debemos comprar con todas las garantías y en una tienda de confianza.

No te olvides se suscribirte al canal y compartir!


<iframe width="560" height="315" src="https://www.youtube.com/embed/hcSTTvZ4QVg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).