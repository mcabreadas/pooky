---
author: maria
date: '2021-03-04T09:10:29+02:00'
image: /images/posts/dia-mujer/p-mujeres-en-fabrica.png
layout: post
tags:
- mujer
title: ¿Por qué se celebra el 8 de marzo el Día Internacional de la Mujer?
---

Las tragedias en las textileras se suceden con una frecuencia escandalosa. La más reciente ocurrió en febrero de este año, en Marruecos en una fábrica clandestina ubicada en un sótano: 28 personas murieron electrocutadas.

La mayoría de las víctimas son mujeres en condiciones de trabajo infrahumanas. En muchas ocasiones, al iniciarse el incendio, no han podido escapar porque las puertas están cerradas por fuera, lo que revela niveles de barbarie y esclavitud impensables en pleno siglo XXI.

## El día Internacional de la Mujer
Que las condiciones de trabajo sean dignas y que la mujer tenga igualdad de derechos políticos y económicos forman parte de la agenda de los reclamos históricos en torno a este día, nacido en la Segunda Conferencia de Mujeres Socialistas de Copenhague el 27 de agosto de 1910 y que fue ratificado por la Organización de Naciones Unidas en 1977 con la Resolución 32/142.

## Por qué el Día internacional de la Mujer se celebra el 8 de marzo

![por-la-defensa-de-los-derechos](/images/posts/dia-mujer/por-la-defensa-de-los-derechos.png)
Extrañamente existen muchas versiones, pero reconstruyamos un hilo breve a partir de la primera manifestación ocurrida a mediados del siglo XIX.

### 8 de marzo de 1857

Un grupo de trabajadoras textiles sale a las calles de Nueva York para protestar por las míseras condiciones en las que trabajaban.

### 29 de febrero de 1908

Se reivindica aquella primera protesta cuando se organiza en Estados Unidos el primer Día Internacional de la Mujer, para exigir derechos políticos y económicos.

### 15 de marzo de 1908

El 8 de marzo de 1908 estalla una huelga que arroja siete días después, el 15 de marzo, un saldo trágico: 129 mujeres mueren en un incendio en Washington Square, New York, en la textilera Cotton Textile Factory, provocado por el dueño de la fábrica, que quería impedir la protesta de las trabajadoras.

### 27 de agosto de 1910

En la Segunda Conferencia de Mujeres Socialistas que tiene lugar en Copenhague, Dinamarca, el 27 de agosto de 1910, se establece el 8 de marzo como Día Internacional de la Mujer.

![igualdad-y-equidad](/images/posts/dia-mujer/igualdad-y-equidad.png)

## La propuesta la hizo Clara Zetkin

La precursora del feminismo, Clara Zetkim (Alemania,  5 de julio de 1857 – 20 de junio de 1933), quien consiguió el derecho al voto de la mujer alemana, fue quien propuso celebrar el Día Internacional de la Mujer el 8 de marzo en honor a las 129 costureras de New York, que murieron tras manifestarse en huelga para exigir un sueldo digno, la reducción de la jornada de trabajo a diez horas y la prohibición de utilizar mano de obra infantil.


*Photo de portada by Claudio Schwarz on Unsplash*

*Photo by Birmingham Museums Trust on Unsplash*

*Photo by Claudio Schwarz on Unsplash*

*Photo by Claudio Schwarz on Unsplash*