---
layout: post
slug: educacion-inteligencia-emocional
title: Educar las emociones para prevenir el bullying
date: 2021-07-08T10:12:17.720Z
image: /images/uploads/image.jpeg
author: maria
tags:
  - invitado
  - educacion
---
Hoy merendamos con [Irene Muñiz](https://www.instagram.com/docenteconsciente/?utm_medium=copy_link) para apoyar su lucha por una educacion emocional en los colegios, mas alla de la formacion academica para formar personas mentalmente sanas, equilibradas y, en definitiva, felices.

## ¿Qué es la educación emocional según Rafael Bisquerra?

La definicion de educacion emocional es un proceso educativo, continuo y permanente, que pretende potenciar el desarrollo emocional como complemento indispensable del desarrollo cognitivo, constituyendo ambos los elementos esenciales del desarrollo de la personalidad integral.

Os dejo con Irene:

<a href="https://www.osoigo.com/es/irene-muniz-2-los-centros-educativos-deben-tomar-conciencia-sobre-el-tipo-de-persona-que-quieren-formar-por-que-no-se-fomenta-la-educacion-emocional-desde-las-aulas-involucrando-a-los-padres-y-madres-en-el-proceso.html?utm_source=Whatsapp&utm_medium=Whatsapp&utm_campaign=Irene" class='c-btn c-btn--active c-btn--small'>Firma por una educacion emocinal en los colegios</a>

Hola. Soy Irene Muñiz, maestra de infantil, profesora de magisterio de educación física y con un máster en educación emocional. Desde hace más de 11 años convivo con niños y su desarrollo evolutivo, tanto cognitivo como emocional. A lo largo de mi trayectoria he podido observar muchas situaciones de carencias emocionales, tanto en los más pequeños como en los adultos que se responsabilizan de estos niños. Después de mucho tiempo rumiendo la importancia que tiene la educación emocional en nuestro día a día, me he lanzado y he propuesto una pregunta al parlamento andaluz 

### ¿Por qué no se fomenta la educación emocional desde las aulas involucrando a los padres y madres en el proceso?

![los maestros son los verdaderos influencers](/images/uploads/image-2.jpeg)



## ¿Qué es y para qué sirve la inteligencia emocional?

Cómo todos sabemos, los niños y niñas son la base de nuestra sociedad, por lo tanto debemos centrarnos en hacer todo lo posible para que estos pequeños crezcan seguros, con un buen autoconcepto, resilientes y capaces de saber tolerar la frustración y gestionar las emociones menos agradables, en vez de darse por vencidos a la primera de cambio. Y para ello, es de vital importancia que nosotros los adultos, los que convivimos con ellos y los que les trasmitimos esos **nutrientes emocionales**, debamos esforzarnos para poder tener esas **herramientas** y ser capaces de “aprender a ser” para poder transferir esos conocimientos a los más pequeños. 

En muchos casos, los propios padres han vivido una infancia llena de carencias emocionales y lo han normalizado, por lo que no ven necesario que sus hijos lo aprendan. Quizá si en los centros educativos y más instituciones se empiezan a implicar en la educación emocional y a impartir la importancia que tiene en nuestro día a día, probablemente mejoraría nuestra salud mental, tanto personal como sociedad. 
La convivencia en las escuelas sería mucho más empática y **evitaríamos el [acoso y el bullying](https://madrescabreadas.com/2019/06/14/acoso-escolar-protocolo/)**. 

<a href="https://www.osoigo.com/es/irene-muniz-2-los-centros-educativos-deben-tomar-conciencia-sobre-el-tipo-de-persona-que-quieren-formar-por-que-no-se-fomenta-la-educacion-emocional-desde-las-aulas-involucrando-a-los-padres-y-madres-en-el-proceso.html?utm_source=Whatsapp&utm_medium=Whatsapp&utm_campaign=Irene" class='c-btn c-btn--active c-btn--small'>Firma por una educacion emocinal en los colegios</a>

#### Es necesario que los centros educativos tomen conciencia sobre el tipo de personas que quieren formar. 

> Más que una buena calificación, lo más valioso es que los niños aprendan a desarrollar su inteligencia emociona.

Pero esto no es todo. La educación emocional es cosa de dos partes: escuela y familia. El papel de los padres y madres es imprescindible para que los niños y niñas crezcan adquiriendo todos los conocimientos y los sepan aplicar en la vida cotidiana. La **salud emocional de los adultos determina los cimientos de la base emocional de los menores**, y es ahí donde debemos incidir. La participación activa de los padres y  madres en esta etapa es fundamental para nutrir las habilidades y reforzar la conducta positiva que el niño posee.

Cuando los niños y niñas reconocen sus emociones y cuando saben que sus progenitores están listos para hablar con ellos cuando lo necesiten, aprenden a comunicarse mejor consigo mismo y con los demás.

Si queremos que nuestros pequeños sean **ciudadanos responsables, solidarios, resilientes, autodidacticas, colaboradores** … debemos crear programas de ducacion emocional específicos para lograrlo. Por ello, como andaluza y docente me dirijo a los miembros del Parlamento de Andalucía para que se fomenten los proyectos de educación emocional en las aulas que involucren a padres y madres.

Firma por una educacionemocional en los colegios!

<a href="https://www.osoigo.com/es/irene-muniz-2-los-centros-educativos-deben-tomar-conciencia-sobre-el-tipo-de-persona-que-quieren-formar-por-que-no-se-fomenta-la-educacion-emocional-desde-las-aulas-involucrando-a-los-padres-y-madres-en-el-proceso.html?utm_source=Whatsapp&utm_medium=Whatsapp&utm_campaign=Irene" class='c-btn c-btn--active c-btn--small'>Firma por una educacion emocinal en los colegios</a>

Comparte para conseguir muchos mas apoyos!