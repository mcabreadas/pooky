---
author: maria
date: 2021-06-04 16:01:00.833000
image: /images/uploads/captura-de-pantalla-2021-06-04-a-las-18.03.46.png
layout: post
slug: tiktok-para-adultos
tags:
- adolescencia
- revistadeprensa
title: 'Colaboración con El País: Tiktok, el puente hacia nuestros adolescentes'
---

Mi primera colaboracion con El Pais, en su blog De Mamas & de Papas. Una bitacora que sigo hace muchos años, y que siempre me ha encantado por su importante difusion sobre temas impotantes sobre el mundo infantil, adolescente, la maternidad y la familia. 

Tras haber recopilado parte del [vocabulario adolescente y jerga juvenil actual](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/), me hace especial ilusion haber podido aportar mi vision sobre el papel que pueden jugar las redes sociales en la comunicacion con nuestros adolecentes.

Os dejo el articulo: [Tiktok, el puente hacia nuestros adolescentes aqui](https://elpais.com/mamas-papas/2021-06-02/tiktok-el-puente-hacia-nuestros-adolescentes.html).

Espero que os sea de utilidad.

![tik tik adolescentes](/images/uploads/image_6487327-7.jpg)