---
layout: post
slug: consejos-vacaciones-con-adolescentes
title: Consejos para sobrevivir a las vacaciones con tus adolescentes
date: 2023-08-07T11:00:55.682Z
image: /images/uploads/photo_2023-08-07-13.17.31.jpeg
author: .
tags:
  - revistadeprensa
  - adolescencia
---
Escuchame a partir del minuto 35 en Onda Cero, programa Gente Viajera.
Te doy consejos para [viajar con tus adolescentes](https://madrescabreadas.com/2022/11/07/viajes-originales-adolescentes/) y evitar conflictos para lograr unas vacaciones en familia en las que todos disfruteis.

<iframe loading="lazy" src="https://www.ondacero.es/embed/gente-viajera-06082023/audio/2/2023/08/06/64cf9c79ea26b0e48b45e1e2" width="560" height="315" frameborder="0" allowfullscreen></iframe>