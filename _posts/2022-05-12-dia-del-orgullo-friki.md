---
layout: post
slug: dia-del-orgullo-friki
title: Ideas para celebrar el día del orgullo friki
date: 2022-05-25T14:38:52.480Z
image: /images/uploads/dia-friki.jpg
author: faby
tags:
  - planninos
---
**¡Puedes sentirte orgulloso de ser un friki!** Ser un experto en la literatura o contenido fantástico, ciencia ficción, cómics no es motivo de vergüenza.

El día del orgullo friki se inventó en España, y alude a todo tipo de **afición obsesiva de películas, series, comics, videojuegos, libros,** etc. Es un día en el que puedes gritar al mundo que eres un auténtico fan de los Juegos de tronos, The BigBang Theory, Star Wars, etc. Además, puedes disfrutar de fabulosos descuentos en muchas tiendas.

Además, puede servir de excusa para acercarte másá a tus adolescentes y poder hacer alguna actividad juntos (si no les da mucho cringe...)

Te dejo este [diccionario de palabras que usan los adolescente](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/)s para que los entiendas mejor.

En esta entrada, te ofreceré algunas recomendaciones para que puedas disfrutar el orgullo friki este 25 de mayo.

## Organiza tu propia Friki parade

Sabemos que este tipo de fiestas son multitudinarias, lo que supone un problema con la actual pandemia, que aunque ha ido disminuyendo, todavía hay que seguir tomando precauciones.

Pues bien, puedes organizar una pequeña fiesta en casa con tus amigos más allegados. **Convierte tu casa en un pequeño museo**. Saca todos tus juguetes y accesorios frikis. Por ejemplo, enseña ese lindo cuadro decorativo del “Señor de los anillos”.

[![](/images/uploads/cuadro-fotografico-el-senor-de-los-anillos.jpg)](https://www.amazon.es/Cuadro-Moderno-fotografico-Madera-Anillos/dp/B01B8A78HI/ref=sr_1_1?__mk_es_ES=ÅMÅŽÕÑ&crid=12HA9QOI0B7NK&keywords=cuadros+friki&qid=1652293917&sprefix=cuadrosfriki%2Caps%2C261&sr=8-1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Cuadro-Moderno-fotografico-Madera-Anillos/dp/B01B8A78HI/ref=sr_1_1?__mk_es_ES=ÅMÅŽÕÑ&crid=12HA9QOI0B7NK&keywords=cuadros+friki&qid=1652293917&sprefix=cuadrosfriki%2Caps%2C261&sr=8-1&madrescabread-21) (enlace afiliado)

## Luce camisetas

Disfrazarse para este día siempre ha sido una opción más que acertada, pero si ya no quieres seguir comprando nuevos disfraces, **puedes lucir una bella camiseta con tu personaje favorito.** Estarás cómodo y en sintonía con la ocasión.

[![](/images/uploads/anime-gamer-t-shirt.jpg)](https://www.amazon.es/Anime-Gamer-T-Shirt-Manga-Corta/dp/B07DHSB7Q1/ref=sr_1_1_sspa?__mk_es_ES=%C5M%C5&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Anime-Gamer-T-Shirt-Manga-Corta/dp/B07DHSB7Q1/ref=sr_1_1_sspa?__mk_es_ES=%C5M%C5&madrescabread-21) (enlace afiliado)

De igual forma, puedes hacer un regalo especial en este día. Por ejemplo, ¿a tu amiga le encanta la película “**Regreso al Futuro”**? Puedes obsequiarle una camiseta para que muestre su orgullo por tan clásica película.

[![](/images/uploads/camiseta-para-mujer-estampada-con-coche-delorean.jpg)](https://www.amazon.es/MAKAYA-Talla-Grande-Manga-Corta/dp/B01NBF0TAY/ref=sr_1_16?crid=GX7MEC4V3P9&keywords=camisetas+frikis+mujer&qid=1652294102&sprefix=camisetas+friki%2Caps%2C269&sr=8-16&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/MAKAYA-Talla-Grande-Manga-Corta/dp/B01NBF0TAY/ref=sr_1_16?crid=GX7MEC4V3P9&keywords=camisetas+frikis+mujer&qid=1652294102&sprefix=camisetas+friki%2Caps%2C269&sr=8-16&madrescabread-21) (enlace afiliado)

## Organiza una tarde de juegos de mesa

Si la idea de hacer una pequeña reunión en casa está muy lejos de tus posibilidades, puedes planificar una **tarde de juegos con tu familia.**

Los **colonos de Catán** son uno de los juegos de mesa de estrategia. Pueden participar entre 4 a 6 jugadores.

[![](/images/uploads/catan-juego-de-mesa.jpg)](https://www.amazon.es/Devir-Castellano-Ampliación-Jugadores-BGCATAN56/dp/B08QPKK8PJ/ref=sr_1_2?__mk_es_ES=ÅMÅŽÕÑ&crid=2DW2RQLC112QA&keywords=Los+colonos+de+Catán&qid=1652294265&sprefix=los+colonos+de+catán%2Caps%2C424&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Devir-Castellano-Ampliación-Jugadores-BGCATAN56/dp/B08QPKK8PJ/ref=sr_1_2?__mk_es_ES=ÅMÅŽÕÑ&crid=2DW2RQLC112QA&keywords=Los+colonos+de+Catán&qid=1652294265&sprefix=los+colonos+de+catán%2Caps%2C424&sr=8-2&madrescabread-21) (enlace afiliado)

Si te gustan los juegos en los que debes eliminar a tus enemigos sí o sí, entonces **Bang** el pistolero del Oeste es el mejor.

[![](/images/uploads/bang-bang-la-fiebre-del-oro.jpg)](https://www.amazon.es/ABACUSSPIELE-¡Bang-Fiebre-del-Erweiterung/dp/B0112KEAC0/ref=psdc_1641955031_t1_B003VODGPI&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/ABACUSSPIELE-¡Bang-Fiebre-del-Erweiterung/dp/B0112KEAC0/ref=psdc_1641955031_t1_B003VODGPI&madrescabread-21) (enlace afiliado)

### Clásicos de cine

Otra buena idea para celebrar el orgullo friki es planificar un **día de clásicos de cine**. Por ejemplo, la saga de “Star Wars” es una de las mejores piezas audiovisuales.

[![](/images/uploads/star-wars-trilogia-ep-i-iii-dvd-.jpg)](https://www.amazon.es/Star-Wars-Trilogía-Episodios-I-III/dp/B00FJ2FQEE/ref=sr_1_9?keywords=star+wars&pd_rd_r=1129d23f-7a19-4927-8a47-cf5d41fb4dfa&pd_rd_w=o1qAb&pd_rd_wg=feQZ4&pf_rd_p=c00fb911-6cac-43bf-80ad-80b141b69bba&pf_rd_r=HYEVZPBAXYAAJD9SE2GW&qid=1652295227&s=dvd&sr=1-9&wi=l2as5jhm₂&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Star-Wars-Trilogía-Episodios-I-III/dp/B00FJ2FQEE/ref=sr_1_9?keywords=star+wars&pd_rd_r=1129d23f-7a19-4927-8a47-cf5d41fb4dfa&pd_rd_w=o1qAb&pd_rd_wg=feQZ4&pf_rd_p=c00fb911-6cac-43bf-80ad-80b141b69bba&pf_rd_r=HYEVZPBAXYAAJD9SE2GW&qid=1652295227&s=dvd&sr=1-9&wi=l2as5jhm₂&madrescabread-21) (enlace afiliado)

Claro, no podemos dejar de mencionar a otros grandes clásicos como “**Star Trek”**. Con respecto a la famosa historia de la **tripulación Enterprise**, podemos señalar que tienes a disposición la historia original y una nueva versión, igual de exitosa.

[![](/images/uploads/star-trek-enterprise-season-4.jpg)](https://www.amazon.es/Star-Trek-Enterprise-Alemania-Blu-ray/dp/B00J89UK2M/ref=sr_1_2?__mk_es_ES=ÅMÅŽÕÑ&crid=2GBBD81EA9EYA&keywords=película+stark+trek&qid=1652294854&sprefix=película+stark+tre%2Caps%2C370&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Star-Trek-Enterprise-Alemania-Blu-ray/dp/B00J89UK2M/ref=sr_1_2?__mk_es_ES=ÅMÅŽÕÑ&crid=2GBBD81EA9EYA&keywords=película+stark+trek&qid=1652294854&sprefix=película+stark+tre%2Caps%2C370&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/star-trek.-mas-alla-dvd-.jpg)](https://www.amazon.es/Star-Trek-Más-Allá-DVD/dp/B01KZJYIK8/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&crid=2GBBD81EA9EYA&keywords=película+stark+trek&qid=1652294985&sprefix=película+stark+tre%2Caps%2C370&sr=8-4&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Star-Trek-Más-Allá-DVD/dp/B01KZJYIK8/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&crid=2GBBD81EA9EYA&keywords=película+stark+trek&qid=1652294985&sprefix=película+stark+tre%2Caps%2C370&sr=8-4&madrescabread-21) (enlace afiliado)

Sea que decidas ver estos clásicos o disfrutar de Regreso al futuro, Harry Potter, el Señor de los anillos, entre otros, puedes disfrutar de un día extraordinariamente friki con amigos o familiares.

### Regalos frikis

Por último, si deseas celebrar a lo grande el orgullo friki no puedes dejar de **regalar juguetes, accesorios, bisutería o cualquier objeto friki.**

No es necesario que gastes mucho dinero. Un buen regalo puede ser una taza de desayuno que respalde que tu amigo es un Friki.

[![](/images/uploads/taza-miss-miserable-mensaje-eres-un-friki.jpg)](https://www.amazon.es/Taza-Miss-Miserable-mensaje-friki/dp/B08987QH96/ref=sr_1_9?__mk_es_ES=ÅMÅŽÕÑ&crid=1FOHR2V23RX0M&keywords=friki&qid=1652293265&sprefix=fri%2Caps%2C921&sr=8-9&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Taza-Miss-Miserable-mensaje-friki/dp/B08987QH96/ref=sr_1_9?__mk_es_ES=ÅMÅŽÕÑ&crid=1FOHR2V23RX0M&keywords=friki&qid=1652293265&sprefix=fri%2Caps%2C921&sr=8-9&madrescabread-21) (enlace afiliado)

Por cierto, cómo olvidar a Wilson de la película el “Náufrago”, quien ganó el galardón al mejor objeto inanimado de los premios Critics Choice Awards de 2001. ¿Tu amigo lloró por Wilson?

[![](/images/uploads/wilson-pelota-de-voleibol.jpg)](https://www.amazon.es/Wilson-Cast-Pelota-Unisex-Blanco/dp/B00005LL1K/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&crid=ZADUBJJ4J1IJ&keywords=objetos%2Bfriki&qid=1652295579&s=dvd&sprefix=objetos%2Bfriki%2Cdvd%2C287&sr=1-4&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Wilson-Cast-Pelota-Unisex-Blanco/dp/B00005LL1K/ref=sr_1_4?__mk_es_ES=ÅMÅŽÕÑ&crid=ZADUBJJ4J1IJ&keywords=objetos%2Bfriki&qid=1652295579&s=dvd&sprefix=objetos%2Bfriki%2Cdvd%2C287&sr=1-4&th=1&madrescabread-21) (enlace afiliado)

En definitiva, tienes muchas formas de celebrar tu orgullo friki.

Más información sobre el [Dia del orgullo friki aqui](https://www.diainternacionalde.com/ficha/dia-orgullo-friki).