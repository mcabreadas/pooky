---
layout: post
slug: madres-cabreadas-en-la-revista-thaderconsumo
title: Fui contra corriente y elegí cuidar de mi familia
date: 2023-01-19T11:17:46.765Z
image: /images/uploads/madres-cabreadas-en-thaderconsumo.jpg
author: faby
tags:
  - revistadeprensa
---
Un placer haber compartido una interesante entrevista en la afamada **Revista  THADERCONSUMO**. Me encantó recordar algunos detalles de mis inicios en el blog, aunque todo empezó por Twitter. Bueno, en realidad nunca pensé que se desarrollaría  toda una comunidad de madres.

![](/images/uploads/maria-con-hijos-reducida.jpg)

Con más de 10 años de aquellos tiempos, hoy somos madres menos cabreadas, **madres que hemos podido lograr una conciliación con la vida laboral y familiar**. ¡Podemos con todo!

Te invito a leer o descargar el artículo completo en este enlace  👉🏻 **[Revista THADERCONSUMO](https://thader.org/nueva/wp-content/uploads/2022/12/REVISTA-53.pdf)**