---
layout: post
slug: como-elegir-silla-de-auto-para-mi-hijo
title: Qué grupo de silla de coche necesita mi peque
date: 2022-01-31T09:17:48.309Z
image: /images/uploads/istockphoto-455257499-612x612.jpg
author: faby
tags:
  - puericultura
---
Si sabes **elegir una buena silla para auto** los viajes serán placenteros y seguros. En el mercado hay una gran variedad de tipos y tamaños. Por lo tanto, puede parecer confuso saber cuál es el mejor para tu hijo.

En esta entrada te **daré algunas recomendaciones para que puedas elegir la mejor silla de coche.**

## ¿Cómo saber qué silla de coche comprar?

La silla de coche es un **mecanismo de seguridad.** Sin embargo, para que realmente cumpla su función es imperativo que la silla infantil se ajuste a la **talla y peso de tu hijo**.

![padre pponiendo arnes silla auto a bebe](/images/uploads/istockphoto-1209117975-612x612.jpg)

En el mercado hay sillas para críos de 9 meses y otras para niños de 9 años. Es evidente que estos dos niños no pueden usar el mismo tipo de silla. 

Por otra parte, debes considerar el diseño de la silla. Si es un niño pequeño lo ideal son las sillas a contramarcha. También **hay modelos en sentido inverso, con arnés, con cinturón de seguridad,** etc. Debes elegir la que se ajuste a las necesidades de tu pequeño.

## ¿Qué es el Grupo 1 2 3 en sillas de coche?

Las sillas de coche están clasificadas en grupo. Se trata de las **sillas de coche con las que crecerá tu hijo.** El grupo más pequeño de esta clasificación es de 9 meses y el más grande hasta los 12 años. A continuación, te lo detallaré:

* **Grupo I.** Es para críos de 9 a 18 kg. Generalmente entran en este grupo los bebés de 9 meses a 4 años aproximadamente.
* **Grupo II.** Es para niños de 15 a 25 kg. En esta clasificación entran desde los 3 años hasta los 7.
* **Grupo III.** Es para niños de 22 a 36 kg. Generalmente entran en este grupo los de 6 a 12 años.

## ¿Cuándo los niños no tienen que llevar silla en el coche?

La normativa española establece que el niño debe **viajar con silla de coche hasta los 1.35 metros (12 años aproximadamente).** Por consiguiente, solo cuando el niño alcanza una altura superior a los 1.35 metros y cuenta con el peso establecido es que podrá usar el cinturón de seguridad que utilizan los adultos.

## ¿Cuál es la mejor silla de niños para el coche?

La mejor silla infantil para coche **es la que se ajusta a las necesidades de tu hijo.** A continuación te presento los mejores modelos.

### LIONELO Bastiaan One

Esta es una de las pocas sillas que puedes **usar desde el nacimiento hasta los 36 kg,** gracias a que cuenta con un reductor. 

Además, el **asiento es giratorio,** por lo que puedes llevar al peque a contramarcha hasta los 4 años o el tiempo que estimes. El **sistema ISOFIX y cinturón Top Tether l**a convierte en una de las sillas de coche más seguras.

[![](/images/uploads/lionelo-bastiaan.jpg)](https://www.amazon.es/Lionelo-Bastiaan-nacimiento-giratoria-seguridad/dp/B08TMRH2L5/ref=sr_1_3_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=silla%2Bde%2Bcoche&qid=1639576882&sr=8-3-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyQ1JXWFQ5STVXOTcxJmVuY3J5cHRlZElkPUEwNjkwMTcxMksyWDVCUk0zRUtBNCZlbmNyeXB0ZWRBZElkPUEwMzA1NDQ2MkhVRkZITjE4NVFaOSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Lionelo-Bastiaan-nacimiento-giratoria-seguridad/dp/B08TMRH2L5/ref=sr_1_3_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=silla%2Bde%2Bcoche&qid=1639576882&sr=8-3-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyQ1JXWFQ5STVXOTcxJmVuY3J5cHRlZElkPUEwNjkwMTcxMksyWDVCUk0zRUtBNCZlbmNyeXB0ZWRBZElkPUEwMzA1NDQ2MkhVRkZITjE4NVFaOSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

### BRITAX RÖMER

Esta silla es para **niños del grupo 2 y 3.** Tiene un diseño fácil de usar incluso por el mismo crío. Es transpirable. Su reposacabezas permite que el niño vaya cómodo y seguro.

Tiene **anclajes [isofix](https://www.fundacionmapfre.org/educacion-divulgacion/seguridad-vial/temas-clave/sistemas-retencion-infantil/sillas-mas-seguras/isofix/), además de SICT** para proteger de impactos laterales. Es una excelente opción con relación calidad precio.

[![](/images/uploads/britax-romer2.jpg)](https://www.amazon.es/Britax-R%C3%B6mer-Kidfix%C2%B2-Storm-Grey/dp/B07K85MYCP/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=silla%2Bde%2Bcoche&qid=1639576882&sr=8-4-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyQ1JXWFQ5STVXOTcxJmVuY3J5cHRlZElkPUEwNjkwMTcxMksyWDVCUk0zRUtBNCZlbmNyeXB0ZWRBZElkPUEwMTUwMzU4M0FRQVJVWkE2WVVLNSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Britax-R%C3%B6mer-Kidfix%C2%B2-Storm-Grey/dp/B07K85MYCP/ref=sr_1_4_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=silla%2Bde%2Bcoche&qid=1639576882&sr=8-4-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyQ1JXWFQ5STVXOTcxJmVuY3J5cHRlZElkPUEwNjkwMTcxMksyWDVCUk0zRUtBNCZlbmNyeXB0ZWRBZElkPUEwMTUwMzU4M0FRQVJVWkE2WVVLNSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&madrescabread-21) (enlace afiliado)

En conclusión, **la seguridad de tu hijo debe ser tu prioridad.** Este tipo de sillas se pueden ajustar durante el crecimiento del peque. Podrán viajar cómodos y felices. Al mismo tiempo, al elegir una buena silla, cumples con las normativas de seguridad vial.

Si necesitas encajar tres sillas en tu coche, te interesará este post sobre las 👉🏻 [sillas de auto mas estrechas del mercado](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/). 👈🏻

Photo Portada by gilaxia on Istockphoto

Photo by SDI Productions on Istockphoto