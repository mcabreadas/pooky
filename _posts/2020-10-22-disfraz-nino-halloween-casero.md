---
author: belem
date: '2020-10-22T10:19:29+02:00'
image: /images/posts/disfraz-nino-halloween/p-calabaza.jpg
layout: post
tags:
- trucos
title: 5 disfraces infantiles de Halloween caseros
---

Es muy probable que este año busquemos algo que se adapte a nuestras nuevas necesidades. Para mí, por ejemplo, esto quedó muy claro desde agosto pasado; quería complacer a mi hijo que espera esta época con tanta ilusión (ama los disfraces) sin gastarme una pequeña fortuna en un disfraz. Por lo tanto, comencé a buscar disfraces caseros de Halloween para niños que fueran tan atractivos para mi hijo como fáciles y accesibles para mí.

En este artículo, les comparto algunos de mis descubrimientos, en espera que les sean de tanta ayuda a ustedes como lo han sido para nosotros. 

Si queréis completar la fiesta con una rica merienda a base de patatas con arañas y unos bizcochos de calavera y cerebro para la noche de los muertos os dejo la receta en este enlace: Receta de [patatas asadas con arañas](https://madrescabreadas.com/2017/10/29/recetas-cena-halloween/) y [bizcocho de calavera y cerebro](https://madrescabreadas.com/2017/10/29/recetas-cena-halloween/).

También doy ideas para preparar dulces de Halloween super originales y escalofriantes en este post: [Dulces de Halloween originales](https://madrescabreadas.com/2015/10/27/dulces-halloween/).

Les dejo aquí alguno de los [materiales necesarios para elaborar los disfraces](https://amzn.to/37v239z) (enlace afiliado), aunque casi todo lo encontrareis por casa.

## 5 disfraces de Halloween caseros y originales para tus hijos

## Disfraz de Halloween casero "Come frutas y verduras"

Si a nuestra hija le ponemos un vestido amarillo y fabricamos con retazos de tela, cartón o foami una especia de penacho verde que se sujete a una diadema; voilà, tenemos una piña. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=258534834847793504" height="614" frameborder="0" scrolling="no" style="width:100%;"></iframe>

Si a nuestro hijo lo vestimos de color morado y pegamos con cinta doble cara pequeños [globos morados](https://www.amazon.es/dp/B085L24KLJ/?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado) a lo largo de su torso tendremos un adorable racimo de uvas. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=430164201888304431" height="446" frameborder="0" scrolling="no" style="width:100%;"></iframe>


Vestido rojo = sandía, Playera y pantalón naranja con penacho verde = zanahoria; en fin, hay muchas posibilidades para explorar, solo hay que dejar la creatividad fluir.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=827536500256898948" height="359" frameborder="0" scrolling="no" style="width:100%;"></iframe>

## Disfraz de Halloween casero "Trolls"

Quiero creer que no estoy sola cuando afirmo que mi hijo pasó por una etapa de obsesión con la primera película de Trolls. No me quejo, la encuentro adorable, pero verla tres veces al día (cuando mínimo) comenzó a ser cansado después de las primeras dos semanas.  

Si compramos suficiente [tela de tul](https://www.amazon.es/dp/B07B3PQ3W2/?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado), podemos fabricar un tutú para nuestras hijas. La técnica es muy simple; hay que cortar tiras largas de tul que al doblarlas por la mitad queden de lo largo que deseamos que la falda o el tutú sea. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=495396027767047295" height="537" frameborder="0" scrolling="no" style="width:100%;"></iframe>

Luego, en un resorte o listón que sea de la medida de la cintura de nuestra pequeña, vamos haciendo nudos con las tiras de tul dobladas a la mitad. Esto es muy fácil, simplemente se trata de introducir la mitad de la tira doblada dentro del ojal que se forma justo en donde está el doblez. El resorte o listón queda en medio del nudo. 

Necesitaremos utilizar suficiente tul para que quede un tutú lo suficientemente esponjado. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=147492956530616201" height="445" frameborder="0" scrolling="no" style="width:100%;"></iframe>

No olvides combinar colores. Para el cabello basta usar suficiente gel o laca, peinarlo hacia arriba y sujetarlo con una pequeña liga. En el supermercado venden tintes en aerosol que se retiran con agua. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=700239442056771688" height="560" frameborder="0" scrolling="no" style="width:100%;"></iframe>

## Disfraz de Halloween casero "Máquina de chicles"

<iframe src="https://assets.pinterest.com/ext/embed.html?id=311170655500362016" height="618" frameborder="0" scrolling="no" style="width:100%;"></iframe>

A una camiseta playera blanca, la más simple que tengan, puede incluso no ser blanca y ni siquiera ser de su talla, vamos a pegarle en un trozo de [tela de fieltro](https://www.amazon.es/dp/B07QGFLGCV/?ref=exp_madrescabreadas_dp_vv_d) de forma circular (o coser para las que tengan tiempo y ganas) (enlace afiliado), pompones del mismo tamaño, pero diferentes colores. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=57420963989253855" height="445" frameborder="0" scrolling="no" style="width:100%;"></iframe>

[Hay tutoriales para hacer los pompones con lana](https://www.youtube.com/embed/Scj7IusVCfI) o estambre. Pero también podemos [comprar pompones de lana](https://www.amazon.es/dp/B07QWCT2XN/?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado)  o pedirlos en línea, no suelen ser caros. 

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="https://rcm-eu.amazon-adsystem.com/e/cm?ref=tf_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS1=1&npa=1&asins=B077GMV7P2&linkId=7676ddb28779db051bf87a046a64bdad&bc1=ffffff&lt1=_blank&fc1=333333&lc1=bfa900&bg1=ffffff&f=ifr">
    </iframe>
    
    
Con una falda o pantalón rojo, ¡tendremos una máquina de chicles andante al instante! 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=91479436160780642" height="618" frameborder="0" scrolling="no" style="width:100%;"></iframe>

## Disfraz de Halloween casero "Reportero del clima"

Viste a tu pequeño niño de reportero: camisa con cuello, pantalón de vestir y una corbata. Ahora viene el twist. Con ayuda de unas pequeñas puntadas, cose la corbata a la camisa en la posición en la que quedaría si se estuviera volando por el viento. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=21251429467939433" height="648" frameborder="0" scrolling="no" style="width:100%;"></iframe>

Con gel o laca, peina su cabello hacia arriba, igual; como se ve cuando le da el viento en la cara. Toma un par de pañuelos o envolturas de caramelos (que parezcan basura) y cóselas con un par de puntadas, puede ser en un brazo, una pierna y el torso. Ahora los toques finales. En caso de tener en casa un paraguas o sombrilla que ya no sirva al cien, este será el accesorio perfecto. 

<iframe src="https://assets.pinterest.com/ext/embed.html?id=4433299619630479" height="532" frameborder="0" scrolling="no" style="width:100%;"></iframe>

Si podemos hacerlo, hay que abrirla y doblarla hacia afuera, como cuando el viento nos la voltea a medio parque y todos se nos quedan viendo. Puedes también elaborar un micrófono con cartón (si tenemos uno en casa podemos añadir una pequeña “cajita” para simular esos micrófonos de las cadenas televisivas). Te aseguro que no verás un disfraz igual de original en toda la noche. 

## Disfraz de Halloween casero "Gnomo de jardín"

Adorable y simple. Especialmente con los más pequeños. Sólo hay que conseguir una playera verde o azul, un pantalón rojo, verde o azul (pero de color distinto a la playera), y fabricar con tela un grueso cinturón negro con hebilla dorada.

<iframe src="https://assets.pinterest.com/ext/embed.html?id=289919294767466974" height="416" frameborder="0" scrolling="no" style="width:100%;"></iframe>

 Finalmente, con tela o cartón, hacemos un gran cono rojo para su cabeza. Como extra, podemos agregar una barba blanca y zapatos picudos (o que den la apariencia de ser picudos).


Listo, cinco originales opciones de disfraces caseros para Halloween. Puede que nuestros hijos quieran disfrazarse de algún personaje de moda, esto es especialmente real con los niños mayores. 

Mi hijo, por ejemplo, quería ser un **personaje de Fortnite**, no me pregunten cuál, no lo sé (para quienes se pierdan, pueden consultar este post sobre el [Fornite de madre a madre](https://madrescabreadas.com/2019/10/22/el-fortnite-de-madre-a-madre/)). Pero lo logramos con la ropa que teníamos en casa y la ayuda de algunos accesorios de casa de la abuela. Mi hermana le ayudó a hacer un “lanzacohetes” con cartón. Lo pintaron juntos y les quedó bastante bien. 

En la fabricación de su disfraz casero para Halloween no olviden explorar todas sus opciones; pregunten a sus familiares y vecinos si están buscando un accesorio muy específico y no olviden que, si lo hacemos nosotros con la ayuda de los pequeños, ellos van a disfrutar el resultado mucho más que si simplemente corriéramos a comprarlo. 

Pasen un muy divertido Halloween. No olviden divertirse y cuidarse mucho. 

Si les ha servido, por favor compartan este post, eso nos ayudaría a poder seguir escribiendo este blog.

Belem Martínez


*Photo portada by Julia Raasch on Unsplash*