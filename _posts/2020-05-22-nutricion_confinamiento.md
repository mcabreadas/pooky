---
date: '2020-05-22T12:19:29+02:00'
image: /images/posts/alimentacion-confinamiento/p-rebeca-maria.jpg
layout: post
tags:
- salud
- crianza
title: Confinamiento y alimentación infantil
---

Qué tarde más divertida pasamos con Rebeca Pastor, de [My Personal Food](https://mypersonalfood.es) hablando de todo lo que nos hemos comido en este confinamiento (podéis echar un vistazo a [este post](https://madrescabreadas.com/2020/04/12/comida-cuarentena-covid/), para que veáis que no miento), y cómo podemos salir de esta espiral de azúcar en la que nos hemos metido con tanto bizcocho, galletas, tartas, torrijas y hasta paparajotes.

Re educar nuestro paladar y el de nuestros hijos para disfrutar de los sabores básicos es la clave, pero es mejor no hacerlo de golpe y no ponernos metas muy altas o meternos en recetas muy elaboradas.

Rebeca Pastor es Dietista-Nutricionista Infantil y Familiar y trabaja en el Hospital De Molina de Segura, en Murcia. Diplomada en Nutricion Humana y Dietética, Licenciada y ciencia y tecnología de los alimentos, el objetivo de su vida ser feliz, y si puede ayudar a los demás, mejor. Su color favorito es el naranja, y es puro optimismo; le encanta saludar y sonreír. Se autodenomina  loca de los lunares, y siempre va a la playa... con los labios pintados, eso sí permanentes. Su verdura favorita son las judías verdes, y el espectáculo forma parte de ella misma porque cree que hay otra forma de ver las cosas.

En este vídeo que capta una entrevista en directo en Instagram con intervención del público y preguntas muy interesantes encontraréis recetas saludables y atractivas para comenzar el camino hacia la alimenta saludable, que es el mejor regalo que podemos hacerle a nuestros hijos.

No os lo perdáis!!

<iframe width="560" height="315" src="https://www.youtube.com/embed/460wPC8rNZ8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


En el video podréis encontrar respuestas a preguntas como:
 

-cuánto hemos engordado estos días de confinamiento?

-y los niños? Te llegan mas consultas por obesidad?

-tendríamos que cambiar nuestros hábitos alimenticios durante el confinamiento con respecto a los anteriores, pero que nos movemos mucho menos?  Y los niños?

-Cómo podemos afrontar la operación bikini confiando llegados a este punto. Nos da tiempo?

-hemos sido más permisivos con los niños con las pantallas y también con la comida por compensar un poco o premiar su esfuerzo en el confinamiento. Lo hemos hecho bien?

-cómo podemos enmendarlo? Sería buena idea un plan de reconducción de la dieta o desescalada del azúcar?

-cómo podemos ofrecer de manera atractiva la verdura?

-ideas para el pescado?

-hemos tenido carencia de vitamina D, se puede recuperar con la dieta?

Además, en mi colaboración semanal en 7 Televisión Región de Murcia, en el programa Quédate Conmigo, de Encarna Talavera, también hablé sobre este tema.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7RIyOvRehDI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>