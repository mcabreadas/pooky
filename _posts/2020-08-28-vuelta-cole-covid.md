---
date: '2020-08-28T10:19:29+02:00'
image: /images/posts/vuelta-cole-covid/p-pizarra.jpg
layout: post
tags:
- salud
- crianza
title: Consejos para afrontar la vuelta al colegio en la nueva normalidad
---

## ¿Los niños necesitan volver al cole?

El secretario general de la ONU así lo cree, y todo apunta a que los niños y niñas van a volver a las aulas en breve, por lo que voy a daros unos consejos para la vuelta al cole, ya que se ha advertido que la pandemia de COVID-19 ha provocado la mayor interrupción de la educación de la historia y que en caso de no reanudarse en breve podría suponer una catástrofe generacional, esto ha provocado que los padres, especialmente las mujeres, se hayan visto obligados a asumir una pesada carga en el cuidado del hogar. Y que a pesar de impartir las clases online , y de los esfuerzos de profesores y padres, muchos estudiantes se han quedado fuera de la educación.

> "Será esencial equilibrar los riesgos para la salud con los riesgos para la educación y la protección de los niños, y tener en cuenta el impacto en la participación de la mujer en la fuerza laboral"


> "Necesitamos invertir en la alfabetización digital y la infraestructura, una evolución hacia el aprendizaje del aprendizaje, un rejuvenecimiento del aprendizaje a lo largo de toda la vida y el fortalecimiento de los vínculos entre la educación formal y no formal".



## Cómo afrontar la vuelta al colegio de nuestros hijos

La situación actual, la incertidumbre y el desconcierto nos genera frustración y ansiedad a nosotros los padres, a los profesores y también a nuestros hijos.

- Procura no proyectar tus miedos porque el mundo de los niños es mucho más sencillo y  suelen afrontar con más naturalidad las situaciones difíciles que nosotros. Ellos en general no se angustiarán con cifras, repuntes o consecuencias que puedan suceder en el futuro si no se lo transmitimos nosotros.

- Evitar que vean las noticias sin filtro: es mejor contarles nosotros las novedades de forma sencilla  sin necesidad de muchos detalles y sólo en la medida que su edad pueda asimilar. Podemos preguntar si tienen alguna duda o temor y resolverla con calma y con palabras asequibles a su edad.

![](/images/posts/vuelta-cole-covid/urnas.jpg)

- Evitar las conversaciones con otros adultos delante de los niños porque ellos son esponjas aunque parece que no están escuchando (y la verdad es que cada vez que nos encontramos con alguien o hablamos por teléfono la conversación es monotema).

## Prepara a tu hijo para la nueva normalidad dentro de las aulas

Explica las normas del cole con pocas palabras y usa afirmaciones simples. Deja que sea el propio peque quien comente y pregunte las dudas.

En este escenario es reconfortante encontrar maestras y maestros implicados, como Jennifer Birch Pierson, una profesora de preescolar en Texas, que se prepara para el regreso a clases en plena pandemia y ha adaptado su clase para cumplir las medidas necesarias de distanciamiento social dejando volar su creatividad y decorando cada pupitre como si fuera un camión para que sus alumnos se sientan más cómodos.

![](/images/posts/vuelta-cole-covid/camiones.jpg)

## Preparémonos para las clases on line

- Establece una rutina y unos horarios para combinar las clases y el estudio con el ocio y el deporte. Pero sin rigidez mental y dejando margen a la flexibilidad por si sirvieran imprevistos.

- Haz un periodo de adaptación: empieza por sesiones cortitas y ve ampliando su duración poco a poco.

- Procura que no se rompa el vínculo con el colegio y habla a menudo con el tutor o tutora y organiza videoconferencias entre tu hijo y sus compañeros 

- reserva un tiempo para mimarte y hacer algo que te guste. Sé comprensivo con tus errores y no te exijas demasiado. Estamos sobreviviendo!

## ¿Puedo optar por no llevar a mis hijos al colegio?

Muchas son las familias que tienen miedo de llevar a sus hijos al colegio, máxime con el clima de incertidumbre y la falta de información que hay a pocos días del regreso al cole. Sobre todo se lo plantean las familias con hijos de riesgo o que conviven con personas de riesgo. Pero no debemos olvidar las implicaciones legales que esto conllevaría.

Normalmente, ante una falta de asistencia considerable sin justificación, suele ser por porcentaje de horas lectivas sin que haya mucha unicidad de criterios, se puede abrir un procedimiento administrativo  por parte del centro educativo, con comunicación a la comisión municipal encargada de estos asuntos en la que participan los servicios sociales. 

En este caso estudiarían el problema y determinarían las líneas de actuación, que podrían conllevar incluso multas. Pero esto suele ocurrir cuando los padres hacen dejación de funciones y no se responsabilizan de que su hijo vaya al colegio. En una situación en la que se toma la decisión con responsabilidad y con una justificación y además se procura que el menor tenga una enseñanza en casa siguiendo las pautas del colegio, no estaríamos hablando de un caso de absentismo escolar al uso.

Entiendo que si la familia puede justificar su decisión por la falta de las medidas de seguridad para prevenir el contagio tendría armas para defenderse frente a un eventual expediente sancionador, máxime en caso de situación de riesgo del menor certificada por un médico o de riesgo para las personas con vivientes siendo estas de riesgo.

Una opción sería pedir por escrito un certificado del centro de que cumple todas las medidas anti Covid (distancia social, disminución de ratio...), y caso de no obtenerlo, como será en la mayoría de los casos , no porque no se quiera, sino porque no hay medios físicos ni humanos en la mayoría de los centros, tendríamos una justificación clara para no enviar a nuestro hijo al colegio para no poner en riesgo su Salud y la de los mayores que conviven con el, por ejemplo, en caso de que conviva con los abuelos.

El derecho-obligación a la educación choca en este caso con el derecho-obligación de los padres de protección y a preservar la salud de sus hijos, siendo en última instancia un Juez quien debería determinar cuál prima en cada caso concreto.




Si te han servido y piensas que pueden ser de utilidad para otras familias, comparte! Así también contribuirás a que pueda seguir escribiendo este blog.


*Photo portada by Deleece Cook on Unsplash*