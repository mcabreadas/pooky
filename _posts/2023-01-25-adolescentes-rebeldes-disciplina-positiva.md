---
layout: post
slug: adolescentes-rebeldes-disciplina-positiva
title: Mentoría de disciplina positiva para madres de adolescentes
date: 2023-01-25T11:42:48.381Z
image: /images/uploads/mentori-a-desaprendo.jpg
author: .
tags:
  - adolescencia
---
Sé que gritar a mis hijos adolescentes no es la solución, pero grito

Sé que castigar a mis hijos adolescentes no es la solución, pero castigo

Sé que menazar a mis hijos adolescentes no es la solución, pero amenazo.

Y no me gusta.

Pero lo hago porque no se me ocurre qué más hacer, necesito herramientas que me funcionen y no me hagan sentir desarmada cuando me propongo no gritar, no castigar y no amenazar.

Dime que no soy la unica

\
Últimamente vivo una auténtica batalla campal cada dos por tres en casa.

Y digo cada dos por tres porque es muy a menudo y porque tengo tres hijos, y si no es uno, son dos, o incluso los tres quienes se pelean protagonizando episodios que, vistos desde fuera, no me quiero ni imaginar lo que parecen.

Qué te voy a contar...

Todo empieza con una chispa que pronto prende en cualquier mecha y se expande como la pólvora convirtiendo lo que parecía una comida tranquila en familia en un polvorín donde se suceden los gritos, ruidos, insultos...

Reconozco que más veces de las que me gustaría me pongo a su nivel y acabo como ellos hasta que me doy cuenta, y no me reconozco, no me gusta en lo que me convierto. Además, yo soy la adulta, necesitan que controle la situación, mantenga la calma y contribuya a la solución del conflicto.

## Mentoria DesAprendo

Ya puedes [unirte a la mentoría DesAprendo](https://madrescabreadas--desaprendo.thrivecart.com/mentoria-desaprendo-precio-especial/).

Con el reto descubrimos cómo mejora la relación y comunicación con tu adolescente, las claves sobre los temas que más nos preocupan y que son a la vez los que más conflictos y peleas generan en casa.

¿Y ahora qué? ¿es esto suficiente? 

Si quieres seguir aprendiendo y conocer las profundidades de cómo seguir avanzando para conseguir ser una madre más consciente, ser capaz de mantener la calma en cualquier situación, reconectar con tu adolescente y descubrir la única herramienta que consigue la colaboración en casa, la Mentoría desAprendo es tu sitio.

Es el momento de decidir si quieres apostar por una relación con tu adolescente basada en el respeto y la colaboración mutua o seguir como hasta ahora. 

\
Tu decides si das el paso.

## El reto Menos Peleas y Mas paz en Casa con tus adolescentes

Ya realic el reto Mas Paz en Casa porque pensé que estaba hecho para mí y para madres que, como yo, nos vemos desbordadas muchas veces porque nos faltan recursos para manejar este tipo de situaciones, más allá de los gritos, las imposiciones y los castigos.

Este reto me ayud a hacer un reseteo parental, basándonos en estrategias y herramientas de Disciplina Positiva y de resolución de conflictos. 

> ¿Te imaginas mejorar la convivencia en casa y a aprender a resolver los problemas desde el respeto y la colaboración mutua?

El reto consistio en una semana completa de formación gratuita intensiva en directo, con interacción directa con las familias en un formato que combina masterclass, propuesta de actividades para las familias (les ponemos deberes cada día) y preguntas-respuestas.

DÍA Nº1 – DESCONTROL CON LAS PANTALLAS

Aprende estrategias y herramientas que te ayudarán a que tus hijos aprovechen los beneficios de las pantallas, esquivando los riesgos y evitando las peleas.

DÍA Nº2 – AGRESIVIDAD VERBAL Y FÍSICA

Aprende cómo acompañar estos momentos y cómo actuar durante y después, para ir reduciendo al máximo estos episodios.

\
DÍA Nº3 – RIVALIDAD Y PELEAS ENTRE HERMANOS E IGUALES

Aprende cómo acompañar estos momentos y cómo actuar después, para ir reduciendo al máximo estos episodios.

\
DÍA Nº4 – CUANDO TU HIJ@ SOLO DICE “NO”

Aprenderás a abordar esta situación desde otro ángulo, para dejar de actuar como policía y empezar a ejercer de facilitador@

\
DÍA Nº5 – DESALINEAMIENTO EN LA PAREJA

Te enseñaremos cómo avanzar para abordar los problemas relacionados con crianza/educación, con o sin el apoyo de tu pareja (y verás cómo termina subiéndose al carro)

\
DÍA Nº6 – LAS 4 C’s PARA VOLVER A CONECTAR CON TU HIJ@

Cómo ponerte en marcha y generar cambios permanentes en tu familia:

* Las 4 C’s que tienes que dominar
* La hoja de ruta para volver a conectar con tu hij@
* Los 2 ingredientes que acelerarán el cambio en tu casa

\
DÍA Nº7 – DE LOS GRITOS A LA PAZ EN CASA

Conoci a otras familias que estaban en tu situación y ahora disfrutan de una convivencia familiar positiva y podrás plantear tus dudas, a Daniel y su equipo, y a estas familias.