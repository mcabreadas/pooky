---
author: ana
date: 2021-05-20 07:41:44.072000
image: /images/uploads/whatsapp-image-2021-05-18-at-4.22.45-pm.jpeg
layout: post
slug: mi-bebe-solo-quiere-brazos
tags:
- puericultura
- crianza
title: ¿Qué hago si mi bebé solo quiere estar en brazos?
---

Primero, disfrútalo, crecerá muy rápido y ya extrañarás poder tener su olor y calorcito a flor de piel como en los primeros meses. Tanto para él como para ti, con esa cercanía están tejiendo el vínculo mágico que se desarrolla con los progenitores, especialmente con la madre; a vecs nos preguntamos Por que mi bebe solo quiere estar conmigo.

Es sabido que no hay mayor placer para un bebé que estar en brazos. Y tiene lógica, los brazos de mamá o papá son sinónimo de seguridad y confort. Al estar en brazos estás respondiendo a su necesidad de atención y afecto casi de manera exclusiva. Sin embargo, si es un [bebe de alta demanda](http://www.revistamipediatra.es/articulo/446/bebes-de-alta-demanda) también es cierto que no todo el tiempo podrás hacerlo porque la crianza requiere otras ocupaciones, y ni hablar si ya te has incorporado al trabajo. 

![hombre porteando bebe](/images/uploads/derek-owens-iljf9ljdjjy-unsplash.jpg "portabebé")

## ¿Qué hacer cuando un bebé solo quiere estar en brazos?

Hay bebes que solo quieren ester en brazos de pie. Ante este panorama os dejamos acá algunas recomendaciones que pueden ayudarte a sobrellevar la demanda de los brazos.

### Usa un portabebes ergonomico para liberar tus brazos

**Prueba con un portabebé**. Si el bebé ya sostiene bien la cabeza puedes probar con un suplemento para sujetar la cabeza que ofrecen muchas marcas de portabebs ergonmicos, o con un fulard que se la recoja  para llevarlo cargado tipo canguro, con esto logras la independencia en los brazos para hacer otras tareas. Y, a la par, el bebé se mantiene cerca de tu pecho, atento a tus movimientos como en una aventura.

Ya hablamos de la utilidad del portabebes cuando nuestro [bebe tiene reflujo en este post](https://madrescabreadas.com/2015/11/20/reflujo-bebes/).



{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=B077D8SN55&linkId=dd05accbf9678d5234ce0ebeb77ed65b&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}



### Sientalo cerca de ti

Otra alternativa que puedes probar es sentado en su coche frente a ti, para que puedas hablarle, cantarle o hacerle caritas graciosas que lo hagan sentir involucrado mientras realizas tus actividades.

### Hazle un corralito con juguetes

**Convierte un [corral en una pequeña salita de juego](https://amzn.to/3v4Tify) (enlace afiliado)**. A algunos niños les funciona la táctica del corral lleno de peluches con luces coloridas o sonidos. Puede que al principio sólo funcione unos pocos minutos, a medida que va creando el hábito puedes acompañarlo un ratico y quizás cada día puedas sumar un tiempito más sin que se desespere.

![bebe jueguetes suelo](/images/uploads/whatsapp-image-2021-05-18-at-4.27.33-pm.jpeg "corral de bebé para descansar los brazos")

## Como hacer que mi bebe deje los brazos

Si lo has probado todo y aún así el bebé quiere estar solamente en tus brazos, pues ten paciencia y dale todo el amor que está solicitando. Cada persona tiene un ritmo de desarrollo y a medida que madure podrá ir aceptando pasar un rato separado de mamá o papá sin que esto signifique que lo dejaremos llorar hasta dormirse agotado. 

Intenta, en lo posible, que ese tránsito hacia la autonomía esté lleno de comprensión y tiempo de tu parte. Que no quede asociado a un desprendimiento tajante que pueda interferir luego en sus emociones con inseguridades que pueden hasta perturbar sus patrones de sueño. 

Como mamá, ante las primeras pruebas de independencia que decidas realizar, sabrás encontrar el balance para no dejarte manipular al primer quejido y aprenderás a distinguir poco a poco cuando se trate de un llanto de mentirita para que acudas corriendo. Así que, háblale a tu bebé, hazle saber con palabras y acciones que aunque no esté en tus brazos estarás ahí cuando te necesite. Y en el ínterin, estarás aprendiendo a caminar como malabarista en el delgado hilo de la manipulación y el entendimiento.

Si te ha gustado, comparte y suscribete al blog para no perderte nada

*Photo de portada by Luiza Braun on Unsplash*

Photo by Derek Owens on Unsplash

*Photo by motrek bali on Unsplash*