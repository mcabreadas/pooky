---
layout: post
slug: tartas-caseras-cumpleanos
title: Las 7 tartas de cumpleaños favoritas de mis hijos
date: 2020-11-08T11:19:29+02:00
image: /images/posts/tartas-cumpleanos/p-tarta-colores.jpg
author: maria
tags:
  - 6-a-12
  - 3-a-6
  - planninos
  - recetas
---
Una de las cosas que más ilusión les ha hecho siempre a mis hijos es elegir su tarta de cumpleaños. Es tradición en casa que ese gran día, **el cumpleañero elija su comida favorita de cumpleaños y su tarta favorita** que, curiosamente, suele ser casera, a pesar de tener un gran confitería muy cerca. Pero el hecho de elaborar la tarta en casa y colaborar tanto en la compra de los ingredientes como en la ejecución les hace tanta o más ilusión que comérsela.

## 1. Tarta cofre pirata

Solo necesitas un bizcocho rectangular de los que sueles hacer, monedas de chocolate, crema de cacao para cubrirlo y conguitos blancos.

Es muy sencillo de montar:

1. Partes el bizcocho por la mitad transversalmente
2. Pones monedas por los bordes, **como si formaran parte de un tesoro** que rebosa del cofre
3. Lo cubres con una fina capa de crema de cacao.
4. Después, le pones los conguitos blancos como en la foto de arriba, que quedarán pegados, y listo.

El secreto está en hacer un delicioso bizcocho de los que mejor te salgan, y si es de cacao, mejor.

Más [detalles para la fiesta pirata aquí](https://madrescabreadas.com/2017/02/05/fiesta-infantil-pirata/)

![tarta cofre pirata](/tumblr_files/tumblr_oka4icqRGW1qgfaqto8_r1_1280.jpg)

## 2. Tarta de Star Wars: Estrella de la Muerte

### Ingredientes de la tarta de Estrella dela Muerte

* Un bizcocho casero de yogur  
* Conguitos o bolitas de chocolate blancos y negro  
* Kit Kat de chocolate blanco  
* Crema de cacao  
* Una cinta negra  

### Elaboración de la tarta de Star Wars

1. Haz un [bizcocho casero](http://www.bizcochocasero.net/Bizcochos/BizcochoYogur/BizcochoYogur.html) de los que sueles hacer  normalmente como más te guste.  
2. Cuando se enfríe, cúbrelo con una capa fina de crema de cacao.  
3. Pégale las bolitas de chocolate jugando con los colore blanco y negro. Puedes hacer un círculo  en el centro, o una estrella… Eso ya, depende de tu imaginación.  
4. Coloca alrededor los Kit Kat sin separar las barritas, rodéalas con una cinta o cuerda negra, y haz un lazo.  

Más [detalles para tu fiesta de Star Wars aquí](https://madrescabreadas.com/2016/05/02/cumpleanos-star-wars/)

![tarta estrella de la muerte](/tumblr_files/tumblr_inline_o6hojdbQqA1qfhdaz_540.jpg)

Una excelente herramienta de repostería que te hará la preparación de la tarta Star Wars más amena son los moldes desmontables. Así, te quedarán perfectamente redonditas.

[![](/images/uploads/molde-desmontable-para-tarta.png)](https://www.amazon.es/Lékué-Desmontable-Redondo-Pastel-Silicona/dp/B08XYCRSFT/ref=pd_sbs_5/259-8252965-5643646?pd_rd_w=ylWhX&pf_rd_p=073ad75e-0f2f-4e00-af92-a91d92e1e105&pf_rd_r=R0RYBH0MMW6NV8S0WSTK&pd_rd_r=fb478346-99bd-41a9-8312-105e832c70a2&pd_rd_wg=hy4KP&pd_rd_i=B08XYCRSFT&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Lékué-Desmontable-Redondo-Pastel-Silicona/dp/B08XYCRSFT/ref=pd_sbs_5/259-8252965-5643646?pd_rd_w=ylWhX&pf_rd_p=073ad75e-0f2f-4e00-af92-a91d92e1e105&pf_rd_r=R0RYBH0MMW6NV8S0WSTK&pd_rd_r=fb478346-99bd-41a9-8312-105e832c70a2&pd_rd_wg=hy4KP&pd_rd_i=B08XYCRSFT&psc=1&madrescabread-21) (enlace afiliado)

## 3. Tarta de sandía

Las tartas no siempre tienen que estar plagadas de azúcar y calorías. Os traigo una **manera atractiva de presentar la fruta a los niños** que aprendí en casa de unos buenos amigos. No me negaréis que tiene una pinta estupenda.

### Ingredientes de la tarta de sandía

* 1 sandia gigante sin semillas o una grande y otra pequeña.
* 250 g de moras negras y rojas
* 2 plátanos
* 1 piña (opcional)
* palillos mondadientes

### Elaboración de la tarta de sandía

1. Se corta la sandia en forma hexagonal reservando un trozo para colocar en la parte de arriba con la misma forma, pero más pequeña.
2. Se decora alrededor y por arriba al gusto **combinando los colores de las frutas** pinchando los trocitos en la sandia con los palillos con cuidado de que no se queden a la vista.

Nosotros no pusimos piña, pero da un toque colorido precioso a la tarta.

**Consejo**:
Si lo **metes en el congelador tres minutos antes de servirlo** quedará como una tarta helada y la textura será deliciosa.

[Paso a paso de la tarta de sandía aquí](https://madrescabreadas.com/2014/07/24/receta-tarta-de-sand%C3%ADa-las-tartas-no-siempre/)

 ![tarta de sandía](/tumblr_files/tumblr_n903geqHaR1qgfaqto3_1280.jpg)

## 4. Tarta de la abuela

Esta tarta es **especialmente ideal para hacerla con los peques** (ponles un babi porque se van a poner perdidos, pero lo van a pasar bomba).

### Ingredientes de la tarta de la abuela

* 1 paquete de galletas cuadradas María
* 1 L y ½ de leche
* Canela
* 1 sobre de polvos para hacer flan
* 2 tabletas de chocolate para postres
* Cositas para poner por encima de adorno

### Elaboración de la tarta de la abuela

1. Lo primero, se hace el flan, pero con 1/4 L más de leche del que pone en las instrucciones. Yo lo hice en el microondas, pero se puede hacer en el fuego en una olla.
2. Derretimos el 1 tableta de chocolate con un poco de leche en el microondas o en un cazo.
3. Mezclamos el chocolate con el flan antes de que se solidifique éste y removemos bien.
4. Mientras vamos mojando las galletas en leche con canela, pero primero **recomiendo tomarle el pulso a la galleta.** Si vemos que empapa demasiado la mojaremos ligeramente, y si vemos que son de las que no empapan casi al principio, las dejaremos más rato a remojo.

   Esta el la clave de la tarta, porque en el punto medio está la virtud y, ojo, que **hay galletas que engañan y parece que no absorben, y luego se te desintegran de repente.** Si son galletas traicioneras, cuidado que pueden hacer que fracase la tarta.
5. Colocamos una primera capa de galletas en un recipiente cuadrado (ésta es la parte que más les gusta a los niños, bueno y guarrear con las galletas mojadas chorreándoles hasta el codo, también)
6. Las cubrimos con el flan chocolateado que no esté ni muy líquido ni ya cuajado del todo.
7. Ponemos otra capa de galletas, y las cubrimos con otra capa de flan, y así hasta que queramos o nos quede flan.
8. Para la capa final o cobertura, derretimos la otra pastilla de chocolate con un poco de leche, que puede mezclarse con una tableta de chocolate blanco para que quede más vistosa, y la extendemos con una cuchara.
9. Y ahora viene **la parte más creativa para los peques: decorar la tarta.**

   He de decir que Princesita abusó de las bolitas, corazones y fideos rosas y lilas, de manera que se emocionó y hasta que quedó uno en el bote estuvo poniendo. Pero quién soy yo para frenar su arte? Así que el resultado fue otra capa rosa/lila un poco dura de más muy consistente y un poco empalagosa que derrochaba dulzor.
10. Para terminar, tapa con film transparente y se deja 24 h en la nevera.

Tenéis el [paso a paso de la tarta de la abuela aquí](https://madrescabreadas.com/2014/01/31/recetas-con-niños-tarta-de-la-abuela-no-os-voy-a/) 

![tarta de la abuela decoración](/tumblr_files/tumblr_n02v87GzRC1qgfaqto6_1280.jpg)

Una batidora de repostería es ideal para que las mezclas queden mucho más homogéneas, lo que te resultará en una tarta de acabado profesional.

[![](/images/uploads/batidora-amasadora-reposteria.png)](https://www.amazon.es/Mellerware-repostería-Inoxidable-movimiento-Lavavajillas/dp/B08LPSMS37/ref=pd_sbs_4/259-8252965-5643646?pd_rd_w=M8Elw&pf_rd_p=073ad75e-0f2f-4e00-af92-a91d92e1e105&pf_rd_r=ZKG1DQNB56SBXJBBDGGH&pd_rd_r=0e7a6182-86d4-4ecd-9065-dcea46beb84f&pd_rd_wg=uc2Dg&pd_rd_i=B08LPSMS37&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Mellerware-repostería-Inoxidable-movimiento-Lavavajillas/dp/B08LPSMS37/ref=pd_sbs_4/259-8252965-5643646?pd_rd_w=M8Elw&pf_rd_p=073ad75e-0f2f-4e00-af92-a91d92e1e105&pf_rd_r=ZKG1DQNB56SBXJBBDGGH&pd_rd_r=0e7a6182-86d4-4ecd-9065-dcea46beb84f&pd_rd_wg=uc2Dg&pd_rd_i=B08LPSMS37&th=1&madrescabread-21) (enlace afiliado)

## 5. Tarta de Ferrero

### Ingredientes de la tarta de ferrero

* 10 obleas grandes
* Un bote de Nutella
* Lacasitos, fideos de colores… o lo que se os ocurra para decorar

### Elaboración de la tarta ferrero

1. Se calienta la Nutella en el microondas para **que quede un poco líquida de manera que se extienda con facilidad** (no os paséis, que se quema). Aseguraos que no queda ningún trocito de la tapa dorada porque si no saltarán chispas!!
2. Sobre una superficie plana se coloca una oblea (un plato normal, no, porque no cabe y la oblea se rompe a la mínima), y se extiende una capa de Nutella.
3. Sobre ella se coloca otra oblea, y se extiende otra capa de Nutella, y así hasta 10 obleas.
4. Sobre la última se extiende una capa, esta vez más gruesa, y **se decora al gusto con lo que se os ocurra**.
5. Servir fría. La podéis **meter 30 minutos al frigorífico,** y triunfaréis seguro!

Tenéis el [paso a paso de la tarta de ferrero aquí](https://madrescabreadas.com/2015/06/15/tarta-obleas-nutella/)

![Tarta de Ferrero](/tumblr_files/tumblr_inline_npzz67EprS1qfhdaz_540.jpg)

## 6. Tiramisú para niños

### Ingredientes del tiramisú para niños:

* 1 paquete bizcochos de soletilla (de esos finitos)
* 250 g queso mascarpone (o en su defecto queso de untar)
* 250 g nata líquida para montar
* 1 cafetera de café o café soluble (descafeinado si van a tomar los niños)
* 7 cucharadas de azúcar
* cacao puro en polvo

### Elaboración del tiramisú para niños:

1. Se hace el café lo primero para que dé tiempo a que se enfríe. No hace falta ponerle azúcar, y **puede hacerse clarito o fuerte al gusto**, incluso descafeinado para que puedan comer los niños.
2. Se hace la crema mezclando primero con la batidora un poco de nata y el azúcar, y después montando toda añadiendo poco a poco el queso mascarpone o queso de untar normal hasta que quede consistente.
3. Se bañan los bizcochos en el café (ni mucho ni poco para que no quede demasiado seco ni demasiado empapado)
4. Y se pone una capa en un recipiente cuadrado.
5. Después se espolvorea con un colador el cacao.
6. Encima se pone una capa de crema.
7. Otra vez se espolvorea cacao.
8. Se pone otra capa de bizcocho bañado, otra vez cacao.

Queda mejor si la **última capa de crema es más gruesa,** y se cubre con bastante cacao.

Tenéis el [pasos a paso del tiramisú para niños aquí](https://madrescabreadas.com/2015/03/27/recetas-ninos-tiramisu/)

![Tiramisú](/tumblr_files/tumblr_inline_nlvi1vkaKk1qfhdaz_500.jpg)

Como es una actividad con los peques de casa, puedes hacer que se sientan como cocineritos profesionales con un kit de respotería para niños. Te aseguro que lo amarán.

[![](/images/uploads/utensilios-de-reposteria-para-ninos.png)](https://www.amazon.es/Kitchen-Craft-Lets-Make-Utensilios/dp/B001XNJFPO/ref=sr_1_1?crid=1HQD0KAQ0KJQ6&dchild=1&keywords=kit%2Bde%2Brepostería%2Bpara%2Bniños&qid=1634953688&qsid=259-8252965-5643646&sprefix=kit%2Bde%2Brepostaría%2Caps%2C327&sr=8-1&sres=B001XNJFPO%2CB075JRVRKM%2CB01MUYER39%2CB08B13HMNG%2CB08SR68LX2%2CB08WL23XSC%2CB08J9XKWTJ%2CB08J9NZFC3%2CB08MWKYGCZ%2CB0986XZN58%2CB09G6J4J35%2CB07LFXCX6Y%2CB014W5CIJY%2CB01BAOTVCM%2CB07QHT6339%2CB00QX35ZYO%2CB08BFLCT2T%2CB01KO0RE04%2CB08QYFNF9L%2CB094R5VDSM&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Kitchen-Craft-Lets-Make-Utensilios/dp/B001XNJFPO/ref=sr_1_1?crid=1HQD0KAQ0KJQ6&dchild=1&keywords=kit%2Bde%2Brepostería%2Bpara%2Bniños&qid=1634953688&qsid=259-8252965-5643646&sprefix=kit%2Bde%2Brepostaría%2Caps%2C327&sr=8-1&sres=B001XNJFPO%2CB075JRVRKM%2CB01MUYER39%2CB08B13HMNG%2CB08SR68LX2%2CB08WL23XSC%2CB08J9XKWTJ%2CB08J9NZFC3%2CB08MWKYGCZ%2CB0986XZN58%2CB09G6J4J35%2CB07LFXCX6Y%2CB014W5CIJY%2CB01BAOTVCM%2CB07QHT6339%2CB00QX35ZYO%2CB08BFLCT2T%2CB01KO0RE04%2CB08QYFNF9L%2CB094R5VDSM&th=1&madrescabread-21) (enlace afiliado)

## 7. Árbol de hojaldre con nutella

### Ingredientes del árbol de hojaldre

* 2 placas de hojaldre
* crema de cacao
* 1 huevo
* azúcar glas para decorar
* gominolas, frambuesas, lacasitos, conguitos… lo que tú quieras para decorar

### Elaboración del árbol de hojaldre:

1. Precalienta el horno a tope de temperatura.
2. En una bandeja de horno pon papel vegetal y **extiende con cuidado las placas de hojaldre.**
3. Unta una de ellas con crema de cacao.
4. Cúbrela con la otra.
5. Diseña tu árbol haciendo un gran triángulo y un tronco rectangular en la base (márcalo con el dedo)
6. Corta la silueta con un cuchillo y retira lo que sobra. Resérvalo para hacer la estrella y otras figuras que te apetezcan.
7. Haz franjas perpendiculares al tronco, pero **sin llegar hasta el centro del mismo,** que harán de ramas.
8. Gira cada rama para que se quede torneada.
9. Con el resto de masa que ha quedado al recortar el árbol haz estrellas, lacitos retorciendo tiras… o lo que se te ocurra, que puedes poner al pie del árbol como regalos o alrededor.
10. Mételo al horno, previamente precalentando a tope de temperatura hasta que esté dorado.
11. Déjalo enfriar, espolvorea con azúcar glass y decora con frambuesas, gominolas, lacasitos, conguitos, o lo que tengas por casa.

Paso a paso de la [tarta de árbol de hojaldre aquí](https://madrescabreadas.com/2016/12/31/arbol-hojaldre-chocolate/)

![Árbol de hojaldre con nutella](/images/uploads/captura-de-pantalla-de-2021-10-23-11-07-10.jpg)

Ten este post a mano para cuando llegue una fecha especial, y si te ha gustado, comparte! De este modo también contribuyes a que siga escribiendo este blog. 

*Photo de portada by Deva Williamson on Unsplash*