---
author: ana
date: '2020-12-22T17:19:29+02:00'
image: /images/posts/pagos-con-tarjetas/p-comprarcontarjeta.jpg
layout: post
tags:
- trucos
title: ¿Los pagos con tarjeta de crédito y nuestras emociones tienen relación?
---

Los [pagos con tarjeta](https://precio.com/tarjetas-credito/) vienen filtrados por el crédito, estos, a su vez, desde el remoto pasado, nos susurran que la operación mercantil toca el corazón, el centro simbólico de los sentimientos. Creer, confianza y fe se entremezclan para dotar de sustancia metafísica el simple y cotidiano acto de comprar.

La anécdota relata que las tarjetas de crédito nacieron porque llegado el momento de pagar un cliente no tenía dinero encima. El hecho ocurrió en el Major’s Cabin Grill, en la ciudad de Nueva York. Los comensales despreocupados eran Frank X. McNamara, de la «Hamilton Credit Corporation», Ralph Sneider, su abogado, y Alfred Bloomingdale, nieto del fundador de «Bloomigndale’s», o sea, personas como se suele decir, dignas de crédito.

## Repasemos la etimología

Según el [Diccionario de la Real Academia Española (DRAE)](https://dle.rae.es/crédito), la palabra "crédito" entre sus significados contempla "Reputación, fama o autoridad"; y en otra acepción: "Opinión que goza alguien de que cumplirá puntualmente los compromisos que contraiga". La reputación abre pues, la puerta al crédito.

Y "crédito" viene del latín _credere_, que traduce "creer". Mas lo interesante de este recorrido por la historia de la palabra es que su origen se remonta al indogermánico _kerd_, de donde viene, nada más y nada menos, que el latín _cor, cordis_, que traduce corazón.

Como bien sabemos, en muchos casos y en condiciones normales, comprar supone el uso del corazón. Es decir, pasa por el deseo y por la elección. En la variedad está el gusto, reza el dicho, y en lo que nos gusta, siempre está el corazón implicado.

![elegir](/images/posts/pagos-con-tarjetas/elegir.jpg)

Creer entonces es, propiamente, un asunto del corazón. Eso ya lo sabíamos, pero de pronto no tanto que el crédito en asuntos comerciales esté tan ligado -al menos desde la raíz de la palabra-, a ese músculo que se encuentra en la mitad del pecho.

Deseos, gustos, preferencias, caprichos, orientan el uso que le podemos dar a las tarjetas, según las necesidades o apetencias. El consumo deriva de comprar, que viene del latín _comparare_: es decir, antes de comprar, comparamos, sopesamos, elegimos.

![desear](/images/posts/pagos-con-tarjetas/desear.jpg)

La publicidad sale a la caza de estos sentimientos, buscando adelantarse a los gustos, les da forma y de alguna manera, tira de ellos. La prudencia y el conocimiento deben guiarnos para que no nos tiren del cabello las ofertas engañosas.

## En conclusión, las tarjetas de crédito son una expresión material de la credibilidad

Las tarjetas de crédito son una expresión material de la credibilidad. Y es seguro que no alcancen todos sus portadores, el mismo nivel de crédito de aquellos comensales en aquel restaurante neoyorquino de 1949. Sin embargo, es justo ahí donde radica la importancia de un valor, la confianza, que se constituye en la base de todo el sistema.

Creer da paso a la confianza y sin duda, es sobre este valor interpersonal que se levanta el régimen de las relaciones comerciales. Solo si hay confianza podemos llegar a acuerdos. Y si hay concordia, los corazones laten al unísono.

Sin credibilidad, las relaciones humanas no dan un paso. Y en el momento de la transacción comercial, existe al menos un segundo en el que antes que depositar dinero, depositamos confianza.

Si te ha gustado suscríbete al blog y comparte!

_Foto de portada by Clay Banks on Unsplash_

_Foto by Christian Koepke on Unsplash_

_Foto by Priscilla Jelle van Leest on Unsplash_