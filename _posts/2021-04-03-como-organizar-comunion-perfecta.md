---
author: luis
date: 2021-04-03 07:10:00
image: /images/uploads/d34587e4-7d35-44ef-a632-dae937da4f44.jpeg
layout: post
tags:
- familia
- comunion
title: Cómo organizar una comunión perfecta en tiempos de Covid
---

Al final del post encontrarás una **guía gratuita de comuniones 2021** con consejos para organizar paso a paso una comunión segura pero sin perder la magia. Además, incluye descuentos de empresas, la mayoría de mujeres emprendedoras, que te aliviarán un poco la carga económica que supone este evento para el presupuesto familiar.

Ya nos advirtieron hace más de un año que nos íbamos a enfrentar a una nueva normalidad, y no nos estaban engañando. Todo ha tenido que adaptarse a esta situación, incluido los grandes eventos como bodas, bautizos y comuniones. Son estas últimas las que van a acaparar nuestra atención en este artículo en el que os vamos a dar una serie de claves sobre cómo organizar una comunión perfecta en tiempos de COVID.

Puedes consultar uno de los posts más leídos del blog sobre [cómo celebrar la comunión sin arruinarte](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/), y [consejos para elegir tu vestido de comunión 2021
](https://madrescabreadas.com/2020/12/09/comprar-vestido-comunion-murcia/)


![niño de comunión con mascarilla](/images/posts/comunion-covid/comunioncoronavirus.jpg)

## ¿Qué precauciones debes tomar al celebrar una comunión en tiempos de Covid?

Lo primero que debes tener en cuenta a la hora de organizar una comunión en tiempos de Coronavirus es la normativa vigente, en la cual se establecerán horarios para locales, toque de queda, número de asistentes a este tipo de eventos, etc.

No podemos ser más concretos en este aspecto porque tendríamos que hacer una lista por Comunidades Autónomas, que son las que tienen la potestad para este tipo de cambios. Así que la normativa dependerá del lugar en el que residas y vaya a tener lugar el evento.

Otro hándicap será que las normas varían cada 15 días, por norma general, según la evolución de la pandemia en cada territorio. Hoy podríamos deciros lo establecido hasta la fecha pero no sería una información del todo veraz, ya que está sujeta a cambios.

Lo que sí podemos hacer es dejarte una serie de consejos para celebrar la comunión perfecta a pesar de la situación que estamos atravesando. Consejos generales que te serán de gran ayuda para que nada arruine el gran día de tu hijo o hija.

## Consejos para celebrar una comunión perfecta a pesar de la pandemia

Como ya todo es lo suficientemente difícil, queremos aportar nuestro granito de arena con estos consejos que te pueden ayudar a la hora de planificar la celebración perfecta de comunión.

### Consejos sobre el lugar de celebración de la comunión

La opción más recurrente es optar por un local de celebraciones o restaurante que se encargue de todo lo relacionado con el catering y el local. Nuestra primera recomendación en este punto será que tengas una buena comunicación con los encargados, dejando siempre claro el número de comensales, mesas, etc. En este aspecto también debes tener en cuenta que debes evitar reunir a más de dos unidades familiares por mesa y siempre es mejor una, y siempre de acuerdo a las normas.

Recuerda preguntar su política de cancelación, porque puede que la situación para la fecha de la ceremonia haya cambiado y se pueda llegar a cancelar. La mayoría de locales están respetando las cancelaciones por el tema de la pandemia pero siempre es mejor preguntar que quedarte con la duda y luego llevarte una sorpresa.


La segunda opción, cada vez más demandada debido a la pandemia es hacerla en casa, sobre todo si dispones de terraza o jardín. Además esto reduce el número de invitados, haciendo la reunión más íntima y evitando las posibilidades de contagio.

![mesa celebración al aire libre](/images/posts/comunion-covid/celebracion.jpg)

En ambos casos debemos de exigir o garantizar las medidas de higiene y protección contra el COVID, ya sea en un local en nuestro hogar. Aquí viene la segunda recomendación y es que siempre es más aconsejable optar por lugares al aire libre que sitios cerrados.

### Consejos sobre comida y menú de la comunión

En este aspecto, mi primera recomendación sería el evitar los tipo buffet o self service, ya lo realices en un local, restaurante en tu casa. Si es esta última opción, intenta que sólo sea una persona la encargada de manipular la comida.

Para las celebraciones en el hogar o en algún chalet o similar, también es recomendable que la vajilla sea desechable y usar cubiertos de madera (procurar que sean de un material biodegradable). Así, al finalizar, todo irá directamente a la basura.

Evita los típicos platos al centro, más si en una mesa habrá gente de distintas unidades familiares. Muchos restaurantes y locales de celebraciones dan como opción el plato de entrantes individual por comensal.

### Mesas dulces o Candy Bar adaptadas al COVID

Otra preocupación son las mesas dulces o Candy Bar, ya que éstas también deberán cumplir las precauciones anti COVID. Es recomendable que esté todo envasado individualmente, para evitar que todos estén tocando el mismo lugar para coger un dulce.

![Candy bar comunión](/images/posts/comunion-covid/candybar.jpg)

### Consejos de regalos y decoración de la comunión

El COVID nos puede ayudar en la decoración y es que hay que sacarle partido hasta a las peores situaciones. Podemos poner un stand de COVID Free en el que ofrezcamos mascarillas extra, gel de mano, toallitas de gel hidroalcohólico, etc.

También se está popularizando como regalo de agradecimiento a los invitados los kit de higiene, que también lleva un poco de lo anteriormente mencionado y hace que nuestra celebración cuente con todas las medidas higiénicas necesarias.

Podemos decorar las botellitas de gel con una pegatina con el nombre del comulgante y la fecha.


La última recomendación es que disfrutéis lo máximo posible del día. Siguiendo estos consejos y manteniendo siempre el sentido común, estamos convencidos de que lograréis encontrar el punto adecuado a cómo organizar una comunión perfecta pese a la pandemia.

Y para ayudarte hemos creado esta guía gratuita con consejos para organizar paso a paso una comunión segura pero sin perder la magia. Además, incluye descuentos de empresas, la mayoría de mujeres emprendedoras, que te aliviarán un poco la carga económica que supone este evento para el presupuesto familiar.


<a href="https://mailchi.mp/3487caa89f08/7su9mxwyew" class='c-btn c-btn--active c-btn--small'>Descargar gratis guía comuniones 2021</a>

Si te ha sido de ayuda, comparte!

Suscríbete al blog para no perderte nada.

*Portada by Gabriella Clare Marino on Unsplash*

*Photo by Gabriella Clare Marino on Unsplash*

*Photo by Katrien Sterckx on Unsplash*

*Photo by Chasse Sauvage on Unsplash*