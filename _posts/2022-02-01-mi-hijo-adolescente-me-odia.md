---
layout: post
slug: mi-hijo-adolescente-me-odia
title: Te odio y otras mentiras que escucharás de tu adolescente
date: 2022-02-03T17:53:09.948Z
image: /images/uploads/istockphoto-1212309681-612x612.jpg
author: faby
tags:
  - adolescencia
---
La adolescencia es una etapa de desarrollo muy difícil en la que se crea una brecha de distanciamiento entre padres e hijos. **¿Tu hijo dice que te odia?** Si tu hijo dice que eres mala no significa que sea verdad. Recuerda que en el calor de una discusión se dicen cosas que realmente no son ciertas.

En esta etapa difícil es necesario que **no pierdas la cordura** y orientes a tu hijo hacia una convivencia de respeto. De hecho, esos gritos insultantes, los portazos en los que deja de hablarte y dice que te odia no deben llevarte a responder al mismo nivel ni a tomarte al pie de la letra sus palabras, ya que enrealidad es cuando mas necesitan tu amor.

Aqui te dejo nuestra lista de [libros favoritos para madres de adolescentes](https://www.amazon.es/create/collection?id=amzn1.ideas.2Y9ZGCQHAZACW) (enlace afiliado) que te ayudaran a sobrellevar esta etapa lo mejor posible.

En esta entrada hablaremos de lo **que puedes hacer para que tu hijo te respete**. Al mismo tiempo, hablaremos de por qué un hijo puede decir que odia a su madre. ¡Empecemos!

## ¿Por qué un hijo odia a su madre?

El odio es un sentimiento profundo y malsano, ya que implica desear que el objeto del odio no exista. Así que en la práctica, si una persona odia a otra, desea que muera. Este sentimiento puede ser “justificado” en caso de **abusos de menores o faltas graves de violencia,** en el que la madre no ha cumplido con su papel.

Sin embargo, este escenario no es muy común. En tal sentido, cuando un adolescente le dice a su madre que la odia, en realidad no lo esta diciendo en serio. Más bien, está **expresando su disgusto hacia una “regla” que la madre quiere que él obedezca.**

De modo que, no te tortures pensando “qué hacer si mi hijo adolescente me odia”. Ese no es el verdadero sentimiento que está experimentando.

## ¿Qué hacer si tu hijo adolescente te dice que te odia?

![adolescente discutiendo con su madre](/images/uploads/istockphoto-517323339-612x612.jpg)

El odio que los hijos dicen tener a sus padres en una discusión de “normas de convivencia” no es real. Así que, **no lo tomes a pecho.** Sigue estos consejos:

* **Mantén la calma**. Trata de comprender su frustración y no generes una nueva discusión porque acaba de decir que te odia. Si has impuesto una norma, no cedas. Recuerda que tu hijo está utilizando esa expresión de un modo visceral y seguro que posteriormente se va a arrepentir, aunque no te lo diga.
* **No te defiendas.** Algunos padres empiezan argumentar con sus hijos diciendo “¿por qué me odias” “te estoy cuidando” “he dado todo por ti”. Eso irrita más a tu hijo, así que mejor ignora sus palabras de odio y deja“que él (a) piense lo que quiera”. No es el momento de intenta razonar con el.
* **Dale espacio**. Cuando él exprese su odio, debes mantener la calma. Así que en voz firme puedes decir “hablaremos luego” y te retiras. De esta forma le das tiempo a que se calme. Al mismo tiempo, demuestras que no cedes fácilmente.

Si tras algunas horas tu **hijo adolescente no te habla**, no te preocupes. Está haciendo una rabieta. Deja que se calme. Puedes intentar ser amable, incluso usar el humor y emplear alguna palabra tipica de adolescentes para romper el hielo. Si no se te ocurre ninguna, aqui te dejo este [diccionario de terminos adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) que seguro te sera de gran utilidad.

## ¿Qué hacer cuando un hijo adolescente le falta el respeto a su madre?

![adolescente faltando al respetoa su madre](/images/uploads/istockphoto-1255589431-612x612.jpg)

La falta de respeto es habitual durante el desarrollo. Claro, desde la infancia hay que ayudar a los hijos a controlar la frustración. Es decir, **debes enseñarle a que no hagan rabietas y expresen sus disgustos sin herir.**

Desde luego, con tantos cambios hormonales, es posible que tu hijo cambie durante la adolescencia. No tienes por qué tolerar en casa las faltas de respeto. Tu labor de madre no ha cesado aunque tu hijo desee autonomía e independencia. Sigue estos consejos:

* **Ten paciencia.** Si tu hijo te ha gritado no te rebajes a su nivel devolviendo los gritos. Comprende que está frustrado, pero no contribuyas a que siga faltándote al respeto. Cierra la discusión diciendo “hablaremos cuando estés calmado”.
* **Conversen francamente.** Después de unas horas toma la iniciativa de conversar. Mantén un buen tono y empieza la conversación diciendo “¿por qué te molestaste esta tarde?” Si él empieza a elevar la voz, enseguida corrígelo y dile que si grita no lo escucharas.
* **Presta atención a lo que dice.** Trata de comprender qué es lo que quiere. No lo interrumpas, quizás lo que pide no tiene importancia para ti, pero para él si la tiene. De este modo, demuestras con el ejemplo que también lo respetas.
* **Oriéntalo respecto a sus emociones.** Si él se ajusta al tono durante la conversación está aprendiendo a respetarte y eso tiene mérito. Explícale de forma firme y cariñosa que te duele que te trate de esa manera.
* **Sé razonable.** Pide disculpas si de algún modo actuaste mal. Pero no lo presiones a que haga lo mismo. Ahora bien, evalúa su petición (lo que generó la discusión inicial). Quizás puedes decirle que pensarás en lo que te ha pedido (esto solo aplica si su petición no supone un peligro grave para él). Y aclara que hablando de forma calmada las cosas marchan mejor.

## ¿Cómo manejar a los adolescentes difíciles?

![madre dialogando con adolescente](/images/uploads/istockphoto-1176301048-612x612.jpg)

Algunos jóvenes pueden ser muy difíciles con su deseo de marcar la diferencia pero al mismo tiempo buscar la aprobación de los demás. Puede que la causa de su frustración es que[ le cueste conseguir amigos.](https://madrescabreadas.com/2021/01/21/hijo-adolescente-no-tiene-amigos/)

[](https://madrescabreadas.com/2021/01/21/hijo-adolescente-no-tiene-amigos/)Presta atención a las siguientes sugerencias a fin de hacer esta etapa más llevadera.

* **No pierdas la paciencia.** Nunca disciplines a tu hijo mientras estás molesta. Si vas a imponer un castigo procura que sea después de haberte calmado.
* **Coloca reglas.** Aunque a los adolescentes no les guste tienen que apegarse a las reglas de la casa, las cuales deben ser claras. Estas deben ser razonables y estar acorde a la edad.
* **Premios.** Recompensa a tus hijos si se apegan a las normas o realizan sus quehaceres. Enseña que la libertad se gana con responsabilidad, lo que incluye cumplir con las normas de la casa. Si no cumplen, quita algunos privilegios. Una biena idea puede ser preparar un [viaje en familia que le haga ilusion a tu adolescente](https://madrescabreadas.com/2022/11/07/viajes-originales-adolescentes/).
* **Trabaja en equipo.** Nunca te contradigas con tu pareja, apoya las decisiones de tu pareja para que los hijos no pierdan el respeto.

En resumidas cuentas, vivir con adolescentes no es fácil. Aunque es evidente que te sientes frustrada por sus cambios de humor y pretensiones de llevar la delantera en todo, la verdad es que esta conducta es; en cierto sentido, sana, debido a que evidencia que tu hijo está forjando sus propias ideas y personalidad.

La [disciplina positiva](https://disciplinapositivaespana.com) te ayudara en esta fase tan dificil a veces.

Para él es muy difícil no entenderse, pero con paciencia, amor y comunicación afectiva podrás ayudarlo a atravesar este viaje.