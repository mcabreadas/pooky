---
layout: post
slug: dia-mundial-de-los-oceanos
title: Cómo celebrar el Día Mundial de los océanos con la familia
date: 2022-06-10T16:32:04.310Z
image: /images/uploads/oceano.jpg
author: faby
tags:
  - educacion
---
Los padres y madres deben enseñar a los más pequeños a celebrar ciertas efemérides como excusa para enseñar a nuestros hijos ciertos valores que les ayudaran a su desarrollo como personas, como por ejemplo [el dia de los abuelos](https://madrescabreadas.com/2021/07/25/dia-de-los-abuelos-espana/) o el dia de los oceanos, que segun la *[UNESCO](https://es.unesco.org/DiaMundialdelosOceanos)* se celebra todos los **8 de junio.**

Las Naciones Unidas han proclamado la celebración de la misma desde el 2009. Sin embargo, la ONU ha demostrado durante décadas su interés en que la población en general pueda concientizarse respecto a la **importancia de cuidar la biodiversidad y la vida en el Océano.**

En esta oportunidad, ofreceré algunas ideas para celebrar esta interesante efeméride. 

## ¿Por qué se celebra el Día Mundial de los Océanos?

El día de los océanos es una celebración a nivel mundial, que tiene como finalidad fomentar el apoyo hacia los actos de concientización de la salud y el buen estado de los océanos. 

Para nadie es un secreto que **la vida marina está siendo amenazada** constantemente. Cada año  llegan toneladas de basura o desechos al mar; plástico, combustibles, bolsas, causando la muerte de más de cien mil especies marinas. 

Entonces, este día es un recordatorio para que las Organizaciones gubernamentales, pescadores y la población en general **cuiden la biodiversidad marina.**

## ¿Cómo celebrar el día de los océanos en familia?

Para que este día tenga significado para los más pequeños, es necesario **hacer algo diferente como familia**. A continuación, compartiré algunas ideas para celebrar el 8 de junio, el día de los océanos.

### Visita un acuario

Los acuarios permiten que toda la familia desarrolle **amor por los animales marinos,** e incluso pueden aprender mucho de ellos. Será la base para defender su hábitat y seguridad.

Otra idea es **tener un pequeño acuario en casa,** de esta forma se puede disfrutar de bellos pececitos.

[![](/images/uploads/fluval-flex-kit-de-acuario.jpg)](https://www.amazon.es/Fluval-Flex-Kit-Acuario-Negro/dp/B01MRIQW7K/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=ATRVRQFG2O66&keywords=acuario&qid=1652899259&sprefix=acuari%2Caps%2C391&sr=8-5&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Fluval-Flex-Kit-Acuario-Negro/dp/B01MRIQW7K/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=ATRVRQFG2O66&keywords=acuario&qid=1652899259&sprefix=acuari%2Caps%2C391&sr=8-5&th=1&madrescabread-21) (enlace afiliado)

### Mira un documental

Si tus hijos son pre-adolescentes, puede ser muy útil ver un documental que explique el daño que se le está infringiendo a los mares. Puedes ver algún material audiovisual que hable del cambio climático, como por ejemplo, El futuro del Ártico, en busca del coral, Océanos, entre otros.

[![](/images/uploads/oceanos-dvd-bd-.jpg)](https://www.amazon.es/Oceanos-DVD-Blu-ray-Jacques-Perrin/dp/B0053CBBR6/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2UUSHPW714RBA&keywords=documental+%C3%B3ceanos&qid=1652898675&sprefix=documenta%2Caps%2C639&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Oceanos-DVD-Blu-ray-Jacques-Perrin/dp/B0053CBBR6/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2UUSHPW714RBA&keywords=documental+%C3%B3ceanos&qid=1652898675&sprefix=documenta%2Caps%2C639&sr=8-2&madrescabread-21) (enlace afiliado)

### Bolsas ecológicas

Otra forma de celebrar este día y al mismo tiempo ayudar a los niños a concientizarse sobre la amenaza en los mares, es hacer compras más ecológicas. Por ejemplo, en vez de comprar bolsas de plásticos, se puede **adquirir bolsas reutilizables,** las bolsas de compra con forma de caja son una de las mejores, son resistentes y tienen amplia durabilidad.

[![](/images/uploads/bolsas-de-la-compra-reutilizables.jpg)](https://www.amazon.es/Reutilizables-comestibles-unidades-reforzados-respetuoso/dp/B06XNLWB3B/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1AF295JOX3643&keywords=bolsas%2Breutilizables%2Bcompra&qid=1561877152&s=gateway&sprefix=bolsas%2B%2Caps%2C481&sr=8-25-spons&linkCode=sl1&tag=concursator-21&linkId=0c9e9c0ad5507d19b141afb937468e45&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Reutilizables-comestibles-unidades-reforzados-respetuoso/dp/B06XNLWB3B/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1AF295JOX3643&keywords=bolsas%2Breutilizables%2Bcompra&qid=1561877152&s=gateway&sprefix=bolsas%2B%2Caps%2C481&sr=8-25-spons&linkCode=sl1&tag=concursator-21&linkId=0c9e9c0ad5507d19b141afb937468e45&th=1&madrescabread-21) (enlace afiliado)

Si estás en búsqueda de bolsas más ligeras, las de **maya o rejilla son una buena opción.** Tiene asas y son ideales para transportar vegetales o frutas.

[![](/images/uploads/bolsas-de-malla-reutilizables.jpg)](https://www.amazon.es/Creatiee-reutilizable-organizador-alimentos-almacenamiento/dp/B07CV97DQD/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1AF295JOX3643&keywords=bolsas%2Breutilizables%2Bcompra&qid=1561877152&s=gateway&sprefix=bolsas%2B%2Caps%2C481&sr=8-20&linkCode=sl1&tag=concursator-21&linkId=cbd81d9a16711b9dbddcfbb666cadf14&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Creatiee-reutilizable-organizador-alimentos-almacenamiento/dp/B07CV97DQD/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1AF295JOX3643&keywords=bolsas%2Breutilizables%2Bcompra&qid=1561877152&s=gateway&sprefix=bolsas%2B%2Caps%2C481&sr=8-20&linkCode=sl1&tag=concursator-21&linkId=cbd81d9a16711b9dbddcfbb666cadf14&th=1&madrescabread-21) (enlace afiliado)

Las **bolsas de algodón** tienen una excelente resistencia y son muy cómodas de usar. Sirve para llevar las compras de alimentos.

[![](/images/uploads/leviatan-set-de-10-bolsa-de-algodon-natural.jpg)](https://www.amazon.es/LEVIATAN-pesada-paquete-natural-unidades/dp/B085V6WX7N/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1TWV7VLQKV663&keywords=bolsas%2Becologicas&qid=1652900100&sprefix=bolsas%2Becologicas%2Caps%2C320&sr=8-5&th=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/LEVIATAN-pesada-paquete-natural-unidades/dp/B085V6WX7N/ref=sr_1_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1TWV7VLQKV663&keywords=bolsas%2Becologicas&qid=1652900100&sprefix=bolsas%2Becologicas%2Caps%2C320&sr=8-5&th=1&madrescabread-21) (enlace afiliado)

En conclusión, tienes muchas formas de celebrar el día mundial de los océanos. Sea cual sea tu plan para ese día, recuerda que lo más importante es **cuidar los océanos durante todo el año.** Así pues, hay que hacer actividades relacionadas al cuidado ambiental con mucha frecuencia, para que los más pequeños empiecen a concientizarse desde temprana edad.