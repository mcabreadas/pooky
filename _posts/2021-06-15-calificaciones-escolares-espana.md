---
layout: post
slug: calificaciones-escolares-espana
title: ¿Premios o castigos por las notas?
date: 2021-06-15T16:57:35.941Z
image: /images/uploads/mq2-6.jpg
author: maria
tags:
  - vlog
  - educacion
---
Esta semana hablamos de las notas, y reflexionamos sobre ¿Que significan las notas escolares? 

Ten en cuenta que las calificaciones no definen a tu hijo.

¿Cuanto te importan las calificaciones escolares?

Mira el video y dejame tu opinion.

<iframe width="560" height="315" src="https://www.youtube.com/embed/2vnzMDdUHjk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha gustado, comparte y suscribete!