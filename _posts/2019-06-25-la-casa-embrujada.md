---
layout: post
slug: la-casa-embrujada
title: La Casa Embrujada de Primos S.A.
date: 2019-06-25T09:00:29+02:00
image: /images/posts/2019-06-25-la-casa-embrujada/p-portada-la-casa-embrujada.jpg
author: .
tags:
  - 6-a-12
  - planninos
  - libros
---

Para mí el mejor indicativo de que un libro infantil es bueno para recomendároslo es que mi hijo no lo suelte hasta que se lo termina. Es cierto que él es muy fan de los libros de misterio en los que hay que resolver enigmas, y si lo hacen un grupo de niños que además son primos, y tienen un perro listo que les saca de apuros, como [La Casa Embrujada, de Literatura SM](https://es.literaturasm.com/libro/casa-embrujada), pues ya tiene todos los ingredients necesarios para que triunfe.

Esta lectura es ideal para niños a partir de 9 años, donde el lector se convierte en parte activa de la historia al tener que ir resolviendo enigmas al final de cada capítulo y llegar a sus propias conclusiones.

La historia se ubica en un pueblo de Galicia, en el que viven Pablo y su hermana Verónica, e invitan a sus primos. Todo gira entorno a una casa misteriosa y a extraños sucesos acontecidos en el pueblo que los envolverán en una excitante aventura que afrontarán cada uno con sus cualidades pero formando un gran equipo; Pablo es el más analítico, Javier, el valiente, y Natalia es la experta en plantas, la más pequeña y un poco miedosa.

Pero lo que más le ha gustado a mi hijo es el papel que juega el perro Jazz en la resolución del misterio.

La saga Primos S.A. sin duda os recordará, como a mí, a los clásicos Cinco, de Enid Blyton, con los que nuestra generación ha crecido. Y es que este formato de historia es un éxito asegurado para estas edades.

Recordáis los [libros de Los Cinco](https://madrescabreadas.com/2015/10/09/libros-infantiles-cinco/)?