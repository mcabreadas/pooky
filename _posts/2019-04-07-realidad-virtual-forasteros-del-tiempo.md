---
layout: post
slug: realidad-virtual-forasteros-del-tiempo
title: Libros Los forasteros del Tiempo y realidad virtual
date: 2019-04-07T18:19:29+02:00
image: /images/posts/forasteros-del-tiempo/p-gafas-realidad-virtual.jpg
author: .
tags:
  - 6-a-12
  - planninos
  - libros
---

Hoy quiero hablaros de la colección de libros infantiles que nos ha enganchado en casa, sobre todo a mi hijo mediano. Se trata de la serie [Los Forasteros del Tiempo](https://es.literaturasm.com/libro/forasteros-tiempo-6la-aventura-de-balbuena-entre-dinosaurios), editorial SM, de Roberto Santiago, el mismo autor de Los Futbolísimos. 

![portada libro forasteros del tiempo](/images/posts/forasteros-del-tiempo/portada-forasteros-del-tiempo.jpg)
    

En esta serie de libros recomendada para lectores a partir de 9 años los protagonistas viajan a través del tiempo y descubren todas las curiosidades de la época a la que viajan. En esta última entrega se incluyen además unas gafas de realidad virtual para poder viajar en el tiempo casi de verdad. A mis hijos les encanta ponérselas, incluso al pequeño, que ha quedado totalmente maravillado con la experiencia.

Mi pequeño lector califica el libro de “un poco comedia” e “historia entretenida” donde la familia Balbuena, los protagonistas de la saga, va por la selva en el jurásico entre dinosaurios y tienen que ir sobreviviendo.
Y para quien no los conozca todavía, la familia Balbuena eran un familia normal que un día estaban comprando con sus vecinos una bicis nuevas y de repente les atrapó un agujero negro y los llevó a la época medieval (primer libro). Pero cada vez que hay una tormenta los vuelve a atrapar un agujero negro y los lleva a otra época.

Sebas es el protagonista, y junto con su padre, su hermano Santi, y su vecina Mari Carmen y la madre de ésta correrán todas estas aventuras.

## Forasteros del tiempo y realidad virtual

Las gafas de realidad virtual se usan para descubrir los objetos que los Balbuena han ido dejando en las distintas épocas que relata cada libro.
![app forasteros del tiempo](/images/posts/forasteros-del-tiempo/pantallazo-app-forasteros-del-tiempo.jpg)

También hay una opción para que una locución vaya narrando los capítulos ideal para niños más pequeños que les interese la historia pero todavía no tengan la habilidad lectora necesaria para estos libros.


Muy recomendable!