---
date: '2020-06-22T11:19:29+02:00'
image: /images/posts/proteccion-solar-ladival/p-sombrilla.jpg
layout: post
tags:
- trucos
- bebes
- recomendaciones
- salud
- ad
- crianza
title: Triple protección solar para niños y bebés
---

Ha llegado el verano y con él los primeros baños en playas y piscinas y los primeros rayos de sol sobre nuestras pieles blancas y desprotegidas tras un invierno sin apenas exposición. También han llegado ya las primeras quemaduras porque, aunque rara es la familia que no usa protector solar y que no está conciencia con la importancia de cuidar la piel de niños y bebés sobre todo por los riesgos de cáncer de piel, me he dado cuenta de que pocas aplican las cremas correctamente. Por eso os voy a dar unos consejos para que vayáis bien protegidos toda familia.

## Usa un protector solar correcto para niños y bebés

¿En qué me tengo que fijar para elegir el protector solar para mis hijos? Tenemos que mirar la etiqueta muy atentamente:

-Tiene que ser de de amplio espectro, es decir que ponga expresamente que proteje correctamente contra los rayos ultravioleta A y B y también frente al infrarrojo A.

-Debe ser pediátrico ya que la piel de los niños es diferente a la de los adultos, y como tal debemos protegerla con un producto específico para ella. También hay protectores solares específicos para pieles atópicas que son muy recomendables, como éstos de Ladival [https://ladival.es/ladival-ninos-y-piel-atopica](https://ladival.es/ladival-ninos-y-piel-atopica)

![](/images/posts/proteccion-solar-ladival/ladival.jpg)

-Tiene que tener un FPS de 50 o superior. La [Academia Americana de Dermatología](https://www.aad.org) recomienda que todos los niños, independientemente del tono de piel que tengan, se pongan protector solar con un FPS de 30 o superior. Aunque las personas de piel oscura tienen más melanina protectora y se broncean con más facilidad, en vez de quemarse, el bronceado es un indicador de lesión en la piel por efecto del sol. Los niños de piel oscura también se pueden hacer quemaduras solares dolorosas.

-Tiene que ser resistente al agua.

-Debe tener filtros físicos, mejor que químicos.

-No debe contener PABA, ya que pueden causar alergia en la piel.

![crema solar niños](/images/posts/proteccion-solar-ladival/manos.jpg)


## Aplica el protector solar correctamente

-Desecha los protectores solares que hayan caducado o que tengan más de 3 años.

-Aplícalo 20 minutos antes de la exposición

-Repón crema cada 2 horas o antes si salen del agua o han estado sudando.

-Sé generosa en la cantidad de producto que pones. Es mejor que cubra bien toda la superficie de la piel.

-Usa crema para la cara, cuello y hombros para lograr una mayor cobertura, y protector en spray si te resulta más cómodo y práctico para el resto del cuerpo (sobre todo si son niños que no paran quietos). Ladival ha sacado este protector solar en spray súper fácil de aplicar, incluso por ellos mismos [https://ladival.es/ladival-spray-ninos-y-piel-atopica/](https://ladival.es/ladival-spray-ninos-y-piel-atopica/)

![protector solar spray](/images/posts/proteccion-solar-ladival/ladival-spray.jpg)

## Otras medidas para protegernos del sol

-Limita la duración de la exposición al sol y procura hacerlo en las horas del día menos peligrosas: hasta las 11:00 h, y después de las 17:00 h.

-Ponles gorras o sombreritos.

![niño en la playa con gorro](/images/posts/proteccion-solar-ladival/gorro.jpg)

-Hidrátalos bien con agua, frutas o verduras frescas. Úsalas como tentempié.


## Zonas olvidadas

No nos olvidemos de las orejas, las manos, los pies, los hombros y detrás del cuello! Levanta las tiras de los trajes de baño y aplica protector solar debajo por se desplazaran con el movimiento. Protéjele también los labios usando una manteca de cacao con un FPS de 30 al menos.

Ya tienes todas las claves para disfrutar del sol ese verano de forma segura. Si te han servido de ayuda comparte par ayudar a otras familias!







*Photo by Feri & Tasos on Unsplash niño con cubo*

*Photo by Vidar Nordli-Mathisen on Unsplash sombrilla*

*Photo by Limor Zellermayer on Unsplash niños de la mano*