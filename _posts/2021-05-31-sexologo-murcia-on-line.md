---
author: maria
date: 2021-06-03 08:37:00.516000
image: /images/uploads/etienne-girardet-npedxzkscsm-unsplash.jpg
layout: post
slug: sexologo-murcia-on-line
tags:
- mujer
- salud
- postparto
- invitado
- crianza
title: Cómo me puede ayudar un sexólogo
---

## ¿Qué es un sexólogo o sexologa?

Los sexólogos somos los profesionales de los sexos, entendiendo sexo de una manera amplia, más allá de las relaciones sexuales. Trabajamos sobre cómo nos vivimos como hombres o mujeres entendiendo que cada persona es única e irrepetible.

## ¿Cómo me puede ayudar un sexólogo o sexóloga?

El trabajo de un sexólogo puede ser:

A través de la [Educación Sexual](https://madrescabreadas.com/2013/11/19/prevención-del-abuso-sexual-nfantil-juego-del/), que tiene como objetivos: conocerse, aceptarse y relacionarse de manera satisfactoria.

Realizando asesoramiento sexológico, es decir, atendiendo dudas puntuales que nos pueden surgir.

Mediante terapia sexológica, para tratar dificultades tanto a nivel individual como de pareja relacionadas con las relaciones sexuales o relaciones de amor.

## ¿Qué es M de Mamen?

[M de Mamen](https://www.mdemamen.es) es un espacio donde encontrar información, formación y asesoramiento personalizado relacionado con la sexualidad, maternidad y crianza desde un enfoque integral, actualizado y, sobre todo, respetuoso. Fruto de la experiencia personal y profesional de su creadora, Mamen Baeza Hernández, quien, a través de este proyecto busca acompañar a mujeres, sean madres o no, para que se conozcan, se vivan y se disfruten. 

![](/images/uploads/186291670_371853097588852_544886390637246941_n.jpg)

## ¿Quién es Mamen?

Soy una mujer trabajadora y luchadora (como la mayoría), inconformista por naturaleza y en continua búsqueda de nuevos retos que me hagan crecer profesional y personalmente. Mamá de una niña de casi 3 años y de otros dos bebés que me acompañan brillando muy alto. Además, soy Terapeuta Ocupacional experta en Atención Temprana, Sexóloga, asesora de Porteo y de Lactancia. Casi toda mi experiencia profesional ha sido trabajando con peques y sus familias (sobre todo mamás) pero no fue hasta que me convertí en madre cuando conseguí empatizar de verdad y entender a todas esas mujeres que tenía delante.

Este conjunto de formaciones y vivencias (profesionales y personales), me ha hecho ver la necesidad que tenemos las mujeres de quitarnos la culpa con la que cargamos, de no dejar espacio a las críticas que no aporten y de disfrutar(nos) la vida. He comprendido lo maravillosa que es la maternidad, pero también esa otra parte dura y difícil que no nos contaron. Y con esto me refiero a ese sentimiento de no sé si lo estoy o lo sabré hacer bien, ese continuo dudar de nosotras mismas, esa falta de tiempo, de tiempo para nosotras, para cuidarnos, mimarnos y querernos, para estar con amigas, para nuestro ocio… Me refiero también a todos esos cambios, en todas y cada una de las parcelas de nuestra vida: trabajo, pareja, nuestro propio cuerpo… Por supuesto dando cabida a todo tipo de maternidades, ¡viva la diversidad!

Con todo esto, mi propósito no es otro que acompañarte para hacerlo más fácil, hacerlo tuyo y hacerlo bonito.

## ¿Qué servicios se pueden encontrar en M de Mamen?

###  *Información* sobre sexualidad actualizada y rigurosa

tanto a través del blog como del perfil de Instagram [@sexologamamen](https://www.instagram.com/p/CO-7sEqBejM/). Puedes encontrar post relacionados con diversas temáticas: porteo ergonómico, lactancia materna, educación sexual y crianza. 

###  *Formación*

Hasta antes de confinarnos se estaban realizando cursos y talleres presenciales que pronto volverán a ponerse en marcha cumpliendo con todas las medidas de seguridad. Próximamente en la web se dispondrá de formación online, para que puedas acceder desde cualquier parte. Serán cursos en directo vía Zoom que quedarán grabados para todas aquellas personas que no puedan participar en el momento acordado.

### *Asesorías personalizadas* sobre sexualidad

Así como de lactancia materna o porteo. Estas asesorías pueden hacerse presenciales o de manera virtual.

## **M de Mamen SHOP**

La [tienda online de M de Mamen](https://www.mdemamen.es/tienda/) nace en febrero con el objetivo de complementar al resto de servicios que se ofrecen en la web. En ella podrás encontrar 3 líneas de productos:

### Comprar Portabebés ergonomicos

Fulares elásticos y mochilas de porteo que te permitirán llevar a tu bebé contigo de una forma ergonómica para ambos y de darán esa libertad de brazos que tanto necesitamos. 

### Comprar productos infantiles

Algunos accesorios y productos de cuidado del bebé.

### Comprar productos para la mujer(mi favorita):    

[Productos de higiene menstrual sostenibles](https://www.mdemamen.es/categoria-producto/mujer/) ([bragas de fibra de bambu](https://www.mdemamen.es/producto/bragas-menstruales-en-fibra-de-bambu/) y [copas menstruales](https://www.mdemamen.es/producto/copa-menstrual-tamano-b/))

[Productos de autocuidado](https://www.mdemamen.es/categoria-producto/mujer/) ([cosmética ecológica](https://www.mdemamen.es/producto/protector-solar-facial-con-color-de-munnah/), [velas](https://www.mdemamen.es/producto/siesta-estival-vela-no-246-lata-80grs-de-the-singular-olivia/)…)

Productos para el disfrute ([juguetería erótica](https://www.mdemamen.es/producto/tiani-3-deep-rose-de-lelo/))

Cuando decidí abrir la tienda, me esforcé mucho en elegir todos y cada uno de los productos que quería que formasen parte de ella. Lejos de querer contar con un gran catálogo de productos, tenía muy claro que el objetivo era tener una selección de los mejores para cuidarte a ti (y a tu bebé), productos que por sus características y su calidad cumplen  los requisitos para formar parte de la SHOP.

O﻿s dejamos el directo de Instagram donde hablamos del sexo tras la maternidad. como cambia, como podemos gestionar los cambios y como recuperar nuestra relacion de pareja en plena crianza. Os animamos a verlo porque os va a ayudar mucho!

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/tv/CPEFi6HK79G/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/tv/CPEFi6HK79G/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> Ver esta publicación en Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/tv/CPEFi6HK79G/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Una publicación compartida de Madre de adolescentes y niños (@madrescabreadas)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

*Photo by Etienne Girardet on Unsplash*