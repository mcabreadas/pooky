---
date: '2020-04-21T18:19:29+02:00'
image: /images/posts/ninos-calle/p-nina-ventana.jpg
layout: post
tags:
- familia
- mecabrea
- derechos
title: Niños a la calle a pasear, no al super
---

> Quien ha tomado la decisión no ha ido nunca con un niño al súper 

Esta frase se repite constantemente en las redes sociales desde que el Gobierno ha anunciado las medidas de "alivio” del confinamiento para los niños. El sentido común de las madres ha hablado. Y es que no conozco a ninguna que discrepe de esto. Da igual la orientación política, o incluso si no tiene. La reacción de las madres en cuanto a la protección de nuestra prole suelen ser universales, y por eso nos hemos unido en las redes sociales para exigir que se rectifique y dejen salir a los niños y niñas a la calle, pero a dar un paseo al aire libre y a tomar el sol (qué falta les hace la vitamina D), no a meterse en un supermercado, una farmacia o en un banco.

¡Lo que nos faltaba a las familias! Ya es bastante estresante que vaya uno de nosotros a la compra una vez a la semana con todas las medidas de protección y una precaución exquisita, para que tengamos que llevar al niño, si queremos que salga un rato, con para tener que estar con mil ojos para que no toque todo, lo que es bastante probable dada su curiosidad natural y su emoción por salir por fin y encontrarse en el pasillo de las galletas.


Bromas aparte, no imagino la cara de mi vecina que ya peina canas, cuando se encuentre en la cola con mis tres fieras y se le figuren tres virus gigantes amenazantes.

Tampoco imagino la cara de la cajera del súper cuando vea a los niños correr por los pasillos para coger carrerilla antes de tirarse al suelo para patinar con las rodillas (creo que mis hijos no son los únicos que en cuanto me descuido lo hacen).

Tampoco me imagino conteniéndolos en una larga cola para entrar a la farmacia o al banco sin que se pongan nerviosos y quieran correr o saltar.

Escapa totalmente a mi comprensión el hecho de que cómo un día nos digan que van a “aliviar” el confinamiento de los niños para que tomen el aire y el sol, y al día siguiente los meten en un supermercado... ahí... al caldo de cultivo del bicho. 

¿No será mejor, señores del Gobierno, que salgan al aire libre, aunque sea un paseo cortito, a que anden en lugares cerrados, donde pueden tocar cosas y acercarse a la gente? ¿No decíais que son el mayor vector de contagio? ¿En qué quedamos?

![niños de paseo](/images/posts/ninos-calle/ninos-paseo.jpg)

En serio, que nombren una comisión de madres y nosotras lo organizamos en un momento por barrios, calles o bloques, no os preocupéis. Pero déjenlos salir a pasear, por Dios! 

O si no, que sean ellos quienes les expliquen a mis fieras que no salen el lunes. Yo no tengo valor.



Por cierto, si te indigna tanto como a mí, busca el hashtag [#niñosdepaseo](https://twitter.com/hashtag/niñosdepaseo?src=hashtag_click&f=live) en las redes
sociales y apóyanos!!

Que nos oigan!





*Photo by Kevin Gent on Unsplash niños paseando*

*Photo by Harry cao on Unsplash niño en supermercado*

*Photo de portada by Sharon McCutcheon on Unsplash niña mirando por la ventana*