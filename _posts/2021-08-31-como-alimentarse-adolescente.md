---
layout: post
slug: como-alimentarse-adolescente
title: Los 4 hábitos que debes inculcar a tu adolescente para ayudarle a comer sano
date: 2021-09-30T07:55:31.136Z
image: /images/uploads/istockphoto-623207940-612x612.jpg
author: faby
tags:
  - adolescencia
  - nutricion
---
Durante la adolescencia se producen distintos cambios corporales. De hecho, desde los 12 a los 18 (etapa de desarrollo y adolescencia) los **requerimientos nutricionales cambian** y es crucial que se cuide y adapte la alimentacion de casa a esta nueva fasetan importante en la vida de nustos hijos e hijas.

Lejos de seguir los parámetros de la sociedad y lacomodidad de la comida rapida es necesario recibir los nutrientes necesarios, loque ayudara  a **reducir brotes de acné, obesidad, deficiencias vitamínicas** y otros problemas asociados a la etapa de desarrollo.

En esta entrada te hablaré de la importancia de la alimentación en la adolescencia y como asegurarnos de estar haciendolo bien con nuestros chicos y chicas.

## **Importancia de la alimentación en la adolescencia**

Los **adolescentes deben comer tres comidas diarias más la merienda.** Desde luego, la cantidad dependerá en gran medida de la edad, el sexo y constitución física.

La adolescencia es una **etapa de muchos cambios hormonales y físicos** en el que se da un aumento de masa corporal, masa ósea, por lo que el cuerpo necesita los nutrientes y energía adecuados para funcionar de forma correcta.

Si un joven en proceso de desarrollo no se alimenta de forma adecuada **pueden presentarse problemas de salud** como síndrome de ovario poliquístico, desnutrición, obesidad, enfermedades cardiovasculares, entre otras patologías, ademas de poder afectr a su salud en el futuro.

## **¿Qué nutrientes son indispensables en la adolescencia?**

Cuanto más temprano se implementen hábitos alimenticios saludables, mucho mejor. Por esa razón, no se debe permitir que la estresante época de estudio y otras [actividades extraescolares](https://madrescabreadas.com/2017/09/17/elegir-activiadades-extraescolares-según-la-edad/) fomente distorsiones.

![Estudiante que tiene un almuerzo saludable](/images/uploads/estudiante-que-tiene-un-almuerzo-saludable.jpg "Nutrientes son indispensables en la adolescencia")

De acuerdo a la opinión de los expertos en nutrición, durante la adolescencia se requiere **aumentar la ingesta de los siguientes nutrientes**:

* **Proteínas.** Son responsables del desarrollo de nuevos tejidos. Puede ser de origen animal o vegetal.
* **Vitaminas y minerales.** Aportan energía y tienen un papel importante en los procesos del metabolismo (sistemas enzimáticos). Están presentes en frutas, verduras y vegetales.
* **Calcio.** El [calcio](https://es.wikipedia.org/wiki/Calcio) es necesario para fortalecer los huesos. Está presente en la leche y sus derivados así como en los frutos secos.
* **Hierro.** Las chicas adolescentes requieren este mineral debido al cambio que supone la aparición de la menstruación.

## **Consejos para alimentarse bien durante la adolescencia**

La cantidad de alimentos dependerá de cada joven, sin embargo, hay ciertos parámetros nutricionales que vale la pena tomar en cuenta.

### **Variar la alimentación**

Una buena dieta debe incluir **verduras, legumbres, vegetales y proteínas.** Con relación a esta última, las de origen animal son las mejores, pero hay que tener cuidado y consumir pescado o carne blanca, ya que es más sano, por su menor aporte de grasa.

Es de suma importancia preparar alimentos que conserven los nutrientes y no aporten grasas calóricas o insaturadas. En este sentido, **cocinar al vapor es una excelente idea.**

Si nos metemos en Tik Tok, encontrareemos muchas recetas atractivas y saludables, que las recomiendan los propios influencers a quienes nuestros adolescentes siguen, y les pueden animar a cocinar ellos mismos.

![Cocinar al vapor](/images/uploads/russell-hobbs-cook-home-vaporera-3-recipientes.jpg "Russell Hobbs Cook@Home Vaporera - 3 Recipientes")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/Russell-Hobbs-19270-56-recipientes-temporizador/dp/B008Y6IN3S/ref=sr_1_7?__mk_es_ES=ÅMÅŽÕÑ&dchild=1&keywords=cocinar&qid=1630354718&sr=8-7&th=1&madrescabread-21) (enlace afiliado)

### **Desayunar bien**

El desayuno es la comida más importante. Se admiten jugos que aporten vitaminas. Los **smoothies son nutritivos** y si cuentas con un buen robot de cocina tu propio hijo podrá hacer deliciosos refrescos naturales.

![Máquina de smoothies](/images/uploads/russell-hobbs-mix-go-exprimidor-y-batidora-de-vaso-individual.jpg "Russell Hobbs Mix & Go - Exprimidor y Batidora de Vaso Individual")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/dp/B01N9UN8WS/ref=sspa_dk_detail_5?psc=1&pd_rd_i=B01N9UN8WS&pd_rd_w=HVTeV&pf_rd_p=444f018a-62d7-48b2-a88a-cea784dc658f&pd_rd_wg=TR49u&pf_rd_r=6JK08CVNM3PVMWNH6A69&pd_rd_r=9b1b4cfb-aa9d-4215-b824-6a59b13b76fc&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyUFZDNk9USzMxRTRMJmVuY3J5cHRlZElkPUEwNDM4NDQwMVYzSzAzNFFHSkdPRCZlbmNyeXB0ZWRBZElkPUEwNjIzNjYxMllLTVo3NExWTFZZVyZ3aWRnZXROYW1lPXNwX2RldGFpbCZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&madrescabread-21) (enlace afiliado)

### **Usa suplementos alimenticios**

En ocasiones, la vida acelerada de los adolescentes no les permite consumir la cantidad de vitaminas apropiadas. En este sentido, ciertos minerales como el calcio, el hierro, zinc, así como las vitaminas **contribuyen al crecimiento corporal.**

Pues bien, los suplementos en comprimidos (**multivitamínicos**) aportan micronutrientes y contrarrestan la **fatiga** y cansancio, y podemos usarlos no de forma habitual o como costrumbre, pero si quiza en epocas de mayor desgaste fisico o intelectual, como es la temporada de examenes.

![Ssuplementos alimenticios](/images/uploads/multicentrum-complemento-alimenticio-multivitaminas-con-13-vitaminas-y-11-minerales.jpg "Multicentrum Complemento Alimenticio Multivitaminas con 13 Vitaminas y 11 Minerales")

[![](/images/uploads/boton-comprar-amazon-120.png)](https://www.amazon.es/MULTICENTRUM-F000030605-90-Comprimidos/dp/B00W3RDLMC/ref=sr_1_2?__mk_es_ES=ÅMÅŽÕÑ&dchild=1&keywords=vitaminas&qid=1630355085&sr=8-2&madrescabread-21) (enlace afiliado)

### **Mantener horarios de las comidas**

**Saltarse las comidas** o incluso variar el horario puede causar distorsiones alimenticias, por eso, en la medida de lo posible hay que mantener un mismo horario. Además, es de suma importancia **comer despacio.**

Finalmente, los productos ultraprocesados, dulces entre otros, suelenser muy atractivos para los jovenes, pero no son alimentos recomendables en los adolescentes ni para un adulto. 

Te dejo nuestro [diccionario de expresiones adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) para que la comunicacion con tu hijo sea mas fluida.

*Photo Portada by demaerre on Istockphoto*

*Photo by ASIFE on Istockphoto*