---
layout: post
slug: picazón-en-el-cuerpo-durante-embarazo-colestasis
title: ¿Por qué me pica la barriga durante el embarazo?
date: 2023-03-24T09:58:11.767Z
image: /images/uploads/mujer-embarazada-tiene-dolor-estomago_99043-998.jpg
author: .
tags:
  - embarazo
---
Es común sentir una leve picazón durante el embarazo. También es normal sentir picazón en la piel del abdomen, a medida que este se estira.

Gran parte de la picazón que se siente durante el embarazo es normal, pero, si esta es grave, entonces pudiese ser signo de una condición que requiere atención, como la colestasis intrahepática o colestasis obstetricia.

En este caso, se debe acudir con el médico, quien preguntará por los síntomas, revisará la historia clínica, realizará un examen físico y solicite unas pruebas de laboratorio para verificar si el hígado está funcionando correctamente.

## Las colestasis del embarazo

Se trata de una alteración del hígado en la cual la secreción de bilis se inhibe y hace que las toxinas estén más tiempo en el hígado.

En este caso, la mujer siente una picazón fuerte en todo el cuerpo, o en las manos y pies, puede ser peor de noche y no se observa erupción en la piel. También puede presentarse con heces de color gris, orina oscura o color amarillento en la piel y en la parte blanca de los ojos –ictericia-.

Para tratar la colestasis del embarazo hay **cremas para la comezón,** pero es necesario que el médico conozca de la condición, ya que puede representar un riesgo para el bebé.

Entre los factores que pueden incrementar la probabilidad de padecer colestasis en el embrazo se encuentran los antecedentes personales o familiares de esta condición, daño previo o enfermedad hepática, así como el embarazo de mellizos o múltiple.

## Estoy embarazada y me pica la barriga, ¿qué hago?

Durante el embarazo, la piel del abdomen se estira, esto provoca picazón debido a la sequedad.

También, se puede tratar de [eccema](https://www.topdoctors.es/diccionario-medico/eccema), la cual es una enfermedad de la piel en la que hay picor intenso y manchas rojas. La presencia hormonal pudiese empeorar estas condiciones.

Para tratarlo se recomienda hidratar siempre la piel, usando cremas hidratantes neutras, beber mucho líquido y con una buena alimentación. Se sugiere evitar las duchas con agua muy caliente, usar jabones suaves y secarse cuidadosamente.

Asimismo, se debe evitar la exposición directa al sol y se sugiere vestir con ropa holgada y fresca, de algodón.

## Tratamiento de la colestasis

En cuanto a la colestasis, el tratamiento se enfoca en aliviar los síntomas y prevenir complicaciones.  

No existe una forma de prevenir la aparición de colestasis en sí, simplemente puede suceder, por lo que la comunicación con el médico es importante durante toda la gestación.

El médico podría hacer pruebas o [ecografias](https://madrescabreadas.com/2021/05/25/ecografia-4-semanas-no-se-ve-nada/) para verificar la frecuencia cardíaca del bebé y sus movimientos, otras pruebas para comprobar la cantidad de líquido amniótico, el tono muscular, la respiración, entre otros. Por último, la condición también pudiese llevar a un trabajo temprano de parto, pues, aunque todas las pruebas arrojen resultados positivos, el médico podría considerar en algún momento que lo mejor es la inducción temprana para disminuir el riesgo de muerte fetal.

La colestasis no es riesgosa para la madre, pero sí para el bebé, a causa de sufrimiento fetal, interrupción del embarazo o parto prematuro.

En el parto, las complicaciones también aumentan, ya que hay posibilidad de hemorragia. En embarazos futuros se puede presentar nuevamente. Pero, después del embarazo, no tiene consecuencias en la salud ni en el bebé y el hígado vuelve a su funcionamiento normal.