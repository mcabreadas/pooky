---
layout: post
slug: podcast-paula-minana-escritora
title: Podcast cabreadas con el sistema educativo
date: 2022-11-21T12:52:34.289Z
image: /images/uploads/6.png
author: .
tags:
  - podcast
---
Hablamos con Paula Miñana, miembro histórico la la comunidad de Madres Cabreadas, madre de familia numerosa, maestra y escritora.

## Parte I

Nos cuenta su secreto para llegar a todo, y además hacerlo bien. 

Nos cuenta cómo pasó de escribir retazos de historias en Instagram a, animada por sus seguidoras,  autopublicar una novela, y su posterior fichaje por Espasa.

También hablamos de su despido estando embarazada, su posterior [moobing laboral](https://es.wikipedia.org/wiki/Acoso_laboral), y cómo logró superar este bache.

Cómo estudió magisterio y tuvo a sus tres hijos a la vez.

Cómo logró estudiar oposiciones a la vez que criaba y trabajaba, y obtener plaza.

Sigueme en Spotify para no perderte ningun episodio:

Finalmente, y ya en su faceta de [maestra](https://madrescabreadas.com/2021/06/21/regalos-para-el-mejor-profesor/), hacemos una radiografía del sistema educativo español desde el punto de vista de una maestra y de una madre.

Paula es un ejemplo de que lo que se quiere conseguir se puede si una se empeña y lucha por ello.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1aDgIEQCJGNBMcFdEAZn9r?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

## Parte II

Paula hace una crítica al sistema educativo actual.

**Altas capacidades**: diagnóstico y trabas burocráticas en los plazos

Falta de medios

**El sistema educativo ideal de Paula contaría con:**

\-Tiempo y recursos

\-Contar con los profesionales expertos en la materia para hacer la Ley (los currículos, programaciones...) Ejemplo: no estudiar la historia de manera parcelada, no incluir conceptos abstractos a edades tempranas porque no tienen madurez, potenciar la creatividad

\-Que el centro sean los niños y niñas.

\-Escuelas en espacios abierto.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/5yy8q02aL1v91JGZLbZQkF?utm_source=generator&theme=0" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Recomendaciones de este episodio:

[Nosotros en singular se dice tú y yo](https://www.amazon.es/dp/8467056584?tag=onamzmadresca-21&linkCode=ssc&creativeASIN=8467056584&asc_item-id=amzn1.ideas.3KR7V3D54525M&ref_=aip_sf_list_spv_ons_mixed_d_asin) (enlace afiliado)

[Si sentara la cabeza pensaría con el culo](https://www.amazon.es/dp/8467057742?tag=onamzmadresca-21&linkCode=ssc&creativeASIN=8467057742&asc_item-id=amzn1.ideas.3KR7V3D54525M&ref_=aip_sf_list_spv_ons_mixed_d_asin) (enlace afiliado)

[Mi Teta Derecha](https://www.amazon.es/dp/1089988451?tag=onamzmadresca-21&linkCode=ssc&creativeASIN=1089988451&asc_item-id=amzn1.ideas.3KR7V3D54525M&ref_=aip_sf_list_spv_ons_mixed_d_asin) (enlace afiliado)

A quién seguir:

[Paula Miñana](https://www.instagram.com/paulamgram/)

[Maestra de Pueblo](https://twitter.com/maestradepueblo)

[Madres Cabreadas](https://www.instagram.com/madrescabreadas/)