---
author: maria
date: '2020-09-16T10:10:29+02:00'
image: /images/posts/podcast/p-metropolis.jpg
layout: post
tags:
- conciliacion
- revistadeprensa
title: Entrevista Metrópolis FM sobre conciliación en tiempos de pandemia
---

Un placer haber compartido una interesante charla sobre conciliación, teletrabajo y otros aspectos relativos a la maternidad, la familia y sus formas de compaginarla con el trabajo en Radio Social Corporativa de Metrópolis FM.

<a href="https://metropolisfm.es/podcast/conciliacion-con-madres-cabreadas-y-angelica-pajares/m" class='c-btn c-btn--active c-btn--small'>Escuchar el podcast</a>

Tratamos la vuelta al cole con mucha incertidumbre y dudas sobre los protocolos gubernamentales Y la posibilidad de súper contagios entre los escolares, y consecuentemente en sus hogares.

Analizamos por qué la conciliación nunca es una prioridad en la agenda política. 

Reflexionamos sobre el rol que han tenido los diferentes sexos y como han evolucionado en los últimos años.

En la segunda parte del programa tengo la oportunidad de hablar con Angélica Pajares, Talent Manager & CSR Leader en PRAMAC IBERICA, Experta en Responsabilidad Social Corporativa y madre de familia numerosa. Una madre muy volcada en la conciliación y que sea una realidad en la empresa en la que trabaja. Angélica nos contó su experiencia y cómo se organiza para sacar adelante un hogar a la vez que ostenta un puesto de responsabilidad en su empresa.

Muchas gracias por invitarme a hablar sobre conciliación en tiempos del COVID-19.

Si te ha gustado comparte y suscríbete al blog para no perderte nada.