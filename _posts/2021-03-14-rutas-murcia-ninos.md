---
author: maria
date: '2021-03-14T10:10:29+02:00'
image: /images/pages/p-cenizas.jpg
layout: post
tags:
- planninos
title: Rutas para hacer en familia por la Región de Murcia
---

El ocio familiar se ha tenido que adaptar a esta etapa de pandemia donde este al aire libre para la forma más saludable de divertirse y compartir tiempo con os que más queremos. El cierre perimetral de algunas Comunidades Autónomas, incluso municipios, ha hecho que redescubramos lugares que siempre habíamos tenido cerca, pero quizá nunca nos habíamos parado a disfrutar de ellos.

A nosotros nos ha pasado en nuestra maravillosa Región de Murcia, que nos está regalando grandes domingos en los que cogemos la mochila y nos vamos de ruta a descubrir nuevos parajes y vivir aventuras con los niños.

Os dejo algunas de las rutas por la Región de Murcia que hemos hecho y que más hemos disfrutado:

<a href="https://foro.madrescabreadas.com/t/rutas-por-murcia" class='c-btn c-btn--active c-btn--small'>Ver las mejores rutas por la Región de Murcia</a>