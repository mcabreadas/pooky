---
date: '2019-10-23T13:19:29+02:00'
image: /images/posts/encuentro-madres-cabreadas/p-banner-evento-mc.jpg
layout: post
tags:
- local
- eventos
title: Apúntate al I Encuentro Madres Cabreadas
---

## [Compra aquí tu entrada I Encuentro Madres Cabreadas](https://eventomadrescabreadas.eventbrite.es)

Muchas de vosotras me conocéis desde mis inicios en Internet, hace ya 9 años, y sabéis que al principio iba de incógnito, pues hablaba de cosas personales; de seas cosas que se sienten cuando eres madre y estás criando y le cuentas a una amiga. Quizá por eso me resultaba más fácil hacerlo desde el anonimato.

Así era yo cuando comencé:


![alto o mi madre dispara](/images/posts/encuentro-madres-cabreadas/avatar-madres-cabreadas.jpg)

Pero poco a poco me fui abriendo más y descubrí que eso me traía más cosas buenas que malas porque lograba conectar mejor con vosotras y me aportábais muchísimo cada día.

## De una protesta a un blog de familia
Mi blog también fue evolucionando y pasó de ser una protesta social por una conciliación real del trabajo y la familia, lo que me hacía pasarme el tiempo cabreada, a convertirse en una bitácora de experiencias en ambas direcciones sobre crianza, lactancia, educación, familia, incluso moda y recetas en casi 600 artículos hechos desde el corazón y la mayoría en mis horas de sueño.

Hasta que hemos llegado a un punto en que esta comunidad de madres cabreadas necesita conocerse en persona! A muchas de vosotras os considero amigas, y a veces no logro recordar si os conozco en persona o no porque ya formáis parte de mi vida. 

## Somos una comunidad diversa

Me encanta la comunidad que formamos porque somos muy diferentes unas de otras, con circunstancias muy distintas, ideas y creencias diferentes... ni os lo imagináis, pero tenemos en común lo más importante, lo que une a todas las madres del mundo sean de donde sean: el amor por nuestros hijos. Acaso hay algo más universal?

## ¿Qué os parece que nos conozcamos y pasemos una mañana de convivencia haciendo cosas chulas?

Sé que sois de todos los puntos de España, pero he querido ubicar este primer encuentro en mi ciudad, Murcia.

![ponentes-evento-madres-cabreadas](/images/posts/encuentro-madres-cabreadas/ponentes-evento-mc.png)

El encuentro tendrá lugar el 9 de noviembre de 2019 en Murcia y tiene como objetivo la desvirtualización de la comunidad en una mañana divertida y relajante, que consistirá en una sesión de yoga en grupo al aire libre,a cargo de la profesora Petra Caldenius ([@mangapetra](https://www.instagram.com/mangapetra/)), una charla coloquio sobre herramientas jurídicas para conciliar a cargo de una servidora, que también es abogada, un  taller de maquillaje express para madres con prisa mañanera a cargo de la maquilladora Julia Sánchez Manzano ([@giiuliia_sm](https://www.instagram.com/giiuliia_sm/)) y la influencer Silvia Barba ([@unafamiliaalamoda](https://www.instagram.com/unafamiliaalamoda/)),y un delicioso "brunch descabreante" en el Restaurante Godis Dulce Poder.


![programa encuentro madres cabreadas](/images/posts/encuentro-madres-cabreadas/programa-encuentro-madres-cabreadas.jpg)

## Escritoras y artesanas murcianas
Además las autoras murcianas, y también madres, Paula Miñana  [@paulagram](https://www.instagram.com/paulamgram/) (Nosotros en Singular se dice tú y yo) y Paqui Vivancos [@paqivivancos](https://www.instagram.com/paquivivancos/) (Cuentos de Myrtea) sortearán un ejemplar de su último libro.
Habrá sorteos de varias creaciones de las artesanas murcianas Margarita Pérez Plaza [@margus.es](https://www.instagram.com/margus.es/) de la firma Margus Handmade y Ana Alcaraz [@kihanadecora](https://www.instagram.com/kihanadecora/), de Kihana Decora, photocall y sorpresas para pasar una mañana divertida y recargar pilas.

## Entradas a la venta

El encuentro será de 10:00 a 14:00 h y tendrá lugar el 9 de noviembre de 2019 en el Restaurante Godis Dulce Poder [@godisdulcepoder](https://www.instagram.com/godisdulcepoder/), en Gran Vía Alfonso X el Sabio, 14, Murcia. La sesión de yoga será en el "Parque de los Perros", junto al restaurante.

Las entradas ya están a la venta con un precio de 11 €, y se pueden comprar aquí:

[Compra tu entrada online I Encuentro Madres Cabreadas](https://eventomadrescabreadas.eventbrite.es/). 

Aforo limitado!

Nos vemos?