---
date: '2019-12-20T01:00:29+02:00'
image: /images/posts/sudio/p-chica-bailando-auriculares.jpg
layout: post
tags:
- adolescencia
- trucos
- recomendaciones
- familia
- salud
- crianza
title: Auriculares y adolescentes. Consejos de uso.
---

Esto no es algo nuevo propio de los adolescentes de ahora. Nosotras también íbamos con nuestros auriculares escuchando música por la calle y nos encantaba tumbarnos en nuestro cuarto a escucharla tranquilamente para aislarnos del mundo. 

Esto es típico de esa edad en la que necesitan momentos de soledad tanto como momentos con sus amigos, y los padres y madres debemos respetarlo. Por eso los cascos siempre han sido un elemento muy ligado a la adolescencia, y es curioso que los modelos retro vuelvan a estar de moda (si lo llego a saber, hubiera guardado los míos, incluso mi casete).

![adolescente con casete](/images/posts/sudio/cassete.jpg)

## Elige auriculares de calidad
Pero esto no significa que los padres y madres no debamos conocer los riesgos que entrañan para la salud auditiva de nuestros hijos el uso de dispositivos de baja calidad o un uso inadecuado de los mismos.

En el mercado hay muchas marcas y modelos, pero debemos esforzarnos por buscar la calidad por encima de cualquier otro criterio, por muy baratos o bonitos que sean.

Yo, hace años que uso los [Sudio](https://www.sudio.com/es/) par mi trabajo, como ya os conté en este [post sobre auriculares cuquis](https://madrescabreadas.com/2017/07/09/auriculares-fashion-bluetooth/), y os puedo decir que merecen mi total confianza porque tienen varios adaptadores de diferentes tamaños para distintos tipos de oido, lo que hace que te encajen a la perfección logrando un mayor aislamiento del ruido exterior , lo que me permite poder escuchar de forma óptima con un menor volumen, y por su diseño tipo joya, que me permite llevarlos en reuniones y conferencias de forma discreta y elegante.

![auriculares de casco sudio](/images/posts/sudio/sudio.jpeg)

Por eso he elegido esta marca para regalar a mi hija esta Navidad, pero para ella he elegido el modelo [Regent II White](https://www.sudio.com/es/regent2-white) tipo casco para proteger aún más sus oídos, ya que el aislamiento que se logra es mayor con este formato y necesitará aún menos volumen para escuchar su música o videos favoritos, y además el acolchado es súper dulce y confortable. Además es adaptable a todos los tañamos de cabeza, así que si algún día se los pido prestados, me servirán a mí también.

Este regalo irá acompañado de unos consejos que comparto aquí para su correcta utilización, y así evitar futuros problemas de audición.

Si os parece buena idea para regalar, os dejo un CÓDIGO DESCUENTO DEL 15% para la web de Sudio: 

> **CÓDIGO DESCUENTO [auriculares Sudio](https://www.sudio.com/eu/) 15%: MCH15**

## Regla 60/60 para un uso correcto de los de auriculares
Esta regla consiste en que el volumen no debe superar el 60% del volumen máximo, y la exposición no deberá superar 1 hora al día. Siendo necesarios descansos, caso de ser necesario usarlos durante más tiempo, y lo ideal sería no superar los 65 decibelios, aunque esto resulta algo utópico en ciudades como Barcelona, en las que el ruido suele rondar los 70 decibelios. 

![adolescente con auriculares por la calle](/images/posts/sudio/cascos-calle.jpg)

Para controlar los decibelios existen Apps para [iOS](https://apps.apple.com/es/app/decibel-10th-medidor-sonido/id448155923) y para [Android](https://play.google.com/store/apps/details?id=kr.sira.sound&hl=en).

De todos modos, un método sencillo y eficaz es preguntar al de al lado si escucha la música, y si es así, tendré que bajar el volumen hasta que se deje de escuchar desde fuera.

## ¿Auriculares de tapón o cascos?
Para los adolescentes, que suelen usarlos por la calle, y además tienden a abusar del tiempo de uso, yo me inclino por los cascos, ya que aislan mejor el ruido exterior y permiten escuchar correctamente a un menor volumen, además, pueden usarlos también sin escuchar música simplemente para aislar el ruido de la casa cuando tienen que concentrarse mucho para estudiar un examen, por ejemplo. De hecho, en oficinas donde trabajan muchas personas suelen darles ese uso más aislante que lúdico.

Espero haberte ayudado con estos consejos para proteger los oídos de tus hijos.
Si te han parecido útiles comparte, quizá ayuda a otros!






- *Photo  casete by Eric Nopanen on Unsplash*
- *Photo chico por la calle by Ross Sneddon on Unsplash*