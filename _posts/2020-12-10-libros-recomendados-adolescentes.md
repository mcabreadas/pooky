---
date: '2020-12-10T18:19:29+02:00'
image: /images/posts/libros-recomendados-adolescentes/p-portaaviones.jpg
layout: post
tags:
- planninos
- adolescencia
- invitado
- libros
title: A mí no me gusta leer
---

> 
> Hoy en ["Merendando con..."](https://madrescabreadas.com/merendando-con) merendamos con Graziela Arias, una madre que consiguió que su hijo re descubriera su amor por la lectura escribiéndole ella misma un libro para que le gustase (porque quién mejor que una madre para saber qué le gusta a su hijo...). En casa ya leímos el "El misterioso asesinato de Pico Len" ¡y nos encantó!, además, también sorteamos en nuestro Instagram dos ejemplares que disfrutásteis dos de vosotras, y me consta que también os encantó.
> 
> Así que os animo a comprarlo como regalo de Navidad, porque va a ser un acierto seguro.
> 
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8409187477&linkId=3a20f4d1ada848feed1b15562ccf5667&bc1=ffffff&lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}
> 
> ¡Os dejo con nuestra invitada!

Antes de nada me presento. Soy Graziela Arias, madre de un niño y autora de la novela juvenil [“El misterioso asesinato de Pico Len”. Libro pensado para niñ@s de entre 9 y 13 años. Autopublicado en Amazon](https://amzn.to/2VXsXA6) (enlace afiliado).  Estoy muy feliz por la acogida que ha tenido, de verdad que nunca lo habría imaginado. Y a parte de feliz, muy agradecida.


![mujer con sombrero en blanco y negro](/images/posts/libros-recomendados-adolescentes/sombrero.jpg)


## Por qué me decidí a escribir un libro

Estoy aquí para contaros los motivos que me llevaron a escribir este libro.

Todo comenzó cuando escuché pronunciar de los labios de mi hijo la famosa frase: 

> Es que a mí no me gusta leer

Os explico: hasta ese momento, todas las noches, antes de acostarnos, yo iba a su habitación y le leía en voz alta. Una noche tras otra, y así infinidad de libros. Era el momento más mágico del día para los dos. Dejábamos apartadas nuestras vidas, para meternos en la piel de otros personajes y viajar juntos a otros mundos.

El problema vino cuando le pedí que lo hiciera él solo. Ya no era lo mismo, él tenía que poner de su parte para leer y crear ese mundo que antes construíamos juntos, y eso no le parecía tan divertido y cómodo.

Lo que ocurría es que los libros de la temática que a él le gustaban, eran demasiado extensos para él y los que gráficamente le atraían , la temática era excesivamente infantil.

## La edad crítica ante la lectura

Detecté entonces, que había una edad en que los niñ@s no tenían muchas opciones: o libros muy infantiles o libros muy interesantes pero físicamente demasiado arduos por ser excesivamente extensos y sin apenas ilustraciones.

Así que ni corta ni perezosa, cogí una libreta y empecé a escribir lo que en principio iba a ser un pequeño cuento. Escogí una temática que pensé que le gustaría a mi hijo, una aventura maravillosa basada en un hecho real, donde mezclaría datos familiares (el abuelo Luis existe y el comandate Carlos Len también), un poquito de historia (tratamos la Primera Guerra Mundial ni más ni menos) y amistad (una pandilla maravillosa de amigos entrañables y auténticos). 

## Lo que empezó siendo un cuento, se convirtió en un libro de más de doscientas páginas

Llegados a este punto y viendo la reacción que había causado en mi hijo, decidí publicarlo. Pensé que había cientos de niñ@s en la misma situación que el mío y este libro podría ayudarles a adentrarse en el maravilloso  mundo de la lectura.


![mujer leyendo en sillón de rayas](/images/posts/libros-recomendados-adolescentes/leyendo.jpg)

Es indescriptible la sensación que se siente cuando te das cuenta de que SÍ te gusta leer y de que hay millones y millones y millones de libros esperándote. Aún recuerdo la cara de mi hijo cuando vino con el libro en la mano y me dijo: 

> Mamá, creo que me encanta leer – con una sonrisa de oreja a oreja.

Sobra decir, que en ese momento, me sentí la madre más feliz del mundo.

## A todo el mundo le gusta leer

Hay miles de libros ahí fuera esperándonos. Que haya temáticas o géneros que no nos atraigan, no significa que no nos guste leer. A todo el mundo le tiene que gustar leer,  sólo hay que descubrir sobre qué.

No quiero entrar en temas científico aunque podría, ya que cómo sabéis esta demostrado que la lectura nos hace mas inteligentes: favorece el desarrollo de distintas áreas del cerebro, ayuda a mejora la empatía, agudiza la memoria, ayuda a gestionar las emociones, mejora el vocabulario y la habilidad verbal, hace que la mente esté activa y sana, nos ayuda a aprender, desarrolla la capacidad de reflexión, aumenta el bagaje cultural, fomenta el esfuerzo, favorece el desarrollo de la observación y la concentración… 


![portada libro el misterioso asesinato de pico len](/images/posts/libros-recomendados-adolescentes/portada-libro.jpg)

Y así podría rellenar carillas y carillas de folios, pero no lo haré, porque para mí por encima de todo esto, la lectura es otra cosa.

Para mí la lectura es: aprender a conocernos a nosotros mismo, otros mundos, otras personas, otras situaciones, es dudar, temer, amar, imaginar, engrandecer, explorar, crecer, reir, llorar, admirar, juzgar, examinar, entender, descubrir, reaccionar, luchar, sopesar, cuestionar, creer, anteponer, explorar, odiar, renacer, sentir, soñar…VIVIR

Por favor, animad a vuestros hijos a leer, es de las mejores cosas que podéis hacer por ellos ¿no es tentador vivir mil vidas en una vida? A mí me parece apasionante.



Firmado: una que de pequeña también dijo: “A mi no me gusta leer”


*Si te ha gustado comparte y suscríbete al blog para no perderte nuestro contenido.*