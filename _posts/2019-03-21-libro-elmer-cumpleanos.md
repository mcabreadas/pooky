---
date: '2019-03-21T18:19:29+02:00'
image: /images/posts/libro-elmer-cumpleanos/p_libro-elmer-cumpleanos.jpg
layout: post
tags:
- planninos
- libros
title: Nos iniciamos en la lectura con Elmer, el elefante multicolor
tumblr_url: https://madrescabreadas.com/post/183606146714/libro-elmer-cumpleanos
---

Mi hijo pequeño de 6 años está empezando a leer libros infantiles, y a veces se cansa y nos preocupaba que no cogiera un buen hábito de lectura, por eso pregunté a mi comunidad en las redes sociales (la sabiduría de las madres es la mejor) por consejos para estimularlo y motivarlo, ya que creo que esta etapa del desarrollo es crucial para que o se enganchen o la odien para siempre. 

## Cómo enganchar a los niños en la lectura

Uno de los consejos más repetido, aparte de llevarlo a la biblioteca, hacerle un [rincón de lectura infantil en casa](https://madrescabreadas.com/post/176183695464/nuevo-cat%C3%A1logo-ikea-2019-rinc%C3%B3n-de-lectura), darle ejemplo leyendo nosotros, leer con él... fue que buscáramos una colección o temática que le gustara mucho y con la que disfrutara, así que cuando el primer libro de Elmer cayó en sus manos, casi por casualidad porque lo encontró en un cajón en la casa de su abuela, fue una gran suerte, porque enseguida se enganchó con este elefante multicolor que es todo optimismo, solidaridad, amistad, respeto, tolerancia y empatía.

## Quién es Elmer

Todo el mundo sabe que los elefantes son grandes y grises. ¡Pero Elmer es totalmente distinto al resto de su manada! Su personalidad alegre se refleja en su exterior: está formado por un mosaico de colores brillantes y es precisamente esta característica la que le hace único.

Pero mi pequeño también es muy fan de su primo Wilbur, que es ventrílocuo y le encanta gastar bromas escondiéndose e imitando las voces del resto de animales, y su abuelo Eldo, personaje por el que siente especial cariño.

Tanto le gusta Elmer que ha vencido la pereza por el esfuerzo que le supone los comienzos en la lectura, y cada noche me pide que lo leamos, hasta le pidió a su abuela otro libro más de la colección, ya que le encanta leer con ella.
![](/images/posts/libro-elmer-cumpleanos/tumblr_inline_popqhjafyO1qfhdaz_540.jpg)

## El elefante Elmer cumple 30 años

Pero no os creáis que Elmer es ningún jovencito, sino que ahí donde lo veis, este personaje creado por David McKee va a cumplir 30 años Por eso, y para conmemorar un número tan redondo, [Beascoa](https://amzn.to/3iaxefF) (enlace afiliado) ha publicado publicado, el libro El gran día de Elmer. Una historia que cuenta cómo, un año más, llega el día del cumpleaños de Elmer y todos los elefantes comienzan con los preparativos sin darse cuenta de que están haciendo mucho ruido y molestan al resto de animales de la selva. Elmer decide cambiar las normas e invitar a todos los animales a unirse a su desfile, no solo a los elefantes. Además, el libro incluye actividades para que los niños hagan unos ejercicios de comprensión lectora e interactúen con las escenas del cuento.
    
{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8448851897&linkId=cea0b8ab6619b479b3847db0a8fbe90b&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}


Elmer es tan famoso que se ha convertido en todo un icono y ya ha vendido más de 300.000 ejemplares en España y 10 millones en todo el mundo. La relación que tiene David McKee con él es muy especial y tras- pasa la barrera del papel, según afirma él mismo «Elmer es muy importante para mí, él me cuenta sus aventuras y yo las escribo». Pero, además, Sus libros se han traducido a más de 50 idiomas y se han publicado 26 historias.

## ¿Celebramos el cumpleaños de Elmer?

Así que el sábado 25 de mayo es el día Elmer y soplará las 30 velas de su colorida tarta. Durante toda la mañana tendrá lugar una celebración en [Casa del Libro Rambla Barcelona](https://www.casadellibro.com) a la que está todo el mundo invitado. Habrá cuentacuentos, pintacaras y varias actividades para que los más pequeños conozcan a Elmer.

Además, en el resto de ciudades habrá también actividades de celebración de cumpleaños, y quién sabe si, como en el libro, una cabalgata de elefantes...

Feliz cumpleaños, Elmer!!!
![](/images/posts/libro-elmer-cumpleanos/tumblr_inline_popqaem3YV1qfhdaz_540.jpg)

Nota: La editorial Penguin Random me ha cedido un ejemplar de El Gran Día de Elmer para reseñar.