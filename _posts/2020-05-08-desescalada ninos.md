---
date: '2020-05-08T18:19:29+02:00'
image: /images/posts/desescalada-ninos/p-top.jpg
layout: post
tags:
- familia
- vlog
title: Mi hijo no quiere salir a la calle
---

El lunes pasado me estrené como colaboradora del programa [Quédate Conmigo de la 7 Televisión Región de Murcia conducido por Encarna Talavera](http://webtv.7tvregiondemurcia.es/informativos/quedate-conmigo-en-la7/2020/lunes-4-de-mayo/). Aún confinada, mi intervención tuvo lugar desde casa por video llamada y hablamos de la salida de los niños y niñas a la calle y las dificultades que están teniendo algunos pequeños que sufren miedos o falta de motivación para salir de sus casas ante la prohibición de hacer uso de los parques infantiles o de jugar con sus amigos.

Basándome en vuestras opiniones a través de las redes sociales y en la entrevista que realicé en el directo de Instagram con la [psicóloga clínica y educativa  Carmen Pérez Saussol](http://www.ciocentropsicologia.com/sobre-nosotros/), di una serie de tips que quizá os puedan ayudar para afrontar esta desescalada con vuestros hijos.

Aquí tenéis el video con mi intervención en el programa de televisión Quédate Conmigo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/j9AEsfh9O-w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Y aquí os dejo enlazado el directo de Instagram con la psicóloga Carme Pérez Saussol para quienes no pudisteis conectaros en tiempo real, donde recopilé todas las preguntas que me fuisteis dejando para ella, y contestó también las que le hicísteis en directo, como por ejemplo:
 

-¿Estás teniendo ahora más consultas con motivo del confinamiento?

-En general ¿cómo piensas que han tomado los niños esta situación ?

-¿Y la vuelta a las calles? ¿Ha generado algún problema en los niños?

-Consejos sobre cómo debemos plantear a los niños la salida y todas las normas que tienen que cumplir

-Niños que no quieren salir. ¿Hay que forzarlos?

-Niños que les cuesta conciliar el sueño, que no quieren irse a la cama, que se despiertan con pesadillas. ¿Cómo actuar?  

-¿cómo lo planteamos cuando tenemos hijos que pueden salir e hijos que no (por ser mayores de 14 o porque están malitos)?

-Las mías ayer salieron vigilantes y obsesionadas con que las regañasen.

-Los míos tampoco cuando les he dicho que al volver se tenían que duchar.

-La mía mayor ha dicho que si no se puede ir al parque ni quedar con nadie, pasa.

-Mi hijo sí ha querido salir, pero a los cinco minutos de estar allí ya ha querido volver

-Nos han hecho sentir que en casa estamos seguros, a tal punto, que, inconscientemente, ahora da miedo salir...

A todo esto y más respondió la psicóloga. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/fS1oeBzpUbE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Esta situación es nueva para todos, y ahora más que nunca las madres debemos apoyarnos y compartir nuestras vivencias, que sin duda nos enriquecen a todas. Así que comparte para ayudar a más familias y suscríbete al blog para no perderte más cosas interesantes!

Comparte y suscríbete, así me ayudarás a seguir escribiendo este blog!