---
author: maria
date: '2021-03-03T10:13:29+02:00'
image: /images/posts/vlog/p-enfermedades-raras.png
layout: post
tags:
- vlog
- salud
- crianza
title: Vlog en el Día de las Enfermedades Raras conocemos a Selena
---

Esta semana hablo con Encarna Talavera con motivo del día de las enfermedades raras, que se celebra 28 de febrero, sobre Selena, una niña única, que nos ha permitido a través de su cuenta de Instagram [@elviajedeselena](https://www.instagram.com/elviajedeselena/), creada por su madre Tatiana Pérez Cortés, comprender un poco más este tipo de enfermedades, el día a día de las familias, la complejidad de vivir en la incertidumbre al no haber suficiente investigación y tratamientos y, sobre todo, aprender del ejemplo Tatiana, una mujer admirable cuyo único propósito en la vida es que su hija sea feliz.


<iframe width="560" height="315" src="https://www.youtube.com/embed/0AzeXSXgyhQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Comparte para que llegue a mucha gente y suscríbete para no perderte nada!