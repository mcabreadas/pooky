---
layout: post
slug: madres-por-el-clima-contra-el-cambio-climático
title: Madres por el clima contra el cambio climático
date: 2021-04-19 07:51:44.814000
image: /images/uploads/madres_clima.jpg
author: maria
tags:
  - invitado
---
> Hoy merendamos con [Madres por el Clima](https://madresporelclima.org/), más concretamente con Lolita Soubrier, madre de familia numerosa, mujer comprometida con el medio ambiente, que saca horas de debajo de las piedras para aportar su granito de arena en la lucha para salvar el planeta junto con otras madres de toda España, cuyo objetivo es luchar contra las consecuencias del cambio climático para que los niños tengan vivan en un lugar mejor. Porque al fin y al cabo, lo que no hagamos las madres muchas veces se queda sin hacer, así que con este empuje nos presenta un joven pero prometedor movimiento, que ya ha logrado empezar a cambiar cosas y que seguro dará mucho que hablar.
>
> Prepárate un té , un chocolate o un café, que vamos a conocer la labor y los logros de estas Madres por el Clima. Os dejo con ellas:

## Orígenes de Madres por el clima

El movimiento de [Madres por el Clima](https://madrescabreadas.com/2023/02/17/cambio-climatico-para-ninos-actividades/) nació de un grupo de madres que quisimos pasar a la acción ante la crisis climática, uniéndonos a las reivindicaciones de los jóvenes que luchan contra el cambio climático y en defensa del medio ambiente. Ellos luchan por su futuro y nosotras nos vemos en la necesidad de luchar por el futuro de nuestros hijos e hijas, porque el cambio climático no es algo que va a suceder, sino que ya está aquí.

Cada vez se producen más incendios, más olas de calor, las DANA son más destructivas… Estamos generando un daño en el planeta, que produce la reducción de la biodiversidad y que incrementa las posibilidades de que aparezcan más pandemias.

Ante esta situación, decidimos organizarnos para exigir un entorno saludable, que redundará en la salud de la sociedad.

La iniciativa de Madres por el Clima nació en 2019, primero a nivel nacional. Meses después empiezan a surgir los nodos regionales, entre ellos el de Región de Murcia, que precisamente es de las zonas más vulnerables al cambio climático.

Aunque por nuestros orígenes nos llamemos madres, en nuestro grupo tiene cabida todo el mundo, padres, tías/os, abuelas/os e incluso personas que no tienen hijos, que son nuestras madrinas y padrinos por el clima.

Somos diversas, constructivas y tratamos de no juzgar y aportar soluciones, colaborando con los demás colectivos que trabajan en defensa del medio ambiente, ya que contra el cambio climático tenemos que sumar todos y todas: la ciudadanía, los políticos de todo signo, las instituciones, los colectivos…

![bolsa de tela reutilizable](/images/uploads/bolsa.jpg)

## Objetivos de Madres por el Clima

Nuestros objetivos son visibilizar la situación de emergencia climática y exigir respeto y protección para el planeta, asegurando el futuro y la salud de nuestros hijos e hijas, reclamando una sociedad sostenible y respetuosa con el medio ambiente.

Para ello, **reclamamos** la protección del aire, del agua y de los espacios naturales, una economía sostenible, social y circular, la inversión en energía solar y **fomentamos** la educación ambiental en general, no solo a los niños y niñas, sino a nuestras familias, en el trabajo y, en general a nuestro entorno, el consumo responsable y una movilidad y alimentación saludables y sostenibles.

Creemos firmemente que todas las políticas y actuaciones públicas deben tener en cuenta la sostenibilidad e implantar soluciones basadas en la naturaleza, que es lo que a la larga nos va a asegurar una adaptación y mitigación del cambio climático.

Para ello pedimos que se cuente con los expertos y que se pase a la acción antes de que sea demasiado tarde. Que se desarrollen ya las estrategias de adaptación al cambio climático y no se queden en un cajón.

![vaso de plastico y pajitas de plastico](/images/uploads/pajitas.jpg)

## Acciones de Madres por el Clima

Además de los [Eco Consejos](https://madresxelclimamurc.wixsite.com/madresxelclimamurcia/eco-consejos) para reducir nuestro impacto en el planeta (que podéis encontrar en nuestro blog y en nuestras rss) hemos llevado a cabo las siguientes acciones:

* Impulsar la sustitución de miles de vasos de un solo uso por reutilizables en 90 comedores escolares de la Región.
* Campañas “Compra Sostenible”, “Carnaval low cost”, “Navidad sostenible” y “Recreos Residuo Cero”, esta última junto con Teachers For Future.
* \#SOSperiodistas, que consiste en pedirles que den voz a la emergencia climatica.
* El 5 de junio, con motivo del Día Mundial del Medio Ambiente, nos reunimos con las concejalas de los ayuntamientos de Murcia, Cartagena y Santomera y enviamos una carta a mujeres influyentes de la Región, pidiéndoles su ayuda para conseguir medidas reales y eficientes en la lucha contra el cambio climático.
* Campaña “Más árboles, menos toldos” en la que se envió, junto con otros colectivos, una carta al Ayuntamiento de Murcia, pidiendo que en todas las zonas donde sea posible se planten árboles en lugar de poner toldos, ya que además de proteger de la contaminación y tener menos impacto ambiental, tienen efectos psicológicos positivos en las personas.
* Con motivo de la Huelga Mundial por el Clima del 19 de marzo, preparamos un video con un mensaje de nuestros pequeños, las [\#VocesNextGeneration](https://www.youtube.com/hashtag/vocesnextgeneration)​, que piden a los responsables políticos que inviertan estos fondos en medidas adecuadas para combatir el cambio climático, ya que es su futuro lo que está en juego. También hemos querido retomar el contacto con aquellas mujeres en las que confiamos el 5 de junio.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xdsIxapGduc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Colaboración en el proyecto “Cartagena Amable, ciudad 30”, para conseguir una movilidad sostenible en esta ciudad.
* Impulso y colaboración en la implantación en Cartagena del proyecto “La Ciudad de los Niños” del psicopedagogo Francesco Tonucci, que pretende transformar las ciudades recuperando el espacio público para toda la ciudadanía mediante la implantación de las iniciativas de los niños y niñas.

## ¿Te sumas a Madres por el Clima?

¡¡La lucha contra el cambio climático necesita apoyo y visibilidad!!

Te esperamos! Puedes encontrarnos en:

E-mail: 

[madresxelclimamurcia@gmail.com](mailto:madresxelclimamurcia@gmail.com) 

[madresporelclimacartagena@gmail.com](mailto:madresporelclimacartagena@gmail.com)

Instagram: 

[@MadresxelclimaMurcia](https://www.instagram.com/madresxelclimamurcia/) 

[@MadresxelclimaCartagena](https://www.instagram.com/madresxelclimacartagena/)

Facebook: 

[@madresxelclimamurcia](https://www.facebook.com/madresxelclimamurcia) 

[@madrescartagena](https://www.facebook.com/madrescartagena)

Twitter: 

[@murciaclima](https://twitter.com/MurciaClima) 

[@madresct](https://twitter.com/MadresCt)

[Web de Madres por el clima](https://madresxelclimamurc.wixsite.com/madresxelclimamurcia/blog)

Si te ha gustado, comparte para que esta iniciativa tan necesaria llegue a cuantas más familias, mejor!

Escucha el episodio que grbmos con Patricia Esteve, de Madres por el Clima Murcia en el podcast de las Madres Cabreadas.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/2eeq9OKRIMb4Eur2eQsHYj?utm_source=generator&theme=0" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Suscríbete al blog para no pederte nada!

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
>
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)