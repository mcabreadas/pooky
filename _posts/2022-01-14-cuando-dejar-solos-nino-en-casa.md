---
layout: post
slug: cuando-dejar-solos-niño-en-casa
title: ¿Cuándo podrás dejar a tus hijos solos en casa?
date: 2022-01-17T13:08:27.323Z
image: /images/uploads/portadas-ninos-solos-en-casa.jpg
author: luis
tags:
  - adolescencia
  - trucos
---
¿A qué edad te dejaban tus padres solos en casa? Seguro que si haces memoria, te sorprenderá la respuesta y nos parecerá incluso raro que a tan tempranas edades nos dejaran solos antaño. Lo cierto es que el ritmo de vida y la educación han cambiado mucho desde hace 30-40-50 años para adelante y lo que antes parecía normal, ahora nos parece una completa locura.

La cuestión que vamos a abordar hoy ronda por la cabeza de los padres, sobre todos los primerizos, ya que nunca se encuentra el momento adecuado para poder dejarles solos en casa. Nosotros vamos a intentar dar solución a las dudas más frecuentes acerca de este tema y para ello vamos a valernos de un informe realizado por la AEP (Asociación Española de Pediatría) y la[ Fundación Mapfre](https://www.fundacionmapfre.org/), muy útil y que nos da las claves para resolver la gran pregunta: ¿Cuándo se puede dejar solos a los niños en casa?

## ¿Cuál es la edad legal para dejar a un niño solo en casa?

La legislación española regula muchos aspectos de nuestra vida y lo mejor para asegurarnos de que estamos cumpliendo con la legalidad es echar un vistazo a las leyes. No existe una ley concreta en la que se establezca una edad mínima para dejar solo a un menor en casa. 

No obstante, sí que hay leyes que protegen a los menores cuando llegan a estar en situación de desamparo. El artículo 172 del Código Civil es un ejemplo ya que regula y castiga el incumplimiento de los deberes de protección que se recogen en las leyes para la guarda de menores. 

![niños solos en casa](/images/uploads/ninos-solos-en-casa.jpg "niños solos en casa")

Por tanto, hay que tener mucho cuidado porque si la situación se vuelve en regular y el niño no posee la suficiente madurez para estar solo en casa, se puede acusar a los padres de haber incurrido en un delito de desamparo de los hijos, dando lugar a una sanción que puede llevar a los progenitores a la cárcel o perder la tutela de sus hijos. 

Por tanto, en la legislación no se nos habla de una edad concreta, pero sí que nos avisan de que el niño debería tener la madurez y las capacidades necesarias para permanecer en el hogar sin la supervisión de un adulto.

## Edad recomendada para dejar a los niños solos en casa según los expertos

Lo que sí vamos a poder encontrar son las recomendaciones de los expertos en la materia, que pueden ser diferentes ya que cada uno tiene en cuenta algunas pautas de referencia (la responsabilidad, la personalidad del niño, su madurez, etc.) que llegan a fijar como las adecuadas para que un niño se quede en casa sin supervisión. 

En lo que suelen coincidir todos estos expertos es que las aptitudes necesarias para quedarse solo en casa no son alcanzadas hasta, al menos, los 7 años de edad. Niños menores de esta edad no tienen conciencia del riesgo y no son capaces de resolver situaciones complicadas con éxito. Por tanto, el tramo de edades para los que se aconseja que los niños comiencen a quedarse solos en casa está entre los 9 y los 12 años.

Los datos que se recogen en el [informe ](https://www.fundacionmapfre.org/publicaciones/todas/informe-accidentes-en-la-poblacion-infantil-espanola/)promovido por la AEP y Fundación Mapfre son muy significativos, como que el 9,1% de los menores de 12 años se quedan solos en casa, siendo el 5% menores de 4 años. Estas actitudes son inaceptables ya que estamos dándole una responsabilidad a niños que no pueden soportar, mucho menos tienen las capacidades para hacerse cargo de sí mismos.

## Cómo preparar a nuestros hijos para manejarse sin la supervisión de un adulto

Ahora que ya sabes cuáles son las recomendaciones según los expertos, lo que debe quedarte claro es que la decisión final es tuya, no hay una regla fija, más allá de que menores de 7 años no deberían quedarse solos, porque dependerá de la madurez y la personalidad de tu hijo. Si tu vástago ya se encuentra en el rango de edad recomendado y quieres comenzar a probar a dejarle solo, te vamos a dejar unas pautas de preparación que os vendrán bien tanto a ti, para quedarte tranquila, como a tus hijos.

![edad niños solos en casa](/images/uploads/edad-ninos-solos-en-casa.jpg "edad niños solos en casa")

### Enséñale a ser autónomo y no lo sobreprotejas

Siempre se habla de que esta nueva generación de madres y padres somos muy sobreprotectores con nuestros hijos y es por eso que nos invitan a dejarles un poco de libertad y enseñarles a ser más autónomos. Llegará el momento que ellos mismos te pidan la confianza para quedarse solos y esta ya es una actitud que deberíamos aplaudir, puesto que están demostrando confianza y responsabilidad.

La verdadera virtud está en el término medio, cuando no se sobreprotege a los niños y se les da la suficiente libertad para que puedan ir creciendo conforme a su edad, poco a poco, y siendo cada vez más autónomos y siendo capaces de realizar las tareas por su propia cuenta.

### Comienza de manera progresiva

Como es normal que en los inicios no tengamos todavía la suficiente confianza, lo mejor es ir haciéndolo de manera paulatina. Lo ideal sería comenzar por que se quede solo en su habitación y ver como reacciona, si requiere mucho de vuestra presencia o si se adapta bien. 

Un paso más allá sería hacer salidas cortas de casa, como ir a tirar la basura o bajar un momento a por el pan. Esos minutos ya les va a valer para darse cuenta de que están solos y cómo se adaptan a esa situación, aunque sepan que durará pocos minutos y sus padres estarán al volver. Experimentando, preguntando y viendo cómo os sentís por ambas partes es la clave para ir alargando esos momentos cada vez más.

### Déjale claras las normas

Que no haya supervisión de un adulto en el hogar no significa que vaya a cundir la anarquía. En la casa hay unas normas claras, estén o no estén papá y mamá en casa. Siempre tienes que darle las instrucciones de manera clara, incluso en los inicios es conveniente dejárselas por escrito para que nunca las olviden. 

Desde no abrir la puerta a desconocidos hasta no utilizar aparatos de riesgos o alertar de la extrema precaución en la cocina, son algunas de las pautas que más se suelen repetir y que no deberíamos olvidar.

### Asegúrate de que se queda bien comunicado y llámale de vez en cuando

Que no estemos de cuerpo presente no significa que nuestro hijo se tenga que hacer cargo de todo. Es por eso que ante la duda, lo mejor es que nos llamen y, si no es posible localizarte directamente, dejarle números de contactos como pueden ser la abuela o incluso de algún vecino. Además, es importante que tengan los números de los servicios de emergencia a mano, por si sucediera algo inesperado. Policía, bomberos y urgencias, deberían ser los que no pueden faltar.

Recuerda que si ellos no están por la labor de llamarte, puedes enviarles tú un mensaje o hacerle una llamada breve para controlar que todo está bien. Eso sí, no lo hagas como una paranoica que está imaginando la casa en llamas, puesto que eso le transmitirá falta de confianza en él.

### Prepárale alguna actividad para que se mantenga ocupado

En los inicios el tiempo que se queden solos se les puede hacer pesado, por lo que lo ideal es que le dejes preparadas algunas actividades con las que se puedan distraer. Esto además, hará que se mantengan ocupados y no estén pululando por la casa y se les ocurran acciones que puedan resultar peligrosas. Entre estas actividades podemos dejarle deberes, prepararle algún juego o que vean la televisión.

aqui te dejamos unas [recomendaciones de series para adolescente](https://madrescabreadas.com/2020/06/18/recomendaciones-series-adolescentes-dos/)s que seguramente le gusten.

### Déjale comida preparada

Da igual si el tiempo que van a estar solos sea pequeño o no coincida con las horas de comida, siempre es conveniente dejarles alimentos disponibles por si tienen hambre. Hacer esto nos ayuda a que no sientan la tentación de experimentar en la cocina si el hambre les sorprende y no encuentran nada con lo que saciarse.

En definitiva, no existe una edad legal en España para dejar a los niños solos, como si la hay para que puedan viajar en avión, por ejemplo. En este caso, son los padres los que deben valorar si su hijo está preparado para quedarse sin supervisión de un adulto al tener la madurez y el sentido de la responsabilidad desarrollados. Y tú, ¿ya has dejado solos a tus hijos alguna vez en casa? Seguro tu experiencia puede ayudar al resto de lectores, así que no dudes en compartirla.

*Photo by Allen Taylor on Unsplash*