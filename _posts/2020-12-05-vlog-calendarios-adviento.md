---
author: maria
date: '2020-12-05T18:19:29+02:00'
image: /images/posts/vlog/p-calendarios-adviento.jpg
layout: post
tags:
- vlog
title: Vlog ideas para calendarios de Adviento
---

Qué es un calendario de Adviento, cuál es su origen, ideas para fabricar vuestro propio calendario de Adviento casero y de regalillos para meter dentro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TpwJSr9ziko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Si te ha gustado comparte y suscríbete a mi canal!