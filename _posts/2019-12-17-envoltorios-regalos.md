---
date: '2019-12-17T01:00:29+02:00'
image: /images/posts/envolver-regalos/p-entrega.jpg
layout: post
tags:
- planninos
- trucos
- navidad
- regalos
- diy
title: Ideas navideñas de packaging para empresas
---

Tengas el negocio que tengas este post te puede interesar para aprovechar el periodo navideño y coger ideas para el packaging de tu empresa en estas fechas tan señaladas. Una buena forma de diferenciarse de los competidores es ofrecer un packaging personalizado al consumidor, ya sea hecho por vosotros mismos o encargado a otra empresa, como puede ser [Embaleo](https://www.embaleo.es). 

Si para este año aún no disponéis todavía de cajas ya usadas para empaquetar de forma original, siempre podéis [comprar cajas de cartón baratas](https://www.embaleo.es/1010-cajas-de-carton), pero tenéis que aprovechar estas fiestas para guardar todos los envoltorios que podáis de cara al próximo año (nunca es tarde para empezar a reciclar).

¿Os habéis parado a pensar la cantidad de basura que generamos en estas fechas? Cada año, del 15 de diciembre al 15 de enero los servicios de recogida de basura de los ayuntamientos se desbordan. La noche del 6 al 7 de enero se alcanza el récord de volumen del año. No en peso, sino en volumen, por las dificultades de manejo de la masa de cartones y embalajes que colapsan los camiones. La noche del 25 al 26 de diciembre y las del 1 al 2 de enero también se baten récords. Sólo en Madrid se aumenta la recogida de papel en un 35%, pasando de recoger diariamente 3.249 toneladas, a superar las 5.500.

Ya sea para vuestros clientes o para empleados, el envoltorio en el que se recibe un paquete aún cobra más importancia en estas fechas. A continuación, os mostraré ideas originales para dar forma a vuestros paquetes navideños:

## Cajita de cartón reciclado
Con cartón ondulado pintado se pueden hacer unas cajitas ideales para empaquetar un regalo o para colgar en el árbol. con esta [plantilla para hacer una caja](https://madrescabreadas.com/tumblr_files/Plantillaparacubo2-2.jpg), pegamento y un cordón o lazo, os quedará así de bonita. Podéis hacerla con los niños porque se trata de una manualidad muy sencilla. 

![caja carton diy](/images/posts/envolver-regalos/caja-rosa.jpg)

## Envoltorio eco para el regalo de Navidad
Podéis usar elementos naturales para poner el broche final a vuestro paquete con ramas secas, hojas o piñas (mis favoritas). O si tenéis la suerte de vivir cerca del mar, podéis usar conchas.

![envolver regalo lavanda](/images/posts/envolver-regalos/lavanda.jpg)

## Telas accesorios de costura para decorar el paquete
Con retales olvidados de la caja de los hilos y un anudado original sobre el envoltorio de papel lograremos un acabado diferente para nuestro embalaje. 
También podemos usar el botón grande que se le cayó a aquel abrigo y que nunca llegamos a coser, o cinta de raso que ya no usamos para hacer un bonito lazo a juego con los colores del envoltorio. 
Si sois más manitas podéis coser un saquito con la tela y acabarlo con una lazada de cordón.
![bolsa de tela de regalo](/images/posts/envolver-regalos/saco.jpg)
También podéis buscar en Internet unos imprimibles de plantillas de letra industrial, y con pintura en spray estampar el nombre de la persona a quien va dirigido el regalo.

## Papel reciclado con cuerda para hacer un paquete
Usa papel de periódico o revistas antiguas, y si no tienes, puedes teñirlas con café para dar un aire vintage siguiendo estos pasos:

1. Arrugamos el papel
2. Lo volvemos a extender
3. Preparamos un cuenco con café soluble
4. Lo extendemos sobre el folio con ayuda de un algodón sin incidir muchas veces sobre una misma zona, para no levantar el papel.
5. Dejamos secar

## Mapas o callejeros para envolver regalos
Este tipo de print está súper de moda, y quedarán unos paquetitos muy chic, sobre todo si los atas con un lazo coordinado con los tonos del papel.

## Ata una etiqueta con una cuerda al paquete del regalo
Con cartón o cartulina usada y una troquelada puedes fabricarte tus propias etiquetas para poner un mensaje navideño o identificar el titular del reglo. 
Hazle un agujero para pasarles un cordón y poder atarlo al paquete o bolsa.
![paquete de regalo con etiqueta](/images/posts/envolver-regalos/etiqueta.jpg)

## Estrellitas para decorar el regalo
Unas estrellas recortadas o troqueladas no pueden faltar en cualquier reglo que se precie. Usa restos de bolsas o papel metalizado para aportar brillo.
![paquete de regalo con estrella](/images/posts/envolver-regalos/estrella.jpg)

## Cascabeles para dar un toque alegrar envoltorio
Me encantan los cascabeles! 
Cada vez que cae en mis manos uno, no lo dejo escapar y lo guardo porque me hace mucha gracia atarlos a una cuerdecita y ponerlo en el árbol de Navidad o en algún envoltorio de regalo.
![envoltorio de regalo con cascabel](/images/posts/envolver-regalos/cascabel.jpg)

## Bolsas de cartón para meter los regalos de Navidad
Para mí son imprescindible, y me gustan mil veces más que las de plástico, por eso cuando voy de compras jamás me deshago de ellas. 
![bolsas de regalo](/images/posts/envolver-regalos/bolsas.jpg)

No te olvides de reciclar los envoltorios y paquetes para la próxima Navidad!


Si te ha gustado este post, compártelo!






- *Photo de portada by Kira auf der Heide on Unsplash* 
- *Photo 1 madres cabreadas*
- *Photo 2 by Tetiana Shadrina on Unsplash*
- *Photo 3 by Joanna Kosinska on Unsplash*
- *Photo 4 by Samuel Holt on Unsplash*
- *Photo 5 by Trifon Yurukov on Unsplash*
- *Photo 6 by Joshua Hoehne on Unsplash*
- *Photo 7 by freestocks.org on Unsplash*