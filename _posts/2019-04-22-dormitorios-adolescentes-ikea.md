---
layout: post
slug: dormitorios-adolescentes-ikea
title: Dormitorios que mejoran el sueño de los adolescentes
date: 2019-04-22T18:19:29+02:00
image: /images/posts/dormitorios-adolescentes-ikea/p-dormitorios-adolescentes-ikea.jpg
author: .
tags:
  - adolescencia
  - trucos
  - salud
  - decoracion
  - crianza
---

Cuando nuestros hijos se convierten en adolescentes cambian muchas cosas en ellos y debemos adaptarnos toda la familia a los cambios, incluso a veces son necesarios ciertos cambios en su entorno, por ejemplo su dormitorio. Quizá ya no quepan en su cama infantil o quieran independizarse de habitación si hasta ahora había sido compartida. Hoy vamos a hablar de algo tan importante como los hábitos de sueño en los adolescentes, ya que suelen cambiar bastante y a pesar de que sus necesidades de sueño son de unas nueve horas, estos tienden a retrasar la hora de acostarse porque dan preferencia a las actividades al final del día, duermen menos horas y hasta un 20% padece somnolencia diurna.

## Los móviles afectan al sueño de los adolescentes

Se trata de un grupo poblacional con patrones de sueño irregulares, en gran parte, por el uso de los dispositivos electrónicos, especialmente el teléfono móvil, que tienden a utilizar en sus habitaciones antes de acostarse lo que influye en una incorrecta regulación del patrón de sueño en el adolescente.

Si bien es cierto que lo ideal, según los expertos, es que los espacios de estudio y descanso estén separados, lo habitual es que los hogares españoles cuenten con una superficie reducida y estancias pequeñas, por eso la zona de estudio suele encontrarse en el dormitorio. 

Por ello, es importante que este espacio incluya facilidades para que, al menos dos horas antes de acostarse, el dormitorio quede liberado de estos dos factores perjudiciales para el sueño a través de sistemas de almacenaje para guardar los dispositivos electrónicos apagados.
![dormitorio juvenil moderno ikea](/images/posts/dormitorios-adolescentes-ikea/panel-dormitorio-adolescentes-ikea.jpg)

## La habitación perfecta para el sueño de los adolescentes

Ya os di unas ideas muy chulas para hacer un [rincón de lectura infantil](https://madrescabreadas.com/2018/07/23/nuevo-cat%C3%A1logo-ikea-2019-rinc%C3%B3n-de-lectura/) con cositas de Ikea, pero hoy toca hablar de dormitorios de adolescentes, y no sólo a nivel estético, porque [IKEA](https://www.ikea.com) ha creado la habitación saludable del sueño siguiendo las evidencias científicas y las recomendaciones de la Sociedad Española del Sueño:
[![sueño adolescentes infografia ikea dormitorio](/images/posts/dormitorios-adolescentes-ikea/infografia_dormitorio_adolescentes-ikea.jpg)](/images/posts/dormitorios-adolescentes-ikea/infografia_dormitorio_adolescentes-ikea.pdf)


## Temperatura y sueño de los adolescentes

Al existir una relación directa entre la temperatura del ambiente y la denuestro cuerpo, se recomienda mantener una temperatura neutra, entre 18 y 21 grados.

Teniendo en cuenta que durante la fase REM del sueño, la más sensible, nuestro cuerpo tiene mayor dificultad para la termorregulación, es importante usar tejidos que la favorezcan.

## Luz y sueño de los adolescentes

La luz que emiten los dispositivos electrónicos tienen consecuencias como retraso en la secreción de melatonina, aumento de la alerta o retraso en el inicio del sueño. El dormitorio ha de ser un espacio libre de dispositivos electrónicos, que no se deben usar antes de dormir.

Dormir con la luz encendida o en dormitorios con 
contaminación lumínica produce un sueño más superficial, mayor número de estados de vigilia y alteración de las ondas cerebrales responsables del sueño profundo. La
intensidad de la luz en la habitación a la hora de acostarnos no debe superar los 75 luxes. Un sistema de iluminación inteligente para regular la intensidad de la luz es perfecta para conciliar bien el sueño.
![dormitorio juvenil ikea iluminacion](/images/posts/dormitorios-adolescentes-ikea/iluminacion-dormitorio-adolescentes-ikea.jpg)

## Ruido y sueño de los adolescentes

El ruido ambiental es uno de los principales responsables de la interrupción
del sueño, generando situaciones de insomnio o dificultando un sueño profundo en la fase REM. La recomendación de la OMS establece 30 decibelios como nivel máximo de ruido durante la noche. El uso de técnicas de aislamiento acústico son muy útiles para controlar el ruido en el dormitorio.

## Decoración del dormitorio de los adolescentes

El desorden de objetos en el entorno, como ropa, libros, revistas, ordenadores… no permite que la mente desconecte. Es importante mantener el espacio ordenado con sistemas de almacenaje en los que guardar los objetos cuando ya no los utilizamos.

Las tonalidades neutras o pastel favorecen la paz, la relajación y el descanso.

![dormitorio juvenil ikea almacenaje](/images/posts/dormitorios-adolescentes-ikea/almacenaje-dormitorio-adolescentes-ikea.jpg)

## Cama para adolescentes
La cama es el lugar en el que pasamos una tercera parte de nuestra vida, por lo que resulta clave elegir un colchón, una almohada y una ropa de cama que favorezcan el sueño. Un 7% de los problemas de sueño son consecuencia de un colchón incómodo.

Un colchón ergonómico de dureza media y una almohada bien elegida mejoran la calidad del sueño y facilitan un sueño reparador.

## Dormir bien es una cuestión de salud para todos
La relación existente entre el espacio donde dormimos y la calidad de nuestro sueño es, en palabras de la SES, “fundamental”, ya que la habitación y el ambiente son los responsables en buena medida de la duración y estructura del sueño, y cuanto más escaso e irregular sea este, mayor riesgo de desarrollar problemas de sueño.

Según el Dr. Carlos Egea, organizador de este congreso de la [SES](http://ses.org.es/): 
> “El 30% de los españoles sufre algún trastorno del sueño, lo que aumenta un 36% el riesgo de alteraciones coronarias. Si pensamos que dormir es tiempo perdido, tendremos un problema como especie. La sociedad tiene que tomar conciencia de los riesgos de salud que implica dormir poco y a partir de ahí modificar los hábitos relacionados con el sueño”.

Por tanto todo entorno que no induzca a conciliar y mantener el sueño tendrá un impacto en nuestra salud mental, emocional y física. Un correcto descanso contribuye a evitar problemas relacionados con el funcionamiento cognitivo y las emociones así como el riesgo de desarrollar patologías.

¿Duermes bien?