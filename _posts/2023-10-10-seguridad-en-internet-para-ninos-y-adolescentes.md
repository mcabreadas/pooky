---
layout: post
slug: seguridad-en-internet-para-ninos-y-adolescentes
title: 3 maneras de colaboración entre escuela y familia para proteger a
  nuestros menores en Internet
date: 2023-10-11T11:02:06.426Z
image: /images/uploads/depositphotos_402100732_xl.jpg
author: .
tags:
  - 6-a-12
  - educacion
---
Un nuevo año escolar ha comenzado y con él la necesidad de que tanto las familias como el equipo educativo nos invlucremos en el desarrollo de las habilidades digitales de nuestros niños y adolescentes. Esto es esencial para minimizar los riesgos relacionados de Internet y el uso de la tecnología por parte de los más jóvenes, lo que también puede afectar su desempeño en la escuela. En este sentido, el papel de los educadores es fundamental para guiar a las familias en los aspectos más importantes relacionados con el uso responsable de la tecnología por parte de sus hijos. 

A continuación, te ofrecemos una serie de recursos para ayudarte a prevenir problemas en línea y crear un entorno digital seguro en casa.

## ¿Cómo podemos garantizar la seguridad en Internet para niños?

Aqui te dejo la informacion sobre nuestros talleres de seguridad en internet para menores, padres, y profesorado.

<a href="https://www.granviaabogados.com/impartimos-talleres/" class='c-btn c-btn--active c-btn--small'>Info sobre talleres sobe seguridad en Internet</a>

Por parte del equipo docente, existe la responsabilidad de informarnos a las familias sobre diversos temas relacionados con el uso de la tecnología por parte del alumnado. Esto incluye la información sobre los protocolos de la escuela y los números de ayuda disponibles, como el número gratuito y confidencial de Tu Ayuda en Ciberseguridad de INCIBE, 017. 

Durante reuniones o coordinaciones con las familias se deben ofrecer recursos para mediar y actuar ante situaciones negativas a las que los menores puedan estar expuestos. 

## Los colegios pueden fomentar las Competencias Digitales de las Familias

No solo es importante informarnos a las familias, sino también apoyarnos en cuestiones tecnológicas que puedan afectar nuestra relación con la escuela. Ayudarnos a las familias a mejorar nuestras propias competencias digitales puede contribuir a un mejor desempeño en nuestro papel de mediadores parentales. Si eres profesor, te damos algunas sugerencias para tu proxima reunion de padres:

![](/images/uploads/depositphotos_19485371_xl.jpg)

* Ofrecer información sobre cómo gestionar los grupos de WhatsApp escolares de manera efectiva, promoviendo un ambiente respetuoso y seguro para proteger la privacidad de los estudiantes.
* Orientar a las familias sobre la configuración de seguridad y privacidad en las plataformas educativas utilizadas en la escuela, como Microsoft Teams o Google Classroom.
* Invitar a las familias a realizar pruebas de conocimientos sobre ciberseguridad y el uso seguro de Internet, en relación con sus hijos.

  ## Herramientas para educar a nuestros hijos en un uso de Internet seguro

  Para que las familias podamos desempeñar nuestro papel de mediadores de manera efectiva, es importante que nos proporcionen recursos y herramientas. Algunas de estas incluyen:
* [Hablando de Internet en familia](https://www.incibe.es/menores/recursos/mediacion-parental): ofrece recomendaciones para iniciar conversaciones sobre Internet y el uso de la tecnología por parte de los niños, promoviendo un diálogo constante al respecto.
* [Organizador digital familiar](https://www.incibe.es/sites/default/files/contenidos/materiales/Campanas/is4k_organizador_digital_familiar.pdf): una herramienta para establecer normas y límites en el uso de Internet, incluyendo horarios de uso para cada miembro de la familia.
* [Pactos familiares](https://www.incibe.es/menores/familias/pactos-familiares-para-el-buen-uso-de-dispositivos): documentos de apoyo para consensuar normas y límites en el uso de dispositivos, aplicaciones y servicios en línea.
* [Vales de tiempo](https://www.incibe.es/menores/recursos/mi-mundo-digital/vales-de-tiempo): una herramienta para controlar el tiempo de uso de la tecnología, como redes sociales o videojuegos.
  La tecnología ya es una parte integral de nuestras casas y nuestras aulas, y es fundamental involucrarnos a las familias desde el colegio para crear un entorno digital seguro para nuestros niños y niñas y adolescentes. 
  Si deseas obtener más información sobre cómo abordar la tecnología en el entorno educativo, promover actividades didácticas o mejorar tu formación en ciberseguridad te recomendamos nuestros talleres de seguridad en internet para menores, padres, y profesorado.

 <a href="https://www.granviaabogados.com/impartimos-talleres/" class='c-btn c-btn--active c-btn--small'>Info sobre talleres sobe seguridad en Internet</a>

  Juntos, podemos generar un entorno más seguro para nuestros niños, niñas y adolescentes.
  Fuente del articulo: [INCIBE](https://www.incibe.es)