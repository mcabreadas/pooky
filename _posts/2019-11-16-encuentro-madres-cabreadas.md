---
date: '2019-11-16T18:19:29+02:00'
image: /images/posts/encuentro-madres-cabreadas/p-grupo-madres-cabreadas.jpg
layout: post
tags:
- local
- eventos
title: I Encuentro Madres Cabreadas
---

Después de 9 años de trayectoria y tantas experiencias compartidas a través de las redes sociales llegó el momento de que las Madres Cabreadas nos conociéramos en persona en una soleada mañana, y con el apoyo de la multinacional [Hero](https://www.hero.es/baby?age=all), en la que comenzamos relajándonos con una sesión de Yoga en grupo al aire libre a cargo de la profesora Petra Caldenius ([@yogaconpetra](https://www.instagram.com/yogaconpetra/)) en la que "reconectamos", esta vez en persona, y dejamos fluir la energía entre nosotras creándose este ambiente tan distendido y divertido.

![yoga circulo mujeres](/images/posts/encuentro-madres-cabreadas/yoga-circulo-madres-cabreadas.jpg)
![yoga en grupo](/images/posts/encuentro-madres-cabreadas/yoga-grupo-madres-cabreadas.jpg)
![yoga puente](/images/posts/encuentro-madres-cabreadas/yoga-puente.jpg)



## Charla-coloquio sobre novedades legislativas en materia de conciliación
A raíz de la charla impartida por mí en la que expuse las novedades legislativas en cuanto a la distribución del tiempo de baja maternal, la ampliación del permiso por paternidad (prestación por nacimiento y cuidado del menor por el progenitor distinto a la madre biológica), el permiso de lactancia para el padre (prestación por corresponsabilidad en el cuidado del lactante), la la "jornada a la carta" y el teletrabajo, tuvo lugar un acalorado debate sobre estas nuevas medidas legales de conciliación, cómo afectarán a familias y a empresas y los problemas prácticos en su aplicación: ¿Son suficientes? ¿Serán eficientes? ¿Hemos avanzado en este ámbito en los últimos años? 

![ponente charla conciliacion](/images/posts/encuentro-madres-cabreadas/charla-conciliacion.jpg)
![asistentes charla conciliacion](/images/posts/encuentro-madres-cabreadas/asistentes-charla-conciliacion.jpg)
![pregunta charla conciliacion](/images/posts/encuentro-madres-cabreadas/intervencion-asistente-charla.jpg)



## Taller de maquillaje express para descabrearnos
El taller de maquillaje express diario para llegar a tiempo a llevar a los niños al cole fue a cargo de la maquilladora profesional Julia Sánchez Manzano ([@giiuliia_sm](https://www.instagram.com/giiuliia_sm/)) que contó como modelo con Carolina de [@lafamilia_leon](https://www.instagram.com/lafamilia_leon/) y usó productos cedidos por la firma Identy Beautuy. 
La sesión se proyectó en tiempo real en una pantalla grande para que todas las asistentes pudieran apreciar los detalles con ayuda de un aro de luz y se retransmitió en un directo de Instagram. Finalmente se subió el video completo a Youtube. 
Podéis ver el [taller de maquillaje express aquí](https://youtu.be/Wip9G9cee3U)

![maquillaje en pantalla gigante](/images/posts/encuentro-madres-cabreadas/paquillaje-pantalla-gigante.jpg)
![aro de luz maquillaje](/images/posts/encuentro-madres-cabreadas/aro-luz-maquillaje.jpg)


Como colofón final sorteamos un lote de productos de maquillaje [Identy Beauty](https://www.identybeauty.com/es/?utm_campaign=ES-IG-I-PER-BIOG-&utm_source=Instagram&utm_content=2610)  igual que los usados por la maquilladora para el taller y una sesión de maquillaje personalizada a cargo de Julia Sánchez Manzano.

![Maquilladora Identy](/images/posts/encuentro-madres-cabreadas/167_Madres Cabreadas_baja___E2A1330.jpg)
![caja identy beauty](/images/posts/encuentro-madres-cabreadas/sorteo-identy-maquillaje.jpg)



## Escritoras y artesanas murcianas

Las autoras murcianas, y también madres, Paula Miñana  [@paulagram](https://www.instagram.com/paulamgram/) autora de la exitosa novela ["Nosotros en Singular se dice tú y yo"](https://www.amazon.es/gp/search/ref=as_li_qf_sp_sr_tl?ie=UTF8&tag=madrescabread-21&keywords=paula miñana&index=aps&camp=3638&creative=24630&linkCode=ur2&linkId=d03e8f499790614631fa05150bb67fa3), Mª Felisa Martínez Talavera , autora del libro infantil ["Grandes Poesías de Pequeños Poetas"](https://www.casadellibro.com/ebook-grandes-poesias-de-pequenos-poetas-ebook/9788461344314/2385231?utm_medium=AfiliadosDirectos&utm_campaign=23045&utm_source=ND), y la mallorquina Noemí García de marina, autora de  "[Mamá, ¿tú me quieres?](https://www.amazon.es/gp/product/8417852352/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=8417852352&linkId=264b276a387956835dc48da7bb66d3a1) (enlace afiliado)" sortearon un ejemplar de su último libro.

![Paula Miñana](/images/posts/encuentro-madres-cabreadas/nosotros-en-singular.jpg)
![Noemí Batega](/images/posts/encuentro-madres-cabreadas/mama-tu-me-quieres.jpg)
![Grandes Poesías de Pequeños Poetas](/images/posts/encuentro-madres-cabreadas/grandes-poesias.jpg)


Además, hubo sorteos de varias creaciones de las artesanas murcianas Margarita Pérez Plaza @margus.es de la firma [Margus Handmade](https://www.subidaenmistacones.com/2016/04/marcas-molona-margus-handmade.html) y Ana Alcaraz @kihanadecora, de la [tienda on line de creaciones maravillosas con flores preservadas Kihana Decora](https://kihanadecora.com).

![margus](/images/posts/encuentro-madres-cabreadas/ganadoras-sorteo-margus.jpg)
![bonsai preservado kihana decora](/images/posts/encuentro-madres-cabreadas/bonsai-kihana-decora.jpg)

[Hacienda del Carche](https://www.haciendadelcarche.com/es/inicio/) sorteó 3 pases dobles para el [Museo del Vino de Jumilla](http://www.museodelvinojumilla.com), [Fini Golosinas](https://www.fini.es/es/) 6 lotes de gominolas, y [Pisamonas Cartagena](https://www.pisamonas.es/zapateria-infantil/tienda/cartagena/) regaló un vale descuento y un detallito a cada asistente.

![entradas museo del vino](/images/posts/encuentro-madres-cabreadas/entradas-museo-del-vino-jumilla.jpg)
![Fini madres cabreadas](/images/posts/encuentro-madres-cabreadas/fini-evento-madres-cabreadas.jpg)
![pisamonas en evento madres cabreadas](/images/posts/encuentro-madres-cabreadas/pisamonas-evento-madres-cabreadas.jpg)




## Despedida

Para terminar nos hicimos una foto de grupo y nos divertimos con el photocall para llevarnos un recuerdo a casa de una mañana inolvidable en buena sintonía y nada de cabreos.


![photocall evento madres cabreadas](/images/posts/encuentro-madres-cabreadas/photocall-madres-cabreadas.jpg)



## Revista de prensa

[Entrevista en Televisión Murciana](https://www.youtube.com/watch?v=_5NQrrJyGcY) 


[Entrevista Onda Regional](https://www.orm.es/programas/viva-la-radio-el-factor-humano-madres-cabreadas/) 


[Reseña Revista murcia.com](https://www.murcia.com/region/noticias/2019/10/21-i-encuentro-madres-cabreadas.asp)


[Podcast RevistaGeneración Fénix](https://generacionfenix.com/podcast-sobre-el-blog-madres-cabreadas-de-la-murciana-maria-sanchez/)


[Reseña Agenda Menuda](https://www.agendamenuda.es/agenda-semanal/eventodetalle/58370/-/i-encuentro-madres-cabreadas)



## Fotos oficiales del I encuentro Madres Cabreadas
Si asististe y quieres buscarte en las fotos oficiales, o si no viniste pero te apetece echar un vistazo, aquí tienes todas las [fotos del I Encuentro Madres Cabreadas](https://photos.app.goo.gl/ZwFvG4krcqTWJVmB7)