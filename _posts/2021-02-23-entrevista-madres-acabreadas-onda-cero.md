---
author: maria
date: '2021-02-23T10:10:29+02:00'
image: /images/posts/onda-cero/p-ordenador.jpg
layout: post
tags:
- revistadeprensa
title: Hablamos sobre nuestro foro en Onda Cero
---

Hablo sobre la reciente creación del Foro Madres Cabreadas con Pura Cánovas en el programa Más Mujer, de Onda Cero Murcia.

En el foro resolvemos y ayudamos a las mujeres en una gran variedad de temas que ellas mismas proponen basados en las dificultades que encuentran a diario para sacar adelante su familia, su casa y su trabajo.

<a href="https://www.ondacero.es/emisoras/murcia/murcia/audios-podcast/mas-de-uno/mas-mujer-maria-sanchez-impulsora-madres-cabreadas_20210223603504d0ec8b8d00010bed66.html" class='c-btn c-btn--active c-btn--small'>Escucha la entrevista</a>