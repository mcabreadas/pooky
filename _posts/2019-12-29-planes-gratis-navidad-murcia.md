---
date: '2019-12-29T18:19:29+02:00'
image: /images/posts/planes-navidad/p-roscon.jpg
layout: post
tags:
- local
- planninos
title: 9 planes gratis de Navidad en Murcia
---

Si Murcia está bonita en primavera no os cuento cómo luce en Navidad. A la belleza de nuestra ciudad se une un clima suave para esta época del año que invita a salir a la calle con los niños a hacer cualquiera de las actividades que podemos encontrar en sus plazas y jardines, ya sean mercadillos navideños, artesanales, ferias de invierno para patinar sobre hielo, Belenes, árboles de Navidad gigantes, conciertos, cabalgatas, espectáculos...
No en contratéis otra ciudad con más variedad de ocio al aire libre y gratuito.
Por eso, aunque hay muchas más, os voy a recomendar nueve actividades gratuitas para hacer esta Navidad con vuestra familia en Murcia, que gustarán tanto a niños como adolescentes , adultos y abuelos también!

## 1- Árbol de Navidad gigante de la Plaza Circular de Murcia

Cada año la plaza Circular acoge un Gran Árbol de Navidad. Se trata de una infraestructura que alcanza los cuarenta metros de altura y cuenta con el adorno de 7.000 metros de guirnaldas. Más de 30.000 personas acudieron a su encendido este año que contó con la actuación de Chenoa.

![arbol navidad gigante murcia](/images/posts/planes-navidad/arbol.jpg)

Se puede visitar durante toda las Fiestas Navideñas y participar en los distintos talleres que se imparten: cocina dulces navideños, Juegos infantiles, Manualidades, Salud y alimentación, Robótica y nuevas tecnologías, medioambiente, planta tu árbol.

También hay conciertos de Ópera y lírica, Flamenco, Rock, Pop, Blues, Jazz, Tecno.

Con respecto a las actuaciones infantiles, el Gran Árbol contará con Títeres, Magia, Cuentacuentos, Masterclass dance, Nickelodeon game y Nochevieja Infantil.

[Os dejo el programa de actividades y talleres del gran árbol de Navidad de la Plaza Circular de Murcia aquí](http://www.murcia.es/documents/11263/532202/Programa+Navidad+2019-20+WEB.pdf).


Junto al espacio del Paje y Buzón Real también hay el punto Solidario de recogida de juguetes.

![buzones reyes magos](/images/posts/planes-navidad/buzones.jpg)

## 2- Feria Blanca del cuartel de artillería y espacio cultural 
(Las atracciones no son gratuitas)
La plaza del Cuartel de Artillería acoge hasta el día 7 de enero una pista de patinaje sobre hielo de 500 metros cuadrados, una montaña de 45 metros de descenso de trineos neumáticos y un tren infantil, un bosque suspendido con tirolina y el hogar de Papa Noel que, tras el Día de Navidad, se convertirá en el de Sus Majestades los Reyes Magos de Oriente.
La montaña de nieve artificial para realizar descensos en trineos neumáticos, que también permite la participación de padres e hijos juntos en trineos dobles. 

![trineos cuartel artillería](/images/posts/planes-navidad/trineos.jpg)

También hay puestos de comida, decoración navideña, un carrusel, un tren navideño y la Casa de Papá Noel con photocall incluido.

La Feria amplía este año su programación de actividades culturales gratuitas dirigidas a toda la familia, con conciertos y actuaciones durante todas las fiestas.
El horario de apertura de la pista de hielo de 500 metros cuadrados, que está situada en el Cuartel de Artillería, es de lunes a viernes de 16.00 a 22.

![carrusel](/images/posts/planes-navidad/carrusel.jpg)


## 3- Mercadillo artesanal navideño de Alfonso X

Durante las fiestas navideñas del 10 diciembre al 5 de enero, y de 11 a 14 horas y de 17 a 22 horas el paseo Alfonso X el Sabio de Murcia acoge la Muestra de Artesanía Regional centrada en artículos para regalo y decoración navideña.

Se dan cita cada año algunos de los mejores artesanos belenistas, ceramistas, bodegueros, queseros, talladores de vidrio y madera, alfareros, tallistas, confiteros... Todos ellos ponen a disposición de los viandantes sus obras hasta el día de Reyes.
Los visitantes tienen ocasión de encontrar auténticas obras de arte en figuras para decorar el Belén. La mayor parte de las piezas son exclusivas, ya que todos los artesanos belenistas cuentan con el certificado que acredita que sus obras están hechas a mano. 

![mercadillo artesanal alfonso x murcia](/images/posts/planes-navidad/mercadillo-alfonso-x.jpg)

Además también se pueden encontrar los tradicionales y auténticos dulces navideños hechos de forma totalmente artesana.


## 4- Mercado navideño de la Glorieta y espectáculo de luces en la fachada del Ayuntamiento 

Una año más podemos disfrutar del Mercadillo Navideño de la Glorieta de España con el espectáculo audiovisual Cuento de Navidad, un espectáculo inédito para toda la familia.

![flechas ilusiones](/images/posts/planes-navidad/ilusion.jpg)

**El Mercadillo**, conformado por 20 casetas de madera, permanecerá abierto hasta el 3 de enero, en horario de 11 a 23 horas. La creatividad, la música y las actividades serán los protagonistas de este enclave navideño, por el que pasarán más de 40 marcas de jóvenes creadores para mostrar sus productos. Además, contará con cuatro ‘foodtrucks’.
Más de 40 elementos decorativos conforman el Mercadillo, entre luces, figuras navideñas, abetos, etc. El punto central será el escenario circular, por el que pasarán 16 grupos que han participado en el CreaMurcia para poner ritmo a la Navidad. En cuanto a la programación del Mercadillo de Navidad, en Nochebuena y Nochevieja se realizará un aperitivo con actuaciones.

![mercadillo navideño glorieta](/images/posts/planes-navidad/mercadillo-glorieta.jpg)

Se proyectará en la fachada del Consistorio murciano el **Cuento de Navidad**, con música en directo, la historia del reloj del Ayuntamiento
El Cuento de Navidad se proyectará todos los días, a las 19 y 22:00 horas, en la fachada del Ayuntamiento. Su historia transcurre en el Consistorio murciano, y tiene como protagonista a un relojero, quien tras casi 800 años cuidando el reloj del Ayuntamiento, pierde el tiempo justo antes de que empiece la Navidad. La trama completa de este cuento, que aúna la fantasía y la temática navideña, se desvelará el día de la inauguración. Julio Navarro y Clara Morata ponen las voces a los personajes.


## 5- Talleres del triángulo de Murcia

Este ciclo de talleres navideños terminará el 3 de enero con el 'Taller de electricidad para novatos'. Una farmacia ofreció el día 26 el «Taller de análisis de piel personalizado'. 

![analisis de piel farmacia](/images/posts/planes-navidad/taller-farmacia.jpg)

Estos encuentros se celebran en la plaza Escultor Roque López a partir de las 11.30 o las 12.00 horas y serán clausurados con una chocolatada el día 3. También se sorteará una cesta de Navidad con un valor cercano a los 2.000 euros.
Los clientes que realicen compras superiores a los 75 euros entre el 20 de diciembre y el 6 de enero podrán dejar a sus hijos durante tres horas en una ludoteca.


## 6- Deporte en familia en el jardín de la seda
Un trozo de naturaleza en pleno centro con estanques con patos, caminos de tierra para correr, tirolina, instrumentos para ejercicio físico, parques infantiles y un restaurante sobre el agua.

![tirolina urbana](/images/posts/planes-navidad/tirolina.jpg)
A nosotros nos encanta ir porque tanto adultos como niños encontramos nuestro espacio favorito.

![estanque jardín de la seda](/images/posts/planes-navidad/estanque-seda.jpg)

## 7- Casita de Papá Noel en calle Basabé 

![casa papa noel calle basaba murcia](/images/posts/planes-navidad/casa-papa-noel-fuera.jpg)
De 11:00 a 13:30 h y 17:00 a 20:00 horas en la calle Besabé de Murcia hasta el 30 de diciembre Papá Noel recibirán en su Casa a todos los niños que quieran acercarse a saludarlo.

![sillon papa noel](/images/posts/planes-navidad/casita-papa-noel.jpg)


## 8- La calle salsa

Cada año esta asociación que promueve el baile latino con fines benéficos toma una calle del centro de Murcia y la llena de alegría. Se trata de una actividad abierta al público gratuita donde cualquiera puede bailar salsa o bachata y participar en las animaciones .
Este año tocó en el paseo Alfonso x, con el gran árbol de navidad de la redonda al fondo. Fue un lujo de escenario.

![bailarinas la calle salsa](/images/posts/planes-navidad/calle-salsa.jpg)

## 9- Churros con chocolate
(este plan no es gratuito)
En cualquier puesto callejero si paseáis por el centro podréis degustar churros y gofres con chocolate casero para disfrutar de una merendola, pero si lo preferís también tenéis cafeterías chulísimas para sentaros un ratito a descansar.

![churros con chocolate](/images/posts/planes-navidad/churros.jpg)

Godis es una de mis favoritas.

![gofre en godis](/images/posts/planes-navidad/godis.jpg)

Espero que te hayan gustado estas sugerencias y que si vienes a Murcia en Navidad lo paséis fenomenal.

Ya me contarás!

Si te ha gustado comparte!!