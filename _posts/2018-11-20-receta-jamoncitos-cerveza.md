---
date: '2018-11-20T18:19:29+02:00'
image: /images/posts/jamoncitos_cerveza/p_ingredientes_jamoncitos_cerveza.jpg
layout: post
tags:
- familia
- recetas
title: Receta de jamoncitos de pollo a la cerveza
tumblr_url: https://madrescabreadas.com/post/180316723699/receta-jamoncitos-cerveza
---

Éste no es un blog de cocina, ni yo soy una gran cocinera, pero sabéis que de vez en cuando me gusta compartir con vosotras alguna [ receta de cocina](https://madrescabreadas.com/tagged/recetas) que ha triunfado en casa, sobre todo para daros ideas a la hora de la comida en familia con platos que puedan agradar a todos, tanto a niños como adultos. 

En este caso he adaptado a nuestro gusto la receta de pollo a la cerveza del blog [ Cocina casera y fácil](https://www.cocinacaserayfacil.net). Para mí esta receta es un truco para que coman verdura y carne sin problemas porque gusta a todos, no hay problema en hacer grandes cantidades si sois familia numerosa como nosotros o vais a recibir invitados porque queda resultona.

## Ingredientes para los jamocitos de pollo a la cerveza

*   8 muslitos de pollo

*   2 dientes de ajo

*   2 cebollas

*   2 zanahorias

*   tomillo y romero secos

*   2 hojas de laurel

*   1/2 l de cerveza rubia o tostada

*   2 cucharadas de harina

*   Un puñadito de perejil fresco picado

*   aceite de oliva

*   sal y pimienta![hortalizas y carne](/images/posts/jamoncitos_cerveza/tumblr_piawbhdnaZ1qfhdaz_540.jpg)

(Los tomates de la foto son unos infiltrados porque la receta no lleva tomate)

## Cómo se hacen los jamoncitos de pollo a la cerveza

-En una olla alta, ponemos un chorrito de aceite de oliva a calentar a fuego fuerte y cuando esté caliente añadimos el pollo y lo sellamos por todos sus lados. Tiene que coger color, pero quedar crudo por dentro. Después, retiramos el pollo y lo reservamos en un plato.

-En el mismo aceite añadimos los dientes de ajo, las cebollas ambos bien picados y las zanahorias en rodajas finas. Salpimentamos y removemos con frecuencia, mientras dejamos cocinar a fuego suave unos 15 minutos.

-Cuando la cebolla esté transparente y la zanahoria parcialmente cocinada, añadimos las dos cucharadas de harina  para darle cuerpo a la salsa. Mezclamos todos los ingredientes e integramos la harina con la verdura. 
![ola con cebolla y zanahorias](/images/posts/jamoncitos_cerveza/tumblr_piawbkFZu91qfhdaz_540.gif)

-Después devolvemos el pollo a la olla con su jugo, añadimos el tomillo y el romero, las hojas de laurel y cubrimos todo con la cerveza. Cuanto más fuerte sea, más amargo va a quedar el plato. Te aconsejo que si no te gusta la comida con demasiado sabor, utilices una cerveza rubia. 

![pollo con cerveza](/images/posts/jamoncitos_cerveza/tumblr_piawblRKrM1qfhdaz_540.gif)

-Mezclamos e integramos bien todos los ingredientes con una cuchara y dejamos cocer a fuego suave unos 45-50 minutos, hasta que la salsa reduzca y espese.
![](/images/posts/jamoncitos_cerveza/tumblr_piawbf71By1qfhdaz_540.jpg)

## Consejo para servir el pollo a la cerveza

Se puede servir acompañado de un puré de patatas, tortilla de patata o incluso unas patatas al horno (¿se nota que nos encantan las patatas?), y una buena ensalada.
![plato de pollo a la cerveza](/images/posts/jamoncitos_cerveza/tumblr_piawbfABf31qfhdaz_540.jpg)