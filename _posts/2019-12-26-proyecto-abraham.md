---
date: '2019-12-26T12:19:29+02:00'
image: /images/posts/proyecto-abraham/p-bola.jpg
layout: post
tags:
- local
- moda
- mujer
- participoen
title: La lona que quiso ser adorno de Navidad
---

Quienes vivís en Murcia recordaréis seguro la [gran lona de Estrella de Levante](https://www.ginsa.es/portfolio-item/estrella-levante-presenta-en-el-centro-de-murcia-su-cerveza-verna/) que cubrió este verano el edificio junto al corte Inglés de Avenida de la Libertad. Casi 1000m2 de lona publicitaria con la [cerveza con limón Verna](http://www.estrelladelevante.es/verna/) como protagonista. Pero lo que casi nadie se pregunta es qué ha sido de ella...

<iframe width="560" height="315" src="https://www.youtube.com/embed/zJMbQgN87pE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## ¿Qué hacemos con la gran lona publicitaria?

¿Y si en vez de tirarla le damos una nueva vida? Pensaron en Estrella de Levante y [Proyecto Abraham](http://www.proyectoabraham.org). Y así empezó esta colaboración que ha generado 3 puestos de trabajo y que están llevando a cabo las mujeres de esta asociación de la que ya sabéis que soy muy fan porque cree en las segundas oportunidades, no sólo del material reciclable, sino de las personas a las que la vida las ha tratado mal. Esas mujeres que se levantan tras caer y que vuelven a creer en ellas mismas cuando alguien deposita su confianza en ellas.

![puesto mercadillo navideño](/images/posts/proyecto-abraham/puesto.jpg)

Para algunas de estas mujeres ya es un acto heroico el hecho  de acudir a los cursos de formación que ofrece la asociación, así nos lo cuenta Felicidad Cano, quien trabaja cada día junto al resto del equipo para lograr su inserción en el mundo laboral.

Gracias a estas mujeres y a su imaginación sin límites, está lona que era un desecho publicitario ha cobrado vida en forma de bolsos, neceseres, adornos para el árbol de Navidad, delantales, zapateros y hasta un tipi ideal para pedirlo a los Reyes Magos.

![tippi reciclado](/images/posts/proyecto-abraham/tippi.jpg)

Hasta 10 tipos de producto podéis encontrar en el puesto de Proyecto Abraham del mercadillo de Navidad de la Glorieta, en Murcia, hasta el 3 de enero, y en su sede y tiendas que podéis consultar en la [web de Proyecto Abraham](http://www.proyectoabraham.org).

![adorno árbol navidad reciclado](/images/posts/proyecto-abraham/arbolito.jpg)

Los beneficios irán destinados íntegramente a la financiación de más proyectos de esta asociación.

## Qué es Proyecto Abraham
Se trata de una entidad sin ánimo de lucro, intercultural, con un alto compromiso con la sociedad y el medio ambiente que trabaja con colectivos en situación de riesgo de exclusión social con el objetivo de promover su inserción socio-laboral desde iniciativas que garanticen la sostenibilidad y respeto medioambiental.

## Qué hace Proyecto Abraham

Su objetivo es proporcionar herramientas y recursos a personas en riesgo de exclusión de vulnerabilidad para que puedan encontrar un trabajo e integrarse en la sociedad.

También recogen ropa, juguetes, calzado y aceite usado en cocina para transformarlos en recursos con los que prestar acogida en vivienda, formación, empleo, ropero, acompañamiento y apoyo.


## El Ropero Regional de Proyecto Abraham
Es un programa para el reparto de ropa, calzado y juguetes en el desarrollo de actuaciones de lucha contra la pobreza en la Región de Murcia subvencionado por la Consejería de Familia e Igualdad de oportunidades, a través de contenedores de recogida instalados en los distintos municipios, y a los que Proyecto Abraham les da un segundo uso destinándolos a quienes lo necesiten.

## Atelier El Costurerico y empoderamiento femenino
El Costurerico es un proyecto de inserción laboral consistente en un taller de costura especializado en reparación, transformaciones y upcycling donde se confeccionan todo tipo de prendas y elementos del hogar, decoración o regalo. Aunque también ofrecen servicios a particulares y empresas.

Es uno de sus proyectos más grandes que genera empleo principalmente para mujeres enseñándoles una profesión con la que lograr tener sus propios ingresos y mantenerse con sus propios medios.

Tiene su sede en Calle Delicias, 6, Puente Tocinos, Murcia, y se puede visitar previa cita en el teléfono 968926541

## El Costurerico de Ikea

Pero seguramente lo que más conozcas de este proyecto sea el servicio de bordado y reparación y confección de textiles de la tienda Ikea Murcia, que ha empleado a 5 mujeres con un puesto de trabajo de calidad.

Si vienes por el centro de Murcia estas Fiestas no puedes faltar al mercadillo de la Glorieta y al puesto de Proyecto Abraham para conocerlas y llevarte un adorno para tu árbol!

![encarna talavera y luis alcazar](/images/posts/proyecto-abraham/grupo.jpg)

Yo ya estuve y compartí unos momentos de charla y una cerveza con Encarna Talavera, Luis Alcázar y Felicidad Cano, quien nos enseñó todos los productos que han salido de esta gran lona. Te muestro algunos:

![zapatero reciclado](/images/posts/proyecto-abraham/zapatero.jpg)

![neceser reciclado](/images/posts/proyecto-abraham/neceser.jpg)

![bolsa reciclada](/images/posts/proyecto-abraham/bolsa.jpg)



Si te ha gustado, comparte!!