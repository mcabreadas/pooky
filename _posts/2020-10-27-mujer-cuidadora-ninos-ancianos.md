---
author: maria
date: '2020-10-27T08:19:29+02:00'
image: /images/posts/mujer-cuidadora/p-dando-de-comer.jpg
layout: post
tags:
- invitado
- mujer
title: La mujer cuidadora
---

> Hoy en la sección "Merendando con..." de este blog, que da voz a las madres, y en la que te animo a [participar aquí](https://madrescabreadas.com/2020/10/16/comunidad-madres/), tenemos como invitada a [Mª José Galiana](https://www.instagram.com/galiana_enfermera/), bi madre de dos preciosas piezas de 14 y 7 años, Dra en Enfermería, enfermera del centro de salud del barrio del Carmen y profesora en la facultad de Enfermería de la universidad de Murcia. Ella se considera madre cabreada y malamadre a partes iguales :).

![Galiana enfermera](/images/posts/mujer-cuidadora/galiana.jpg)
> 
> María José nos va a hablar sobre los cuidados, esos que las madres sabemos hacer tan bien, pero que no sabemos si por ciencia infusa, por instinto o porque es lo que se espera de nosotras... Nos plantea una reflexión muy interesante y documentada sobre el papel de la mujer en los cuidados, su origen histórico, y el importante e invisible papel de éstos en la sociedad.
> 
> El artículo es largo y merece la pena disfrutarlo con un  buen café o chocolate caliente en la mano para reflexionar sobre este tema tan interesante del que esperamos tu opinión.> 
> 
> Os dejo con ella: 

1. <a href="#Redescubrí el cuidado cuando fui madre"> Redescubrí el cuidado cuando fui madre</a>
2. <a href="#¿Por qué se presupone que por ser mujeres debemos saber cuidar?"> ¿Por qué se presupone que por ser mujeres debemos saber cuidar?</a>
3. <a href="#La pandemia apuesto en valor el trabajo de los cuidados"> La pandemia apuesto en valor el trabajo de los cuidados</a>
4. <a href="#¿El cuidado familiar es insustituible?"> ¿El cuidado familiar es insustituible?</a>
5. <a href="#Amor y cuidado como cimientos de la sociedad"> Amor y cuidado como cimientos de la sociedad</a>
6. <a href="#¿Por qué el cuidado va ligado a la condición de mujer?"> ¿Por qué el cuidado va ligado a la condición de mujer?</a>
7. <a href="#Es necesario visibilizar los cuidados"> Es necesario visibilizar los cuidados</a>
8. <a href="#Recomendaciones para reflexionar"> Recomendaciones para reflexionar</a> 

Cuando María de Madres Cabreadas se puso en contacto conmigo para que escribiera sobre la mujer como cuidadora lo primero que vino a mi mente fue mi profesión ya que dentro de la Disciplina de Enfermería se estudia el cuidado de las personas en todas las situaciones de salud, enfermedad y etapa vital. El origen de los cuidados está íntimamente relacionado con la evolución de la especie humana, y es precisamente ahí, en el principio de todo donde surge la “mujer cuidadora”. 

Cuando decidí estudiar Enfermería lo hice sin saber muy bien dónde me metía ya que la imagen de la enfermera como “ayudante del médico” estaba muy arraigada, y yo no quería ser ayudante de nadie… pero ya en la carrera vi cómo las enfermeras tenemos un campo de estudio propio e independiente: “el cuidado”, y ahí me enamoré de la ciencia enfermera, por lo que decidí seguir formándome para profundizar en el ámbito de los cuidados, primero con un Máster en Cuidados de Enfermería y posteriormente con los estudios de Doctorado en Cultura de los cuidados. 

![enfermera vintage](/images/posts/mujer-cuidadora/enfermera-tradicional.jpg)

## <a name="Redescubrí el cuidado cuando fui madre">Redescubrí el cuidado cuando fui madre</a>
 
Por otro lado, el hito que hizo que entendiera en primera persona los entresijos y la esencia del cuidado fue cuando nació mi hija  hace 14 años. Recuerdo muy bien ese momento, ya que cuando te conviertes en madre y ponen en tus brazos a esa criaturita indefensa, de pronto todo el mundo presupone que sabes lo que tienes que hacer porque eres “su madre” porque “eres mujer”, se da implícito atribuir el cuidado al género femenino, es una construcción social que da por hecho que como eres mujer, sabes y debes cuidar de tu descendencia, y además se considera imperativo de tu sexo el que lo hagas,  y además se te exige que sepas reaccionar ante cualquier cosa que les pase a tus hijos, así sin más, porque eres su madre, porque eres mujer. ¿Pero de dónde surge todo esto?

> de pronto todo el mundo presupone que sabes lo que tienes que hacer porque eres “su madre” porque “eres mujer”

## <a name="¿Por qué se presupone que por ser mujeres debemos saber cuidar?">¿Por qué se presupone que por ser mujeres debemos saber cuidar?</a> 

El cuidado constituye un hecho histórico y es una constante que  existe  desde  el  principio  de  la  Humanidad. Definir el concepto de cuidado implica  conocer  previamente  lo  que  significa  cuidar.  Se  trata  de una  palabra  ampliamente  utilizada  y  aplicada  a  campos  muy diversos;    así    decimos    que    cuidamos    cuando    realizamos actividades  tales  como  encargarnos  de  las  plantas,  ocuparnos  de un  bebé  o  aplicar  técnicas  que  mejoran  nuestra  apariencia  física. 

El cuidado es entendido como el conjunto de todas aquellas actividades  humanas  físicas,  mentales  y  emocionales  dirigidas  a mantener  la  salud  y  el  bienestar  del  individuo  y/o  comunidad, pero el cuidado ha estado desde siempre tan ligado al ámbito de lo privado y familiar que ha permanecido invisible a los ojos de la sociedad y sólo somos conscientes de su importancia cuando falta. 

> el cuidado ha permanecido invisible a los ojos de la sociedad y sólo somos conscientes de su importancia cuando falta. 

Los   cuidados, han  existido  siempre,  pero  su  visibilidad  social  no  ha ido  paralela  a  la  relevancia  de los  mismos. Además como comenta José Siles profesor de Historia de la Enfermería en la universidad de Alicante: “Las enfermeras por antonomasia, son las  herederas del  rol  biológico  constituido  por  el  arte  de  cuidar enfermos,  niños,  ancianos  y  heridos.  Por  ese  motivo su  regulación  como  profesión  se  demoró  tanto  como el   resto   de   las   actividades   fundamentadas   en  la división sexual del trabajo”. Se considera a la madre como la primera mujer cuidadora, y a los cuidados domésticos como precursores de los cuidados profesionales, de donde surge la Disciplina enfermera.

> Todo lo privado se ha devaluado, se ha quedado en la “oscuridad” de lo doméstico y no se valora igual que aquellos trabajos de cuidados realizados en lo público

Tenemos por tanto por un lado  la división  sexual  del  trabajo  y  por  otro  la  dicotomía  entre  la  esfera pública  y  la  privada,  como  bien  se  muestra  en  los  estudios feministas  y sociológicos. Todo lo privado se ha devaluado, se ha quedado en la “oscuridad” de lo doméstico y no se valora igual que aquellos trabajos de cuidados realizados en lo público. ¿No pagamos lo que sea porque nuestros hijos estén bien cuidados en una escuela infantil? ¿No buscamos a la mejor cuidadora/maestra para ellos? Pero entonces por qué no se pone valor en lo hecho en casa… 

![Galiana enfermera](/images/posts/mujer-cuidadora/bebe-gateando.jpg)

## <a name="La pandemia apuesto en valor el trabajo de los cuidados">La pandemia apuesto en valor el trabajo de los cuidados</a> 

¿Qué ha pasado en esta época de Pandemia? Pues que las familias y sobretodo las madres hemos tenido que asumir aún más cuidados, ya no solo los básicos de alimentación, higiene, protección… sino asegurar la educación de nuestros hijos y dar continuidad a lo que no podían hacer en el colegio porque estos estaban cerrados.  Y esa labor invisible ¿Quién la paga? ¿Qué valor tiene en el mercado? ¿Cuánto vale que una mujer tenga que renunciar a su puesto de trabajo, carrera y horario para cuidar de sus hijos?

> Y esa labor invisible ¿Quién la paga? ¿Qué valor tiene en el mercado? ¿Cuánto vale que una mujer tenga que renunciar a su puesto de trabajo, carrera y horario para cuidar de sus hijos?

Uno de los libros que leí para mi Tesis “La mercantilización de la vida íntima” de una socióloga Hochschild, me marcó en ese sentido porque decía que contrariamente  a  lo  que pudiera  esperarse,  que  podría  ser  que  el  cuidado  adquirido  en  el mercado   fuera   profesional, de  mayor  calidad  y  mejor. Al delegarse   las   funciones   familiares   a   favor   del   movimiento mercantil,  y  al  producirse  el traspaso  del  trabajo  doméstico  y del  cuidado  a  la  esfera  del  mercado,  no  se  ha  traducido  en  una profesionalización  de  esta  ocupación. La familia "artesanal", está dando paso a una familia postindustrial,  en  las  que  las  tareas  ya  no  son  desempeñadas  por madres o abuelos, sino por "especialistas externos" como niñeras, enfermeras, psicólogos,    cuidadoras    contratadas,    e    incluso, animadores de fiestas de cumpleaños. 

> el traspaso  del  trabajo  doméstico  y del  cuidado  a  la  esfera  del  mercado,  no  se  ha  traducido  en  una profesionalización  de  esta  ocupación

Yo creía que el cuidado de los niños era más valorado, porque como he dicho antes “no dejamos a los niños con cualquiera”, buscamos a la persona que consideramos “la mejor”, pero se ha visto en esta dura situación que nos ha hecho vivir el Covid, que no teníamos opciones, que la mejor opción éramos nosotras, no había otro lugar dónde nuestros hijos fueran cuidados fuera del hogar, y si teníamos la suerte de compartir los cuidados con nuestra pareja se hacía más llevadero, pero no existían “profesionales del cuidado” de niños a las que pudiéramos acudir fuera de las escuelas infantiles. 

> en esta dura situación que nos ha hecho vivir el Covid... la mejor opción éramos nosotras ...no existían “profesionales del cuidado” de niños a las que pudiéramos acudir fuera de las escuelas infantiles. 


## <a name="¿El cuidado familiar es insustituible?">¿El cuidado familiar es insustituible?</a> 

El cuidado familiar y la mujer cuidadora se queda sin sustitutos, no es un bien de consumo, no tiene valor social. Y si eso ha pasado con los niños que son un “BIEN PRECIADO” en la sociedad, que son el futuro, y que cuidar de ellos se considera en términos teóricos importante, imaginad qué estará ocurriendo con el cuidado de ancianos… 

> la mujer cuidadora se queda sin sustitutos, no es un bien de consumo, no tiene valor social

No lo imaginéis, lo hemos vivido en primera plana con todo lo ocurrido en las residencias. A  pesar  de  que  el  cuidado de personas mayores  sigue  siendo objeto   de   preocupación   y   de   estudio,   en   la   práctica   se   ha convertido en un trabajo “del que es preciso salir” y que en muchos casos es asumido por personas de baja cualificación, y como describo en mi tesis doctoral, es más invisible cuando lo asumen cuidadoras inmigrantes.  

![anciana mirando un movil y su cuidadora](/images/posts/mujer-cuidadora/anciana.jpg)
 

## <a name="Amor y cuidado como cimientos de la sociedad">Amor y cuidado como cimientos de la sociedad</a>
El amor  y  el  cuidado,  son los cimientos  de  la  vida  social,  y hoy suscitan   grandes   interrogantes   en   el   núcleo   familiar. 
¿Cómo cuido? 

¿De qué apoyos dispongo? 

¿Cómo logro conciliar? 

Lo curioso es que el  término  cuidador  es  un  constructo  socio-político, ya que en ciertas lenguas no existe la equivalencia,  y en otras el vocablo  es  nuevo. Hace  20  años,  en España,  esta  palabra  no  se  empleaba  aunque  la actividad sin duda existía: en las familias tradicionales, la mujer se encargaba de los niños y además se  designaba  a  una  hija  para  que  se  ocupara  de  los mayores.  La  herencia  y  el  sexo  eran  las  pautas  que establecían  el  papel  de la cuidadora.   

## <a name="¿Por qué el cuidado va ligado a la condición de mujer?">¿Por qué el cuidado va ligado a la condición de mujer?</a>

Hablamos de mujer cuidadora porque el  perfil  sociodemográfico  de quien asume los cuidados   es  un  reflejo de la estructura de la familia tradicional, que inscribe a la mujer la  responsabilidad  de  los  cuidados  dentro  del  hogar.  En esa familia tradicional los roles de género están muy marcados, y estas  cuidadoras  asumirán  el  cuidado de sus hijos y después de sus  maridos o padres, realizando todo tipo de  actividades de la vida  diaria  o  cuidado  personal  que  no  sean  capaz  de  realizar  las personas  que cuiden.  

![pies de bebé](/images/posts/mujer-cuidadora/pies-bebe.jpg)

## <a name="Es necesario visibilizar los cuidados">Es necesario visibilizar los cuidados</a>

Si bien sabemos que el cuidado  es  cambiante,  y se  adapta  a  los tiempos y situaciones, pero lejos de mejorar se sigue relegando al ámbito de lo privado, invisibilizando las circunstancias que lo rodean y como siempre digo, no existe un cuidado y una cuidadora. Los cuidados y las cuidadoras son cada uno único y con unas necesidades diferentes. 

> lejos de mejorar se sigue relegando al ámbito de lo privado, invisibilizando las circunstancias que lo rodean

Por eso es importante sacar a la luz y hablar de cómo hacemos las mujeres para cuidar, de cómo nos intentamos adaptar a las diferentes circunstancias, hablar de donde sacamos los apoyos, y cómo logramos llevarlo todo hacia delante a veces solas, o con nuestras parejas, y a veces con el apoyo de otras mujeres. 

Es importante hablar de cómo se tejen las redes de cuidados, del apoyo social, de la importancia de que el cuidado de niños y ancianos sea visible y que se luche porque sea el mejor que se pueda dar, porque dice mucho de una sociedad que se preocupa de sus niños y de sus mayores. 

**‌**¿Qué pensáis?

 Dejo muchas cuestiones en el aire porque así generamos un debate y nos ayuda a reflexionar sobre nosotras como cuidadoras 
 ¿Quién nos ha cuidado? 
 
 ¿A quién cuidamos? 
 
 ¿Cómo queremos que sea nuestro cuidado, el que damos y el que recibimos?
 
>  Si te ha gustado, comparte para llegar a más gente, y des ese modo ayudarás a que este blog siga compartiendo nuestras historias.
 
## <a name="Recomendaciones para reflexionar">Recomendaciones para reflexionar</a>

Os dejo recomendaciones de películas y libros para pensar. 

Un abrazo 

***Películas**:* 
- *“La Habitación de Marvin” (1986)*

- *“Quédate a mi lado” (1998)*

- *“Intocable”(2011)*

- *“Siempre Alice” (2014)*

- *“Los principios del cuidado” (2016)*

- *“El Hijo de la Novia” (2001)*

***Libros sobre el cuidado y maternidad**:* 

- *“Feminismo para principiantes” (2018) [Nuria Varela](https://twitter.com/nuriavarela?lang=es)*

- *“Mamá desobediente”  [Esther Vivas](https://twitter.com/esthervivas?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor) (2020)*

- *Todos los de [Lucía Galán](https://www.luciamipediatra.com/mis-libros/) (@luciamipediatra)*

- *Todos los de [Carlos González](http://www.carlosgonzalezpediatra.com/libros/)*

- *Todos los de [Álvaro Bilbao](https://alvarobilbao.com/libros)*

***Novelas y libros de entretenimiento**:*

- *“39 semanas” [Esther Gili](http://www.esthergili.com/p/publicaciones.html) (2016)*

- *“Como ser una mama cruasán: una nueva forma de educar con sentido común” [Pamela Druckerman](https://pameladruckerman.com/books/) (2012)* 


*Photo by National Cancer Institute on unsplash enfermera tradicional*

*Photo by Alex Pasarelu on Unsplash pies bebé*

*Photo by Tanaphong Toochinda on unsplash madre dando de comer*

*Photo by Jordan Rowland on Unsplash niño gateando*

*Photo by Georg Arthur Pflueger on unsplash anciana*

*Photo by krakenimages on Unsplash familia numerosa*