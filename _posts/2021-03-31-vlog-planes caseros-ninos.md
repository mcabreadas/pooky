---
author: maria
date: '2021-03-31T10:13:29+02:00'
image: /images/posts/vlog/p-planes-semana-santa.png
layout: post
tags:
- vlog
- planninos
title: Vlog ideas de planes caseros con niños para Semana Santa
---

Esta semana hablo en la tele sobre planes caseros con niños para esta Semana Santa que seguro que os salvan alguna tarde que otra en la que años atrás estaríamos de procesiones.

Nazarenos con rollos de papel higiénico, torrijas, pintar huevos de pascua, hacer juegos por videollamada son sólo algunas de las ideas que os proponemos para estos días de vacaciones con los niños.

<iframe width="560" height="315" src="https://www.youtube.com/embed/NGkq0gRovPk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<a href="https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c" class='c-btn c-btn--active c-btn--small'>Consigue gratis el e-book de poesía infantil</a>

Comparte para que llegue a mucha gente y suscríbete para no perderte nada!