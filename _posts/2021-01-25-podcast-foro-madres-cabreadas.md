---
layout: post
slug: podcast-foro-madres-cabreadas
title: "Programa La Máquina en Radio Metrópolis FM: presentación Foro Madres
  Cabreadas"
date: 2021-01-25T10:10:29+02:00
image: /images/posts/podcast/p-maquina.jpg
author: maria
tags:
  - revistadeprensa
---
Siempre es un placer compartir con buena gente mis proyectos. Cuando me llamó Pepe Conesa interesado en que le hablara sobre nuestro nuevo Foro de maternidad me encantó que alguien que no tiene hijos pudiera pensar que mi proyecto era interesante. 

Al hablar con él durante la entrevista me hizo sentir que la crianza de los hijos, cómo compaginarla con el trabajo, y los problemas con los que nos encontramos las madres día a día para llegar a todo es algo que importa a la sociedad y cada vez más la gente está tomando conciencia de ello.

<a href="https://metropolisfm.es/podcast/la-maquina-con-carmen-murcia-ukele-80s-y-madres-cabreadas/" class='c-btn c-btn--active c-btn--small'>Escuchar el podcast</a>

Tras la comparecencia en directo del consejero de Sanidad en la que dimitía de su cargo, entraron vía telefónica Eva Latonda, Ana Alcázar y Pilar Fonseca, integrantes de la comunidad Madres Cabreadas y emprendedoras, que nos contaron su forma de vivir la familia y la crianza de sus hijos, además de los proyectos en los que están inmersas: [Con Causa](http://www.concausa.com) (Eva Latonda), [Kihana Decora](https://www.kihanadecora.com) (Ana Alcázar) y el libro [Mamá Full Tim](https://mamasfulltime.com)e (Pilar Fonseca).

![](/images/posts/podcast/canva-maquina.jpg)

Si te ha gustado comparte y suscríbete al blog para no perderte nada.