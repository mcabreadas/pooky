---
layout: post
slug: acoso-escolar-protocolo
title: El aislamiento también es acoso escolar
date: 2019-06-14T18:19:29+02:00
image: /images/posts/acoso-escolar-protocolo/p-dibujo-amistad.jpg
author: maria
tags:
  - adolescencia
  - educacion
  - mecabrea
  - 6-a-12
  - 3-a-6
---

El acoso escolar o bullying se cobra alrededor de 200 mil suicidios al año entre jóvenes de entre 14 y 28 años según un informe realizado por la Organización Mundial de la Salud junto a Naciones Unidas.  Según un estudio de la ONG Bullying Sin Fronteras y la ONG británica Beat Bullying, dentro de la Unión Europea, hasta 24 millones de niños y jóvenes al año son víctimas de acoso y maltrato por bullying. Reino Unido es el país más afectado, seguido por Rusia, Irlanda, España e Italia.

Al final del post encontraréis teléfonos y enlaces de interés si estáis sufriendo una situación de este tipo envuelta familia o tenéis alguna duda al respecto.

## Las estadísticas sobre bullying no mienten

El otro día mis hijos recibieron una charla en el colegio a cargo de la Policía local que comenzó con una estadística que los propios niños no creyeron: 1 de cada 5 alumnos sufre acoso escolar.

No sé si es más preocupante el hecho de que existan estas cifras o que no seamos conscientes de ello hasta el punto de pensar que "en mi cole no pasan estas cosas", porque sí que pasan. En todos los coles. Es así. Otra cosa es que no se haga público, que no se hable de ello, salvo en casos muy sonados, porque el acoso escolar no es sólo el que se plasma en las películas del típico abusón que pega a un niño más débil y le roba el almuerzo. Si fuera así siempre sería tan evidente que a nadie le sorprenderían las estadísticas. Pero no. Lamentablemente existen otros tipos de [bullying más silenciosos](https://madrescabreadas.com/2018/02/06/acoso-escolar-aislamiento/) y menos evidentes incluso para los profesores, aunque convivan con los protagonistas 5 horas al día o más.

## Aislamiento social como forma de acoso escolar

Me refiero, por ejemplo al aislamiento social, tema en el que me quiero centrar hoy, y que consiste en dar de lado a una persona, por ejemplo no dejándola que juegue en un grupo, evitando sistemáticamente elegirla cuando se hacen equipos para deportes o para hacer trabajos conjuntos de clase, dejándola como única no invitada en una fiesta cumpleaños, o que se pase los recreos sola en un banco sentada. 

![grupo escolares trabajo](/images/posts/acoso-escolar-protocolo/escolares-haciendo-trabajo.jpg)

Además de lo anterior el acosador/a en estos casos suele impedir activamente que esta persona fragüe cualquier tipo de relación o amistad con otros miembros del grupo, logrando así que quede aislada y con la sensación de que no tiene amigos ni va a ser capaz de tenerlos en la vida.

El daño que sufre la persona acosada es un [daño silencioso](https://madrescabreadas.com/2017/03/26/bullying-acoso-escolar/), muy difícil de detectar si no lo cuenta. Incluso aunque lo cuente, es fácil pensar que es ella la que decide aislarse por su carácter tímido e inseguro, y que prefiere estar sola porque normalmente el acosador/a obviamente no hace nada a la vista de nadie, y usa todo tipo de estrategias para que no se le vea el plumero, llegando a ser incluso una de las personas más populares de la clase a la vista de los alumnos y de los propios profesores, que normalmente los suelen tener en gran estima.

Si lo pensamos, es lógico que sea necesario tener un gran carisma de líder para lograr aislar a un compañero/a porque los demás deben seguirte sin sospechar intenciones ocultas.
Nos encontramos con que la principal dificultad que va a encontrar la persona acosada es que la crean y no le echen la culpa e ella y la manden al sicólogo directamente (cuando lo que la víctima debe de pensar seguramente es que quien debería ponerse en tratamiento primero es el la persona acosadora).

## Primera reacción de los padres ante el bullying

El primer paso cuando detectemos problemas de este tipo es escuchar a nuestro hijo/a sin quitarle importancia a lo que está contando y observar si es un hecho aislado o si se prolonga durante más de 2 semanas porque en este último caso ya tendríamos fundamentadas sospechas para considerarlo como tal.

Es muy importante que tengáis claro que si ha tenido el valor de contároslo no podéis fallarle. No dudéis de él o ella, jamás lo pongáis en entre dicho, y mucho menos en público, porque los demás puede que lo hagan continuamente (lo que no quiere decir que vosotros por vuestra cuenta no indaguéis lo necesario). 

Tened en cuenta que vuestro hijo sólo os tiene a vosotros para defenderlo, porque el colegio es otra  parte diferente implicada en el asunto, y el profesor suele adoptar una actitud de prudencia y árbitro, por lo que quien va a hacer una postura fuerte a favor del acosado sois sus padres.

## ¿Lo que sufre mi hijo es acoso escolar?

![telefono acosoescolar](/images/posts/acoso-escolar-protocolo/cartel-telefono-acoso-escolar.jpg)

Es común tener dudas sobre si es acoso lo que le está pasando a vuestro hijo/a. Por eso, recomiendo que llaméis al teléfono de acoso escolar para que con vuestro diario en mano os lo confirmen. Aunque lo primero que os van a decir si es que así lo consideran, es que iniciéis formalmente el protocolo de coso escolar por escrito porque si no ellos no podrán ayudaros más. 

## Un diario de hechos y fechas

Sería bueno abrir una libreta y hacer un diario detallado con fechas, lugares, horarios y profesores y niños presentes en los distintos episodios que sufra el acosado, y que el niño narrara por escrito sus experiencias, siempre que tenga la edad adecuada par ello. 

Esto es tremendamente útil porque la memoria nos suele fallar cuando pasa el tiempo, y precisamente es el detalle de los hechos lo que dará credibilidad a nuestra versión caso de que se abra un expediente de acoso escolar y tengamos que relatar lo sucedido.

## El protocolo de acoso escolar

Se trata de una formalidad que establece una serie de directrices para todo el personal del centro y cuyo objetivo es investigar si existe o ha existido bullying, adoptar medidas individuales y generales y, caso de que se determine que sí ha habido acoso, se impondrán las sanciones oportunas. 

Cada Comunidad Autónoma tiene un protocolo de acoso escolar diferente, por eso recomiendo que consultéis el que os corresponde, desee modo estaréis informados sobre cada paso del procedimiento que se va a seguir, sobre los derechos que tenéis y tendréis la información suficiente para controlas que los pasos que va dando el colegio son correctos.

El proceso suele tener una serie de garantías para el acosado, como es que siempre que se le entreviste tienen que estar sus padres delante, que no se le pueda someter a "juicios públicos", por ejemplo poner todas las cartas sobre la mesa delante de toda la clase y que se señale al acosado y al acosador ante todos, que se debe llevar el asunto con la máxima discreción posible... 

La aplicación de las medidas de los protocolos de acoso escolar requieren formación específica del profesorado por parte del equipo de orientación del centro, ya que son situaciones muy delicadas y no se pueden improvisar, ya que cualquier paso en falso podría empeorar la situación.

![observatorio convivencia escolar](/images/posts/acoso-escolar-protocolo/cartel-observatorio-convivencia.jpg)

Otra recomendación importante que incluyen estos protocolos es que no se hable directamente entre las familias implicadas, sino que siempre se haga todo a través de los cauces legales, lo que evitará situaciones desagradables.

Tened en cuenta que el hecho de que a nadie le siente bien que le digan que su hijo es un presunto acosador, y de que la familia del acosado suele estar bastante dolida, hacen que no sean los actores ideales para llegar a un entendimiento sino que lo ideal sería que mediara un profesional.

## Expediente de acoso escolar: recomendaciones prácticas

Os recomiendo ir a todas las reuniones siempre mínimo 2 personas de vuestra parte, es decir, nunca vayáis una sola, y pedid copia del acta que se levante.

Por supuesto no firméis nada de lo que no estéis seguros, y no dejéis que pasen muchos días entre actuación y actuación porque es algo que tiene la máxima prioridad.

Consultad agentes externos al centro, por ejemplo en Murcia, el servicio de Juventud del Ayuntamiento, o el Observatorio de Convivencia Escolar, donde sin duda os orientarán. Al final del post os dejo todos los enlaces de interés.

## La actitud del colegio

En cuanto a los colegios, no todos son así, pero los hay reacios a iniciar este tipo de procedimientos porque para ellos supone más papeleo, reuniones y trabajo extra, lo que hace que no estén muy por la labor, por eso se recomienda pedir que se ponga en macha el protocolo de acoso escolar por escrito, presentarlo en Secretaría, quedaros con copia sellada de la presentación, y exigir respuesta por escrito en el plazo que establezca el protocolo. 

Incluso podría darse el caso de que os trataran de disuadir intentándoos convencer de que no hace falta el protocolo, y que mejor lo arreglan por su cuenta, pero desde el Teléfono de Acoso Escolar os recomendarán siempre que abráis el protocolo cuanto antes.

## Una lucha en solitario

Una vez abierto el protocolo comienza una  lucha que puede convertirse en titánica  porque podéis encontraros solos ante el peligro, ya que muchas veces, sobre todo si se trata de un caso de acoso por aislamiento social donde el daño es psíquico, y no es nada evidente el acoso, es posible que os intenten convencer de que todo está en vuestra imaginación y de que incluso vosotros como padres estáis alimentando la sensación de acoso en vuestro hijo. 

Habrá momentos en que dudéis de vosotros mismos y penséis que estáis armando un lío tremendo por nada.

## Cuando las medidas anti acoso no son efectivas

Otra opción ante una situación de acoso es cambiar a la víctima de colegio, ya que es muy difícil que se acuerde expulsar al presunto agresor y, en todo caso, si esto sucediera, sería después de la tramitación del expediente. Esta decisión la tenéis que valorar como padres porque puede que sea la medida más efectiva y práctica para evitar que vuestro hijo siga sufriendo durante más tiempo.

Espero haberos dado algo de luz en un tema que es muy complicado y duro de afrontar, y en el que muchas familias se encuentran desamparadas y perdidas sin saber cómo actuar. Pero lo más importante es que os mostréis seguros ante vuestro hijo/a, le deis todo el cariño y tiempo que sea necesario y demostrarle que os tiene incondicionalmente. Eso jamás lo olvidará.

Si este post te he ayudado y piensas que puede servir de ayuda alguien más, compártelo!

## Enlaces de interés:
[Teléfono Acoso Escolar](https://www.anar.org/anar-telefono-contra-acoso-escolar-ministerio-educacion-fp/)

[Observatorio de convivencia escolar Murcia](https://www.carm.es/web/pagina?IDCONTENIDO=90818&IDTIPO=160&RASTRO=c792$m4001,5316,59685)

[Protocolo de acoso escolar Murcia](https://www.carm.es/web/pagina?IDCONTENIDO=57966&IDTIPO=100&RASTRO=c792$m4001)

[Talleres de habilidades sociales](https://mindup-psicologos.com/grupo-terapeutico/)

[Servicio de juventud Ayuntamiento de Murcia](https://www.informajoven.org)

[Formación para padres y profesores](http://geamurcia.org/escuela-de-padres/)

Comparte!