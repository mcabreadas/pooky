---
layout: post
slug: juguetes-tecnologicos-conectados-internet
title: Peligros de los juguetes conectados a internet. Consejos
date: 2018-12-13T18:19:29+02:00
image: /images/posts/juguetes-tecnologicos-conectados-internet/p_juguetes-tecnologicos-conectados-internet.jpg
author: maria
tumblr_url: https://madrescabreadas.com/post/181073863949/juguetes-tecnologicos-conectados-internet
tags:
  - 6-a-12
  - juguetes
  - tecnologia
---
Nuestros niños y adolescentes piden cada vez más a los Reyes Magos y a Papá Noel juguetes tecnológicos conectados a internet, pero no todos saben usarlos correctamente y de forma segura, ni siquiera la mayoría de los padres estamos al corriente de ciertas normas básicas de utilización de estos dispositivos para proteger a nuestros hijos.

Te dejamos unas [ideas para regalar a jovenes o adolescanetes en este post](https://madrescabreadas.com/2020/11/26/ideas-regalos-jovenes/).

## Los juguetes tecnológicos suscitan dudas a los padres

Seguro que os pasa como a mí y os preguntaréis ¿Qué es un juguete interactivo? ¿Para qué recogen estos juguetes nuestros datos? ¿A qué nos exponemos? ¿Cómo elijo un juguete conectado para mi hijo? ¿Cómo configuro el juguete para que sea más seguro? ¿Cómo les enseño a jugar con más seguridad?

Por eso el [Instituto Nacional de Ciberseguridad (INCIBE)](https://incibe.es/?utm_source=notas%20de%20prensa&utm_medium=mmcc&utm_campaign=Cibercooperantes), entidad dependiente del Ministerio de Economía y Empresa a través de la Secretaría de Estado para el Avance Digital, junto con la [Asociación Española de Fabricantes de Juguetes](https://www.aefj.es) han hecho pública una [“Guía para el uso seguro de los juguetes conectados”](https://www.is4k.es/de-utilidad/recursos/guia-para-el-uso-seguro-de-juguetes-conectados), con el fin de asegurar la diversión de los niños y adolescentes a través de la tecnología sin riesgos. 
![](/images/posts/juguetes-tecnologicos-conectados-internet/tumblr_inline_pjo4m5t7Ne1qfhdaz_540.jpg)

## Recomendaciones para el uso de juguetes conectados a Internet

Es cierto que los juguetes conectados a internet ofrecen nuevas posibilidades de juego, fomentando la interactividad y participación de los niños y niñas, pero siempre es necesario conocer las recomendaciones para evitar riesgos, como:

* contraseñas seguras
* control parental
* criterio para el uso y difusión de imágenes y audios, etc.

La guía que os he enlazado anteriormente, y que os recomiendo que os descarguéis y laleáis con atención, da respuesta a estas cuestiones y otras muchas, como el papel de los datos personales que utilizan estos juegos y cómo podemos asegurar la privacidad. También proporciona consejos para los padres a la hora de elegir un juguete conectado, como comprar con responsabilidad, leer las especificaciones técnicas e informarse consultando la web del fabricante u otras páginas en las que expertos y usuarios cuenten su experiencia y su opinión. 

## Cómo configurar un juguete conectado a Internet

Además, se detallan los pasos básicos que se han de seguir para configurar correctamente un juguete conectado. Para ello, es vital tener en cuenta herramientas específicas de control parental y la protección de los datos personales, eliminando los registros de mensajes. 

 Así, INCIBE, a través de [Internet Segura for Kids (IS4K)](https://www.is4k.es/regalos-y-juguetes-conectados) invita a fabricantes, vendedores de juguetes, padres y educadores a conocer esta guía para ser parte activa en el futuro de la educación y la seguridad de los menores en la era digital. 

## La responsabilidad de los padres de estar formados

 Además, la misma propone actitudes que se deben fomentar en los menores cuando tienen al alcance esta tecnología, colaborando con la educación de los niños y adolescentes en este sentido, aunque ya sabemos que la responsabilidad es de los padres y que ya no nos vale la excusa de que “es que yo no entiendo de esas cosas” porque la web IS4K te está dando la herramienta idónea para aprender como padre o madre a proteger a tus hijos de forma gratuita y continuada.

Ponte al día por ellos!

¿Tus hijos han pedido juguetes tecnológicos para Navidad?