---
layout: post
slug: cuentos-navidad
title: 12 cuentos clásicos para que los niños lean esta Navidad
date: 2020-12-07T17:19:29+02:00
image: /images/uploads/depositphotos_406148704_xl.jpg
author: belem
tags:
  - planninos
  - navidad
  - libros
---
<!-- BEGIN PROPELBON -->
<a href="https://juguettos.boost.propelbon.com/ts/94359/tsc?typ=r&amc=con.propelbon.499930.510438.CRTz1GCzu0f" target="_blank" rel="sponsored">
<img src="https://juguettos.boost.propelbon.com/ts/94359/tsv?amc=con.propelbon.499930.510438.CRTz1GCzu0f" border=0 width="468" height="60" alt="" />
</a>

<!-- END PROPELBON -->

Esta Navidad será completamente distinta de cualquier otra Navidad que recordemos. Para terminar este año tan inusual tendremos una Navidad y año nuevo muy diferentes de lo que seguramente teníamos planeado antes que llegara el COVID-19. Sin embargo, he aprendido que lo distinto no es sinónimo de malo o arruinado.

Decidí quitarle esa connotación a la palabra “diferente” y eso me ayudó a sobrellevar mucho mejor el reto. Ahora, desde que vi que las cosas serían diferentes de ahora en adelante, comencé a hacerme a la idea de que Navidad sería distinta también. Personalmente, tanto para mí como para mi esposo, el **mantener la ilusión de nuestro hijo durante las fiestas ha sido prioridad**. Como parte del plan, hemos horneado y decorado galletas, hecho adornos navideños, escrito cartas para aquellos a quienes amamos pero no podremos ver pronto, decorado la casa y visto películas navideñas. También hemos estado leyendo **cuentos clásicos de Navidad para niños** antes de dormir.

Leer **cuentos de Navidad** son una excelente opción para prepararnos para las fechas sin comprometer nuestra salud ni el espíritu navideño. Se trata de **lecturas navideñas para primaria**, pero también para los más pequeños e incluso para los mayores. De hecho, son para toda la familia.  

A continuación, un recopilado de excelentes **cuentos de Navidad para niños. 12** libros, cuentos e historias que nos ayudarán a comprender o recordar, el verdadero significado de la Navidad. 

## 12 cuentos de Navidad para niños

## Cómo el Grinch robó la Navidad

Lo más seguro es que hayamos visto alguna de las dos películas de este famoso personaje, pero apuesto a que muy pocas personas han leído este cuento. Honestamente nosotros tampoco lo habíamos leído a pesar de ser una opción muy obvia.

Sin embargo, y a pesar de ya haber visto las dos películas mencionadas anteriormente, tengo que confesar que **mi hijo y yo disfrutamos este libro como pocos**. Del famoso autor Dr. Seuss, estoy dispuesta a apostar que a todos en casa les encantará esta navideña historia. 

[![](/images/uploads/grinch.jpg)](https://www.amazon.es/dp/B016X1NVO6/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=4c5e3e7a1a66d63e51b2227d5c41867a&creativeASIN=B016X1NVO6) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B016X1NVO6/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=4c5e3e7a1a66d63e51b2227d5c41867a&creativeASIN=B016X1NVO6) (enlace afiliado)

## La pequeña cerillera (o La Niña de los Fósforos)

Del reconocido autor danés de cuentos infantiles Hans Christian Andersen. Este triste cuento navideño podría no ser el mejor si sus niños son muy pequeños o sensibles. Sin embargo, es una excelente historia con un **conmovedor mensaje que en estos tiempos de deshumanización** bien nos vendría recordar, especialmente en estas épocas navideñas.

El mensaje de este este cuento es que la compasión es un valor que no debemos perder en ningún momento y que ayudar al prójimo siempre que se pueda es una buena idea.

[![](/images/uploads/cerillera.jpg)](https://www.amazon.es/Peque%C3%B1a-Cerillera-Hans-Christian-Andersen-ebook/dp/B09L15LV1B/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2VU9EYN5TYFQE&keywords=La+peque%C3%B1a+cerillera&qid=1643045224&s=digital-text&sprefix=la+peque%C3%B1a+cerillera%2Cdigital-text%2C1566&sr=1-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Peque%C3%B1a-Cerillera-Hans-Christian-Andersen-ebook/dp/B09L15LV1B/ref=sr_1_2?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2VU9EYN5TYFQE&keywords=La+peque%C3%B1a+cerillera&qid=1643045224&s=digital-text&sprefix=la+peque%C3%B1a+cerillera%2Cdigital-text%2C1566&sr=1-2&madrescabread-21) (enlace afiliado)

## El Cascanueces y el rey de los ratones

Esta famosa historia navideña no solo es un famoso ballet, sino que originalmente se trata de un cuento obra del alemán Ernst Theodor Amadeus Hoffmann. Lo más probable es que tengamos al menos una idea general del argumento de esta historia, sin embargo, **el cuento es muy lindo y entretenido para leer**.

Definitivamente ideal para esta temporada, y más aún si al leerlo nos acompañamos de la hermosa composición musical de Tchaikovski. 

[![](/images/uploads/cascanueces.jpg)](https://www.amazon.es/dp/8469621394/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=bc78a58a52e1867f806a3cb9d984b4fd&creativeASIN=8469621394) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8469621394/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=bc78a58a52e1867f806a3cb9d984b4fd&creativeASIN=8469621394) (enlace afiliado)

## El soldadito de plomo

Otro clásico de Hans Christian Anderson. Aunque menos melancólico que “La pequeña cerillera”, este también es un emotivo cuento. Con **temas como el amor, el coraje y la fuerza de voluntad**, el soldadito de plomo es el cuento ideal para leer y prepararnos para la llegada de la Navidad. 

[![](/images/uploads/soldadito.jpg)](https://www.amazon.es/dp/8460680630/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=7fbb249da8ff2890c5198ef7fff32782&creativeASIN=8460680630) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8460680630/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=7fbb249da8ff2890c5198ef7fff32782&creativeASIN=8460680630) (enlace afiliado)

## El expreso polar

Otra película que antes fue un libro. Esta clásica historia navideña de Chris van Allsburg, tiene lugar en Nochebuena, cuando un niño despierta con el sonido de un tren. Dentro de este tren, llamado el Expreso Polar, él y sus amigos emprenden un viaje al polo norte, gracias al cual reafirman su fé en la Navidad. **Es una bonita historia para aquellos niños mayores que comienzan a dejar de creer en la Navidad**.

Una hermosa historia para reafirmar el espíritu navideño en nosotros. PS, el libro es una excelente opción para aquellos de nosotros que no fuimos fans de la famosa película, adaptación de este cuento. 

[![](/images/uploads/expres-polar.jpg)](https://www.amazon.es/dp/980257046X/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=430e8d9091c8fd97933e195929f9d0ab&creativeASIN=980257046X) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/980257046X/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=430e8d9091c8fd97933e195929f9d0ab&creativeASIN=980257046X) (enlace afiliado)

## Canción de Navidad

El primer cuento (o historia corta) del rey de las historias de Navidad, Charles Dickens. Esta es la historia del viejo Ebenezer Scrooge, un hombre avaro y malvado que, en víspera de Navidad, es visitado por tres fantasmas que lo ayudarán a comprender el verdadero significado de ésta: una **época de unión familiar y de valorar** que los mejores regalos no son cosas (o sea, de dejar de lado el materialismo).

Este cuento cuenta con varias adaptaciones en forma de caricaturas, series o películas, sin embargo, el cuento transmite una emotividad que hasta el momento no he atestiguado en ninguna adaptación televisiva. 

[![](/images/uploads/cancion.jpg)](https://www.amazon.es/dp/8468209791/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=fc6849c29c9f07beedc0abb3dbde400a&creativeASIN=8468209791) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8468209791/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=fc6849c29c9f07beedc0abb3dbde400a&creativeASIN=8468209791) (enlace afiliado)

## Las crónicas de Narnia: el león, la bruja y el ropero

Este es un libro un poquito más largo que los mencionados anteriormente, sin embargo, considero que vale muchísimo la pena darse un tiempo para leerlo. No es necesario leer los otros libros de la saga (aunque más adelante pueden hacerlo, por supuesto), este cuento de C.S Lewis no necesita contexto.

Esta historia también nos ayudará a repasar la ya conocida **lección de valorar a la familia y a los amigos** por encima de los bienes materiales, al mismo tiempo que nos enseña que el bien siempre triunfará.

[![](/images/uploads/narnia.jpg)](https://www.amazon.es/dp/8408099035/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=ad94e6efef9b37469b678f96e66d811e&creativeASIN=8408099035) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8408099035/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=ad94e6efef9b37469b678f96e66d811e&creativeASIN=8408099035) (enlace afiliado)

## Olivia recibe la Navidad

De la saga de esta famosa cerdita, esta historia puede **ayudar a los más pequeños de casa a comprender como funciona la Navidad**. Desde los preparativos, las decoraciones y hasta el festejo, Olivia solo tiene una interrogante; ¿cuánto más falta para que llegue Santa?

Este libro de Ian Falconer es corto pero adorable. Una excelente opción para aquellos pequeños impacientes. 

[![](/images/uploads/olivia.jpg)](https://www.amazon.es/dp/9681685644/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=c38f5dd7be097e39c9c701346296952e&creativeASIN=9681685644) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/9681685644/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=c38f5dd7be097e39c9c701346296952e&creativeASIN=9681685644) (enlace afiliado)

## Manual de la Navidad

De  Ana Alonso, este libro en realidad no es un cuento, sino, como su nombre lo indica, un **manual de preparación para la Navidad**. Con proyectos, manualidades, recetas, canciones e historias acerca del origen de las más famosas tradiciones de esta época.

Este libro me gustó para ir leyendo en familia y ocupar nuestro tiempo en casa preparándonos para celebrar la Navidad.

[![](/images/uploads/manual-de-la-navidad.jpg)](https://www.amazon.com/-/es/Ana-Alonso/dp/846786172X&madrescabread-21)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.com/-/es/Ana-Alonso/dp/846786172X&madrescabread-21)

## Cuentos de Navidad

De nuevo, Charles Dickens. Este recopilatorio de historias cortas referentes a esta anhelada época es un clásico que todos debemos leer. Todas las historias **transportan el mensaje de la importancia de la unión familiar** y de que lo material no lo es todo, como podría llegar a parecer a veces.

En esta recopilación podemos encontrar Canción de Navidad, pues forma parte de esta colección. Otros cuentos que están incluidos son El grillo del hogar, La batalla de la vida y Las campanas. 

[![](/images/uploads/cuento-de-navidad.jpg)](https://www.amazon.es/dp/843167167X/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=394e9291888c06de7c6bca55f85134d3&creativeASIN=843167167X) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/843167167X/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=394e9291888c06de7c6bca55f85134d3&creativeASIN=843167167X) (enlace afiliado)

## La mejor Navidad

Este no es un clásico, pero si que es una historia hermosa. Del autor taiwanés Chih-Yuan Chen, este bonito cuento ayudará a nuestros pequeños a comprender que a pesar que a veces las cosas no salen como queremos (cosas de adultos; ya saben, dinero, negocios, una pandemia mundial…) **durante la Navidad lo único que importa es que estemos juntos** y que nos tengamos los unos a los otros.

[![](/images/uploads/la-mejor-navidad.jpg)](https://www.amazon.es/dp/8496473503/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=95ac9be617c98c3411297502fb84aa49&creativeASIN=8496473503) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8496473503/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=95ac9be617c98c3411297502fb84aa49&creativeASIN=8496473503) (enlace afiliado)

## Cartas de Papá Noel

Del autor de El Señor de los Anillos, llega un hermoso y emotivo libro. El argumento es el siguiente; cada diciembre, los hijos de J.R.R. Tolkien recibían cartas de Papá Noel y otros habitantes del polo norte. Junto con las cartas, recibían dibujos, ilustraciones y muchas anécdotas de la vida cotidiana es esos rumbos.

Es una especie de anecdotario muy gracioso e interesante, pero sobre todo emotivo para nosotros, los que somos padres, pues es un **testimonio de lo que somos capaces de hacer con tal de ver la ilusión de la Navidad en los ojos de nuestros hijos.** 

[![](/images/uploads/cartas-de-papa-noel.jpg)](https://www.amazon.es/dp/8445008919/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=742f1718d13bf5101b80e8966e4e4a41&creativeASIN=8445008919) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8445008919/ref=as_sl_pc_qf_sp_asin_til?tag=madrescabread-21&linkCode=w00&linkId=742f1718d13bf5101b80e8966e4e4a41&creativeASIN=8445008919) (enlace afiliado)

Ahí están, algunos de los que, a mi parecer, son los mejores **cuentos clásicos de Navidad para niños** de todas las edades. Yo sé que lo más probable es que no podamos festejar estas épocas como lo habíamos hecho en temporadas anteriores. Sin embargo, esto no quiere decir que todo esté arruinado.

Puede ser el nacimiento de una nueva tradición familiar en la que nos juntamos todos antes de dormir y leemos **cuentos navideños para infantil,** pero que todos disfrutaremos muchísimo y que nos ayudarán a prepararnos para esta fiesta.

Si te ha gustado comparte y suscríbete al blog para no perderte contenido interesante!

*Foto de portada by S&B Vonlanthen*