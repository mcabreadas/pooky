---
layout: post
slug: comprar-vestido-comunion-murcia
title: Los 7 consejos que te ayudarán a comprar el vestido de Comunión sin dar
  mil vueltas
date: 2020-12-09T19:00:29+02:00
image: /images/posts/comprar-vestido-comunion/p-perchero.jpg
author: .
tags:
  - 6-a-12
  - comunion
---
Este curso hace la primera Comunión el peque de la casa, así que es hora de ponernos manos a la obra y empezar a organizar los preparativos de la primera comunión con tiempo para que no nos pille el toro, y lo primero que se me viene a la cabeza es comprar el vestido de comunión. 

Te dejo tambien estas [ideas de regalos de primera comunion aqui](https://madrescabreadas.com/2023/02/24/regalos-de-comunion-2023/).

## Cómo elegir el vestido perfecto de comunión

Comprar el vestido de Comunión de niña o el traje de Comunión de niño, según lo que os toque, es algo con lo que andamos algo perdidas porque no sabemos cuándo encargarlo, dónde comprarlo, o incluso hay quien se plantea si comprarlo o no dado lo incierto de la situación y la posibilidad de que se vuelvan a suspender las ceremonias religiosas y celebraciones. 

Por eso he querido informarme de primera mano con una experta en el sector de la moda de Comunión, Montse Navarro, propietaria del comercio local [Momo Kids](http://momo.com.es) de Murcia, con 20 años de experiencia, para daros unos consejos prácticos sobre lo que debemos tener en cuenta a la hora de comprar el traje de Comunión de nuestros hijos en plena pandemia de coronavirus, una situación atípica totalmente y que hace que tengamos que adaptarnos a un posible cambio de fecha incluso talla del niño, y por ello debemos comprar con todas las garantías y en una tienda de confianza.

![escaparate-tienda-comunion](/images/posts/comprar-vestido-comunion/escaparate.jpg)

### 1-Preselección de tiendas para comprar el traje de comunión

Ahorra tiempo haciendo una selección de las tiendas a visitar según los gustos de tu hijo o hija y los tuyos para no volveros locos yendo a mil sitios. Elige una o dos tiendas que se adapten a vuestros gustos y presupuesto antes de ir de peregrinación con la abuela de sitio en sitio para al final quedaros con el último que habéis visto porque se os mezclan todos en la cabeza.

### 2-Pide cita previa para comprar el traje de comunión

Ve siempre con cita previa, y reserva 1 hora de tu tiempo para elegir con calma y dando el protagonismo que merece al momento y asegúrate de que han recibido toda la mercancía antes de tu visita a la tienda.

![probador tienda comunión cancán](/images/posts/comprar-vestido-comunion/probador.jpg)

### 3-Vestidos para los acompañantes en la comunión

Lo ideal sería que la tienda tuviera también ropa para vestir para esa ocasión especial a los hermanos, incluso a la madre, comenta Montse, por experiencia propia de su tienda.

### 4-Medidas del traje de comunión

No encargues un traje más grande por si acaso, sino que asegúrate de que dan el servicio para adaptarlo unos 20 días antes de la Comunión, y de que disponen de tejido de sobra para agrandarlo caso de que se pospusiera la fecha de la Comunión por cualquier causa.

### 5-Déjalos elegir su traje de comunión

Deja elegir a tu hijo. Si quieres puedes hacer una preselección tú antes para cribar, pero déjalo que se sienta protagonista de su día.

![niña asomada a ventana con vestido de comunión romántico mon air](/images/posts/comprar-vestido-comunion/ventana.jpg)

### 6-Ten un presupuesto claro

Infórmate de cuánto vale un vestido de primera comunión porque los hay de todos los precios, y elige la tienda en función del mismo. 
Por ejemplo, en Momo Kids Los trajes de Comunión de niño de Outlet (otras temporadas) van desde 99€ a 580€, y los trajes de Comunión de temporada en niño, van desde 300€ a 600€ dependiendo de modelo, tejido y complementos.

En niña, los precios van desde 360€ a 998€, dependiendo de la diseñadora, de los tejidos y complementos. 

![tocado comunión azul](/images/posts/comprar-vestido-comunion/tocado.jpg)

### 7-Cuándo comprar el vestido de Comunión

Los vestidos y trajes tardan en fabricarse entre dos meses y dos meses y medio, por ello es conveniente no dejarlo para más tarde de enero, pero es aconsejable empezar a mirar en noviembre, que es cuando llegan casi todas las colecciones a las tiendas, y dejárselo prácticamente cerrado antes de fin de año.

Así que, si se tiene claro el modelo que se quiere, lo más aconsejable es encargarlo para asegurarlo y así hacer reserva del tejido , aunque la talla se decida un poco más adelante. 

## Qué es un Vestido de Comunión exclusivo

Si te apetece que tu hijo o hija lleve un vestido de Comunión exclusivo, es decir, con garantía de que no va a llevar nadie otro igual ese día en tu ciudad, debes buscar una tienda que ofrezca este servicio. 

En el caso de Momo Kids de Murcia, trabajan con varias [firmas de Comunión de las que os he hablado en este blog](https://madrescabreadas.com/2016/05/08/comunion-varones-carmy-monair/) en mis múltiples visitas al Paseo Mágico de la Feria de Comuniones FIMI, y garantiza totalmente la exclusividad del modelo elegido para ese gran día. Tenéis varias opciones:

![vestido de comunión mon air](/images/posts/comprar-vestido-comunion/sentada.jpg)

[Hortensia Maeso colección de comunión](https://hortensiamaeso.com) ofrece una colección sencilla, tranquila, etérea, sincera, con tejidos naturales, colores empolvados, bucólicos… trajes hechos artesanalmente y tejidos tintados a mano. De ella Montse destaca los trajes de niño porque tienen un estilo diferente a las demás marcas de niño (no tiene los típicos marineros), y tiene un patronaje que se adapta y sienta de maravilla. 

![traje decomunión de niño hortensia maeso](/images/posts/comprar-vestido-comunion/hortensia.jpg)

Crisby’s Colección es especialista en trajes más informales y desenfadados, y usa tejidos naturales en algodón y lino para sus camisas con cuello mao, chalecos, sobre camisas… 

Amaya Colección tiene una colección extensísima, con modelos atrevidos y diferentes para esas niñas que buscan algo especial, no el típico vestido; conjuntos de 2 piezas convertible (vestido+falda larga) que se desmonta y queda como un vestido corto para cualquier otro evento, monos, conjuntos de pantalón, etc. 
Recuerdo que mi hija mayor llevó un [vestido de Amaya en su graduación](https://madrescabreadas.com/2018/06/26/vestidos-graduacion/) de Primaria.

## Cómo elegir los zapatos de Comunión

Por último debemos atender a los zapatos de comunión, ya que hay gran variedad respecto a lo que se ofrecía cuando me tocó buscar para mis hijos mayores.

En este sentido Montse Navarro nos recomienda zapatos para los niños: Blucher con cordones o mocasín en ante o piel, también está muy en tendencia el abotinado con cordón natural en lino, más versatil para luego utilizar. 

Y para las niñas: Bailarinas en terciopelo, bailarinas en lino, con cintas cruzadas, esparteñas a conjunto con los vestidos (mismo tejido y mismas flores), botines en ante o lino. 

![zapatos bailarinas comunión con flor](/images/posts/comprar-vestido-comunion/bailarinas.jpg)

 Puedes ver [cómo organizar la primera comunión sin arruinarte aquí](https://madrescabreadas.com/2015/05/19/cómo-celebrar-la-comunión-sin-arruinarse/).

Espero que os hayan sido de utilidad estos consejos para comprar el vestido de comunión.

¡Si es así comparte para ayudar a más familias!