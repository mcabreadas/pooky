---
author: Belem
date: 2021-02-24 08:10:29
image: /images/posts/timidez/p-hojas.jpg
layout: post
slug: vencer-timidez-consejos
tags:
- adolescencia
- puericultura
- crianza
title: Ayuda a tu hijo vencer la timidez con estos consejos
---

Como padres, pocas cosas duelen tanto como ver a nuestros hijos no ser capaces de integrarse a un grupo social. Ya sea por factores externos o porque esa es su personalidad; lo más probable es que una de nuestras primeras reacciones sea querer ayudarles a vencer la timidez.

Sin embargo, ¿cómo saber si debemos ayudarle? ¿Realmente la timidez será un problema que deba superar? ¿Qué puedo hacer yo, como madre o padre por mi hijo o hija?

La timidez infantil es una situación que no siempre puede suponer un problema y que puede pasar inadvertida por mucho tiempo. Lo más común es que sea confundida con introversión o con una personalidad tranquila o actitud pasiva o indiferente. 


Nos permitimos recomendaros el libro "Hijos que callan, gestos que hablan: Lo que los adolescentes dicen sin palabras", de Susana Fuster.

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=8467052929&linkId=d8c02ddb134d89db015bd0d9f3d48f99&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Sin embargo un niño tímido, de la misma forma que no hace ruido en situaciones típicas de la interacción social infantil, tampoco pedirá ayuda en voz alta. Debido a esto, es probable que no nos demos cuenta de que nuestros hijos están pasando por un mal momento hasta mucho después y quizás la situación requiera la intervención de un profesional. Por supuesto, esto nada de malo tiene; recordemos que la salud mental debe ser prioridad y esto es particularmente cierto si se trata de la salud mental de nuestros hijos. 

Para evitar llegar a este punto, existen una serie de conductas que podemos detectar de manera temprana. Sólo basta con prestar atención a nuestros hijos, especialmente cuando se desenvuelven en un ambiente social. Pedir retroalimentación de sus profesores o cuidadores también es una buena idea. 

Cabe mencionar que la timidez infantil no siempre es un problema que necesita una solución. Muchos niños pueden naturalmente ser así. Esto quiere decir que la introversión o la timidez es un rasgo de su personalidad en lugar de un síntoma de algo más que pueda estar pasando. 

![tortuga metida en caparazón de frente](/images/posts/timidez/tortuga.jpg)

## ¿Qué se puede considerar timidez en un niño?

La timidez infantil conlleva un número de aspectos de la personalidad, no sólo que sea un niño o niña con vergüenza de hablar en público (aunque ésta es una de las características principales). El aspecto más evidente de la timidez infantil es la introversión en situaciones sociales, es decir, al convivir con otras personas. 

Sin embargo, esto no es todo. Existen otras características que pueden indicarnos que un niño sufre de timidez infantil. Por ejemplo el niño puede mostrar ansiedad en situaciones sociales, necesidad de controlar situaciones que no se pueden controlar, rehuir a las relaciones interpersonales, incluso puede suponerle una limitación importante que le impida hacer vida normal. Aveces incluso puede afectar físicamente con temblores, tartamudeo, sudor, náuseas, palpitaciones... o provocarle cambios extremos de humor: de la ira a la tristeza, de llorar de la nada pueden pasar a mostrar poca tolerancia a la frustración con actitudes violentas,
y sufrir por no ser capaz de desarrollar relaciones significativas con sus similares.

Este libro os puede ayudar:

{% include banner-120x240.html iframe-url="https://rcm-eu.amazon-adsystem.com/e/cm?ref=tf_til&t=madrescabread-21&m=amazon&o=30&p=8&l=as1&IS2=1&npa=1&asins=006084129X&linkId=c19675eb503fb638f4798028f033fa81&bc1=ffffff&amp;lt1=_blank&fc1=333333&lc1=ebb915&bg1=ffffff&f=ifr" %}

Recordemos que los seres humanos somos seres gregarios, necesitamos la compañía de otras personas para el correcto desarrollo de habilidades sociales que nos ayudarán a incorporarnos a la sociedad exitosamente. Esto es especialmente importante en los niños, pues la interacción con otros niños ayuda a desarrollar cualidades como la empatía, la autosuficiencia, la modulación y la buena autoestima, entre otras.  

![incertidumbre](/images/posts/timidez/planta.jpg)

## Causas de la timidez infantil

Como lo mencionamos anteriormente, la primera causa de timidez infantil es cuando ésta forma parte de la personalidad del niño. Esto quiere decir que siempre se ha mostrado igual, desde pequeño, salvo ligeras variaciones dependiendo de las circunstancias. Si bien, solemos pensar que los niños son seres más bien extrovertidos, alegres, amigables, curiosos y sociables, lo cierto es que hay niños que poseen personalidades introvertidas y que disfrutan más las actividades en solitario o en compañía de personas de su entera confianza. 

Si no se muestra angustiado en situaciones sociales, no sufre por no tener amigos y le tiene sin cuidado el estar solo, entonces no tenemos un problema. Simplemente se trata de la personalidad o el temperamento de nuestro hijo o hija.
Cuando el niño o la niña en cuestión solía ser extrovertido o ligeramente más sociables, pero de un día para otro (o poco a poco) se volvió tímido y retraído, puede que haya alguna razón detrás de este cambio. Existen algunas pautas que pueden indicarnos si la timidez infantil es síntoma de que algo más está pasando. 

Por ejemplo, si el niño o niña evita a toda costa el contacto físico, si presenta algún grado de retraso en el desarrollo del lenguaje, si muestra ansiedad exagerada en situaciones donde hay personas desconocidas o no puede estar tranquilo sin que mamá o papá estén presentes, o si el niño tiene 3 años o más y no se muestra interesado ni busca la compañía de otros niños de du edad, lo más conveniente será consultar con un profesional. 

Detrás de estas conductas, acompañadas de una timidez incapacitante, suele haber situaciones como algún grado de autismo no diagnosticado, un suceso personal traumático que le haya marcado, una muy mala experiencia con otras personas o algo más. Lo importante es guardar la calma y buscar ayuda lo antes posible.

![incertidumbre](/images/posts/timidez/nieve.jpg)

## Consejos para ayudar a nuestros hijos a superar la timidez

### Demuéstrale que lo quieres como es

Lo más importante es mostrarles a nuestros hijos todo nuestro amor y aceptación. Si les repetimos que algo está mal con él o ella por ser tímidos, podemos impactar en distintas medidas su autoestima. Por lo tanto, demostrarles amor por el simple hecho de ser quienes son tal y como lo son es el primer paso. 

### Fuera agobios

Existen algunas acciones que podemos tomar como parte de un plan de ayuda para enseñar a nuestros hijos cómo superar la timidez y ser, aunque sea, solo un poco más sociables. Podemos comenzar por no agobiarnos para no agobiarlos. Asimismo, suele ser una buena idea armarnos de paciencia extra. Recordemos no forzar, no juzgar, no comparar y ser empáticos con nuestros hijos. 

### Foméntale aquello en lo que destaca
Actividades que le ayuden a reforzar su autoestima suelen ser de gran ayuda. Por ejemplo, que practique una actividad, deporte o idioma en el que sobresalga le ayudará a aumentar la confianza en sí mismo, la cual es primordial al trabajar en superar la timidez. De la misma manera, actividades que le ayuden a mejorar su inteligencia emocional, el manejo de crisis, la tolerancia a la frustración y el desapego (o apego, según sea el caso), le ayudarán a la construcción de una autoestima saludable. 

### Apúntalo a actividades en grupo que le gusten
 
Actividades donde tenga que convivir con otros niños de su edad son buena idea, pero nunca hay que forzarlos a ellas. Simplemente trabajar los aspectos mencionados anteriormente, suele bastar para que el niño o niña busque y pida la convivencia con otros niños. 

![incertidumbre](/images/posts/timidez/juego.jpg)

Para terminar, recordemos que vencer la timidez no será siempre una cuestión de mera voluntad. Interfieren factores como el grupo social al que se desee pertenecer, el grado de ansiedad que la timidez provoca y el “compromiso” del niño. 
No dudes en buscar ayuda de un profesional si sientes que no puedes ayudarle a tu hijo o hija, o, si en vez de ayudarle a vencer la timidez, ésta empeora. 

Si te ha ayudado comparte par allegar a más familias y suscríbete para no perderte nada (no enviamos spam, no te preocupes ;)

*Photo by Joshua Eckstein on Unsplash tortuga*

*Photo by Sharon McCutcheon on Unsplash portada*

*Photo by Artur Rutkowski on Unsplash hojas*

*Photo by Adrian Infernus on Unsplash nieve*

*Photo by Artem Kniaz on Unsplash juego*