---
layout: post
slug: como-controlar-movil-hijo
title: "Podcast cómo proteger de Internet a nuestros hijos sin prohibir "
date: 2022-10-18T16:28:50.870Z
image: /images/uploads/sigmund-f_m44ut3xtw-unsplash.jpg
author: .
tags:
  - podcast
---
En este podcast hablo de tu a tu con una abogada digital, que ademas es mama para que nos oriente sobre como debemos proteger a nuestros hijos e hijas de los peligros de Internet lejos de prohibiciones y para ser madres y padres del S. XXI

Laura responder a preguntas como:

¿Cómo hago el control parental?

¿Qué es el control parental y para qué sirve?

¿Cuántos tipos de Parental control existen?

¿Cómo controlar el celular de mi hijo desde el mío?

Dale a seguir para no perderte nada!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6cy4jMY7c1irNG4zFQVeQw?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>



<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6ghtIXpPbZowI1zpTKC9ko?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Ademas de otras que formularos madres como tu a traves de el Instagram de las [Madres Cabreadas](https://www.instagram.com/madrescabreadas/).

## Recomendamos en este episodio

\-[Jornadas sobre Innovacion Educativa del Noroeste Murciano](https://www.eventbrite.es/e/jornadas-educaccion-tickets-423287663797?fbclid=IwAR24Cl5toJndWcTHXGIKibtRx7Svob1esGM5RH18Ys8EkKokyH0mgofAyig)

\-[Family Link](https://families.google.com/intl/es/familylink/) control parental

\-[Articulo centros con dispositivos digitales y responsabilidad](https://mamaconhijosenlared.com/menores-y-tecnologia/centros-digitales/)

\-[Noticia menor austriaca denunca a sus padres por subir imagenes suyas a -internet](http://www.antena3.com/noticias/mundo/joven-denuncia-sus-padres-publicar-fotos-cuando-era-pequena-facebook_2016092257e415b90cf21f619134d239.html).

\-[Pablo Duchement en twitter](https://twitter.com/PDuchement)

\-[El verdadero control parental - Mamá con hijos en la Red](https://mamaconhijosenlared.com/educacion/control-parental/)

[\-](https://mamaconhijosenlared.com/quien-soy-y-que-hago/)[Quién soy y qué hago - Mamá con hijos en la Red](https://mamaconhijosenlared.com/quien-soy-y-que-hago/)

Dale a seguir el podcast para no perderte el siguiente episodio.