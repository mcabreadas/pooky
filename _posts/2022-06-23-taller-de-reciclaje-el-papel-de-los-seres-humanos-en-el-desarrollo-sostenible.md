---
layout: post
slug: recicaje-niños-actividades
title: "Taller infantil de reciclaje: el papel de los niños en el desarrollo
  sostenible"
date: 2022-06-24T11:49:56.261Z
image: /images/uploads/la-creacion-de-nuevas-cosas-divertidas-a-partir-de-cosas-usadas.jpg
author: faby
tags:
  - invitado
  - educacion
  - planninos
---
Con motivo de la celebración del día del medio ambiente, y en colaboracion con la [ONG Cooperación Internacional](https://ciong.org), se llevó a cabo un taller promovido por el [Ayuntamiento de Santa rsula, Tenerife](https://santaursula.es/#), e impartido por María Jesús Sánchez Acosta. El mismo se enfocó en **concientizar a todas las personas sobre las diferentes maneras de cuidar el medio ambiente**. Para ello, se realizó una actividad en la que participaron varias familias.

A continuación vamos a comentar en detalle la actividad. Además, conoceremos un poco sobre nuestro invitado, la ONG Cooperación Internacional.

## La actividad de reciclaje para niños

La actividad se dividió en 3 categorías: **juego, cuento y dibujo**. Mencionaremos cada uno de ellos, así como las lecciones aprendidas a través de estas.

### El juego y el reciclaje en familia

Para llevar a cabo el juego se tomaron algunas medidas. Primero, se dividió a todos los presentes en dos grupos, los cuales a su vez eran una mezcla de adultos, niños y [adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/). A cada uno de los grupos se les asignó un objeto. Cabe señalar que estos objetos se encontraban rotos o dañados de alguna forma.

![niños en taller de reciclaje](/images/uploads/photo_2022-06-23_08-26-302.jpg)

El objetivo del juego era que **dijeran algunas cosas que aún se podían hacer con ese objeto, a pesar de no estar en buenas condiciones**. La idea era fomentar la importancia de no desperdiciar los recursos, sino darles un uso nuevo, diferente al de siempre. El equipo que diera más ideas, resultaría ganador, y recibiría caramelos como premio.

Tras finalizar el juego, los ganadores recibieron 2 caramelos cada uno, y los perdedores, uno. De igual manera, se aprovechó esa oportunidad para seguir creando conciencia sobre la importancia de cuidar el planeta. Esto se hizo al **usar uno de los objetos como papelera**, evitando que las envolturas de los caramelos quedaran regadas en todas partes.

### El cuento infantil para aprender a reciclar

La actividad continuó con un cuento, el cual se centró en grabar en las personas la importancia de las 4 R:

* **R**eusar
* **R**eciclar
* **R**educir
* **R**estaurar

La historia consistía en que todos éramos superhéroes y esas 4 R eran los superpoderes que cada persona tenía. Se tomaba un objeto y se empezaban a decir en voz alta cada una de las palabras antes mencionadas.

![Superheroes del reciclaje](/images/uploads/superheroes-del-reciclaje.jpg "Superheroes del reciclaje")

Debido a que en el grupo se encontraban muchos niños, **se hizo especial énfasis en definir cada una de esas 4 palabras**, a fin de que todos tuvieran claro qué significaban y la diferencia que hay entre ellas.

**Durante el cuento se daban ideas de cómo aplicarlas en cada objeto elegido**. Por ejemplo, si el palo de la escoba se rompe, lo más seguro es que la mayoría de las personas optarán por tirarlo a la basura, pero ¿por qué no buscar una manera de restaurarlo y volver a usarlo? Quizás podrían acondicionarlo para usarlo ahora como barra de cortina. Esto a su vez contribuiría a reducir la utilización de recursos, y también a ahorrar dinero.

Dado que se prestaba mucho para la ocasión, y el objetivo del taller, se destacó en varias oportunidades que es vital que cuidemos el planeta, debido a que solo tenemos uno. La meta era ayudar, tanto a pequeños como a grandes, a entender el valor de proteger el medio ambiente.

### El dibujo sobre el reciclaje

El cuento y el dibujo se realizaron de manera simultánea. En el caso del dibujo solo participaron los niños más pequeños. Así, mientras se explicaba el cuento, ellos estaban coloreando. El dibujo consistía en un superhéroe y a su lado tenía las 4 R: **Reusar, reciclar, reducir y restaurar.**

![dibujos sobre reciclaje](/images/uploads/photo_2022-06-23_08-26-353.jpg)

![panel de juegos de reciclaje](/images/uploads/photo_2022-06-23_08-26-262.jpg)

## Nuestro invitado

La ONG Cooperación Internacional es una organización sin fines de lucro que, desde el año 1993, se encuentra presente en Madrid, España. Dentro de sus objetivos principales está **sensibilizar a todas las personas sobre las condiciones de vida en las que nos encontramos**, y la responsabilidad que todos, como colectividad, tenemos ante estas.

![niños en taller de reciclaje](/images/uploads/photo_2022-06-23_08-26-202.jpg)

Además, **tiene como premisa lograr que todos podamos ser una fuente de ayuda**. Por eso, pone a disposición de todas las personas, si así lo desean, los medios que se necesiten para llevar a cabo las ideas o actividades que permitan aportar un granito de arena, y así ser parte de la solución.

![taller de reciclaje monitora y niños](/images/uploads/photo_2022-06-23_08-26-392.jpg)

[](<>)Sea en el cuidado del medio ambiente, la reducción de la pobreza o el hambre, o cualquier otra actividad similar, la ONG Cooperación Internacional, sobre todo con la participación de los jóvenes, busca ofrecer la mayor ayuda posible.

Muchas gracias a Cooperacion Internacional y a Mª Jesus, por hacernos pasar una tarde divertida en familia aprendiendo a cuidar el medio ambiente.