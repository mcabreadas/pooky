---
date: '2019-11-25T12:19:29+02:00'
image: /images/posts/yoga/p-yoga-circulo-madres-cabreadas.jpg
layout: post
tags:
- familia
- local
- salud
- crianza
title: Yoga con Petra para descabrear a las madres
---

> "Mi nombre es [Petra Caldenius soy profesora de Yoga](https://www.facebook.com/Yoga-con-Petra-321999211745801/), Aeroyoga y de Acroyoga. 
> Llevo 12 años practicando yoga y hace 6 años impartiendo clases.
> Amo mi trabajo."

Así se presenta nuestra invitada de hoy en este blog de maternidad y familia.
A Petra la conocí en La Manga, donde imparte en la playa unos talleres de acroyoga espectaculares (ahora también lo hace en el centro de Murcia), pero asequibles para todos los públicos. En ese momento vi algo especial en ella; no sé si fue la calma de su voz, su sonrisa o la sensación de paz que me quedó después de su clase en la arena y escuchando el mar en el silencio de la mañana.

![yoga en grupo en la playa](/images/posts/yoga/yoga-circulo-playa.jpg)


Fijaos cómo son las cosas que sólo coincidimos esa vez, una única vez este verano, pero el destino nos volvió a unir por casualidad en Murcia ciudad, y cuando le propuse participar en el [I Encuentro Madres Cabradas](https://madrescabreadas.com/2019/11/16/encuentro-madres-cabreadas/) impartiendo una clase de yoga, enseguida se embarcó en el proyecto porque si había alguien que puede ayudarnos a calmar a las madres estrelladas y descabrearnos era ella y su yoga.

![yoga en grupo con madres](/images/posts/yoga/yoga-grupo-madres-cabreadas.jpg)


## Beneficios del yoga para las madres
Dice Petra que con la práctica del yoga aprendemos a conocer nuestro cuerpo, despejamos la mente y liberamos tensiones (deberían incluirlo en la Seguridad Social, eso lo digo yo). Además el yoga:

- Reduce el estrés y facilita la relajación
- Mejora nuestra concentración
- Nos pone en contacto con el “yo” interior
- Aumenta nuestra autoestima y mejora las relaciones
- Mejora nuestro descanso
- Nos proporciona paz interior
- Nos ayuda a centrarnos en el presente



Pero también tiene importantes beneficios físicos, ya que:

- Nos ayuda a perder peso y quemar calorías
- Mejora nuestra flexibilidad
- Aumenta nuestra capacidad pulmonar y mejora la respiración
- Mejora nuestra circulación sanguínea
- Pone a punto nuestro corazón
- Aumenta nuestra fuerza y resistencia
- Mejora nuestra postura y equilibrio
- Alivia el dolor

![viñeta yoga madres](/images/posts/yoga/vineta-yoga-madres.jpg)
[^Viñeta de [yoguineando.com](https://yoguineando.com)]

## ¿Qué es el acroyoga, aeroyoga y el Yinyoga?

Pero no vayáis a pensar que sólo existe un tipo de Yoga. Para gustos colores, y Petra conoce casi todos, ya que ha estado impartiendo clases de vinyasa y hatha yoga en La Manga del Mar Menor muchos años, y actualmente se ha trasladado a Murcia, donde la podéis encontrar en el **centro CYM** dando yoga aéreo, AcroYoga y Yinyoga.


En el **Acroyoga** nos relacionamos de una manera diferente, interactuamos con otras personas, jugamos y nos permitimos el contacto físico sin tabúes. 
Podemos llegar a aislar mente y sin darnos apenas cuenta vamos liberando nuestro cuerpo. En pocas palabras volvemos a ser niños. 

![acroyoga](/images/posts/yoga/acroyoga1.jpg)

![acroyoga horizontal](/images/posts/yoga/acroyoga2.jpg)

En el **yoga aéreo** el columpio nos ayuda a mantener las posturas y alinear ya que sujeta el cuerpo. Además facilita las posturas de invertidas que en el yoga tradicional pueden ser difíciles. 

![yoga aéreo](/images/posts/yoga/yoga-aereo.jpg)

El **yin yoga** es un tipo de yoga meditativo. Las posturas se mantienen durante varios minutos. Trabajando la meditación y la respiración en cada postura.


## Dónde puedo practicar Yoga en Murcia

Podéis encontrar a Petra en el [Centro CYM](https://www.facebook.com/pg/centrodeyogayartesmarcialesinternas/posts/) y en el [centro “Samsara”](https://www.facebook.com/samsaramurcia/), ubicado en pleno centro de la ciudad de Murcia.

Además ha dirigido en “Parques, Música y Acción “, evento de multiactividades al aire libre organizado por el Ayuntamiento de Murcia, el programa de "Acroyoga en parques públicos" que, si tenéis ocasión, no dejéis de participar porque merece mucho la pena, y es gratuito.
Para el Ayuntamiento de Pilar de la Horadada, dentro de su programa mente cuerpo Petra también ha dirigido  actividades relacionadas con el Acroyoga en familias con la participación de gente joven, familias y personas de diferentes edades adaptando sus actividades para todas las edades; desde 8 a 60 años, incluso más, siempre y cuando tu cuerpo y mente te lo permitan.

![yoga acroyoga ancianos playa](/images/posts/yoga/acroyoga-playa.jpg)


Petra sigue reciclándose constantemente en cosas la apasionan siempre relacionadas con el movimiento, como el Taichi y Chikung.

Petra es una persona excepcional, que ama su trabajo y que, si estás pensando iniciarte en el Yoga, o ya lo practicas y quieres probar nuevos tipos de práctica, con ella acertarás seguro.

- *[Facebook de Petra Caldenius](https://www.facebook.com/Yoga-con-Petra-321999211745801/)*
- *[Instagram de Petra Caldenius](https://www.instagram.com/yogaconpetra/)*