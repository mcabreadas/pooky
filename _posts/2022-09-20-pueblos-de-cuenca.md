---
layout: post
slug: pueblos-de-cuenca-vino
title: Podcast de las aulas a las viñas. Una apuesta por la vida en el campo
date: 2022-09-22T08:03:31.120Z
image: /images/uploads/photo_2022-09-20-12.09.29.jpeg
author: .
tags:
  - podcast
---
Mariví Rubio nos cuenta su secreto para no cabrearse casi nunca. 

Un acontecimiento importante cambió el rumbo de su vida, y decidió dejar la ciudad y su trabajo para vivir en un pequeño pueblo de La Mancha, lo que resultó ser la mejor decisión que udo tomar, ya que le permite vivir como siempre había querido: ser dueña de su tiempo y llevar una vida sencilla disfrutando de su familia y de las pequeñas cosas.  

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3oRLqTs840BqsgZv6uXUyC?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Mariví estudió Psicología, y posteriormente un máster en Neuropsicología Clínica, ha trabajado como experta en comunicación alternativa (lengua de signos) lo que la llevó a diseñar un metodo bilingüe "[Lengua de Signos - Lengua oral Dímelo tú](http://asociacionintermedia.es/dimelo-tu-metodo-bilingue-lengua-oral-lengua-de-signos/)". 

Tras un acontecimiento que marcó su vida dar un golpe de timón y decidió convertirse en empresaria y viticultora en un pequeño pueblo de la Mancha.  

Está comprometida con las asociaciones [ Intermedia](http://asociacionintermedia.es/), para mediar entre las personas sordas y la Administración o las empresas y [AFA La Roda](http://afalaroda.es/), de apoyo a familiares de enfermos de alzheimer

Actualmente tiene el [alojamiento rural La Moravieta](https://www.google.com/search?client=safari&rls=en&q=la+moravieta&ie=UTF-8&oe=UTF-8), en Pozo Amargo, Cuenca y junto a su familia trabaja en sus propios viñedos.  

![mujeres en viñas pozo amargo cuenca](/images/uploads/photo_2022-09-20-12.09.24.jpeg)

![mujer midiendo el grado de la uva en un vinedo](/images/uploads/photo_2022-09-22-10.05.58.jpeg)

¿Y tu? ¿Te has planteado alguna vez vivir en un pueblo tranquilo?

Suscribete al blog para no perderte los siguientes episodios.