---
author: maria
date: '2021-03-17T10:13:29+02:00'
image: /images/posts/vlog/p-rutas.png
layout: post
tags:
- vlog
- planninos
title: Vlog Rutas en familia por la Región de Murcia
---

Esta semana hablo en la tele sobre ocio familiar y las alternativas que estamos buscando para pasar tiempo en familia de forma segura, al aire libre y cumpliendo las recomendaciones anti COVID.

Nosotros hemos apostado por las rutas por la Región y estamos descubriendo rincones sorprendentes, y descubriendo que no hace falta irse muy lejos para disfrutar cada fin de semana en un sitio totalmente diferente al anterior.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LLJQ-rZN7lo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Os dejo los itinerarios e información sobre las rutas que hemos hecho hasta ahora, y os animo a que compartáis las vuestras en el foro:

-Cieza: [Medina Siyasa y Cañón de Almadenes](https://madrescabreadas.com/2016/03/26/siyasa-almadenes-excursion/)

-[Bahía de Portman](https://foro.madrescabreadas.com/t/rutas-por-murcia/145/1) y [Batería de las Cenizas](https://madrescabreadas.com/2014/03/03/excursión-con-niños-a-la-bater%C3%ADa-de-las-cenizas/)

-SanJosé de la Montaña: [Ruta delas Caras](https://foro.madrescabreadas.com/t/rutas-por-murcia/145/2)

-Calasparra: [Bosque Cañaverosa](https://foro.madrescabreadas.com/t/rutas-por-murcia/145/10)

-Cartagena: [Cabo Tiñoso](https://foro.madrescabreadas.com/t/rutas-por-murcia/145/11)

-Librilla: Barranco del Infierno

<a href="https://foro.madrescabreadas.com/t/rutas-por-murcia" class='c-btn c-btn--active c-btn--small'>Ver las mejores rutas por la Región de Murcia</a>

Comparte para que llegue a mucha gente y suscríbete para no perderte nada!