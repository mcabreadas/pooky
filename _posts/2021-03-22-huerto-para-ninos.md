---
author: ana
date: '2021-03-22T09:13:29+02:00'
image: /images/posts/huerto-urbano-para-ninos/p-huerto-urbano-y-ninos.jpg
layout: post
tags:
- planninos
title: 'Huerto urbano en familia: crea tu propio huerto para niños'
---

Cultivar un huerto para niños, en un entorno urbano nos va a permitir sembrar y cosechar en familia muchas cosas además de frutas y hortalizas, entre ellas, el sentido de la vida y la responsabilidad, la importancia del cuidado y de la construcción armónica del día a día.

Nos brinda comprensión de lo que nos rodea y de cómo contribuimos con lo que hacemos al sostenimiento de la existencia. Un huerto nos hace a todos, niños y grandes, del mismo tamaño frente a la tarea de mantener y velar por el crecimiento y el milagro de las plantas.

Podemos aprovechar para alejarnos de las pantallas y conectar más con la naturaleza y con nosotros mismos. Os dejamos también esta sugerencia de libro de [150 actividades para hacer sin pantallas](https://madrescabreadas.com/2016/11/30/planes-caseros-con-niños-150-actividades-sin/).


## Beneficios de cuidar un huerto urbano para los niños

El huerto para niños de preescolar nos acerca a la tierra, pasamos más tiempo con nuestros hijos mientras todos aprendemos cosas nuevas. Se potencia la agricultura local con la ventaja de que se accede a alimentos orgánicos no contaminados. Los niños asumen responsabilidades, adquieren confianza, y comprenden la importancia de cuidar el entorno y hacerlo sostenible.

## Cómo crear un huerto urbano para niños

Lo primero es la elección del lugar donde poner las [jardineras o mesas de cultivo](https://www.amazon.es/dp/B08R2L1JBN?ref=exp_madrescabreadas_dp_vv_d), pues necesitan recibir sol buena parte del día. No olvidemos los agujeros para el drenaje. Añadimos a las jardineras el [sustrato](https://www.amazon.es/dp/B004WY11HQ?ref=exp_madrescabreadas_dp_vv_d), que se puede adquirir en tiendas especializadas, pero te recomendamos el de [compost](https://es.wikipedia.org/wiki/Compost) (enlace afiliado), que podemos hacer en familia a partir de los residuos orgánicos que generamos en casa.

Lo importante es que el sustrato sea esponjoso, aireado y retenga bien el agua. Luego alisamos su superficie con ayuda de un pequeño [rastrillo](https://www.amazon.es/dp/B07TC28HJC?ref=exp_madrescabreadas_dp_vv_d) y listo, está preparado para recibir las plántulas o [semillas](https://www.amazon.es/shop/madrescabreadas?listId=28MTJDKXN6VY6&ref=cm_sw_em_r_inf_list_own_madrescabreadas_dp_cG7IZvTsBLlI7) (enlace afiliado). Y, una vez hecha la siembra, debemos regar con abundancia.

![brotes-en-huerto-urbano](/images/posts/huerto-urbano-para-ninos/brotes-en-huerto-urbano.jpg)

A estas alturas te estarás preguntando...

## Qué puedes plantar en tu huerto urbano

Podemos sembrar de todo, pero sabiendo que las especies responden al tiempo, al clima y a la altitud, por ello es importante construir un [**calendario de siembra**](https://www.amazon.es/dp/B08QGD4P24?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado). Para cumplir con los objetivos escolares de orden y sistematización, enumeremos la rotación de los cultivos:

### Siembra 1

[Berenjenas](https://www.amazon.es/dp/B00KM28W0G?ref=exp_madrescabreadas_dp_vv_d), [pimientos](https://www.amazon.es/dp/B00ID4TTLG?ref=exp_madrescabreadas_dp_vv_d) y [tomates](https://www.amazon.es/dp/B00ID4TXBC?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado). Estos precisan mucho sol, tierra removida y aireada y deben ser sembrados entre enero y marzo. 

### Siembra 2 del huerto urbano para niños

Ajos, [cebollas](https://www.amazon.es/dp/B0173F95L4?ref=exp_madrescabreadas_dp_vv_d) y [puerros](https://www.amazon.es/dp/B00ID4TF7O?ref=exp_madrescabreadas_dp_vv_d); y apio, [perejil](https://www.amazon.es/dp/B00ID4TEW0?ref=exp_madrescabreadas_dp_vv_d) y [zanahori](https://www.amazon.es/dp/B00ID4TYKM?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado)a. Se pueden sembrar durante todo el año y resisten bien el frío

### Siembra 3 del huerto urbano para niños

[Lechugas](https://www.amazon.es/dp/B00ID4TQ5A?ref=exp_madrescabreadas_dp_vv_d) y [escarolas](https://www.amazon.es/dp/B00ID4TBVE?ref=exp_madrescabreadas_dp_vv_d). Necesitan mucho sol y se adaptan a cualquier época del año. En 50 días, habrá lechuga en las ensaladas familiares. Calabazas, [calabacines](https://www.amazon.es/dp/B00ID4TKJW?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado), melones, sandías han de sembrarse entre marzo y abril y esperar seis meses para obtener la primera carga. Finalmente, acelgas y espinacas, van bien durante todo el año, menos en invierno.

![calabazas-en-canteros](/images/posts/huerto-urbano-para-ninos/calabazas-en-canteros.jpg)

### Siembra 4 del huerto urbano para niños

[Brócoli](https://www.amazon.es/dp/B00KM28X5A?ref=exp_madrescabreadas_dp_vv_d), coles y coliflores, rábanos, se siembran en cualquier época del año, pero prefieren las temperaturas templadas del otoño o final del invierno;  las habas, [judías](https://www.amazon.es/dp/B00KM2CNS8?ref=exp_madrescabreadas_dp_vv_d) y [guisantes](https://www.amazon.es/dp/B00KM2C4NM?ref=exp_madrescabreadas_dp_vv_d) (enlace afiliado) a finales de otoño y en climas muy fríos a finales de primavera.

El huerto para niños es una experiencia de conocimiento integral, capaz de brindar muchas satisfacciones. Entre ellas la de ver cómo brota la vida y llevar el fruto de nuestro trabajo divertido directo a la mesa en forma de alimentos frescos y sanos.

Si te ha gustado comparte para ayudar a más familias y suscríbete para no perderte nada!


*Photo de portada by Paige Cody on Unsplash*

*Photo by Jametlene Reskp on Unsplash*

*Photo by Clarissa Watson on Unsplash*