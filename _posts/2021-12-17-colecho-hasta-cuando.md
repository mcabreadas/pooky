---
layout: post
slug: colecho-hasta-cuando
title: Cuándo no es buena idea hacer colecho
date: 2022-05-05T16:56:35.173Z
image: /images/uploads/portada-colecho.jpg
author: luis
tags:
  - crianza
---
El colecho es una práctica por la que, cada día se deciden más nuevas madres y padres de nuestro país y del mundo. No obstante, siempre existe la duda sobre si practicar el colecho o no y es que para esta práctica existen tanto detractores como defensores. Damos por hecho que todos conocéis de qué hablamos, pero para que no queden dudas, el colecho simplemente es el compartir el lecho con el bebé, lo que quiere decir que dormirá en la misma cama con los padres o lo hará con una cuna especial de colecho.

Nuestra visión sobre el colecho es clara y es que estamos a favor de esta práctica por la de beneficios que aporta tanto al bebé como a los padres, siempre y cuando hablemos de un colecho responsable. Con esto, queremos decir que hay que conocer la parte menos buena del colecho y también de las circunstancias específicas en las que no es recomendable practicar colecho. Todo ello, te lo intentamos resumir de la mejor manera en las siguientes líneas, ¿te quedas con nosotros?

En este post te explicamos [como fabicar tu propia cuna de colecho a partir de una cuna de Ikea](https://madrescabreadas.com/2013/01/27/cuna-colecho-ikea/).

## ¿Qué tiene de malo el colecho?

El colecho no es malo cuando se sabe cómo practicarlo y evitar los riesgos que esta práctica pudiera tener. Para los defensores del colecho, el hecho de que los bebés duerman en la misma cama favorece la lactancia materna, además de que repercute a una mayor calidad del sueño del bebé, a la vez que se fortalecen los vínculos emocionales entre padres e hijos. Sin embargo, los detractores suelen agarrarse a la idea de que dormir con los bebés es mal acostumbrarlos y que luego será una odisea pasar al niño a su cuarto.

![cuando no hacer colecho](/images/uploads/cuando-no-hacer-colecho.jpg "cuando no hacer colecho")

Desde luego, esta es una opinión muy personal que está muy lejos de ser lo peor que puede atraer el colecho. El punto más preocupante es la controversia existente al relacionar el colecho con el Síndrome de Muerte Súbita del Lactante, SMSL. Para apoyarse en ello, se basan en algunos estudios como el que se realizó en 2012, cuando aún esta práctica no eran tan conocida, en la que se expuso que, aunque la tasa de muerte súbita en lactantes era baja, el riesgo era 2.89 veces mayor en aquellos pequeños que practican el colecho, dormían con sus padres. Este aumento del riesgo se encontraba cuando el colecho se llevaba a cabo en superficies blandas, siendo inexistente dicho aumento en aquellos que usaban cunas de colecho.

En otros [estudios](https://archivos.evidenciasenpediatria.es/DetalleArticulo/_LLP3k9qgzIh7aNQBiadwmTB17CjpKICuoK3dInUBUhh9igMrw5rhtntW9AAWTCVyk1M72NwvJLBevSw41hyxYw), se habla de un aumento significativo de SMSL en bebés que practican colecho, pero igualmente era un aumento que se mostraba con factores externos. Esos factores son las claves que veremos en el siguiente punto de cuándo no se puede hacer colecho. Lo que tienes que tener claro, es que estos estudios arrojaban peligros a la hora de hacer colecho, pero no indican que esta práctica sea peligrosa si se realiza de la manera correcta y siguiendo las pautas recomendadas.

## ¿Cuándo no se puede hacer colecho?

Partiendo de la base de que la Asociación Española de Pediatría, [AEP](http://www.aeped.es/sites/default/files/documentos/201406-colecho-muerte-subita.pdf), señala que la forma más segura para que los bebés duerman hasta los 6 meses es en su cuna, boca arriba y cerca de la cama de sus padres, basándose en evidencias científicas que avalan que esta práctica reduce el riesgo de SMSL en más del 50%. No obstante, además de los beneficios también hay que informar de ciertas circunstancias que pueden hacer que esta práctica aumente los riesgos de SMSL.

Entre las recomendaciones para un colecho seguro podemos encontrar: que los bebés duerman boca arriba y cerca de la cama de sus padres; mantener una temperatura ambiente correcta; dormir sobre un colchón firme evitando superficies blandas; no compartir cama con otros niños o múltiples personas; proteger al bebé de la caída de la cama con los artilugios necesarios; etc.

Pero, por otra parte tenemos pautas en las que no se recomienda el colecho, y son las siguientes:

* No es recomendado en bebés prematuros y/o bajo peso al nacer.
* No se recomienda la práctica cuando alguno de los padres es fumador, ha consumido bebidas alcohólicas, drogas o medicamentos sedantes.
* No se recomienda cuando los adultos experimentan episodios de cansancio extremo.
* No se recomienda el colecho en superficies planas como sofá, sillones, colchones de agua, etc.

A esto hay que añadir que es mucho más seguro realizar esta práctica con cunas de colecho que se insertan en un lateral de la cama de los padres, dejando su propio espacio para el bebé. Así también se evitan los riesgos de aplastamiento o muerte por asfixia.

## ¿Cuándo dejar de dormir con el bebé?

Seguramente si has leído sobre esto antes hayas encontrado que en otros lugares te dan fechas límites, en las que ya se considera a los niños demasiado mayores para dormir con sus padres al llegar los 3-4-5 años. 

![cuando dejar el colecho](/images/uploads/cuando-dejar-el-colecho.jpg "cuando dejar el colecho")

En lo personal, cuando alguien saca el tema del colecho y cuándo acabar con esta práctica recurro a una de las psicólogas más reconocidas en el sector maternidad y crianza respetuosa, Mariela Cacciola, quién considera que dejar el colecho es una decisión que compete al ámbito familiar. La experta habla de que al igual que tomar la decisión de colechar es algo que se decide en familia, abandonarlo también debería ser igual y debería ser una decisión en la que se respetaran los deseos de todos los miembros de la familia.

También me gusta mucho la visión que tiene la psicóloga Mónica Serrano que explica que el sueño es un proceso evolutivo más y que en él están implicados una serie de factores (emocionales, sociales, fisiológicos, culturales) que hay que tener en cuenta a la hora de tomar la decisión. Hay que tener en cuenta, entre otras cosas, la capacidad de nuestro hijo de entender y expresarse usando el lenguaje, lo que hará más sencilla la tarea de que este comprenda y comience a dormir solo.

Me gusta compartir ambas visiones porque hablan de que dejar de colechar de un modo respetuoso es posible y que la solución pasa por respetar los deseos de todos los que componen la familia. Es posible llegar a dejar de colechar de manera natural cuando sea tu propio hijo el que manifieste su deseo o interés por dormir en su propio cuarto. Igual de respetuoso sería abandonar el colecho por el deseo de ambos padres, porque consideran que ha llegado el momento y que es lo más necesario para el niño o para ellos.

A día de hoy, ellas como tantos otros expertos en la materia coinciden en que no existe una fecha específica o momento clave para abandonar el colecho para todos los casos. Se trata de un proceso que no va a depender de la edad ni de otros hechos puntuales, como sí puede ocurrir en otros periodos como el fin de la lactancia o los despertares nocturnos. En este caso, es una suma de elementos en cada situación en particular que llevará al final del colecho de la manera más natural y respetuosa.

*Portada by Helena Lopes on Unsplash*

*Photo by Alex Bodini on Unsplash*

*Photo by Isaac Quesada on Unsplash*