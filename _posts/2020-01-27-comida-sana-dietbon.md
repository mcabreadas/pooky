---
date: '2020-01-27T12:19:29+02:00'
image: /images/posts/dietbon/p-lentejas-estofadas.jpg
layout: post
tags:
- colaboraciones
- familia
- salud
- nutricion
- ad
- crianza
title: Reto 2 Kg en una semana con la comida de dieta a domicilio de Dietbon
---

En este post os voy a contar mi experiencia perdiendo peso con la <a href="https://www.dietbon.com/es/" title="comida de dieta a domicilio" >comida de dieta a domicilio</a> de  Dietbon, y si seguís leyendo hasta el final, tendréis un regalito!

Las madres tenemos que cuidarnos. Es cierto que también es importante que nos cuiden y que sepamos dejarnos querer, pero no debemos olvidar que par que la familia esté bien, nosotras debemos sentirnos bien. Tras pasar una época de cierto abandono por la absorción de vida que conlleva la [crianza de 3 fieras](https://madrescabreadas.com/tag/crianza/) me propuse hace cosa de un año mirar más por mí en aras de la felicidad familiar, que pasa por la mía propia y... Qué narices! Porque yo lo valgo!

## Mi historia con el peso

El año pasado logré casi mi peso ideal bajando alrededor de  6 kg que se habían instalado en mí hacía años, y se negaban a abandonarme... Como mucho lograba bajar 2 kg, para luego recuperarlos a la mínima que me tomaba un gofre.

Ainsss... y es que comer para mí, y para muchas de vosotras seguramente, es un placer; nos da la vida una cervecita y unas risas con los amigos, y nos quita el sentío unos churros con chocolate. Pero para presumir hay que sufrir, y no sólo para presumir (que también), sino para recuperar mi forma física de antes, que la barriguita ya me estaba estorbando para atarme los zapatos y que no lograba abrocharme ningún vaquero sin tener que hacer un hipopresivo de urgencia en el probador de Zara.

Como digo, bajé 6 kg a base de mejorar hábitos alimentarios (lo que viene siendo acostarse con más hambre que el perro de afilador, que se comía las chispas para comer caliente), y gracias a mis clases de baile (y el ansia que te da cuando una cosa nueva te ilusiona y quieres practicarla todo el rato).

## En Navidad volví a recuperar peso

Pero, !Ay, Navidad! ¿Por qué me tientas? Estas pasadas Navidades me lo comí todo y volví a coger otros 2 Kg. Así que cuando descubrí la comodidad de Dietbon, y vi la pinta de sus platos pensé que quizá lograría perder ese peso de más en una semana.

##  Qué es Dietbon: la dieta cómoda a domicilio
Dietbon es un régimen para que no tengas excusas para seguir una dieta. Es decir, es tan cómodo que te mandan los platos ya cocinados a casa para que sólo tengas que calentarlos en el microondas o al baño maría.

Hay mucha variedad y está cocinados con mucho mimo, por eso los que he probado me han encantado!

 ![productos dietbon](/images/posts/dietbon/productos.jpg)

No te tienes que preocupar de comprar ni cocinar ni fregar ni pensar en qué vas a comer, tan sólo tienes que ir a tu caja y elegir uno de los platos en cada comida.


Los platos Dietbon están certificados 100% naturales: sin adictivos, conservantes ni colorantes artificiales, y libres de edulcorantes y aceite de palma.


## Gran variedad de platos Dietbon a elegir

Tienen gran variedad de platos para que puedas elegir lo que más te guste dentro de las indicaciones de la nutricionista, que te hará una consulta telefónica de unos 40 minutos donde hablaréis de tu historial de peso, tus costumbres alimentarias, los objetivos que quieres conseguir, tu índice de masa corporal y tus alimentos favoritos.

También te mandan una guía de 26 páginas “Seguir con éxito mi programa Dietbon” para anotar tu curva de pérdida de peso y otros datos que sirven de ayuda, así como consejos nutricionales.

Me encantan las ensaladas de pasta, así que uno de mis favoritos es éste:

 ![ensalada de pasta dietbon](/images/posts/dietbon/ensalada-pasta.jpg)
 [^Ensalada de pollo-farfalle-tomates seco y parmesano]

## Seguimiento por nutricionista

A mí, por ejemplo me puso 5 comidas al día, y me añadió un puñado de frutos secos en el tentempié de media mañana porque estaba acostumbrada a comer algo más que un yogur o una fruta. Además, me dejó tomarme una onza de chocolate puro después de la cena!
 
Los platos de la comida y de la cena los podía acompañar con la verdura al vapor o cruda que yo quisiera, así que mi truco era tomarme un buen ball de ensalada antes para que me saciara, y después el plato más el postre, que también me lo enviaron y consistía en una compota de manzana sin azúcares añadidos.

 ![pollo a curry dietbon](/images/posts/dietbon/curry.jpg)
 [^Pollo Tikka massala y mezcla de arroz quinoa]

Pero también me mandaron unos caprichos, como magdalenas de limón y tortitas (o o que mis hijos llaman Dorayakis),para los desayunos y las meriendas, para acompañar con una fruta o un lácteo.

 ![tortitas saludables dietbon](/images/posts/dietbon/tortitas.jpg)
 [^Pancakes naturales]

## Detox: el complemento perfecto

Además complementaba con unos tés Detox y otros relajantes para la noche, que me ayudaron contra la retención de líquidos, y que duran 28 días (cosa que recomiendo para depurar el organismo y sentirnos menos hinchadas).

El efecto es inmediato, ya que enseguida dan ganas de hacer pis, y el sabor es bastante intenso, así que recomiendo infusionar en bastante agua, lo que te ayudará a ingerir más cantidad de líquido y tendrá mayo efecto.
Al final del pots os dejaré un enlace con una sorpresita sobre esto. 

## ¿Pasé hambre?
 
Seguir el régimen fue fácil por la comodidad detenerlo todo a mano y no tener que preocuparme por nada, lo único que tenía que hacer era no salirme de toque me habían puesto. Reconozco que el primer día fue duro porque a las que somos de buen comer no nos gusta que nos recorten pienso, pero el resto de los días fue más llevaderoy finalmente mereció la pena!

 ![pasta con albóndiga dietbon](/images/posts/dietbon/albondigas.jpg)
 [^Penne con albondigas en salsa de tomate y verduras]

## El día libre

Para quien sigue régimen más de una semana hay un día en el que se puede comer lo que se quiera para hacerlo más llevadero, en mi caso me tomaría una cerveza y una marinera.

## Mis nuevos hábitos 
 
Pero mejor es que algunos hábitos los he incorporado a mi alimentación, como el tentempié de media mañana a base de fruta y frutos secos envés de bocadillo que hace que me siento mejor y más activa. 

También he hecho más ejercito físico durante esta semana, lo que me ha venido genial, y lo voy a incorporara mi rutina diaria.
 
## Reto superado!
 
 El resultado fue que bajé casi los 2 Kg que me había propuesto, pero sobre todo que me deshinché un montón y se me quitó la barriga (dentro de lo que se me puede quitar después de 3 partos).
 
 La satisfacción por haberlo logrado y poder abrocharme los vaqueros sin meter barriga lo compensaron todo
 
## Regalo Detox

Dietbon quiere haceros un regalo por ser seguidoras de mi blog y haberme acompañado en esta experiencia! Si hacéis vuestro pedido a través de [este enlace](http://bit.ly/db-madrescabreadas) tendréis 28 días de Detox para depurar con sus tes relajantes.

¿Te apuntas al reto?
 
 ![té detox dietbon](/images/posts/dietbon/detox.jpg)