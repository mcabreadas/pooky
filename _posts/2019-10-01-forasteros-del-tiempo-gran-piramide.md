---
layout: post
slug: forasteros-del-tiempo-gran-piramide
title: Los Forasteros del Tiempo en la Gran Pirámide
date: 2019-10-01T17:44:29+02:00
image: /images/posts/libro-forasteros-del-tiempo-piramides/p-nino-leyendo.jpg
author: .
tags:
  - 6-a-12
  - planninos
  - libros
---

En casa estamos super enganchados a las aventuras de la familia Balbuena, quienes protagonizan la saga [Los Forasteros del Tiempo, Editorial SM](https://es.literaturasm.com/libros?key=forasteros), de Roberto Santiago, el mismo autor de Los Futbolísimos, y de la que ya os hablé en este blog de familia cuando os reseñaba el libro "[La aventura de los Balbuena entre dinosaurios](https://madrescabreadas.com/2019/04/07/realidad-virtual-forasteros-del-tiempo/)".

En esta serie de libros recomendada para niños y niñas a partir de 9 años los protagonistas viajan a través del tiempo y descubren todas las curiosidades de la época en la que aterrizan cada vez. 

Además, podrán usar unas gafas de realidad virtual para poder viajar en el tiempo casi de verdad. A mis hijos les encanta ponérselas, incluso al pequeño, que ha quedado totalmente maravillado con la experiencia.


Las gafas de realidad virtual se usan para descubrir los objetos que los Balbuena han ido dejando en las distintas épocas que relata cada libro. 

![realidad virtual](/images/posts/forasteros-del-tiempo/p-gafas-realidad-virtual.jpg)

También hay una opción para que una locución vaya narrando los capítulos ideal para niños más pequeños que les interese la historia pero todavía no tengan la habilidad lectora necesaria para estos libros.

Esta vez esta divertida familia, que ya casi que forma parte de la nuestra, tras ser absorbida por un agujero negro, viene a caer en Egipto, donde les ocurren todo tipo de situaciones catastróficas y de las que no les será fácil salir airosos. 

Por poner un ejemplo, los lugareños, llevados por sus creencias ancestrales de que los que caen del cielo tienen que ser arrojados al abismo infernal, cuelgan a todo el grupo boca abajo, menos a Sebas, el protagonista, lo que le permite urdir un rocambolesco plan para liberarlos que pasa por entablar amistad con el emperador, que es un niño y se parece bastante.

En casa nos ha encantado porque mezcla misterio, diversión y cultura de las distintas civilizaciones. Sin duda una buenísima opción para regalar.