---
layout: post
slug: mejores-series-para-adolescentes-2021
title: Las 7 series para adolescentes que lo han petado en los últimos años
date: 2022-02-10T11:16:03.612Z
image: /images/uploads/portada-series-para-adolescentes.jpg
author: luis
tags:
  - series
  - adolescencia
---
¿Cuál es el entretenimiento que está de moda entre los jóvenes? Sin duda alguna, el mundo de las series y la llegada de plataformas de streaming como Netflix, HBO o Amazon Prime están dando un auge bastante importante a este sector. Es cierto que en este tipo de aplicaciones encontramos desde series hasta películas y documentales, pero en el día de hoy nos vamos a centrar en las series.

Muchas son las producciones que se crean en un entorno juvenil y que abordan las problemáticas que tiene este sector de la población. A ellos están dirigidas estas creaciones y es normal que triunfen entre este público. Los padres solemos quedarnos atrás  o centrarnos en los clásicos de nuestra época como Friends o Lost, pero el abanico ha crecido mucho desde esos años. Eso es lo que vamos a tratar hoy, las mejores series para adolescentes en la actualidad, aunque no estaría mal que hiciéramos una recopilación de series clásicas que nuestros hijos adolescentes deberían de ver. ¿Os gustaría la idea? Pero eso quizás sea en el futuro, ahora toca centrarnos en la actualidad y las series que están de moda entre los adolescentes.

Si todava no estás suscrito a alguna de las plataformas de series tipo Netflix, HBO, Amazon... , no podrás resistirte por mucho tiempo, así que te recomiendo varias opciones basadas en mi experiencia:

\-Si soléis comprar cosas en Amazon os interesa Amazon Prime Video, porque **compagina ventaja en envíos y compras con series y películas Online**.

Es lo que tenemos en casa porque categorizan muy bien los contenidos (si dice que es una serie para adolescentes coincide, no te meten otras cosas, como pasa en otras plataformas), tienen mogollón de **contenido original** y es bastante bueno. 

Puedes [darte de alta en Amazon Prime Video aquí](https://www.primevideo.com/?tag=ID_de_madrescabread-21)

\-Oferta para universitarios. Si tienes universitarios en casa, te interesa Amazon Prime Student. ¡Ahora hay una prueba de 90 días! Con esta opción tienes **todas las ventajas de Amazon Prime a mitad de precio**; solo EUR 18,00/año. Eso sí, esta oferta es sólo para estudiantes universitarios.

Puedes [darte de alta en Amazon Prime Student aquí](http://www.amazon.es/joinstudent?tag=ID_de_madrescabread-21)



## ¿Qué tipo de series prefieren los adolescentes?

Nos parece importante antes de comenzar dejar claro a qué nos referimos cuando hablamos de adolescentes y es que, la OMS estipula que este periodo de la vida comienza en torno a los 10 años y acaba sobre los 19. Son muchos años y todos sabemos que no es lo mismo un niño de 11 años que uno de 17, o incluso que un mayor de edad legalmente. Por tanto, tomaremos la adolescencia como ese punto intermedio entre los 15-16 años, momentos en los que sus preocupaciones diarias van creciendo y son más conscientes de la vida.

Eso es, en gran parte, lo que buscan los adolescentes con las series, sentirse arropados y saber que no están solos. Suelen preferir aquellas series en las que el protagonista comparte su misma edad, o similar, y en los que se puedan ver reflejados. No todo será realidad, la ciencia ficción también triunfa entre los jóvenes y una de las temáticas que más se está presentando es la de los mundos distópicos.

La moda es algo que también va a influir mucho en la decisión de los adolescentes a la hora de saber qué ver en la televisión. Muchos querrán ver la serie de moda porque sus amigos ya la han visto o la están viendo y no quedarse descolgados en las conversaciones del patio del instituto o reuniones con amigos.

## Las mejores series para adolescentes de los últimos años

Pero, ¿qué ven los jóvenes de hoy en día?, ¿cuáles son las series de actualidad para adolescentes? En esta lista hemos hecho una selección de las series que más están dando que hablar desde los últimos años para los jóvenes, con temáticas muy diversas y para todos los públicos. 

### Los 100 - Netflix

Los 100 es una serie que cuenta con 7 temporadas, todas ellas disponibles en la plataforma Netflix. En ella se nos presenta un mundo distópico, al que se llega tras un gran apocalipsis nuclear que hace la tierra inhabitable, lo que obliga a unos cuantos supervivientes a vivir durante un siglo en una estación espacial, en la que se crean unas normas de convivencia muy estrictas. Todo delito está penado con la muerte, a excepción de los menores de edad, que son recluidos hasta alcanzar la edad adulta.

Cuando la situación en la estación espacial se vuelve insostenible, deben comenzar a hacer expediciones a la tierra para comprobar su habitabilidad. Para esta expedición, seleccionan a un grupo de 100 adolescentes que se encuentran pendientes de cumplir con su pena. Es así como empieza una gran aventura que se alargará durante las 7 temporadas con las que cuenta la serie.

### Stranger Things - Netflix

![serie stranger things](/images/uploads/serie-stranger-things.jpg "serie stranger things")

En el año 2016 Netflix da luz a la serie norteamericana Stranger Things, siendo un verdadero éxito desde sus inicios por una historia que engancha y divierte a partes iguales. Actualmente son 3 las temporadas disponibles en la plataforma, a la espera de una cuarta que presumiblemente se estrenará durante 2022. Se trata de una serie ambientada en los años 80 y cuya trama principal transcurre en un pequeño pueblo llamado Hawkins, en el estado de Indiana.

Se trata de una ciudad muy tranquila, pero todo cambia el día que Will Byers, de tan solo 12 años, desaparece de una manera misteriosa y sin dejar rastro. Su pandilla de amigos, comienzan la búsqueda de Will ante tanta preocupación. La historia se torna aún más rara cuando aparece una niña que parece perdida, Once. Ella se sumará al grupo de amigos que buscan al desaparecido, trayendo una nueva trama a la trama principal y convirtiéndose en la total protagonista de la serie.

### Sex Education - Netflix

Seguimos en la plataforma de la N roja y es que es una máquina de crear éxitos para los adolescentes, además de ser una de las plataformas más extendidas y utilizadas. Sex Education es una serie de instituto protagonizada por Otis Milburn, un joven tímido que aún guarda su virginidad, algo difícil teniendo una madre terapeuta sexual, divorciada y con una vida sentimental bastante activa. La vida de Otis da un giro cuando conoce a Maeve, la chica rebelde de la escuela, a la cual aconseja en el sexo por sus conocimientos adquiridos de escuchar a su madre. Esto les lleva a crear un consultorio de sexo clandestino en el instituto, resolviendo las dudas de los jóvenes acerca del sexo, enfermedades de transmisión sexual y mucho más.

Salvo lo que pueda parecer por el título, el sexo es un tema que es tratado en la serie sin tabúes y desde una manera sencilla para que los adolescentes se familiaricen con este tema. Cuenta con 3 temporadas y es una fácil y amena de ver, por lo que los jóvenes pueden aprender y entretenerse al mismo tiempo.

### The Society - Netflix

![serie the society](/images/uploads/serie-the-society.jpg "serie the society")

¿Cómo funcionaría una ciudad en la que solo hubiera adolescentes sin supervisión de un adulto? Esa es la trama que nos presenta The Society, una serie de Netflix que cuenta con una única temporada y en la que el suspense está asegurado. Un grupo de adolescentes se encuentran en lo que parece ser su pueblo de siempre, West Ham en Nueva Inglaterra, pero descubren que los adultos han desaparecido. 

En los inicios cundirá la anarquía y se dejarán llevar por la fiesta, el alcohol, el sexo y las drogas. Cuando son conscientes de que están solos y nadie vendrá a ayudarlos, comienzan a preocuparse y entrar en pánico. Comienzan los racionamientos de comida y la división del grupo en pequeños subgrupos que lucharán por sobrevivir. Todo ello, mientras intentan resolver el misterio de dónde están los adultos.

### Pretty Little Liars - HBO

Esta casi se podría considerar una serie de culto adolescente y es que hace ya 12 años desde su estreno, pero para encontrar su finalización solo tendremos que remontarnos a 2017. Es una serie que se compone de 7 temporadas, un total de 152 episodios, que se encuentra acabada, lo que suele ser un aliciente a la hora de comenzar una serie.

El éxito de Pequeñas Mentirosas, título que recibió en España, fue brutal en su primera temporada, siendo más exitosa que grandes series como Glee o Gossip Girl. Se trata de un drama que sigue la vida de 4 adolescentes: Aria, Emily, Hannah y Spencer, las cuales formaban un grupo de amigas junto a Alison. La trama comienza con la desaparición de Alison de manera repentina y sin dejar rastro, lo que divide al grupo por completo. Un año después, las amigas se reencuentran y comienzan a recibir misteriosos mensajes de una fuente anónima que firma sus notas como “A”. Las primeras sospechas apuntan a Alison, pero entonces su cuerpo aparece. Ahora las jóvenes se enfrentarán a un peligro desconocido, una persona que dice conocer sus más oscuros secretos y amenaza con contarlos.

### Riverdale - Netflix

![serie riverdale](/images/uploads/serie-riverdale.jpg "serie riverdale")

Siguiendo con la temática juvenil y adolescente, Riverdale es otra de las series que mayor éxito ha cosechado entre este sector de la población, por esa mezcla de tratar problemas cotidianos de los adolescentes y el misterio de un serie de asesinatos que tendrá lugar en la ciudad.

La vida en Riverdale aún se repone después de la trágica muerte de Jason Blossom, el hijo de una de las familias más importantes de la ciudad. Con el nuevo curso, sus protagonistas comienzan a tomar caminos diferentes y llegan personas nuevas a la ciudad, es el caso de Verónica Lodge. Cheryl Blossom, es otra de las grandes protagonistas y afectada directamente por la extraña muerte de su hermano gemelo Jason. Cheryl es una chica llena de secretos, los cuales pueden salir a la luz en cualquier momento.

### Por trece razones - Netflix

Otro de los grandes boom de los últimos años para Netflix fue su serie Por Trece Razones en la cual se tratan todos los conflictos a los que se puede enfrentar un joven actual. El centro de la trama gira en torno a una problemática casi tabú y al que actualmente se está dando más visibilidad, el suicidio.

La historia gira en torno a Hannah Baker, una adolescente que se quita la vida, una decisión que, como veremos, no fue nada improvisada. La chica ha dejado preparada 13 cajas misteriosas para 13 personas diferentes, a las que culpa de su terrible final. Para comprender qué llevó a Hannah a tomar esta decisión, iremos siguiendo sus grabaciones, una por capítulo, y conociendo los entresijos de todo lo ocurrido antes de su desenlace fatal.

Aqui te dejo otras [recomendaciones de series para adolescentes](https://madrescabreadas.com/2020/06/18/recomendaciones-series-adolescentes-dos/).

Mas informacion [aqui](https://www.netflix.com/).

*Portada by Alin Surdu on Unsplash*

*Photo by Rafal Werczynski on Unsplash*