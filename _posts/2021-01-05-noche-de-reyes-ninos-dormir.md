---
author: ana
date: 2021-01-05 T09:19:29+02:00
image: /images/posts/como-lograr-que-los-ninos-duerman-en-la-noche-de-reyes/p-noche-de-reyes.png
layout: post
tags:
- navidad
title: Cómo conseguir que los niños duerman en la Noche de Reyes
---

Finalmente llega la fecha más esperada por los pequeños de la casa. Un cúmulo de emociones vividas en los días previos se agolpa en sus corazones llenándolos de ansiedad. El reto: lograr que duerman en la Noche de Reyes.

## Por qué los niños no pueden dormir la noche de los Reyes Magos

La emoción no les permite conciliar el sueño y con probabilidad harán que en el día tengan momentos de rabia o llanto. No es fácil canalizar las emociones y nada razonable pedirle a nuestros niños que lo hagan al menos de manera consciente o voluntaria.

Comprenderlos y acompañarlos es lo más importante en la víspera de un acontecimiento tan especial. Todas las rutinas se han roto, los preparativos, las fiestas, los encuentros, han llenado las horas de inquietud. Las celebraciones repercuten en la alteración de los horarios habituales, de modo que si queremos que descansen necesitamos tomar algunas medidas.

## Consejos para que los niños duerman la Noche de Reyes

![gastar-energia](/images/posts/como-lograr-que-los-ninos-duerman-en-la-noche-de-reyes/gastar-energia.png)

La clave consiste en que consuman energía, que lleguen a la noche de la víspera contentos, satisfechos y agotados. Ya en la cama, acompañémosles en esa delicada tibieza nocturna, y si es a la luz de un cuento, mejor. No los dejemos solos, porque tal vez no quieran pegar un ojo y tengan sobresaltos.

### En la víspera, hay que madrugar

Bueno, no exageremos, pero sí debemos lograr que el día comience más temprano. Acordemos con anterioridad una agenda de actividades que inicien a primera hora. Un paseo, montar bicicleta, visitar un parque, en fin, aire libre, carreras y diversión.

![felices-y-agotados](/images/posts/como-lograr-que-los-ninos-duerman-en-la-noche-de-reyes/felices-y-agotados.png)

### Preparar la casa para recibir visita

Los Reyes llegarán y es bueno que la casa esté arreglada para tan ilustres visitantes. Hagamos que los niños participen de manera activa en los preparativos, y claro está, se agoten un poco. Mejor si empleamos el tiempo de la siesta en estas actividades: ganar tiempo al tiempo sería el lema.

### Un plus de comprensión

Atendamos que han sido unos meses complicados y en sus mentes puede revolotear la idea de que la Noche de Reyes se haya visto afectada. Si todo ha sido difícil, incluso para los Reyes. ¿Creemos que los pequeños no han seguido de cerca nuestras preocupaciones? Pues bien, esa ansiedad puede manifestarse con rabia, desasosiego o impotencia y mal podemos responder exasperándonos. Es la hora de brindar confianza y seguridad, es la hora de abrazarlos y sentir profundamente que ya es un regalo estar juntos.

### Manténte a su lado hasta que se duerman

Nada como la calma de esos últimos minutos antes de dormir. Aprovechemos para contar una historia, leer un cuento, o charlar de manera tranquila sobre algún aspecto curioso del día. Alimentemos una atmósfera de calma, de hogar y refugio.

## Finalmente, hagamos que la Noche de Reyes obre su magia

![por-fin-los-regalos](/images/posts/como-lograr-que-los-ninos-duerman-en-la-noche-de-reyes/por-fin-los-regalos.png)

La han esperado con ansias los niños de la casa y eso se refleja en su inquietud. Nos toca comprenderlos y hacer que la expectativa que encierra para los niños recibir regalos, se transforme en esperanza y alegría. 

Llegados los presentes, la novedad se confundirá con la plenitud de haber hecho todo lo mejor para recibirlos y merecerlos.

Feliz Noche de Reyes, y que os traigan muchas cosas! Habéis sido buenas?

Si te ha gustado nos ayudarás si compartes. 

Suscríbete para no perderte nada! 


_Photo de portada by Rick Oldland on Unsplash_

_Photo by Atoms on Unsplash_

_Photo by Marcus Wallis on Unsplash_

_Photo by Nathan Dumlao on Unsplash_