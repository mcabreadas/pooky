---
author: maria
date: '2021-03-15T09:10:29+02:00'
image: /images/posts/hijos-universidad/p-graduacion.jpg
layout: post
tags:
- crianza
- adolescencia
- invitado
- educacion
title: Cuando los hijos se van a la Universidad
---

> Pensamos en la etapa universitaria de nuestros hijos e hijas y la solemos ver lejana pero, amigas todo llega, y antes de lo que pensamos... Y sino que se lo digan a nuestra invitada de hoy a merendar, que prefiere mantenerse en el anonimato, que nos va a contar su experiencia como madre de una universitaria; etapa crucial en la vida de nuestros hijos, en la que nuestro papel y actitud como padres creo que será determinante para su futuro tanto a la hora de elegir el carrera, como en el acompañamiento durante la misma.
> 
> Así que es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. Os dejo con nuestra invitada de hoy:

## La elección de la carrera universitaria

Cuando  mi hija empezó  enfermería lo hizo gruñendo.
Quería  estudiar medicina, y aunque se esforzó  todo lo que pudo, y mucho más, no llegó  a la nota de corte que pedían.
Las mates le bajan todas las "notamedia", no hay manera. Cada uno vale para lo que vale.

Por más que le decíamos que sería  un paso intermedio, que podía hacer un año y luego cambiarse, aunque no le convalidarían  nada, y tendría  que repetir parte de la selectividad, no había  manera de que se sintiera feliz por el esfuerzo, por las notas conseguidas...

![joven unversitaria libros](/images/posts/hijos-universidad/cafe.jpg)


Una de las cosas buenas que tienen las nuevas tecnologías es que llegan a la universidad con un grupo de wasap de futuros compañeros que se van encontrando sobre todo por twitter. Luego, ya sabes, algunas se convierten en amigas inseparables, y otras se ven un día y descubren que no se quieren ver más, creo que les hace llegar con un poco menos de miedo de lo que hicimos tu y yo. Y el grupo wasap que acaba siendo estable, es un gran apoyo, hasta en los exámenes online, ¡¡¡pero no te chives!!!

Cada facultad tiene su plan de estudios, las hay que empiezan enseguida con prácticas  y otras, como la de María  que no ven un enfermo hasta segundo.

## Mi hija va a ser enfermera

Un paréntesis, [enfermería](https://es.wikipedia.org/wiki/Enfermer%C3%ADa) es un grado relativamente nuevo, antes era "sólo" una carrera técnica, intermedia, eso hace que no tenga muchas catedráticas, la gran  mayoría de enfermeras son mujeres, la mayoría de las profesoras son enfermeras...  y luego el presidente de consejo de enfermería es un señor, con mil títulos,  y enfermero, eso sí. También hace que la catedrática de una asignatura se jubile y mientras buscan la manera, la asignatura desaparece. (Este año les ha pasado con estudio y manejo del dolor)

![enfermera antigua](/images/posts/hijos-universidad/enfermera-antigua.jpg)

Seguimos. Empezó segundo, seguía  pensando que el médico  llegaba a la cama del paciente, miraba, decidía  el tratamiento y las enfermeras corrían a obedecer, incluso bromeábamos en la cena diciendo que los médicos trabajaban un segundo y sentados, y las enfermeras cumplían órdenes sin parar y sin pensar si quiera.

No me he fijado mucho en el temario, que a estas edades, ya te dejan poco, pero si en los títulos de las asignaturas: cuidados del anciano, cuidados de la mujer, cuidados del niño... CUIDADOS (Os recuerdo [este interesante post sobre los cuidados](https://madrescabreadas.com/2020/10/27/mujer-cuidadora-ninos-ancianos/)). Estudian más  cosas, claro, anatomía, productos sanitarios, psicología... les insisten mucho en que tienen que aprender a protegerse también.

## Las prácticas de enfermería

Y entonces empezaron las prácticas, primero en un ambulatorio, luego en un hospital.
Llegan donde les toca, y una enfermera, sin más  títulos (no profesora, me refiero) será  su tutora. Pincha aquí, venda así... y mi hija volvía  a casa encantada con  lo aprendido, y yo daba gracias en silencio a las que le iban enseñando no solo a hacer todo eso, si no a enamorarse de su futura profesión.

![grupo enfermeros en hospital](/images/posts/hijos-universidad/grupo.jpg)


Si la tutora no está, siempre hay otra que dice ven hoy conmigo, y si creen que debe ver algo que se sale de la rutina, pues lo hacen, vente, que vamos a casa de un paciente a curar esto, y ves cómo es ser enfermera en casas ajenas, ayúdame, o mira cómo se atiende una herida complicada... vente a ver una cesarea de un prematuro...

## Y de repente llega el COVID-19

Y llega marzo... y todos a casa.
Llegó  casi llorando porque  "les habían  echado" El hospital en el que estaba paró  las prácticas  antes de que el gobierno decretase el estado de alarma, y tal como lo contaba era casi como si les hubiesen empujado.
Luego, claro, lo fue entendiendo. Y aunque a veces decía que era una oportunidad magnífica para aprender, yo aplaudía  viéndola  en casa.

Otro paréntesis. Las horas de prácticas las decide Europa, mi hija perdió  una semana, como la evaluaron (como muy valiosa), el resto del curso fue online, pero los que perdieron marzo, abril y mayo, tendrán  que recuperarlo para poder tener el título. ¿Cuándo? A saber, cuando se pueda, o cuando se acuerden, algunos en navidad, otros en verano...

![grupo enfermeros en hospital](/images/posts/hijos-universidad/ordenador.jpg)

Ha pasado por cardiología, oncología, pediatría.. se ha dado cuenta que hay personas jóvenes que mueren, que hay quien es capaz de sonreír mientras le duele todo, quien se queja algo más  de la cuenta, quien obedece sin dudar y alguno que nunca hace caso.

Voy a serte sincera. De vez en cuando la he pedido que se cambiase de carrera y que estudie lenguas clásicas, derecho, carpintería, que se hiciese agricultora... y me va a costar lo mío cuando empiece a trabajar sin tutora, aunque por lo que voy observando, y observo mucho, mucho, nunca se dejan de la mano.

¿Sabes en qué le hacen mucho hincapié?
En que reivindiquen su profesión, en que se den importancia, en que son importantes.
Tanto como los médicos, y tanto como los auxilares, limpiadoras...
Que llegue al paciente y le diga. Soy Maria y soy enfermera.

## Sueños trucados por la pandemia

Se iba de erasmus en octubre, y les pidieron que lo dejasen para febrero... y ya les han dicho que lo olviden, que es mejor no ir, y yo, ¡te lo puedes imaginar! Entre qué  pena, pierdes una experiencia única  y ¡viva, viva, bien, hurra, hurra!

No sé si me quitaré algún los miedos de verla  tan mayor, tan capaz, pero... ¿sabes eso de me llena de orgullo y satisfacción??

Pues yo mucho, mucho, mucho más 


> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí]()

Si te ha sido de ayuda, comparte!
Suscríbete al blog para no perderte nada.
https://madrescabreadas.com/merendando-con


*Photo portada by Brett Jordan on Unsplash*

*Photo by Green Chameleon on Unsplas café*

*Photo by Luis Melendez on Unsplash grupo enfermeros*

*Photo by Luis Melendez on Unsplash enfermera antigua*

*Photo by Windows on Unsplash*