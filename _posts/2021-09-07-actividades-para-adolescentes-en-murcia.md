---
layout: post
slug: actividades-para-adolescentes-en-murcia
title: Tú también puedes montar tu propia banda de rock con tu familia y que
  suene bien
date: 2021-09-08T08:28:59.966Z
image: /images/uploads/ea575435-aa48-43f5-8ea4-a6ef57eb3b50.png
author: maria
tags:
  - invitado
  - educacion
---
¿Y si te digo que en 8 sesiones con Néstor y su método puedes montar tu propio grupo de música pop-rock con tu familia aunque no hayais tocado un instrumento en vuestra vida ni tengáis nociones musicales?

Se acabó el concepto de soltar a tu hijo en la extraescolar y salir corriendo porque ahora tú también te vas a divertir. 

Lo que Néstor plantea de novedoso es una actividad familiar, pero que mole de verdad para los chicos y chicas adolescentes, y que os ayude a estrechar lazos familiares y encontrar esos puntos de encuentro entre varias generaciones (yo me llevaría hasta a los abuelos).

Porque la música conecta a las personas aunque hablen idiomas diferentes (te ayudará también el [diccionario del lenguaje adolescente](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/)), y puede lograr que de repente descubramos que a nuestro hijo, que últimamente está intratable, le encantan los Beattles, y sorprenderos tocando juntos Let it Be logrando esa conexión a veces tan difícil con palabras en estas edades, y convirtiendo el salto generacional en un puente.

No es magia, es Mr. Music,  una nueva metodología para aprender Música basada en 3 elementos: 

* un sistema innovador de escritura musical llamado ESCELSI
* la práctica musical en vivo sobre el escenario 
* la interacción social

La escritura musical **ESCELSI**, ideada por el profesor de Música murciano **Néstor de San Lázaro Rubio,** se desarrolla a partir de unos principios que son: 

* escritura **sin las barreras del solfeo** 
* es **intuitiva, inequívoca y oral** 

![mr. music nestor](/images/uploads/img-20210907-wa0020.jpg)

## Directamente al escenario

Al desarrollo de la metodología Mr. Music se une el profesor de guitarra **Vicente Cristiano Barba** para hacer que el aprendizaje se produzca sobre el **escenario**, utilizando la misma técnica de sonido que se usa en un escenario real y aprendiendo los entresijos de la Música a través de la práctica del bajo, la batería, la guitarra y el piano.

## Porque yo tengo una banda de Rock and Roll...

Por último, parafraseando a [Loquillo](https://www.loquillo.com), y partiendo de que la Música tiene un gran componente social, el enfoque se realiza a través de la formación de una banda Pop-Rock: los participantes de enriquecen de las aportaciones de los compañeros y tienen la oportunidad de practicar con todos los instrumentos de la banda.

El resultado obtenido utilizando la metodología Mr. Music lo dice todo: tu primera banda Pop-Rock en 8 sesiones y empezando desde cero. Además, cada participante adquiere los conocimientos suficientes como para participar en la banda con cualquiera de los instrumentos trabajados.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0QKO0R6foWY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Quiero apuntarme a Mr. Music

Toda la información en:

*  [www.mrmusicplay.com](http://www.mrmusicplay.com/) 
* por teléfono oWhatsapp **611108826**
* **infomrmusicplay@gmail.com**

La primera Aula Mr. Music se ha montado en la **Academia de Guitarra de Murcia**, situada en la C/ Cieza, 12 en Murcia.

![alumnos clase de musica instrumentos](/images/uploads/foto-hacking2.jpg)

## Plan de estudios de Mr. Music

El Plan de Estudios se divide en un nivel básico y 6 niveles de evolución musical y módulos formativos:

* Instrumentos de la banda 
* Instrumento principal 
* Transferencia al Lenguaje Musical 
* Estilos Musicales, Técnico de Estudio 
* Audición Activa 
* Improvisación Musical

## Implementa Mr. Music en tu academia 

* Mr. Music en **Grupo, Individual u Online**.
* Formación por **Módulos** en Lectura ESCELSI para integrantes de **coros**,
* Técnico de Estudio y **Dinámicas de Grupo** tanto para empresas como familias, poniendo en común una práctica **lúdica** entre **compañeros, amigos o familiares.**
* **Certificaciones** en sistema ESCELSI para maestros y profesores, 
* Profesor Mr. Music y Aula Mr. Music, para poner en práctica esta metodología en tu propia academia o escuela de Música.

Prueba una sesion porque os va a encantar!

Comparte si te ha gustado y suscribete para no perderte las novedades.