---
author: luis
date: 2021-04-21 14:00:00
image: /images/uploads/p-diamundialtierra.jpg
layout: post
slug: dia-mundial-tierra
tags:
- derechos
title: ¿Sabes por qué se celebra el día mundial de la tierra?
---

¿Sabes cuándo es el día de la tierra? Esta es una de las múltiples preguntas que os vamos a contestar en este artículo para que sepas todo lo necesario sobre este día en el que se conmemora al planeta tierra.

Además de ver qué día es el día de la tierra, también vamos a aclarar por qué se celebra el día de la tierra y cual es el motivo de que ese sea el día seleccionado.

## El día mundial de la tierra: ¿Qué se celebra?

El día mundial de la tierra es una fecha en la que se busca generar conciencia mundial sobre la importancia de nuestra relación con el planeta, la casa común en la que todos habitamos. La ONU califica este día como una celebración cuyo objetivo es el de llegar a un equilibrio entre la viabilidad y la salud medioambiental y las necesidades económicas y sociales de nuestra especie.

## Cuándo se celebra el día mundial de la tierra

La fecha elegida para la celebración del día mundial de la tierra se fijó en el 22 de abril de cada año, declarado así en 2009 por la Asamblea General de las Naciones Unidas. No se escogió esta fecha por casualidad sino que tiene su explicación histórica.

En 1970, el senador y activista ecologista Gaylor Nelson, del Partido Demócrata de EE.UU. orquestó una gran movilización para visibilizar el movimiento medioambiental. La fecha fue pura casualidad, simplemente coincidía que se trataba de un miércoles y se pensó que era el día perfecto para organizar una manifestación. Así fue, a la manifestación se unieron más de 20 millones de personas que exigían respeto al medio ambiente.

![marcha día de la tierra](/images/uploads/marcha-activista.jpg "marcha día de la tierra")

Gracias a esta manifestación se crea la Agencia para la protección del Medio Ambiente y, dos años después, se celebró la primera cumbre de la tierra en Estocolmo, reuniendo a políticos de diversos países. El año 1990 también es importante en la efeméride ya que la celebración del Día Internacional de la Tierra se globalizó en dicho año. Se consiguió congregar a doscientos millones de personas en 141 países diferentes por los problemas ambientales y esa gran repercusión consiguió que se celebrara la Cumbre de Río en 1992.

Todo esto sentó unas bases en cuanto a la concienciación sobre el clima y que los gobiernos de los países tomaran conciencia de esta problemática que estaba siendo ignorada, casi, por completo. 

## ¿Cómo se celebra actualmente el día de la tierra?

Como venimos diciendo, el día de la tierra no es una fiesta como tal en la que se celebre algo ni en la que tengas que participar en una celebración multitudinaria. Se trata de dar visibilidad al día y que cada vez más gente tome conciencia sobre ello.

Una de las mejores formas de celebrar este día es dando pequeños cambios en tu vida diaria para tener un planeta mejor como reciclar o inculcar esos valores a los más pequeños. Otro gesto que puede ayudar en este día es plantar un árbol o crear un grupo de recolección para limpiar un pinar, la playa o zonas donde la mala cabeza de los humanos hayan dejado residuos dañinos para el medio ambiente.

![recogida basura playa](/images/uploads/recogidabasura.jpg "recogida basura playa")

Además puedes participar en el sitio web del Día de la Tierra en sus múltiples actividades online como charlas, actividades y discusiones.

Y tú, ¿Conocías ya el día de la tierra? 

Déjanos un comentario con más ideas para celebrar este día y que todos juntos tomemos conciencia de que solo tenemos un planeta en el que vivir y hay que cuidarlo.

Comparte y suscríbete al blog para no perderte nada!

<!--StartFragment-->

Portada by Noah Buscher on Unsplash

<!--EndFragment-->

<!--StartFragment-->

Photo by The Climate Reality Project on Unsplash

<!--EndFragment-->

<!--StartFragment-->

Photo by Brian Yurasits on Unsplash

<!--EndFragment-->