---
layout: post
slug: el-secreto
title: Juntos descubrimos El Secreto
date: 2019-09-02T18:19:29+02:00
image: /images/posts/el-secreto/p-portada-el-secreto.jpg
author: .
tags:
  - 3-a-6
  - planninos
  - libros
---

[El Secreto, de Literatura SM](https://es.literaturasm.com/libro/secreto), es más que un libro infantil. Nada más abrirlo y mirar sus azules ilustraciones nos podemos dar cuenta de ello porque encierran tanta magia que los niños quedarán largo rato mirándolas y tratando de descubrir la historia por su cuenta, sobre todo si no saben leer aún.

Es ideal para edades de 5 a 7 años, pero lo podéis leer a niños más pequeños.

Esta original historia que ahonda en la peculiar amistad entre un gato libre y un tigre encerrado en la jaula de un zoológico, hará pensar a los peques, plantearse cosas y hacer preguntas. 
![niño leyendo](/images/posts/el-secreto/nino-leyendo.jpg)
Así que preparaos sobre todo si están en la edad que ronda los 7 años cuando empiezan a plantearse cosas de la vida, y a querer respuestas, además, inmediatas. Son pequeños filósofos sedientos de verdades, no creéis?

El gato tiene algo que el tigre añora: libertad y mil historias que cada día cuenta puntualmente a su gran y fuerte amigo. Sin embargo el tigre tiene la fortaleza y grandiosidad que al gato le gustaría.
![ilustracion monos secreto](/images/posts/el-secreto/monos.jpg)

La amistad entre ellos hace que tracen un plan para lograr que el tigre logre la su tan ansiada libertad donde el gato tiene una gran papel.
Amistad, generosidad, altruismo... esta historia nos da pie a explicar estos valores a nuestros hijos.

Este libro es una pequeña joyita, que debe de estar en la mesita de noche para saborear junto a vuestro peque cada día un poquito y recrearos en sus maravillosas ilustraciones.

Juntos descubriréis El Secreto