---
layout: post
slug: batch-cooking
title: Cocina poco y come bien con el metodo batch cooking
date: 2020-09-05T18:19:29+02:00
image: /images/posts/batch-cooking/p-platos.jpg
author: maria
tags:
  - trucos
  - recetas
---
Os voy a dar un truco que para mí ha significado un antes y un después en mi organización, ya que me ha liberado horas y espacio mental diario para poder dedicar a mi trabajo: el batch cooking o método por el cual se cocina sólo una vez semanal, quincenal o mensualmente grandes cantidades con un poco más de esfuerzo de lo que harías para cocinar para una vez.

En este enlace os dejo [mi lista de imprescindibles para practicar el batch cooking](https://www.amazon.es/shop/madrescabreadas?listId=13Z6T1GGS6CEC) (enlace afiliado) que os he seleccionado basándome en mi experiencia con este método, pero mi consejo es que antes de lanzaros a comprar recopiléis todo lo que tenéis por casa y tratéis de usarlo, y cuando se vaya estropeando y tengáis que reponer, entonces vayáis invirtiendo poco a poco en cosas nuevas.  

Te dejo también algunas [recetas aptas para batch cooking](https://madrescabreadas.com/tag/recetas/), por si te sirven o te dan alguna idea.

Reconozco que pensar cada día en el menú de comida y cena me supera muchísimo y que he estado años agobiada con este tema ya que mis hijos nunca han hecho uso del comedor escolar. Por eso cuando descubrí este método me cambió la vida porque cuando se tiene una familia grande, como es mi caso, las comidas no se pueden improvisar, y si se hace siempre se acaba comiendo cualquier cosa, y de una forma no equilibrada.

![guiso tradicional](/images/posts/batch-cooking/rabanos.jpg)

## Ventajas del batch cooking

* Se logra comer equilibradamente
* Se planifican los menús sólo una vez (no cada día)
* Se compra sólo una vez (salvo el pan diario y la fruta...)
* Se cocina sólo una vez
* Se friega y se limpia sólo una vez
* Se ahorra tiempo y energías en el día a día 
* Con unas sencillas instrucciones los niños mayores de 12 años pueden prepararse su comida o cena.

![ensalada de legumbres con vinagreta](/images/posts/batch-cooking/garbanzos.jpg)

## Qué necesitas para el batch cooking

\-No necesitas una gran cocina, con una normal, vale. 

\-La clave está en las ollas y sartenes que uses, que deben ser altas, en vez de anchas de manera que te encajen en todos los fuegos que tengas en tu cocina a la vez, y preferiblemente sin mangos o ases muy largos o voluminosos. De esta manera optimizarás el tiempo que pasas cocinando.

![](/images/posts/batch-cooking/ollas.jpg)

\-Imprime unas [plantillas de menús semanales como éstas.](https://www.pequerecetas.com/wp-content/uploads/2009/11/plantilla-menu-semanal-infantil1.pdf)

\-Envases para guardar lo que vayas cocinando: mira que sean apilables, de manera que ocupen el menos espacio posible en el congelador. 

![](/images/posts/batch-cooking/tuppers.jpg)

Si decides cocinar para un mes, lo ideal para ahorrar espacio es usar una máquina de envasar al vacío y guardes la comida de esta forma, ya que las bolsas ocupan mucho menos y quizá con un congelador normal tengas suficiente.

![](/images/posts/batch-cooking/patatas.jpg)

Nosotros invertimos en un arcón porque somos 5, y cocino de golpe para un mes comidas y cenas. 

## Cómo hacer batch cooking sin volverte loca

\-La planificación es imprescindible. Piensa bien para cuánto tiempo quieres cocinar, de cuánto espacio dispones y cuántos recipientes tienes para envasar antes de lanzarte .

\-Elabora el menú semanal, quincenal o mensual de forma equilibrada. Incluye platos realistas, que domines, y que permitan ser congelados y queden bien tras la descongelación con algún retoque final

\-Mi consejo es que las primeras veces, hasta que se domine la técnica y se experimenten los resultados y lo que más gusta a la familia, se haga de forma semanal. Luego se aumente a quincenal, y para los que se atrevan y tengan espacio o envasadora al vacío, se repita el menú quincenal para convertirlo en mensual doblando cantidades.

\-Reserva una mañana o tarde completa para dedicarla exclusivamente a cocinar. Ten en cuenta que envasar, clasificar y marcar los recipientes también lleva tiempo.

\-Asegúrate de que tienes todos y cada uno de los ingredientes en casa, así como los utensilios que vas a necesitar para elaborar las recetas: batidora...

\-Escribe cada plato que vas a cocinar en un post it y la cantidad de raciones que tienes que hacer, pégalos en el frigo por ejemplo ve quitando los que vas marchando.

\-Búscate un pinche: tardarás la mitad si tienes quien te ayude, pero tiene que estar a tus órdenes porque es fundamental que dirija quien conozca el plan al dedillo.

\-Ve envasando y fregando cacharros para reutilizarlos.

\-Cuando tengas todo envasado coge tu calendario de menús y pon un número junto al nombre del plato que comeréis ese día. Número que deberás poner también en el envase donde lo hayas guardado. De este modo sólo tendrás que acordarte la noche antes de mirar el número en el calendario, buscarlo en el congelador y sacarlo.

Antes de comer los platos descongelados normalmente tendrás que darles un toque para que queden más apetecibles. Por ejemplo:

* Si es una crema, añadir un poco de leche y calentar
* Si es arroz: habrás congelado la carne, el sofrito y el caldo y sólo tendrás que añadir el arroz y cocer

![paella de conejo](/images/posts/batch-cooking/arroz.jpg)

* Si es un guiso tendrás que añadirle las patatas (que podrás tener previamente peladas y cortadas en trozos de bocado y envasadas al vacío para sólo tener que cocerlas 5 minutos).

![guiso de pavo con patatas](/images/posts/batch-cooking/guiso.jpg)

* Si es pasta con salsa boloñesa, quizá tengas que añadir un poco de tomate frito.

![macarrones con albóndigas](/images/posts/batch-cooking/pasta.jpg)

* Si son unas lentejas quizá tengas que añadirles un poco de agua y remover

![lentejas](/images/posts/batch-cooking/lentejas.jpg)

* Si es una empanada o un solomillo relleno envuelto en hojaldre, sólo tendrás que pintarlos con huevo y hornear.

![aguja de hojaldre al horno](/images/posts/batch-cooking/aguja.jpg)

* Si son filetes empanados sólo tendrás que freír u hornear.

Hay mil sitios con recetas para batch cooking que puedes consultar, y también te puedes animar a crear las tuyas propias.

Os dejo la lista del material que yo uso para cocinar con el método batch cooking:

\-[Olla a presión](https://amzn.to/358omkl) (enlace afiliado): la capacidad depende de los que seáis de familia.

\-[Sartenes y cacerolas](https://amzn.to/3h2ZMUf) (enlace afiliado) rectas, que no ensanchen por arriba para poder poner tantas como fuegos haya.

\-[Tuppers o recipientes para almacenar alimentos](https://amzn.to/2R1HesQ) (enlace afiliado) en el congelador apilables

\-[Envasadora al vacío](https://amzn.to/3bvrXtP) y [bolsas para envasar al vacío](https://amzn.to/3gZi09n) (enlace afiliado)

Si te ha gustado, comparte para que llegue a más familias. 

De este modo también me ayudas para que pueda seguir escribiendo este blog.

En este video puedes ver de forma práctica cómo me organizo yo con el batch cooking.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5E1okGnqVFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![mujer sonriente cocinando](/images/uploads/influencer-10997495_1684748250369_original._fmjpg_.jpeg)