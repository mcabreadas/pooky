---
layout: post
slug: entrevista-madres-cabreadas-encarna-talavera-entre-mujeres
title: Me entrevista Encarna Talavera Entre Mujeres
date: 2020-03-03T18:19:29+02:00
image: /images/posts/entre-mujeres/p-encarna-maria.jpg
author: maria
tags:
  - local
  - revistadeprensa
  - vlog
---

Me hace muchísima ilusión compartir con vosotras la entrevista que me hizo la periodista murciana a la que admiro desde jovencita Encarna Talavera en el programa que dirige en 7 TV Región de Murcia, "Entre Mujeres"para quienes no pudísteis verla el miércoles pasado.

![Encarna Talavera y Madres Cabreadas](/images/posts/entre-mujeres/encarna-depie.jpg)

## Premio 8-M

Además su emisión coincidió con que el programa acaba de ser galardonado en los premios 8-M que reconocen la lucha por la igualdad, el liderazgo y talento femenino.

Muchas gracias a todo el equipo por lo bien que me habéis tratado, por la sensibilidad con la que habéis transmitido mi trabajo y por el cariño que habéis puesto.

Sin duda merecéis el premio por vuestra labor en la visibilización y empoderamiento de la mujer.

Ha sido un honor participar en un programa donde han ido tantas mujeres luchadoras y talentosas, entre ellas mi compañera de cartel, la diseñadora de moda para ceremonias Cayetana Ferrer, cuyo magnífico trabajo tuve ocasión de conocer en la pasada edición de Murcia Pasarela Mediterránea.

## Aquí tenéis el enlace(minuto 24):

>  [Entrevista completa con Encarna Talavera, Programa Entre Mujeres](http://webtv.7tvregiondemurcia.es/divulgativos/entre-mujeres/2020/miercoles-26-de-febrero/) 


## Resumen en titulares de la entrevista:


![programa entre mujeres granada](/images/posts/entre-mujeres/granada.jpg)

![programa entre mujeres twitter](/images/posts/entre-mujeres/twitter.jpg)

![programa entre mujeres comunidad madres cabreadas](/images/posts/entre-mujeres/comunidad.jpg)

![programa entre mujeres estudios madres cabreadas](/images/posts/entre-mujeres/estudios.jpg)

![programa entre mujeres profesion madres cabreadas](/images/posts/entre-mujeres/despacho.jpg)

![programa entre mujeres madre de tres](/images/posts/entre-mujeres/madre.jpg)

![programa entre mujeres madres cabreadas en el congreso](/images/posts/entre-mujeres/congreso.jpg)

![programa entre mujeres madres cabreadas en el congreso](/images/posts/entre-mujeres/congreso-2.jpg)

![programa entre mujeres encuentro madres cabreadas](/images/posts/entre-mujeres/encuentro.jpg)

![programa entre mujeres premio puericultura madres cabreadas](/images/posts/entre-mujeres/premio-puericultura.jpg)

![programa entre mujeres premio puleva madres cabreadas](/images/posts/entre-mujeres/premio-puleva.jpg)

![programa entre mujeres colaboracionismos revistas madres cabreadas](/images/posts/entre-mujeres/revistas.jpg)