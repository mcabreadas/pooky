---
layout: post
slug: recetas-filipinas-autenticas
title: ¿Por qué es importante que los niños conozcan otras culturas?
date: 2023-02-06T13:39:57.750Z
image: /images/uploads/ninos-de-varias-culturas.jpg
author: faby
tags:
  - 6-a-12
---
Los problemas económicos han hecho que millones de personas emigren a otros países; algunos incluso se han mudado a otros continentes. En este sentido, se ha visto mayor diversidad cultural en las escuelas y los vecindarios. De ahí, que es necesario que los niños desarrollen empatía por las personas de otras regiones.

¿Qué significa conocer a otras culturas? Implica **informarse sobre su vestimenta, comida e incluso tradiciones**.

Por ejemplo, si una familia de Filipina se muda al vecindario, se le puede enseñar al niño a dar la bienvenida con un presente, quizás, preparando una de las** *[recetas de Filipinas auténticas.](https://www.amazon.es/LIBRO-RECETAS-FILIPINAS-Michelle-Lee/dp/B0BGZM9PT1/ref=sr_1_10?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=IB7Y77AP9P96&keywords=libro+de+recetas+filipinas&qid=1665068794&qu=eyJxc2MiOiIwLjU1IiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sprefix=libro+de+recetas+filipin%2Caps%2C894&sr=8-10) (enlace afiliado)***  ¿Verdad que eso impresionaría a los extranjeros? Sería una excelente forma de llevarse bien con los nuevos vecinos.

## ¿Por qué es importante conocer otras culturas en la infancia?

En la **etapa infantil se desarrollan los principales valores que definen la personalidad de adulto.**

Si el niño aprende que hay diversidad cultural y se le enseña a ser tolerante y mostrar respeto por las tradiciones o costumbre de otras regiones, **podrá disfrutar de una mejor relación con otras personas.**

![Amistad entre niños de diferentes etnias](images/uploads/amistad-entre-ninos-de-diferentes-etnias.jpg "Amistad entre niños de diferentes etnias")

A continuación, indicaremos los principales beneficios de familiarizarse con otras culturas:

* **Amplía la visión del mundo y estimula la creatividad**. El niño logra entender que el mundo es mucho más amplio que lo que ve en su entorno. Esto genera curiosidad, incentiva su creatividad e incluso lo motiva a viajar. Así pues, debe aprender que hay diferentes países, continentes e idiomas.
* **Mejora la relación social.** Conocer distintas etnias y diferentes nacionalidades permite que se actúe con naturalidad frente a lo que es diferente. De este modo, se forja una mejor sociedad.
* **Mejora la comunicación.** Informarse sobre costumbre de otras regiones le da al niño un buen tema de conversación, lo hace una persona más culta, empática e interesante frente a los demás.
* **Previene el bullying.** No es un secreto que los niños y jóvenes extranjeros son objetos de acoso escolar. Pero, si los niños aceptan que el mundo es multicultural se reduce el margen de bullying.
* **Se abre a nuevas profesiones de adulto.** Un niño culturizado siente mayor motivación para aprender idiomas, esto permite que tenga mejores oportunidades de empleo cuando sea adulto.

## ¿Cómo educar a los niños en la diversidad cultural?

![Niños de varias culturas familiarizados](images/uploads/ninos-de-varias-culturas-familiarizados.jpg "Niños de varias culturas familiarizados")

Hay varias formas de hacer que el niño se familiarice con otras culturas.

* **Recetas.** Se pueden hacer recetas de comida de diferentes lugares. Para preparar las recetas basta con *comprar un libro de cocina*. Por ejemplo, los platillos típicos de Filipinas se pueden preparar con pocos ingredientes. Y si no tienes tiempo de cocinar, seguro que te ayudara usar directamente [caldos envasados](https://madrescabreadas.com/2022/07/27/mejores-caldos-envasados/).
* **Mudanza.** Otra idea es aprovechar la mudanza de un extranjero para averiguar información sobre el país de estos. Si los nuevos vecinos provienen de un continente lejano como Asia, sería conveniente que el niño escuche algunos audios en ese idioma, conozca el estilo de ropa e incluso averigüe las normas de respeto de ese continente.
* **Educación multicultural.** Por último, puedes hablar de forma general de varias culturas, e incluso de algunas culturas indígenas dentro de tu propio país. Eso sería muy entretenido para el niño y le ayudaría a actuar con naturalidad cuando conozca en persona algún extranjero.
* **Evita el vocabulario discriminatorio.** Esta es la mejor forma de educar a tu hijo. Deja de lado los comentarios que perpetúan los estereotipos. Por ejemplo, “trabajar como un chino”, “tenías que ser gocho”, “ir hecha una gitana”, etc.

En conclusión, tus peques necesitan saber que [el mundo es multicultural](https://es.wikipedia.org/wiki/Educación_multicultural) y eso es lo que le da color a la vida.