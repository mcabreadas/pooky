---
layout: post
slug: leches-adaptadas-embarazadas-ninos-puleva
title: Leches adaptadas para embarazadas y niños ¿sí o no?
date: 2019-02-03T18:19:29+02:00
image: /images/posts/leches-adaptadas-embarazadas-ninos-puleva/p_leches-adaptadas-embarazadas-ninos-puleva.jpg
author: maria
tumblr_url: https://madrescabreadas.com/post/182526164274/leches-adaptadas-embarazadas-ninos-puleva
tags:
  - embarazo
  - nutricion
  - ad
  - crianza
---

Con motivo del V Blog Trip Puleva Infantil, marca que este año celebra su 60 aniversario, en el que un año más he participado como bloguera y como ponente, ha vuelto a salir a la palestra la conveniencia o no de recurrir a las leches adaptadas en momentos de la vida en los que  las necesidades nutricionales son mayores o más específicas, y cruciales para el desarrollo y prevención de futuras enfermedades como son el embarazo y la etapa de crecimiento de los niños.

Aquí, mi participación en [Blog Trip de Puleva de años anteriores](https://madrescabreadas.com/post/153132564709/nutricion-infantil-leche).

Fui invitada por [Puleva](https://www.lechepuleva.es) junto con mi familia a pasar un fin de semana en Granada para asistir a una jornada de ponencias sobre nutrición donde varios expertos en ginecología, pediatría y nutrición nos pusieron al día sobre las últimas novedades.

También hubo un bloque de comunicación digital y límites legales donde tuve la suerte de compartir micrófono con el periodista Alberto Herrera, coordinador de salud del programa [Más Vale Tarde, de la Sexta](https://www.lasexta.com/programas/mas-vale-tarde/), quien se encargó de la parte ética, dejándome a mí el aspecto legal del asunto que, para mi alegría, interesó bastante y la participación fue abundante en cantidad y el calidad, lo que generó intercambios de opiniones interesantes y nuevas ideas para futuras ponencias.
![ponencia derecho](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3v05YvXz1qfhdaz_540.jpg)

## ¿Mi hijo necesita una leche adaptada?

Ya sabéis que el tema de la nutrición me preocupa bastante porque no siempre estoy segura de estar haciéndolo bien con mis hijos, sobre todo porque el ritmo de vida que llevamos a veces no nos deja tiempo ni de pensar en qué voy a hacer de cenar... Por eso procuro estar al día de los estudios que van saliendo, y en la medida de lo posible seguir las recomendaciones de los expertos, aunque a veces sea un poco desastre y sucumba a la tentación ir a lo fácil, ya me entendéis...

Pero luego veo en las estadísticas que los niños españoles comen demasiada carne, pocos cereales y muy pocas verduras, hortalizas, frutas, legumbres y pescado, que sólo el 25% de los niños tienen una dieta equilibrada, y que el 20% llevan una alimentación desastrosa y, sinceramente, no quiero que mis hijos formen parte del segundo grupo. Así que me he propuesto esforzarme más en mejorar la alimentación de mi familia porque una ingesta excesiva de calorías e inadecuada de micronutrientes podría poner en riesgo nuestra salud.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3cegVi291qfhdaz_540.png)

Llamamos micronutrientes al hierro, zinc, yodo, vitaminas A y D, omega3... cuya deficiencia en el organismo podría ocasionar estas enfermedades:
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3cfy35cS1qfhdaz_540.png)

Para corregir los desequilibrios nutricionales lo que hay que hacer es cambiar los hábitos alimentarios, reeducando a nuestros hijos para enseñarles a comer correctamente. Este proceso puede ser lento, pero merece la pena llevarlo a cabo con paciencia, y enseñarles el gusto por los alimentos saludables que quizá no estén acostumbrados a comer, pero que son necesarios. 

Para ello, Marian Garcia, más conocida las redes como [Boticaria Garcia](https://boticariagarcia.com), nos sugiere integrar a los niños en el proceso de la alimentación haciendo de la visita al super para hacer la compra una actividad en familia donde aprender a seleccionar los alimentos más nutritivos diferenciándolos de los no saludables.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3d2r1uxq1qfhdaz_540.png)

Pero hay veces que se detectan carencias importantes de micronutrientes en niños que podrían poner en riesgo su salud, y sería necesario un efecto choque por lo que además de comenzar este proceso de reeducación alimentaria, habría que suplementar la dieta bien con comprimidos o con [alimentos fortificados, como las leches adaptadas o de “crecimiento”](https://www.lechepuleva.es/productos/leche-puleva-max). Pero ¡ojo!  nunca como sustitutivos de una dieta equilibrada o para nuestra comodidad, sino como medida de ayuda mientras se logra instaurar una dieta correcta.

## ¿Y por qué precisamente leche?

Os preguntaréis por qué es precisamente la leche la que se fortalece con micronutrientes, y no otro alimento.

Pues porque así aprovechamos y de paso están ingiriendo también otros nutrientes importantes, que en otros alimentos no encontraríamos en igual cantidad, como por ejemplo el calcio, proteínas de alta calidad y vitaminas. Porque como dice [Luis Zamora](https://twitter.com/search?q=luis%20zamora&amp;src=typd), nutricionista del programa Más Vale Tarde, de la Sexta:

“No hay un vaso de ninguna cosa que contenga lo mismo que un vaso de leche”
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3djhqg601qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3dkp93tp1qfhdaz_540.png)

**Especial atención al calcio**
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3e4bByFL1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3ds0AGMe1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3e83myRE1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3dt4k9Yj1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3dviLq1y1qfhdaz_540.png)

## Mitos sobre la leche que tenemos grabados a fuego

Alguno de estos mitos o creencias erróneas que repetimos, y nos repiten nuestras madres, incluso abuelas, sin saber muy bien por qué, nos fueron desentrañados por la Boticaria García quien, con la ironía que la caracteriza, nos explicó su origen y por qué tenemos que quitárnoslos de la cabeza.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3ehz7sh91qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3ejm4C8v1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3ekiNu0j1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3elwM9ql1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3eeet44z1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3emnkLnD1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3eqoVd9g1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3es1vGNZ1qfhdaz_540.png)![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3et2FjpC1qfhdaz_540.png)

Según nos contó el Dr. Federico Lara, del Instituto Puleva de Nutrición, otro bulo que corrió hace un tiempo fue que algunos productos lácteos, comoel de la foto, contienen una cantidad de azúcar errónea al no distinguir entre azúcares añadidos (en realidad sólo un terrón) y azúcares naturalmente presentes en la lactosa (resto de terrones). Esta creencia errónea viene inducida porque las normas de etiquetado no contemplan distinguir entre estos dos tipos de azúcar, de manera que parece que todo el azúcar que contiene el producto lo añade el fabricante, sin tener en cuenta que muchos alimentos contienen azúcar de forma natural. De hecho, la leche de crecimiento Puleva Peques 3 no contiene azúcares añadidos, y esto se debe al compromiso de la marca de eliminar paulatinamente el azúcar añadido de sus productos, incluídos los batidos de sabores, que actualmente tienen un 30% menos de azúcar, estando previsto en una segunda fase la reducción del 50%, para terminar con el objetivo a medio plazo de ofrecer batidos sin azúcar. 
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3rpz5bQQ1qfhdaz_540.png)

La lactosa es el azúcar natural de la leche que interviene en el funcionamiento cerebral y en la absorción del calcio, por eso es tan necesaria para el organismo, y no tiene nada que ver con el azúcar añadido.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3s7xjTBR1qfhdaz_540.png)

El último mito que tenemos gravado a fuego es que la leche nos sienta mal, pero está demostrado que la mayoría de personas digiere un vaso de leche sin problemas, y en cuanto a la alergia a la proteína de la leche de vaca, se oye mucho hablar de ella, pero sólo un 2% de los niños la padecen y suelen superar el problema antes de cumplir los 2 años.

## ¿Leches adaptadas también para Embarazadas?

Si hablamos de la dieta de la mayoría de mujeres embarazadas, los datos son también desoladores, y no olvidemos que la mala alimentación perjudicará tanto a la madre como al futuro bebé, que podrá desarrollar enfermedades en el futuro fruto de unas carencias básicas de micronutrientes.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3r5h9RYn1qfhdaz_540.png)

Según los últimos estudios la alimentación de las embarazadas presenta insuficiencias y desequilibrios que es importante corregir.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3r7bxjWw1qfhdaz_540.png)

Esto tiene como repercusión una ingesta insuficiente de nutrientes clave para el correcto desarrollo del feto y para prevenir enfermedades en el futuro, además de ocasionar también problemas en la madre, como osteoporosis, si le falta calcio.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3rafzwYg1qfhdaz_540.png)

El ácido fólico ayuda a la correcta formación del tubo neural, que se forma a los 21 días de gestación, por tanto, si nuestros depósitos estuvieran medio vacíos no daría tiempo a llenarlos una vez que nos enteramos de la noticia del embarazo, sino que será necesario tomar este nutriente al menos dos meses antes de quedarnos embarazadas.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3sboMf6J1qfhdaz_540.png)

Siete de cada diez mujeres embarazadas suplementan su dieta con comprimidos, pero consumir alimentos fortificados es una forma más natural de ingerir los micronutrientes, y puede resultar más placentero que tomarse una pastilla. Existen [productos lácteos en el mercado especiales para embarazadas](https://www.lechepuleva.es/web/puleva-mama?utm_source=lechepuleva&amp;utm_medium=slider-home&amp;utm_content=site-mama&amp;utm_campaign=MAMA) que pueden resultar atractivos por sus sabores, por ejemplo Puleva tiene uno de chocolate.
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3rddjwPT1qfhdaz_540.png)

Por tanto yo sí que os recomendaría suplementar la dieta antes y durante el embarazo, ya sea con comprimidos o con leche fortificada y, personalmente, pienso que si hubieran existido este tipo de productos en la época en la que estaba embarazada quizá me hubiera apetecido más tomármelos a modo de tentempié a media mañana que estar tragando pastillas, pero eso ya es opinión personal.

## Crónica de sociedad del Blog Trip Puleva

Ya sabéis que un blog trip es algo más que asistir a charlas, compartir conocimiento y comunicarlo; se trata de convivir, vivir experiencias con otras bloggers, intercambiar impresiones con la marca, “tratar de tú” a grandes profesionales y expertos, que demuestran respeto y admiración por nuestro trabajo, que lo valoran realmente...

Puleva ha demostrado con esta iniciativa humildad para escucharnos, debatir, responder preguntas, a veces difíciles, aceptar sugerencias y ponerse a disposición del consumidor aceptando críticas sin ningún pudor. Tengo que reconocer que esta valentía no la muestran todas las marcas.

Os dejo unas fotos que muestran lo bien que lo pasamos, el buen rollo que había entre las compañeras bloggers, el ambiente familiar del que os hablaba, y lo bien que lo pasaron mis fieras, que visitaron la Alhambra por primera vez, y que se la llevaron fotografiada enterita. 
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3uzyjKHx1qfhdaz_540.jpg)

Charlas con grandes mujeres:
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3v00M09R1qfhdaz_540.jpg)

Terraceo en buena compañía y buena comida con Granada a nuestros pies:
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3v03VOAm1qfhdaz_540.jpg)

Descubriendo las maravillas de la Alhambra y queriendo capturarlas todas con fotos:
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3vgiy0hy1qfhdaz_540.jpg)

Creo que este viaje va a quedar en el recuerdo de mis fieras para siempre. Muchas gracias a Puleva de parte de ellos también!
![](/images/posts/leches-adaptadas-embarazadas-ninos-puleva/tumblr_inline_pm3vgiwVup1qfhdaz_540.jpg)

Nota: Este artículo resultó ganador en los III Premio de Comunicación “Grandes retos de la Alimentación Materno Infantil en el Nuevo milenio” convocados por el Instituto Puleva de Nutrición.

![premio puleva](/images/premio_comunicacion.png)