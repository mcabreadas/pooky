---
layout: post
slug: propositos-de-ano-nuevo-2021
title: Dale la vuelta a tu vida en 2021 con estos nuevos propósitos de año nuevo
date: 2022-01-04T23:00:00.000Z
image: /images/posts/propositos/p-propositos.png
author: ana
tags:
  - trucos
---
Termina un año singular, distinto a muchos de los vividos y por lo tanto, en aquel remoto diciembre de 2019, difícil de prever. La tradicional lista de propósitos de año nuevo se vio sacudida por la emergencia de un mundo que estaba cambiando. El planeta fue otro, y nosotros también. Pero, ¿qué fue de nuestros propósitos?

Te recomiendo nuestro [podcast sobre consejos para hacerte propositos que si puedas cumplir](https://madrescabreadas.com/2022/09/29/propositos-de-vida-septiembre-ano-nuevo/) y mantener en el tiempo.

## Por qué nos ponemos nuevos objetivos en año nuevo

Cuando un año termina, sentimos que la vida nos da la oportunidad de volver a comenzar. La tradición de los propósitos de año nuevo se remonta a la Babilonia de hace 4 mil años, con el akitu o "corte de cebada". Iniciaba la primavera y con ella los deseos de bienestar y prosperidad elevados a los dioses. Con el calendario gregoriano, desde 1582 los deseos de año nuevo cambian de marzo a enero, pero la antigua tradición se mantiene.

## ¿Cumpliste los propósitos de tu año anterior?

![listadedeseosañonuevo](/images/posts/propositos/listadedeseosanonuevo.png)

El 2020 seguro fue un reto para los estrictos planificadores. Por más realistas que fuesen los propósitos, se enfrentaron a lo increíble e inevitable. ¿Se podía prever que por meses desaparecerían los restaurantes o los vuelos internacionales? La realidad superó esta vez los objetivos para año nuevo más conservadores y nos reafirmó la lección de que los logros están hechos de constancia, paciencia y dedicación. Quienes a pesar de la sorpresa de la pandemia cumplieron su lista de deseos, felicitaciones, alcanzaron un grado de sabiduría envidiable.

## Lista de propósitos de año nuevo más comunes

### Propósito de hacer ejercicio

Todos los años encabezamos la lista de propósitos con un firme objetivo, atender la salud en dos aspectos esenciales: la alimentación y nuestro cuerpo, lo que se traduce en un ahora sí haré ejercicio, empiezo una dieta o comeré más sano.

![propositoanonuevo](/images/posts/propositos/propositoanonuevo.png)

### Propósito de viajar

Otro de los deseos que con frecuencia se proyectan es un viaje a aquel lugar que siempre hemos soñado, pero este año si no lo cumplimos pede que sea por la pandemia, así que no deberíamos empeñarnos mucho en él.

### Propósito de aprender inglés

También aprender un idioma o replantearnos un mejor trabajo...

Se trata de propósitos, deseos, anhelos, puestos en papel para que no se los lleve el viento y se fijen en nuestra mente.

### Propósito de dejar de fumar

Dejar de fumar, pasar más tiempo con la familia y los amigos, organizarnos mejor, culminar los estudios, cultivar un jardín... siguen siendo comunes en nuestro lista de propósitos.

## Nuevos propósitos de año nuevo que deberías incluir

Las eventualidades pusieron a prueba nuestra manera de hacer las cosas y entendimos bajo presión que a veces es necesario cambiar sobre la marcha, y así en vez de hundirnos, flotamos. En ese sentido, como propósitos para este nuevo año, proponemos:

![listadepropositosanonuevo](/images/posts/propositos/listadepropositosanonuevo.png)

### Propósito de reinventarte

La realidad cambia con increíble rapidez. Los entornos se modifican y las tecnologías se transforman, pero sin duda no al mismo ritmo que nos transforman interiormente. ¿Qué estamos haciendo para seguir el ritmo? Lo mejor será reinventarnos: darle la vuelta a la vida en este 2021.

### Propósito de aprender a cambiar

Movernos de nuestra zona de confort si no estamos conformes con lo que hemos logrado. La estabilidad era un valor que se atesoraba, hoy la realidad nos pide planificar varias opciones ante posibles eventualidades, con una meta macro que nos marque el camino.

### Plantear metas sencillas

Busquemos lo que podamos encontrar y hagamos lo que hagamos, comencemos por el más humilde de los principios. A todas partes se llega caminando, un paso tras otro. Seguro nos llevará más tiempo del que creíamos, pero la satisfacción de llegar lo compensará todo.

Feliz Año Nuevo!

Si te ha gustado comparte y suscríbete para no perderte nada!

*Foto de portada by Moritz Knoringer on Unsplash*

*Foto by Jakub Kapusnak on Unsplash*

*Foto by Brooke Lark on Unsplash*

*Foto by Jeshoots.com on Unsplash*