---
layout: post
slug: primera-regla
title: Cómo acompañar a tu hija en su primera menstruación
date: 2019-09-21T18:19:29+02:00
image: /images/posts/primera-regla/p-adolescente-pensativa.jpg
author: maria
tags:
  - adolescencia
---
Mucho han cambiado las cosas desde que nosotras tuvimos nuestra primera regla. Seguro que todas os acordáis del momento exacto en que os vino... dónde estabais, con quién, lo que sentisteis, la primera persona a la que se lo dijisteis... ¿A que sí? Pues debemos recordar todo esto porque nos va a ayudar a saber cómo afrontarlo con nuestras hijas y porque no hay mejor persona para acompañarlas en este cambio tan importante como nosotras, y no hay mejor manual que nuestra empatía y recuerdo de cómo lo vivimos o cómo nos hubiera gustado vivirlo, qué nos hubiera gustado que nos dijeran...

Creo que la información es clave para esta generación de adolescentes, y más teniendo en cuenta la época digital de "sobreinformación" que les ha tocado vivir, así que las claves son: **empatía, optimismo, cariño e información** en el momento y también con anterioridad.

## Adelántate

Es cierto que en el colegio se aborda el tema de la reproducción humana (creo que en quinto de Primaria), pero no debemos confiar todo el peso de este tema en los libros. Mi consejo es que nos adelantemos a ello, o al menos estemos atentas y lo abordemos cuando lo estén estudiando porque nadie mejor que nosotras, que conocemos a nuestras hijas, para darles la información que necesitan en cada momento según su madurez, las preguntas que les surjan... Y para eso debemos tener una relación fluida con ellas desde que nacen y mantenerla en todas las etapas de su vida. Debemos estar siempre dispuestas a escuchar y a responder con naturalidad, incluso sacar el tema si vemos que les preocupa pero no se atreven a plantearlo por vergüenza o timidez.

## Naturalidad ante todo

Es decir, todo lo contrario a cómo nos lo plantearon seguramente a la mayoría de nosotras. 

Fuera tabúes y fuera prejuicios. Ellas saben más de lo que imaginas, y si te ven hablando del tema como quien habla de las croquetas tan ricas que te han salido, lo normal es que también se lancen al ruedo y pregunten todo lo que les inquieta y expresen los sentimientos que le provoca.

Este [diccionario de palabras adolescentes](https://madrescabreadas.com/2021/01/24/palabras-jerga-adolescentes/) te va a ayudar a hablar en su idioma o, al menos, a echaros unas risas para establecer una mayor conexion en la comunicacion.

## Sé positiva

Sí, en este tema también debemos transmitirles buenas vibraciones, a pesar de que muchas veamos cosas negativas, (y os lo está diciendo alguien que ha sufrido y sufre mucho con la menstruación desde sus 13 años) pero aún así, [debemos plantearlo en positivo](https://madrescabreadas.com/2017/10/22/adolescencia-positiva/) porque para ellas es un cambio tan grande que sólo se puede asemejar, por lo transcendente en cuanto a cambio de vida, a cuando [pasaron de llevar pañal a no llevarlo](https://madrescabreadas.com/2016/06/19/cuandopanalbebe/), o cuando tuvieron que empezar a ir al colegio. 

Además, no a todas las mujeres les supone un quebranto de su rutina tan grande, incluso algunas tan sólo sienten una ligera molestia, por eso no debemos condicionarlas ni meterles el susto en el cuerpo, más bien hay que explicarles los cambios tan bonitos que va a experimentar su cuerpo, la maravilla de convertirse en una mujer, lo afortunadas que somos de poder engendrar vida, alimentar con nuestro cuerpo a un bebé, lo que podemos sentir en cada fase del cliclo...

![noria](/images/posts/primera-regla/noria.jpg)

Nunca poner el acento en lo negativo, aunque sí que deben conocerlo, pero que sea siempre con una sonrisa. 

Y cuando llegue el momento, lo primero es un abrazo y felicitarla porque ya es una mujer, y las mujeres somos poderosas y fuertes.

Recuerdo la reacción de mi padre cuando mi madre y yo le dijimos que me había venido el periodo: me dio el abrazo más sentido y fue la primera vez que lo vi llorar y era de la emoción. Sólo en ese momento comprendí la importancia de lo que me acababa de suceder. Ahora entiendo sus lágrimas.

## Cuestiones prácticas

1. Es fundamental que vaya preparada para cuando le venga su primera regla, que la espere sin sobresaltos y sepa exactamente qué hacer y a quién avisar porque esto le va a dar a nuestra hija la seguridad que necesita para afrontar ese momento con calma y sin dramas. Una buena idea es preparar con ella un [kit menstrual](https://madrescabreadas.com) y tenerlo a mano para cuando sea necesario.
2. Por eso, a partir de cierta edad en la que veamos que empieza a cambiarle el cuerpo (cada madre conoce a su hija, no se puede decir una edad exacta porque cada niña es un mundo pero por ejemplo los 11 o 12 años) además de hablarle, como hemos comentado anteriormente, debemos mostrarle una compresa, cómo se pone y sugerirle que lleve siempre una en la mochila del cole. 
3. También podemos introducirlas en el uso del salvaslip, ya que a esa edad empiezan a tener flujo y muchas no sabrán ni lo que es (este sería un buen momento para empezar a hablar del tema y prepararlas para lo más o menos inminente).
4. Que observen su flujo es una buena idea, y si ven que cambia de color os lo pueden mostrar para que podáis orientarlas.
5. Mostraros totalmente accesibles para que os llamen si no estáis en casa cuando suceda, y darles una segunda opción de persona de contacto por si no os localizan ayudará a que no entren en pánico si les viene la regla cuando estén solas (aunque si habéis hablado con ellas como os he comentado seguramente estén tranquilas y lo tomen con naturalidad).
6. Tened a mano algún analgésico para el dolor por si hiciera falta, y tampones mini de los que os parezcan más fáciles de poner por si sucede en verano y le apetece bañarse.

![infusion](/images/posts/primera-regla/infusion.jpg)

Sobre todo, creo que lo más importante es que perciban normalidad, que nada tiene por qué interrumpirse ni tiene por qué interferir de forma importante en su vida, siempre y cuando no les duela demasiado o tengan otro tipo de inconvenientes, pero que no suele ser lo normal, en cuyo caso es aconsejable ir a un médico para descartar cualquier tipo de problema.

## Pueden conseguir lo que se propongan

Y, por supuesto, que la regla no les condicione sus sueños ni les coarte en absoluto sus aspiraciones porque ellas podrán lograr lo que se propongan, siempre y cuando trabajen duro por ello y luchen por conseguirlo salvando cualquier obstáculo que se les presente en la vida porque es verdad que las mujeres lo tenemos más difícil, pero también es cierto que tenemos agallas para llegar donde nos propongamos.

*Foto de portada by Allie Smith on Unsplash*
*Foto 1 by Marco Secchi on Unsplash*
*Foto 2 by Alejandro Barba on Unsplash*