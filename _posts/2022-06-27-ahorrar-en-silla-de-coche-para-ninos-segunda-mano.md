---
layout: post
slug: ahorrar-en-silla-de-coche-para-ninos-segunda-mano
title: ¿Es seguro comprar una silla de coche de segunda mano?
date: 2022-06-27T21:10:43.533Z
image: /images/uploads/silla-de-coche-para-ninos-de-segunda-mano.jpg
author: faby
tags:
  - puericultura
---
En la mayoría de las compras que efectuamos, buscamos el equilibrio entre la calidad y la economía. Y en muchas ocasiones lo conseguimos. A veces es posible obtener productos de segunda mano, que permiten disfrutar de sus características sin tener que invertir mucho dinero. Pero, **¿será buena idea hacer esto al comprar sillas de coche para niños?**

En este artículo veremos por qué es mejor obtener una silla de coche nueva. Además, mencionaremos algunas medidas que se pueden tomar si solo es posible comprar una de segunda mano.

## [](<>)¿Por qué comprar una silla de coche nueva?

**Es normal que las sillas que ya han tenido uso por cierto tiempo se desgasten**. Eso fácilmente puede hacer que disminuya su capacidad para proteger la vida del niño, que al final es siempre lo más importante. Y hay otros factores a tomar en cuenta al comprar una silla de coche para niños. Entre ellos resaltan los siguientes:

* **Daños**. Al usarse por mucho tiempo, la silla puede haber sufrido golpes que no están a simple vista. De ser así, no ofrecería seguridad completa.
* **Cuidado**. Dependiendo del cuidado, o la falta de este, la silla también puede no ser segura. Incluso si está bien cuidada, puede ser inservible después de varios años.
* **Falta de guía**. Al comprar una silla nueva, contamos con un manual para guiarnos tanto para la instalación como para su mantenimiento. No así con las de segunda mano.
* **Normativas de seguridad**. Las normas de seguridad van avanzando, siempre para dar mayor seguridad a todos, pero una silla que tenga varios años de uso puede no seguir actualizada.

## [](<>)¿Qué hacer si se compra una silla de coche para niños usada?

![Precauciones al comprar una silla de coche para niños usada](/images/uploads/precauciones-al-comprar-una-silla-de-coche-para-ninos-usada.jpg "Precauciones al comprar una silla de coche para niños usada")

[](<>)Como hemos visto, el riesgo puede ser mucho mayor al beneficio por lo que, en caso de tener la capacidad para comprar una silla nueva, no hay que dudar en hacerlo. Pero, ¿Qué pasa cuando la única opción que la persona se puede permitir es una silla de coche usada? Aunque no es la mejor alternativa, **siempre se puede optar por ello tomando en cuenta varios consejo**s.

### ➤ Revisar cada detalle de la silla

Puede ser que, aunque no se vea con facilidad, la silla presente deterioro o efectos de golpes fuertes, como por ejemplo, los que se producen en un accidente. Hay dos maneras de salir de dudas. La primera es **hacer una revisión completa de la silla al momento de la compra**, de ser posible frente al vendedor. Revisar cada detalle de esta permitirá verificar que está en buen estado.

La otra alternativa es **preguntar al vendedor sobre el uso y daño que ha sufrido la silla**. De esta manera, se tendrá una idea general de la seguridad que puede brindar al niño. Dentro de los detalles importantes destaca el saber qué tiempo de uso tiene la silla, para lo cual se puede revisar la etiqueta que esta posee. Si tiene más de 6 años, no es buena idea comprarla.

### ➤ Asegurarse de que está completa

Este tipo de sillas cuenta con muchas piezas, algunas muy pequeñas, por lo que es vital **verificar que estén todas y en buen estado**. Esto permitirá que la instalación y uso sean completamente seguros.

### ➤ Conseguir el manual de usuario

Es posible que, por el paso del tiempo, la persona que vende la silla ya no tenga el manual con el que esta vino, pero siempre es posible **conseguirlo en alguna página web**, preferiblemente la del fabricante. Esto también ayuda a instalarla y usarla según las indicaciones, lo que permite que sea más segura y que su vida útil se extienda.

### ➤ Confirmar que esté actualizada

Si la silla no cumple con las normas aprobadas para la seguridad de los niños, no se podrá usar para circular con ellas. De allí la importancia de asegurarse de que la silla a comprar cumpla con las normas vigentes, conocidas como **[ECE R44/04](https://www.fundacionmapfre.org/educacion-divulgacion/seguridad-vial/sistemas-retencion-infantil/sillas-mas-seguras/normativa/homologacion-ece-r4404/) y ECE R129**. Se puede encontrar esta información en la etiqueta naranja.

Si se puede comprar una silla nueva, mucho mejor, aquí te recomendamos las [sillas de coche mas estrechas del mercado](https://madrescabreadas.com/2021/05/11/sillas-de-coche-estrechas/). Pero sino, entonces es vital que se revise a fondo la silla para **confirmar que tiene la capacidad de proteger la vida del niño.**