---
author: maria
date: '2020-08-19T10:10:29+02:00'
image: /images/posts/prensa/p-pizarra.jpg
layout: post
tags:
- derechos
- revistadeprensa
title: 'Colaboración con Bebés Más: ¿Puedo negarme a llevar a mi hijo al colegio por
  miedo al COVID?'
---

En mi colaboración con Bebés y Más analizamos hasta qué punto es legal negarme a llevaras hijo al colegio por miedo al COVID.


<a href="https://www.bebesymas.com/educacion-infantil/vuelta-al-cole-puedo-negarme-a-llevar-a-mi-hijo-al-colegio-miedo-al-covid" class='c-btn c-btn--active c-btn--small'>Leer artículo completo en Bebés y Más</a>


Si te ha gustado comparte y suscríbete al blog para no perderte nada.