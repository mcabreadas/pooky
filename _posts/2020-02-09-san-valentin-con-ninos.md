---
layout: post
slug: san-valentin-con-ninos
title: 12 ideas para celebrar San Valentín con niños
date: 2020-02-09T16:19:29+02:00
image: /images/posts/san-valentin/p-nino-corazon.jpg
author: .
tags:
  - 0-a-3
  - ""
  - planninos
  - 3-a-6
  - 6-a-12
---
San Valentín con niños puede ser una fiesta de lo más divertida para las familias. No renuncies a vivir un momento especial con tu pareja porque no tengáis con quién dejar a los niños o porque no os apetezca buscar canguro (esa noche están muy solicitados).

La idea de escribir sobre ideas para celebrar **el Día de los Enamorados con niños** me ha surgido cuando recordé la de veces que nos quedamos en casa mi marido y yo en esta festividad cuando los tres peques eran muy peques, y nos costaba un mundo organizarlos, así que preferíamos preparar algo en casa divertido que andar con líos de logística.

Un año recuerdo que preparamos una fondue de queso con un buen vino "servida" por unos camareros muy especiales que nos pusieron hasta velitas en la mesa decoraciones varias, y fue bastante divertido. Después, se fueron a la cama prontito y tuvimos nuestro momento tranquilos también.

Recuerdo que **nos vestimos elegantes**, como si fuéramos a salir. Esto, aunque parezca una tontería cambió totalmente el clima de la reunión, **y le dio un toque especial** que a los niños les encantó; Estaban entusiasmados de ser parte de esta velada romántica!

Os voy a dar algunas ideas que a veces hemos hecho en este día tan especial, y otras que se me ocurren que pueden serviros para celebrar el día de los enamorados en familia.

## Decoración colaborativa

Podéis poner la casa bonita para este día recogiendo flores y ramas y haciendo unos ramos bonitos para colocarlos en sitios estratégicos del salón, que crearán un ambiente muy romántico.

![jarron-flores-san-valentin](/images/posts/san-valentin/jarron-flores.jpg)

También podéis hacer manualidades juntos, como por ejemplo **corazones de cartulina con purpurina y fotos pegadas de la familia**, que podéis colgar con un hilo por el salón, u otros [detalles hand made de San Valentín](https://www.pinterest.es/decobeltran/ideas-diy-san-valentin/).

![corazones de cartulina](/images/posts/san-valentin/manualidades-san-valentin.jpg)

También tenemos la opción de comprar hermosos decorados alusivos al amor que combinarían muy bien con las manualidades que ya hemos hecho.

[![](/images/uploads/colgante-de-corazon.jpg)](https://www.amazon.es/Colgante-Arpillera-Valentín-Decoración-Valentines/dp/B09JZ2YK8G/ref=sr_1_17?keywords=decoración+san+valentin&qid=1644581932&sprefix=decoración+san%2Caps%2C675&sr=8-17&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Colgante-Arpillera-Valentín-Decoración-Valentines/dp/B09JZ2YK8G/ref=sr_1_17?keywords=decoración+san+valentin&qid=1644581932&sprefix=decoración+san%2Caps%2C675&sr=8-17&madrescabread-21) (enlace afiliado)

## Desayuno especial

Dar los buenos días con un desayuno romántico preparado entre todos con **galletas en forma de corazón** decoradas con fondant puede resultar súper divertido. Os dejo el enlace de unas [galletas decoradas](https://madrescabreadas.com/2015/12/07/decora-galletas-de-navidad-en-familia/) que hice con los niños en Navidad que os puede servir de guía para hacerlas igual, pero cambiando los moldes de Navidad por cortadores con forma de corazón.

[![](/images/uploads/cortador.jpg)](https://www.amazon.es/gp/product/B07DHP2XMK/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B07DHP2XMK&linkId=23b5e182d1c96a6888073c5a815f444d) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/B07DHP2XMK/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=madrescabread-21&creative=24630&linkCode=as2&creativeASIN=B07DHP2XMK&linkId=23b5e182d1c96a6888073c5a815f444d) (enlace afiliado)

![desayuno romantico](/images/posts/san-valentin/desayuno-san-valentin.jpg)

Otra opción es preparar **fresas con chocolate fundido** por encima aprovechando que estamos en plena temporada y que están deliciosas. ¿Y por qué no? También podemos hacernos de una fundidora de chocolate para hacer la ocasión mas romántica y especial.

[![](/images/uploads/fuente-de-chocolate.jpg)](https://www.amazon.es/Bestron-Fuente-Chocolate-Alturas-Diseño/dp/B087DKFBSC/ref=sr_1_19?keywords=chocolate+para+fundir&qid=1644583234&sprefix=chocolate+para%2Caps%2C552&sr=8-19&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Bestron-Fuente-Chocolate-Alturas-Diseño/dp/B087DKFBSC/ref=sr_1_19?keywords=chocolate+para+fundir&qid=1644583234&sprefix=chocolate+para%2Caps%2C552&sr=8-19&madrescabread-21) (enlace afiliado)

## Decorad la mesa

Es momento de poner una mesa muy bonita! Deja que sean ellos quienes decidan cómo decorarla y te sorprenderá su imaginación.

Podéis usar **las típicas velas que nunca fallan**, piñas, o conchas marinas, usar cintas para poner unos lazos en las sillas diseñarías tarjetas con el nombre de cada uno para colocar en su sitio para hacer de la cena algo más especial.

Como siempre pensamos en la seguridad de los más consentidos en la casa, podemos optar por usar velas de led, las cuales no suponen peligro en ningún momento.

[![](/images/uploads/velas-led.jpg)](https://www.amazon.es/parpadeante-propuesta-matrimonio-Valentín-aniversario/dp/B08QZX5X6C/ref=sr_1_11?keywords=velas+san+valentin&qid=1644583685&sprefix=velas+san%2Caps%2C526&sr=8-11&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/parpadeante-propuesta-matrimonio-Valentín-aniversario/dp/B08QZX5X6C/ref=sr_1_11?keywords=velas+san+valentin&qid=1644583685&sprefix=velas+san%2Caps%2C526&sr=8-11&madrescabread-21) (enlace afiliado)

## Preparad el menú entre todos

**A los niños les encanta cocinar con nosotros**, ¿lo habéis probado? Aprovechad porque luego no siempre es asnillos y cuando crecen ya no les hace tanta ilusión. Así que hagámosles partícipes de los preparativos para que se sientan meza importante de la celebración, y dejémoslos que se pringuen las manos ayudándonos con el menú de la cena.

Y si se visten como chefs profesionales, será una experiencia inolvidable.

[![](/images/uploads/disfraz-chef.jpg)](https://www.amazon.es/Rubie-154662m-Traje-Disfraz-Talla/dp/B00LA72UXC/ref=sr_1_3?keywords=disfraz+chef+niño&qid=1644586744&sprefix=disfraz+che%2Caps%2C460&sr=8-3&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Rubie-154662m-Traje-Disfraz-Talla/dp/B00LA72UXC/ref=sr_1_3?keywords=disfraz+chef+niño&qid=1644586744&sprefix=disfraz+che%2Caps%2C460&sr=8-3&madrescabread-21) (enlace afiliado)

Incluso puedes tomar apunte sobre [5 dulces de San Valentín fáciles](https://madrescabreadas.com/2021/02/14/dulces-de-san-valentin-faciles/) para hacer con tus peques y pasarla en grande.

[![](/images/uploads/barbie-chef.jpg)](https://www.amazon.es/Ciao-Barbie-Chaqueta-disfraz-11667/dp/B07MRV3SPL/ref=sr_1_6?keywords=disfraz+chef+niño&qid=1644586744&sprefix=disfraz+che%2Caps%2C460&sr=8-6&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Ciao-Barbie-Chaqueta-disfraz-11667/dp/B07MRV3SPL/ref=sr_1_6?keywords=disfraz+chef+niño&qid=1644586744&sprefix=disfraz+che%2Caps%2C460&sr=8-6&madrescabread-21) (enlace afiliado)

![reposteria-san-valentin](/images/posts/san-valentin/reposteria-san-valentin.jpg)

## Unos camareros muy especiales

Y ya, si les dejáis que hagan de metre y camareros si tenéis varios, se lo van a pasar pipa sirviendo la mesa y cuidando los detalles.

[![](/images/uploads/traje-de-sirvienta.jpg)](https://www.amazon.es/Rubies-sirvienta-victoriana-delantal-fregona/dp/B00B34O3KK/ref=pd_sbs_21/262-9764483-4987731?pd_rd_w=yeBAE&pf_rd_p=f9559607-0e83-4976-8590-72740e10e24e&pf_rd_r=073TJ8KG16HKEHBRWHWB&pd_rd_r=f1e0dc5e-d678-48b8-b464-6e84c631718c&pd_rd_wg=2Boyj&pd_rd_i=B00J2P1FXU&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Rubies-sirvienta-victoriana-delantal-fregona/dp/B00B34O3KK/ref=pd_sbs_21/262-9764483-4987731?pd_rd_w=yeBAE&pf_rd_p=f9559607-0e83-4976-8590-72740e10e24e&pf_rd_r=073TJ8KG16HKEHBRWHWB&pd_rd_r=f1e0dc5e-d678-48b8-b464-6e84c631718c&pd_rd_wg=2Boyj&pd_rd_i=B00J2P1FXU&psc=1&madrescabread-21) (enlace afiliado)

Nosotros lo hicimos cuando eran más pequeños y les resultó divertidísima la situación. Y a nosotros nos encantó la experiencia de verlos cuidar los detalles y ocuparse de todo a su manera. Entonces aprendimos que nos pueden sorprendernos si les damos la oportunidad de hacer cosas que normalmente no hacen.

## Amigo invisible casero

Algo que me encantaría hacer en casa es un amigo invisible de San Valentín, pero con regalos hechos a mano por cada uno, por ejemplo, una camiseta pintada con rotuladores de tela, o un collages de fotos de la familia, o un marco decorado...

![regalo-san-valentin](/images/posts/san-valentin/regalo-san-valentin.jpg)

## Karaoke romántico-familiar

Para después de la cena, y si al día siguiente no hay cole, podéis hacer un Karaoke familiar con canciones románticas para dejar salir todo vuestro arte.

[![](/images/uploads/karaoke-.jpg)](https://www.amazon.es/RockJam-RJSC01-BK-Recargable-Bluetooth-micrófonos/dp/B06Y1JCB9T/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&crid=XCCY9RH3NCUC&keywords=karaoke&qid=1644587764&sprefix=kara%2Caps%2C823&sr=8-5&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/RockJam-RJSC01-BK-Recargable-Bluetooth-micrófonos/dp/B06Y1JCB9T/ref=sr_1_5?__mk_es_ES=ÅMÅŽÕÑ&crid=XCCY9RH3NCUC&keywords=karaoke&qid=1644587764&sprefix=kara%2Caps%2C823&sr=8-5&madrescabread-21) (enlace afiliado)

## Competición de Just Dance familiar

O si sois más bailongos que cantarines siempre podéis echar una competición al juego de la Wii Just Dance, que tan buenos momentos nos ha hecho pasar en casa.

[![](/images/uploads/just-dance-2022.jpg)](https://www.amazon.es/UBI-Soft-Just-Dance-SWITCH/dp/B097RQYZ46/ref=sr_1_1?crid=1NA7S716H5QC1&keywords=wii+just+dance+2022&qid=1644588000&sprefix=Wii+Just+Dance%2Caps%2C4596&sr=8-1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/UBI-Soft-Just-Dance-SWITCH/dp/B097RQYZ46/ref=sr_1_1?crid=1NA7S716H5QC1&keywords=wii+just+dance+2022&qid=1644588000&sprefix=Wii+Just+Dance%2Caps%2C4596&sr=8-1&madrescabread-21) (enlace afiliado)

## Pintar un cuadro entre todos

Si os van los planes más tranquilos o no queréis alterar a los peques demasiado antes de que se vayan a dormir, podéis comprar un lienzo en blanco y pintar un cuadro todos juntos, cada uno a su nivel; los más pequeños con pinturas de dedos plasmando su mano o su pie, o dibujando con sus deditos, y los mayores, completando con dibujos a su estilo.

[![](/images/uploads/kit-de-pintura.jpg)](https://www.amazon.es/Ravensburger-Estrellado-Creativo-Pinceles-Recomendada/dp/B097DRW118/ref=sr_1_47?keywords=lienzos+para+pintar&qid=1644588993&sprefix=lienzo%2Caps%2C470&sr=8-47&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Ravensburger-Estrellado-Creativo-Pinceles-Recomendada/dp/B097DRW118/ref=sr_1_47?keywords=lienzos+para+pintar&qid=1644588993&sprefix=lienzo%2Caps%2C470&sr=8-47&madrescabread-21) (enlace afiliado)

## Jugar al baúl de los recuerdos

Esta idea me encanta, y la pienso poner en práctica este año.

Se trata de poner en **una caja recuerdos de vuestra vida en común**, primero en pareja, y luego las primeras ecografías, u objetos de cada uno que os traigan recuerdos especiales. Puede ser un buen momento para mostrarles las fotos de vuestra boda o de viajes, videos...

[![](/images/uploads/cofre.jpg)](https://www.amazon.es/Mopec-W1601-Mensajes-Madera-Marrón/dp/B06XR99XSW/ref=sr_1_2?keywords=baúl+de+recuerdos&qid=1644588416&sprefix=baúl+de+re%2Caps%2C543&sr=8-2&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Mopec-W1601-Mensajes-Madera-Marrón/dp/B06XR99XSW/ref=sr_1_2?keywords=baúl+de+recuerdos&qid=1644588416&sprefix=baúl+de+re%2Caps%2C543&sr=8-2&madrescabread-21) (enlace afiliado)

## Intercambio de tarjetas con mensajes

Algo muy tradicional en San Valentín son las tarjetas con mensajes, así que podéis hacer tarjetas caseras y escribir una a cada miembro de la familia con un mensaje especial.

![tarjetas san valentin](/images/posts/san-valentin/tarjetas-san-valentin.jpg)

## Post it con notas en las paredes

Otra cosa que queda muy chula, y además decora, es pegar post it de notas en la pared formando un corazón, y dejar vuestros mensajes en ellos. Las pegatinas de corazones son idóneas para la decoración.

[![](/images/uploads/pegatinas-corazon.jpg)](https://www.amazon.es/Pegatinas-Kalolary-Autoadhesivas-Cumpleaños-Decoración/dp/B09KLJ53V1/ref=sr_1_16?keywords=san+valentin&qid=1644588704&sprefix=san%2Caps%2C555&sr=8-16&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Pegatinas-Kalolary-Autoadhesivas-Cumpleaños-Decoración/dp/B09KLJ53V1/ref=sr_1_16?keywords=san+valentin&qid=1644588704&sprefix=san%2Caps%2C555&sr=8-16&madrescabread-21) (enlace afiliado)

Después de estas 12 ideas para celebrar el Día de los Enamorados con niños ya no tenéis excusa para no pasar una velada de San Valentín de lo más especial en familia.

Ya me contaréis!

Nos vemos en las redes sociales! 

Si te ha gustado, comparte para que pueda ayudar a más gente!!! 

* *Photo portada by Anna Kolosyuk on Unsplash*
* *Photo by Kelly Sikkema on Unsplash corazones pared*
* *Photo by sheri silver on Unsplash dulces*
* *Photo by Leonardo Wong on Unsplash jarron flores*
* *Photo by Miroslava on Unsplash regalo*
* *Photo by Joshua Hoehne on Unsplash desayuno*
* *Photo by Allie Smith on Unsplash tarjetas*