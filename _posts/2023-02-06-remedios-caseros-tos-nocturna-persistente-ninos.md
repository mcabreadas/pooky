---
layout: post
slug: remedios-caseros-tos-nocturna-persistente-ninos
title: 5 remedios naturales para la tos que sí le gustarán a tu hijo
date: 2023-02-10T10:04:51.747Z
image: /images/uploads/depositphotos_226639028_xl.jpg
author: .
tags:
  - 3-a-6
  - ad
  - salud
---
Esta época está siendo especialmente dura para los resfriados, los mocos y las toses por su duración en el tiempo y por la intensidad de estos síntomas, tanto para nosotras como para nuestros hijos.

Mi hijo pequeño ya lleva este invierno varios procesos de constipado seguidos, y ya no sabemos si es el mismo o es que va reenganchando uno con otro. El caso es que desde que llegó el frío no ha estado parado.

Otras mamás me han comentado lo mismo; este año están tardando mucho en desaparecer los síntomas y los resfriados están siendo especialmente largos.

En nuestro caso el peque no es que haya tenido mucha fiebre ni haya estado grave, sino que lo más molesto para él ha sido la tos recalcitrante y persistente, sobre todo por la noche, hasta el punto de no dejarle dormir, ni a él ni a su hermano con el que comparte habitación, ni a mí, que le he aplicado todo tipo de remedios caseros para la tos nocturna, además de seguir el tratamiento de su pediatra, por supuesto.

## ¿Qué es bueno para la tos en la noche?

Por la noche la tos a veces se ceba con los niños porque entran en un bucle: cuanto más tosen más se les irrita la garganta, y cuanto más se les irrita, más les hace toser. Esto hace que se pongan nerviosos y tosan aún más.

Por eso lo principal es mantener la calma y armarse de paciencia.

Evitar ambientes secos es fundamental, por eso, si tienes calefacción, es una buena idea poner un recipiente con agua bajo el radiador del dormitorio para restablecer la humedad, incluso abrir un poco la ventana siempre con el niño bien tapado.

Mi pediatra me aconsejó para cuando la tos fuera muy persistente, que lo sacara a la terraza para que respirara el aire de la madrugada unos minutos siempre bien abrigado.

Evitar alimentos fuertes que provoquen reflujo, que haga que se irrite más la garganta siempre es aconsejable.

Ya sabes que los lavados nasales antes de dormir son fundamentales.

Y como consejo para mantener la paz familiar, si comparte habitación y tienes posibilidad, saca al otro niño durante unos días, porque puede llegar a ser agobiante para él no descansar bien por la noche.

## ¿Cómo quitar la tos con remedios naturales?

Hay infinidad de remedios caseros para aliviar la tos, aunque ya sabemos que no la curarán, porque para eso necesitamos un tratamiento médico, pero sí nos harán los días, y sobre todo las noches de resfriado, más llevaderas.

Hay algunos remedios para la tos que, aunque sean efectivos, resultan desagradables para los niños, y provocarán más una pelea que una solución.

Por ejemplo, no soportan los sabores "raros", por eso yo evito darles infusiones de jengibre, romero u otro tipo de hierbas con las que no están familiarizados.

![](/images/uploads/depositphotos_122524242_xl.jpg)

Tampoco es conveniente ponerlos a hacer gárgaras con agua y sal o bicarbonato que, aunque son efectivas porque desinfectan, les resultará difícil dominar la técnica, y quizá terminen tragando bastante líquido.

También hay otros remedios sólo para adultos, como el [ponche de mi abuelo para el resfriado](https://madrescabreadas.com/2013/10/14/remedio-casero-para-el-resfriado-el-ponche-del/), pero obviamente, desaconsejado para los peques por contener alcohol.

### 1-Piruletas Pharmachups

¿A qué niño no le gusta una piruleta, especialmente cuando está sin parar de toser?

Lo que más me gusta de las piruletas [Pharmachups](https://pharmachups.com) es su formato tan atractivo para los niños, y que están hechas con ingredientes de origen natural específicos para ayudar a suavizar la garganta.

Hay 3 tipos:

\-Tosichups, que sabe a cola, y ayuda a suavizar la garganta y la mucosa bucofaríngea porque lleva altea, hedera, hélix y ácido hialurónico. 

\-Farinchups con sabor naranja, que ayuda al bienestar de las vías respiratorias altas, y lleva tomillo y ácido hialurónico.

\-Inmunochups, que sabe a frutos del bosque, y lleva própolis, miel y ácido hialurónico para reforzar las defensas y reducir el cansancio y la fatiga.

### 2-Miel y limón a cucharaditas

Mezcla dos cucharaditas de miel con un chorro de limón, calienta 10 segundos en el microondas, y remueve.

Le puedes dar una cucharadita de las del café cuando tenga un golpe de tos, y dejárselo en la mesita de noche para que vaya tomando cuando lo necesite.

### 3-Cebolla partida sobre la mesita e noche

Cierto que el olor puede ser desagradable, pero al final te acostumbras, y suele funcionar muy bien con determinados tipos de tos nocturna. Este remedio me lo recomendaron cuando estaba embarazada, ya que no podía tomar casi ningún medicamento.

### 4-Caldo casero

Un buen caldo casero calentito de pollo o de verduras reconforta, aporta nutrientes, sienta bien y puede aliviar la garganta.

Si no tienes a mano, no te vayas a poner a hacer un caldo a las 3 de la madrugada; puedes usar [caldos caseros envasados](https://madrescabreadas.com/2022/07/27/mejores-caldos-envasados/) o le puedes dar un vaso de leche caliente con miel.

![](/images/uploads/depositphotos_224748986_xl.jpg)

### 5-Dormir semi incorporado

Mete una almohada fina o una toalla doblada bajo el colchón para lograr un ángulo de unos 20 grados aproximadamente. Esta posición suele aliviar bastante la tos, además de que disminuirá el [reflujo](https://madrescabreadas.com/2015/11/20/reflujo-bebes/) y la irritación que éste pudiera provocar en la garganta agravando el cuadro que ya tenga el peque.

Ojo, no funciona poniendo almohadas bajo la cabeza porque se trata de elevar el tronco, además de que le podría causar daño en el cuello.

Bueno, y el remedio más fulminante de todos, y el que más gustará sin duda a tu peque son tus mimos y atenciones... todos sabemos que el amor de mamá lo cura todo ;)

Espero haberte ayudado con mi experiencia en remedios naturales para la tos nocturna y persistente de los niños, y que se cure pronto tu peque.

*Fotos gracias a [Depositphotos](https://sp.depositphotos.com/)*