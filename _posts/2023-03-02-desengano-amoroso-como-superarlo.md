---
layout: post
slug: desengano-amoroso-como-superarlo
title: "Podcast: Desamor. La tribu de los corazones rotos"
date: 2023-03-02T12:21:18.570Z
image: /images/uploads/caratula-podcast-desamor.png
author: .
tags:
  - podcast
  - libros
---
Hoy hablamos de desamor. Recuerdo perfectamente mi primer desengaño amoroso allá en mi juventud, en mi primera juventud cuando despiertas a la vida y piensas en el amor como un ente ideal único y para toda la vida. 

Si nunca te han roto el corazón, has tenido suerte. En caso contrario, este libro es para ti. Porque el desamor, en cualquiera de sus versiones, te desgarra cuando te toca. Y es en ese momento cuando hay que hacer acopio de la fuerza necesaria para lograr recomponer el puzle de la vida a base de sus pedazos. 

A través de quince experiencias directas, cotidianas y conmovedoras, la tribu creadora de este libro te ofrece nuevos ángulos desde los que poder afrontar esta etapa con valentía y resiliencia. 

Un coro en el que se rompe el silencio combinando ilusiones y desilusiones, tristezas y esperanzas, empoderamiento, autodirección y ¿por qué no? sentido del humor. Quince voces muy distintas que te llevarán a entender el desamor de otra manera.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/5iLCoUx6CiU7J6tQTrrM91?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Hoy nos descabreamos con:

**Noeleen Fonseca** 

[@noeleenfonseca](https://www.instagram.com/noeleenfonseca/), coautora del libro (Des)amor. Su capítulo puedes leerlo gratuitamente en la [página de Amazon](https://amzn.to/3YfwBTB) (enlace afiliado). En él relata la historia que la convirtió en una de las primeras víctimas protegidas por la Ley de Violencia de Género.

Es asturiano-leonesa y madre soltera de una niña de 11 años. Tras licenciarse en ADE, se especializó en el campo del marketing digital y abrió un blog de maternidad con el que ganaría varios premios, justo antes convertirse en community manager para IKEA España.

Ha publicado [su propia página web de escritora](https://noeleenfonseca.com) y ya ha firmado su segundo contrato editorial, esta vez para la publicación de un libro que habla del empoderamiento femenino desde muy diversas perspectivas; Se trata de un libro de no ficción en el que relata cómo puede una mujer caer y mantener una relación con violencia machista y, sobre todo, cómo ella consiguió salir.

**Leti Moregal**

 [@letimoregal](https://www.instagram.com/letimoregal/). Libros: (Des)amor y cuento infantil ["Diez recetas monstruosas"](https://amzn.to/41IvhM4) (enlace afiliado), "Seis caminos de Diversidad Funcional". Este mismo año otra novela infantil verá la luz antes de Navidad.

Es madrileña, madre de dos peques, psicopedagoga y maestra de pedagogía terapéutica. En sus veinte años de docencia ha realizado cuentacuentos, animaciones a la lectura y dramatizaciones para acercar la literatura a los más pequeños. 

En sus cuentos prioriza la educación emocional. Tiene un [blog de literatura infantil y juvenil](https://letimoregal.blogspot.com) (en el que hace recomendaciones y reseñas literarias). Y en marzo de este año ganó un Concurso Literario con el cuento en verso “Un propósito imparable”, sobre la igualdad de oportunidades entre géneros.

**Beatriz Codina**

[@kalimour](https://www.instagram.com/kalimour/) libro (Des)amor, novela [Durante los aplausos](https://amzn.to/3SI8bkC) (publicada en dic 2022) (enlace afiliado). En marzo tb se lanza "Seis caminos de Diversidad Funcional". En 2023 verá la luz su primera novela infantil escrita junto a su hijo.

Es mallorquina y madre divorciada de un niño de nueve años.  Tras trabajar dieciocho años en banca, en enero decidió dejar la empresa para luchar por sus sueños y emprender su nuevo camino como escritora. 

Desde niña entretenía a sus amigas y a su hermana relatando historias y, desde siempre, supo que algún día escribiría un libro. Actualmente acaba de terminar su primera novela que verá la luz en breve y está sumergida en varios proyectos literarios, así como en crear páginas web de comparativa de productos y [su propio blog](https://beatrizcodina.com).

 **¿Cómo os conocisteis?** 

En un curso acerca del mundo editorial llamado MAPEA e impartido por Roger Domingo, director de varios sellos dentro del Grupo Planeta. 

**Contadme sobre vuestro libro. ¿Cómo surgió? ¿De qué trata?**

\-Desamor. Siempre quisimos que fuese desde un enfoque constructivo/positivo.

\-Es benéfico: las regalías son para la fundación Colores de Calcuta, dedicada a la educación y la salud de niños en Pilkhana, la ciudad de la Alegría.

**¿Cómo conseguir el libro?**

En la web de La Casa del libro, [Amazon](https://amzn.to/3YfwBTB) (enlace afiliado), El Corte Inglés y la web de la editorial Libros Indie.

* **¿Cuáles son vuestros proyectos individuales actuales?**

Nuestros relatos de Desamor todos tienen relación de alguna manera con nuestros proyectos individuales.

\-Noe: ensayo de superación personal que ya está enviando a las editoriales.

\-Bea: novela romántica erótica (chick lit), en la calle en noviembre.

\-Leti: cuento y novela infantil recien publicados

**¿Tenéis otras antologías entre manos para el futuro?**

\-Noe una antología sobre empoderamiento femenino

\-Bea y Leti una antología sobre Diversidad funcional (que será benéfica, beneficios para la Asociación Española del Síndrome Rubinstein-Taybi).

**¿Tenéis algún proyecto de futuro entre vosotras tres?**

**“Lollipop MOM / MOMis piruleta. Con amor y con humor”**: ensayo con toques de humor. Este es el punto que nos unió desde el principio, el HUMOR. Nuestro libro reflejará experiencias del día a día con humor. Nuestro símbolo es una piruleta: los problemas del día a día son un dulce comparados con los problemas de verdad (guerras, pandemias, pobreza extrema…). 

**¿Por qué Lollipop mom?** 

Aunque no lo parezca, MOM indica todo lo que nos une y también aquello que nos diferencia. Las tres somos madres y nos ha unido nuestro momento WOW**:** la publicación de un libro, nuestro sueño cumplido. 

Todo, a pesar de la distancia que nos separa. Porque Leti es de Madrid (M), Noe de Oviedo (O) y Bea de Mallorca (M). Y también de que una escribe cuentos, otra ensayo y otra novela.

**¿Qué es lo que más os cabrea?**

\-Noe: los estereotipos hombre-mujer y la injusticia

\-Bea: la injusticia (sobre todo cuándo afecta a terceros) y la falta de empatía, el mundo sería un lugar mucho mejor si, por un momento, todos nos pusiéramos en el lugar del otro.

\-Leti: la desigualdad en cuanto a oportunidades, sobre todo en función del lugar en el que nacemos.