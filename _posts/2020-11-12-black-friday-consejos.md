---
layout: post
slug: black-friday-consejos
title: Consejos para comprar de forma segura en el Black Friday
date: 2020-11-12T09:19:29+02:00
image: /images/posts/consejos-black-friday/p-bolsa.jpg
author: belem
tags:
  - destacado
  - trucos
---
Mi época favorita del año, y la de muchos, me aventuro a asegurar, ha llegado. Si bien, este año será (bastante) diferente, la emoción no se disipa. Creo que muchos hemos esperado con bastante anticipación la parte final de este año; la comida, las reuniones, la atmósfera de júbilo generalizada y, por qué no, los [regalos](https://madrescabreadas.com/2022/04/05/regalos-para-invitados-cumpleanos-ninos/).

Personalmente, debo admitir que eso de los regalos a veces me estresa más de lo que quisiera. Por eso, procuro ser lo más organizada posible y anticipar mis compras tanto como pueda, las compras de último momento no son lo mío. Debido a esto, suele ser por estas fechas que comienzo a pensar en el Black Friday, el Buen Fin (una especie de Black Friday mexicano), el Cyber Monday, las ofertas navideñas o de fin de año y otras.

Hoy vamos a platicar un poco acerca del Black Friday consejos para hacer buenas compras, consejos para hacer compras seguras y formas de asegurarnos de hacer compras consientes. 

## ¿Qué se puede comprar en el Black Friday?

Vaya, en Black Friday podemos comprar prácticamente de todo. Cada vez son más las cadenas, empresas y comercios independientes que se unen a esta iniciativa (originalmente estadounidense). Por supuesto, puede que lo primero en que pensemos sean las grandes cadenas como [Amazon](https://www.amazon.es?&_encoding=UTF8&tag=madrescabread-21&linkCode=ur2&linkId=a10f0fac21ac22ab425fa0c2a42be15c&camp=3638&creative=24630) (enlace afiliado), El Corte Inglés en España, Liverpool y Palacio de Hierro en México, Editor Market en Argentina o Ripley en Chile y Colombia. Sin embargo, como está iniciativa se ha expandido a pasos agigantados en muchos países hispanoparlantes, lo normal es que cada año sean más los establecimientos que se unen a ella. 

Regresando a la pregunta, entonces ¿qué se puede comprar en el Black Friday?, la respuesta es todo, desde bienes inmateriales como softwares; servicios de peluquería, servicios médicos (yo he visto dentistas y médicos, especialmente plásticos, que ofrecen promociones interesantes en Black Friday), servicios mecánicos y otros hasta, obviamente bienes materiales, como ropa, calzado, equipo tecnológico (que es de lo que más se compra), y artículos para el hogar, para bebés y niños, etc.

Esto significa que Black Friday es la oportunidad ideal para hacer las compras navideñas a buenos precios y con cierta anticipación, en caso de necesitar un cambio o una devolución.   

## Consejos para acertar en tus compras en Black Friday

Es importante, antes de lanzarnos al ruedo de las compras, conocer y establecer parámetros básicos para realizar buenas compras en Black Friday. Si bien, la situación actual de la pandemia viene acompañada de un halo de incertidumbre, y lo más probable es que el distanciamiento social nos impida hacer nuestras compras como solíamos hacerlo, esto también representa una oportunidad para hacer mejores compras a mi parecer.

## Compara precios

Esta es una muy obvia. Muchos vamos por la vida comparando precios, esto es especialmente cierto cuando somos madres o padres de familia. Sin embargo, en este caso lo ideal es llevar una lista física de los precios que hemos estado viendo. Puede ser como tal, en una hoja de papel en donde vayamos llevando registro de precios y lugares, una nota en el móvil o una hoja de cálculo en la computadora u ordenador. 

A mí personalmente me sirve más cuando lo hago en un pequeño cuaderno de notas que siempre llevo conmigo. Me ayuda a fijar los datos en mi memoria y a tener siempre presente la evidencia física de las comparaciones. 

El registro previo a las compras de Black Friday puede comenzar entre un mes y un par de semanas antes del evento, estamos hablando de principios a mediados de noviembre. Esto con el fin de evitar la desagradable, pero recurrente sorpresa de comprar una falsa oferta (cuando a un artículo le suben el precio unas semanas antes del Black Friday para ese viernes bajar el precio, al precio previo original, y hacerlo parecer una gran oferta).

![](/images/uploads/lekue-recipiente-para-cocinar-palomitas-rojo-20-cm.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/B00NF9CT0C/ref=as_sl_pc_tf_til?tag=madrescabread-21&linkCode=w00&linkId=cf20c26af56bf1ce3688dc1e77f82946&creativeASIN=B00NF9CT0C&th=1) (enlace afiliado)

## Anticipa o planea tus compras

Antes de comenzar a comparar, tenemos que conocer exactamente qué es lo que queremos comprar. Hacer wishlists o listas de deseos en aplicaciones suele ser la forma más fácil. Pero también puede ser tan simple como apartar un par de horas antes de que todos despierten en casa o cuando todos duermen para reflexionar en lo que queremos adquirir.

Pensemos en lo que necesitamos, en lo que queremos, en los regalos de los niños, pareja, padres, suegros, etc. Llevemos registro de nuestras ideas. De esta forma evitaremos las compras de pánico en las que terminamos comprando algo al último momento, a un precio elevado y que probablemente ni siquiera le guste a la persona. 

## Compra conscientemente

Para anticipar las compras, es importante conocer lo que necesitamos y lo que queremos comprar. En estos tiempos, lo mejor siempre va a ser avanzar con conciencia (ecológica, ética, económica). Entonces comencemos por separar las necesidades de los deseos. Lo primero serán, o tendrían que ser, las necesidades. 

Por ejemplo, los niños suelen crecer rápidamente y necesitar mucho más que un par de zapatos por año. Lo mismo con la ropa. Puede que nosotros necesitemos un abrigo nuevo para la temporada, pues el que teníamos se ha roto. La ropa interior debe ser renovada año con año idealmente.

La cocina podría necesitar una reparación importante, el motor de la licuadora puede estar dañado sin remedio, etc. A aquello que realmente sea necesario debemos darle prioridad. 

Después, vienen los deseos. Los regalos de navidad y de reyes que no hayan sido considerados una necesidad, unos audífonos nuevos que realmente queremos, un florero nuevo para la sala, etc.

![florero blanco con flores rosas](/images/posts/consejos-black-friday/flores.jpg)

Recordemos hacer compras consientes: evitemos en la medida de lo posible el fast fashion, démosle oportunidad a marcas o creadores independientes y compremos local. En estos tiempos difíciles, muchas veces echar la mano al prójimo no nos cuesta nada si era algo que de todos modos íbamos a hacer y para los comerciantes independientes, puede hacer mucha diferencia.

Asimismo, hay que evaluar lo que vamos a comprar. Si hay cosas que pueden ser reparadas y seguir siendo utilizadas, ¡adelante! Si de todos modos queremos cambiarlas, podemos reparar las antiguas y regalarlas o venderlas en lugar de desecharlas.  

## Haz un presupuesto

El presupuesto es importantísimo. Antes de establecerlo, descuenta del total tus pagos fijos: la tarjeta de crédito, luz, hipoteca o renta, etc. No utilices dinero que necesitas para otras cosas. No te endeudes innecesariamente, no suele valer la pena. 

En un momento que tengas a solas o con tu pareja, hagan cuentas y decide o decidan cuánto se van a permitir gastar en el Black Friday. Y, una vez establecido, ¡respétalo! Cuando no lo hacemos nos metemos en aprietos innecesarios, pellizcando dinero de otros lados y desajustándonos. Esto suele terminar en arrepentimiento y estrés de a gratis. De nuevo, no vale la pena.

## Considera comprar on line

Especialmente este año con el distanciamiento social que tenemos que respetar y con el cierre de algunos establecimientos en algunas ciudades que se ha impuesto para bajar el número de contagios, esta idea suena cada vez mejor. Desde y hacia la comodidad de tu hogar y ni siquiera necesitas zapatos para hacerlo. 

<a href="https://s.click.aliexpress.com/e/_AUu2hP?bz=725*90" target="_parent"><img width="725" height="90" src="//ae01.alicdn.com/kf/H4bd1f28f0a10436bb59460ec88777c7fX.png"/></a>

Solamente te recomendamos considerar las fechas de entrega, ya que las paqueterías y las entregas suelen saturarse y lo más probables que se les tome hasta una semana extra hacer la entrega. 

Asimismo, existen otras [pautas de seguridad que conviene tomar en cuenta antes de hacer compras en línea](https://madrescabreadas.com/2019/11/26/black-friday-precauciones/), las cuales revisaremos en el siguiente apartado. 

## ¿Cómo comprar por Internet de forma segura?

A estas alturas, lo más probable es que ya hayamos, por lo menos una vez, comprado en línea. Los sitios de comercios establecidos suelen ofrecer toda la seguridad para nuestros datos. Sin embargo, a veces podemos preguntarnos cómo asegurarnos de estar comprando de la manera más segura posible. A continuación, algunos consejos:

Compra desde el Wi-Fi de casa o los datos móviles, evita la conexión gratuita o pública que muchos establecimientos ofrecen.
![florero blanco con flores rosas](/images/posts/consejos-black-friday/pagando.jpg)

Mantén actualizado el antivirus y el firewall de tu ordenador y no abras ligas que te lleven a otra página a media compra. 

Procura hacer tus compras en las páginas de establecimientos conocidos. Muchos incluso cuentan con sus propias aplicaciones para hacerlo. 

Siempre que esté la opción es mejor comprar con PayPal, para evitar tener que ingresar los datos bancarios. Es una compra segura a la que podemos seguirle la pista. 

Enciende las alertas de tu cuenta bancaria para que te lleguen notificaciones cuando la cuenta registre un movimiento como un cargo o un depósito. 

Date un tiempo para leer y conocer bien las políticas de devolución.

Lee las reseñas de otras personas que hayan comprado el mismo artículo que deseas adquirir.

Averigua acerca del envío: costos, fechas, quién lo hará, etc. Asegúrate de ingresar de manera correcta tu dirección y datos de contacto. 

No te dejes avasallar por las grandes ofertas; si suena muy bueno para ser cierto, probablemente no lo es. Investiga todo lo que puedas, pregunta en grupos o en foros antes de hacer la compra. 

![florero blanco con flores rosas](/images/posts/consejos-black-friday/sale.jpg)

## Black Friday 2022, las mejores ofertas

Contra todo pronóstico sobrevivimos al temible año 2020, luego llegó el 2021 y ahora estamos en 2022; casi para culminarlo. 

Pues bien, este año el Black Friday es el viernes 25 de noviembre.

En la actualidad se respira un aire más relajado, en el que los regalos son una prioridad, pues después de mucho tiempo, se vivirá la navidad más cercana a la que se vivía antes de la pandemia.

Te presentamos las mejores ideas ofertas para este Black Friday 2022.

### Regalos para niños

Aprovecha las ofertas de este año para hacer varios regalos a tus niños. Hay muchas opciones.

Por ejemplo, puedes regalar un perrito de juguete interactivo. Es una buena opción si no hay espacio en casa para adoptar uno de verdad. 

También es una excelente idea para poder entrenar a tu pequeño en la tarea de cuidar a una mascota. Este tipo de juguetes contribuye a que el niño pueda cultivar la empatía, consideración y generosidad.

![](/images/uploads/little-live-pets-sleepy-puppy.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/little-live-pets-espa%C3%B1ola-700013210/dp/B01IIHK3GC/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=26928J62BIDWV&keywords=juguetes&psr=EY17&qid=1669040459&qu=eyJxc2MiOiIxMC43NyIsInFzYSI6IjEwLjUyIiwicXNwIjoiOS40MSJ9&s=black-friday&sprefix=juguete%2Cblack-friday%2C486&sr=1-6&th=1&madrescabread-21) (enlace afiliado)

Por otra parte, puedes regalar juguetes que ayuden a tus hijos a jugar a la casita, como una lavadora interactiva.

![](/images/uploads/little-tikes-first-washer-dryer-.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Little-Tikes-First-Washer-Dryer-Electrodom%C3%A9stico/dp/B0851KYQVX/ref=sr_1_19?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=26928J62BIDWV&keywords=juguetes&psr=EY17&qid=1669040459&qu=eyJxc2MiOiIxMC43NyIsInFzYSI6IjEwLjUyIiwicXNwIjoiOS40MSJ9&s=black-friday&sprefix=juguete%2Cblack-friday%2C486&sr=1-19&th=1&madrescabread-21) (enlace afiliado)

Otra opción que nunca falla son los carritos de control remoto, la verdad es que este tipo de obsequios son un acierto seguro ¿A qué niño no le gusta la velocidad?

![](/images/uploads/dickie-toys-cars-coche-rayo-mc-queen-rc-single-drive.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Disney-Cars-203081000S03-Distancia-dise%C3%B1o/dp/B06XR2L6YB/ref=sr_1_23?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=26928J62BIDWV&keywords=juguetes&psr=EY17&qid=1669041174&qu=eyJxc2MiOiIxMC43NyIsInFzYSI6IjEwLjUyIiwicXNwIjoiOS40MSJ9&s=black-friday&sprefix=juguete%2Cblack-friday%2C486&sr=1-23&madrescabread-21) (enlace afiliado)

### Electrodomésticos

Las tareas domésticas se realizan mejor cuando se tienen todas las herramientas adecuadas. Por eso, debes aprovechar las promociones de este día para poder actualizar tus ollas y sartenes.

![](/images/uploads/monix-m740040-copper.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Monix-sartenes-antiadherente-part%C3%ADculas-inducci%C3%B3n/dp/B07HBF29Y8?ref_=Oct_DLandingS_D_4033adbd_61&madrescabread-21) (enlace afiliado)

También puedes hacerte de tus electrodomésticos. Por ejemplo, adquirir una vaporera te ayuda a [cocinar recetas nutritivas a tus niños](https://madrescabreadas.com/2020/09/19/recetas-cocinar-sano/), recuerda que en la etapa de crecimiento es necesario adicionar vegetales al vapor.

![](/images/uploads/russell-hobbs-vaporera-cook-home.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Russell-Hobbs-19270-56-recipientes-temporizador/dp/B008Y6IN3S/ref=sr_1_13?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=M7IT3F3HQEJZ&keywords=electrodomesticos&qid=1669041387&qu=eyJxc2MiOiI4LjIxIiwicXNhIjoiNy4wNyIsInFzcCI6IjMuNzkifQ%3D%3D&sprefix=electrodomesticos%2Caps%2C317&sr=8-13&madrescabread-21) (enlace afiliado)

La verdad es que hay muchas opciones en electrodomésticos, los precios tienen rebajas increíbles. Puedes comprar [robots de cocina](https://www.amazon.es/dp/B06W5VH4V9/ref=sspa_dk_detail_2?psc=1&pd_rd_i=B06W5VH4V9&pd_rd_w=UOnvR&content-id=amzn1.sym.36674dec-8b0a-4047-ad8a-2904aa774a22&pf_rd_p=36674dec-8b0a-4047-ad8a-2904aa774a22&pf_rd_r=HJYH2699PGH5Y8KSHT2E&pd_rd_wg=NWS3E&pd_rd_r=3c1ba16b-773b-467b-a6ed-f63817592105&sp_csd=d2lkZ2V0TmFtZT1zcF9kZXRhaWxfdGhlbWF0aWM&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFNNFYwRDlGR1pEVEEmZW5jcnlwdGVkSWQ9QTAzOTQ4NjMyMFBYRTlTUVRFTUZNJmVuY3J5cHRlZEFkSWQ9QTAxNjUxODdCNzdENk45NUlLSjcmd2lkZ2V0TmFtZT1zcF9kZXRhaWxfdGhlbWF0aWMmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl), [freidora de aire](https://www.amazon.es/dp/B0B49S2D56/ref=sspa_dk_detail_0?psc=1&pd_rd_i=B0B49S2D56&pd_rd_w=AEv72&content-id=amzn1.sym.4bd66532-7b33-429c-b5f3-8f37d2eceaa2&pf_rd_p=4bd66532-7b33-429c-b5f3-8f37d2eceaa2&pf_rd_r=3KA6HMWJEBPVDEG35TJQ&pd_rd_wg=gxD1t&pd_rd_r=2b815686-d61a-40e9-aa95-348c7f55d362&s=kitchen&sp_csd=d2lkZ2V0TmFtZT1zcF9kZXRhaWw), [cafeteras](https://www.amazon.es/DeLonghi-Magnifica-ECAM-22-110-B-superautom%C3%A1tica/dp/B00400OMU0/ref=sr_1_9?crid=1V6JIWGHGAFUS&keywords=electrodomesticos+hogar&qid=1669041882&qu=eyJxc2MiOiI1Ljc1IiwicXNhIjoiNC43NSIsInFzcCI6IjAuMDAifQ%3D%3D&s=kitchen&sprefix=electrodomes%2Ckitchen%2C731&sr=1-9), [microondas](https://www.amazon.es/Taurus-Microondas-Fastwave-Grill-Revestimiento/dp/B084THZPKQ/ref=sr_1_16?crid=1V6JIWGHGAFUS&keywords=electrodomesticos+hogar&qid=1669041930&qu=eyJxc2MiOiI1Ljc1IiwicXNhIjoiNC43NSIsInFzcCI6IjAuMDAifQ%3D%3D&s=kitchen&sprefix=electrodomes%2Ckitchen%2C731&sr=1-16) (enlace afiliado), etc.

### Productos masculinos

Papá también merece recibir regalos, por lo que en este viernes negro hay muchas ofertas en productos pensados para el hombre, como [lentes de sol](https://www.amazon.es/Polaroid-Hombre-Gafas-Negro-Dkruth/dp/B00PA4EAZC/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3PYWY3GNZW0HI&keywords=lentes+de+sol+hombre&psr=EY17&qid=1669042648&qu=eyJxc2MiOiI1LjQzIiwicXNhIjoiNC4yOCIsInFzcCI6IjAuOTIifQ%3D%3D&s=black-friday&sprefix=lentes+de+sol+hombr%2Cblack-friday%2C261&sr=1-6), [reloj](https://www.amazon.es/anal%C3%B3gico-Tommy-Hilfiger-1791066-marr%C3%B3n/dp/B00MLYDJ10/ref=sr_1_6?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=34GFIY1F5NVNN&keywords=reloj+hombre&qid=1669042682&qu=eyJxc2MiOiI5LjcxIiwicXNhIjoiOS43NiIsInFzcCI6IjguNDgifQ%3D%3D&sprefix=reloj+hombre%2Caps%2C469&sr=8-6), [camisetas](https://www.amazon.es/Hackett-London-Selvedge-Camiseta-Blanco/dp/B09QL697XY/ref=sr_1_34_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1BTR2MLHSYA11&keywords=camiseta+hombre&qid=1669042782&qu=eyJxc2MiOiIxMC44MSIsInFzYSI6IjEwLjMzIiwicXNwIjoiOS4yNiJ9&sprefix=camisetahombre%2Caps%2C409&sr=8-34-spons&sp_csd=d2lkZ2V0TmFtZT1zcF9tdGY&psc=1) (enlace afiliado), etc.

Claro, las afeitadoras son una de las mejores ofertas de este año. Hay muchas marcas y diseños, por lo que es conveniente conocer los gustos de afeitado de los hombres de la casa, bien sea papá, abuelo o incluso nuestro adolescente.

![](/images/uploads/afeitadora-electrica-wet-and-dry-shaver-series-7000.jpg)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Afeitadora-el%C3%A9ctrica-Philips-S7783-63/dp/B09G3KYR5P?ref_=Oct_DLandingS_D_26498d08_70&th=1&madrescabread-21) (enlace afiliado)

**En conclusión, en este año ha mejorado el movimiento económico con relación a lo sucedido en el 2020. Claro, hay muchos retos que superar, pero definitivamente el 2022 ha sido el año en el que hemos recuperado gran parte de nuestra “normalidad”, así pues, aprovecha los descuentos del Black Friday en cualquier categoría, pues en Amazon puedes comprar cualquier producto en oferta durante esta semana.**

Listo, creo que después de esto ya estamos listos para nuestras compras de Black Friday consejos, sugerencias y tips: esperamos que todos ellos te ayuden a hacer las mejores compras y de la mejor forma en esta temporada.  

Si te ha gustado, comparte! Así nos ayudas a seguir escribiendo este blog. 

*Photo portada by Lucrezia Carnelos on Unsplash*

*Photo by Clay Banks on Unsplash pagando*

*Photo by Justin Lim on Unsplash sale*

*Photo by Alexandra Gorn on Unsplash flores*