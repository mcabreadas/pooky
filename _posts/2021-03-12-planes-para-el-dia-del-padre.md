---
author: ana
date: '2021-03-12T10:10:29+02:00'
image: /images/posts/ideas-para-dia-del-padre/p-dia-del-padre.png
layout: post
tags:
- trucos
title: Los mejores planes para celebrar el día del padre
---

Este 19 pinta bien para que esos sencillos deseos queden satisfechos y los gustos de papá formen parte de los planes para este Día del Padre.

## Cuándo se celebra el día del padre

La tradición católica celebra el 19 de marzo como el Día de San José. Por extensión, en España y otros países se celebra el Día del Padre. No ocurre así en otras regiones y son otros los motivos que llevan a dedicarle un día a los papás.

Por ejemplo, siguiendo la tradición norteamericana, se celebra el tercer domingo de junio. En Líbano, Egipto, Jordania, Palestina, Siria y Uganda es el 21 de junio, en Rusia el 23 de febrero, en Tailandia el 5 de diciembre. Indagando solo un poco vemos que las fechas varían con la geografía, mas seguro es uno solo el sentimiento. Acompáñanos a celebrarlo con cosas para hacer el día del padre especial, sea que esté con nosotros bajo el mismo techo o en otro lugar.

De todos modos, después de lo vivido en este año de pandemia y cuarentena, ¿queda algo verdaderamente lejos? Nada que no lo aproximen el amor y la nostalgia. Espantemos los nubarrones, y hagamos salir el sol en este día del padre.

![padre-es-proteccion](/images/posts/ideas-para-dia-del-padre/padre-es-proteccion.png)

## Prepara un plan perfecto para el día del padre

No dejemos que llegue la fecha y nos tome desprevenidos. Si se trata de [cocinar lo que más le apetece](https://madrescabreadas.com/tag/recetas/), debemos tener en casa con antelación lo necesario. Por otro lado, tal vez no sea de darle "me gusta" a publicaciones de ropa. Pero una camisa o pijama que sepas le haya interesado y llegue _delivery_ puede hacer la diferencia.

Desde ya, pregúntate. Qué más le atrae. Clasifica y ordena según las posibilidades que brindan las circunstancias. Si le apasiona el fútbol, qué tal un vídeo de goles, de jugadas, de momentos emocionantes de ligas o mundiales. En internet abundan estos materiales, la idea consistiría en prepararlo en un _pack_ según los colores y equipos que despiertan la pasión de tu padre.

Y si no es el fútbol, sino la música, el teatro o el cine, vale igual. En verdad, cualquier afición puede convertirse en un regalo si disponemos que se de, sin interrupciones ni cortes, un banquete.

## Cómo sorprender por el día del padre

![galeria](/images/posts/ideas-para-dia-del-padre/galeria.png)

Algo que no falla y va directo al corazón, son las imágenes. Vayamos a aquellas impresas (sí, antes se imprimían...) en algún álbum semiolvidado y rescatémoslas digitalmente. Prepara una película con estas fotos y de fondo una pieza instrumental de una canción de su preferencia. La alegría y la ternura se alimentan con recuerdos y una velada de anécdotas suscitadas y al vuelo, harán especial e inolvidable este Día del Padre.


*Photo de portada by Bermix Studio on Unsplash*

*Photo by Juliane Liebermann on Unsplash*

*Photo by Barna Kovács on Unsplash*