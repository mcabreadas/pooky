---
author: maria
date: '2020-10-20T11:19:29+02:00'
image: /images/posts/cancer-medula/p-hermanos.jpg
layout: post
tags:
- invitado
- mujer
title: Donar medula salva vidas
---

> Hoy en la sección "Protagonistas" de este blog, que da voz a las madres, y en la que te animo a [participar aquí](https://madrescabreadas.com/2020/10/16/comunidad-madres/), tenemos como invitada a [Fina](https://www.instagram.com/fina_y_su_tribu/), una mujer luchadora de 45 años, nacida en Francia pero criada en España desde los 5 años. Es mamá divorciada con 3 niños que son el motor de su vida. Trabaja en una residencia de ancianos como auxiliar, y le encanta y apasiona su trabajo, sobre todo "sus mayores", como ella los llama, a los que intenta hacer la vida más bonita, divertida y alocada. Es amantes de los animales, sobre todo los gatos. Se considera una mujer recientemente empoderada, resiliente, sabe escuchar, y de fácil trato, aunque reconoce que algo cabezota, y le encanta ayudar a quien haga falta.
> 
> Bienvenida y gracias por compartir con nosotras tu duro testimonio de lucha por tu hijo. 
> Y vosotras, poneos una buena taza de té, chocolate o café, y vamos a leer a Fina:

![Fina cancer leucemia](/images/posts/cancer-medula/fina.jpg)

Hola me llamo Fina y soy mamá  de 3 niños maravillosos, el mayor de 18 el mediano de 15 y el peque de 7. Nuestra vida en estos años no ha sido fácil, pero la hemos sobrellevado; una vida estresada sin parar, pero como casi toda madre trabajadora. 

## Cuando la vida te para en seco

A veces no te das cuenta del estrés que llevas hasta que la vida te hace parar de golpe. Sí, de golpe se nos paró la nuestra. Mi hijo mediano a finales de febrero se encontraba cansado y con dolores de cabeza. Al principio pensé que era por el crecimiento porque entrenaba todos los días al fútbol, pero decidí llevarlo a que le hiciesen una analítica por ver sus niveles de hierro, pero salió muy alterada, así que decidieron repetírsela y volvió a salir alterada, pero me dijeron que estuviese tranquila, que parecía una talasemia, que hiciera vida normal. Entonces llegó el confinamiento y ahí quedó la cosa.

En mayo ya se podía volver a llamar al Centro de salud, y comenté de nuevo que seguía con dolores de cabeza e insistí en otra analítica. Se la hicieron y a las 3h me dijeron que mi hijo tenia que ingresar para transfundirle sangre porque tenía mucha anemia. Ingresamos, transfundieron sangre y estuvieron unos días haciéndole pruebas hasta que el día 16 de junio, día de mi cumpleaños, un día donde era todo bonito con muchas felicitaciones, recibimos noticias del médico donde nos mandaban a ingresar... Ese día nos comunicaron que mi hijo mediano tenia leucemia.

![medico y paciente cogiéndose de las manos](/images/posts/cancer-medula/manos.jpg)

Se nos cayó el mundo en pedazos, mi vida se paralizó de momento y entramos en un túnel oscuro donde la palabra cáncer estaba escrita por todos lados. Lo primero que se te viene a la mente cuando escuchas esa palabra es miedo, sufrimiento, desesperación y muerte. 

## No entendía por qué a nosotros

Lloramos mucho, me sentía enfadada con la vida, no entendía por qué a nosotros, pero lo que menos entendía era por qué a mi hijo... Pero, y por qué no? Nadie estamos libres de sufrir cualquier percance...Nadie... Nos a tocado...

Ese día se paralizó mi vida para empezar otra paralela donde entran palabras nuevas que algunas de ellas no conocíamos: "blastos", "fibrinogeno", "plaquetas", "hemoglobina", "aspirados de medula", "punciones", y la tan temida quimioterapia. Otra vida donde todo es nuevo y tienes que aprender a convivir  de nuevo con hospitales, médicos, enfermeros, auxiliares. Y doy gracias a esos médicos que desde primer momento nos han tranquilizado y nos han hecho entender que esto es grave, pero se cura.

## Hemos aprendido a sacar cosas buenas del cáncer

Así que después de asimilar, interiorizar y sacar el enfado con la vida hemos dado paso a aceptar y sacar lo mejor de esta enfermedad. Lo mejor es que tenemos un montón de amigos  que nos apoyan, una familia increíble, unos médicos y la [Asociacion ASPANION](https://aspanion.es) formidables, y gente que estamos conociendo que nos dan muchas esperanzas.

Estamos aprendiendo a vivir con la enfermedad sin dejar que la enfermedad se apodere de nosotros. La vida sigue, y cada día está lleno de esperanza.

Lo que más me sorprende es que Bryan es un súper guerrero, es muy valiente y tiene muchas ganas de vencer esta batalla de la que estoy seguro que saldar victorioso porque  tiene a su alrededor un ejército que le va a respaldar siempre.

![niño con cáncer en hospital](/images/posts/cancer-medula/nino-cancer.jpg)

La vida te da palos, pero nos hacen más fuertes. Después del primer tratamiento de quimioterapia le hicieron un aspirado de médula que pensábamos que había ido bien, pero su médula era resistente a la enfermedad y aumentó. Entonces decidieron ponerle otra tanda de quimio más agresiva y cuando terminó ese ciclo fuimos a revisión al hospital de día en La Fe porque tenía analíticas y aspirado de médula, y salimos super contentos porque su médula había entrado en remisión con la quimio última; estaba limpia de células malas! Qué sorpresa más grande nos llevamos, ya que íbamos con recelo, pues el primer ciclo de quimio no le fué bien.

## Una médula compatible le salvaría la vida 

Ahora el próximo paso sería un  trasplante de médula para evitar una recaída, ya que su médula era resistente. Entonces fuimos a la unidad de trasplantes y nos dijeron que seguramente le pondrían la médula de su hermano, que es 50 compatible (encontrar una médula 100 compatible es muy difícil; llevaba en el registro de donantes desde el inicio de la enfermedad y nadie salía compatible aunque fuese un poco, por eso la importancia de donar).

Bryan tiene la suerte que al menos su hermano y yo somos el 50 compatibles y pueden aprovechar de las nuestras para el trasplante, pero muchos no tienen nadie en la familia que lo sea. En esos casos estudian si las células del cordón umbilical son buenas para él también. De ahí que donar cordón umbilical también es otra opción.

## Su hermano ha sido su salvador

![hermanos](/images/posts/cancer-medula/hermanos.jpg)
 
Podrían haber esperado a que saliese una médula, pero nos dijeron que podría tardar meses, y tendrían que volver a ponerle más quimio para mantener su médula limpia y que no sufriese recaída, por que que decidimos hacerle el transplante en 2 o 3 semanas, y nos pusimos hacer  campaña de donación de médula para concienciar a la gente de la importancia de ello, ya que con una médula una vida puede ser salvada: #DONAMEDULAREGALAVIDA.

![médicos en quirófano](/images/posts/cancer-medula/quirofano.jpg)

Bryan recibió la médula de su hermano y ahora mismo estamos ingresados viendo si empieza a fabricar defensas y empieza a funcionar bien. Esta semana debería empezar  a mejorar. Tenemos fe y confianza en que así será.

Ahora nos espera un año duro, pero al final todo quedará en un mal sueño...

Sigue así de fuerte mi SUPER GUERRERO!

Gracias por leerme

*Fina García Puche*

Déjame tu comentario y comparte para concienciar de la importancia de la donación de médula.

- *Photo by National Cancer Institute niño*
- *Photo by National Cancer Institute quirófano*
- *Photo by Matheus Ferrero on unsplash mano*
- *Photo by Jude Beck on unsplash hermanos*