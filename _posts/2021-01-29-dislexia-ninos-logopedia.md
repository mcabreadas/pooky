---
author: maria
date: '2021-01-29T11:10:29+02:00'
image: /images/posts/logocreas/p-labios.jpg
layout: post
tags:
- educacion
- invitado
- crianza
title: Logopedia creativa para niños con dislexia
---

> Puedes participar en la sección del blog "Merendando con..." si piensas que tu historia o testimonio puede inspirar o ayudar a otras madres, o compartiendo tu proyecto si piensas que puede aportarles.
> 
> [Participa en Merendando con... aquí](https://madrescabreadas.com/merendando-con)
> 
> Hoy merendamos con Emilia Morell Rigo, maestra de educación especial y diplomada en logopedia. Hace más de diez años acompaña a familias, niños y adultos para mejorar su comunicación y su lenguaje.
> Siempre le gustaron los niños y tenía claro que quería ser maestra, por eso escogió la especialidad de magisterio de educación especial, pero al acabar decidió seguir estudiando y eligió la diplomatura de logopedia, y ahí es cuando se dio cuenta de que era su pasión.
> 
> Desde el día que acabó la carrera no ha dejado de formarme y le encanta seguir aprendiendo. Esa es una de las razones por las que he creado [Logocreas](https://logocreas.com).

## Qué es la logopedia

"La logopedia es una disciplina dinámica y en continuo desarrollo, que se ocupa a grandes rasgos de: la investigación, diagnóstico, tratamiento, y prevención de las patologías y trastornos de la comunicación, lenguaje, habla, voz, audición y otras funciones orales, como la deglución” 

Es curioso lo poco que conocemos de esta ciencia, porque yo descubrí recientemente que la dislexia se trata con logopedia, y que realmente hay muchos casos de dislexia en la población, pero l mayoría son tan leves que no tiene prácticamente consecuencias (yo sospecho que soy uno de ellos).

## Qué es la dislexia

La dislexia consiste en se hace una lectura visual y se deduce en vez de leer. Por ejemplo, se puede leer “casa” en vez de “cosa”. Las personas con este tipo de dislexia pueden leer las palabras familiares, pero les resulta difícil leer palabras desconocidas, palabras largas o pseudo palabras.

> Es hora de que os sirváis un té, un chocolate o un café, y os sentéis tranquilamente con nosotras a charlar. 
> Os presento a Emilia, fundadora de Logocreas:

![Emilia Logocreas logopeda](/images/posts/logocreas/emilia.jpg)


En primer lugar quiero agradecer a Maria su invitación a este
ratito de merienda. 

## Qué es Logocreas

Logocreas es la web de la logopedia creativa y una web de referencia en la logopedia española. Difundimos nuestra profesión, formando e informando a profesionales y estudiantes. Y acompañamos a familias y usuarios en su proceso logopédico, siempre desde la formación y la creatividad.

En Logocreas las familias van a encontrar recursos fáciles para ayudar a sus hijos y un acompañamiento emocional para recorrer su camino. 

![Emilia Logocreas logopeda](/images/posts/logocreas/microfono.jpg)

También hay un espacio para profesionales, donde pueden encontrar recursos, novedades, formas de aprovechar sus materiales y una agenda con las formaciones que van a realizarse en nuestro país para seguir formándose. 

Y sobretodo compartir lo maravilloso de nuestra profesión y como ayudamos a la población a ganar calidad de vida

## Cómo puede ayudarte Logocreas

### Si eres un profesional

Puede ayudarte si eres un profesional de la logopedia y/o del mundo de la educación y la psicología a estar al día de las novedades nacionales, a trabajar de forma más creativa y por tanto más funcional para todos tus usuarios. 

![Emilia Logocreas logopeda](/images/posts/logocreas/postit.jpg)


### Si eres una familia

Y puede ayudarte si eres una familia, ya que los niños y adolescentes que acuden al logopeda es porque les ha tocado adquirir por su cuenta unas habilidades comunicativas que el resto de población ha adquirido de forma automática; es decir tienen que recorrer un camino más duro. 

No es fácil como padres acompañar y ver el duro camino que les ha tocado a sus hijos. Y en logocreas proporcionamos ideas, recursos y acompañamiento emocional de una forma fácil. Ya que soy muy defensora de trabajar jugando y convertir las rutinas del día a día de una familia en rutinas de trabajo divertidas. 

Podéis encontrarme en [www.logocreas.com](https://logocreas.com) y en [Instagram](https://www.instagram.com/logocreas/), [Facebook](https://twitter.com/logocreas1) y [Twiter](https://twitter.com/logocreas1) como @logocreas. 

Muchas gracias a todos y a María por dejarme un espacio en su maravilloso blog. 

Emilia Morell.

Si te ha gustado y piensas que puede ayudar a otras familias, comparte y suscríbete al blog para no perderte nada (no mandamos spam).