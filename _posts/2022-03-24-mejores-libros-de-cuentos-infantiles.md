---
layout: post
slug: mejores-libros-de-cuentos-infantiles
title: 7 cuentos infantiles para ayudarles a crear buenos hábitos desde el principio
date: 2022-05-03T16:53:35.390Z
image: /images/uploads/cuentos-infantiles.jpg
author: faby
tags:
  - libros
---
**Los cuentos infantiles potencian la imaginación de los niños**. A su vez, permite que puedas forjar una relación especial con tus peques. Si te preguntas qué puedes leer a tus hijos, has llegado al lugar indicado.

En esta entrada, te presentaré **cuáles son los mejores libros de cuentos para niños**. Por cierto, hay que destacar que hay libros para todas las etapas, es decir desde que son bebés hasta que tienen 11 años. **Nos centraremos en los cuentos para niños pequeños.**

## Buenas Noches, Todos a Dormir

Empezamos esta lista de libros con una obra literaria especialmente **diseñada para dormir**. Se trata de un cuento infantil con hojas de cartón gruesas y preciosos dibujos de animales que tu hijo de apenas **dos años** podrá reconocer.

En la portada encontramos un bello **dibujo de la luna con luz**. Si tu crío la toca durante unos segundos se ilumina.

[![](/images/uploads/buenas-noches-todos-a-dormir.jpg)](https://www.amazon.es/Buenas-noches-todos-dormir-Cuentos/dp/8417210679/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=cuentos+el+pirata+dormir&qid=1604998522&sr=8-7&linkCode=sl1&tag=bebes0b-21&linkId=4ce096cad5508aebbc0c1b8462872f2d&language=es_ES&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Buenas-noches-todos-dormir-Cuentos/dp/8417210679/ref=as_li_ss_tl?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=cuentos+el+pirata+dormir&qid=1604998522&sr=8-7&linkCode=sl1&tag=bebes0b-21&linkId=4ce096cad5508aebbc0c1b8462872f2d&language=es_ES&madrescabread-21) (enlace afiliado)

## El pollo Pepe (El pollo Pepe y sus amigos)

Es un cuento infantil para bebés. Narra de forma divertida lo glotón que es el pollo pepe. Tiene preciosas **ilustraciones y pop-ups que activan los sentidos del crío**. Puedes contar muchas historias mientras te apoyas en sus páginas. 

Al mismo tiempo, es un libro que logra captar la atención de los bebés, quienes no paran de reírse y desean abrirlo constantemente.

[![](/images/uploads/el-pollo-pepe.jpg)](https://www.amazon.es/gp/product/8434856816/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8434856816&linkCode=as2&tag=bebes0b-21&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/gp/product/8434856816/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=3626&creative=24790&creativeASIN=8434856816&linkCode=as2&tag=bebes0b-21&madrescabread-21) (enlace afiliado)

## ¿Puedo mirar tu pañal?

Este cuento interactivo dispone de **páginas con grandes desplegables** en su interior, colores llamativos y un divertido peluche de ratón.

Es ideal para niños que están dando sus **primeros pasos con el orinal.** Este ratoncito presume de sus logros de ir al baño. Es educativo y muy divertido.

[![](/images/uploads/pack-puedo-mirar-tu-panal-.jpg)](https://www.amazon.es/Pack-%C2%BFPuedo-mirar-pa%C3%B1al-mu%C3%B1eco/dp/841318875X/ref=sr_1_1?crid=3G8EMAWVYZQSL&keywords=puedo+mirar+tu+pa%C3%B1al&qid=1648065372&s=books&sprefix=puedo+mirar+%2Cstripbooks%2C379&sr=1-1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Pack-%C2%BFPuedo-mirar-pa%C3%B1al-mu%C3%B1eco/dp/841318875X/ref=sr_1_1?crid=3G8EMAWVYZQSL&keywords=puedo+mirar+tu+pa%C3%B1al&qid=1648065372&s=books&sprefix=puedo+mirar+%2Cstripbooks%2C379&sr=1-1&madrescabread-21) (enlace afiliado)

## La pequeña Marina dice ¡No!

¿Tu hija no quiere bañarse o vestirse? No te preocupes, eso pasa muy a menudo. Pero este cuento infantil le ayudará a tu hija a decir “si”. 

Narra la historia de Marina quien después de decir “no”, se da cuenta de que decir sí no es difícil, y es muy divertido. **Ideal para niños de 0 a 3 años que están en plena época de rabietas.**

[![](/images/uploads/la-pequena-marina-dice-no.jpg)](https://www.amazon.es/dp/842614067X?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/842614067X?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

## Marina ya no quiere llevar pañales

Una verdadera obra literaria infantil que te ayudará a conducir a tu peque al nuevo mundo de “eres grande, no uses pañal”.

El cuento habla de Marina una niña que ya no quiere usar pañales, pero anda distraída y a veces se moja de pipí. **Tu hijo ya no querrá usar pañal.**

Te dejo unos [consejos para limpiar el pipi del colchon](https://madrescabreadas.com/2015/10/14/limpiarpipicolchon/), por si hubiera alguna fuga.

[![](/images/uploads/marina-ya-no-quiere-llevar-panales.jpg)](https://www.amazon.es/Marina-quiere-llevar-pa%C3%B1ales-PEQUE%C3%91O/dp/8426138012/ref=pd_sbs_4/262-0743235-1049906?pd_rd_w=2jxRs&pf_rd_p=f9559607-0e83-4976-8590-72740e10e24e&pf_rd_r=V3M3GJPVWV8ZYFBH52ER&pd_rd_r=a9dccf63-a2ac-42cc-a356-4c746ee950f2&pd_rd_wg=spYlM&pd_rd_i=8426138012&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Marina-quiere-llevar-pa%C3%B1ales-PEQUE%C3%91O/dp/8426138012/ref=pd_sbs_4/262-0743235-1049906?pd_rd_w=2jxRs&pf_rd_p=f9559607-0e83-4976-8590-72740e10e24e&pf_rd_r=V3M3GJPVWV8ZYFBH52ER&pd_rd_r=a9dccf63-a2ac-42cc-a356-4c746ee950f2&pd_rd_wg=spYlM&pd_rd_i=8426138012&psc=1&madrescabread-21) (enlace afiliado)

## Todos bostezan

Este cuento está pensado para **niños más grandes, de unos 3 a 6 años**. Tiene un mecanismo dinámico en el que cada animalito bosteza de sueño, y al final todos duermen.

Puedes contar una historia de tu inventiva cada noche que tu hijo se resista a ir a la cama.

[![](/images/uploads/todos-bostezan.jpg)](https://www.amazon.es/dp/8491010211?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8491010211?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

## Los cinco sentidos de Nacho

Nacho es un niño curioso que empieza a conocer sus sentidos. Cuenta con textos breves que permite enseñar a tus pequeños la **importancia de cada sentido para explorar el mundo.** Este libro forma parte de una colección de 3 ediciones.

[![](/images/uploads/los-cinco-sentidos-de-nacho.jpg)](https://www.amazon.es/dp/8426386830?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/dp/8426386830?tag=bebesymaspivot-21&madrescabread-21) (enlace afiliado)

Otro de los títulos es **Emociones de Nacho**, en el que se habla de la tristeza, el enfado, temor y la alegría, un tema importante para ayudar a tu hijo a controlar sus emociones.

[![](/images/uploads/las-emociones-de-nacho.jpg)](https://www.amazon.es/Emociones-Colecci%C3%B3n-Libros-Moviles-Edelvives/dp/8426382657/ref=pd_bxgy_img_1/262-0743235-1049906?pd_rd_w=GH4ew&pf_rd_p=6003b884-667d-4d91-a6f1-ce2e55c4ddc2&pf_rd_r=WY0023RC51HF1XHX30CT&pd_rd_r=119db005-7bde-4c11-a940-b0b913579a0a&pd_rd_wg=j3JBM&pd_rd_i=8426382657&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Emociones-Colecci%C3%B3n-Libros-Moviles-Edelvives/dp/8426382657/ref=pd_bxgy_img_1/262-0743235-1049906?pd_rd_w=GH4ew&pf_rd_p=6003b884-667d-4d91-a6f1-ce2e55c4ddc2&pf_rd_r=WY0023RC51HF1XHX30CT&pd_rd_r=119db005-7bde-4c11-a940-b0b913579a0a&pd_rd_wg=j3JBM&pd_rd_i=8426382657&psc=1&madrescabread-21) (enlace afiliado)

Por otra parte, también cuentas con **Nacho y el cuerpo humano**, en el que tu hijo aprenderá a conocer las partes del cuerpo de forma divertida.

[![](/images/uploads/nacho-y-el-cuerpo-humano.jpg)](https://www.amazon.es/Nacho-cuerpo-humano-%C3%81lbumes-ilustrados/dp/8414016707/ref=pd_bxgy_img_2/262-0743235-1049906?pd_rd_w=GH4ew&pf_rd_p=6003b884-667d-4d91-a6f1-ce2e55c4ddc2&pf_rd_r=WY0023RC51HF1XHX30CT&pd_rd_r=119db005-7bde-4c11-a940-b0b913579a0a&pd_rd_wg=j3JBM&pd_rd_i=8414016707&psc=1&madrescabread-21) (enlace afiliado)

[![](/images/uploads/boton-comprar-amazon-centrado-400x48.png)](https://www.amazon.es/Nacho-cuerpo-humano-%C3%81lbumes-ilustrados/dp/8414016707/ref=pd_bxgy_img_2/262-0743235-1049906?pd_rd_w=GH4ew&pf_rd_p=6003b884-667d-4d91-a6f1-ce2e55c4ddc2&pf_rd_r=WY0023RC51HF1XHX30CT&pd_rd_r=119db005-7bde-4c11-a940-b0b913579a0a&pd_rd_wg=j3JBM&pd_rd_i=8414016707&psc=1&madrescabread-21) (enlace afiliado)

En resumidas cuentas, dispones de muchos cuentos infantiles para ayudar a tus hijos a dormir, usar el orinal, [dejar el pañal](https://www.luciamipediatra.com/adios-panales/), conocerse a sí mismo, entre muchas cosas.

*Photo Portada by evgenyatamanenko on Istockphoto*