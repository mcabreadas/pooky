---
layout: page
slug: about
title: Sobre mí
image: /images/pages/maria-altavoz.jpeg
navigation: true
---
Soy María, una madre superviviente de familia numerosa con hijos ya adolescentes. Comencé este portal en 2010, cuando mis dos primeros hijos eran bebés y atravesaba los momentos más duros de la crianza.

El blog fue mi espacio personal de supervivencia y gracias a él y a la comunidad que se formó a su alrededor pude seguir adelante con fuerza y energía.

El nombre "Madres Cabreadas" surgió como protesta por las dificultades con las que me encontré al intentar conciliar mi vida profesional como abogada con la familiar, y fue tan grande la respuesta que obtuve en redes sociales, que poco a poco se fue generando una comunidad de madres muy potente y diferentes entre sí.

Más de 600 post de pura vivencia honesta y sin adornos, con sus luces y sus sombras, con sus cabreos y des cabreos, pero siempre con honestidad, humor y un punto de ironía.



### Mi propio estilo de crianza (que no tiene por qué ser el tuyo)

Empecé a criar cuando estaba de moda la llamada crianza natural, y me cambió la forma de entender la maternidad (menos mal porque a punto estuve de caer en la trampa de Estivill).

Nunca agradeceré lo suficiente a quien puso en mis manos el libro de Carlos González "Bésame mucho", el primero de muchos otros que leí para intentar aprender sobre un mundo totalmente nuevo para mí.

Nunca fui radical ni purista de ningún sistema o método, pero éste me cambió la mirada y me enseñó conceptos como crianza respetuosa y apego seguro, los que aún hoy intento practicar con mis hijos y compartir con las familias que me acompañan en este portal, por mis redes sociales o en mi podcast, pero sin seguir estrictamente ningún sistema, y adaptándolo a mi familia y mis circunstancias.

Recientemente he descubierto la disciplina positiva para mis adolescentes, y me encuentro en continua formación para intentar hacerlo lo mejor posible con ellos a la vez que me preparo para afrontar con ellos la etapa universitaria.

 ![grupo](/images/pages/maria-hijos.jpeg)

### Los libros que me han marcado y que puedo recomendarte:

* Qué esperar mientras se está esperando
* Una maternidad diferente
* Bésame Mucho
* Un regalo para toda la vida
* Diario de una mamá pediatra
* Cómo hablar para que los niños escuchen...
* El cerebro del niño
* El cerebro del adolescente

Y muchos otros!
Si te interesa alguno puedes conseguirlo [aquí](https://amzn.to/3Kee1GG)

<a href="https://amzn.to/3Kee1GG" class='c-btn c-btn--active c-btn--small'>Quiero ver todos los libros</a>

![camiseta levantate y brilla madres cabreadas](/images/uploads/camiseta-floracion-blog.jpeg)

Por cierto esta foto me la hizo mi amiga, la fotgrafa de familia Marta Ahijado, y la camiseta es un diseño especial y exclusivo para mis madres cabreadas porque todas sois mujeres brillantes, cada una con sus circunstancias; sólo tienes que levantarte y dejarte ver... brillar.
Si te gusta puedes [conseguirla aquí](https://www.latostadora.com/shop/madrescabreadas/) para regalársela a esa mujer que brilla a tu lado acompañándote cada día, o para lucirla tú.

<a href="https://www.latostadora.com/shop/madrescabreadas/" class='c-btn c-btn--active c-btn--small'>Quiero la camiseta</a>

### Fui una madre guerrera y activista

[Conciliación Real Ya](https://madrescabreadas.com/tag/conciliacionrealya/), fue un movimiento social espontaneo que nacio en 2011 en las redes sociales de las inquietudes de un grupo de mamás y papás blogueros, que no se conformaban con las medidas de conciliación de la vida laboral y familiar y el modo de aplicarlas por algunas empresas.

Llegamos a presentar nuestras propuestas de cambio legislativo en el Congreso de los Diputados.

![mamas con bebe en la puerta del congreso de los diputados](/images/uploads/tumblr_inline_pgauznvvex1qfhdaz_540.jpg)

[Lactancia en Libertad](https://madrescabreadas.com/2014/01/16/los-logros-de-lactancia-en-libertad/) fue una asociación que intento promover una ley que protegiera a las madres para que tuvieran libertad para amantar a sus criaturas donde y cuando éstas lo necesitaran, sin que por ello sean incomodadas, discriminadas o expulsadas.

Surgio en 2013 a raiz de las [movilizaciones por la expulsion de una madre lactante de Primark](https://madrescabreadas.com/2013/08/24/crónica-de-una-tetada-anunciada-18-primark/).

![manifestacion lactancia materna primark](/images/uploads/tumblr_inline_pe6xvicwqi1qfhdaz_540.jpg)

## Colaboraciones y patrocinios

A veces recomiendo productos o servicios que me gustan. Si eres una marca y estás interesada en que conozca tu producto y, si me gusta, lo recomiende honestamente, o estás interesada en un patrocinio escríbeme a
<a href="mailto:info@madrescabreadas.com"> **info@madrescabreadas.com**</a>

![maquillaje](/images/pages/maquillaje.jpg)

<a href="https://www.amazon.es/shop/madrescabreadas" class='c-btn c-btn--active c-btn--small'>Aquí mi selección de productos descabreantes</a>

## Descabreate cada domingo con nosotras. ¡Suscríbete!

Valoro mucho los autocuidados, el derecho al cabreo y al descabreo son claves para que las madres podamos preservar la salud física y mental en la crianza de forma sostenible en el tiempo. Y para ello esta comunidad es ideal.

En este portal encontrarás información sobre la etapa en la que e encuentras, empatía, humor y frescura. Más de 25.000 madres ya formamos parte de la comunidad de Madres Cabreadas, Nos acompañas? Nos dejas que te acompañemos?

¡Únete y recibe las últimas novedades en tu email, además del regalo del e-book Grandes Poesías de Pequeños Poetas!

![portada libro poesía infantil](/images/libro-grandes-poesias.jpg)

<a href="https://gmail.us20.list-manage.com/subscribe/post?u=10d9dc33eda90af955f11574d&id=38e71df09c" class='c-btn c-btn--active c-btn--small'>Suscríbete: e-book de regalo</a>

## Enganchate al podcast de las Madres cabreadas

Deja que te acompañe en tus viajes en coche, tus caminatas o mientras cocinas, sales a correr o haces la colada!

En cada episodio te presento a una mujer increible, con un proyecto que te puede aportar o con un testimonio que merece la pena escuchar.
Dale a seguir!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/7ycAAJFVLQuVGWTBAoKfRh?utm_source=generator&theme=0" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

![circulo mujeres yoga](/images/pages/yoga.jpeg)
