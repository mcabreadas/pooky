---
layout: page
slug: legalidad
title: Aviso legal, política de cookies y política de privacidad
image: /images/uploads/depositphotos_306050500_xl.jpg
---
### Aviso legal

#### Contenido del blog

Madres Cabreadas se cataloga como un sitio web personal.

### Qué datos personales recogemos y por qué los recogemos

**Comentarios**

Los comentarios de esta web son gestionados por Disqus, sita en San Francisco: [ver política de privacidad](https://help.disqus.com/en/articles/1717103-disqus-privacy-policy)

**Formulario de suscripción**

El formulario de suscripción es gestionado a través de Mailchimp, sito en Atlanta ([Términos](https://mailchimp.com/legal/privacy/#3._Privacy_for_Contacts))

**Cuánto tiempo conservamos tus datos**

De los usuarios que se suscriben al blog, también almacenamos la información personal que proporcionan en los formularios. Todos los usuarios pueden a través de Mailchimp ver, editar o eliminar su información personal en cualquier momento (excepto que no pueden cambiar su nombre de usuario). Los administradores de la web también pueden ver y editar esa información.

**Finalidad de los datos**

Tal y como se recoge en la normativa, se informa al USUARIO que, a través de los formularios de contacto, o suscripciones se recaban datos, los cuales se almacenan en un fichero, con la exclusiva finalidad de envío de comunicaciones electrónicas, tales como: boletines (newsletters), nuevas entradas (posts), ofertas comerciales, webinars gratuitos, así como otras comunicaciones que Madres Cabreadas entiende interesantes. Los campos marcados como de cumplimentación obligatoria, son imprescindibles para realizar la finalidad expresada.

Únicamente el titular tendrá acceso a sus datos a través de Mailchimp, y bajo ningún concepto, estos datos serán cedidos, compartidos, transferidos, ni vendidos a ningún tercero.La aceptación de la política de privacidad, mediante el procedimiento establecido de doble opt-in, se entenderá a todos los efectos como la prestación de CONSENTIMIENTO EXPRESO E INEQUÍVOCO del USUARIO al tratamiento de los datos de carácter personal en los términos que se exponen en el presente documento, así como a la transferencia internacional de datos que se produce, exclusivamente debido a la ubicación física de las instalaciones de los proveedores de servicios y encargados del tratamiento de datos.
En ningún caso se realizará un uso diferente que la finalidad para los que han sido recabados los datos ni muchos menos cederé a un tercero estos datos.

**Legitimación**

Gracias al consentimiento, podemos tratar tus datos siendo requisito obligatorio para poder suscribirte a la página web.

Como bien sabes, puedes retirar tu consentimiento en el momento que lo desees.

**Categoría de los datos**

Los datos recabados en ningún momento son especialmente protegidos, sino que se encuentran categorizados como datos identificativos.

**Tiempo de conservación de los datos**

Conservaremos los datos durante el tiempo legalmente establecido o hasta que solicites eliminarlos.

**Responsable del fichero y encargado del tratamiento**

El responsable del fichero de datos es María Sánchez Martínez, DNI: 34834810E, con domicilio en Murcia

Los servicios de Hosting y servidor son propiedad de Netlify, sito en California ([política de privacidad](https://www.netlify.com/privacy)).

**Google Analytics**

Google Analytics es un servicio análisis de datos estadísticas prestado por la empresa Google ([política de privacidad](https://policies.google.com/privacy?hl=es)).Madres Cabreadas utiliza este servicio para realizar un seguimiento de las estadísticas de uso del mismo.

Google Analytics utiliza cookies para ayudar al sitio web a analizar datos estadísticos sobre el uso del mismo (número de visitas totales, páginas más vistas, etc.). La información que genera la cookie (incluyendo su dirección IP) será directamente transmitida y archivada por Google en los servidores de Estados Unidos.

**Amazon**

En calidad de Afiliado de Amazon, obtengo ingresos por las compras adscritas que cumplen los requisitos aplicables.

Puedes consultar la [politica de privacidad de Amazon aqui](https://www.amazon.es/gp/help/customer/display.html?nodeId=201909010&ref_=footer_privacy).

**Propelbon**

En calidad de Afiliado de Propelbon, obtengo ingresos por las compras adscritas que cumplen los requisitos aplicables.

Puedes consultar la [politica de privacidad de Propelbon aqui](https://propelbon.com/politica-de-privacidad/).

**Tradedoubler**

En calidad de Afiliado de Tradedoubler, obtengo ingresos por las compras adscritas que cumplen los requisitos aplicables.

Puedes consultar la [politica de privacidad de Tradedoubler aqui](https://www.tradedoubler.com/es/privacy-policy/).



## **Contacto**

info@madrescabreadas.com

### Política de cookies

Sólo utilizamos cookies de Google analytics y Disqus, y se rigen por sus respectivas políticas de cookies.

**Contenido incrustado de otros sitios web**

Los artículos de este sitio pueden incluir contenido incrustado (por ejemplo, vídeos, imágenes, artículos, etc.). El contenido incrustado de otras web se comporta exactamente de la misma manera que si el visitante hubiera visitado la otra web.

Estas web pueden recopilar datos sobre ti, utilizar cookies, incrustar un seguimiento adicional de terceros, y supervisar tu interacción con ese contenido incrustado, incluido el seguimiento de su interacción con el contenido incrustado si tienes una cuenta y estás conectado a esa web.